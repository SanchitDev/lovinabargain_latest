<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "site/landing";
$route['404_override'] = '';

$route['admin'] = "admin/adminlogin";
$route['signup'] = "site/user/signup_form";
$route['login'] = "site/user/login_form";
$route['logout'] = "site/user/logout_user";
$route['forgot-password'] = "site/user/forgot_password_form";
$route['send-confirm-mail'] = "site/user/send_quick_register_mail";
$route['onboarding'] = "site/product/onboarding";
$route['gift-cards'] = "site/giftcard";
$route['cart'] = "site/cart";
$route['auctioncart'] = "site/cart/auctioncart";
$route['same-day-delivery'] = "site/samedaydelivery";
$route['checkout/(:any)'] = "site/checkout";
$route['order/(:any)'] = "site/order";
$route['invite-friends'] = "site/user/invite_friends";
$route['settings'] = "site/user_settings";
$route['settings/account'] = "site/user_settings/account_settings";
$route['settings/password'] = "site/user_settings/password_settings";
$route['settings/preferences'] = "site/user_settings/preferences_settings";
$route['settings/notifications'] = "site/user_settings/notifications_settings";
$route['settings/apps'] = "site/user_settings/app_network";
$route['purchases'] = "site/user_settings/user_purchases";

/**** Auction && Bids ***/
$route['bids'] = "site/user/bids";
$route['auctions'] = "site/user/auction_prodcut_list";
$route['auction/(:any)'] = "site/user/auction_detail";
$route['conversation/(:any)'] = "site/user/conversation/$1";
$route['send_message'] = "site/user/send_message";
$route['send_bid'] = "site/user/send_offer";
$route['update_bid'] = "site/user/update_offer";
$route['accept_bid'] = "site/user/accept_offer";
$route['decline_bid'] = "site/user/decline_offer";
$route['reject_bid'] = "site/user/reject_offer";
/**** Auction && Bids ***/

//$route['gifts/(:any)'] = "site/groupgift/display_product_details";
$route['orders'] = "site/user_settings/user_orders";
$route['bulk'] = "site/user_settings/bulk_upload";
$route['fancyybox/manage'] = "site/user_settings/manage_fancyybox";
$route['settings/shipping'] = "site/user_settings/shipping_settings";
$route['settings/shipping-edit/(:any)'] = "site/user_settings/shipping_settings_edit";
$route['shipping-address'] = "site/user_settings/insertEdit_shipping_address";


$route['settings/payment-method'] = "site/user_settings/payment_method";
$route['about/promotions'] = "site/user_settings/promotions";
$route['credits'] = "site/user_settings/user_credits";
$route['referrals/(:any)'] = "site/user_settings/user_referrals/$1";
$route['referrals'] = "site/user_settings/user_referrals";
$route['settings/giftcards'] = "site/user_settings/user_giftcards";
$route['things/(:any)/change_status/(:any)'] = "site/product/change_status/$1";
$route['things/(:any)/edit'] = "site/product/edit_product_detail/$1";
$route['things/(:any)/edit/(:any)'] = "site/product/edit_product_detail/$1";
$route['things/(:any)/delete'] = "site/product/delete_product/$1";
$route['things/shuffle'] = "site/product/display_product_shuffle";
$route['things/(:any)'] = "site/product/display_product_detail/$1";
$route['add'] = "site/product/add_new_product";

$route['user/(:any)/added'] = "site/user/display_user_added";
$route['user/(:any)/lists'] = "site/user/display_user_lists";
$route['user/(:any)/lists/(:num)'] = "site/user/display_user_lists_home";
$route['user/(:any)/lists/(:num)/followers'] = "site/user/display_user_lists_followers";
$route['user/(:any)/lists/(:num)/settings'] = "site/user/edit_user_lists";
$route['user/(:any)/wants'] = "site/user/display_user_wants";
$route['user/(:any)/owns'] = "site/user/display_user_owns";
$route['user/(:any)/activity'] = "site/user/display_user_activity";
$route['user/(:any)/follows'] = "site/user/display_user_follow";
$route['user/(:any)/following'] = "site/user/display_user_following/$1";
$route['user/(:any)/followers'] = "site/user/display_user_followers/$1";
$route['user/(:any)/things/(:any)'] = "site/product/display_user_thing/$1";
$route['user/(:any)'] = "site/user/display_user_profile/$1";
$route['shopby/(:any)'] = "site/searchShop/search_shopby/$1";
$route['shop'] = "site/shop";
$route['stores'] = "site/seller";
$route['badges'] = "site/badges";
$route['seller/dashboard/shop-settings'] = "site/user_settings/shop_settings";

$route['featured'] = "site/user_settings/seller_featured";
$route['featured_plan'] = "site/user_settings/view_featured_plan";
$route['featured_plan_pay'] = "site/user_settings/seller_plan_pay";

$route['giftguide/list/(:any)/followers'] = "site/searchShop/search_priceby_followers/$1";
$route['colorsby/list/(:any)/followers'] = "site/searchShop/search_colorby_followers/$1";

$route['giftguide/list/(:any)'] = "site/searchShop/search_priceby/$1";
$route['colorsby/list/(:any)'] = "site/searchShop/search_colorby/$1";

$route['fancybox'] = "site/fancybox";
$route['fancybox/(:any)/(:any)'] = "site/fancybox/display_fancybox/$1";

$route['sales/create'] = "site/product/sales_create";
$route['seller_signup'] = "site/user/seller_signup_form";

$route['pages/(:num)/(:any)'] = "site/cms/page_by_id";
$route['pages/(:any)'] = "site/cms";

$route['create-brand'] = "site/user/create_brand_form";
$route['view-purchase/(:any)'] = "site/user/view_purchase";
$route['view-order/(:any)'] = "site/user/view_order";
$route['order-review/(:num)/(:num)/(:any)'] = "site/user/order_review";
$route['order-review/(:num)'] = "admin/order/order_review";

$route['image-crop/(:any)'] = "site/user/image_crop";
$route['lang/(:any)'] = "site/fancybox/language_change/$1";

$route['notifications'] = "site/notify/display_notifications";

$route['payment-success'] = "admin/commission/display_payment_success";
$route['payment-failed'] = "admin/commission/display_payment_failed";

$route['bookmarklet'] = "site/bookmarklet";
$route['bookmarklets'] = "site/bookmarklet/display_bookmarklet";

$route['invite'] = "site/user/invite_friends";
$route['find-friends'] = "site/user/find_friends";
$route['feedback/(:num)'] = "site/user/display_user_product_feedback/$1";

$route['update-status/(:any)'] = "site/user_settings/update_buyer_status/$1";
$route['cod-payment'] = "site/user_settings/user_cod_payment";
/**************GROUP GIFTS************/
$route['group-gifts'] = "site/groupgift";
$route['refund/(:any)'] = "site/groupgift/RefundPaymentCredit";
$route['gifts/contribute/(:any)'] = "site/groupgift/payment_contribute_details/$1";
$route['gifts/(:any)'] =  "site/groupgift/display_groupProduct_detail/$1";
/**********GROUP GIFTS END***********/
$route['t/(:any)'] = "site/product/load_short_url";
$route['payment-method-edit/(:any)'] = "site/user_settings/get_upayment_details";

//user affiliate list
$route['affiliate'] = "site/affiliate/index";

/**************Store routing *********/
$route['open-store'] = "site/store/open_store";
$route['store/(:any)'] = "site/store/view_store";
$route['seller/dashboard'] = "site/store/settings";
$route['seller/dashboard/about'] = "site/store/about_settings";
$route['seller/dashboard/profile'] = "site/store/profile_settings";
$route['seller/dashboard/privacy'] = "site/store/privacy_settings";
$route['seller/dashboard/products'] = "site/store/product_settings";
$route['seller/dashboard/orders'] = "site/store/order_settings";
/**************Store routing *********/
$route['seller-product'] = 'site/product/add_newSell_product';
/* End of file routes.php */
/* Location: ./application/config/routes.php */