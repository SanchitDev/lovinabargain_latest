<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This model contains all db functions related to Cart Page
 * @author Teamtweaks
 *
 */
class Checkout_model extends My_Model
{
	
	public function add_checkout($dataArr=''){
			$this->db->insert(PRODUCT,$dataArr);
	}

	public function edit_checkout($dataArr='',$condition=''){
			$this->db->where($condition);
			$this->db->update(PRODUCT,$dataArr);
	}
	
	
	public function view_checkout($condition=''){
			return $this->db->get_where(PRODUCT,$condition);
			
	}
	
	
	public function mani_gift_total($userid=''){
		
		$giftRes = $this->checkout_model->get_all_details(GIFTCARDS_TEMP,array( 'user_id' => $userid));
		$giftAmt = 0;
		if($giftRes -> num_rows() > 0 ){ 
			
			foreach ($giftRes->result() as $giftRow){
				$giftAmt = $giftAmt + $giftRow->price_value;
			}

		}
		
		return number_format($giftAmt,2,'.','');

	}
	
	public function mani_checkout_total($userid=''){
		
		
		$checkoutVal = $this->checkout_model->get_all_details(SHOPPING_CART,array( 'user_id' => $userid));
		$checkoutAmt = 0; $checkoutShippingAmt = 0; $checkoutTaxAmt = 0;
		$Shipping_Val = $this->checkout_model->get_all_details(PAYMENT,array( 'user_id' => $userid, 'dealCodeNumber' => $this->session->userdata('randomNo')));
		
		if($checkoutVal -> num_rows() > 0 ){ 
			foreach ($checkoutVal->result() as $CartRow){
				$checkoutAmt = $checkoutAmt + (($CartRow->product_shipping_cost +  ($CartRow->product_tax_cost * 0.01 * $CartRow->price) + $CartRow->price)  * $CartRow->quantity);
			}
			$checkoutSAmt = $Shipping_Val->row()->shippingcost;
			if($checkoutSAmt == ''){
				$checkoutSAmt = 0.00;
			}
			$checkoutTAmt = $Shipping_Val->row()->tax;
			$grantAmt = $checkoutAmt + $checkoutSAmt + $checkoutTAmt ;
			
		}
		
		$this->db->select('discountAmount');
		$this->db->from(SHOPPING_CART);
		$this->db->where('user_id = '.$userid);
		$query = $this->db->get();
		
		if($query->row()->discountAmount !=''){
			$grantAmt = $grantAmt - $query->row()->discountAmount;
		}
		
		return number_format($checkoutAmt,2,'.','').'|'.number_format($checkoutSAmt,2,'.','').'|'.number_format($checkoutTAmt,2,'.','').'|'.number_format($grantAmt,2,'.','').'|'.$countVal.'|'.number_format($query->row()->discountAmount,2,'.','').'|'.$Shipping_Val->row()->shippingid;

	}
	public function auction_mani_checkout_total($userid='',$cart_id){
		$checkoutVal = $this->checkout_model->get_all_details(SHOPPING_CART,array( 'type'=>'auction','user_id' => $userid,'id'=>$cart_id));
		// echo '<pre>';print_r($checkoutVal->result());die;
		$checkoutAmt = 0; $checkoutShippingAmt = 0; $checkoutTaxAmt = 0;
		$Shipping_Val = array();
		if($checkoutVal->num_rows() > 0 ){
			
			$checkoutAmt = $checkoutAmt + (($checkoutVal->row()->product_shipping_cost +  ($checkoutVal->row()->product_tax_cost * 0.01 * $checkoutVal->row()->price) + $checkoutVal->row()->price)  * $checkoutVal->row()->quantity);
			
			$checkoutSAmt = $checkoutVal->row()->product_shipping_cost;
			$checkoutTAmt = $checkoutVal->row()->tax;
			$grantAmt = $checkoutAmt + $checkoutTAmt ;
		}
		$this->checkout_model->update_details(SHOPPING_CART,array('authorised_amount'=>$grantAmt),array('id'=>$cart_id));
		$this->db->select('discountAmount');
		$this->db->from(SHOPPING_CART);
		$this->db->where('user_id = '.$userid);
		$this->db->where('type = "auction"');
		$query = $this->db->get();
		if($query->row()->discountAmount !=''){
			$grantAmt = $grantAmt - $query->row()->discountAmount;
		}
		
		return number_format($checkoutAmt,2,'.','').'|'.number_format($checkoutSAmt,2,'.','').'|'.number_format($checkoutTAmt,2,'.','').'|'.number_format($grantAmt,2,'.','').'|'.$checkoutVal->row()->ship_to.'|'.number_format($query->row()->discountAmount,2,'.','');
		
    }
	
	
	
	public function mani_subcribe_total($userid=''){
	
		$SubcribRes = $this->checkout_model->get_all_details(FANCYYBOX_TEMP,array( 'user_id' => $userid));
		$SubcribAmt = 0; $SubcribSAmt = 0; $SubcribTAmt = 0; $SubcribTotalAmt = 0;
		if($SubcribRes -> num_rows() > 0 ){ 
			
			foreach ($SubcribRes->result() as $SubscribRow){
				$SubcribAmt = $SubcribAmt + $SubscribRow->price;
			}
			$SubcribSAmt = $SubcribRes->row()->shipping_cost;
			$SubcribTAmt = $SubcribRes->row()->tax;
			$SubcribTotalAmt = $SubcribAmt + $SubcribSAmt + $SubcribTAmt ;

		}
		
		
		return number_format($SubcribAmt,2,'.','').'|'.number_format($SubcribSAmt,2,'.','').'|'.number_format($SubcribTAmt,2,'.','').'|'.number_format($SubcribTotalAmt,2,'.','');

	}
	
	
	
	public function view_checkout_details($condition = ''){
		$select_qry = "select p.*,u.full_name,u.user_name,u.thumbnail from ".PRODUCT." p LEFT JOIN ".USERS." u on u.id=p.user_id ".$condition;
		$checkoutList = $this->ExecuteQuery($select_qry);
		return $checkoutList;
			
	}
	
	public function view_atrribute_details(){
		$select_qry = "select * from ".ATTRIBUTE." where status='Active'";
		return $attList = $this->ExecuteQuery($select_qry);
	
	}
	public function addAuctionPayment($userid = '', $paymentType = '', $productIds='', $type='',$authorize_amt='', $transaction_id='', $cart_id='', $token = '') {

		$productArr = array_filter(explode(',', $productIds));
        $condition = "a.id = ".$cart_id."";
        $this->db->select('a.*,p.product_name,p.seller_product_id,p.image,c.attr_name as attr_type,d.attr_name,d.pid as attrPID');
        $this->db->from(SHOPPING_CART . ' as a');
        $this->db->join(PRODUCT . ' as p', 'p.id=a.product_id');
        $this->db->join(SUBPRODUCT . ' as d', 'd.pid = a.attribute_values', 'left');
        $this->db->join(PRODUCT_ATTRIBUTE . ' as c', 'c.id = d.attr_id', 'left');
        $this->db->where($condition);
		$this->db->where_in('a.product_id', $productArr);
        $AddPayt = $this->db->get();

        $dealCodeNumber = mt_rand();
  

        $condition1 = 'user_id = "'.$userid.'" and country="'.$this->input->post('shipping_id').'"';
        $shippingAddr = $this->cart_model->get_all_details(SHIPPING_ADDRESS, $condition1);
        $insertIds = array();
       
        foreach($AddPayt->result() as $result){
			if(in_array($result->product_id, $productArr)){
				$this->cart_model->update_details(SHOPPING_CART, array('ship_product'=>'Yes'), array('product_id'=>$result->product_id));
			}
            if($this->input->get('is_gift') == '') {
                $ordergift = 0;
            }else{
                $ordergift = 1;
            }
            $attr_name = '';
            if($result->attr_type != '' && $result->attr_name != ''){
                $attr_name = $result->attr_type . '/' . $result->attr_name.'/'.$result->attrPID;
            }
            $sumTotal = number_format((($result->price + $result->product_shipping_cost + ($result->product_tax_cost * 0.01 * $result->price)) * $result->quantity), 2, '.', '');
            if($result->product_type == 'physical'){
                $shipdata = 'shippingcountry = "' . $shippingAddr->row()->country . '",
				shippingid = "' . $this->input->get('Ship_address_val') . '",
				shippingstate = "' . $shippingAddr->row()->state . '",
				shippingcity = "' . $shippingAddr->row()->city . '",
				shippingcost = "' . $this->input->get('cart_ship_amount') . '",
				tax = "' . $this->input->get('cart_tax_amount') . '",
				product_shipping_cost = "' . $result->product_shipping_cost . '",
				product_tax_cost = "' . $result->product_tax_cost . '",';
				$status = 'Pending';
            }else{
                $shipdata = '';
            }
            if($result->product_type == 'digital'){
                $digital = 'product_download_code = "' . $dealCodeNumber . '@' . $result->seller_product_id . '",
				product_code_status = "Pending",
				product_type = "' . $result->product_type . '",';
				$status = 'Paid';
            }else{
                $digital = '';
            }
			
            $insert = ' insert into ' . PAYMENT . ' set
				product_id 				= "' . $result->product_id . '",
				auction_product_id 		= "' . $result->product_id . '",
                type 					= "' . $type . '",
				sell_id 				= "' . $result->sell_id . '",
				price 					= "' . $result->price . '",
				quantity 				= "' . $result->quantity . '",
				indtotal 				= "' . $result->indtotal . '",
				coupon_id  				= "' . $result->couponID . '",
				discountAmount 			= "' . $this->input->get('discount_Amt') . '",
				couponCode  			= "' . $result->couponCode . '",
				coupontype 				= "' . $result->coupontype . '",
                sumtotal 				= "' . $sumTotal . '",
				user_id 				= "' . $result->user_id . '",
				created 				= now(),
				dealCodeNumber 			= "' . $dealCodeNumber . '",
				status 					= "'.$status.'",
				do_capture 				= "Pending",
						' . $shipdata . '
				payment_type 			= "' . $paymentType . '",
				attribute_values		= "' . $result->attribute_values . '",
				shipping_status			= "Pending",
				total  					= "' . $authorize_amt . '",
				athorized_amount  		= "' . $authorize_amt . '",
				paypal_transaction_id	= "' . $transaction_id . '",
				token					= "' . $token . '",
				note 					= "",
				order_gift 				= "' . $ordergift . '",
				old_product_name		= "' . $result->product_name . '",
				old_attr_name 			= "' . $attr_name . '",
				old_image 				= "' . $result->image . '",
						' . $digital . '
				inserttime 				= "' . time() . '"';
            $insertIds[] = $this->cart_model->ExecuteQuery($insert, 'insert');
        }
        $lastpmtid = $this->cart_model->get_last_insert_id();
        if( $lastpmtid  != ''){
       	 $this->cart_model->CommonDelete(SHOPPING_CART,array('id'=>$cart_id));
   		}

        return $insertIds;
    }
	
	
	
	
}

?>