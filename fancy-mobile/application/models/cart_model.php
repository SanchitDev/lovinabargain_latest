<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * This model contains all db functions related to Cart Page
 * @author Teamtweaks
 *
 */
class Cart_model extends My_Model
{

	public function add_cart($dataArr=''){
		$this->db->insert(PRODUCT,$dataArr);
	}

	public function edit_cart($dataArr='',$condition=''){
		$this->db->where($condition);
		$this->db->update(PRODUCT,$dataArr);
	}


	public function view_cart($condition=''){
		return $this->db->get_where(PRODUCT,$condition);
			
	}


	public function mani_cart_view($userid=''){

		$MainShipCost = 0;
		$MainTaxCost = 0; $cartQty = 0;


		$shipVal = $this->cart_model->get_all_details(SHIPPING_ADDRESS,array( 'user_id' => $userid));

		if($shipVal -> num_rows() >0 ){

			$shipValID = $this->cart_model->get_all_details(SHIPPING_ADDRESS,array( 'user_id' => $userid, 'primary' => 'Yes'));
			$dataArr = array('shipping_id' => $shipValID->row()->id);
			$condition = array('user_id' => $userid);
			$this->cart_model->update_details(FANCYYBOX_TEMP,$dataArr,$condition);
			$ShipCostVal = $this->cart_model->get_all_details(COUNTRY_LIST,array( 'country_code' => $shipValID->row()->country));

			$MainShipCost = $ShipCostVal->row()->shipping_cost;
			$MainTaxCost = $ShipCostVal->row()->shipping_tax;
			$dataArr2 = array('shipping_cost' => $MainShipCost, 'tax' => $MainTaxCost);
			$this->cart_model->update_details(SHOPPING_CART,$dataArr2,$condition);
		}

		$GiftValue = ''; $CartValue = ''; $SubscribValue = '';

		$giftSet = $this->cart_model->get_all_details(GIFTCARDS_SETTINGS,array( 'id' => '1'));
		$giftRes = $this->cart_model->get_all_details(GIFTCARDS_TEMP,array( 'user_id' => $userid));

		$SubcribRes = $this->minicart_model->get_all_details(FANCYYBOX_TEMP,array( 'user_id' => $userid));

		$this->db->select('a.*,b.product_name,b.quantity as mqty,b.seourl,b.quantity as pqty,b.image,b.id as prdid,b.price as orgprice,b.ship_immediate,c.attr_name as attr_type,d.attr_name');
		$this->db->from(SHOPPING_CART.' as a');
		$this->db->join(PRODUCT.' as b' , 'b.id = a.product_id');
		$this->db->join(SUBPRODUCT.' as d' , 'd.pid = a.attribute_values','left');		
		$this->db->join(PRODUCT_ATTRIBUTE.' as c' , 'c.id = d.attr_id','left');		
		$this->db->where('a.user_id = '.$userid);
		$cartVal = $this->db->get();
		
		$this->db->select('discountAmount,couponID,couponCode,coupontype');
		$this->db->from(SHOPPING_CART);
		$this->db->where('user_id = '.$userid);
		$discountQuery = $this->db->get();
		
		$disAmt = $discountQuery->row()->discountAmount;

		/**Lanuage Transalation**/
			
		if($this->lang->line('cart_product') != '')
		$cart_product =  stripslashes($this->lang->line('cart_product'));
		else
		$cart_product =  "Product";

		if($this->lang->line('giftcard_price') != '')
		$giftcard_price =  stripslashes($this->lang->line('giftcard_price'));
		else
		$giftcard_price =  "Price";
			
		if($this->lang->line('product_quantity') != '')
		$product_quantity =  stripslashes($this->lang->line('product_quantity'));
		else
		$product_quantity =  "Quantity";


		if($this->lang->line('purchases_total') != '')
		$purchases_total =  stripslashes($this->lang->line('purchases_total'));
		else
		$purchases_total =  "Total";
			
		if($this->lang->line('product_remove') != '')
		$product_remove =  stripslashes($this->lang->line('product_remove'));
		else
		$product_remove =  "Remove";
			
		if($this->lang->line('giftcard_reci_name') != '')
		$giftcard_reci_name =  stripslashes($this->lang->line('giftcard_reci_name'));
		else
		$giftcard_reci_name =  "Recipient name";
			
		if($this->lang->line('giftcard_rec_email') != '')
		$giftcard_rec_email =  stripslashes($this->lang->line('giftcard_rec_email'));
		else
		$giftcard_rec_email =  "Recipient e-mail";
			
		if($this->lang->line('giftcard_message') != '')
		$giftcard_message =  stripslashes($this->lang->line('giftcard_message'));
		else
		$giftcard_message =  "Message";
			
		if($this->lang->line('choose_shipping_address') != '')
		$choose_shipping_address =  stripslashes($this->lang->line('choose_shipping_address'));
		else
		$choose_shipping_address =  "Choose Your Shipping Address";

		if($this->lang->line('delete_this_address') != '')
		$delete_this_address =  stripslashes($this->lang->line('delete_this_address'));
		else
		$delete_this_address =  "Delete this address";

		if($this->lang->line('addnew_shipping_address') != '')
		$addnew_shipping_address =  stripslashes($this->lang->line('addnew_shipping_address'));
		else
		$addnew_shipping_address =  "Add new shipping address";
			
		if($this->lang->line('continue_payment') != '')
		$continue_payment =  stripslashes($this->lang->line('continue_payment'));
		else
		$continue_payment =  "Continue to Payment";
			
		if($this->lang->line('checkout_item_total') != '')
		$checkout_item_total =  stripslashes($this->lang->line('checkout_item_total'));
		else
		$checkout_item_total =  "Item total";
			
		if($this->lang->line('update') != '')
		$update =  stripslashes($this->lang->line('update'));
		else
		$update =  "Update";
			
		if($this->lang->line('referrals_shipping') != '')
		$referrals_shipping =  stripslashes($this->lang->line('referrals_shipping'));
		else
		$referrals_shipping =  "Shipping";

		if($this->lang->line('shipping_speed') != '')
		$shipping_speed =  stripslashes($this->lang->line('shipping_speed'));
		else
		$shipping_speed =  "Shipping Speed";
			
		if($this->lang->line('items_in_shopping') != '')
		$items_in_shopping =  stripslashes($this->lang->line('items_in_shopping'));
		else
		$items_in_shopping =  "items in shopping cart";
			
		if($this->lang->line('order_from') != '')
		$order_from =  stripslashes($this->lang->line('order_from'));
		else
		$order_from =  "Order from";
			
		if($this->lang->line('merchant') != '')
		$merchant =  stripslashes($this->lang->line('merchant'));
		else
		$merchant =  "Merchant";
			
		if($this->lang->line('header_ship_within') != '')
		$header_ship_within =  stripslashes($this->lang->line('header_ship_within'));
		else
		$header_ship_within =  "Ships within 1-3 business days";

		if($this->lang->line('shopping_cart_empty') != '')
		$shopping_cart_empty =  stripslashes($this->lang->line('shopping_cart_empty'));
		else
		$shopping_cart_empty =  "Your Shopping Cart is Empty";

		if($this->lang->line('dont_miss') != '')
		$dont_miss =  stripslashes($this->lang->line('dont_miss'));
		else
		$dont_miss =  "Don`t miss out on awesome sales right here on";

		if($this->lang->line('shall_we') != '')
		$shall_we =  stripslashes($this->lang->line('shall_we'));
		else
		$shall_we =  "Let`s fill that cart, shall we?";

		if($this->lang->line('checkout_tax') != '')
		$checkout_tax =  stripslashes($this->lang->line('checkout_tax'));
		else
		$checkout_tax =  "Tax";

		if($this->lang->line('gift') != '')
		$gift =  stripslashes($this->lang->line('gift'));
		else
		$gift =  "Gift";
			
			
		if($this->lang->line('order_is_gift') != '')
		$order_is_gift =  stripslashes($this->lang->line('order_is_gift'));
		else
		$order_is_gift =  "This order is a gift";

		if($this->lang->line('coupon_codes') != '')
		$coupon_codes =  stripslashes($this->lang->line('coupon_codes'));
		else

		$coupon_codes =  "Coupon Codes";

		if($this->lang->line('checkout_order') != '')
		$checkout_order =  stripslashes($this->lang->line('checkout_order'));
		else
		$checkout_order =  "Order";

		if($this->lang->line('ship_to') != '')
		$ship_to =  stripslashes($this->lang->line('ship_to'));
		else
		$ship_to =  "Ship to";

		if($this->lang->line('note_to') != '')
		$note_to =  stripslashes($this->lang->line('note_to'));
		else
		$note_to =  "Note to";

		if($this->lang->line('apply') != '')
		$apply =  stripslashes($this->lang->line('apply'));
		else
		$apply =  "Apply";
			
		if($this->lang->line('optional') != '')
		$optional =  stripslashes($this->lang->line('optional'));
		else
		$optional =  "Optional";
			
		if($this->lang->line('note_here') != '')
		$note_here =  stripslashes($this->lang->line('note_here'));
		else
		$note_here =  "You can leave a personalized note here";

		if($this->lang->line('have_coupon_code') != '')
		$have_coupon_code =  stripslashes($this->lang->line('have_coupon_code'));
		else
		$have_coupon_code =  "Have a coupon code?";
			
		if($this->lang->line('retail_price') != '')
		$retail_price =  stripslashes($this->lang->line('retail_price'));
		else
		$retail_price =  "Retail price";

		/**Lanuage Transalation**/


		$resultCart = $cartVal->result_array();
		/* echo "<pre>"; print_r($resultCart);
		die; */
		/****************************** Gift Card Displays **************************************/

		if($giftRes -> num_rows() > 0 ){

			$GiftValue.= '<div id="GiftCartTable" style="display:block;background: none repeat scroll 0 0 #FFFFFF;margin-bottom:10px;border: 1px solid #BCBFC3;border-radius: 0.2em;box-shadow: 0 0 0.3em rgba(202, 203, 209, 0.2), 0 1px 0.3em rgba(0, 0, 0, 0.1);font-size: 1em;">
			<dt style="font-weight:normal" class="tit-profile"><b>'.$this->config->item('email_title').' Gift Cards</b>
				<button class="cancel-search" style="display: inline-block;border: 1px solid #c3c3c3;padding: 0 1em;border-radius: 0.15em;color: #4c4f53;font-weight: bold;background: #f0f0f0;background: -webkit-linear-gradient(top, #fcfcfc,#f0f0f0);background: -ms-linear-gradient(top, #fafafa,#f0f0f0);background: -moz-linear-gradient(top, #fafafa,#f0f0f0);background: -o-linear-gradient(top, #fafafa,#f0f0f0);filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#fafafa\',endColorstr=\'#f0f0f0\');box-shadow: 0 1px 1px rgba(0,0,0,0.1);line-height: 1.85em;font-size: 0.94em;text-shadow: 0 1px 0 #fff;border-color: #c3c3c3 #bebebe #b6b6b6;position: absolute;right: 5px;top:7px;" type="button">Cancel</button>
				<button class="grey-search" type="button"><i class="delete-ico" style="height:1.4em;"></i></button></dt>
			<form method="post" name="giftSubmit" id="giftSubmit" class="continue_payment" enctype="multipart/form-data" action="site/cart/giftcheckout">
				<div class="cart-payment-wrap cart-note"><span class="cart-payment-top"><b></b></span><div class="table-cart-wrap">';	
			$giftAmt = 0; $g=0;

			foreach ($giftRes->result() as $giftRow){
			$GiftValue.= '<div id="giftId_'.$g.'" style="display:block;">';
			$GiftValue.='<dl class="profile-account"><dd class="cart-item-list"><ul>';
			$GiftValue.='<li style="position:relative;"><img style="width:70px;" src="'.GIFTPATH.$giftSet->row()->image.'" alt="'.$giftSet->row()->title.'"><br>
			<span class="option-tit">'.$giftcard_reci_name.':</span><span class="option-txt">'.$giftRow->recipient_name.'</span><br>
			<span class="option-tit">'.$giftcard_rec_email.':</span><span class="option-txt">'.$giftRow->recipient_mail.'</span><br>
			<span class="option-tit">'.$giftcard_message.':</span><span class="option-txt">'.$giftRow->description.'</span>
			<span style="float: right;position: absolute;right: 0;top: 20px;font-weight: bold;">'.$this->data['currencySymbol'].$giftRow->price_value.'</span>
			<a href="javascript:delete_gift('.$giftRow->id.','.$g.')" class="remove_gift_card" style="display: none;
border: 1px solid #c3c3c3;
padding: 0 1em;
border-radius: 0.15em;
color: #4c4f53;
font-weight: bold;
background: #f0f0f0;
background: -webkit-linear-gradient(top, #fcfcfc,#f0f0f0);
background: -ms-linear-gradient(top, #fafafa,#f0f0f0);
background: -moz-linear-gradient(top, #fafafa,#f0f0f0);
background: -o-linear-gradient(top, #fafafa,#f0f0f0);
filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#fafafa\',endColorstr=\'#f0f0f0\');
box-shadow: 0 1px 1px rgba(0,0,0,0.1);
line-height: 1.85em;
font-size: 0.94em;
text-shadow: 0 1px 0 #fff;
border-color: #c3c3c3 #bebebe #b6b6b6;
position: absolute;
right: 0px;
bottom: 5px;
z-index:3;" cid="66577">'.$product_remove.'</a>
<span class="trick"></span>
			</li>
			';
			 $GiftValue.='</ul></dd></dl></div>';
				$giftAmt = $giftAmt + $giftRow->price_value;
				$g++;
			}
			
  			$GiftValue.='<dl class="profile-account"><dd><p style="position:relative;">
        <label class="label">Payment Option</label>
        <ul class="payment-method">
        	<li>
				<input id="" class="" type="radio" checked="checked" value="creditcard" name="payment_type">
				<label for="">
				<i class="camr"></i>
				</label>
			</li>
        	<li>
				<input id="" type="radio" value="paypal" name="payment_type">
				<label for="">
				<b>PayPal</b>
				</label>
			</li>
  					<li>
				<input id="" type="radio" value="paypal_adaptive" name="payment_type">
				<label for="">
				<b>PayPal(Adaptive)</b>
				</label>
			</li>
        
  					<li>
				<input id="" type="radio" value="paypal_twocheckout" name="payment_type">
				<label for="">
				<b>Twocheckout</b>
				</label>
			</li>
			
			<li>
				<input id="" type="radio" value="paypal_cashondelivery" name="payment_type">
				<label for="">
				<b>Cash on Delivery</b>
				</label>
			</li>
        </ul>
        </p></dd></dl>';
			
			$GiftValue.= '</div>
			<input name="gift_cards_amount" id-"gift_cards_amount" value="'.number_format($giftAmt,2,'.','').'" type="hidden">
			<input name="checkout_type" id-"checkout_type" value="giftpurchase" type="hidden">
			<div class="cart-payment" id="giftcard-cart-payment"><input type="hidden"><span class="bg-cart-payment"></span>
<!--		    <dl class="cart-payment-order">
			   	<dt>'.$checkout_order.'</dt><dd>
			   	<ul>
				<li class="first"><span class="order-payment-type">'.$checkout_item_total.'</span><span class="order-payment-usd"><b>'.$this->data['currencySymbol'].'<span id="item_total">'.number_format($giftAmt,2,'.','').'</span></b> '.$this->data['currencyType'].'</span></li>
				<li class="total"><span class="order-payment-type"><b>Total</b></span><span class="order-payment-usd"><b>'.$this->data['currencySymbol'].'<span id="total_item">'.number_format($giftAmt,2,'.','').'</span></b> '.$this->data['currencyType'].'</span></li>
				</ul>
		      	</dd>
			</dl>
-->			<dl class="profile-account">
			 <dd style=" padding: 1em;">
		    <button type="submit" style="width:100%;background-color: #569D37;" class="btn btn-green" id="button-submit-giftcard">'.$continue_payment.'</button>
			</dd>
        </dl>
		  	</div></div></form></div>';
			
			
        
       
      
         
			
			
		}

		/****************************** Subscribe Card Displays **************************************/
		if($SubcribRes -> num_rows() > 0 ){
			$SubscribValue.= '<div id="SubscribeCartTable" style="display:block;background: none repeat scroll 0 0 #FFFFFF;margin-bottom:10px;border: 1px solid #BCBFC3;border-radius: 0.2em;box-shadow: 0 0 0.3em rgba(202, 203, 209, 0.2), 0 1px 0.3em rgba(0, 0, 0, 0.1);font-size: 1em;">
			<dt style="font-weight:normal" class="tit-profile">Oliver Subscriptions</b>
				<button class="cancel-search" style="display: inline-block;border: 1px solid #c3c3c3;padding: 0 1em;border-radius: 0.15em;color: #4c4f53;font-weight: bold;background: #f0f0f0;background: -webkit-linear-gradient(top, #fcfcfc,#f0f0f0);background: -ms-linear-gradient(top, #fafafa,#f0f0f0);background: -moz-linear-gradient(top, #fafafa,#f0f0f0);background: -o-linear-gradient(top, #fafafa,#f0f0f0);filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#fafafa\',endColorstr=\'#f0f0f0\');box-shadow: 0 1px 1px rgba(0,0,0,0.1);line-height: 1.85em;font-size: 0.94em;text-shadow: 0 1px 0 #fff;border-color: #c3c3c3 #bebebe #b6b6b6;position: absolute;right: 5px;top:7px;" type="button">Cancel</button>
				<button class="grey-search" type="button"><i class="delete-ico" style="height:1.4em;"></i></button></dt>
			<form method="post" name="SubscribeSubmit" id="SubscribeSubmit" class="continue_payment" enctype="multipart/form-data" action="site/cart/Subscribecheckout">
				<div class="cart-payment-wrap cart-note"><span class="cart-payment-top"><b></b></span><div class="table-cart-wrap">
				';	
			$SubscribAmt = 0; $subcribSAmt = 0; $subcribTAmt = 0; $SubgrantAmt = 0; $s=0;

			foreach ($SubcribRes->result() as $SubcribRow){
				$SubscribValue.= '<div id="SubscribId_'.$s.'" style="display:block;">';
				
				 $SubscribValue.='<dl class="profile-account"><dd class="cart-item-list"><ul>
					<li><img style="width:70px;" src="'.FANCYBOXPATH.$SubcribRow->image.'" alt="'.$SubcribRow->name.'">'.$SubcribRow->name.'
					<span style="position: absolute;right: 0;top: 20px;font-weight: bold;">'.$this->data['currencySymbol'].number_format($SubcribRow->price,2,'.','').'</span>
					<a style="display: none;
border: 1px solid #c3c3c3;
padding: 0 1em;
border-radius: 0.15em;
color: #4c4f53;
font-weight: bold;
background: #f0f0f0;
background: -webkit-linear-gradient(top, #fcfcfc,#f0f0f0);
background: -ms-linear-gradient(top, #fafafa,#f0f0f0);
background: -moz-linear-gradient(top, #fafafa,#f0f0f0);
background: -o-linear-gradient(top, #fafafa,#f0f0f0);
filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#fafafa\',endColorstr=\'#f0f0f0\');
box-shadow: 0 1px 1px rgba(0,0,0,0.1);
line-height: 1.85em;
font-size: 0.94em;
text-shadow: 0 1px 0 #fff;
border-color: #c3c3c3 #bebebe #b6b6b6;
position: absolute;
right: 0px;
bottom: 5px;
z-index:3;" href="javascript:delete_subscribe('.$SubcribRow->id.','.$s.')" class="remove_gift_card" cid="66577">'.$product_remove.'</a>
<span class="trick"></span>
</li>
					</div>';
				$SubscribAmt = $SubscribAmt + $SubcribRow->price;
				$s++;
			}
			$subcribSAmt = $MainShipCost;
			$subcribTAmt = ($SubscribAmt * 0.01 * $MainTaxCost);
			$SubgrantAmt = $SubscribAmt + $subcribSAmt + $subcribTAmt ;





			


$SubscribValue.= '<dl class="profile-account" style="position:relative;"><dd><label class="label">'.$ship_to.'</label>
				<a style="cursor: pointer;
background: #ccc;
padding: 5px;
display: inline-block;
border: 1px solid #c3c3c3;
padding: 0 1em;
border-radius: 0.15em;
color: #4c4f53;
font-weight: bold;
background: #f0f0f0;
background: -webkit-linear-gradient(top, #fcfcfc,#f0f0f0);
background: -ms-linear-gradient(top, #fafafa,#f0f0f0);
background: -moz-linear-gradient(top, #fafafa,#f0f0f0);
background: -o-linear-gradient(top, #fafafa,#f0f0f0);
filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#fafafa\',endColorstr=\'#f0f0f0\');
box-shadow: 0 1px 1px rgba(0,0,0,0.1);
line-height: 1.85em;
font-size: 0.94em;
text-shadow: 0 1px 0 #fff;
border-color: #c3c3c3 #bebebe #b6b6b6;
position: absolute;
right: 20px;
top: 30px;" class="adres-button add-shipping-addr" href="javascript:void(0);" >'.$addnew_shipping_address.'</a>
				<select data-type="subscribe" onchange="SubscribeChangeAddress(this.value,this);">
					<option value="" id="address-select">'.$choose_shipping_address.'</option>
			';



//old

			/*$SubscribValue.= '</div>
			 <div class="cart-payment" id="merchant-cart-payment">
		    <input type="hidden">
		    <span class="bg-cart-payment"></span>
		    <dl class="cart-payment-ship">
		      <dt>'.$ship_to.'</dt>
		      <dd>
			<select id="address-cart" class="select-round select-shipping-addr" onchange="SubscribeChangeAddress(this.value);">
				  <option value="" id="address-select">'.$choose_shipping_address.'</option>
			';*/

			foreach ($shipVal->result() as $Shiprow){
				if($Shiprow->primary == 'Yes'){ $optionsValues = 'selected="selected"';
				$ChooseVal = $Shiprow->full_name.'<br>'.$Shiprow->address1.'<br>'.$Shiprow->city.' '.$Shiprow->state.' '.$Shiprow->postal_code.'<br>'.$Shiprow->country.'<br>'.$Shiprow->phone; $ship_id =$Shiprow->id;  }else{ $optionsValues ='';}
				$SubscribValue.='<option '.$optionsValues.' value="'.$Shiprow->id.'" l1="'.$Shiprow->full_name.'" l2="'.$Shiprow->address1.'" l3="'.$Shiprow->city.' '.$Shiprow->state.' '.$Shiprow->postal_code.'" l4="'.$Shiprow->country.'" l5="'.$Shiprow->phone.'">'.$Shiprow->full_name.'</option>';
			}

			$SubscribValue.='</select>
			<input type="hidden" name="SubShip_address_val" id="SubShip_address_val_subscribe" value="'.$ship_id.'" />
			<p class="default_addr"><span id="SubChg_Add_Val">'.$ChooseVal.'</span></p>
				<span class="address-table"> </span>
				<span class="deleta-address" style="cursor: pointer;
background: #ccc;
padding: 5px;
display: inline-block;
border: 1px solid #c3c3c3;
padding: 0 1em;
border-radius: 0.15em;
color: #4c4f53;
font-weight: bold;
background: #f0f0f0;
background: -webkit-linear-gradient(top, #fcfcfc,#f0f0f0);
background: -ms-linear-gradient(top, #fafafa,#f0f0f0);
background: -moz-linear-gradient(top, #fafafa,#f0f0f0);
background: -o-linear-gradient(top, #fafafa,#f0f0f0);
filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#fafafa\',endColorstr=\'#f0f0f0\');
box-shadow: 0 1px 1px rgba(0,0,0,0.1);
line-height: 1.85em;
font-size: 0.94em;
text-shadow: 0 1px 0 #fff;
border-color: #c3c3c3 #bebebe #b6b6b6;
position: absolute;
right: 20px;
bottom: 10px;
z-index:3;" data-type="subscribe" onclick="subshipping_cart_address_delete(this);">'.$delete_this_address.'</span>
<span class="trick"></span>
				</p></dd></dl>


			

<!--			   <dl class="cart-payment-order">
		      <dt>'.$checkout_order.'</dt>
		      <dd>
			<ul>
			  <li class="first">
			    <span class="order-payment-type">'.$checkout_item_total.'</span>
			    <span class="order-payment-usd"><b>'.$this->data['currencySymbol'].'<span id="SubCartAmt">'.number_format($SubscribAmt,2,'.','').'</span></b> '.$this->data['currencyType'].'</span>
			  </li>
			  <li>
			    <span class="order-payment-type">'.$referrals_shipping.'</span>
			    <span class="order-payment-usd"><b>'.$this->data['currencySymbol'].'<span id="SubCartSAmt">'.number_format($subcribSAmt,2,'.','').'</span></b> '.$this->data['currencyType'].'</span>
			  </li>
			  <li>
			    <span class="order-payment-type">'.$checkout_tax.' (<span id="SubTamt">'.$MainTaxCost.'</span>%) of '.$this->data['currencySymbol'].$SubscribAmt.'</span>
			    <span class="order-payment-usd"><b>'.$this->data['currencySymbol'].'<span id="SubCartTAmt">'.number_format($subcribTAmt,2,'.','').'</span></b> '.$this->data['currencyType'].'</span>
			  </li></ul>';

			$SubscribValue.='
		  <ul>
			 <li class="total">
			    <span class="order-payment-type"><b>Total</b></span>
			    <span class="order-payment-usd"><b>'.$this->data['currencySymbol'].'<span id="SubCartGAmt">'.number_format($SubgrantAmt,2,'.','').'</span></b> '.$this->data['currencyType'].'</span>
			  </li>
			</ul>
		      </dd>
	              
		    </dl>
-->			
		    <input name="user_id" value="'.$userid.'" type="hidden">
			<input name="subcrib_amount" id="subcrib_amount" value="'.number_format($SubscribAmt,2,'.','').'" type="hidden">
			<input name="subcrib_ship_amount" id="subcrib_ship_amount" value="'.number_format($subcribSAmt,2,'.','').'" type="hidden">
			<input name="subcrib_tax_amount" id="subcrib_tax_amount" value="'.number_format($subcribTAmt,2,'.','').'" type="hidden">
			<input name="subcrib_total_amount" id="subcrib_total_amount" value="'.number_format($SubgrantAmt,2,'.','').'" type="hidden">
			<dl class="profile-account">
        <dd style=" padding: 1em;">
		    <input type="submit" class="btn btn-green" style="background-color: #569D37;cursor:pointer;" name="SubscribePayment" id="button-submit-merchant" value="'.$continue_payment.'" />
		    </dd>
			</dl>
		  </div>
		</div>
	</form></div>';
		}


		/****************************** Cart Displays **************************************/

		if($cartVal -> num_rows() > 0 ){
			$CartValue.='<div id="cart_container" style="background: none repeat scroll 0 0 #FFFFFF;border: 1px solid #BCBFC3;border-radius: 0.2em;box-shadow: 0 0 0.3em rgba(202, 203, 209, 0.2), 0 1px 0.3em rgba(0, 0, 0, 0.1);font-size: 1em;"><dl class="profile-account"><dt style="font-weight:normal" class="tit-profile">'.$order_from.' <b>'.$this->config->item('email_title').' '.$merchant.'</b>
				<button class="cancel-search" style="display: inline-block;border: 1px solid #c3c3c3;padding: 0 1em;border-radius: 0.15em;color: #4c4f53;font-weight: bold;background: #f0f0f0;background: -webkit-linear-gradient(top, #fcfcfc,#f0f0f0);background: -ms-linear-gradient(top, #fafafa,#f0f0f0);background: -moz-linear-gradient(top, #fafafa,#f0f0f0);background: -o-linear-gradient(top, #fafafa,#f0f0f0);filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#fafafa\',endColorstr=\'#f0f0f0\');box-shadow: 0 1px 1px rgba(0,0,0,0.1);line-height: 1.85em;font-size: 0.94em;text-shadow: 0 1px 0 #fff;border-color: #c3c3c3 #bebebe #b6b6b6;position: absolute;right: 5px;top:7px;" type="button">Cancel</button>
				<button class="grey-search" type="button"><i class="delete-ico" style="height:1.4em;"></i></button></dt>
				<form method="post" name="cartSubmit" id="cartSubmit" class="continue_payment" enctype="multipart/form-data" action="site/cart/cartcheckout">
				<dd class="cart-item-list"><ul>';
				$cartAmt = 0; $cartShippingAmt = 0; $cartTaxAmt = 0;$s=0;$ship_need = 0;
			//echo("<pre>");print_r($cartVal->result());die;
				foreach ($cartVal->result() as $CartRow){
					if ($CartRow->product_type=='physical') $ship_need++;
					$attri = array_filter(explode(',', $CartRow->attribute_values));
					$curdate = date('Y-m-d');
					$newImg = @explode(',',$CartRow->image);
					$CartValue.='<li id="cartdivId_'.$s.'"><img style="width:70px;" src="'.PRODUCTPATH.$newImg[0].'" alt="'.$CartRow->product_name.'">
						<span class="img-title">
						<a style="width:70px;" href="things/'.$CartRow->prdid.'/'.$CartRow->seourl.'">'.$CartRow->product_name.'</a>';
					foreach($attri as $_attributes){
				$Query = "select b.attr_name as attr_type,a.attr_name from ".SUBPRODUCT." as a left join ".PRODUCT_ATTRIBUTE." as b on a.attr_id = b.id where a.pid = '".$_attributes."'";
				$attributes = $this->ExecuteQuery($Query); 
				if($attributes->num_rows()>0){
					$CartValue.='<br>'.$attributes->row()->attr_type.' / '.$attributes->row()->attr_name.'';
				}
			}
			
			if ($CartRow->product_type=='physical'){
				$CartValue.='<br></span>
					<span class="suges"><b class="dolar" id="IndTotalVal'.$s.'">'.$this->data['currencySymbol'].$CartRow->indtotal.'</b>
						<select class="count" data-mqty="'.$CartRow->pqty.'" onchange="javascript:update_cart('.$CartRow->id.','.$s.','.$CartRow->product_id.',this);">';
					for($i = 1; $i <= $CartRow->pqty; $i++){
						if($CartRow->quantity == $i){
							$CartValue.='<option value="'.$i.'" selected>'.$i.'</option>';
						}else{
							$CartValue.='<option value="'.$i.'">'.$i.'</option>';
						}
					}		
				$CartValue.='</select>';}else{
					$CartValue.='		<span class="suges"><b class="dolar" id="IndTotalVal'.$s.'">'.$this->data['currencySymbol'].$CartRow->indtotal.'</b><td style="float:left;width:70px;" class="quantity"><span style="line-height: 2.5;">'.$CartRow->quantity.'</span></td>';
				}
						$CartValue.='<a class="remove-prod" style="display: inline-block;
border: 1px solid #c3c3c3;
padding: 0 1em;
border-radius: 0.15em;
color: #4c4f53;
font-weight: bold;
background: #f0f0f0;
background: -webkit-linear-gradient(top, #fcfcfc,#f0f0f0);
background: -ms-linear-gradient(top, #fafafa,#f0f0f0);
background: -moz-linear-gradient(top, #fafafa,#f0f0f0);
background: -o-linear-gradient(top, #fafafa,#f0f0f0);
filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#fafafa\',endColorstr=\'#f0f0f0\');
box-shadow: 0 1px 1px rgba(0,0,0,0.1);
line-height: 1.85em;
font-size: 0.94em;
text-shadow: 0 1px 0 #fff;
border-color: #c3c3c3 #bebebe #b6b6b6;
position: absolute;
right: 0px;
bottom: 5px;
z-index:3;" href="javascript:void(0);" onclick="javascript:delete_cart('.$CartRow->id.','.$s.')" class="remove_item">'.$product_remove.'</a>
				</span>
				<span class="trick"></span>
				';
				$cartAmt = $cartAmt + (($CartRow->product_shipping_cost + $CartRow->price + ($CartRow->price * 0.01 * $CartRow->product_tax_cost))  * $CartRow->quantity);
				$cartShippingAmt = $cartShippingAmt + ($CartRow->product_shipping_cost * $CartRow->quantity);
				$cartTaxAmt = $cartTaxAmt + ($CartRow->product_tax_cost * $CartRow->quantity);
				$cartQty = $cartQty + $CartRow->quantity;
				$s++;
				
				
			}
			
		
			$cartSAmt = $MainShipCost;
			$cartTAmt = ($cartAmt * 0.01 * $MainTaxCost);
			$grantAmt = $cartAmt + $cartSAmt + $cartTAmt ;
	

			$CartValue.='</ul></dd></dl><dl class="profile-account"><dd>
					<p>
				<label class="label" >'.$note_to.' '.$this->config->item('email_title').' '.$merchant.'</label>
				<textarea style="height:4.36em;"  maxlength="1000" class="note-to-seller" name="note" placeholder="'.$note_here.'"></textarea>
				</p></dd></dl>';
			if ($ship_need>0){
						$CartValue.='	<dl class="profile-account" style="position:relative;"><dd><label class="label">'.$ship_to.'</label>
				<a style="cursor: pointer;
background: #ccc;
padding: 5px;
display: inline-block;
border: 1px solid #c3c3c3;
padding: 0 1em;
border-radius: 0.15em;
color: #4c4f53;
font-weight: bold;
background: #f0f0f0;
background: -webkit-linear-gradient(top, #fcfcfc,#f0f0f0);
background: -ms-linear-gradient(top, #fafafa,#f0f0f0);
background: -moz-linear-gradient(top, #fafafa,#f0f0f0);
background: -o-linear-gradient(top, #fafafa,#f0f0f0);
filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#fafafa\',endColorstr=\'#f0f0f0\');
box-shadow: 0 1px 1px rgba(0,0,0,0.1);
line-height: 1.85em;
font-size: 0.94em;
text-shadow: 0 1px 0 #fff;
border-color: #c3c3c3 #bebebe #b6b6b6;
position: absolute;
right: 20px;
top: 30px;" class="adres-button add-shipping-addr" href="javascript:void(0);" >'.$addnew_shipping_address.'</a>
				<select data-type="cart" onchange="CartChangeAddress(this.value,this);">
					<option value="" id="address-select">'.$choose_shipping_address.'</option>
			';
			foreach ($shipVal->result() as $Shiprow){		
				if($Shiprow->primary == 'Yes'){ $optionsValues = 'selected="selected"'; $ChooseVal = $Shiprow->full_name.'<br>'.$Shiprow->address1.'<br>'.$Shiprow->city.' '.$Shiprow->state.' '.$Shiprow->postal_code.'<br>'.$Shiprow->country.'<br>'.$Shiprow->phone; $ship_id =$Shiprow->id;  }else{ $optionsValues ='';}
				$CartValue.='<option '.$optionsValues.' value="'.$Shiprow->id.'" l1="'.$Shiprow->full_name.'" l2="'.$Shiprow->address1.'" l3="'.$Shiprow->city.' '.$Shiprow->state.' '.$Shiprow->postal_code.'" l4="'.$Shiprow->country.'" l5="'.$Shiprow->phone.'">'.$Shiprow->full_name.'</option>';
			}
			$CartValue.='</select>
			<input type="hidden" name="Ship_address_val" id="Ship_address_val_cart" value="'.$ship_id.'" />
			
			<p class="default_addr"><span id="Chg_Add_Val">'.$ChooseVal.'</span></p>
				<span class="address-table"> </span>
				<span class="deleta-address" style="cursor: pointer;
background: #ccc;
padding: 5px;
display: inline-block;
border: 1px solid #c3c3c3;
padding: 0 1em;
border-radius: 0.15em;
color: #4c4f53;
font-weight: bold;
background: #f0f0f0;
background: -webkit-linear-gradient(top, #fcfcfc,#f0f0f0);
background: -ms-linear-gradient(top, #fafafa,#f0f0f0);
background: -moz-linear-gradient(top, #fafafa,#f0f0f0);
background: -o-linear-gradient(top, #fafafa,#f0f0f0);
filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#fafafa\',endColorstr=\'#f0f0f0\');
box-shadow: 0 1px 1px rgba(0,0,0,0.1);
line-height: 1.85em;
font-size: 0.94em;
text-shadow: 0 1px 0 #fff;
border-color: #c3c3c3 #bebebe #b6b6b6;
position: absolute;
right: 20px;
bottom: 10px;" data-type="cart" onclick="shipping_cart_address_delete(this);">'.$delete_this_address.'</span>
				</p></dd></dl>';
			}
			
	
		$CartValue.='<dl class="profile-account"><dd><p style="position:relative;">
        <label class="label">Payment Option</label>
        <ul class="payment-method">
        	<li>
				<input id="" class="" type="radio" checked="checked" value="creditcard" name="payment_type">
				<label for="">
				<i class="camr"></i>
				</label>
			</li>
			 <li>
				<input id="" class="" type="radio" value="stripe" name="payment_type">
				<label for="">
				 <b>Stripe Payment</b>
				</label>
			</li>
        	<li>
				<input id="" type="radio" value="paypal" name="payment_type">
				<label for="">
				<b>PayPal</b>
				</label>
			</li>
				<li>
				<input id="" type="radio" value="paypal_adaptive" name="payment_type">
				<label for="">
				<b>PayPal(Adaptive)</b>
				</label>
			</li>
				
				<li>
				<input id="" type="radio" value="paypal_twocheckout" name="payment_type">
				<label for="">
				<b>Twocheckout</b>
				</label>
			</li>
			<li>
				<input id="" type="radio" value="paypal_cashondelivery" name="payment_type">
				<label for="">
				<b>Cash on Delivery</b>
				</label>
			</li>
        
        </ul>
        </p></dd></dl>';


if($disAmt>0){
				$CartValue.='<dl class="cart-coupon">
				<dt>'.$cart_coupon_code.'</dt>
                <dd><input id="is_coupon" name="is_coupon" class="text coupon-code" readonly="readonly" placeholder="'.$have_coupon_code.'" data-sid="616001" type="text" value="'.$discountQuery->row()->couponCode.'">
				<input id="CheckCodeButton" type="button" class="btn-blue-apply apply-coupon" onclick="javascript:checkRemove();" value="Remove" style="cursor:pointer;padding:4px;" /></dd>
				<dd><span id="CouponErr" style="color:#FF0000;"></span></dd>
                
			</dl>';
			}else{
				$CartValue.='<dl class="cart-coupon"><p style="position:relative;">
				<label class="label coupon-code">'.$coupon_codes.'</label>
                <ul class="payment-method">
        <li><input id="is_coupon" name="is_coupon" class="text coupon-code" placeholder="'.$have_coupon_code.'" data-sid="616001" type="text">
        <input id="CheckCodeButton" type="button" class="btn-blue-apply apply-coupon" onclick="javascript:checkCode();" value="'.$apply.'" style="cursor:pointer;padding:4px;" />
				</li></ul>
				<dd><span id="CouponErr" style="color:#FF0000;"></span></dd>
                
			</dl>';
			}



			$CartValue.='<dl class="cart-payment-order"><dd><p style="position:relative;">
		      <label class="label" style="display: block;
font-size: 1.1em;
font-weight: bold;
padding-bottom: 0.5em;
margin-top: -0.2em;">'.$checkout_order.'</label>
		      <dd>
			<ul style="float: left;width: 95%;">
			  <li class="first" style="clear: both;line-height: 1.65em;text-align: right;color: #383d48;">
			    <span class="order-payment-type" style="float: left;text-align: left;color: #8a8f9c;">'.$checkout_item_total.'</span>
			    <b>'.$this->data['currencySymbol'].'<span id="CartAmt">'.number_format($cartAmt,2,'.','').'</span></b> '.$this->data['currencyType'].'
			  </li>
			  <li style="clear: both;line-height: 1.65em;text-align: right;color: #383d48;">
			    <span class="order-payment-type" style="float: left;text-align: left;color: #8a8f9c;">'.$referrals_shipping.'</span>
			    <b>'.$this->data['currencySymbol'].'<span id="CartSAmt">'.number_format($cartSAmt,2,'.','').'</span></b> '.$this->data['currencyType'].'
			  </li>
			  <li style="clear: both;line-height: 1.65em;text-align: right;color: #383d48;">
			    <span class="order-payment-type" style="float: left;text-align: left;color: #8a8f9c;">'.$checkout_tax.' (<span id="carTamt">'.$MainTaxCost.'</span>%) of '.$this->data['currencySymbol'].'<span id="CartAmtDup">'.$cartAmt.'</span></span>
			    <b>'.$this->data['currencySymbol'].'<span id="CartTAmt">'.number_format($cartTAmt,2,'.','').'</span></b> '.$this->data['currencyType'].'
			  </li></ul>';
			if($disAmt >0){
				$grantAmt = $grantAmt - $disAmt;
				$CartValue.='<div id="disAmtValDiv" style="float: left;width: 95%;"><ul><li style="clear: both;line-height: 1.65em;text-align: right;color: #383d48;">
			    <span class="order-payment-type" style="float: left;text-align: left;color: #8a8f9c;">Discount</span>
			    <b>'.$this->data['currencySymbol'].'<span id="disAmtVal">'.number_format($disAmt,2,'.','').'</span></b> '.$this->data['currencyType'].'
			  </li><ul></div>';
			}else{
			 $CartValue.='<div id="disAmtValDiv" style="display:none;float: left;width: 95%;"><ul>
			 <li style="clear: both;line-height: 1.65em;text-align: right;color: #383d48;">
			    <span class="order-payment-type" style="float: left;text-align: left;color: #8a8f9c;">Discount</span>
			    <b>'.$this->data['currencySymbol'].'<span id="disAmtVal">'.number_format($disAmt,2,'.','').'</span></b> '.$this->data['currencyType'].'
			  </li>
			  </ul></div>';
			}
			$CartValue.='<ul style="float: left;width: 95%;">
			 <li class="total" style="clear: both;line-height: 1.65em;text-align: right;color: #383d48;">
			    <span class="order-payment-type" style="float: left;text-align: left;color: #8a8f9c;"><b>Total</b></span>
			    <b>'.$this->data['currencySymbol'].'<span id="CartGAmt">'.number_format($grantAmt,2,'.','').'</span></b> '.$this->data['currencyType'].'
			  </li>
			</ul>
		      </dd>
	              
		    </dl>
			
		    <input name="user_id" value="'.$userid.'" type="hidden">
			<input name="cart_amount" id="cart_amount" value="'.number_format($cartAmt,2,'.','').'" type="hidden">
			<input name="cart_ship_amount" id="cart_ship_amount" value="'.number_format($cartSAmt,2,'.','').'" type="hidden">
			<input name="cart_tax_amount" id="cart_tax_amount" value="'.number_format($cartTAmt,2,'.','').'" type="hidden">
			<input name="cart_tax_Value" id="cart_tax_Value" value="'.number_format($MainTaxCost,2,'.','').'" type="hidden">
			<input name="cart_total_amount" id="cart_total_amount" value="'.number_format($grantAmt,2,'.','').'" type="hidden">
			<input name="discount_Amt" id="discount_Amt" value="'.number_format($disAmt,2,'.','').'" type="hidden">
			<input type="hidden" name="Ship_address_val" id="Ship_address_val" value="'.$ship_id.'" />
			<input name="ship_need" id="ship_need" value="'.$ship_need.'" type="hidden">';

			if($disAmt>0){
				$CartValue.='<input name="CouponCode" id="CouponCode" value="'.$discountQuery->row()->couponCode.'" type="hidden">
			<input name="Coupon_id" id="Coupon_id" value="'.$discountQuery->row()->couponID.'" type="hidden">
			<input name="couponType" id="couponType" value="'.$discountQuery->row()->coupontype.'" type="hidden">';
			}else{
				$CartValue.='<input name="CouponCode" id="CouponCode" value="" type="hidden">
			<input name="Coupon_id" id="Coupon_id" value="0" type="hidden">
			<input name="couponType" id="couponType" value="" type="hidden">';
			}
			$CartValue.='
			
			<dl class="profile-account">
        <dd style=" padding: 1em;">
       <button style="width:100%;background-color: #569D37;" type="submit" class="btn-green btn-payment" id="button-submit-merchant">'.$continue_payment.'</button>
         </dd>
        </dl>
		    
		  </div>
		</div>
	</form></div></div>';
		}

		$countVal = $giftRes -> num_rows() + $cartQty + $SubcribRes -> num_rows();



		if($countVal >0 ){
			$CartDisp = ''.$GiftValue.$SubscribValue.$CartValue.'<div id="EmptyCart" style="border-bottom: none; display:none;background-color: #fff;padding: 10px;border-radius: 5px;" class="empty-alert" >
					<p style="text-align:center;"><img src="images/site/shopping_empty.jpg" alt="Shopping Cart Empty"></p>
					<p style="text-align:center;"><b>'.$shopping_cart_empty.'</b></p>
					<p style="text-align:center;">'.$dont_miss.' '.ucwords($this->config->item('email_title')).'. '.$shall_we.'</p>
				</div>';
		}else{

			$CartDisp = '<div style="background-color: #fff;padding: 10px;border-radius: 5px;"><h2>Shopping Cart</h2>
					<div style="border-bottom: none;" class="empty-alert" >
					<p style="text-align:center;"><img src="images/site/shopping_empty.jpg" alt="Shopping Cart Empty"></p>
					<p style="text-align:center;"><b>'.$shopping_cart_empty.'</b></p>
					<p style="text-align:center;">'.$dont_miss.' '.ucwords($this->config->item('email_title')).'. '.$shall_we.'</p>
				</div></div>';
		}

		return $CartDisp;

	}

	
	
	public function auction_mani_cart_view($userid=''){

		$MainShipCost = 0;
		$MainTaxCost = 0; $cartQty = 0;


		$shipVal = $this->cart_model->get_all_details(SHIPPING_ADDRESS,array( 'user_id' => $userid));

		if($shipVal -> num_rows() >0 ){

			$shipValID = $this->cart_model->get_all_details(SHIPPING_ADDRESS,array( 'user_id' => $userid, 'primary' => 'Yes'));
			$dataArr = array('shipping_id' => $shipValID->row()->id);
			$condition = array('user_id' => $userid);
			$this->cart_model->update_details(FANCYYBOX_TEMP,$dataArr,$condition);
			$ShipCostVal = $this->cart_model->get_all_details(COUNTRY_LIST,array( 'country_code' => $shipValID->row()->country));

			$MainShipCost = $ShipCostVal->row()->shipping_cost;
			$MainTaxCost = $ShipCostVal->row()->shipping_tax;
			$dataArr2 = array('shipping_cost' => $MainShipCost, 'tax' => $MainTaxCost);
			$this->cart_model->update_details(SHOPPING_CART,$dataArr2,$condition);
		}

		$GiftValue = ''; $CartValue = ''; $SubscribValue = '';

		$giftSet = $this->cart_model->get_all_details(GIFTCARDS_SETTINGS,array( 'id' => '1'));
		$giftRes = $this->cart_model->get_all_details(GIFTCARDS_TEMP,array( 'user_id' => $userid));

		$SubcribRes = $this->minicart_model->get_all_details(FANCYYBOX_TEMP,array( 'user_id' => $userid));

		$this->db->select('a.*,b.product_name,b.quantity as mqty,b.seourl,b.quantity as pqty,b.image,b.id as prdid,b.price as orgprice,b.ship_immediate,c.attr_name as attr_type,d.attr_name');
		$this->db->from(SHOPPING_CART.' as a');
		$this->db->join(PRODUCT.' as b' , 'b.id = a.product_id');
		$this->db->join(SUBPRODUCT.' as d' , 'd.pid = a.attribute_values','left');		
		$this->db->join(PRODUCT_ATTRIBUTE.' as c' , 'c.id = d.attr_id','left');		
		$this->db->where('a.user_id = '.$userid);
		$this->db->where('a.type = "auction" ');
		$cartVal = $this->db->get();
		
		$this->db->select('discountAmount,couponID,couponCode,coupontype');
		$this->db->from(SHOPPING_CART);
		$this->db->where('user_id = '.$userid);
		$discountQuery = $this->db->get();
		
		$disAmt = $discountQuery->row()->discountAmount;

		/**Lanuage Transalation**/
			
		if($this->lang->line('cart_product') != '')
		$cart_product =  stripslashes($this->lang->line('cart_product'));
		else
		$cart_product =  "Product";

		if($this->lang->line('giftcard_price') != '')
		$giftcard_price =  stripslashes($this->lang->line('giftcard_price'));
		else
		$giftcard_price =  "Price";
			
		if($this->lang->line('product_quantity') != '')
		$product_quantity =  stripslashes($this->lang->line('product_quantity'));
		else
		$product_quantity =  "Quantity";


		if($this->lang->line('purchases_total') != '')
		$purchases_total =  stripslashes($this->lang->line('purchases_total'));
		else
		$purchases_total =  "Total";
			
		if($this->lang->line('product_remove') != '')
		$product_remove =  stripslashes($this->lang->line('product_remove'));
		else
		$product_remove =  "Remove";
			
		if($this->lang->line('giftcard_reci_name') != '')
		$giftcard_reci_name =  stripslashes($this->lang->line('giftcard_reci_name'));
		else
		$giftcard_reci_name =  "Recipient name";
			
		if($this->lang->line('giftcard_rec_email') != '')
		$giftcard_rec_email =  stripslashes($this->lang->line('giftcard_rec_email'));
		else
		$giftcard_rec_email =  "Recipient e-mail";
			
		if($this->lang->line('giftcard_message') != '')
		$giftcard_message =  stripslashes($this->lang->line('giftcard_message'));
		else
		$giftcard_message =  "Message";
			
		if($this->lang->line('choose_shipping_address') != '')
		$choose_shipping_address =  stripslashes($this->lang->line('choose_shipping_address'));
		else
		$choose_shipping_address =  "Choose Your Shipping Address";

		if($this->lang->line('delete_this_address') != '')
		$delete_this_address =  stripslashes($this->lang->line('delete_this_address'));
		else
		$delete_this_address =  "Delete this address";

		if($this->lang->line('addnew_shipping_address') != '')
		$addnew_shipping_address =  stripslashes($this->lang->line('addnew_shipping_address'));
		else
		$addnew_shipping_address =  "Add new shipping address";
			
		if($this->lang->line('continue_payment') != '')
		$continue_payment =  stripslashes($this->lang->line('continue_payment'));
		else
		$continue_payment =  "Continue to Payment";
			
		if($this->lang->line('checkout_item_total') != '')
		$checkout_item_total =  stripslashes($this->lang->line('checkout_item_total'));
		else
		$checkout_item_total =  "Item total";
			
		if($this->lang->line('update') != '')
		$update =  stripslashes($this->lang->line('update'));
		else
		$update =  "Update";
			
		if($this->lang->line('referrals_shipping') != '')
		$referrals_shipping =  stripslashes($this->lang->line('referrals_shipping'));
		else
		$referrals_shipping =  "Shipping";

		if($this->lang->line('shipping_speed') != '')
		$shipping_speed =  stripslashes($this->lang->line('shipping_speed'));
		else
		$shipping_speed =  "Shipping Speed";
			
		if($this->lang->line('items_in_shopping') != '')
		$items_in_shopping =  stripslashes($this->lang->line('items_in_shopping'));
		else
		$items_in_shopping =  "items in shopping cart";
			
		if($this->lang->line('order_from') != '')
		$order_from =  stripslashes($this->lang->line('order_from'));
		else
		$order_from =  "Order from";
			
		if($this->lang->line('merchant') != '')
		$merchant =  stripslashes($this->lang->line('merchant'));
		else
		$merchant =  "Merchant";
			
		if($this->lang->line('header_ship_within') != '')
		$header_ship_within =  stripslashes($this->lang->line('header_ship_within'));
		else
		$header_ship_within =  "Ships within 1-3 business days";

		if($this->lang->line('shopping_cart_empty') != '')
		$shopping_cart_empty =  stripslashes($this->lang->line('shopping_cart_empty'));
		else
		$shopping_cart_empty =  "Your Shopping Cart is Empty";

		if($this->lang->line('dont_miss') != '')
		$dont_miss =  stripslashes($this->lang->line('dont_miss'));
		else
		$dont_miss =  "Don`t miss out on awesome sales right here on";

		if($this->lang->line('shall_we') != '')
		$shall_we =  stripslashes($this->lang->line('shall_we'));
		else
		$shall_we =  "Let`s fill that cart, shall we?";

		if($this->lang->line('checkout_tax') != '')
		$checkout_tax =  stripslashes($this->lang->line('checkout_tax'));
		else
		$checkout_tax =  "Tax";

		if($this->lang->line('gift') != '')
		$gift =  stripslashes($this->lang->line('gift'));
		else
		$gift =  "Gift";
			
			
		if($this->lang->line('order_is_gift') != '')
		$order_is_gift =  stripslashes($this->lang->line('order_is_gift'));
		else
		$order_is_gift =  "This order is a gift";

		if($this->lang->line('coupon_codes') != '')
		$coupon_codes =  stripslashes($this->lang->line('coupon_codes'));
		else

		$coupon_codes =  "Coupon Codes";

		if($this->lang->line('checkout_order') != '')
		$checkout_order =  stripslashes($this->lang->line('checkout_order'));
		else
		$checkout_order =  "Order";

		if($this->lang->line('ship_to') != '')
		$ship_to =  stripslashes($this->lang->line('ship_to'));
		else
		$ship_to =  "Ship to";

		
		if($this->lang->line('apply') != '')
		$apply =  stripslashes($this->lang->line('apply'));
		else
		$apply =  "Apply";
			
		if($this->lang->line('optional') != '')
		$optional =  stripslashes($this->lang->line('optional'));
		else
		$optional =  "Optional";
			
		if($this->lang->line('note_here') != '')
		$note_here =  stripslashes($this->lang->line('note_here'));
		else
		$note_here =  "You can leave a personalized note here";

		if($this->lang->line('have_coupon_code') != '')
		$have_coupon_code =  stripslashes($this->lang->line('have_coupon_code'));
		else
		$have_coupon_code =  "Have a coupon code?";
			
		if($this->lang->line('retail_price') != '')
		$retail_price =  stripslashes($this->lang->line('retail_price'));
		else
		$retail_price =  "Retail price";

		/**Lanuage Transalation**/


		$resultCart = $cartVal->result_array();
		/* echo "<pre>"; print_r($resultCart);
		die; */
		/****************************** Gift Card Displays **************************************/

		if($giftRes -> num_rows() > 0 ){

			$GiftValue.= '<div id="GiftCartTable" style="display:block;background: none repeat scroll 0 0 #FFFFFF;margin-bottom:10px;border: 1px solid #BCBFC3;border-radius: 0.2em;box-shadow: 0 0 0.3em rgba(202, 203, 209, 0.2), 0 1px 0.3em rgba(0, 0, 0, 0.1);font-size: 1em;">
			<dt style="font-weight:normal" class="tit-profile"><b>'.$this->config->item('email_title').' Gift Cards</b>
				<button class="cancel-search" style="display: inline-block;border: 1px solid #c3c3c3;padding: 0 1em;border-radius: 0.15em;color: #4c4f53;font-weight: bold;background: #f0f0f0;background: -webkit-linear-gradient(top, #fcfcfc,#f0f0f0);background: -ms-linear-gradient(top, #fafafa,#f0f0f0);background: -moz-linear-gradient(top, #fafafa,#f0f0f0);background: -o-linear-gradient(top, #fafafa,#f0f0f0);filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#fafafa\',endColorstr=\'#f0f0f0\');box-shadow: 0 1px 1px rgba(0,0,0,0.1);line-height: 1.85em;font-size: 0.94em;text-shadow: 0 1px 0 #fff;border-color: #c3c3c3 #bebebe #b6b6b6;position: absolute;right: 5px;top:7px;" type="button">Cancel</button>
				<button class="grey-search" type="button"><i class="delete-ico" style="height:1.4em;"></i></button></dt>
			<form method="post" name="giftSubmit" id="giftSubmit" class="continue_payment" enctype="multipart/form-data" action="site/cart/giftcheckout">
				<div class="cart-payment-wrap cart-note"><span class="cart-payment-top"><b></b></span><div class="table-cart-wrap">';	
			$giftAmt = 0; $g=0;

			foreach ($giftRes->result() as $giftRow){
			$GiftValue.= '<div id="giftId_'.$g.'" style="display:block;">';
			$GiftValue.='<dl class="profile-account"><dd class="cart-item-list"><ul>';
			$GiftValue.='<li style="position:relative;"><img style="width:70px;" src="'.GIFTPATH.$giftSet->row()->image.'" alt="'.$giftSet->row()->title.'"><br>
			<span class="option-tit">'.$giftcard_reci_name.':</span><span class="option-txt">'.$giftRow->recipient_name.'</span><br>
			<span class="option-tit">'.$giftcard_rec_email.':</span><span class="option-txt">'.$giftRow->recipient_mail.'</span><br>
			<span class="option-tit">'.$giftcard_message.':</span><span class="option-txt">'.$giftRow->description.'</span>
			<span style="float: right;position: absolute;right: 0;top: 20px;font-weight: bold;">'.$this->data['currencySymbol'].$giftRow->price_value.'</span>
			<a href="javascript:delete_gift('.$giftRow->id.','.$g.')" class="remove_gift_card" style="display: none;
				border: 1px solid #c3c3c3;
				padding: 0 1em;
				border-radius: 0.15em;
				color: #4c4f53;
				font-weight: bold;
				background: #f0f0f0;
				background: -webkit-linear-gradient(top, #fcfcfc,#f0f0f0);
				background: -ms-linear-gradient(top, #fafafa,#f0f0f0);
				background: -moz-linear-gradient(top, #fafafa,#f0f0f0);
				background: -o-linear-gradient(top, #fafafa,#f0f0f0);
				filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#fafafa\',endColorstr=\'#f0f0f0\');
				box-shadow: 0 1px 1px rgba(0,0,0,0.1);
				line-height: 1.85em;
				font-size: 0.94em;
				text-shadow: 0 1px 0 #fff;
				border-color: #c3c3c3 #bebebe #b6b6b6;
				position: absolute;
				right: 0px;
				bottom: 5px;
				z-index:3;" cid="66577">'.$product_remove.'</a>
				<span class="trick"></span>
			</li>
			';
			 $GiftValue.='</ul></dd></dl></div>';
				$giftAmt = $giftAmt + $giftRow->price_value;
				$g++;
			}
			
  			$GiftValue.='<dl class="profile-account"><dd><p style="position:relative;">
        <label class="label">Payment Option</label>
        <ul class="payment-method">
        	<li>
				<input id="" class="" type="radio" checked="checked" value="creditcard" name="payment_type">
				<label for="">
				<i class="camr"></i>
				</label>
			</li>
        	<li>
				<input id="" type="radio" value="paypal" name="payment_type">
				<label for="">
				<b>PayPal</b>
				</label>
			</li>
  					<li>
				<input id="" type="radio" value="paypal_adaptive" name="payment_type">
				<label for="">
				<b>PayPal(Adaptive)</b>
				</label>
			</li>
        
  					<li>
				<input id="" type="radio" value="paypal_twocheckout" name="payment_type">
				<label for="">
				<b>Twocheckout</b>
				</label>
			</li>
			
			<li>
				<input id="" type="radio" value="paypal_cashondelivery" name="payment_type">
				<label for="">
				<b>Cash on Delivery</b>
				</label>
			</li>
        </ul>
        </p></dd></dl>';
			
			$GiftValue.= '</div>
			<input name="gift_cards_amount" id-"gift_cards_amount" value="'.number_format($giftAmt,2,'.','').'" type="hidden">
			<input name="checkout_type" id-"checkout_type" value="giftpurchase" type="hidden">
			<div class="cart-payment" id="giftcard-cart-payment"><input type="hidden"><span class="bg-cart-payment"></span>
			<!--		    <dl class="cart-payment-order">
			   	<dt>'.$checkout_order.'</dt><dd>
			   	<ul>
				<li class="first"><span class="order-payment-type">'.$checkout_item_total.'</span><span class="order-payment-usd"><b>'.$this->data['currencySymbol'].'<span id="item_total">'.number_format($giftAmt,2,'.','').'</span></b> '.$this->data['currencyType'].'</span></li>
				<li class="total"><span class="order-payment-type"><b>Total</b></span><span class="order-payment-usd"><b>'.$this->data['currencySymbol'].'<span id="total_item">'.number_format($giftAmt,2,'.','').'</span></b> '.$this->data['currencyType'].'</span></li>
				</ul>
		      	</dd>
			</dl>
			-->			<dl class="profile-account">
			 <dd style=" padding: 1em;">
		    <button type="submit" style="width:100%;background-color: #569D37;" class="btn btn-green" id="button-submit-giftcard">'.$continue_payment.'</button>
			</dd>
        </dl>
		  	</div></div></form></div>';
			
			
        
       
      
         
			
			
		}

		/****************************** Subscribe Card Displays **************************************/
		if($SubcribRes -> num_rows() > 0 ){
			$SubscribValue.= '<div id="SubscribeCartTable" style="display:block;background: none repeat scroll 0 0 #FFFFFF;margin-bottom:10px;border: 1px solid #BCBFC3;border-radius: 0.2em;box-shadow: 0 0 0.3em rgba(202, 203, 209, 0.2), 0 1px 0.3em rgba(0, 0, 0, 0.1);font-size: 1em;">
			<dt style="font-weight:normal" class="tit-profile">Oliver Subscriptions</b>
				<button class="cancel-search" style="display: inline-block;border: 1px solid #c3c3c3;padding: 0 1em;border-radius: 0.15em;color: #4c4f53;font-weight: bold;background: #f0f0f0;background: -webkit-linear-gradient(top, #fcfcfc,#f0f0f0);background: -ms-linear-gradient(top, #fafafa,#f0f0f0);background: -moz-linear-gradient(top, #fafafa,#f0f0f0);background: -o-linear-gradient(top, #fafafa,#f0f0f0);filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#fafafa\',endColorstr=\'#f0f0f0\');box-shadow: 0 1px 1px rgba(0,0,0,0.1);line-height: 1.85em;font-size: 0.94em;text-shadow: 0 1px 0 #fff;border-color: #c3c3c3 #bebebe #b6b6b6;position: absolute;right: 5px;top:7px;" type="button">Cancel</button>
				<button class="grey-search" type="button"><i class="delete-ico" style="height:1.4em;"></i></button></dt>
			<form method="post" name="SubscribeSubmit" id="SubscribeSubmit" class="continue_payment" enctype="multipart/form-data" action="site/cart/Subscribecheckout">
				<div class="cart-payment-wrap cart-note"><span class="cart-payment-top"><b></b></span><div class="table-cart-wrap">
				';	
			$SubscribAmt = 0; $subcribSAmt = 0; $subcribTAmt = 0; $SubgrantAmt = 0; $s=0;

			foreach ($SubcribRes->result() as $SubcribRow){
				$SubscribValue.= '<div id="SubscribId_'.$s.'" style="display:block;">';
				
				 $SubscribValue.='<dl class="profile-account"><dd class="cart-item-list"><ul>
					<li><img style="width:70px;" src="'.FANCYBOXPATH.$SubcribRow->image.'" alt="'.$SubcribRow->name.'">'.$SubcribRow->name.'
					<span style="position: absolute;right: 0;top: 20px;font-weight: bold;">'.$this->data['currencySymbol'].number_format($SubcribRow->price,2,'.','').'</span>
					<a style="display: none;
						border: 1px solid #c3c3c3;
						padding: 0 1em;
						border-radius: 0.15em;
						color: #4c4f53;
						font-weight: bold;
						background: #f0f0f0;
						background: -webkit-linear-gradient(top, #fcfcfc,#f0f0f0);
						background: -ms-linear-gradient(top, #fafafa,#f0f0f0);
						background: -moz-linear-gradient(top, #fafafa,#f0f0f0);
						background: -o-linear-gradient(top, #fafafa,#f0f0f0);
						filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#fafafa\',endColorstr=\'#f0f0f0\');
						box-shadow: 0 1px 1px rgba(0,0,0,0.1);
						line-height: 1.85em;
						font-size: 0.94em;
						text-shadow: 0 1px 0 #fff;
						border-color: #c3c3c3 #bebebe #b6b6b6;
						position: absolute;
						right: 0px;
						bottom: 5px;
						z-index:3;" href="javascript:delete_subscribe('.$SubcribRow->id.','.$s.')" class="remove_gift_card" cid="66577">'.$product_remove.'</a>
						<span class="trick"></span>
						</li>
											</div>';
										$SubscribAmt = $SubscribAmt + $SubcribRow->price;
										$s++;
									}
									$subcribSAmt = $MainShipCost;
									$subcribTAmt = ($SubscribAmt * 0.01 * $MainTaxCost);
									$SubgrantAmt = $SubscribAmt + $subcribSAmt + $subcribTAmt ;





									


						$SubscribValue.= '<dl class="profile-account" style="position:relative;"><dd><label class="label">'.$ship_to.'</label>
										<a style="cursor: pointer;
						background: #ccc;
						padding: 5px;
						display: inline-block;
						border: 1px solid #c3c3c3;
						padding: 0 1em;
						border-radius: 0.15em;
						color: #4c4f53;
						font-weight: bold;
						background: #f0f0f0;
						background: -webkit-linear-gradient(top, #fcfcfc,#f0f0f0);
						background: -ms-linear-gradient(top, #fafafa,#f0f0f0);
						background: -moz-linear-gradient(top, #fafafa,#f0f0f0);
						background: -o-linear-gradient(top, #fafafa,#f0f0f0);
						filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#fafafa\',endColorstr=\'#f0f0f0\');
						box-shadow: 0 1px 1px rgba(0,0,0,0.1);
						line-height: 1.85em;
						font-size: 0.94em;
						text-shadow: 0 1px 0 #fff;
						border-color: #c3c3c3 #bebebe #b6b6b6;
						position: absolute;
						right: 20px;
						top: 30px;" class="adres-button add-shipping-addr" href="javascript:void(0);" >'.$addnew_shipping_address.'</a>
										<select data-type="subscribe" onchange="SubscribeChangeAddress(this.value,this);">
											<option value="" id="address-select">'.$choose_shipping_address.'</option>
									';



						//old

			/*$SubscribValue.= '</div>
			 <div class="cart-payment" id="merchant-cart-payment">
		    <input type="hidden">
		    <span class="bg-cart-payment"></span>
		    <dl class="cart-payment-ship">
		      <dt>'.$ship_to.'</dt>
		      <dd>
			<select id="address-cart" class="select-round select-shipping-addr" onchange="SubscribeChangeAddress(this.value);">
				  <option value="" id="address-select">'.$choose_shipping_address.'</option>
			';*/

			foreach ($shipVal->result() as $Shiprow){
				if($Shiprow->primary == 'Yes'){ $optionsValues = 'selected="selected"';
				$ChooseVal = $Shiprow->full_name.'<br>'.$Shiprow->address1.'<br>'.$Shiprow->city.' '.$Shiprow->state.' '.$Shiprow->postal_code.'<br>'.$Shiprow->country.'<br>'.$Shiprow->phone; $ship_id =$Shiprow->id;  }else{ $optionsValues ='';}
				$SubscribValue.='<option '.$optionsValues.' value="'.$Shiprow->id.'" l1="'.$Shiprow->full_name.'" l2="'.$Shiprow->address1.'" l3="'.$Shiprow->city.' '.$Shiprow->state.' '.$Shiprow->postal_code.'" l4="'.$Shiprow->country.'" l5="'.$Shiprow->phone.'">'.$Shiprow->full_name.'</option>';
			}

			$SubscribValue.='</select>
			<input type="hidden" name="SubShip_address_val" id="SubShip_address_val_subscribe" value="'.$ship_id.'" />
			<p class="default_addr"><span id="SubChg_Add_Val">'.$ChooseVal.'</span></p>
				<span class="address-table"> </span>
				<span class="deleta-address" style="cursor: pointer;
background: #ccc;
padding: 5px;
display: inline-block;
border: 1px solid #c3c3c3;
padding: 0 1em;
border-radius: 0.15em;
color: #4c4f53;
font-weight: bold;
background: #f0f0f0;
background: -webkit-linear-gradient(top, #fcfcfc,#f0f0f0);
background: -ms-linear-gradient(top, #fafafa,#f0f0f0);
background: -moz-linear-gradient(top, #fafafa,#f0f0f0);
background: -o-linear-gradient(top, #fafafa,#f0f0f0);
filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#fafafa\',endColorstr=\'#f0f0f0\');
box-shadow: 0 1px 1px rgba(0,0,0,0.1);
line-height: 1.85em;
font-size: 0.94em;
text-shadow: 0 1px 0 #fff;
border-color: #c3c3c3 #bebebe #b6b6b6;
position: absolute;
right: 20px;
bottom: 10px;
z-index:3;" data-type="subscribe" onclick="subshipping_cart_address_delete(this);">'.$delete_this_address.'</span>
<span class="trick"></span>
				</p></dd></dl>


			

<!--			   <dl class="cart-payment-order">
		      <dt>'.$checkout_order.'</dt>
		      <dd>
			<ul>
			  <li class="first">
			    <span class="order-payment-type">'.$checkout_item_total.'</span>
			    <span class="order-payment-usd"><b>'.$this->data['currencySymbol'].'<span id="SubCartAmt">'.number_format($SubscribAmt,2,'.','').'</span></b> '.$this->data['currencyType'].'</span>
			  </li>
			  <li>
			    <span class="order-payment-type">'.$referrals_shipping.'</span>
			    <span class="order-payment-usd"><b>'.$this->data['currencySymbol'].'<span id="SubCartSAmt">'.number_format($subcribSAmt,2,'.','').'</span></b> '.$this->data['currencyType'].'</span>
			  </li>
			  <li>
			    <span class="order-payment-type">'.$checkout_tax.' (<span id="SubTamt">'.$MainTaxCost.'</span>%) of '.$this->data['currencySymbol'].$SubscribAmt.'</span>
			    <span class="order-payment-usd"><b>'.$this->data['currencySymbol'].'<span id="SubCartTAmt">'.number_format($subcribTAmt,2,'.','').'</span></b> '.$this->data['currencyType'].'</span>
			  </li></ul>';

			$SubscribValue.='
		  <ul>
			 <li class="total">
			    <span class="order-payment-type"><b>Total</b></span>
			    <span class="order-payment-usd"><b>'.$this->data['currencySymbol'].'<span id="SubCartGAmt">'.number_format($SubgrantAmt,2,'.','').'</span></b> '.$this->data['currencyType'].'</span>
			  </li>
			</ul>
		      </dd>
	              
		    </dl>
-->			
		    <input name="user_id" value="'.$userid.'" type="hidden">
			<input name="subcrib_amount" id="subcrib_amount" value="'.number_format($SubscribAmt,2,'.','').'" type="hidden">
			<input name="subcrib_ship_amount" id="subcrib_ship_amount" value="'.number_format($subcribSAmt,2,'.','').'" type="hidden">
			<input name="subcrib_tax_amount" id="subcrib_tax_amount" value="'.number_format($subcribTAmt,2,'.','').'" type="hidden">
			<input name="subcrib_total_amount" id="subcrib_total_amount" value="'.number_format($SubgrantAmt,2,'.','').'" type="hidden">
			<dl class="profile-account">
        <dd style=" padding: 1em;">
		    <input type="submit" class="btn btn-green" style="background-color: #569D37;cursor:pointer;" name="SubscribePayment" id="button-submit-merchant" value="'.$continue_payment.'" />
		    </dd>
			</dl>
		  </div>
		</div>
	</form></div>';
		}


		/****************************** Cart Displays **************************************/

		if($cartVal -> num_rows() > 0 ){
			$CartValue.='<div id="cart_container" style="background: none repeat scroll 0 0 #FFFFFF;border: 1px solid #BCBFC3;border-radius: 0.2em;box-shadow: 0 0 0.3em rgba(202, 203, 209, 0.2), 0 1px 0.3em rgba(0, 0, 0, 0.1);font-size: 1em;"><dl class="profile-account"><dt style="font-weight:normal" class="tit-profile">'.$order_from.' <b>'.$this->config->item('email_title').' '.$merchant.'</b>
				<button class="cancel-search" style="display: inline-block;border: 1px solid #c3c3c3;padding: 0 1em;border-radius: 0.15em;color: #4c4f53;font-weight: bold;background: #f0f0f0;background: -webkit-linear-gradient(top, #fcfcfc,#f0f0f0);background: -ms-linear-gradient(top, #fafafa,#f0f0f0);background: -moz-linear-gradient(top, #fafafa,#f0f0f0);background: -o-linear-gradient(top, #fafafa,#f0f0f0);filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#fafafa\',endColorstr=\'#f0f0f0\');box-shadow: 0 1px 1px rgba(0,0,0,0.1);line-height: 1.85em;font-size: 0.94em;text-shadow: 0 1px 0 #fff;border-color: #c3c3c3 #bebebe #b6b6b6;position: absolute;right: 5px;top:7px;" type="button">Cancel</button>
				<button class="grey-search" type="button"><i class="delete-ico" style="height:1.4em;"></i></button></dt>
				<form method="post" name="cartSubmit" id="cartSubmit" class="continue_payment" enctype="multipart/form-data" action="site/cart/cartcheckout">
				<dd class="cart-item-list"><ul>';
				$cartAmt = 0; $cartShippingAmt = 0; $cartTaxAmt = 0;$s=0;$ship_need = 0;
			//echo("<pre>");print_r($cartVal->result());die;
				foreach ($cartVal->result() as $CartRow){
					if ($CartRow->product_type=='physical') $ship_need++;
					$attri = array_filter(explode(',', $CartRow->attribute_values));
					$curdate = date('Y-m-d');
					$newImg = @explode(',',$CartRow->image);
					$CartValue.='<li id="cartdivId_'.$s.'"><img style="width:70px;" src="'.PRODUCTPATH.$newImg[0].'" alt="'.$CartRow->product_name.'">
						<span class="img-title">
						<a style="width:70px;" href="things/'.$CartRow->prdid.'/'.$CartRow->seourl.'">'.$CartRow->product_name.'</a>';
					foreach($attri as $_attributes){
				$Query = "select b.attr_name as attr_type,a.attr_name from ".SUBPRODUCT." as a left join ".PRODUCT_ATTRIBUTE." as b on a.attr_id = b.id where a.pid = '".$_attributes."'";
				$attributes = $this->ExecuteQuery($Query); 
				if($attributes->num_rows()>0){
					$CartValue.='<br>'.$attributes->row()->attr_type.' / '.$attributes->row()->attr_name.'';
				}
			}
			
				if( $CartRow->product_type == 'physical'){
					$type = 1;
				}else if($CartRow->product_type == 'digital'){
					$type = 2;
				}
				
				$CartValue.='<a class="remove-prod" style="display: inline-block;
				border: 1px solid #c3c3c3;
				padding: 0 1em;
				border-radius: 0.15em;
				color: #4c4f53;
				font-weight: bold;
				background: #f0f0f0;
				background: -webkit-linear-gradient(top, #fcfcfc,#f0f0f0);
				background: -ms-linear-gradient(top, #fafafa,#f0f0f0);
				background: -moz-linear-gradient(top, #fafafa,#f0f0f0);
				background: -o-linear-gradient(top, #fafafa,#f0f0f0);
				filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#fafafa\',endColorstr=\'#f0f0f0\');
				box-shadow: 0 1px 1px rgba(0,0,0,0.1);
				line-height: 1.85em;
				font-size: 0.94em;
				text-shadow: 0 1px 0 #fff;
				border-color: #c3c3c3 #bebebe #b6b6b6;
				position: absolute;
				right: 0px;
				bottom: 5px;
				z-index:3;" href="javascript:void(0);" onclick="javascript:delete_cart('.$CartRow->id.','.$s.')" class="remove_item">'.$product_remove.'</a>
				</span>
				<button class="btn-green btn-payment" style="width:134%;background-color: #569D37;" onclick="return ajax_auction('.$CartRow->id.','.$CartRow->user_id.','.$type.');">Put to auction<i class="fa fa-angle-right"></i></button>
				<span class="trick"></span>
				';
				$cartAmt = $cartAmt + (($CartRow->product_shipping_cost + $CartRow->price + ($CartRow->price * 0.01 * $CartRow->product_tax_cost))  * $CartRow->quantity);
				$cartShippingAmt = $cartShippingAmt + ($CartRow->product_shipping_cost * $CartRow->quantity);
				$cartTaxAmt = $cartTaxAmt + ($CartRow->product_tax_cost * $CartRow->quantity);
				$cartQty = $cartQty + $CartRow->quantity;
				$s++;
				
				
			}
			
		
			$cartSAmt = $MainShipCost;
			$cartTAmt = ($cartAmt * 0.01 * $MainTaxCost);
			$grantAmt = $cartAmt + $cartSAmt + $cartTAmt ;
	

			$CartValue.='</ul></dd></dl>';
			if ($ship_need>0){
						$CartValue.='	<dl class="profile-account" style="position:relative;"><dd><label class="label">'.$ship_to.'</label>
				<a style="cursor: pointer;
						background: #ccc;
						padding: 5px;
						display: inline-block;
						border: 1px solid #c3c3c3;
						padding: 0 1em;
						border-radius: 0.15em;
						color: #4c4f53;
						font-weight: bold;
						background: #f0f0f0;
						background: -webkit-linear-gradient(top, #fcfcfc,#f0f0f0);
						background: -ms-linear-gradient(top, #fafafa,#f0f0f0);
						background: -moz-linear-gradient(top, #fafafa,#f0f0f0);
						background: -o-linear-gradient(top, #fafafa,#f0f0f0);
						filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#fafafa\',endColorstr=\'#f0f0f0\');
						box-shadow: 0 1px 1px rgba(0,0,0,0.1);
						line-height: 1.85em;
						font-size: 0.94em;
						text-shadow: 0 1px 0 #fff;
						border-color: #c3c3c3 #bebebe #b6b6b6;
						position: absolute;
						right: 20px;
						top: 30px;" class="adres-button add-shipping-addr" href="javascript:void(0);" >'.$addnew_shipping_address.'</a>
				<select data-type="cart" id="address-cart" onchange="CartChangeAddress(this.value,this);">
					<option value="" id="address-select">'.$choose_shipping_address.'</option>
			';
			foreach ($shipVal->result() as $Shiprow){		
				if($Shiprow->primary == 'Yes'){ $optionsValues = 'selected="selected"'; $ChooseVal = $Shiprow->full_name.'<br>'.$Shiprow->address1.'<br>'.$Shiprow->city.' '.$Shiprow->state.' '.$Shiprow->postal_code.'<br>'.$Shiprow->country.'<br>'.$Shiprow->phone; $ship_id =$Shiprow->id;  }else{ $optionsValues ='';}
				$CartValue.='<option '.$optionsValues.' value="'.$Shiprow->id.'" l1="'.$Shiprow->full_name.'" l2="'.$Shiprow->address1.'" l3="'.$Shiprow->city.' '.$Shiprow->state.' '.$Shiprow->postal_code.'" l4="'.$Shiprow->country.'" l5="'.$Shiprow->phone.'">'.$Shiprow->full_name.'</option>';
			}
			$CartValue.='</select>
			<input type="hidden" name="Ship_address_val" id="Ship_address_val_cart" value="'.$ship_id.'" />
			
			<p class="default_addr"><span id="Chg_Add_Val">'.$ChooseVal.'</span></p>
				<span class="address-table"> </span>
				<span class="deleta-address" style="cursor: pointer;
					background: #ccc;
					padding: 5px;
					display: inline-block;
					border: 1px solid #c3c3c3;
					padding: 0 1em;
					border-radius: 0.15em;
					color: #4c4f53;
					font-weight: bold;
					background: #f0f0f0;
					background: -webkit-linear-gradient(top, #fcfcfc,#f0f0f0);
					background: -ms-linear-gradient(top, #fafafa,#f0f0f0);
					background: -moz-linear-gradient(top, #fafafa,#f0f0f0);
					background: -o-linear-gradient(top, #fafafa,#f0f0f0);
					filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#fafafa\',endColorstr=\'#f0f0f0\');
					box-shadow: 0 1px 1px rgba(0,0,0,0.1);
					line-height: 1.85em;
					font-size: 0.94em;
					text-shadow: 0 1px 0 #fff;
					border-color: #c3c3c3 #bebebe #b6b6b6;
					position: absolute;
					right: 20px;
					bottom: 10px;" data-type="cart" onclick="shipping_cart_address_delete(this);">'.$delete_this_address.'</span>
									</p></dd></dl>';
			}
			
			$CartValue.='</div>
		</div>
	</form></div></div>';
		}

		$countVal = $giftRes -> num_rows() + $cartQty + $SubcribRes -> num_rows();



		if($countVal >0 ){
			$CartDisp = ''.$GiftValue.$SubscribValue.$CartValue.'<div id="EmptyCart" style="border-bottom: none; display:none;background-color: #fff;padding: 10px;border-radius: 5px;" class="empty-alert" >
					<p style="text-align:center;"><img src="images/site/shopping_empty.jpg" alt="Shopping Cart Empty"></p>
					<p style="text-align:center;"><b>'.$shopping_cart_empty.'</b></p>
					<p style="text-align:center;">'.$dont_miss.' '.ucwords($this->config->item('email_title')).'. '.$shall_we.'</p>
				</div>';
		}else{

			$CartDisp = '<div style="background-color: #fff;padding: 10px;border-radius: 5px;"><h2>Shopping Cart</h2>
					<div style="border-bottom: none;" class="empty-alert" >
					<p style="text-align:center;"><img src="images/site/shopping_empty.jpg" alt="Shopping Cart Empty"></p>
					<p style="text-align:center;"><b>'.$shopping_cart_empty.'</b></p>
					<p style="text-align:center;">'.$dont_miss.' '.ucwords($this->config->item('email_title')).'. '.$shall_we.'</p>
				</div></div>';
		}

		return $CartDisp;

	}
	
	

	public function mani_gift_total($userid=''){

		$giftRes = $this->cart_model->get_all_details(GIFTCARDS_TEMP,array( 'user_id' => $userid));
		$giftAmt = 0;
		if($giftRes -> num_rows() > 0 ){

			foreach ($giftRes->result() as $giftRow){
				$giftAmt = $giftAmt + $giftRow->price_value;
			}

		}
		$SubcribRes = $this->cart_model->get_all_details(FANCYYBOX_TEMP,array( 'user_id' => $userid));
		$cartVal = $this->cart_model->get_all_details(SHOPPING_CART,array( 'user_id' => $userid));
		$countVal = $giftRes -> num_rows() + $SubcribRes -> num_rows() + $cartVal -> num_rows() ;

		return number_format($giftAmt,2,'.','').'|'.$countVal;

	}

	public function mani_subcribe_total($userid=''){

		$SubcribRes = $this->cart_model->get_all_details(FANCYYBOX_TEMP,array( 'user_id' => $userid));
		$SubcribAmt = 0; $SubcribSAmt = 0; $SubcribTAmt = 0; $SubcribTotalAmt = 0;
		if($SubcribRes -> num_rows() > 0 ){

			foreach ($SubcribRes->result() as $SubscribRow){
				$SubcribAmt = $SubcribAmt + $SubscribRow->price;
			}
			$SubcribSAmt = $SubcribRes->row()->shipping_cost;
			$SubcribTAmt = $SubcribRes->row()->tax;
			$SubcribTotalAmt = $SubcribAmt + $SubcribSAmt + $SubcribTAmt ;

		}
		$giftRes = $this->cart_model->get_all_details(GIFTCARDS_TEMP,array( 'user_id' => $userid));
		$cartVal = $this->cart_model->get_all_details(SHOPPING_CART,array( 'user_id' => $userid));
		$countVal = $SubcribRes -> num_rows() + $giftRes -> num_rows() + $cartVal -> num_rows() ;

		return number_format($SubcribAmt,2,'.','').'|'.number_format($SubcribSAmt,2,'.','').'|'.number_format($SubcribTAmt,2,'.','').'|'.number_format($SubcribTotalAmt,2,'.','').'|'.$countVal;

	}
	public function mani_cart_total($userid=''){

		$giftRes = $this->cart_model->get_all_details(GIFTCARDS_TEMP,array( 'user_id' => $userid));
		$cartVal = $this->cart_model->get_all_details(SHOPPING_CART,array( 'user_id' => $userid));
		$SubcribRes = $this->cart_model->get_all_details(FANCYYBOX_TEMP,array( 'user_id' => $userid));
		$cartAmt = 0; $cartShippingAmt = 0; $cartTaxAmt = 0; $cartMiniMainCount = 0;

		if($cartVal -> num_rows() > 0 ){
			foreach ($cartVal->result() as $CartRow){
				$cartAmt = $cartAmt + (($CartRow->product_shipping_cost +  ($CartRow->product_tax_cost * 0.01 * $CartRow->price ) + $CartRow->price)  * $CartRow->quantity);

				$cartMiniMainCount = $cartMiniMainCount + $CartRow->quantity;

			}
			$cartSAmt = $cartVal->row()->shipping_cost;
			$cartTAmt = $cartAmt * 0.01 * $cartVal->row()->tax;
			$grantAmt = $cartAmt + $cartSAmt + $cartTAmt ;

		}

		$countVal = $giftRes -> num_rows() + $SubcribRes -> num_rows() + $cartMiniMainCount;

		$this->db->select('discountAmount');
		$this->db->where('user_id',$userid);
		$query = $this->db->get(SHOPPING_CART);

		if($query->row()->discountAmount !=''){
			$grantAmt = $grantAmt - $query->row()->discountAmount;
		}

		return number_format($cartAmt,2,'.','').'|'.number_format($cartSAmt,2,'.','').'|'.number_format($cartTAmt,2,'.','').'|'.number_format($grantAmt,2,'.','').'|'.$countVal.'|'.number_format($query->row()->discountAmount,2,'.','');

	}



	public function mani_cart_coupon_sucess($userid=''){

		$cartVal = $this->cart_model->get_all_details(SHOPPING_CART,array( 'user_id' => $userid));
		$cartAmt = 0; $cartShippingAmt = 0; $cartTaxAmt = 0;

		if($cartVal -> num_rows() > 0 ){
			$k=0;
			foreach ($cartVal->result() as $CartRow){
				$cartAmt = $cartAmt + (($CartRow->product_shipping_cost +  ($CartRow->product_tax_cost * 0.01 * $CartRow->price ) + $CartRow->price)  * $CartRow->quantity);
				$newCartInd[] = $CartRow->indtotal;
				$k = $k + 1;
			}
			$cartSAmt = $cartVal->row()->shipping_cost;
			$cartTAmt = $cartAmt * 0.01 * $cartVal->row()->tax;
			$grantAmt = $cartAmt + $cartSAmt + $cartTAmt ;

		}

		$this->db->select('discountAmount');
		$this->db->from(SHOPPING_CART);
		$this->db->where('user_id = '.$userid);
		$query = $this->db->get();
		$newAmtsValues = @implode('|',$newCartInd);

			
		if($query->row()->discountAmount !=''){
			$grantAmt = $grantAmt - $query->row()->discountAmount;
		}

		return number_format($cartAmt,2,'.','').'|'.number_format($cartSAmt,2,'.','').'|'.number_format($cartTAmt,2,'.','').'|'.number_format($grantAmt,2,'.','').'|'.number_format($query->row()->discountAmount,2,'.','').'|'.$k.'|'.$newAmtsValues;

	}




	public function view_cart_details($condition = ''){
		$select_qry = "select p.*,u.full_name,u.user_name,u.thumbnail from ".PRODUCT." p LEFT JOIN ".USERS." u on u.id=p.user_id ".$condition;
		$cartList = $this->ExecuteQuery($select_qry);
		return $cartList;
			
	}

	public function view_atrribute_details(){
		$select_qry = "select * from ".ATTRIBUTE." where status='Active'";
		return $attList = $this->ExecuteQuery($select_qry);

	}

	public function Check_Code_Val($Code = '',$amount = '',$shipamount = '', $userid = ''){

		$code = $Code;
		$amount = $amount;
		$amountOrg = $amount;
		$ship_amount = $shipamount;

		$CoupRes = $this->cart_model->get_all_details(COUPONCARDS,array( 'code' => $code, 'card_status' => 'not used'));
		$GiftRes = $this->cart_model->get_all_details(GIFTCARDS,array( 'code' => $code));
		$ShopArr = $this->cart_model->get_all_details(SHOPPING_CART,array( 'user_id' => $userid));
		$excludeArr = array('code','amount','shipamount');


		if($CoupRes->num_rows() > 0){

			$PayVal = $this->cart_model->get_all_details(PAYMENT,array( 'user_id' => $userid, 'coupon_id' => $CoupRes->row()->id, 'status'=>'Paid' ));

			if($PayVal->num_rows() == 0){
					
				if($ShopArr->row()->couponID == 0){

					if($CoupRes->row()->quantity > $CoupRes->row()->purchase_count){

						$today = getdate();
						$tomorrow = mktime(0,0,0,date("m"),date("d"),date("Y"));
						$currDate = date("Y-d-m", $tomorrow);
						$couponExpDate = $CoupRes->row()->dateto;

						$curVal = (strtotime($couponExpDate) < time());
						if($curVal != '') {
							echo '5';
							exit();
						}


						if($CoupRes->row()->coupon_type == "shipping") {
							$totalamt = number_format($amount - $ship_amount,2,'.','');
							$discount ='0';

							$dataArr = array('discountAmount' => $discount,
											'couponID' => $CoupRes->row()->id,
											'couponCode' => $code,
											'coupontype' => 'Free Shipping',
											'is_coupon_used' => 'Yes',
											'shipping_cost' => 0,
											'total' => $totalamt);
							$condition =array('user_id' => $userid);
							$this->cart_model->commonInsertUpdate(SHOPPING_CART,'update',$excludeArr,$dataArr,$condition);
							echo 'Success|'.$CoupRes->row()->id.'|Shipping';
							exit();


						} elseif($CoupRes->row()->coupon_type == "category") {
							$newAmt = $amount;
							$catAry = @explode(',',$CoupRes->row()->category_id);
							foreach($ShopArr->result() as $shopRow){
								$shopCatArr = '';

								$shopCatArr = @explode(',',$shopRow->cate_id);
									
								$combArr = array_merge($catAry, $shopCatArr);
								$combArr1 = array_unique($combArr);
								if(count($combArr) != count($combArr1)){

									if($CoupRes->row()->price_type == 2){
										$percentage = $CoupRes->row()->price_value;
										$amountOrg = $shopRow->indtotal;
										$discount = ($percentage * 0.01) * $amountOrg;
										$IndAmt = number_format($amountOrg-$discount,2,'.','');
										$TotalAmt = $newAmt = number_format($newAmt - $discount,2,'.','');
										$dataArr = array('discountAmount' => $discount,
											'couponID' => $CoupRes->row()->id,
											'couponCode' => $code,
											'coupontype' => 'Category',
											'is_coupon_used' => 'Yes',
											'indtotal' => $IndAmt);
										$condition =array('id' => $shopRow->id);
										$this->cart_model->commonInsertUpdate(SHOPPING_CART,'update',$excludeArr,$dataArr,$condition);
											
										$dataArr1 = array('total' => $TotalAmt);
										$condition1 =array('user_id' => $userid);
										$this->cart_model->commonInsertUpdate(SHOPPING_CART,'update',$excludeArr,$dataArr1,$condition1);

											
									}elseif($CoupRes->row()->price_type == 1){

										$discount = $CoupRes->row()->price_value;
										$amountOrg = $shopRow->indtotal;
										if($amountOrg > $discount){
											$amountOrg = number_format($amountOrg-$discount,2,'.','');
											$TotalAmt = $newAmt = number_format($newAmt - $discount,2,'.','');
											$dataArr = array('discountAmount' => $discount,
											'couponID' => $CoupRes->row()->id,
											'couponCode' => $code,
											'coupontype' => 'Category',
											'is_coupon_used' => 'Yes',
											'indtotal' => $amountOrg);
											$condition =array('id' => $shopRow->id);
											$this->cart_model->commonInsertUpdate(SHOPPING_CART,'update',$excludeArr,$dataArr,$condition);
											$dataArr1 = array('total' => $TotalAmt);
											$condition1 =array('user_id' => $userid);
											$this->cart_model->commonInsertUpdate(SHOPPING_CART,'update',$excludeArr,$dataArr1,$condition1);
										}else{
											echo '7';
											exit();
										}
									}
								}
							}
							echo 'Success|'.$CoupRes->row()->id.'|Category';
							exit();

						} elseif($CoupRes->row()->coupon_type== "product") {
							$PrdArr = @explode(',',$CoupRes->row()->product_id);
							$newAmt = $amount;
							foreach($ShopArr->result() as $shopRow){

								$shopPrd = $shopRow->product_id;
									
								if(in_array($shopPrd,$PrdArr)==1){

									if($CoupRes->row()->price_type == 2){
											
										$percentage = $CoupRes->row()->price_value;
										$amountOrg = $shopRow->indtotal;
										$discount = ($percentage * 0.01) * $amountOrg;
										$IndAmt = number_format($amountOrg - $discount,2,'.','');
										$TotalAmt = $newAmt = number_format($newAmt - $discount,2,'.','');
										$dataArr = array('discountAmount' => $discount,
											'couponID' => $CoupRes->row()->id,
											'couponCode' => $code,
											'coupontype' => 'Product',
											'is_coupon_used' => 'Yes',
											'indtotal' => $IndAmt);
										$condition =array('id' => $shopRow->id);
											
										$this->cart_model->commonInsertUpdate(SHOPPING_CART,'update',$excludeArr,$dataArr,$condition);
										$dataArr1 = array('total' => $TotalAmt);
										$condition1 =array('user_id' => $userid);
										$this->cart_model->commonInsertUpdate(SHOPPING_CART,'update',$excludeArr,$dataArr1,$condition1);

									}elseif($CoupRes->row()->price_type == 1){

										$discount = $CoupRes->row()->price_value;
										$amountOrg = $shopRow->indtotal;
										if($amountOrg > $discount){
											$newDisAmt = number_format($amountOrg - $discount,2,'.','');
											$TotalAmt = $newAmt = number_format($newAmt - $discount,2,'.','');
											$dataArr = array('discountAmount' => $discount,
											'couponID' => $CoupRes->row()->id,
											'couponCode' => $code,
											'coupontype' => 'Product',
											'is_coupon_used' => 'Yes',
											'indtotal' => $newDisAmt);

											$condition =array('id' => $shopRow->id);
											$this->cart_model->commonInsertUpdate(SHOPPING_CART,'update',$excludeArr,$dataArr,$condition);
											$dataArr1 = array('total' => $TotalAmt);
											$condition1 =array('user_id' => $userid);
											$this->cart_model->commonInsertUpdate(SHOPPING_CART,'update',$excludeArr,$dataArr1,$condition1);

										}else{
											echo '7';
											exit();
										}
									}
								}
							}
							echo 'Success|'.$CoupRes->row()->id.'|Product';
							exit();

						}else{

							if($CoupRes->row()->price_type == 2){

								$percentage = $CoupRes->row()->price_value;
								$discount = ($percentage * 0.01) * $amount;
								$totalAmt = number_format($amount-$discount,2,'.','');
									
								$dataArr = array('discountAmount' => $discount,
											'couponID' => $CoupRes->row()->id,
											'couponCode' => $code,
											'coupontype' => 'Cart',
											'is_coupon_used' => 'Yes',
											'total' => $totalAmt);
								$condition =array('user_id' => $userid);

								$this->cart_model->commonInsertUpdate(SHOPPING_CART,'update',$excludeArr,$dataArr,$condition);
									
								/*foreach($ShopArr->result() as $shopRow){

								$amountOrg = $shopRow->indtotal;
								$discount = ($percentage * 0.01) * $amountOrg;
								$IndAmt = number_format($amountOrg - $discount,2,'.','');

								$dataArr = array('indtotal' => $IndAmt);
								$condition =array('id' => $shopRow->id);
								$this->cart_model->commonInsertUpdate(SHOPPING_CART,'update',$excludeArr,$dataArr,$condition);
								}*/
									
								echo 'Success|'.$CoupRes->row()->id;
								exit();

							}elseif($CoupRes->row()->price_type == 1){

								$discount = $CoupRes->row()->price_value;
								if($amount > $discount){
									$amountOrg = number_format($amount-$discount,2,'.','');
									$dataArr = array('discountAmount' => $discount,
											'couponID' => $CoupRes->row()->id,
											'couponCode' => $code,
											'coupontype' => 'Cart',
											'is_coupon_used' => 'Yes',
											'total' => $amountOrg);
									$condition =array('user_id' => $userid);
									$this->cart_model->commonInsertUpdate(SHOPPING_CART,'update',$excludeArr,$dataArr,$condition);
									/*$newDisAmt = ($discount / $ShopArr->num_rows());
									 foreach($ShopArr->result() as $shopRow){
										$amountOrg = $shopRow->indtotal;
										$IndAmt = number_format($amountOrg - $newDisAmt,2,'.','');
										$dataArr = array('indtotal' => $IndAmt);
										$condition =array('id' => $shopRow->id);
										$this->cart_model->commonInsertUpdate(SHOPPING_CART,'update',$excludeArr,$dataArr,$condition);
										}*/


									echo 'Success|'.$CoupRes->row()->id;
									exit();

								}else{
									echo '7';
									exit();
								}
							}

						}
					} else {
						echo '6';
						exit();
					}
				}else{
					echo '2';
					exit();
				}

					
			}else{
				echo '2';
				exit();
			}


		}elseif($GiftRes->num_rows() > 0){

			$curGiftVal = (strtotime($GiftRes->row()->expiry_date) < time());
			if($curGiftVal != '') {
				echo '8';
				exit();
			}

			if($GiftRes->row()->price_value > $GiftRes->row()->used_amount){

				$NewGiftAmt = $GiftRes->row()->price_value - $GiftRes->row()->used_amount;
				if($amount > $NewGiftAmt){
					$amountOrg = $amountOrg - $NewGiftAmt;

					$dataArr = array('discountAmount' => $NewGiftAmt,
											'couponID' => $GiftRes->row()->id,
											'couponCode' => $code,
											'coupontype' => 'Gift',
											'is_coupon_used' => 'Yes',
											'total' => $amountOrg);
					$condition =array('user_id' => $userid);
					$this->cart_model->update_details(SHOPPING_CART,$dataArr,$condition);



					/*$newDisAmt = ($NewGiftAmt / $ShopArr->num_rows());
						//echo '<pre>'; print_r($ShopArr->result_array());
						foreach($ShopArr->result() as $shopRow){
						$IndAmt = number_format($shopRow->indtotal - $newDisAmt,2,'.','');
						$dataArr = array('indtotal' => $IndAmt);
						$condition =array('id' => $shopRow->id);
						$this->cart_model->update_details(SHOPPING_CART,$dataArr,$condition);
						//echo '<pre>'.$this->db->last_query();
						}*/

				}else{
					$dataArr = array('discountAmount' => $amountOrg,
											'couponID' => $GiftRes->row()->id,
											'couponCode' => $code,
											'coupontype' => 'Gift',
											'is_coupon_used' => 'Yes',
											'total' => '0');
					$condition =array('user_id' => $userid);
					$this->cart_model->update_details(SHOPPING_CART,$dataArr,$condition);

					/*$newDisAmt = ($amountOrg / $ShopArr->num_rows());

					foreach($ShopArr->result() as $shopRow){
					$amountOrg = $shopRow->indtotal;
					$IndAmt = number_format($amountOrg - $newDisAmt,2,'.','');
					$dataArr = array('indtotal' => '0');
					$condition =array('id' => $shopRow->id);
					$this->cart_model->update_details(SHOPPING_CART,$dataArr,$condition);
					}*/
				}

				echo 'Success|'.$GiftRes->row()->id.'|Gift';
				exit();
					
			}else{
				echo '2';
				exit();
			}

		}else{
			echo '1';
			exit();
		}

	}
	public function Check_Code_Val_Remove($userid = ''){

		$excludeArr = array('code');

		$dataArr = array('discountAmount' => 0,
											'couponID' => 0,
											'couponCode' => '',
											'coupontype' => '',
											'is_coupon_used' => 'No');
		$condition =array('user_id' => $userid);
		$this->cart_model->commonInsertUpdate(SHOPPING_CART,'update',$excludeArr,$dataArr,$condition);
		return;



	}

	public function addPaymentCart($userid = '' , $paymentType=''){
	/*	$this->db->select('a.*,b.city,b.state,b.country,b.postal_code');
		$this->db->from(SHOPPING_CART.' as a');
		$this->db->join(SHIPPING_ADDRESS.' as b' , 'a.user_id = b.user_id and a.user_id = "'.$userid.'" and b.id="'.$this->input->post('Ship_address_val').'"');
		$AddPayt = $this->db->get();*/
		
		
		$condition = "a.user_id = ".$userid."";
		$this->db->select('a.*,p.product_name,p.seller_product_id,p.image,c.attr_name as attr_type,d.attr_name');
		$this->db->from(SHOPPING_CART.' as a');
		$this->db->join(PRODUCT.' as p','p.id=a.product_id');
		$this->db->join(SUBPRODUCT.' as d' , 'd.pid = a.attribute_values','left');
		$this->db->join(PRODUCT_ATTRIBUTE.' as c' , 'c.id = d.attr_id','left');
		$this->db->where($condition);
		$AddPayt = $this->db->get();
		
		


		if($this->session->userdata('randomNo') != '') {
			$delete = 'delete from '.PAYMENT.' where dealCodeNumber = "'.$this->session->userdata('randomNo').'" and user_id = "'.$userid.'" ';
			$this->ExecuteQuery($delete, 'delete');
			$dealCodeNumber = $this->session->userdata('randomNo');
		} else {
			$dealCodeNumber = mt_rand();
		}
		$condition1 = 'user_id = "'.$userid.'" and id="'.$this->input->post('Ship_address_val').'"';
		$shippingAddr = $this->cart_model->get_all_details(SHIPPING_ADDRESS,$condition1);
		$insertIds = array();
		foreach ($AddPayt->result() as $result) {

			if($this->input->post('is_gift')==''){
				$ordergift = 0;
			}else{
				$ordergift = 1;
			}
			$attr_name = '';
			if ($result->attr_type != '' && $result->attr_name != ''){
				$attr_name = $result->attr_type.'/'.$result->attr_name;
			}
			$sumTotal = number_format((($result->price + $result->product_shipping_cost + ($result->product_tax_cost * 0.01 * $result->price)) * $result->quantity ),2,'.','');
			
			
			if ($result->product_type=='physical'){
				$shipdata = 'shippingcountry = "'.$shippingAddr->country.'",
				shippingid = "'.$this->input->post('Ship_address_val').'",
				shippingstate = "'.$shippingAddr->state.'",
				shippingcity = "'.$shippingAddr->city.'",
				shippingcost = "'.$this->input->post('cart_ship_amount').'",
				tax = "'.$this->input->post('cart_tax_amount').'",
				product_shipping_cost = "'.$result->product_shipping_cost.'",
				product_tax_cost = "'.$result->product_tax_cost.'",';
			} else {
				$shipdata='';
			}
			if($result->product_type=='digital'){
				$digital = 'product_download_code = "'.$dealCodeNumber.'@'.$result->seller_product_id.'",
				product_code_status = "Pending",
				product_type = "'.$result->product_type.'",';
			}else{
				$digital='';
			}

			$insert = ' insert into '.PAYMENT.' set
								product_id = "'.$result->product_id.'",
								sell_id = "'.$result->sell_id.'",								
								price = "'.$result->price.'",
								quantity = "'.$result->quantity.'",
								indtotal = "'.$result->indtotal.'",
								shippingcountry = "'.$result->country.'",
								shippingid = "'.$this->input->post('Ship_address_val').'",
								shippingstate = "'.$result->state.'",
								shippingcity = "'.$result->city.'",
								shippingcost = "'.$this->input->post('cart_ship_amount').'",
								tax = "'.$this->input->post('cart_tax_amount').'",
								product_shipping_cost = "'.$result->product_shipping_cost.'",
								product_tax_cost = "'.$result->product_tax_cost.'",																												
								coupon_id  = "'.$result->couponID.'",
								discountAmount = "'.$this->input->post('discount_Amt').'",
								couponCode  = "'.$result->couponCode.'",
								coupontype = "'.$result->coupontype.'",
								sumtotal = "'.$sumTotal.'",
								user_id = "'.$result->user_id.'",
								created = now(),
								dealCodeNumber = "'.$dealCodeNumber.'",
								status = "Pending",
								payment_type = "",
								attribute_values = "'.$result->attribute_values.'",
								shipping_status = "Pending",
								total  = "'.$this->input->post('cart_total_amount').'", 
								note = "'.$this->input->post('note').'", 
								order_gift = "'.$ordergift.'", 
										old_product_name = "'.$result->product_name.'", 
								old_attr_name = "'.$attr_name.'", 
								old_image = "'.$result->image.'", 
								'.$digital.'
								inserttime = "'.time().'"';

			$insertIds[] = $this->cart_model->ExecuteQuery($insert, 'insert');
		}
			
		$paymtdata = array(
								'randomNo' => $dealCodeNumber,
								'randomIds' => $insertIds,
		);
		$this->session->set_userdata($paymtdata);

		return $insertIds;
	}


	public function addPaymentSubscribe($userid = ''){

		if($this->session->userdata('InvoiceNo') != '') {
			$InvoiceNo = $this->session->userdata('InvoiceNo');
		} else {
			$InvoiceNo = mt_rand();
		}

		$paymtdata = array(	'InvoiceNo' => $InvoiceNo);
		$this->session->set_userdata($paymtdata);

		$dataArr = array('invoice_no' => $InvoiceNo,
						'shipping_id' => $this->input->post('SubShip_address_val'),
						'shipping_cost' => $this->input->post('subcrib_ship_amount'),
						'tax' => $this->input->post('subcrib_tax_amount'),
						'total' => $this->input->post('subcrib_total_amount'),																		
		);
		$condition =array('user_id' => $userid);
		$this->cart_model->update_details(FANCYYBOX_TEMP,$dataArr,$condition);


		return;

	}
	public function get_commision_price($productprice= '',$type= ''){
		if($productprice != ''){
			$query = "select * from ".COMMISSION."  WHERE `type`='".$type."' AND '".$productprice."' BETWEEN `price_from` AND `price_to`";
			return $this->ExecuteQuery($query);
		}
	}


}

?>