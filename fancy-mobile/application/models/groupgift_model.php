<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This model contains all db functions related to user management
 * @author Teamtweaks
 *
 */
class Groupgift_model extends My_Model
{
	public function __construct() 
	{
		parent::__construct();
	}
	
	/**
    * 
    * Getting Users details
    * @param String $condition
    */
	public function saveGroupgiftSettings(){
		$getGiftcardSettings = $this->get_all_details(GROUPGIFT_SETTINGS,array());
		$config = '<?php ';
		foreach($getGiftcardSettings->row() as $key => $val){
			$value = addslashes($val);
			$config .= "\n\$config['groupgift_$key'] = '$value'; ";
		}
		$config .= ' ?>';
		$file = 'commonsettings/fc_giftcard_settings.php';
		file_put_contents($file, $config);
   }
    public function get_groupgift_list($uid='0'){
		$Query = "select * from ".GROUP_GIFTS.' where user_id='.$uid.' order by created desc';
		return $this->ExecuteQuery($Query);
	}
	 public function get_giftcard_details($condition=''){
   		$Query = " select * from ".GROUP_GIFTS." ".$condition;
   		return $this->ExecuteQuery($Query);
   }
}