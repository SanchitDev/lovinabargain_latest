<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * Landing page functions
 * @author Teamtweaks
 *
 */
class Affiliate_model extends My_Model
{
	public function __construct() 
	{
		parent::__construct();
	}

	public function get_affiliate_list($uid = '0'){
		$this->db->select('p.*,u.email,u.full_name,u.thumbnail,u.user_name');
		$this->db->from(AFFILIATE.' as p');
		$this->db->join(USERS.' as u' , 'p.affiliate_id = u.id');
		$this->db->where('p.referral_id = "'.$uid.'" and p.status="Active" and p.referral_id = u.referId');
		$this->db->order_by('p.dateAdded','Desc');
		return $this->db->get();
		//echo $str = $this->db->last_query();die;
	}
	
	public function admin_all_affiliate_list($Viewlimit = ''){
		$this->db->select('p.*,u.email,u.full_name,u.thumbnail,u.user_name,SUM(p.commission_point) as CPoint');
		$this->db->from(AFFILIATE.' as p');
		$this->db->join(USERS.' as u' , 'p.referral_id = u.id');
		$this->db->where('p.referral_id = u.id');
		$this->db->group_by('p.referral_id');
		$this->db->order_by('CPoint','Desc');
		if($Viewlimit !=''){
		$this->db->limit('10','0');
		}
		return $this->db->get();
	}
	
	public function admin_affiliate_referral_list(){
		$this->db->select('p.*,u.email,u.full_name,u.thumbnail,u.user_name,SUM(p.commission_point) as CPoint');
		$this->db->from(AFFILIATE.' as p');
		$this->db->join(USERS.' as u' , 'p.referral_id = u.id');
		$this->db->where('p.referral_id = u.id');
		$this->db->group_by('p.referral_id');
		$this->db->order_by('p.dateAdded','Desc');
		return $this->db->get();
	}
	
	public function getAffiliateSettings(){
		$this->db->select('p.*,u.email,u.full_name,u.thumbnail,u.user_name,SUM(p.commission_point) as CPoint');
		$this->db->from(AFFILIATE.' as p');
		$this->db->join(USERS.' as u' , 'p.referral_id = u.id');
		$this->db->where('p.referral_id = u.id');
		$this->db->group_by('p.referral_id');
		$this->db->order_by('p.dateAdded','Desc');
		return $this->db->get();
	}
	
	/**
	 *
	 * This function save the admin details in a file
	 */
	public function saveAffiliateSettings(){
		$getAdminSettingsDetails = $this->get_all_details(AFFILIATE_SETTING,array('id'=>'1'));
		$config = '<?php ';
		foreach($getAdminSettingsDetails->row() as $key => $val){
			$value = addslashes($val);
			$config .= "\n\$config['$key'] = '$value'; ";
		}
		$config .= ' ?>';
		$file = 'commonsettings/fc_affiliate_settings.php';
		file_put_contents($file, $config);
	}
	
	public function view_Affiliate_order_details($status){
		$this->db->select('p.*,u.email,u.full_name,u.address,u.phone_no,u.postal_code,u.state,u.country,u.city,pd.product_name,pd.id as PrdID');
		$this->db->from(PAYMENT.' as p');
		$this->db->join(USERS.' as u' , 'p.user_id = u.id');
		$this->db->join(PRODUCT.' as pd' , 'pd.id = p.product_id');		
		$this->db->where('p.status = "'.$status.'"');				
		$this->db->group_by("p.dealCodeNumber"); 
		$this->db->order_by("p.created", "desc"); 
		$PrdList = $this->db->get();
		
		//echo '<pre>'; print_r($PrdList->result()); die;
		return $PrdList;
	}
}
	