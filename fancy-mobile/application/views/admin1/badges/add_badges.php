<?php
$this->load->view('admin/templates/header.php');
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6>Add New Badges</h6>
					</div>
					<div class="widget_content">
					<?php 
						$attributes = array('class' => 'form_container left_label', 'id' => 'adduser_form');
						echo form_open_multipart('admin/badges/insertEditbadges',$attributes) 
					?>
	 						<ul>
							<li>
								 <div class="form_grid_12">
									    <label class="field_title" for="category">Category <span class="req">*</span></label>
									    <div class="form_input">
										    <select name="category" style=" width:295px; height:25px;"  tabindex="1" class="required tipTop">
											<option value="">Select Category</option>
											<?php if($MainCategories->num_rows() >0){
											      foreach($MainCategories->result() as $get_list){?>
												      <option value="<?php echo $get_list->id?>"><?php echo $get_list->cat_name?></option>     
										    <?php } }?>
											</select>
									    </div>
								    </div>
								</li>
                            <li>
								<div class="form_grid_12">
									<label class="field_title" for="name">Name <span class="req">*</span></label>
									<div class="form_input">
										<input name="name" style="width:295px;" type="text" tabindex="2" class="required tipTop" title="Please Enter the Name" value=""/>
									</div>
								</div>
								</li>
								<li>
								<div class="form_grid_12">
								<label class="field_title" for="description">Description<span class="req">*</span></label>
								<div class="form_input">
								<textarea name="description" tabindex="2" style="width:370px;" class="required large tipTop mceEditor" title="Please enter the description"></textarea>
								</div>
								</div>
							</li>
							 <li>
								<div class="form_grid_12">
									<label class="field_title" for="badge_image">Badge Image<span class="req">*</span></label>
									<div class="form_input">
										<input name="badge_image" id="product_image" type="file" tabindex="7" class="large required tipTop" title="Please select badge image"/><!--<span class="input_instruction green">You Can Upload Multiple Images</span>-->
									</div>
								</div>
								</li>
								<li>
								 <div class="form_grid_12">
									    <label class="field_title" for="badgethings">Badge Things<span class="req">*</span></label>
									    <div class="form_input">
										    <select name="badgethings" style=" width:295px; height:25px;"  tabindex="1" class="required tipTop">
											<option value="">Select Things</option>
											 <?php $badge_things = $this->config->item('bages_things');
											       foreach($badge_things as $badgethings){?>
												   <option value="<?php echo $badgethings;?>"><?php echo $badgethings;?></option> 
                                                 <?php  }												   
											 ?>
											</select>
									    </div>
								    </div>
								</li>
					            <li>
								<div class="form_grid_12">
									<div class="form_input">
										<button type="submit" class="btn_small btn_blue" tabindex="8"><span>Submit</span></button>
									</div>
								</div>
								</li>
							</ul>
						<?php echo form_close();?>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>
<script>
$('#adduser_form').validate({
	ignore : '',
	errorPlacement: function(error, element) {
	    error.appendTo( element.parent());  
	}
});
</script>

<?php 
$this->load->view('admin/templates/footer.php');
?>