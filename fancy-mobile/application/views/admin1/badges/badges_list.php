<?php
$this->load->view('admin/templates/header.php');
extract($privileges);
?>
<div id="content">
		<div class="grid_container">
			<?php 
				$attributes = array('id' => 'display_form');
				echo form_open('admin/location/change_location_status_global',$attributes) 
			?>
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading?></h6>
					</div>
					<div class="widget_content">
						<table class="display" id="newsletter_tbl">
						<thead>
						<tr>
							<th class="tip_top" title="Click to sort">
								 Name
							</th>
							<th class="tip_top" title="Click to sort">
								 Badge Things
							</th>
							<th class="tip_top" title="Click to sort">
								 Category
							</th>
							<th>
								 Image
							</th>
							<th>
								 User Counts
							</th>
                            <th>
								 Status
							</th>
							<th>
								 Action
							</th>
						</tr>
						</thead>
						<tbody>
						<?php 
						if ($badgesList->num_rows() > 0){
							foreach ($badgesList->result() as $row){
						?>
						<tr>
							<td class="center  tr_select">
								<?php echo $row->name;?>
							</td>
							<td class="center  tr_select">
								<?php echo $row->badgethings;?>
							</td>
							<td class="center  tr_select">
								<?php echo $row->category;?>
							</td>
							<td class="center  tr_select">
								<img width="50" height="50" src="images/badges/<?php echo $row->badge_image;?>"/>
							</td>
							<td class="center  tr_select">
								<?php $user_ids = explode(',',$row->user_id);?>
								<a href="admin/badges/users_list/<?php echo $row->id;?>"><?php echo count($user_ids);?></a>
							</td>
							<td class="center">
							<?php 
							if ($allPrev == '1' || in_array('2', $samedaydelivery)){
								$mode = ($row->status == 'Active')?'0':'1';
								if ($mode == '0'){
							?>
								<a title="Click to inactive" class="tip_top" href="javascript:confirm_status('admin/badges/change_status/<?php echo $mode;?>/<?php echo $row->id;?>');"><span class="badge_style b_done"><?php echo $row->status;?></span></a>
							<?php
								}else {	
							?>
								<a title="Click to active" class="tip_top" href="javascript:confirm_status('admin/badges/change_status/<?php echo $mode;?>/<?php echo $row->id;?>')"><span class="badge_style"><?php echo $row->status;?></span></a>
							<?php 
								}
							}else {
							?>
							<span class="badge_style b_done"><?php echo $row->status;?></span>
							<?php }?>
							</td>
							<td class="center">
							<?php if ($allPrev == '1' || in_array('2', $badges)){?>
								<span><a class="action-icons c-edit" href="admin/badges/edit_badges/<?php echo $row->id;?>" title="Edit">Edit</a></span>
							<?php }?>
							<?php if ($allPrev == '1' || in_array('3', $badges)){
							//if($row->status!='Active'){
							?>
								<span><a class="action-icons c-delete" href="javascript:confirm_delete('admin/badges/delete_badges/<?php echo $row->id;?>')" title="Delete">Delete</a></span>
							<?php //} 
							}?>
							</td>
						</tr>
						<?php 
							}
						}
						?>
						</tbody>
						<tfoot>
						<tr>
							<th>
								 Name
							</th>
							<th>
								 Badge Things
							</th>
							<th>
								 Category
							</th>
							<th>
								 Image
							</th>
							<th>
								 User Counts
							</th>
                            <th>
								 Status
							</th>
							<th>
								 Action
							</th>
						</tr></tfoot>
						</table>
					</div>
				</div>
			</div>
			<input type="hidden" name="statusMode" id="statusMode"/>
		</form>	
			
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('admin/templates/footer.php');
?>