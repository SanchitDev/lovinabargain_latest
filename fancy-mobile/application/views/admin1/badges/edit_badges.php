<?php
$this->load->view('admin/templates/header.php');
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6>Add New City</h6>
					</div>
					<div class="widget_content">
					<?php 
						$attributes = array('class' => 'form_container left_label', 'id' => 'adduser_form');
						echo form_open_multipart('admin/badges/insertEditbadges',$attributes) 
					?>
					 <?php if($badge_details->num_rows() ==1){?>
	 					<ul>
							<li>
							 <div class="form_grid_12">
							    <label class="field_title" for="category">Category<span class="req">*</span></label>
								<div class="form_input">
								     <select name="category" style=" width:295px; height:25px;"  tabindex="1" class="required tipTop">
										<option value="">Select Category</option>
									      <?php if($MainCategories->num_rows()>0){
											  foreach($MainCategories->result() as $Categories){?>
											    <option value="<?php echo $Categories->id;?>"<?php if($Categories->id == $badge_details->row()->category){echo "selected";}?>><?php echo $Categories->cat_name;?></option> 
                                          <?php  }} ?>
								    </select>
								 </div>
							  </div>
							</li>
                            <li>
							   <div class="form_grid_12">
								   <label class="field_title" for="name">Name <span class="req">*</span></label>
									<div class="form_input">
									   <input name="name" style="width:295px !important;" type="text" tabindex="2" class="required large tipTop" title="Please Enter the Name" value="<?php echo $badge_details->row()->name; ?>"/>
										<input name="badge_id" type="hidden" value="<?php echo $badge_details->row()->id; ?>"/>
									</div>
								</div>
							</li>
							<li>
							  <div class="form_grid_12">
								<label class="field_title" for="description">Description<span class="req">*</span></label>
								<div class="form_input">
								   <textarea name="description" tabindex="2" style="width:370px;" class="required large tipTop mceEditor" title="Please enter the description"><?php echo $badge_details->row()->description; ?></textarea>
								</div>
							  </div>
						   </li>
						   <li>
							  <div class="form_grid_12">
								  <label class="field_title" for="badge_image">Badge Image<span class="req">*</span></label>
								  <div class="form_input">
								      <input name="badge_image" type="file" tabindex="7" class="large tipTop" title="Please select badge image"/><span class="input_instruction green">
									  <img src="images/badges/<?php echo $badge_details->row()->badge_image;?>" width="50" height="50"/>
									  </span>
								  </div>
							  </div>
						   </li>
						   <li>
							 <div class="form_grid_12">
							    <label class="field_title" for="badgethings">Badge Things<span class="req">*</span></label>
								<div class="form_input">
								     <select name="badgethings" style=" width:295px; height:25px;"  tabindex="1" class="required tipTop">
										<option value="">Select Things</option>

									      <?php $badge_things = $this->config->item('bages_things');
											  foreach($badge_things as $badgethings){?>
											    <option value="<?php echo $badgethings;?>"<?php if($badgethings == $badge_details->row()->badgethings){echo "selected";}?>><?php echo $badgethings;?></option> 
                                          <?php  } ?>
								    </select>
								 </div>
							  </div>
							</li>
					       <li>
							  <div class="form_grid_12">
								  <div class="form_input">
									  <button type="submit" class="btn_small btn_blue" tabindex="8"><span>Update</span></button>
								  </div>
							  </div>
						   </li>
					   </ul>
							<?php } ?>
						<?php echo form_close();?>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>
<style>

</style>
<?php 
$this->load->view('admin/templates/footer.php');
?>