<?php
$this->load->view('admin/templates/header.php');
extract($privileges);
?>
<div id="content">
		<div class="grid_container">
			<?php 
				$attributes = array('id' => 'display_form');
				echo form_open('admin/affiliate/change_user_status_global',$attributes) 
			?>
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading?></h6>
						
					</div>
					<div class="widget_content">
						<table class="display" id="AFF_tbl">
						<thead>
						<tr>
							<th class="tip_top" title="Click to sort">
								 Full Name							</th>
							<th class="tip_top" title="Click to sort">
								 User Name							</th>
							<th class="tip_top" title="Click to sort">
								 Email							</th>
							<th>
								Thumbnail							</th>
<!-- 							<th class="tip_top" title="Click to sort">
								User Type
							</th>
 -->							<th class="tip_top" title="Click to sort">
					Commission
				Point($)</th>
							<th>
								 Action							</th>
						</tr>
						</thead>
						<tbody>
						<?php 
						if ($affiliateList->num_rows() > 0){
							foreach ($affiliateList->result() as $row){
						?>
						<tr>
							<td class="center">
								<?php echo $row->full_name;?>							</td>
							<td class="center">
								<?php echo $row->user_name;?>							</td>
							<td class="center">
								<?php echo $row->email;?>							</td>
							<td class="center">
							<div class="widget_thumb">
							<?php if ($row->thumbnail != ''){?>
								 <img width="40px" height="40px" src="<?php echo base_url();?>images/users/<?php echo $row->thumbnail;?>" />
							<?php }else {?>
								 <img width="40px" height="40px" src="<?php echo base_url();?>images/users/user-thumb1.png" />
							<?php }?>
							</div>							</td>
<!-- 							<td class="center">
								<?php //if ($row->group == 'User'){?>
								<span class="badge_style b_high"><?php //echo $row->group;?></span>
								<?php //}else {?>
								<span class="badge_style b_away"><?php //echo 'User / '.$row->group;?></span>
								<?php //}?>
							</td>
 -->							<td class="center">
					<?php echo $row->CPoint;?>
				  </td>
							<td class="center">
								<span><a class="action-icons c-suspend" href="admin/affiliate/view_affiliate_list/<?php echo $row->referral_id;?>" title="View">View</a></span>							</td>
						</tr>
						<?php 
							}
						}
						?>
						</tbody>
						<tfoot>
						<tr>
							<th>
								 Full Name							</th>
							<th>
								 User Name							</th>
							<th>
								 Email							</th>
							<th>
								Thumbnail							</th>
<!-- 							<th>
								User Type
							</th>
 -->							<th>Commission Point($)</th>
							<th>
								 Action							</th>
						</tr>
						</tfoot>
						</table>
				  </div>
				</div>
			</div>
			<input type="hidden" name="statusMode" id="statusMode"/>
			<input type="hidden" name="SubAdminEmail" id="SubAdminEmail"/>
		</form>	
			
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('admin/templates/footer.php');
?>