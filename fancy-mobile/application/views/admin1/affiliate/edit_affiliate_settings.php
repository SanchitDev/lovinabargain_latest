<?php
$this->load->view('admin/templates/header.php');
?>

<div id="content">
  <div class="grid_container">
    <div class="grid_12">
      <div class="widget_wrap">
        <div class="widget_wrap tabby">
          <div class="widget_top"> <span class="h_icon list"></span>
            <h6>Global Affiliate Configuration</h6>
            <div id="widget_tab">
              <ul>
                <li><a href="#tab1" class="active_tab">Affiliate Settings</a></li>
              </ul>
            </div>
          </div>
          <div class="widget_content">
            <?php 
				$attributes = array('class' => 'form_container left_label', 'id' => 'settings_form', 'enctype' => 'multipart/form-data');
				echo form_open_multipart('admin/affiliate/affiliate_global_settings',$attributes) 
			?>
			<input type="hidden" name="form_mode" value="main_settings"/>
            <div id="tab1">
              <ul>
               
                
                <li>
                  <div class="form_grid_12">
                    <label class="field_title" for="unlike_text"><b>Affiliate Setting</b></label>
                  </div>
                </li>
                 <li>
                    <div class="form_grid_12">
                    <label class="field_title" for="aff_option">Affiliate Option</label>
                    <div class="form_input">
                    <input type="radio" name="aff_option" <?php if($affiliate_settings->row()->aff_option == 'true'){?> checked="checked" <?php } ?> value="true" />Yes&nbsp;&nbsp;&nbsp;
                    <input type="radio"  name="aff_option" <?php if($affiliate_settings->row()->aff_option == 'false'){?> checked="checked" <?php } ?> value="false" />No
                    </div>
                    </div>
                </li>
                <li>
                    <div class="form_grid_12">
                    <label class="field_title" for="recurring_opt">Recurring Option</label>
                    <div class="form_input">
                    <input type="radio" name="recurring_opt" <?php if($affiliate_settings->row()->recurring_opt == 'true'){?> checked="checked" <?php } ?> value="true" />Yes&nbsp;&nbsp;&nbsp;
                    <input type="radio"  name="recurring_opt" <?php if($affiliate_settings->row()->recurring_opt == 'false'){?> checked="checked" <?php } ?> value="false" />No
                    </div>
                    </div>
                </li>
                
                <li>
                  <div class="form_grid_12">
                    <label class="field_title" for="com_point">Affiliate Commission Percentage (%) per Dollar ($)</label>
                    <div class="form_input">
                      <input name="com_point" id="com_point" type="text" value="<?php echo $affiliate_settings->row()->com_point;?>" tabindex="10" class="large tipTop" title="Please enter the affiliate commission point"/>
                    </div>
                  </div>
                </li>
                
                <!--<li>
                  <div class="form_grid_12">
                    <label class="field_title" for="com_point">Affiliate Commission Percentage per Dollar </label>
                    <div class="form_input">
                      <input name="com_percent" id="com_percent" type="text" value="<?php echo htmlentities($affiliate_settings->row()->com_percent);?>" tabindex="10" class="large tipTop" title="Please enter the affiliate commission percentage"/>
                    </div>
                  </div>
                </li>-->
                
              </ul>
            <ul><li><div class="form_grid_12">
				<div class="form_input">
					<button type="submit" class="btn_small btn_blue" tabindex="15"><span>Submit</span></button>
				</div>
			</div></li></ul>
			</div>
            </form>
        </div>
      </div>
    </div>
  </div>
  <span class="clear"></span> </div>
</div>
<?php 
$this->load->view('admin/templates/footer.php');
?>
