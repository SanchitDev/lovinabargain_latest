<?php
$this->load->view('admin/templates/header.php');
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6>Add New City</h6>
					</div>
					<div class="widget_content">
					<?php 
						$attributes = array('class' => 'form_container left_label', 'id' => 'adduser_form');
						echo form_open('admin/samedaydelivery/insertEditsamedaydly',$attributes) 
					?>
	 						<ul>
							<li>
								 <div class="form_grid_12">
									    <label class="field_title" for="location_name">Country Name <span class="req">*</span></label>
									    <div class="form_input">
										    <select name="country_name" style=" width:295px; height:25px;" id="name" tabindex="1" class="required tipTop">
											<option value="">Select Country</option>
											<?php if($country_list->num_rows() >0){
											      foreach($country_list->result() as $get_list){?>
												      <option value="<?php echo $get_list->country_code?>"><?php echo $get_list->name?></option>     
										    <?php } }?>
											</select>
									    </div>
								    </div>
								</li>
                            <li>
								<div class="form_grid_12">
									<label class="field_title" for="city_name">City Name <span class="req">*</span></label>
									<div class="form_input">
										<input name="city_name" id="city_name" type="text" tabindex="2" class="required small tipTop" title="Please Enter the City" value=""/>
									</div>
								</div>
								</li>
								<li>
								<div class="form_grid_12">
									<label class="field_title" for="start_zip_code">Start Zip Code <span class="req">*</span></label>
									<div class="form_input">
										<input name="start_zip_code" id="start_zip_code" type="text" tabindex="2" class="required small tipTop" title="Please Enter the Start Zip Code" value=""/>
									</div>
								</div>
								</li>
								<li>
								<div class="form_grid_12">
									<label class="field_title" for="start_zip_code">End Zip Code <span class="req">*</span></label>
									<div class="form_input">
										<input name="end_zip_code" id="end_zip_code" type="text" tabindex="2" class="required small tipTop" title="Please Enter the End Zip Code" value=""/>
									</div>
								</div>
								</li>
					            <li>
								<div class="form_grid_12">
									<div class="form_input">
										<button type="submit" class="btn_small btn_blue" tabindex="8"><span>Submit</span></button>
									</div>
								</div>
								</li>
							</ul>
						<?php echo form_close();?>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('admin/templates/footer.php');
?>