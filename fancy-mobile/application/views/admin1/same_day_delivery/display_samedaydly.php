<?php
$this->load->view('admin/templates/header.php');
extract($privileges);
?>
<div id="content">
		<div class="grid_container">
			<?php 
				$attributes = array('id' => 'display_form');
				echo form_open('admin/location/change_location_status_global',$attributes) 
			?>
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading?></h6>
					</div>
					<div class="widget_content">
						<table class="display" id="newsletter_tbl">
						<thead>
						<tr>
							<th class="tip_top" title="Click to sort">
								 Country Code
							</th>
							<th class="tip_top" title="Click to sort">
								 City Name
							</th>
                            <th>
								 Status
							</th>
							<th>
								 Action
							</th>
						</tr>
						</thead>
						<tbody>
						<?php 
						if ($samedaydlyList->num_rows() > 0){
							foreach ($samedaydlyList->result() as $row){
						?>
						<tr>
							<td class="center  tr_select">
								<?php echo $row->country_name;?>
							</td>
							<td class="center  tr_select">
								<?php echo $row->city_name;?>
							</td>
							<td class="center">
							<?php 
							if ($allPrev == '1' || in_array('2', $samedaydelivery)){
								$mode = ($row->status == 'Active')?'0':'1';
								if ($mode == '0'){
							?>
								<a title="Click to inactive" class="tip_top" href="javascript:confirm_status('admin/samedaydelivery/change_status/<?php echo $mode;?>/<?php echo $row->id;?>');"><span class="badge_style b_done"><?php echo $row->status;?></span></a>
							<?php
								}else {	
							?>
								<a title="Click to active" class="tip_top" href="javascript:confirm_status('admin/samedaydelivery/change_status/<?php echo $mode;?>/<?php echo $row->id;?>')"><span class="badge_style"><?php echo $row->status;?></span></a>
							<?php 
								}
							}else {
							?>
							<span class="badge_style b_done"><?php echo $row->status;?></span>
							<?php }?>
							</td>
							<td class="center">
							<?php if ($allPrev == '1' || in_array('2', $location)){?>
								<span><a class="action-icons c-edit" href="admin/samedaydelivery/edit_samedaydly/<?php echo $row->id;?>" title="Edit">Edit</a></span>
							<?php }?>
							<?php if ($allPrev == '1' || in_array('3', $location)){
							//if($row->status!='Active'){
							?>
								<span><a class="action-icons c-delete" href="javascript:confirm_delete('admin/samedaydelivery/delete_samedaydly/<?php echo $row->id;?>')" title="Delete">Delete</a></span>
							<?php //} 
							}?>
							</td>
						</tr>
						<?php 
							}
						}
						?>
						</tbody>
						<tfoot>
						<tr>
							<th>
								 Country Code
							</th>
							<th>
								 City Name
							</th>
                            <th>
								 Status
							</th>
							<th>
								 Action
							</th>
						</tr></tfoot>
						</table>
					</div>
				</div>
			</div>
			<input type="hidden" name="statusMode" id="statusMode"/>
		</form>	
			
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('admin/templates/footer.php');
?>