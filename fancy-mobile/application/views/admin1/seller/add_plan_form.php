<?php
$this->load->view('admin/templates/header.php');
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6>Edit Plan</h6>
					</div>
					<div class="widget_content">
					<?php 
						$attributes = array('class' => 'form_container left_label');
						echo form_open('admin/seller/updateEditplan',$attributes) 
					?>
	 						<ul>
                            
                            <li>
								<div class="form_grid_12">
									<label class="field_title" for="user_name">Plan name<span class="req">*</span></label>
									<div class="form_input">
										<input name="plan_name" id="code" type="text" tabindex="2" class="required small tipTop" title="Please enter plan name" value="<?php echo $sellerPlan->row()->plan_name; ?>"/>
									</div>
								</div>
								</li>
                                
                                <li>
								<div class="form_grid_12">
									<label class="field_title" for="group">Plan price <span class="req">*</span></label>
									<div class="form_input">
										<input name="plan_price" id="quantity" type="text" tabindex="3" class="required small tipTop" title="Please enter plan price" value="<?php echo $sellerPlan->row()->plan_price; ?>"/>
									</div>
								</div>
								</li>
                                
                                <li>
								<div class="form_grid_12">
									<label class="field_title" for="datefrom">Plan period(days)<span class="req">*</span></label>
									<div class="form_input">
										<input name="plan_period" type="text" tabindex="5" class="required small tipTop" title="Please enter plan period" value="<?php echo $sellerPlan->row()->plan_period; ?>"/>
									</div>
								</div>
								</li>
								<li>
								<div class="form_grid_12">
									<label class="field_title" for="dateto">No of products promoted
										<span class="req">*</span></label>
									<div class="form_input">
										<input name="plan_product_count" id="dateto" type="text" tabindex="6" class="required small tipTop" title="Please enter no of products to be promoted" value="<?php echo $sellerPlan->row()->plan_product_count; ?>"/>
									</div>
								</div>
								</li>
                                
                                <li>
								<div class="form_grid_12">
                                    <label class="field_title">No of sellers for featured<span class="label_intro"></span></label>
									<div class="form_input">
										<input name="plan_promoted_count" type="text" tabindex="6" class="required small tipTop" title="Please enter maximum no of sellers featured" value="<?php echo $sellerPlan->row()->plan_promoted_count; ?>"/>										
									</div>
								</div>
								</li>
                                </ul>
	                            <ul>		
	 							<li>
								<div class="form_grid_12">
									<div class="form_input">
										<button type="submit" class="btn_small btn_blue" tabindex="8"><span>Update</span></button>
									</div>
								</div>
								</li>
							</ul>
						</form>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('admin/templates/footer.php');
?>