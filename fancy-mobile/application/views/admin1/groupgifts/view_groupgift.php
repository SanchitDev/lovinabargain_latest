<?php
$this->load->view('admin/templates/header.php');
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6>View Store</h6>
					</div>
					<div class="widget_content">
					<?php 
						$attributes = array('class' => 'form_container left_label');
						echo form_open(ADMIN_PATH,$attributes) 
					?>
					<div id="tab1">
	 						<ul>
								<li>
								<div class="form_grid_12">
									<label class="field_title" for="admin_name">Recipient Name</label>
									<div class="form_input">
										<?php 
										if ($view_groupgift->row()->recipient_name == ''){
											echo 'Not available';
										}else {
											echo $view_groupgift->row()->recipient_name;
										}
										?>
									</div>
								</div>
								</li>
								<li>
								<div class="form_grid_12">
									<label class="field_title" for="admin_name">Recipient Email</label>
									<div class="form_input">
										<?php 
										if ($view_groupgift->row()->recipient_email == ''){
											echo 'Not available';
										}else {
											echo $view_groupgift->row()->recipient_email;
										}
										?>
									</div>
								</div>
								</li>
								<li>
								<div class="form_grid_12">
									<label class="field_title" for="admin_name">Gift Name</label>
									<div class="form_input">
										<?php 
										if ($view_groupgift->row()->gift_name == ''){
											echo 'Not available';
										}else {
											echo $view_groupgift->row()->gift_name;
										}
										?>
									</div>
								</div>
								</li>
								<li>
								<div class="form_grid_12">
									<label class="field_title" for="admin_name">Gift Description</label>
									<div class="form_input">
										<?php 
										if ($view_groupgift->row()->gift_description == ''){
											echo 'Not available';
										}else {
											echo $view_groupgift->row()->gift_description;
										}
										?>
									</div>
								</div>
								</li>
								<li>
								<div class="form_grid_12">
									<label class="field_title" for="admin_name">Created</label>
									<div class="form_input">
										<?php 
										if ($view_groupgift->row()->created == ''){
											echo 'Not available';
										}else {
											echo $view_groupgift->row()->created;
										}
										?>
									</div>
								</div>
								</li>
								<li>
								<div class="form_grid_12">
									<label class="field_title" for="admin_name">Expiry date</label>
									<div class="form_input">
										<?php 
										if ($view_groupgift->row()->expired_date == ''){
											echo 'Not available';
										}else {
											echo $view_groupgift->row()->expired_date;
										}
										?>
									</div>
								</div>
								</li>
								<li>
								<div class="form_grid_12">
									<label class="field_title" for="admin_name">Status</label>
									<div class="form_input">
										<?php echo $view_groupgift->row()->status;?>
									</div>
								</div>
								</li>
								<li>
								<div class="form_grid_12">
									<div class="form_input">
										<a href="admin/groupgift/display_groupgift<?php //if ($view_groupgift->row()->status=='pending'){echo 'requests';}else {echo 'list';}?>" class="tipLeft" title="Go to Group Gifts <?php //if ($view_groupgift->row()->status=='pending'){echo 'requests';}else {echo 'list';}?>"><span class="badge_style b_done">Back</span></a>
									</div>
								</div>
								</li>
							</ul>
						</div>
					     
						</form>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('admin/templates/footer.php');
?>