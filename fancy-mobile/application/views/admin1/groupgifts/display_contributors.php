<?php
$this->load->view('admin/templates/header.php');
extract($privileges);
?>
<div id="content">
		<div class="grid_container">
			<?php 
				$attributes = array('id' => 'display_form');
				echo form_open('admin/groupgift/change_groupgift_status_global',$attributes) 
			?>
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading?></h6>
						<!--<div style="float: right;line-height:40px;padding:0px 10px;height:39px;">
						<?php 
						if ($allPrev == '1' || in_array('2', $giftcards)){
							if ($giftcard_status == 'Enable'){
						?>
							<div class="btn_30_light" style="height: 29px;">
								<a href="javascript:void(0)" onclick="return disableGiftCards('admin/giftcards/change_giftcards_status/2','<?php echo $subAdminMail; ?>');" class="tipTop" title="click here to disable giftcards"><span class="icon delete_co"></span><span class="btn_link">Disable Giftcards</span></a>
							</div>
						<?php }else {?>
							<div class="btn_30_light" style="height: 29px;">
								<a href="javascript:void(0)" onclick="return disableGiftCards('admin/giftcards/change_giftcards_status/1','<?php echo $subAdminMail; ?>');" class="tipTop" title="click here to enable giftcards"><span class="icon active_co"></span><span class="btn_link">Enable Giftcards</span></a>
							</div>
						<?php 
						}
						}
						if ($allPrev == '1' || in_array('3', $giftcards)){
						?>
							<div class="btn_30_light" style="height: 29px;">
								<a href="javascript:void(0)" onclick="return checkBoxValidationAdmin('Delete','<?php echo $subAdminMail; ?>');" class="tipTop" title="Select any checkbox and click here to delete records"><span class="icon cross_co"></span><span class="btn_link">Delete</span></a>
							</div>
						<?php }?>
						</div>-->
					</div>
					<div class="widget_content">
						<table class="display display_tbl" id="groupgift_tbl">
						<thead>
						<tr>
							<th class="tip_top" title="Click to sort">
								 Contributor Name
							</th>
							<th class="tip_top" title="Click to sort">
								 Transaction Id
							</th>
							<th class="tip_top" title="Click to sort">
								Amount
							</th>
							<th class="tip_top" title="Click to sort">
								 Payment Type
							</th>
							<th class="tip_top" title="Click to sort">
								Status
							</th>
							<th>
								 Action
							</th>
						</tr>
						</thead>
						<tbody>
						<?php 
						if ($contributeDetails->num_rows() > 0){
							foreach ($contributeDetails->result() as $row){
						?>
						<tr>
							<td class="center">
								<?php echo $row->contributor_name;?>
							</td>
                            <td class="center">
								<?php 
									echo $row->transaction_id;
								?>
							</td>
							<td class="center">
							
									<?php echo $row->amount;?>
							</td>
							<td class="center">
							
									<?php echo $row->payment_type;?>
							</td>
							<td class="center">
								<a class="tip_top"><span class="badge_style b_done"><?php echo $row->status;?></span></a>
							</td>
							<td class="center">
							<span><a class="action-icons c-suspend" href="admin/groupgift/view_contributors/<?php echo $row->id;?>" title="View">View</a></span>
							</td>
						</tr>
						<?php 
							}
						}
						?>
						</tbody>
						<tfoot>
						<tr>
							<th>
								 Contributor Name
							</th>
							<th>
								 Transaction Id
							</th>
							<th>
								Amount
							</th>
							<th>
								 Payment Type
							</th>
							<th>
								Status
							</th>
							<th>
								 Action
							</th>
						</tr>
						</tfoot>
						</table>
					</div>
				</div>
			</div>
			<input type="hidden" name="statusMode" id="statusMode"/>
			<input type="hidden" name="SubAdminEmail" id="SubAdminEmail"/>
		</form>	
			
		</div>
		<span class="clear"></span>
	</div>
</div>
<script>
$('#groupgift_tbl').dataTable({   
		 "aoColumnDefs": [
							{ "bSortable": false, "aTargets": [ 4,5 ] }
						],
						"aaSorting": [[1, 'asc']],
		"sPaginationType": "full_numbers",
		"iDisplayLength": 100,
		"oLanguage": {
	        "sLengthMenu": "<span class='lenghtMenu'> _MENU_</span><span class='lengthLabel'>Entries per page:</span>",	
	    },
		 "sDom": '<"table_top"fl<"clear">>,<"table_content"t>,<"table_bottom"p<"clear">>'
		 
		});
		</script>
<style>
#groupgift_tbl tr th, #groupgift_tbl tr td {border-right: 1px solid #CCCCCC;}
</style>
<?php 
$this->load->view('admin/templates/footer.php');
?>