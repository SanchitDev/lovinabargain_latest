<?php
$this->load->view('admin/templates/header.php');
extract($privileges);
?>
<div id="content">
		<div class="grid_container">
			<?php 
				$attributes = array('id' => 'display_form');
				echo form_open('admin/groupgift/change_groupgift_status_global',$attributes) 
			?>
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading?></h6>
						<!--<div style="float: right;line-height:40px;padding:0px 10px;height:39px;">
						<?php 
						if ($allPrev == '1' || in_array('2', $giftcards)){
							if ($giftcard_status == 'Enable'){
						?>
							<div class="btn_30_light" style="height: 29px;">
								<a href="javascript:void(0)" onclick="return disableGiftCards('admin/giftcards/change_giftcards_status/2','<?php echo $subAdminMail; ?>');" class="tipTop" title="click here to disable giftcards"><span class="icon delete_co"></span><span class="btn_link">Disable Giftcards</span></a>
							</div>
						<?php }else {?>
							<div class="btn_30_light" style="height: 29px;">
								<a href="javascript:void(0)" onclick="return disableGiftCards('admin/giftcards/change_giftcards_status/1','<?php echo $subAdminMail; ?>');" class="tipTop" title="click here to enable giftcards"><span class="icon active_co"></span><span class="btn_link">Enable Giftcards</span></a>
							</div>
						<?php 
						}
						}
						if ($allPrev == '1' || in_array('3', $giftcards)){
						?>
							<div class="btn_30_light" style="height: 29px;">
								<a href="javascript:void(0)" onclick="return checkBoxValidationAdmin('Delete','<?php echo $subAdminMail; ?>');" class="tipTop" title="Select any checkbox and click here to delete records"><span class="icon cross_co"></span><span class="btn_link">Delete</span></a>
							</div>
						<?php }?>
						</div>-->
					</div>
					<div class="widget_content">
						<table class="display display_tbl" id="groupgift_tbl">
						<thead>
						<tr>
							<th class="center">
								<input name="checkbox_id[]" type="checkbox" value="on" class="checkall">
							</th>
							<th class="tip_top" title="Click to sort">
								 Recipient Email
							</th>
							<th class="tip_top" title="Click to sort">
								 Gift Name
							</th>
							<th class="tip_top" title="Click to sort">
								Created Date
							</th>
							<th class="tip_top" title="Click to sort">
								 Expired Date
							</th>
							<th class="tip_top" title="Click to sort">
								Status
							</th>
							<th>
								 Action
							</th>
						</tr>
						</thead>
						<tbody>
						<?php 
						if ($GroupgiftList->num_rows() > 0){
							foreach ($GroupgiftList->result() as $row){
						?>
						<tr>
							<td class="center tr_select ">
								<input name="checkbox_id[]" type="checkbox" value="<?php echo $row->id;?>">
							</td>
							<td class="center">
								<?php echo $row->recipient_name;?>
							</td>
                            <td class="center">
								<?php 
									echo $row->gift_name;
								?>
							</td>
							<td class="center">
							
									<?php echo $row->created;?>
							</td>
							<td class="center">
							
									<?php echo $row->expired_date;?>
							</td>
							<td class="center">
							<?php if($row->status == 'Active'){?>
                            <?php if ($allPrev == '1' || in_array('2', $groupgift)){
								$mode = ($row->status == 'Active')?'0':'1';
								if ($mode == '0'){
							?>
								<a title="Click to Cancelled" class="tip_top" href="javascript:confirm_status('admin/groupgift/change_status/<?php echo $mode;?>/<?php echo $row->id;?>');"><span class="badge_style b_done"><?php echo $row->status;?></span></a>
							<?php
								}else {	
							?>
								<a title="Click to active" class="tip_top" href="javascript:confirm_status('admin/groupgift/change_status/<?php echo $mode;?>/<?php echo $row->id;?>')"><span class="badge_style"><?php echo $row->status;?></span></a>
							<?php 
								}
							}}else {
							?>
							<span class="badge_style"><?php echo $row->status;?></span>&nbsp; 
							<?php if($contributeDetails[$row->id]->num_rows()>0){
								     foreach($contributeDetails[$row->id]->result() as $_contributeDetails){
									   $id = $_contributeDetails->groupgift_id;
									 }
								  ?>
								  <a href="admin/groupgift/refund/<?php echo $row->id;?>">Refund</a>
								  <?php }?>
							<?php }?>
							</td>
							<td class="center">
							<span><a class="action-icons c-suspend" href="admin/groupgift/view_groupgift/<?php echo $row->id;?>" title="View">View</a></span>
							</td>
						</tr>
						<?php 
							}
						}
						?>
						</tbody>
						<tfoot>
						<tr>
							<th class="center">
								<input name="checkbox_id[]" type="checkbox" value="on" class="checkall">
							</th>
							<th>
								 Recipient Name
							</th>
							<th>
								 Gift Name
							</th>
							<th>
								Created Date
							</th>
							<th>
								 Expired Date
							</th>
							<th>
								Status
							</th>
							<th>
								 Action
							</th>
						</tr>
						</tfoot>
						</table>
					</div>
				</div>
			</div>
			<input type="hidden" name="statusMode" id="statusMode"/>
			<input type="hidden" name="SubAdminEmail" id="SubAdminEmail"/>
		</form>	
			
		</div>
		<span class="clear"></span>
	</div>
</div>
<script>
$('#groupgift_tbl').dataTable({   
		 "aoColumnDefs": [
							{ "bSortable": false, "aTargets": [ 0,8 ] }
						],
						"aaSorting": [[1, 'asc']],
		"sPaginationType": "full_numbers",
		"iDisplayLength": 100,
		"oLanguage": {
	        "sLengthMenu": "<span class='lenghtMenu'> _MENU_</span><span class='lengthLabel'>Entries per page:</span>",	
	    },
		 "sDom": '<"table_top"fl<"clear">>,<"table_content"t>,<"table_bottom"p<"clear">>'
		 
		});
		</script>
<style>
#groupgift_tbl tr th, #groupgift_tbl tr td {border-right: 1px solid #CCCCCC;}
</style>
<?php 
$this->load->view('admin/templates/footer.php');
?>