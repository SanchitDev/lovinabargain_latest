<?php
$this->load->view('site/templates/header',$this->data);
?>
<link rel="stylesheet" href="css/site/<?php echo SITE_COMMON_DEFINE ?>timeline.css" type="text/css" media="all"/>
<link rel="stylesheet" media="all" type="text/css" href="css/site/<?php echo SITE_COMMON_DEFINE ?>profile.css" />
<!-- Section_start -->
<div class="lang-en wider no-subnav">
<div id="container-wrapper">
  <div class="container usersection">
    <div class="icon-cache"></div>
    <div id="tooltip"></div>
    <div class="wrapper-content right-sidebar">
<?php if($flash_data != '') { ?>
		<div class="errorContainer" id="<?php echo $flash_data_type;?>">
			<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
			<p><span><?php echo $flash_data;?></span></p>
		</div>
		<?php } ?>
      <div id="content">
        <div class="wrapper timeline normal">
          <ul class="user-tab">
            <li><a href="user/<?php echo $userProfileDetails->row()->user_name;?>" ><b><?php echo LIKED_BUTTON;?></b> <small><?php echo $userProfileDetails->row()->likes;?></small></a></li>
            <li><a href="user/<?php echo $userProfileDetails->row()->user_name;?>/added" ><b><?php if($this->lang->line('display_added') != '') { echo stripslashes($this->lang->line('display_added')); } else echo "Added"; ?></b> <small><?php echo $userProfileDetails->row()->products;?></small></a></li>
            <li><a href="user/<?php echo $userProfileDetails->row()->user_name;?>/lists"><b><?php if($this->lang->line('display_lists') != '') { echo stripslashes($this->lang->line('display_lists')); } else echo "Lists"; ?></b> <small><?php echo $userProfileDetails->row()->lists;?></small></a></li>
            <li><a href="user/<?php echo $userProfileDetails->row()->user_name;?>/wants"><b><?php if($this->lang->line('display_wants') != '') { echo stripslashes($this->lang->line('display_wants')); } else echo "Wants"; ?></b> <small><?php echo $userProfileDetails->row()->want_count;?></small></a></li>
            <li><a href="user/<?php echo $userProfileDetails->row()->user_name;?>/owns"><b><?php if($this->lang->line('display_owns') != '') { echo stripslashes($this->lang->line('display_owns')); } else echo "Owns"; ?></b> <small><?php echo $userProfileDetails->row()->own_count;?></small></a></li>
            <li><a href="user/<?php echo $userProfileDetails->row()->user_name;?>/follows"><b><?php if($this->lang->line('display_follows') != '') { echo stripslashes($this->lang->line('display_follows')); } else echo "Follows"; ?></b> <small><?php echo $follow->num_rows();?></small></a></li>
          </ul>
          <div class="top-menu">
          </div>
              
     <?php 
        if ($recentActivityDetails->num_rows()>0){
        ?>
     
         
      
          <ul class="activity-list">
           <?php 
          if($this->lang->line('date_year') != '') 
          	$date_year = $this->lang->line('date_year');
          else
          	$date_year = "Year";
          
          if($this->lang->line('date_years') != '') 
          	$date_years = $this->lang->line('date_years');
          else
          	$date_years = "Years";
          
          if($this->lang->line('date_month') != '') 
          	$date_month = $this->lang->line('date_month');
          else
          	$date_month = "Month";
          
          if($this->lang->line('date_months') != '') 
          	$date_months = $this->lang->line('date_months');
          else
          	$date_months = "Months";
          
          if($this->lang->line('date_week') != '') 
          	$date_week = $this->lang->line('date_week');
          else
          	$date_week = "Week";
          
          if($this->lang->line('date_weeks') != '') 
          	$date_weeks = $this->lang->line('date_weeks');
          else
          	$date_weeks = "Weeks";
          
          if($this->lang->line('date_day') != '') 
          	$date_day = $this->lang->line('date_day');
          else
          	$date_day = "Day";
          
          if($this->lang->line('date_days') != '') 
          	$date_days = $this->lang->line('date_days');
          else
          	$date_days = "Days";
          
          if($this->lang->line('date_hour') != '') 
          	$date_hour = $this->lang->line('date_hour');
          else
          	$date_hour = "Hour";
          
          if($this->lang->line('date_hours') != '') 
          	$date_hours = $this->lang->line('date_hours');
          else
          	$date_hours = "Hours";
          
          if($this->lang->line('date_minute') != '') 
          	$date_minute = $this->lang->line('date_minute');
          else
          	$date_minute = "Minute";
          
          if($this->lang->line('date_minutes') != '') 
          	$date_minutes = $this->lang->line('date_minutes');
          else
          	$date_minutes = "Minutes";
          
          if($this->lang->line('date_second') != '') 
          	$date_second = $this->lang->line('date_second');
          else
          	$date_second = "Second";
          
          if($this->lang->line('date_seconds') != '') 
          	$date_seconds = $this->lang->line('date_seconds');
          else
          	$date_seconds = "Seconds";
          
          if($this->lang->line('ago') != '') 
          	$date_ago = $this->lang->line('ago');
          else
          	$date_ago = "ago";
          
          $date_txt_arr = array(
          	"Years",
          	"Year",
          	"Months",
          	"Month",
          	"Weeks",
          	"Week",
          	"Days",
          	"Day",
          	"Hours",
          	"Hour",
          	"Minutes",
          	"Minute",
          	"Seconds",
          	"Second",
          	"ago"
          );
          $date_lg_arr = array(
          	$date_years,
          	$date_year,
          	$date_months,
          	$date_month,
          	$date_weeks,
          	$date_week,
          	$date_days,
          	$date_day,
          	$date_hours,
          	$date_hour,
          	$date_minutes,
          	$date_minute,
          	$date_seconds,
          	$date_second,
          	$date_ago
          );
        //echo "<pre>"; print_r($recentActivityDetails->result());die;
          foreach ($recentActivityDetails->result() as $recentActivityDetailsRow){  
          	$activityTime = strtotime($recentActivityDetailsRow->activity_time);
          	//         	$actTime = humanTiming($activityTime) . " ago" ;
          	$actTime = timespan($activityTime).' ago';
          	$actTime = str_replace($date_txt_arr, $date_lg_arr, $actTime);
          	if ($recentActivityDetailsRow->activity_name == 'add_product'){ 
          		$icon = 'ic-fancy';
          		$actTxt = "add the product";
					if($recentActivityDetailsRow->web_link!=''){
						$link = 'user/'.$userProfileDetails->row()->user_name.'/things/'.$recentActivityDetailsRow->activity_id.'/'.url_title($recentActivityDetailsRow->user_product_name);
						$linkTxt = $recentActivityDetailsRow->user_product_name;
						$user_product_image=explode(",",$recentActivityDetailsRow->user_product_image);
					}else{
						$product_image=explode(",",$recentActivityDetailsRow->image);
						$link = 'things/'.$recentActivityDetailsRow->productID.'/'.url_title($recentActivityDetailsRow->product_name);
						$linkTxt = $recentActivityDetailsRow->product_name;
					}
          			
          			?>
          			<li><span class="icon <?php echo $icon;?>"></span> <?php echo $userProfileDetails->row()->user_name; ?>  <?php echo $actTxt;?> <a href="<?php echo $link;?>"><?php echo $linkTxt;?></a>.<small class="time"><?php echo $actTime;?></small> </br></br> <a href="<?php echo $link;?>"><img src="images/product/<?php echo $product_image[0];?>" style="height:200px;width:200px;margin-left:150px;"/></a></br></br><a href="<?php echo $link;?>" style="margin-left:150px;"><?php echo $linkTxt;?></a></li>
          		
        <?php   	}
          else if ($recentActivityDetailsRow->activity_name == 'fancy'){
          	
          		$icon = 'ic-fancy';
          		$actTxt = LIKED_BUTTON;
          		if ($recentActivityDetailsRow->product_name != ''){
          			$link = 'things/'.$recentActivityDetailsRow->productID.'/'.url_title($recentActivityDetailsRow->product_name);
          			$linkTxt = $recentActivityDetailsRow->product_name;
          		}else {
          			$link = 'user/'.$userProfileDetails->row()->user_name.'/things/'.$recentActivityDetailsRow->activity_id.'/'.url_title($recentActivityDetailsRow->user_product_name);
          			$linkTxt = $recentActivityDetailsRow->user_product_name;
          		}?>
          <li><span class="icon <?php echo $icon;?>"></span> <?php echo $userProfileDetails->row()->user_name; ?> <?php echo $actTxt;?> the product  <a href="<?php echo $link;?>"><?php echo $linkTxt;?></a>.<small class="time"><?php echo $actTime;?></small> <?php if( $product_image[0] != ""){?></br></br><a href="<?php echo $link;?>"><img src="images/product/<?php echo $product_image[0];?>" style="height:200px;width:200px;margin-left:150px;" /></a><?php }else{?></br></br><a href="<?php echo $link;?>"><img src="images/product/<?php echo $user_product_image[0];?>" style="height:200px;width:200px;margin-left:150px;" /></a><?php }?></br></br> <a href="<?php echo $link;?>" style="margin-left:150px;"><?php echo $linkTxt;?></a></li>
          
        <?php    	}else if ($recentActivityDetailsRow->activity_name == 'unfancy'){
          	
          		$icon = 'ic-fancy';
          		$actTxt = UNLIKE_BUTTON;
          		if ($recentActivityDetailsRow->product_name != ''){
          			$link = 'things/'.$recentActivityDetailsRow->productID.'/'.url_title($recentActivityDetailsRow->product_name);
          			$linkTxt = $recentActivityDetailsRow->product_name;
          		}else {
          			$link = 'user/'.$userProfileDetails->row()->user_name.'/things/'.$recentActivityDetailsRow->activity_id.'/'.url_title($recentActivityDetailsRow->user_product_name);
          			$linkTxt = $recentActivityDetailsRow->user_product_name;
          		}?>
          		
          		<li><span class="icon <?php echo $icon;?>"></span> <?php echo $userProfileDetails->row()->user_name; ?> <?php echo $actTxt;?> the product  <a href="<?php echo $link;?>"><?php echo $linkTxt;?></a>.<small class="time"><?php echo $actTime;?></small><?php if( $product_image[0] != ""){?></br></br><a href="<?php echo $link;?>"><img src="images/product/<?php echo $product_image[0];?>" style="height:200px;width:200px;margin-left:150px;" /></a><?php }else{?></br></br><a href="<?php echo $link;?>"><img src="images/product/<?php echo $user_product_image[0];?>" style="height:200px;width:200px;margin-left:150px;" /></a><?php }?><a href="<?php echo $link;?>" style="margin-left:150px;"><?php echo $linkTxt;?></a></li>
          		
         <?php  	}else if ($recentActivityDetailsRow->activity_name == 'follow'){
         	echo $userProfileDetails->row()->thumbnail;
          	
          		$icon = 'ic-follow';
          		if($this->lang->line('followed') != '')
          			$actTxt = $this->lang->line('followed');
          		else
          			$actTxt = 'Followed';
          		if ($recentActivityDetailsRow->user_name == ''){
          			$link = 'user/administrator';
          			$linkTxt = 'administrator';
          		}else { 
          			$link = 'user/seller-timeline/'.$recentActivityDetailsRow->user_name;
          			$linkTxt = $recentActivityDetailsRow->full_name;
          		}
				if($recentActivityDetailsRow->thumbnail!=''){
					
					$img = $recentActivityDetailsRow->thumbnail;
				}else{
					$img = "user-thumb1.png";
				}
				?>
          <li><span class="icon <?php echo $icon;?>"></span> <?php echo $userProfileDetails->row()->user_name; ?>  <?php echo $actTxt;?> the user  <a href="<?php echo $link;?>"><?php echo $linkTxt;?></a>.<small class="time"><?php echo $actTime;?></small>
		  </br></br>
		  <a href="<?php echo $link;?>"><img style=" height:200px;width:200px;margin-left:150px;" src="images/users/<?php echo $img; ?>"/></a></li>
		  
		  <a href="<?php echo $link;?>" style="margin-left:150px; padding-left:95px;"><?php echo $linkTxt;?></a></li>
          <?php }else if ($recentActivityDetailsRow->activity_name == 'unfollow'){
          	echo $userProfileDetails->row()->thumbnail;
          	
          		$icon = 'ic-follow';
          		if($this->lang->line('unfollowed') != '')
          			$actTxt = $this->lang->line('unfollowed');
          		else
          			$actTxt = 'Unfollowed';
          		if ($recentActivityDetailsRow->user_name == ''){
          			$link = 'user/administrator';
          			$linkTxt = 'administrator';
          		}else {
          			$link = 'user/'.$recentActivityDetailsRow->user_name;
          			$linkTxt = $recentActivityDetailsRow->full_name;
          		}
				if($recentActivityDetailsRow->thumbnail!=''){
					
				$img = $recentActivityDetailsRow->thumbnail;
				}else{
					$img = "user-thumb1.png";
				}
				?>
           <li>
			   <span class="icon <?php echo $icon;?>"> <?php echo $userProfileDetails->row()->user_name; ?> </span> 
			   <?php echo $actTxt;?> the user  <a href="<?php echo $link;?>"><?php echo $linkTxt;?></a>.
			   <small class="time"><?php echo $actTime;?></small>
			   </br></br>
		  <a href="<?php echo $link;?>"><img style=" height:200px;width:200px;margin-left:150px;" src="images/users/<?php echo $img; ?>"/></a></li>
		  
		  <a href="<?php echo $link;?>" style="margin-left:150px; padding-left:95px;"><?php echo $linkTxt;?></a></li>
		   </li>	
		   <?php } } ?>
          </ul></div>
        </div>
        <?php }else{?>
        <div id="content">
        <div class="wrapper timeline normal" >
      
         <ul class="activity-list">
        
         <li>
         <?php echo("Recent Activities not available.");?>
       </li>  </ul></div></div>
        	
       <?php  }?>
         
        <aside id="sidebar">
        <div class="wrapper user-cover">
        <?php //echo "<pre>"; print_r($userProfileDetails->result());die;
        $userImg = 'user-thumb1.png';
        if ($userProfileDetails->row()->thumbnail != ''){
	        $userImg = $userProfileDetails->row()->thumbnail;
        }
        $followText = 'Follow';
        if($this->lang->line('onboarding_follow') != '') {
	        $followText = $this->lang->line('onboarding_follow');
        }
        $followClass = 'follow';
        if ($loginCheck != ''){
	        $followingListArr = explode(',', $userDetails->row()->following);
	        if (in_array($userProfileDetails->row()->id, $followingListArr)){
	        	$followClass = 'following';
	        	$followText = 'Following';
		        if($this->lang->line('display_following') != '') {
			        $followText = $this->lang->line('display_following');
		        }
	        }
        } 
        ?>
          <div class="profile"> 
          <span class="avatar">
          <span id="user-photo-container">
          <img src="images/users/<?php echo $userImg;?>" />
          <?php if ($loginCheck != '' && $userDetails->row()->id == $userProfileDetails->row()->id){?>
           	<a title="<?php if($this->lang->line('display_edit_img') != '') { echo stripslashes($this->lang->line('display_edit_img')); } else echo "Edit Profile Image"; ?>" style="display: none;" class="btn-edit tooltip" onclick="$.dialog('change-photo').open();return false;" href="#">
          	<i class="ic-pen"></i>
          	<span><?php if($this->lang->line('display_edit_img') != '') { echo stripslashes($this->lang->line('display_edit_img')); } else echo "Edit Profile Image"; ?><b></b></span>
          	</a>
          <?php }?>	
          </span>
          </span>
          <?php 
          $web_url = $userProfileDetails->row()->web_url;
          if (substr($web_url, 0,4)!='http') $web_url = 'http://'.$web_url;
          ?>
            <p class="username"><?php echo $userProfileDetails->row()->full_name;?></p>
            <p class="location"><?php echo $userProfileDetails->row()->location;?>&nbsp;.&nbsp;<a href="<?php echo $web_url;?>" target="_blank"><?php echo $userProfileDetails->row()->web_url;?></a></p>
            <p class="bio"><?php echo $userProfileDetails->row()->about;?></p>
          </div>
          <ul class="sns-list">
          <?php 
          $fb_link = $userProfileDetails->row()->facebook;
          if (substr($fb_link, 0,4)!='http') $fb_link='http://'.$fb_link;
          $tw_link = $userProfileDetails->row()->twitter;
          if (substr($tw_link, 0,4)!='http') $tw_link='http://'.$tw_link;
          $go_link = $userProfileDetails->row()->google;
          if (substr($go_link, 0,4)!='http') $go_link='http://'.$go_link;
          ?>
            <li><a href="<?php echo $fb_link;?>" target="_blank"><span class="icon ic-fb"></span> Facebook</a></li>
            <li><a href="<?php echo $tw_link;?>" target="_blank"><span class="icon ic-tw"></span> Twitter</a></li>
            <li><a href="<?php echo $go_link;?>" target="_blank"><span class="icon ic-gg"></span> Twitter</a></li>
            <?php 
            if (($userProfileDetails->row()->is_verified == 'Yes') && ($userProfileDetails->row()->group == 'Seller'))
            {
            ?>	
            <li > <span class="icon ic-vd"></span> Verified</li>
            <?php 
            }
            ?>
          </ul>
          <ul class="followers">
            <li><a href="user/<?php echo $userProfileDetails->row()->user_name;?>/followers" ><b><?php echo $userProfileDetails->row()->followers_count;?></b> <small><?php if($this->lang->line('display_followers') != '') { echo stripslashes($this->lang->line('display_followers')); } else echo "Followers"; ?></small></a></li>
            <li><a href="user/<?php echo $userProfileDetails->row()->user_name;?>/following" ><b><?php echo $userProfileDetails->row()->following_count;?></b> <small><?php if($this->lang->line('display_following') != '') { echo stripslashes($this->lang->line('display_following')); } else echo "Following"; ?></small></a></li>
          </ul>
          <div class="user-control">
          <?php if ($loginCheck == $userProfileDetails->row()->id){?>
            <div class="relation"> <a href="settings" class="edit-profile btn"><?php if($this->lang->line('display_edit_prof') != '') { echo stripslashes($this->lang->line('display_edit_prof')); } else echo "Edit Profile"; ?></a> </div>
          <?php }else {?>
            <div class="relation"> <a href="#" uid="<?php echo $userProfileDetails->row()->id;?>" <?php if ($loginCheck==''){?>require_login="true"<?php }?> class="follow-user-link follow-link <?php echo $followClass;?>"><i class="icon"></i><?php echo $followText; ?></a> </div>
          <?php }?>
            <div class="setting">
              <div class="trick" onClick="$(this).parents('.setting').removeClass('opened');"></div>
              <a href="#" class="btns-gray-embo btn-setting" onClick="$(this).parents('.setting').toggleClass('opened');return false;"><i class="icon ic-cok"></i></a>
              <div class="menu-content">
                <ul>
                    <li><a href="<?php echo base_url();?>site/feed/user/<?php echo $userProfileDetails->row()->user_name;?>" target="_blank"><?php if($this->lang->line('display_rss_feed') != '') { echo stripslashes($this->lang->line('display_rss_feed')); } else echo "RSS Feed"; ?></a></li> 
                   <li><a href="<?php echo base_url();?>user/<?php echo $userProfileDetails->row()->user_name;?>" username="<?php echo $userProfileDetails->row()->user_name;?>" txt="Check out <?php echo $userProfileDetails->row()->full_name;?>'s profile!" <?php if ($loginCheck == ''){?>require_login="true"<?php }?> class="btn-user-share"><?php if($this->lang->line('display_share_prof') != '') { echo stripslashes($this->lang->line('display_share_prof')); } else echo "Share Profile"; ?></a></li>
                </ul>
              </div>
            </div>
          <?php if ($loginCheck == $userProfileDetails->row()->id && $userProfileDetails->row()->store_payment != 'Paid'){?>
            <div class="setting" style="margin-top: 10px;"> <a href="open-store" class="edit-profile btn"><?php if($this->lang->line('display_open_store') != '') { echo stripslashes($this->lang->line('display_open_store')); } else echo "Open Store"; ?></a> </div>
          <?php } ?>            
          </div>
        </div>
   
      </aside></div></div></div></div>
      
<?php
$this->load->view('site/templates/footer');
?>