<?php 
$this->load->view('site/templates/header.php');
?>
<?php if($flash_data != '') { ?>
	<div class="errorContainer" id="<?php echo $flash_data_type;?>">
		<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
		<p><span><?php echo $flash_data;?></span></p>
	</div>
<?php } ?>
<div id="content" >
	<div class="account-wrap">
		<div class="account-page">
       
        <dl class="profile-account">
        <dd>
       	<h2><?php if($this->lang->line('forgot_passsword') != '') { echo stripslashes($this->lang->line('forgot_passsword')); } else echo "Forgot Password"; ?></h2><br />
    
        
        <dl class="profile-account">
        <dd>
        <form method="post" action="site/user/forgot_password_user" class="frm clearfix"><input type='hidden' />
		
		   <?php if($this->lang->line('forgot_enter_email') != '') { echo stripslashes($this->lang->line('forgot_enter_email')); } else echo "Forgot your password? Enter your email address to reset it."; ?>
			<p class="name-account"><label class="label"><?php if($this->lang->line('signup_emailaddrs') != '') { echo stripslashes($this->lang->line('signup_emailaddrs')); } else echo "Email Address"; ?></label>
			<input type="text" id="username" name="email" placeholder="" class="popup_add_input"/></p><br /><br />
			<input class="next_url" type="hidden" name="next" value="/"/>
                    
		    
		    <p class="btn-area"><button type="submit" class="btns-blue-embo btn-signin btn-blue"><?php if($this->lang->line('forgot_reset_pwd') != '') { echo stripslashes($this->lang->line('forgot_reset_pwd')); } else echo "Reset Password"; ?></button>
		</fieldset>
            </form>
        </dd>
        </dl>
        
        </div></div></div>