<?php
$this->load->view('site/templates/header');
?>

<div class="lang-en wider no-subnav thing signed-out winOS">
    <div id="container-wrapper">
		<div class="container ">
			<?php if($flash_data != '') { ?>
			<div class="errorContainer" id="<?php echo $flash_data_type;?>">
				<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
				<p><span><?php echo $flash_data;?></span></p>
			</div>
			<?php } ?>

			<div class="wrapper-content">					
				<div class="profile-list">            
					<div class="page-header padding_all15 margin_all0">
						<h2> <?php if($this->lang->line('Inbox') != '') { echo stripslashes($this->lang->line('Inbox')); } else echo "Inbox"; ?></h2>
						<h2 style="text-align:left;" class="border_bottom padding_bottom15">	</h2>		 
					</div>
					<div class="box-content">
						<form accept-charset="utf-8" method="post" action="#" id="sellerProdEdit1">
							<section class="left-section min_height" style="height: 924px;">	
								<div class="person-lists bs-docs-example">
									<div class="dun-data" id="dun_requests">
										<ul class="bids_ul auction_ul">
											<?php 
												$c_time  = date('Y-m-d H:i:s', time());
												$currnt_time = strtotime( $c_time.' + 3 minute');
												$current_time  = date('Y-m-d H:i:s', $currnt_time);
												if($auctions->num_rows() > 0){
												foreach($auctions->result() as $row){ //echo $row->read_status;?>
											
												<li>
													<a  href='auction/<?php echo $row->id; ?>'>
														<div class="<?php if($row->read_status == 'unread'){ echo 'unread'; } ?> productname">
															<img class="auctions-img" style=" width: 58%;" src="<?php echo DESKTOPURL;?>images/product/<?php $img = explode(',', $row->image)[0]; if($img == ''){ $img = 'default-product.jpg'; }  echo $img; ?>"> 
															<span class="pdtc_name" ><?php echo ucfirst($row->product_name); ?></span>
														</div>
														<div class="<?php if($row->read_status == 'unread'){ echo 'unread'; } ?> productprice">Price: <?php  echo $row->total.' '.$currencySymbol; ?></div>
														
														<?php if($row->do_capture == 'Pending'){ ?>
														
																<?php if($row->end_date > $current_time ){ ?>
																<span class="auctions_countdown <?php if($row->read_status == 'unread'){ echo 'unread'; } ?>" data-countdown="<?php echo $row->end_date; ?>" ></span>
																<?php }else{ ?>
																<span class="auctions_countdown <?php if($row->read_status == 'unread'){ echo 'unread'; } ?>">Expired</span>
																<?php } ?>
																
														<?php }else{ ?>
														
															<span class="auctions_countdown ">Offer Accepted</span>
															
														<?php } ?>
														<div class="<?php if($row->read_status == 'unread'){ echo 'unread'; } ?> view" > View</div>
													</a>
												</li>
											<?php }
												}else{ ?>
													<li>No Auctions yet</li>
											<?php	}
											?>
										</ul>
									</div>
								</div>
							</section>
						</form>
					</div>
						
				</div>
			</div>
		</div>
		       
	</div>
</div>

	<!-- COUNTDOWN -->
<script type="text/javascript" src="js/assets/lib/countdown/jquery.plugin.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/site/jquery.countdown.js"></script>

<script type="text/javascript">
    $(document).ready(function(e) {
        $('[data-countdown]').each(function() {
            var day  = "Day";
            var days = "Days";
            var $this = $(this), finalDate = $(this).data('countdown');
            $this.countdown(finalDate, function(event) {
                if( event.strftime('%D') != 00 && event.strftime('%D') == 01 ){
                    $this.html(event.strftime('%D '+day+' %H:%M:%S'));
                }
                else if( event.strftime('%D') > 01 ){
                    $this.html(event.strftime('%D '+days+' %H:%M:%S'));
                }
                else{
                    $this.html(event.strftime('%H:%M:%S'));
                }
           });
        });
       
    });
</script>

<style>
.unread{
	color: red !important;
}
.view{
	float: left;
    margin-left: 30%;
}

.productname{
	float: left;
	width: 20%;
}
.productprice{
	float: left;
	margin-left: 30%;
}
</style>