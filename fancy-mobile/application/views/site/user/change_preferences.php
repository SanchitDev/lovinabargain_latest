<?php
$this->load->view('site/templates/header');
?>
<div id="content" class="account-page">
	<div class="account-wrap">
	<form method="post" action="site/user_settings/update_preferences" class="myform">
        <dl class="profile-account">
			<dt class="tit-profile"><?php echo site_lg('lg_preferences', 'Preferences');?></dt>
			<dd>
				<p class="years">
					<label class="label"><?php echo site_lg('lg_language', 'Language');?></label>
					<select data-langcode="en" id="lang" name="language">
					<?php 
						$selectedLangCode = $userDetails->row()->language;
						if ($selectedLangCode == ''){
							$selectedLangCode = 'en';
						}
						if (count($activeLgs)>0){
							foreach ($activeLgs as $activeLgsRow){
						?>	
						<option value="<?php echo $activeLgsRow['lang_code'];?>" <?php if ($selectedLangCode == $activeLgsRow['lang_code']){echo 'selected="selected"';}?>><?php echo $activeLgsRow['name'];?></option>
						<?php }}?>	
					</select>
					<small><?php if($this->lang->line('prference_lang_not') != '') { echo stripslashes($this->lang->line('prference_lang_not')); } else echo "Can't find your language on"; ?> <?php echo $siteTitle;?>? <a href="mailto:<?php echo $siteContactMail;?>"><?php if($this->lang->line('prference_letus') != '') { echo stripslashes($this->lang->line('prference_letus')); } else echo "Let us know"; ?></a>.</small>
				</p>

			</dd>
        </dl>
        <dl class="profile-account">
			<dd>
				<p class="effete">
					<label class="label"><?php if($this->lang->line('prference_prof_visible') != '') { echo stripslashes($this->lang->line('prference_prof_visible')); } else echo "Profile visibility"; ?></label>
					<small style=" padding-bottom: 1.22em;"><?php if($this->lang->line('prference_mange_acti') != '') { echo stripslashes($this->lang->line('prference_mange_acti')); } else echo "Manage who can see your activity, things you"; ?> <?php echo LIKE_BUTTON;?>, <?php if($this->lang->line('prference_search_res') != '') { echo stripslashes($this->lang->line('prference_search_res')); } else echo "your followers, people you follow or in anyone's search results."; ?></small><br /><br />
					<input type="radio" <?php if ($userDetails->row()->visibility == "Everyone"){echo 'checked="checked"';}?> value="Everyone" id="visibility1" name="visibility">
					<label style="margin:0 10px 0 0" class="radio-button" for="visibility1"><?php if($this->lang->line('prference_everyone') != '') { echo stripslashes($this->lang->line('prference_everyone')); } else echo "Everyone"; ?></label>
					<input type="radio" <?php if ($userDetails->row()->visibility == "Only you"){echo 'checked="checked"';}?> value="Only you" id="visibility2" name="visibility">
					<label class="radio-button" for="visibility2"><i class="personal-lock"></i><?php if($this->lang->line('prference_onlyu') != '') { echo stripslashes($this->lang->line('prference_onlyu')); } else echo "Only you"; ?></label>
				</p>
			</dd>
        </dl>
        <dl class="profile-account">
			<dd>
				<p class="effete">
					<label class="label"><?php if($this->lang->line('prference_disp_list') != '') { echo stripslashes($this->lang->line('prference_disp_list')); } else echo "Display lists"; ?></label>
					<small style=" padding-bottom: 1.22em;"><?php if($this->lang->line('prference_show_list') != '') { echo stripslashes($this->lang->line('prference_show_list')); } else echo "Show list options for organizing your things when you"; ?> <?php echo LIKE_BUTTON;?> <?php if($this->lang->line('prference_something') != '') { echo stripslashes($this->lang->line('prference_something')); } else echo "something."; ?></small><br /><br />
					<input type="radio" <?php if ($userDetails->row()->display_lists == "Yes"){echo 'checked="checked"';}?> value="Yes" id="display1" name="display_lists">
					<label style="margin:0 10px 0 0" class="radio-button" for="engines1"><?php if($this->lang->line('prference_yes') != '') { echo stripslashes($this->lang->line('prference_yes')); } else echo "Yes"; ?></label>
					<input type="radio" <?php if ($userDetails->row()->display_lists == "No"){echo 'checked="checked"';}?> value="No" id="display2" name="display_lists">
					<label class="radio-button" for="engines2"><?php if($this->lang->line('prference_no') != '') { echo stripslashes($this->lang->line('prference_no')); } else echo "No"; ?></label>
				</p>
			</dd>
        </dl>
        <dl class="profile-account">
			<dd style=" padding: 1em;">
				<button style="width:100%" class="profile-button"><?php if($this->lang->line('prference_save_prefer') != '') { echo stripslashes($this->lang->line('prference_save_prefer')); } else echo "Save Preferences"; ?></button>
			</dd>
        </dl>
	</form>
	</div>
</div>
<script>
	jQuery(function($) {
		var $select = $('.gift-recommend select.select-round');
		$select.selectBox();
		$select.each(function(){
			var $this = $(this);
			if($this.css('display') != 'none') $this.css('visibility', 'visible');
		});
	});
</script>