<?php $this->load->view('site/templates/header',$this->data);?>
<div id="content" class="account-page">
	<?php if($flash_data != '') { ?>
		<div class="errorContainer" id="<?php echo $flash_data_type;?>">
			<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
			<p><span><?php echo $flash_data;?></span></p>
		</div>
	<?php } ?>
	<div class="account-wrap">
		<form id="seller_signup" onsubmit="return validateSellerSignup();" method="post" action="site/user/seller_signup" novalidate="novalidate">
			<div class="error-box" style="display:none;">
				<p><?php if($this->lang->line('seller_some_requi') != '') { echo stripslashes($this->lang->line('seller_some_requi')); } else echo "Some required information is missing or incomplete. Please correct your entries and try again"; ?>.</p>
				<ul></ul>
			</div>
			<dl class="profile-account">
				<dt class="tit-profile"><?php if($this->lang->line('seller_merc_info') != '') { echo stripslashes($this->lang->line('seller_merc_info')); } else echo "Merchant Information"; ?></dt>
				<dd>
					<p class="name-account">
						<label class="label"><?php if($this->lang->line('seller_brand') != '') { echo stripslashes($this->lang->line('seller_brand')); } else echo "Brand"; ?><sup style="color: red;">*</sup></label>
						<input type="text" id="brand_name" value="" name="brand_name">
					</p>
					<p class="userid">
						<label class="label"><?php if($this->lang->line('header_description') != '') { echo stripslashes($this->lang->line('header_description')); } else echo "Description"; ?><sup style="color: red;">*</sup></label>
						<input type="text"  id="brand_description" value="" name="brand_description">
					</p>
					<p class="weblink">
						<label class="label"><?php if($this->lang->line('seller_web_link') != '') { echo stripslashes($this->lang->line('seller_web_link')); } else echo "Website Link"; ?></label>
						<input type="text" id="web_url" value="" name="web_url">
					</p>
					<p class="places">
						<label class="label"><?php if($this->lang->line('header_addrs_one') != '') { echo stripslashes($this->lang->line('header_addrs_one')); } else echo "Address Line 1"; ?><sup style="color: red;">*</sup></label>
						<input type="text" id="s_address" value="" name="s_address">
					</p>
					<p class="twit">
						<label class="label"><?php if($this->lang->line('header_city') != '') { echo stripslashes($this->lang->line('header_city')); } else echo "City"; ?><sup style="color: red;">*</sup></label>
						<input type="text" id="s_city" value="" name="s_city">
					</p>
					<p class="twit">
						<label class="label"><?php if($this->lang->line('checkout_state') != '') { echo stripslashes($this->lang->line('checkout_state')); } else echo "State"; ?><sup style="color: red;">*</sup></label>
						<input type="text" id="s_state" value="" name="s_state">
					</p>
					<p class="twit">
						<label class="label"><?php if($this->lang->line('seller_postal_code') != '') { echo stripslashes($this->lang->line('seller_postal_code')); } else echo "Postal Code"; ?><sup style="color: red;">*</sup></label>
						<input type="text" id="s_postal_code" value="" name="s_postal_code">
					</p>
					<p class="twit">
						<label class="label"><?php if($this->lang->line('header_country') != '') { echo stripslashes($this->lang->line('header_country')); } else echo "Country"; ?><sup style="color: red;">*</sup></label>
						<?php if(isset($countryList) && $countryList->num_rows()>0){ ?>
						<select name="s_country" class="select-white select-country" id="s_country">
							<?php foreach ($countryList->result() as $country){ ?>
							<option value="<?php echo $country->country_code;?>"><?php echo $country->name;?></option>
							<?php }?>						
						</select>
						<?php }else {?>
							<input type="text" id="s_country" value="" name="s_country">
						<?php }?>
					</p>
					<p class="twit">
						<label class="label"><?php if($this->lang->line('checkout_phone_no') != '') { echo stripslashes($this->lang->line('checkout_phone_no')); } else echo "Phone Number"; ?><sup style="color: red;">*</sup></label>
						<input type="text" id="s_phone_no" value="" name="s_phone_no">
					</p>
				</dd>
			</dl>
			<dl class="profile-account">
				<dt class="tit-profile"><?php if($this->lang->line('seller_bank_info') != '') { echo stripslashes($this->lang->line('seller_bank_info')); } else echo "Bank Information"; ?></dt>
				<dd>
					<p class="twit">
						<label class="label"><?php if($this->lang->line('signup_full_name') != '') { echo stripslashes($this->lang->line('signup_full_name')); } else echo "Full Name"; ?><sup style="color: red;">*</sup></label>
						<input type="text" id="bank_name" value="" name="bank_name">
					</p>
					<p class="twit">
						<label class="label"><?php if($this->lang->line('seller_acc_num') != '') { echo stripslashes($this->lang->line('seller_acc_num')); } else echo "Account Number"; ?><sup style="color: red;">*</sup></label>
						<input type="text" id="bank_no" value="" name="bank_no">
					</p>
					<p class="twit">
						<label class="label"><?php if($this->lang->line('seller_bank_code') != '') { echo stripslashes($this->lang->line('seller_bank_code')); } else echo "Bank Code"; ?><sup style="color: red;">*</sup></label>
						<input type="text" id="bank_code" value="" name="bank_code">
					</p>
					<p class="twit">
						<label class="label"><?php if($this->lang->line('Paypal Email') != '') { echo stripslashes($this->lang->line('Paypal Email')); } else echo "Paypal Email"; ?></label>
						<input type="text" id="paypal_email" value="" name="paypal_email">
					</p>
				</dd>
			</dl>
			</br>
			</br>
			</br>
			<div class="btton-container">
				<div class="btn-list">
				<button id="sign-up" class="profile-button">Complete Sign Up</button>
				</div>
			</div>
		</form>
		<a href="#header" id="scroll-to-top"><span><?php if($this->lang->line('signup_jump_top') != '') { echo stripslashes($this->lang->line('signup_jump_top')); } else echo "Jump to top"; ?></span></a>
	</div>
</div>
<script type="text/javascript">
function validateSellerSignup(){
	var brand = $('#brand_name').val();
	var description = $('#brand_description').val();
	var addr = $('#s_address').val();
	var city = $('#s_city').val();
	var state = $('#s_state').val();
	var pincode = $('#s_postal_code').val();
	var country = $('#s_country').val();
	var phone = $('#s_phone_no').val();
	var bank_name = $('#bank_name').val();
	var bank_no = $('#bank_no').val();
	var bank_code = $('#bank_code').val();
	if(brand == ''){
		alert('Brand name required');
		$('#brand_name').focus();
		return false;
	}else if(description == ''){
		alert('Description required');
		$('#brand_description').focus();
		return false;
	}else if(addr == ''){
		alert('Adrress line 1 required');
		$('#s_address').focus();
		return false;
	}else if(city == ''){
		alert('City name required');
		$('#s_city').focus();
		return false;
	}else if(state == ''){
		alert('State name required');
		$('#s_state').focus();
		return false;
	}else if(pincode == ''){
		alert('Postal code required');
		$('#s_postal_code').focus();
		return false;
	}else if(country == ''){
		alert('Country name required');
		$('#s_country').focus();
		return false;
	}else if(phone == ''){
		alert('Phone number required');
		$('#s_phone_no').focus();
		return false;
	}else if(bank_name == ''){
		alert('Name in bank required');
		$('#bank_name').focus();
		return false;
	}else if(bank_no == ''){
		alert('Account number required');
		$('#bank_no').focus();
		return false;
	}else if(bank_code == ''){
		alert('Bank code required');
		$('#bank_code').focus();
		return false;
	}
}
</script>