<?php 
$this->load->view('site/templates/header');
session_start();
?>
<?php if (is_file('google-login-mats/index.php')){
	require_once 'google-login-mats/index.php';
}?>
	<!-- / header -->
      <div id="content">
        	<div id="c" class="wrap sign">
            	<h2 class="tit"><?php echo site_lg('lg_sign_fancy', 'Sign in to Fancy');?></h2>
				<dl class="sign-sns">
					<dt><?php echo site_lg('signup_connect_network', 'Connect with a social network');?></dt>
					<dd>
						<ul>
						<?php if ($this->config->item('facebook_app_id') != '' && $this->config->item('facebook_app_secret') != ''){?> 
							<li><button class="btn-f sns-f" onclick="window.location.href='<?php echo base_url().'facebook/user.php'; ?>'"><i class="icon"></i><?php echo site_lg('lg_facebook', 'Facebook');?></button></li>
						<?php } ?>
						<?php if ($this->config->item('google_client_secret') != '' && $this->config->item('google_client_id') != '' && $this->config->item('google_redirect_url') != '' && $this->config->item('google_developer_key') != '' && is_file('google-login-mats/index.php')){?> 
							<li><button id="fancyy-g-signin" onClick="window.location.href='<?php echo $authUrl; ?>'" class="btn-g sns-g" next="/"><i class="icon"></i><?php echo site_lg('lg_google', 'Google');?></button></li>
						<?php } ?>
						<?php if ($this->config->item('consumer_key') != '' && $this->config->item('consumer_secret') != ''){ ?> 
							<li><button class="btn-t sns-t" onClick="window.location.href='<?php echo base_url();?>twtest/redirect'"><i class="icon"></i><?php echo site_lg('lg_twitter', 'Twitter');?></button></li> 
						<?php } ?>
						</ul>
					</dd>
				</dl>
				<dl class="signin-frm" style="border-top:1px solid #E2E5E7;">
					<dt><?php echo site_lg('lg_sign_email', 'Or sign in with your email address');?></dt>
					<?php if (validation_errors() != ''){?>
						<div id="validationErr">
							<script>setTimeout("hideErrDiv('validationErr')", 3000);</script>
							<p><?php echo validation_errors();?></p>
						</div>
					<?php }?>
					<?php if($flash_data != '') { ?>
					<div class="errorContainer" id="<?php echo $flash_data_type;?>">
						<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
						<p><span><?php echo $flash_data;?></span></p>
					</div>
					<?php } ?>
					<form class="frm clearfix" action="site/user/login_user" method="post">
						<dd class="frm">
							<p>
								<label class="label"><?php echo site_lg('signup_emailaddrs', 'Email Address');?></label>
								<input type="text" placeholder="Username" name="email" />
							</p>
							<p>
								<label class="label"><?php echo site_lg('lg_password', 'Password');?></label>
								<input type="password" placeholder="Password" name="password" />
								<input class="next_url" type="hidden" value="<?php echo $next;?>" name="next">
							</p>
							<button class="btn-blue btn-signin btn-sign" type="submit"><?php echo site_lg('signup_sign_in', 'Sign in');?></button>
							<a class="go-pw" href="forgot-password"><?php echo site_lg('lg_forget', 'Forgot Password?');?></a>
						</dd>
					</form>
				</dl>
            </div>            
        </div>
    </div>
</div>

</body>
</html>

