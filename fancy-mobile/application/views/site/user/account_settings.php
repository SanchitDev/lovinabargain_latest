<?php $this->load->view('site/templates/header');
$userImg = 'images/users/user-thumb1.png';
if($userDetails->row()->thumbnail != ''){
	$userImg = DESKTOPURL.'images/users/'.$userDetails->row()->thumbnail;
}
?>
<script type="text/javascript">
function readURL(input){
	
	if(input.files && input.files[0]){
	var reader = new FileReader();
	reader.onload = function(e){
		$('#img_prev').attr('src',e.target.result).width(131).height(132);
		$('#img_prev').css('display','block');
	};
		reader.readAsDataURL(input.files[0]);
	}
}
</script>
	<div id="content" class="account-page">
        <div class="account-wrap">
		<form class="myform" id="profile_settings_form" method="post" action="site/user_settings/update_profile" enctype="multipart/form-data">
			<dl class="profile-account">
				<dt class="tit-profile"><?php if($this->lang->line('referrals_profile') != '') { echo stripslashes($this->lang->line('referrals_profile')); } else echo "Profile"; ?></dt>
				<dd><p class="name-account"><label class="label"><?php if($this->lang->line('signup_full_name') != '') { echo stripslashes($this->lang->line('signup_full_name')); } else echo "Full Name"; ?></label>
						<input id="name" class="setting_fullname" type="text" value="<?php echo $userDetails->row()->full_name;?>" autocapitalize="off" autocorrect="off" name="full_name">
						<small><?php if($this->lang->line('settings_realname') != '') { echo stripslashes($this->lang->line('settings_realname')); } else echo "Your real name, so your friends can find you."; ?></small>
					</p>
					<p class="userid"><label class="label"><?php if($this->lang->line('signup_user_name') != '') { echo stripslashes($this->lang->line('signup_user_name')); } else echo "Username"; ?></label>
						<input type="text" disabled="disabled" value="<?php echo $userDetails->row()->user_name;?>" name="username">
						<small><?php echo base_url();?>user/<?php echo $userDetails->row()->user_name;?></small>
					</p>
					<p class="weblink"><label class="label"><?php if($this->lang->line('settings_website') != '') { echo stripslashes($this->lang->line('settings_website')); } else echo "Website"; ?></label>
						<input type="text" value="<?php echo $userDetails->row()->web_url;?>" autocapitalize="off" autocorrect="off" id="site" class="setting_website" name="web_url">
					</p>
					<p class="places"><label class="label"><?php if($this->lang->line('settings_location') != '') { echo stripslashes($this->lang->line('settings_location')); } else echo "Location"; ?></label>
						<input type="text" value="<?php echo $userDetails->row()->location;?>" autocapitalize="off" autocorrect="off" id="loc" class="setting_location" name="location">
					</p>
					<p class="twit"><label class="label"><?php if($this->lang->line('signup_twitter') != '') { echo stripslashes($this->lang->line('signup_twitter')); } else echo "Twitter"; ?></label>
						<input type="text" value="<?php echo $userDetails->row()->twitter;?>" autocapitalize="off" autocorrect="off" id="twitter" class="setting_twitter" name="twitter">
					</p>
					<p class="twit"><label class="label"><?php if($this->lang->line('signup_facebook') != '') { echo stripslashes($this->lang->line('signup_facebook')); } else echo "Facebook"; ?></label>
						<input type="text" value="<?php echo $userDetails->row()->facebook;?>" autocapitalize="off" autocorrect="off" id="facebook" class="setting_facebook" name="facebook">
					</p>
					<p class="twit"><label class="label"><?php if($this->lang->line('signup_google') != '') { echo stripslashes($this->lang->line('signup_google')); } else echo "Google"; ?>+</label>
						<input type="text" value="<?php echo $userDetails->row()->google;?>" autocapitalize="off" autocorrect="off" id="google" class="setting_google" name="google">
					</p>
					<p class="abo"><label class="label"><?php if($this->lang->line('settings_about') != '') { echo stripslashes($this->lang->line('settings_about')); } else echo "About"; ?></label>
						<textarea id="bio" name="about" max-length="180"><?php echo $userDetails->row()->about;?></textarea>
						<small><?php if($this->lang->line('settings_write_yours') != '') { echo stripslashes($this->lang->line('settings_write_yours')); } else echo "Write something about yourself."; ?></small>
					</p>
				</dd>
			</dl>
			<dl class="profile-account">
				<dt class="tit-profile"><?php if($this->lang->line('referrals_account') != '') { echo stripslashes($this->lang->line('referrals_account')); } else echo "Account"; ?></dt>
				<dd>
					<p class="email-link"><label class="label"><?php if($this->lang->line('referrals_email') != '') { echo stripslashes($this->lang->line('referrals_email')); } else echo "Email"; ?></label>
						<input type="text" id="email" data-email="<?php echo $userDetails->row()->email;?>" value="<?php echo $userDetails->row()->email;?>" autocapitalize="off" autocorrect="off" name="email">
						<input id="user_email" value="<?php echo $userDetails->row()->email;?>" type="hidden">
					</p>
					<p class="years"><label class="label"><?php if($this->lang->line('settings_age') != '') { echo stripslashes($this->lang->line('settings_age')); } else echo "Age"; ?></label>
						<select id="age" name="age">
							<option selected="selected" value=""><?php if($this->lang->line('settings_rather') != '') { echo stripslashes($this->lang->line('settings_rather')); } else echo "I'd rather not say"; ?></option>
							<option <?php if ($userDetails->row()->age == '13 to 17'){echo 'selected="selected"';}?> value="13 to 17">13 to 17</option>
							<option <?php if ($userDetails->row()->age == '18 to 24'){echo 'selected="selected"';}?> value="18 to 24">18 to 24</option>
							<option <?php if ($userDetails->row()->age == '25 to 34'){echo 'selected="selected"';}?> value="25 to 34">25 to 34</option>
							<option <?php if ($userDetails->row()->age == '35 to 44'){echo 'selected="selected"';}?> value="35 to 44">35 to 44</option>
							<option <?php if ($userDetails->row()->age == '45 to 54'){echo 'selected="selected"';}?> value="45 to 54">45 to 54</option>
							<option <?php if ($userDetails->row()->age == '55+'){echo 'selected="selected"';}?> value="55+">55+</option>
						</select>
					</p>
					<p class="effete"><label class="label"><?php echo site_lg('lg_gender', 'Gender');?></label>
						<input type="radio" value="male" name="gender" <?php if ($userDetails->row()->gender=='Male'){echo 'checked="checked"';}?> value="Male" id="gender1">
						<label class="radio-button" for="gender1"><?php if($this->lang->line('settings_male') != '') { echo stripslashes($this->lang->line('settings_male')); } else echo "Male"; ?></label>
						<input type="radio" value="female" name="gender" <?php if ($userDetails->row()->gender=='Female'){echo 'checked="checked"';}?> value="Female" id="gender2">
						<label class="radio-button" for="woman"><?php if($this->lang->line('settings_female') != '') { echo stripslashes($this->lang->line('settings_female')); } else echo "Female"; ?></label>
						<input type="radio" value="none" name="gender" <?php if ($userDetails->row()->gender=='Unspecified'){echo 'checked="checked"';}?> value="Unspecified" id="gender3">
						<label class="radio-button" for="gender3"><?php if($this->lang->line('settings_unspecified') != '') { echo stripslashes($this->lang->line('settings_unspecified')); } else echo "Unspecified"; ?></label>
					</p>
				</dd>
			</dl>
			<dl class="profile-frm">
				<dt class="tit"><?php echo site_lg('lg_photo', 'Photo');?></dt>
				<dd  style="margin-bottom: 69px;">
					<div class="user-image">
						<input type="file" class="hidden" name="upload-file" id="recipient_image" onchange="readURL(this);"/>
						<img id="img_prev" src="<?php echo $userImg;?>" alt="" style="margin-top:-150px;"/>
					</div>
					<small class="profile-info"><?php echo site_lg('lg_pick_good', 'Your profile photo is your identity on Fancy, so pick a good one that expresses who you are.');?></small>
				</dd>
			</dl>
			<div class="btton-container">
				<div class="btn-list">
					<button class="profile-button" type="submit"><?php echo site_lg('settings_save_profile', 'Save Profile');?></button>
				</div>
			</div>
			</form>
		</div>
	</div>
<style>
.user-image{background: none repeat scroll 0 0 #FFFFFF; border: 1px solid #E5E5E5; position: relative; width: 131px; height:130px;  margin: 0 auto;}
.user-image .hidden {display: block !important; height: 150px; opacity: 0; position: relative; width: 100%; z-index: 1;}
.user-image #img_prev{width:131px; height:132px;}
</style>