<?php $this->load->view('site/templates/header');?>
	<div id="content">
        <ul class="setting-page">
			<li><a class="identity" href="settings/account"><i class="identy"></i>Profile 
				<i class="arrow"></i></a>
			</li>
			<?php if ($userDetails->row()->group == 'Seller'){?>
			<li><a class="identity" href="bids"><i class="identy"></i>Inbox <?php if($unread_messages_count != ''){  echo '('.$unread_messages_count.')'; } ?>
				<i class="arrow"></i></a>
			</li>	
			<?php } ?>
			<li><a href="auctions" class="ic-credit"><i class="identy"></i> <?php if($this->lang->line('auctions') != '') { echo stripslashes($this->lang->line('auctions')); } else echo "Auctions"; ?> <?php if($unread_auctions_count != ''){  echo '('.$unread_auctions_count.')'; } ?> <i class="arrow"></i></a></li>
 	           
			<li><a class="Pref" href="settings/preferences"><i class="identy"></i>Preferences
				<i class="arrow"></i></a>
			</li>
			<li><a class="Pass" href="settings/password"><i class="identy"></i>Password
				<i class="arrow"></i></a>
			</li>
			<li><a class="Notif" href="settings/notifications"><i class="identy"></i>Notifications
				<i class="arrow"></i></a>
			</li>
		</ul>
		<ul class="setting-page">
			<li><a class="Purc" href="purchases"><i class="identy"></i>Purchases
				<i class="arrow"></i></a>
			</li>
			<li><a class="Purc" href="orders"><i class="identy"></i>Orders
				<i class="arrow"></i></a>
			</li>
			<li><a class="Purc" href="credits"><i class="identy"></i>Earnings
				<i class="arrow"></i></a>
			</li>
			<li><a class="Subs" href="fancyybox/manage"><i class="identy"></i>Subscriptions 
				<i class="arrow"></i></a>
			</li>
			<li><a class="Ship" href="settings/shipping"><i class="identy"></i>Shipping
				<i class="arrow"></i></a>
			</li>
	            <?php if ($userDetails->row()->group == 'Seller'){?>
 	            <li><a href="bulk" class="ic-credit"><i class="identy"></i> <?php if($this->lang->line('bulk_upload') != '') { echo stripslashes($this->lang->line('bulk_upload')); } else echo "Bulk Upload"; ?><i class="arrow"></i></a></li>
 	            <?php }?> 
				
 	            
	            <?php if ($userDetails->row()->group == 'Seller'){?>
 	            <li><a href="featured" class="ic-credit"><i class="identy"></i> <?php if($this->lang->line('featured_seller') != '') { echo stripslashes($this->lang->line('featured_seller')); } else echo "Featured Seller"; ?><i class="arrow"></i></a></li>
 	            <?php }?> 
	            <?php if ($userDetails->row()->group == 'Seller' && $userDetails->row()->store_payment == 'Paid'){?>
 	            <li><a href="seller/dashboard" class="ic-credit"><i class="identy"></i> <?php if($this->lang->line('seller_dashboard') != '') { echo stripslashes($this->lang->line('seller_dashboard')); } else echo "Dashboard"; ?><i class="arrow"></i></a></li>
 	            <?php }?>                     
		</ul>
		<ul class="setting-page">		
			<li><a class="Ref" href="referrals"><i class="identy"></i>Referrals 
				<i class="arrow"></i></a>
			</li>
			<!--<li><a class="Cred" href="affiliate"><i class="identy"></i>Affiliate 
				<i class="arrow"></i></a>
			</li>	
			<li><a class="Inv" href="invite-friends"><i class="identy"></i>Invite Friends 
				<i class="arrow"></i></a>
			</li>-->
			<li><a class="Gif" href="settings/giftcards"><i class="identy"></i>Gift Card 
				<i class="arrow"></i></a>
			</li>
			<li><a class="Gif" href="seller/dashboard/shop-settings"><i class="identy"></i>Shop Settings 
				<i class="arrow"></i></a>
			</li>
        </ul> 
	</div>