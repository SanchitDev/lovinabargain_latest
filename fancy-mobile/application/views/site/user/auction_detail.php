<?php
$this->load->view('site/templates/header');
?>

<div class="lang-en wider no-subnav thing signed-out winOS">
    <div id="container-wrapper">
		<div class="container">
    	    <div class="wrapper-content" style="padding-top: 6%;">					
				<div class="profile-list">            
					<div class="box-content">
						<section class="left_sec_tion min_height" style="height: 924px;">	
							<div id="container-wrapper " >
								<div id="content">
									<?php 	if($auction->row()->do_capture == 'Pending'){ 
													$c_time  = date('Y-m-d H:i:s', time());
													$currnt_time = strtotime( $c_time.' + 3 minute');
													$current_time  = date('Y-m-d H:i:s', $currnt_time);
													if($auction->row()->end_date > $current_time){
												?>
												<span class="auction-countdown" data-countdown="<?php echo $auction->row()->end_date; ?>" ></span>
													<?php }else{ ?>
												<span class="auction-countdown">Expired</span>
													<?php } 
											}else if($auction->row()->do_capture == 'Success'){ ?>
												<span class="auction-countdown">Offer Accepted</span>
									<?php 	}				
									?>
									<h1> Paid Amount : <?php echo $auction->row()->athorized_amount.' '.$currencySymbol; ?></h1>
									<div class="accept_load_wait">
										<?php $img =  explode(',',$auction->row()->image)[0];?>														
										<a target="_blank" href="<?php echo base_url(); ?>things/<?php echo $auction->row()->product_id; ?>">
											<img style=" width: 50%;" class="img-bid" src="<?php echo DESKTOPURL;?>images/product/<?php echo $img ; ?>">
										</a>
										<div class="product_info">
											<h3 style="color:gray;float:left;"><?php echo ucfirst($auction->row()->product_name); ?></h3>
											<div class="product_descr_s" style="margin-top: -13px;"><p><?php echo substr($auction->row()->description,0,299).'...'; ?></p></div>
										</div>
                                        <div class="auction_status">
									<h2>Status :</h2>
									<?php 
										if($auction->row()->do_capture == 'Success'){ ?>
											<div class="auction_status_inner">
												<span>You have accepted the offer</span>
												<a class="link-auction-bid"href="conversation/<?php echo $conversation->row()->BidId; ?>" target="_blank">Click Here</a>
												<span>to see the offer details</span>
											</div>
								<?php 	
										}else{ 
											echo '<div class="link-auction-bid">Pending</div>';
										} 
								?>
									</div>
									</div>
									
								</div>
								<div class="offer-list-auction accept_load_wait1">
									<label>Offers</label>
									<ul>
										<?php if($offersList->num_rows() > 0){ 
											foreach($offersList->result() as $row){
											?>
											
											<li>
												<!--  1  -->
												
												<div class="offr_prdct_img">
													<a href="things/<?php echo $row->productId; ?>" target="_blank" > <img style=" width: 50% !important;"  src="<?php echo DESKTOPURL;?>images/product/<?php echo $row->product_img; ?>"> </a>
												</div>
												
												<div class="offr_price">
													price : <?php echo $row->offer_price.' '.$currencySymbol; ?>
												</div>
												
												
												<div class="three_buttons">
													<?php if($auction->row()->do_capture == 'Pending'){ ?>	
														<?php if($row->offer_status == 'Pending'){ ?>
															
																<button type="button" onclick="return auct('accept','<?php echo $row->BidId; ?>','<?php echo $row->product_sell_id; ?>')" >Accept</button>
																<button type="button" onclick="return auct('decline','<?php echo $row->BidId; ?>','<?php echo $row->product_sell_id; ?>')" >Decline</button>
																<button type="button" onclick="return auct('reject','<?php echo $row->BidId; ?>','<?php echo $row->product_sell_id; ?>')" >Reject</button>
															
														
														<?php }else if($row->offer_status == 'Decline'){ ?>
															<button type="button" disabled style="background: #79797b;border: none;cursor:auto; margin-right: 26%; padding: 7px 22px;">Declined</button> 
														<?php }else if($row->offer_status == 'Reject') { ?>
															<button type="button" disabled style="background: #79797b;border: none;cursor:auto;margin-right: 26%; padding: 7px 22px;" >Rejected</button> 
														<?php } ?>
														
													<?php 	}else { 
																		if($auction->row()->total == $row->offer_price){  ?>
																		<button disabled style="cursor:auto;margin-right: 26%; padding: 7px 19px;" type="button" >Accepted</button>
																<?php	}else{  
																
																				if($row->offer_status == 'Decline'){
																					$status = 'Declined';
																				}else if($row->offer_status == 'Reject'){
																					$status = 'Rejected'; ?>
																					<button type="button" disabled style="background: #79797b;border: none;cursor:auto; margin-right: 26%; padding: 7px 22px;"><?php echo $status; ?></button>
																						<?php 
																				}else{ ?>
																					<button type="button" disabled style="background: #79797b;border: none;cursor:auto;">Accept</button>
																					<button type="button" disabled style="background: #79797b;border: none;cursor:auto;">Decline</button>
																					<button type="button" disabled style="background: #79797b;border: none;cursor:auto;">Reject</button>
																		<?php	}
																?>
																			 
																		
																	
															<?php	} ?>
													<?php 	} ?>
												</div>
												
												
											</li>
											
											<?php } 
										
										}else{ ?>
										<li>
											No offers yet.
										</li>
										<?php } ?>
									</ul>
								</div>
							</div>    
                        </section>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>

	<!-- COUNTDOWN -->
<script type="text/javascript" src="js/assets/lib/countdown/jquery.plugin.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/site/jquery.countdown.js"></script>

<script type="text/javascript">
    $(document).ready(function(e) {
        $('[data-countdown]').each(function() {
            var day  = "Day";
            var days = "Days";
            var $this = $(this), finalDate = $(this).data('countdown');
            $this.countdown(finalDate, function(event) {
                if( event.strftime('%D') != 00 && event.strftime('%D') == 01 ){
                    $this.html(event.strftime('%D '+day+' %H:%M:%S'));
                }
                else if( event.strftime('%D') > 01 ){
                    $this.html(event.strftime('%D '+days+' %H:%M:%S'));
                }
                else{
                    $this.html(event.strftime('%H:%M:%S'));
                }
           });
        });
       
		
		
	});
</script>
