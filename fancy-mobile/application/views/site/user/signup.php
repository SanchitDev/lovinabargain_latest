<?php $this->load->view('site/templates/header');
@session_start();
$social_login_session_array = $this->session->all_userdata(); 
if($social_login_session_array['social_login_name']==''){
	$style = 'display:none';
}else{
	$style = 'display:block';
}
?>
<?php if (is_file('google-login-mats/index.php')){
	require_once 'google-login-mats/index.php';
}?>
         <div id="content">
        	<div id="c" class="wrap sign">
            	<h2 class="tit">Join Fancyy today</h2>
				<?php if($social_login_session_array['social_login_name']==''){?>
                <dl class="sign-sns">
                	<dt>Connect with a social network</dt>
                    <dd>
                    	<ul>
						 <?php if ($this->config->item('facebook_app_id') != '' && $this->config->item('facebook_app_secret') != ''){?> 
                        	<li><button class="btn-f sns-f" onclick="window.location.href='<?php echo base_url();?>facebook/user.php'"><i class="icon"></i><?php echo site_lg('lg_facebook', 'Facebook');?></button></li>
						<?php } ?>
						<?php if ($this->config->item('google_client_secret') != '' && $this->config->item('google_client_id') != '' && $this->config->item('google_redirect_url') != '' && $this->config->item('google_developer_key') != '' && is_file('google-login-mats/index.php')){?> 
						   <li><button id="fancyy-g-signin" onClick="window.location.href='<?php echo $authUrl; ?>'" class="btn-g sns-g" next="/settings/shipping"><i class="icon"></i><?php echo site_lg('lg_google', 'Google');?></button></li>
                        <?php } ?>
						<?php if ($this->config->item('consumer_key') != '' && $this->config->item('consumer_secret') != ''){ ?> 
							<li><button class="btn-t sns-t" onClick="window.location.href='<?php echo base_url();?>twtest/redirect'"><i class="icon"></i><?php echo site_lg('lg_twitter', 'Twitter');?></button></li> 
						<?php } ?>
                        </ul>
                    </dd>
                </dl>
				<?php } ?>
                <dl class="signup-frm" style="border-top:1px solid #E2E5E7;">
				<?php if($social_login_session_array['social_login_name']==''){?>
                    <a href="javascript:showView('1');"><dt><i class="icon arrow"></i>Or sign up with your email address</dt></a>
				<?php } ?>
                    <form method="post" onsubmit="return register_user();">
					<div class="showlist1 frm" style="<?php echo $style; ?>">
					<?php if($social_login_session_array['social_email_name'] != '') { ?> <h3 class="stit">Your email address : <?php echo $social_login_session_array['social_email_name'];?> </h3><?php } ?>
						<p><label class="label">Full Name</label>
                            <input type="text" placeholder="Fullname" name="full_rname" class="fullname" value="<?php echo $social_login_session_array['social_login_name'];?>"/>
                        </p>
                        <p><label class="label">User Name</label>
                            <input type="text" autocomplete="off" placeholder="Username" name="user_name" class="username" onKeyUp="$(this).parents('.frm').find('.url b').text($(this).val())" value=""/>
							<small class="url"><?php echo base_url();?>user/<b><?php echo site_lg('lg_username', 'USERNAME');?></b></small>
                        </p>
                        <p><label class="label">Email Address</label>
                            <input type="<?php if($social_login_session_array['loginUserType'] != 'google') echo 'text'; else echo 'hidden'; ?>" placeholder="Email Address" name="email-id" class="email" value="<?php echo $social_login_session_array['social_email_name'];?>"/>
                        <?php if($social_login_session_array['loginUserType'] != 'google') { ?></p><?php } ?>
                        <?php if($social_login_session_array['social_login_name'] == '') {?> <p><label class="label"><?php if($this->lang->line('signup_password') != '') { echo stripslashes($this->lang->line('signup_password')); } else echo "Password"; ?><span class="error-label" id="error-user_password"></span></label><?php } ?>
						<?php $pwdLength = 10;
							  $userNewPwd = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $pwdLength);?>
                            <input type="<?php if($social_login_session_array['social_login_name'] == '') echo 'password'; else echo 'hidden'; ?>" placeholder="Password" name="password" class="password" value="<?php if($social_login_session_array['social_login_name'] != '') echo $userNewPwd; ?>" />
							<?php if($social_login_session_array['social_login_name'] == '') {?></p><?php } ?>
						<input name="referrer" type="hidden" class="referrer" value="" />
						<input name="invitation_key" type="hidden" class="invitation_key" value="" />
						<input type='hidden' name='csrfmiddlewaretoken' value='UFLfIU881eyZJbm7Bq0kUFZ9sVaWGh54' />
						<input type='hidden' name='thumbnail' id='thumbnail' value='<?php echo $social_login_session_array['social_image_name'];?>' />
					
						<input type='hidden' name='twitter_id' id='twitter_id' value='<?php if($social_login_session_array['social_tw_id']!=''){ echo $social_login_session_array['social_tw_id']; } else { echo "0"; } ?>'/>
						<input type='hidden' name='facebook_id' id='facebook_id' value='<?php if($social_login_session_array['social_fb_id']!=''){ echo $social_login_session_array['social_fb_id']; } else { echo "0"; } ?>'/> 
						<input type='hidden' name='google_id' id='google_id' value='<?php if($social_login_session_array['social_gl_id']!=''){ echo $social_login_session_array['social_gl_id']; } else { echo "0"; } ?>'/>
					
						<input type='hidden' name='twitter_username' id='twitter_username' value='<?php echo $social_login_session_array['social_tw_username'];?>' />
						<input type='hidden' name='facebook_username' id='facebook_username' value='<?php echo $social_login_session_array['social_fb_username'];?>' />
						<input type='hidden' name='google_username' id='google_username' value='<?php echo $social_login_session_array['social_gl_username'];?>' />
					
						<input type='hidden' name='loginUserType' id='loginUserType' value='<?php if($social_login_session_array['loginUserType'] != '') echo $social_login_session_array['loginUserType']; else echo "normal";?>' />
                        <button class="btn-blue btn-signin btn-sign">Create my account</button>
                    </div>
					</form>
                </dl>
            </div>            
        </div>
    </div>
</div>
</body>
</html>