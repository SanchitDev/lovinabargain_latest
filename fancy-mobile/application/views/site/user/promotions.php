<?php
$this->load->view('site/templates/header');
?>
<link rel="stylesheet" media="all" type="text/css" href="css/site/<?php echo SITE_COMMON_DEFINE ?>setting.css">
<link rel="stylesheet" media="all" type="text/css" href="css/site/<?php echo SITE_COMMON_DEFINE ?>verticaltabs.css">
<script type="text/javascript" src="js/site/<?php echo SITE_COMMON_DEFINE ?>verticaltabs.pack.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#textExample").verticaltabs({speed: 500,slideShow: false,activeIndex: 0});
		$("#imageExample").verticaltabs({speed: 1000,slideShow: true,slideShowSpeed: 3000,activeIndex: 0,playPausePos: "topRight"});
	});
</script>
<div class="lang-en no-subnav wider winOS">
	<div id="container-wrapper">
		<div class="container">
        	<div class="fancy_promotion">
            	<div class="promotion_inner">
                	<div class="intro">
						<h3><?php echo site_lg('lg_earn', 'Earn Oliver promotions');?></h3>
						<p><?php echo site_lg('lg_favourite', 'Add your favorite things to Oliver from around the web and earn Oliver promotions as a top contributing member of our community. 
                        Once you unlock a badge, it will be available to you on your Oliver profile.');?></p>
					</div>
                    <div class="verticalslider" id="textExample">
                    	<div style="float:left; width:30%;">
                    	<p class="side_head"><?php echo site_lg('lg_PROMOTIONS', 'PROMOTIONS');?></p>
                        <ul class="verticalslider_tabs">
                            <li><a href="#">Men's</a></li>
                            <li><a href="#">Women's</a></li>
                            <li><a href="#"><?php echo site_lg('lg_kids', 'Kids');?></a></li>
                            <li><a href="#"><?php echo site_lg('lg_pets', 'Pets');?></a></li>
                            <li><a href="#"><?php echo site_lg('lg_interior', 'Interior');?></a></li>
                            <li><a href="#"><?php echo site_lg('lg_technology', 'Technology');?></a></li>
                            <li><a href="#">Art & Design</a></li>
                            <li><a href="#">Food & Drink</a></li>
                            <li><a href="#"><?php echo site_lg('lg_media', 'Media');?></a></li>       
                            <li class="side_head" style="border-top: 1px solid #EBECEF; margin: 15px 0 10px;padding: 25px 0 0;">ADDITIONAL PROMOTIONS</li> 
                            <li><a href="#"><?php echo site_lg('lg_marketing', 'Marketing');?></a></li>
                            <li><a href="#"><?php echo site_lg('lg_gift', 'Gift');?></a></li>                 
                        </ul>
                        </div>
                        <ul class="verticalslider_contents">                        	                        
                            <li class="mens">
                            	<h4>Men's<small>Collect badges that make you look like a real gentleman.</small></h4>    
                            	<div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target"><?php echo site_lg('lg_top', 'TOP');?></b><i class="icon top"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Men's Fashion Director</span>
                                        <p>You added one of the top 50 most popular items in Men’s. You, Sir, are a real gentleman!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">150</b><i class="icon senior"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Senior Men's Stylist</span>
                                        <p>You added 150 things to the Men’s category on Oliver. You are truly a trend-setter in Men’s fashion!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">50</b><i class="icon junior"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Junior Men's Stylist</span>
                                        <p>You added 50 things to the Men’s category on Oliver. All your hard work is paying off!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">25</b><i class="icon assist"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Assistant Men's Stylist</span>
                                        <p>You added 50 things to the Men’s category on Oliver. All your hard work is paying off!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">5</b><i class="icon intern"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Men's Style Intern</span>
                                        <p>You added 5 things to the Men’s category on Oliver! This is just the beginning!</p>
                                    </div>
                                </div>
                            </li>    
                            <li class="womens">
                            	<h4>Women's<small>Collect badges that make you look like a real woman.</small></h4>    
                            	<div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target"><?php echo site_lg('lg_top', 'TOP');?></b><i class="icon top"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Women's Fashion Director</span>
                                        <p>You added one of the top 50 most popular items in Women’s. Even Coco would be impressed!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">150</b><i class="icon senior"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Senior Women's Stylist</span>
                                        <p>You added 150 things to the Women’s category on Oliver. You are truly a trend-setter in Women’s fashion!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">50</b><i class="icon junior"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Junior Women's Stylist</span>
                                        <p>You added 50 things to the Women’s category on Oliver. All your hard work is paying off!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">25</b><i class="icon assist"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Assistant Women's Stylist</span>
                                        <p>You added 25 things to the Women’s category on Oliver. Keep up the good work!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">5</b><i class="icon intern"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Women's Style Intern</span>
                                        <p>You added 5 things to the Women’s category on Oliver. This is just the beginning!</p>
                                    </div>
                                </div>
                            </li>
                            <li class="kids">
                            	<h4><?php echo site_lg('lg_kids', 'Kids');?><small>Collect badges that will make you the coolest kid on the block.</small></h4>    
                            	<div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target"><?php echo site_lg('lg_top', 'TOP');?></b><i class="icon top"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Kids Fashion Director</span>
                                        <p>You added one of the top 50 most popular items in Women’s. Even Coco would be impressed!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">150</b><i class="icon senior"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Senior Kids Stylist</span>
                                        <p>You added 150 things to the Women’s category on Oliver. You are truly a trend-setter in Women’s fashion!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">50</b><i class="icon junior"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Junior Kids Stylist</span>
                                        <p>You added 50 things to the Women’s category on Oliver. All your hard work is paying off!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">25</b><i class="icon assist"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Assistant Kids Stylist</span>
                                        <p>You added 25 things to the Women’s category on Oliver. Keep up the good work!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">5</b><i class="icon intern"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Kids Style Intern</span>
                                        <p>You added 5 things to the Women’s category on Oliver. This is just the beginning!</p>
                                    </div>
                                </div>
                            </li>
                            <li class="pets">
                            	<h4><?php echo site_lg('lg_pets', 'Pets');?><small>Collect badges that will show off your love for animals.</small></h4>    
                            	<div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target"><?php echo site_lg('lg_top', 'TOP');?></b><i class="icon top"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Pet Whisperer</span>
                                        <p>You added one of the top 50 most popular items in Women’s. Even Coco would be impressed!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">150</b><i class="icon senior"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>PETA Activist</span>
                                        <p>You added 150 things to the Women’s category on Oliver. You are truly a trend-setter in Women’s fashion!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">50</b><i class="icon junior"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Pet Aficionado</span>
                                        <p>You added 50 things to the Women’s category on Oliver. All your hard work is paying off!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">25</b><i class="icon assist"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Pet Enthusiast</span>
                                        <p>You added 25 things to the Women’s category on Oliver. Keep up the good work!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">5</b><i class="icon intern"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Pet Lover</span>
                                        <p>You added 5 things to the Women’s category on Oliver. This is just the beginning!</p>
                                    </div>
                                </div>
                            </li>
                            <li class="interior">
                            	<h4><?php echo site_lg('lg_interior', 'Interior');?><small>Collect badges that will show off your pristine sense of beauty.</small></h4>    
                            	<div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target"><?php echo site_lg('lg_top', 'TOP');?></b><i class="icon top"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Senior Interior Designer</span>
                                        <p>You added one of the top 50 most popular items in Women’s. Even Coco would be impressed!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">150</b><i class="icon senior"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Senior Interior Decorator</span>
                                        <p>You added 150 things to the Women’s category on Oliver. You are truly a trend-setter in Women’s fashion!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">50</b><i class="icon junior"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Junior Interior Decorator</span>
                                        <p>You added 50 things to the Women’s category on Oliver. All your hard work is paying off!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">25</b><i class="icon assist"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Assistant Interior Decorator</span>
                                        <p>You added 25 things to the Women’s category on Oliver. Keep up the good work!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">5</b><i class="icon intern"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Interior Decorator Intern</span>
                                        <p>You added 5 things to the Women’s category on Oliver. This is just the beginning!</p>
                                    </div>
                                </div>
                            </li>
                            <li class="tech">
                            	<h4><?php echo site_lg('lg_technology', 'Technology');?><small>Collect badges that will make you look like the coolest wizard out there.</small></h4>    
                            	<div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target"><?php echo site_lg('lg_top', 'TOP');?></b><i class="icon top"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>IT Director</span>
                                        <p>You added one of the top 50 most popular items in Women’s. Even Coco would be impressed!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">150</b><i class="icon senior"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span><?php echo site_lg('lg_technowizard', 'Technowizard');?></span>
                                        <p>You added 150 things to the Women’s category on Oliver. You are truly a trend-setter in Women’s fashion!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">50</b><i class="icon junior"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span><?php echo site_lg('lg_technophile', 'Technophile');?></span>
                                        <p>You added 50 things to the Women’s category on Oliver. All your hard work is paying off!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">25</b><i class="icon assist"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Tech Enthusiast</span>
                                        <p>You added 25 things to the Women’s category on Oliver. Keep up the good work!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">5</b><i class="icon intern"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Tech Geek</span>
                                        <p>You added 5 things to the Women’s category on Oliver. This is just the beginning!</p>
                                    </div>
                                </div>
                            </li>
                            <li class="art">
                            	<h4>Art & Design<small>Collect badges and show off your inner Picasso.</small></h4>    
                            	<div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target"><?php echo site_lg('lg_top', 'TOP');?></b><i class="icon top"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Art Director</span>
                                        <p>You added one of the top 50 most popular items in Women’s. Even Coco would be impressed!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">150</b><i class="icon senior"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Museum Curator</span>
                                        <p>You added 150 things to the Women’s category on Oliver. You are truly a trend-setter in Women’s fashion!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">50</b><i class="icon junior"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Gallery Curator</span>
                                        <p>You added 50 things to the Women’s category on Oliver. All your hard work is paying off!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">25</b><i class="icon assist"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Art Collector</span>
                                        <p>You added 25 things to the Women’s category on Oliver. Keep up the good work!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">5</b><i class="icon intern"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Gallery Intern</span>
                                        <p>You added 5 things to the Women’s category on Oliver. This is just the beginning!</p>
                                    </div>
                                </div>
                            </li>
                            <li class="food">
                            	<h4>Food & Drink<small>Collect badges and show everyone what a foodie you are.</small></h4>    
                            	<div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target"><?php echo site_lg('lg_top', 'TOP');?></b><i class="icon top"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Restaurant Mogul</span>
                                        <p>You added one of the top 50 most popular items in Women’s. Even Coco would be impressed!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">150</b><i class="icon senior"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span><?php echo site_lg('lg_connoisseur', 'Connoisseur');?></span>
                                        <p>You added 150 things to the Women’s category on Oliver. You are truly a trend-setter in Women’s fashion!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">50</b><i class="icon junior"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span><?php echo site_lg('lg_gourmand', 'Gourmand');?></span>
                                        <p>You added 50 things to the Women’s category on Oliver. All your hard work is paying off!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">25</b><i class="icon assist"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Restaurant Critic</span>
                                        <p>You added 25 things to the Women’s category on Oliver. Keep up the good work!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">5</b><i class="icon intern"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span><?php echo site_lg('lg_foodie', 'Foodie');?></span>
                                        <p>You added 5 things to the Women’s category on Oliver. This is just the beginning!</p>
                                    </div>
                                </div>
                            </li>
                            <li class="media">
                            	<h4><?php echo site_lg('lg_media', 'Media');?><small>Collect badges that make you look media-friendly.</small></h4>    
                            	<div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target"><?php echo site_lg('lg_top', 'TOP');?></b><i class="icon top"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Media Mogul</span>
                                        <p>You added one of the top 50 most popular items in Women’s. Even Coco would be impressed!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">150</b><i class="icon senior"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Record Mogul</span>
                                        <p>You added 150 things to the Women’s category on Oliver. You are truly a trend-setter in Women’s fashion!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">50</b><i class="icon junior"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Label Manager</span>
                                        <p>You added 50 things to the Women’s category on Oliver. All your hard work is paying off!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">25</b><i class="icon assist"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Record Collector</span>
                                        <p>You added 25 things to the Women’s category on Oliver. Keep up the good work!</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">5</b><i class="icon intern"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Street Team</span>
                                        <p>You added 5 things to the Women’s category on Oliver. This is just the beginning!</p>
                                    </div>
                                </div>
                            </li>      
                            <li></li>                                                
                            <li class="market">
                            	<h4><?php echo site_lg('lg_marketing', 'Marketing');?><small>Invite friends and family and receive awesome promotions for your support.</small></h4>    
                            	<div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">15</b><i class="icon top"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Marketing Assistant</span>
                                        <p>You have sent 15 invites to Oliver! Keep up the good work! The more friends you invite, the more you will earn.</p>
                                    </div>
                                </div>
                                <div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<b class="target">1</b><i class="icon senior"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Marketing Intern</span>
                                        <p>You sent your first invitation to Oliver! Send more invites and earn more credit and promotions.</p>
                                    </div>
                                </div>                       
                            </li>    
                            <li class="gift">
                            	<h4><?php echo site_lg('lg_gift', 'Gift');?><small>Celebrate your friends on Oliver and receive recognition for it.</small></h4>    
                            	<div class="tab_slider_list">
                                	<div class="tab_slider_list_left">
                                    	<i class="icon top"></i>
                                    </div>
                                    <div class="tab_slider_list_right">
                                    	<span>Gift Contributor</span>
                                        <p>You have contributed to a Group Gift on Oliver. We think you’re amazing and we're sure your friends agree!</p>
                                    </div>
                                </div>                                
                            </li>                         
                        </ul>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>
<?php 
$this->load->view('site/templates/footer');
?>
