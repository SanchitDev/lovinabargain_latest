<?php $this->load->view('site/templates/header.php'); ?>
<link rel="stylesheet" media="all" type="text/css" href="css/site/<?php echo SITE_COMMON_DEFINE ?>setting.css">
<style type="text/css">
	ol.stream {position: relative;}
	ol.stream.use-css3 li.anim {transition:all .25s;-webkit-transition:all .25s;-moz-transition:all .25s;-ms-transition:all .25s;visibility:visible;opacity:1;}
	ol.stream.use-css3 li {visibility:hidden;}
	ol.stream.use-css3 li.anim.fadeout {opacity:0;}
	ol.stream.use-css3.fadein li {opacity:0;}
	ol.stream.use-css3.fadein li.anim.fadein {opacity:1;}
</style>
<script type="text/javascript">
	var can_show_signin_overlay = false;
	if (navigator.platform.indexOf('Win') != -1) {document.write("<style>::-webkit-scrollbar, ::-webkit-scrollbar-thumb {width:7px;height:7px;border-radius:4px;}::-webkit-scrollbar, ::-webkit-scrollbar-track-piece {background:transparent;}::-webkit-scrollbar-thumb {background:rgba(255,255,255,0.3);}:not(body)::-webkit-scrollbar-thumb {background:rgba(0,0,0,0.3);}::-webkit-scrollbar-button {display: none;}</style>");}
</script>
<div class="lang-en no-subnav wider winOS">
<div id="container-wrapper">
<div class="container">
	<?php if($flash_data != '') { ?>
	<div class="errorContainer" id="<?php echo $flash_data_type;?>">
	<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
	<p><span><?php echo $flash_data;?></span></p>
	</div>
	<?php } ?>
<div class="wrapper-content order" >
<div id="content">
<div class="cart-list chept2" style="padding:0;">
<?php $paypalProcess = unserialize($paypal_ipn_settings['settings']);  
if($plan->row()->plan_price > 0){
?>
<div class="cart-payment-wrap card-payment new-card-payment" id="PaypalPay" style="display:block;">
<script type="text/javascript">
$(document).ready(function(){	$("#PaymentPaypalForm").validate();
	$.validator.addMethod("ValidZipCode", function( value, element ) {
	var result = this.optional(element) || value.length >= 3;
	if (!result) {
	return false;
	}
	else{
	return true;
	}
	}, "Please Enter the Correct ZipCode");
});
</script> 
<form name="PaymentPaypalForm" id="PaymentPaypalForm" method="post" enctype="multipart/form-data" action="site/user_settings/paypal_payment"  autocomplete="off">
<input type="hidden" name="paypalmode" id="paypalmode" value="<?php echo $paypalProcess['mode']; ?>"  />
<input type="hidden" name="paypalEmail" id="paypalEmail" value="<?php echo $paypalProcess['merchant_email']; ?>"  />                        
<div id="complete-payment">
<div class="hotel-booking-left">
<dl class="payment-personal">
<dt><b><?php if($this->lang->line('checkout_billing_addr') != '') { echo stripslashes($this->lang->line('checkout_billing_addr')); } else echo "Billing Address"; ?></b> <small><?php if($this->lang->line('checkout_enter_bill') != '') { echo stripslashes($this->lang->line('checkout_enter_bill')); } else echo "Enter your billing address"; ?></small></dt>
<dd>
<label for="payment-personal-name-fst"><?php if($this->lang->line('header_name') != '') { echo stripslashes($this->lang->line('header_name')); } else echo "Name"; ?> <b>*</b></label>
<input name="full_name" id="full_name" type="text" class="required" value="<?php echo $userDetails->row()->full_name; ?>" />
</dd>
<dd>
<label for="payment-adds-1"><?php if($this->lang->line('shipping_address_comm') != '') { echo stripslashes($this->lang->line('shipping_address_comm')); } else echo "Address"; ?> <b>*</b></label>
<input id="address" name="address" type="text" class="required" value="<?php echo $userDetails->row()->address; ?>">
</dd>
<dd>
<label for="payment-adds-1"><?php if($this->lang->line('shipping_address_comm') != '') { echo stripslashes($this->lang->line('shipping_address_comm')); } else echo "Address"; ?> 2</label>
<input id="address2" name="address2" type="text" class="" value="<?php echo $userDetails->row()->address2; ?>">
</dd>
<dd>
<label for="payment-city"><?php if($this->lang->line('header_city') != '') { echo stripslashes($this->lang->line('header_city')); } else echo "City"; ?> <b>*</b></label>
<input id="city" name="city" type="text" class="required" value="<?php echo $userDetails->row()->city; ?>">
</dd>
</dl>
<dl class="payment-card">
<dt><b>&nbsp;</b> <small>&nbsp;</small></dt>
<dd>
<label for="payment-state"><?php if($this->lang->line('checkout_state') != '') { echo stripslashes($this->lang->line('checkout_state')); } else echo "State"; ?> <b>*</b></label>
<input id="state" name="state" type="text" class="required" value="<?php echo $userDetails->row()->state; ?>">
</dd>
<dd>
<label for="payment-state"><?php if($this->lang->line('header_country') != '') { echo stripslashes($this->lang->line('header_country')); } else echo "Country"; ?> <b>*</b></label>
<select id="country" name="country" class="select-round select-white select-country selectBox required">
<option value="">-------------------- <?php if($this->lang->line('checkout_select') != '') { echo stripslashes($this->lang->line('checkout_select')); } else echo "SELECT"; ?> --------------------</option>											
<?php foreach($countryList->result() as $cntyRow){ ?>	
<option value="<?php echo $cntyRow->country_code; ?>" <?php if($cntyRow->country_code == $userDetails->row()->country){ echo 'selected="selected"';} ?> ><?php echo $cntyRow->name; ?></option>
<?php } ?>    
</select>
</dd>
<dd>
<label for="payment-zipcode"><?php if($this->lang->line('checkout_zip_code') != '') { echo stripslashes($this->lang->line('checkout_zip_code')); } else echo "Zip Code"; ?> <b>*</b></label> 
<input id="postal_code" name="postal_code" type="text" class="required ValidZipCode" value="<?php echo $userDetails->row()->postal_code; ?>">
</dd>
<dd>
<label for="payment-phone"><?php if($this->lang->line('checkout_phone_no') != '') { echo stripslashes($this->lang->line('checkout_phone_no')); } else echo "Phone No"; ?> <b>*</b></label> 
<input id="phone_no" name="phone_no" type="text" class="required number" value="<?php echo $userDetails->row()->phone_no; ?>">
</dd>
</dl>
<div class="hotel-booking-noti"><big><?php if($this->lang->line('checkout_secure_trans') != '') { echo stripslashes($this->lang->line('checkout_secure_trans')); } else echo "Secure Transaction"; ?></big><?php if($this->lang->line('checkout_ssl') != '') { echo stripslashes($this->lang->line('checkout_ssl')); } else echo "SSL Encrypted transaction powered by"; ?> <?php echo $siteTitle;?></div>
</div>
<div class="cart-payment">
<dl class="cart-payment-order">
<dt><?php if($this->lang->line('checkout_order') != '') { echo stripslashes($this->lang->line('checkout_order')); } else echo "Order"; ?></dt>
<dd>
<ul>
<li class="first">
<span class="order-payment-type"><?php if($this->lang->line('checkout_item_total') != '') { echo stripslashes($this->lang->line('checkout_item_total')); } else echo "Item total"; ?></span>
<span class="order-payment-usd"><b><?php echo $currencySymbol;?><?php echo number_format($plan->row()->plan_price,2,'.',''); ?></b> <?php echo $currencyType;?></span>
</li>
<li>
<span class="order-payment-type"><?php if($this->lang->line('referrals_shipping') != '') { echo stripslashes($this->lang->line('referrals_shipping')); } else echo "Shipping"; ?></span>
<span class="order-payment-usd"><b><?php echo $currencySymbol;?><?php echo number_format(0,2,'.',''); ?></b> <?php echo $currencyType;?></span>
</li>
<li>
<span class="order-payment-type"><?php if($this->lang->line('checkout_tax') != '') { echo stripslashes($this->lang->line('checkout_tax')); } else echo "Tax"; ?> </span>
<span class="order-payment-usd"><b><?php echo $currencySymbol;?><?php echo number_format(0,2,'.',''); ?></b> <?php echo $currencyType;?></span>
</li>
<li class="total">
<span class="order-payment-type"><b><?php if($this->lang->line('purchases_total') != '') { echo stripslashes($this->lang->line('purchases_total')); } else echo "Total"; ?></b></span>
<span class="order-payment-usd"><b><?php echo $currencySymbol;?><?php echo number_format($plan->row()->plan_price,2,'.',''); ?></b> <?php echo $currencyType;?></span>
</li>
</ul>
</dd>
</dl>
</div>
<input id="total_price" name="total_price" value="<?php echo number_format($plan->row()->plan_price,2,'.',''); ?>" type="hidden">
<input id="email" name="email" value="<?php echo $userDetails->row()->email; ?>" type="hidden">
<input name="PaypalSubmit" id="PaypalSubmit" class="button-complete" type="submit" value="<?php if($this->lang->line('checkout_comp_pay') != '') { echo stripslashes($this->lang->line('checkout_comp_pay')); } else echo "Complete Payment"; ?>" style="cursor:pointer;"  />
<div class="waiting"><?php if($this->lang->line('checkout_processing') != '') { echo stripslashes($this->lang->line('checkout_processing')); } else echo "Processing"; ?>...</div>
<div class="card-payment-foot"><?php if($this->lang->line('checkout_by_place') != '') { echo stripslashes($this->lang->line('checkout_by_place')); } else echo "By placing your order, you agree to the Terms"; ?> &amp; <?php if($this->lang->line('checkout_codtn_privacy') != '') { echo stripslashes($this->lang->line('checkout_codtn_privacy')); } else echo "Conditions and Privacy Policy"; ?>.</div>
</div>
</form> 
</div>        
<?php  } ?>
</div> 
</div> 
<!-- / content -->
<!-- / container -->
</div> 
<!-- / wrapper-content -->
<?php //$this->load->view('site/templates/footer_menu'); ?>
<script type="text/javascript" src="js/site/jquery.validate.js"></script>
<script type="text/javascript" src="js/site/<?php echo SITE_COMMON_DEFINE ?>selectbox.js"></script>
<script type="text/javascript" src="js/site/<?php echo SITE_COMMON_DEFINE ?>shoplist.js"></script>
<script type="text/javascript" src="js/site/<?php echo SITE_COMMON_DEFINE ?>address_helper.js"></script>
</body>
</html>