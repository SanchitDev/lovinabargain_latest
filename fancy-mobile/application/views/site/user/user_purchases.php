<?php
$this->load->view('site/templates/header');
?>
<div class="purchaces" id="content">
	<div style=" margin-top: 20px;" class="figure-product">
		<h2 class="comments-title"><?php if($this->lang->line('purchases_common') != '') { echo stripslashes($this->lang->line('purchases_common')); } else echo "Purchases"; ?></span></h2>
		<?php if($purchasesList->num_rows()>0){ ?>
			<div class="chart-wrap">
				<div class="fullpay">      
					<ul  class="chart cart-head">
						<li><?php if($this->lang->line('purchases__invoice') != '') { echo stripslashes($this->lang->line('purchases__invoice')); } else echo "Invoice"; ?></li>
                        <li><?php if($this->lang->line('purchases__paystatus') != '') { echo stripslashes($this->lang->line('purchases__paystatus')); } else echo "Payment Status"; ?></li>
                        <li><?php if($this->lang->line('purchases_shipstatus') != '') { echo stripslashes($this->lang->line('purchases_shipstatus')); } else echo "Shipping Status"; ?></li>
                        <li><?php if($this->lang->line('purchases_total') != '') { echo stripslashes($this->lang->line('purchases_total')); } else echo "Total"; ?></li>
                        <li><?php if($this->lang->line('purchases_orddate') != '') { echo stripslashes($this->lang->line('purchases_orddate')); } else echo "Order-Date"; ?></li>
                        <li><?php if($this->lang->line('purchases_option') != '') { echo stripslashes($this->lang->line('purchases_option')); } else echo "Option"; ?></li>
					</ul>   
					<ul  class="chart">
						<?php foreach ($purchasesList->result() as $row){?>
						<li>#<?php echo $row->dealCodeNumber;?></li>
                        <li><?php echo $row->status;?></li>
                        <li><?php echo $row->shipping_status;?></li>
                        <li><?php echo $row->total;?></li>
                        <li><?php echo $row->created;?></li>
                        <li><a style="color:green;" target="_blank" href="view-purchase/<?php echo $row->user_id;?>/<?php echo $row->dealCodeNumber;?>"><?php if($this->lang->line('view_invoice') != '') { echo stripslashes($this->lang->line('view_invoice')); } else echo "View Invoice"; ?></a><br/>
	                       	<?php if ($row->status == 'Paid'){?>
							<br />
							<a style="color:red;" href="order-review/<?php echo $row->user_id;?>/<?php echo $row->sell_id;?>/<?php echo $row->dealCodeNumber;?>"><?php if($this->lang->line('seller_discuss') != '') { echo stripslashes($this->lang->line('seller_discuss')); } else echo "Seller Discussion"; ?></a>
							<?php }?>
							</li>
						<?php } ?>
					</ul> 
				</div>
			</div>
			<?php } else {?>
			<div class=" section purchases no-data">
				<span class="icon"><i class="ic-pur"></i></span>
				<p><?php if($this->lang->line('purchases_not_purchase') != '') { echo stripslashes($this->lang->line('purchases_not_purchase')); } else echo "You haven't made any purchases yet."; ?></p>
			</div>
			<?php } ?>
	</div>
</div>