<?php $this->load->view('site/templates/header'); ?>
<link rel="stylesheet" media="all" type="text/css" href="css/site/<?php echo SITE_COMMON_DEFINE ?>setting.css">

        <div id="content" class="account-page">
        <div class="account-wrap">
        <dl class="profile-account">
<dd>
           <h2 class="ptit"><?php if($this->lang->line('settings_network') != '') { echo stripslashes($this->lang->line('settings_network')); } else echo "Networks"; ?></h2>
		   <div class="section apps">
		       <!--<h3 class="stit"><?php if($this->lang->line('app_network') != '') { echo stripslashes($this->lang->line('app_network')); } else echo "Apps"; ?></h3>-->
			   <fieldset class="frm">
                   <label><?php if($this->lang->line('apps_network_google') != '') { echo stripslashes($this->lang->line('apps_network_google')); } else echo "Google"; ?></label> 
				   <?php 
				     if ($loginCheck != ''){
				        if($user_details->row()->google_id != 0){
						  $buttonText = 'on';
						  $style= 'display:none';
						  $style1='display:block';
					    }else{
						  $buttonText = 'off';
						  $style= 'display:block';
						  $style1= 'display:none';
						}
				     }
				   ?>
				   <a class="<?php echo $buttonText;?>" id="<?php echo $buttonText;?>" onclick="google_connect(this.id);">
				      <span class="button_on"><?php if($this->lang->line('app_button_on') != '') { echo stripslashes($this->lang->line('app_button_on')); } else echo "ON"; ?></span><span class="button-off"><?php if($this->lang->line('app_button_off') != '') { echo stripslashes($this->lang->line('app_button_off')); } else echo "OFF"; ?></span>
				   </a>
                   <span class="after-off" style="<?php echo $style; ?>"><small><?php if($this->lang->line('apps_network_googletext') != '') { echo stripslashes($this->lang->line('apps_network_googletext')); } else echo "Connect and use your Google login to sign in to Oliver."; ?></small></span>
                   <span class="after-on" style="<?php echo $style1; ?>"><small><?php if($this->lang->line('apps_network_googletextafter') != '') { echo stripslashes($this->lang->line('apps_network_googletextafter')); } else echo "Connected to Google as "?>&nbsp;<?php echo "<b><font color='#2786C8'>".$user_details->row()->gl_username."</font></b>"; ?></small></span>				   
               </fieldset>
			   <fieldset class="frm">
                   <label><?php if($this->lang->line('apps_network_facebook') != '') { echo stripslashes($this->lang->line('apps_network_facebook')); } else echo "Facebook"; ?></label>
				   <?php 
				     if ($loginCheck != ''){
				        if($user_details->row()->facebook_id != 0){
						  $buttonText = 'on';
						  $checked = "checked";
						  $style= 'display:none';
						  $style1='display:block';
					    }else{
						  $buttonText = 'off';
						  $checked = "";
						  $style= 'display:block';
						  $style1= 'display:none';
						}
				     }
				   ?>
				  <a class="<?php echo $buttonText;?>" id="facebook-connect">
				      <span class="button_on"><?php if($this->lang->line('app_button_on') != '') { echo stripslashes($this->lang->line('app_button_on')); } else echo "ON"; ?></span><span class="button-off"><?php if($this->lang->line('app_button_off') != '') { echo stripslashes($this->lang->line('app_button_off')); } else echo "OFF"; ?></span>
				   </a>
                   <span class="after-off" style="<?php echo $style; ?>"><small><?php if($this->lang->line('apps_network_facebooktext') != '') { echo stripslashes($this->lang->line('apps_network_facebooktext')); } else echo "By connecting your Oliver account to Facebook, you'll be able to share things you Oliver directly to your Facebook Timeline."; ?></small></span>
                   <span class="after-on" style="<?php echo $style1; ?>"><small><?php if($this->lang->line('apps_network_facebooktextafter') != '') { echo stripslashes($this->lang->line('apps_network_facebooktextafter')); } else echo "Connected to Facebook as "?>&nbsp;<?php echo "<b><font color='#2786C8'>".$user_details->row()->fb_username."</font></b>";?></small>
				   <small><input type="checkbox" <?php echo $checked;?> name="">&nbsp;<?php if($this->lang->line('apps_network_fbautomaticallyafter') != '') { echo stripslashes($this->lang->line('apps_network_fbautomaticallyafter')); } else echo "Publish things you Oliver to your Facebook Timeline."; ?></small>
				   </span>				   
               </fieldset>
			   <fieldset class="frm">
			   <?php 
				     if ($loginCheck != ''){
				        if($user_details->row()->twitter_id != 0){
						  $buttonText = 'on';
						  $checked = "checked";
						  $style= 'display:none';
						  $style1='display:block';
					    }else{
						  $buttonText = 'off';
						  $checked = "";
						  $style= 'display:block';
						  $style1= 'display:none';
						}
				     }
				   ?>
                   <label><?php if($this->lang->line('apps_network_twitter') != '') { echo stripslashes($this->lang->line('apps_network_twitter')); } else echo "Twitter"; ?></label>
				   <a class="<?php echo $buttonText;?>" id="twitter-connect">
				      <span class="button_on"><?php if($this->lang->line('app_button_on') != '') { echo stripslashes($this->lang->line('app_button_on')); } else echo "ON"; ?></span><span class="button-off"><?php if($this->lang->line('app_button_off') != '') { echo stripslashes($this->lang->line('app_button_off')); } else echo "OFF"; ?></span>
				   </a>
                   <span class="after-off" style="<?php echo $style; ?>"><small><?php if($this->lang->line('apps_network_twittertext') != '') { echo stripslashes($this->lang->line('apps_network_twittertext')); } else echo "Share your Oliver activity to your Twitter profile."; ?></small></span>
                   <span class="after-on" style="<?php echo $style1; ?>"><small><?php if($this->lang->line('apps_network_twittertextafter') != '') { echo stripslashes($this->lang->line('apps_network_twittertextafter')); } else echo "Connected to Twitter as "?>&nbsp;<?php echo "<b><font color='#2786C8'>".$user_details->row()->tw_username."</font></b>"; ?></small>
				   <small><input type="checkbox" <?php echo $checked;?> name="">&nbsp;<?php if($this->lang->line('apps_network_twautomaticallyafter') != '') { echo stripslashes($this->lang->line('apps_network_twautomaticallyafter')); } else echo "Tweet things you Oliver automatically."; ?></small>
				   </span>				   
               </fieldset>
			   <!--<fieldset class="frm">
                   <label><?php if($this->lang->line('apps_network_instagram') != '') { echo stripslashes($this->lang->line('apps_network_instagram')); } else echo "Instagram"; ?></label>
				   <a href="#" class="on_off on">
				     <span class="button_on"><?php echo site_lg('lg_on', 'ON');?></span><span class="button-off"><?php echo site_lg('lg_off', 'OFF');?></span>
				   </a>
                   <span class="after-off"><small><?php if($this->lang->line('apps_network_instagramtext') != '') { echo stripslashes($this->lang->line('apps_network_instagramtext')); } else echo "Connect to Instagram and post your photos to Oliver."; ?></small></span>
				   <span class="after-on"><small><?php if($this->lang->line('apps_network_instagramtextafter') != '') { echo stripslashes($this->lang->line('apps_network_instagramtextafter')); } else echo "Connected to Instagram as "; ?></small></span>				   
               </fieldset>-->
		   </div>
           </dd>
           </dl>
        </div>
        </div>
	
	
<script	src="http://connect.facebook.net/en_US/all.js"></script>
<script type="text/javascript">
FB.init({
	    appId:'<?php echo $this->config->item('facebook_app_id');?>',
	    cookie:true,
	    status:true,
	    xfbml:true,
		oauth : true
    });
$('#facebook-connect').click(function(){
	var button = $(this).attr('class');
	if(button == 'off'){
		FB.login(function(response){
		if(response.authResponse){
			FB.api('/me', function(response){
				var fb_id = response.id;
				fb_username = response.username;
				url = '<?php echo base_url();?>site/user_settings/facebook_connect';
				$.post(url,{'id':fb_id,'username':fb_username},function(data){
					if(data == 0){
						alert("This Facebook account is already linked with another Oliver account");
					}else if(data == 1){
						window.location.href = '<?php echo base_url();?>settings/apps';
					}else{}
				});
			});
		}else{
			alert('User cancelled login or did not fully authorize.');
		}
		});
	}else{
		alert("Do you really want to remove facebook connection?");
		var url = '<?php echo base_url();?>site/user_settings/facebook_connect';
		$.post(url,function(){
			window.location.href = '<?php echo base_url();?>settings/apps';
		});
	}
});
$('#twitter-connect').click(function(){			
	var button = $(this).attr('class');
	if(button == 'off'){
	var loc = baseURL;
	var param = {'location':loc};
	var popup = window.open('about:blank','_blank', 'height=300,width=800,left=250,top=100,resizable=yes', true);
		$.post(
			baseURL+'site/user/app_twitter',
			param, 
			function(json){
				if (json.status_code==1){
					popup.location.href = json.url;						
				}
				else if (json.status_code==0){
					alert(json.message);
				}   
			},
			'json'
		);
	}else{
		alert("Do you really want to remove twitter connection?");
		var url = '<?php echo base_url();?>site/user/remove_twitter_app';
		$.post(url,function(){
			window.location.href = '<?php echo base_url();?>settings/apps';
		});
	}
});
</script>
<script>
        var OAUTHURL    =   'https://accounts.google.com/o/oauth2/auth?';
        var VALIDURL    =   'https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=';
        var SCOPE       =   'https://www.googleapis.com/auth/userinfo.profile';
        var CLIENTID    =   '670701648559-nv3poukq81crjub3ahlv59089ovcq6t2.apps.googleusercontent.com';
        var REDIRECT    =   'http://localhost/oliver/googlelogin/googleRedirect'
        var TYPE        =   'token';
        var _url        =   OAUTHURL + 'scope=' + SCOPE + '&client_id=' + CLIENTID + '&redirect_uri=' + REDIRECT + '&response_type=' + TYPE;
        var acToken;
        var tokenType;
        var expiresIn;
        var user;

        function google_connect(id) {
	        if(id == 'off'){
            var win         =   window.open(_url, "windowname1", 'width=800, height=600'); 

            var pollTimer   =   window.setInterval(function() { 
                if (win.document.URL.indexOf(REDIRECT) != -1) {
                    window.clearInterval(pollTimer);
                    var url =   win.document.URL;
                    acToken =   gup(url, 'access_token');
                    tokenType = gup(url, 'token_type');
                    expiresIn = gup(url, 'expires_in');
                    win.close();

                    validateToken(acToken);
                }
            }, 100);
			}else{
				alert("Do you really want to remove google connection?");
				var url = '<?php echo base_url();?>site/user_settings/google_connect';
				$.post(url,function(){
					window.location.href = '<?php echo base_url();?>settings/apps';
				});
	       }
        }

        function validateToken(token) {
            $.ajax({
                url: VALIDURL + token,
                data: null,
                success: function(responseText){  
                    getUserInfo();
                },  
                dataType: "jsonp"  
            });
        }

        function getUserInfo() {
            $.ajax({
                url: 'https://www.googleapis.com/oauth2/v1/userinfo?access_token=' + acToken,
                data: null,
                success: function(resp) {
					var tw_username = resp.name;
					    tw_id = resp.id;
                    url = '<?php echo base_url();?>site/user_settings/google_connect';
				    $.post(url,{'id':tw_id,'username':tw_username},function(data){
						if(data == 0){
							alert("This Google account is already linked with another Oliver account");
						}else if(data == 1){
							window.location.href = '<?php echo base_url();?>settings/apps';
						}else{}
				    });
                },
                dataType: "jsonp"
            });
        }

        //credits: http://www.netlobo.com/url_query_string_javascript.html
        function gup(url, name) {
            name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
            var regexS = "[\\#&]"+name+"=([^&#]*)";
            var regex = new RegExp( regexS );
            var results = regex.exec( url );
            if( results == null )
                return "";
            else
                return results[1];
        }

</script>
<?php $this->load->view('site/templates/footer'); ?>