<?php 
$this->load->view('site/templates/header.php');
?>
<?php if($flash_data != '') { ?>
	<div class="errorContainer" id="<?php echo $flash_data_type;?>">
		<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
		<p><span><?php echo $flash_data;?></span></p>
	</div>
<?php } ?>
<div id="content" class="cart-page">
	<div class="account-wrap">
		<div class="account-page">
       
	 <?php if ($shippingList->num_rows() > 0){?>
	
	<div  style=" margin-top: 20px;" class="figure-product">
            <h2 class="comments-title"><?php if($this->lang->line('shipping_saved_addrs') != '') { echo stripslashes($this->lang->line('shipping_saved_addrs')); } else echo "Your Saved Shipping Addresses"; ?></h2>
            <div class="fullpay">
            <table style="width:1336px;" class="shippingtbl">
                <thead bgcolor="#999999">
                    <tr>
                        <th><?php if($this->lang->line('shipping_default') != '') { echo stripslashes($this->lang->line('shipping_default')); } else echo "Default"; ?></th>
                        <th><?php if($this->lang->line('shipping_nickname') != '') { echo stripslashes($this->lang->line('shipping_nickname')); } else echo "Nick Name"; ?></th>
                        <th><?php if($this->lang->line('shipping_address_comm') != '') { echo stripslashes($this->lang->line('shipping_address_comm')); } else echo "Address"; ?></th>
                        <th><?php if($this->lang->line('shipping_phone') != '') { echo stripslashes($this->lang->line('shipping_phone')); } else echo "Phone"; ?></th>
                        <th><?php if($this->lang->line('purchases_option') != '') { echo stripslashes($this->lang->line('purchases_option')); } else echo "Option"; ?></th>
                    </tr>
                </thead>
				
                <tbody>
                    <?php foreach ($shippingList->result() as $row){?>
                    <tr aid="<?php echo $row->id;?>" isdefault="<?php if($row->primary == 'Yes'){echo TRUE; }else {echo FALSE;}?>">
                        <td><?php if($row->primary == 'Yes'){?><i class="ic-check"></i><?php }?></td>
                        <td><?php echo $row->nick_name;?></td>
                        
                        <td><?php echo $row->address1.', '.$row->address2.'<br/>'.$row->city.'<br/>'.$row->state.'<br/>'.$row->country.'-'.$row->postal_code;?></td>
                        <td><?php echo $row->phone;?></td>
                        
                     <!--   <td><a aid="<?php echo $row->id;?>" class="edit_"><?php if($this->lang->line('shipping_edit') != '') { echo stripslashes($this->lang->line('shipping_edit')); } else echo "Edit"; ?></a> / <a class="remove_"><?php if($this->lang->line('shipping_delete') != '') { echo stripslashes($this->lang->line('shipping_delete')); } else echo "Delete"; ?></a></td>-->
                     
                        <td><a href="settings/shipping-edit/<?php echo $row->id;?>" ><?php if($this->lang->line('shipping_edit') != '') { echo stripslashes($this->lang->line('shipping_edit')); } else echo "Edit"; ?></a> / <a class="remove" aid="<?php echo $row->id;?>"><?php if($this->lang->line('shipping_delete') != '') { echo stripslashes($this->lang->line('shipping_delete')); } else echo "Delete"; ?></a></td>
                        
                    </tr>
                    <?php }?>
                    
                </tbody>
            </table>
			</div>
            
			</div>
	<?php 
	}
	?>
       
     
<dl class="profile-account">
        
        <dd style=" padding: 1em;">
       
           <button style="width:100%" class="profile-button add-shipping-addr"><?php if($this->lang->line('shipping_add_ship') != '') { echo stripslashes($this->lang->line('shipping_add_ship')); } else echo "Add Shipping Address"; ?></button>
        
         </dd>
        </dl>
        
       <dl class="profile-account1">
        
         <form class="ltxt" action="shipping-address" method="post" name ="cartForm" id="cartForm" onsubmit="return cart_validation()">
        <dd>
       
        <p class="effete">
        <label class="label"><?php if($this->lang->line('shipping_add_ship') != '') { echo stripslashes($this->lang->line('shipping_add_ship')); } else echo "Add Shipping Address"; ?></label><br /></p>
                
        <p class="name-account"><label class="label"><?php if($this->lang->line('signup_full_name') != '') { echo stripslashes($this->lang->line('signup_full_name')); } else echo "Full Name"; ?></label>
		<input id="full_name" name="full_name" class="popup_add_input" type="text" >
        </p>
        
        <p class="name-account"><label class="label"><?php if($this->lang->line('shipping_nickname') != '') { echo stripslashes($this->lang->line('shipping_nickname')); } else echo "Nickname"; ?></label>
		<input id="nick_name" name="nick_name" class="popup_add_input width180" type="text" placeholder="<?php if($this->lang->line('header_home_work') != '') { echo stripslashes($this->lang->line('header_home_work')); } else echo "E.g. Home, Work, Aunt Jane"; ?>">
		</p>
        
            <p class="name-account"><label class="label"><?php if($this->lang->line('shipping_address_comm') != '') { echo stripslashes($this->lang->line('shipping_address_comm')); } else echo "Address"; ?></label>
		<input class="popup_add_input" id="address1" name="address1" type="text" placeholder="<?php if($this->lang->line('header_address1') != '') { echo stripslashes($this->lang->line('header_address1')); } else echo "Address Line1"; ?>">
		<input class="popup_add_input" id="address2" name="address2" type="text" placeholder="<?php if($this->lang->line('header_address2') != '') { echo stripslashes($this->lang->line('header_address2')); } else echo "Address Line2"; ?>">
		</p>
    
          <p class="name-account"><label class="label"><?php if($this->lang->line('header_city') != '') { echo stripslashes($this->lang->line('header_city')); } else echo "City"; ?></label>
		<input class="popup_add_input width180" name="city" type="text" id="city">
		</p>
        <p class="name-account"><label class="label"><?php if($this->lang->line('header_zip_postal') != '') { echo stripslashes($this->lang->line('header_zip_postal')); } else echo "Zip / Postal Code"; ?></label>
		<input name="postal_code" class="popup_add_input width108" id="postal_code" type="text">
		</p>
         <p class="name-account"><label class="label"><?php if($this->lang->line('header_state_province') != '') { echo stripslashes($this->lang->line('header_state_province')); } else echo "State / Province"; ?></label>
		<input class="popup_add_input width108" name="state" type="text" id="state">
		</p>
        
        <p class="name-account"><label class="label"><?php if($this->lang->line('header_country') != '') { echo stripslashes($this->lang->line('header_country')); } else echo "Country"; ?></label>
		<select name="country" class="full required">
						<?php 
						if ($countryList->num_rows()>0){
							foreach ($countryList->result() as $country){
						?>
						<option value="<?php echo $country->country_code;?>"><?php echo $country->name;?></option>
						<?php 
							}
						}
						?>
					</select>
		</p>
      
       <p class="name-account"><label class="label"><?php if($this->lang->line('header_telephone') != '') { echo stripslashes($this->lang->line('header_telephone')); } else echo "Telephone"; ?></label>
              <input class="popup_add_input width180" name="phone" type="text" id="phone">
		</p><br>
        <p class="effete">
        <label class="radio-button" for="men"><input name="set_default" id="make_this_primary_addr" value="true" type="checkbox"><?php if($this->lang->line('header_make_primary') != '') { echo stripslashes($this->lang->line('header_make_primary')); } else echo "Make this my primary shipping address"; ?></label>
        
        </p>
        
               </dd></dl>   
        <dl class="profile-account"></dl>
    <div class="address-frm">
        <div class="btn-wrap">
<div class="btn-area">
       
           <button type="submit" class="btn-blue btn-save"><?php if($this->lang->line('header_save') != '') { echo stripslashes($this->lang->line('header_save')); } else echo "Save"; ?></button>
            <button type="reset" class="btn-gray btn-cancel"><?php if($this->lang->line('header_cancel') != '') { echo stripslashes($this->lang->line('header_cancel')); } else echo "Cancel"; ?></button>
           
           </div>
        </div>
        </div>
           <input type="hidden" class="ship_id" name="ship_id" />
			<input type="hidden" name="user_id" value="<?php echo $loginCheck;?>"/>
		</form>
        
        </div></div></div>
        <style>
.profile-account1 dd{
    border-bottom: 0.1em solid #E2E5E7;
    padding: 0 0.78em 1.6em;
}
.profile-account1 .label {
    display: block;
    font-size: 1.1em;
    font-weight: bold;
    padding: 1.45em 0 0.35em;
}
.profile-account1 dd{margin:0;}
.profile-account1 small {
    color: #8A8F9C;
    font-size: 0.94em;
}
.profile-account1 input,.profile-account1 textarea{width:100%;}
.profile-account1 textarea{
    height: 10em;
}
.profile-account1 select{width:100%;}
.profile-account1 .years select{
    background-position: 100% 50%;
    border: 0.1em solid #CFD0D2;
    border-radius: 0.15em;
    box-shadow: 0 0.1em 0.1em #F2F2F2;
    font-size: 1em;
    height: 2.4em;
    padding:0.4em 1.6em 0.5em 0.4em;
	 text-indent: 0.01px;
   text-overflow: "";
   vertical-align: inherit;
   background-image:url(../images/select-box.png);
   background-repeat:no-repeat;
      background-size: 17px auto;
}
.profile-account1 p.effete input{width:auto;}
.profile-account1 .dolar small {
    font-size: 0.85em;
    font-weight: normal;
	float:right; margin:0  0 0 3px;
}
.profile-account1 select{
	 background-size: 14px 5px;
	  background-position: 100% 50%;
    border: 0.1em solid #CFD0D2;
    border-radius: 0.15em;
    box-shadow: 0 0.1em 0.1em #F2F2F2;
    font-size: 1em;
    height: 2.4em;
	background-image:url(../images/select-box.png);
	background-repeat:no-repeat;
    padding: 0 1.6em 0 0.4em;
}
.profile-account1 input[type="radio"]{width:auto;}

.address-frm{
width:100%;
}
.address-frm .btn-area button{ font-size: 1.1em;
    height: 2.65em;
    line-height: 2.6em;
    width: 49.5%;}
.btn-gray {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background: -moz-linear-gradient(center top , #FAFAFA, #F0F0F0) repeat scroll 0 0 rgba(0, 0, 0, 0);
    border-color: #C3C3C3 #BEBEBE #B6B6B6;
    border-image: none;
    border-radius: 0.15em;
    border-style: solid;
    border-width: 1px;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
    color: #4C4F53;
    display: inline-block;
    font-size: 0.94em;
    font-weight: bold;
    line-height: 2.45em;
    padding: 0 1em;
    text-shadow: 0 1px 0 #FFFFFF;}
.user-image{background: none repeat scroll 0 0 #FFFFFF; border: 1px solid #E5E5E5; position: relative; width: 10%; height:130px;  margin: 0 auto;}
.user-image .hidden {display: block !important; height: 150px; opacity: 0; position: relative; width: 100%; z-index: 1;}
.user-image #img_prev{width:131px; height:132px;}

.btn-green {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background: -moz-linear-gradient(center top , #6FBF4A, #5DB535) repeat scroll 0 0 rgba(0, 0, 0, 0);
    border-color: #569D37 #549839 #51923D;
    border-image: none;
    border-radius: 0.15em;
    border-style: solid;
    border-width: 1px;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);
    color: #FFFFFF;
    font-size: 1.1em;
    font-weight: bold;
    line-height: 2.65em;
    text-shadow: 0 -1px 1px rgba(0, 0, 0, 0.26);
}
.cart-coupon .label {
    display: block;
    font-size: 1.1em;
    font-weight: bold;
    padding: 1.45em 1em 0.35em;
}
.cart-coupon dd{margin:0;

}
.ic-check {
    background-position: -155px -50px;
    display: inline-block;
    height: 10px;
    width: 14px;
	background-image: url("images/site/settings.png");
}
.shippingtbl tr td {
		
	text-align:center;
	white-space:nowrap;
}
</style>
<script>
function cart_validation(){
var full_name=document.forms["cartForm"]["full_name"].value;
var nick_name=document.forms["cartForm"]["nick_name"].value;
var address1=document.forms["cartForm"]["address1"].value;
var city=document.forms["cartForm"]["city"].value;
var state=document.forms["cartForm"]["state"].value;
var postal_code=document.forms["cartForm"]["postal_code"].value;
var phone=document.forms["cartForm"]["phone"].value;
if (full_name==null || full_name=="")
  {
  alert("Please Enter your Full Name");
  return false;
  }
  else if(nick_name==""){
     alert("Please enter the shipping Nickname");
     return false;
  }
  else if(address1==null || address1==""){
     alert("Please Enter your Address");
     return false;
  }
  else if(city==null || city==""){
     alert("Please Enter your City");
     return false;
  }
  else if(postal_code==null || postal_code==""){
     alert("Please Enter your ZipCode");
     return false;
  }
  else if(state==null || state==""){
     alert("Please Enter your State");
     return false;
  }
   else if(isNaN(phone) || phone==""){
     alert("Please Enter your Phone no");
     return false;
  }
}
  
	$('.add-shipping-addr').click(function(){
		$('.profile-account1,.btn-save, .btn-cancel').show();
		$('.profile-account').hide();
		//look at here
	});
	$('.btn-cancel').click(function(){
		$('.profile-account1,.btn-save,.btn-cancel').hide();
		$('.profile-account').show();//look at here
	});
	function hideShippingForm()
          {
			$('.profile-account1, .btn-save, .btn-cancel').hide();
          }
	  window.onload=hideShippingForm;
	
</script>
<script type="text/javascript" src="js/site/<?php echo SITE_COMMON_DEFINE ?>address_helper.js"></script>
<script type="text/javascript" src="js/site/jquery.validate.js"></script>
<script>

	$("#shippingEditForm").validate();
	$("#shippingAddForm").validate();

	jQuery(function($) {
		var $select = $('.gift-recommend select.select-round');
		$select.selectBox();
		$select.each(function(){
			var $this = $(this);
			if($this.css('display') != 'none') $this.css('visibility', 'visible');
		});
	});
</script>
<script>
$(document).ready(function(){
	$('.remove').click(function(event){
			var $row = $(this).closest('tr');
			event.preventDefault();
			//if($row.attr('isdefault')) return alert(gettext('You cannot remove your default address.'));
			if(!confirm('Do you really want to remove this shipping address?')) return;
			$.ajax({
				type : 'post',
				url  : baseURL+'site/user_settings/remove_shipping_addr',
				data : {id:$row.attr('aid')},
				dataType : 'json',
				success  : function(json){
					if(json.status_code === 1){
						$row.fadeOut('fast', function(){$row.remove()});
					} else if (json.status_code === 0){
						if(json.message) alert(json.message);
					}
				}
			})
		});
});

</script>