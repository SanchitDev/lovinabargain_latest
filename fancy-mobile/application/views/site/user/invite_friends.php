<?php $this->load->view('site/templates/header'); ?>
<script type="text/javascript">
  (function() {
   var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
   po.src = 'https://apis.google.com/js/client:plusone.js';
   var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
 })();
</script>
<?php //echo "<pre>"; print_r($userDetails->result()); die;?>
<div style="padding:0" class="invite" id="content">
	<h2 class="invite-content"><b><?php if($this->lang->line('onboarding_invite_friends') != '') { echo $this->lang->line('onboarding_invite_friends'); } else echo "Invite friends to" ;echo " ".$siteTitle;?></b></h2>
	<div class="social-icon">
		<ul>
			<li>
				<button class="fb facebook"><i class="icon"></i><?php if($this->lang->line('signup_facebook') != '') { echo stripslashes($this->lang->line('signup_facebook')); } else echo "Facebook"; ?></button>
			</li>
			<li>
				<button class="twt twitter"><i class="icon"></i><?php if($this->lang->line('signup_twitter') != '') { echo stripslashes($this->lang->line('signup_twitter')); } else echo "Twitter"; ?></button>
			</li>
			<li>
				<span id="myBtn" class="demo g-interactivepost gpt"
					data-clientid="<?php echo $this->config->item("google_client_id");?>"
					data-contenturl="<?php echo base_url();?>"
					data-calltoactionlabel="INVITE"
					data-calltoactionurl="<?php echo base_url();?>"
					data-cookiepolicy="single_host_origin"
					data-prefilltext="Join me on Oliver and discover amazing things!﻿">
					<span class="icon"></span>
					<span id="google-plus" class="label btn_gplus" style="cursor:pointer;"><?php if($this->lang->line('signup_google+') != '') { echo stripslashes($this->lang->line('signup_google+')); } else echo "Google+"; ?></span>
				</span>
			</li>
		<!--	<li>
				<button class="gmails gmail"><i class="icon"></i><?php if($this->lang->line('onboarding_gmail') != '') { echo stripslashes($this->lang->line('onboarding_gmail')); } else echo "Gmail"; ?></button>
			</li>-->
		</ul>
	</div>
	<p class="protect"><i class="icon"></i><?php echo site_lg('lg_share_contact', "We won't share your contacts with anybody or email anyone without your consent.");?></p>
	<dl class="address">
		
		<dd>
			
		</dd>
	</dl>
	
</div>
<script	src="http://connect.facebook.net/en_US/all.js"></script>
<script type="text/javascript">
FB.init({
	    appId:'<?php echo $this->config->item('facebook_app_id');?>',
	    cookie:true,
	    status:true,
	    xfbml:true,
		oauth : true
    });

$('button.twitter').click(function() {
	var loc = baseURL;
	var param = {'location':loc};
	var popup = window.open(null, '_blank', 'height=400,width=800,left=250,top=100,resizable=yes', true);			
  var $btn = $(this);
  $.post(
		baseURL+'site/user/find_friends_twitter',
		param, 
		function(json){
			if (json.status_code==1) {
				popup.location.href = json.url;						
				}
			else if (json.status_code==0) {
				alert(json.message);
			}  
		},
		'json'
	);
});

$('button.facebook').click(function() {
	FB.ui({
	    method: 'apprequests',
	    message: 'Invites you to join on <?php echo $siteTitle;?> (<?php echo base_url();?>?ref=<?php echo $userDetails->row()->user_name;?>)'
	});
    });
$('button.gmail').click(function() {
  var loc = location.protocol+'//'+location.host;
 var param = {'location':loc};
	var popup = window.open(null, '_blank', 'height=550,width=900,left=250,top=100,resizable=yes', true);
  var $btn = $(this);
	$.post(
		baseURL+'site/user/find_friends_gmail',
		param, 
		function(json){
			if (json.status_code==1) {
				popup.location.href = json.url;	
			}
			else if (json.status_code==0) {
				alert(json.message);
			}  
		},
		'json'
	);
});
</script>
