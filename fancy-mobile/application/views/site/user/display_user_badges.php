<?php
$this->load->view('site/templates/header',$this->data);
?>
<link rel="stylesheet" href="css/site/<?php echo SITE_COMMON_DEFINE ?>timeline.css" type="text/css" media="all"/>
<link rel="stylesheet" media="all" type="text/css" href="css/site/<?php echo SITE_COMMON_DEFINE ?>profile.css" />
<!-- Section_start -->
<div class="lang-en wider no-subnav">
   <div id="container-wrapper">
      <div class="container usersection">
         <div class="icon-cache"></div>
            <div id="tooltip"></div>
            <div class="wrapper-content right-sidebar">
               <?php if($flash_data != '') { ?>
		       <div class="errorContainer" id="<?php echo $flash_data_type;?>">
			      <script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
			      <p><span><?php echo $flash_data;?></span></p>
		       </div>
		       <?php } ?>
               <div id="content">
                  <div class="wrapper timeline normal">
	                 <h2 class="tit"><span class="username"><a href="user/<?php echo $userProfileDetails->row()->user_name;?>"><?php echo $userProfileDetails->row()->full_name;?></a> /</span> <?php if($this->lang->line('display_badges') != '') { echo stripslashes($this->lang->line('display_badges')); } else echo "Badges"; ?></h2>
	                 <ul class="badge-list">
					  <?php if($badgesList->num_rows()>0){?>
					      <?php foreach($badgesList->result() as $_badgesList){?>
						   <li>
						      <div class="deal">
							  <a>
							    <img src="<?php echo DESKTOPURL;?>images/badges/<?php echo $_badgesList->badge_image;?>"/>
							  </a>
							  <b class="dealname"><?php echo $_badgesList->name; ?></b><br/>
							  <span class="deal-info"><?php $dates = $_badgesList->created;
							      echo $created_date = date('d M, Y', strtotime($dates));
 							     ?></span><br/>
							  <span class="info"><?php echo $_badgesList->description; ?></span>
							  </div>
						   </li>
					  <?php }}?>
                     </ul>
	              </div>
				  <div id="infscr-loading" style="display:none">
					 <span class="loading"><?php echo site_lg('lg_loading', 'Loading...');?></span>
				  </div>
              </div>
              <?php $this->load->view('site/user/display_user_sidebar');?>
          </div>
          <?php $this->load->view('site/templates/footer_menu'); ?>
          <a href="#header" id="scroll-to-top"><span><?php if($this->lang->line('signup_jump_top') != '') { echo stripslashes($this->lang->line('signup_jump_top')); } else echo "Jump to top"; ?></span></a> 
	</div>
  <!-- / container -->
  </div>
</div>
<!-- Section_start -->
<?php $this->load->view('site/templates/footer'); ?>