<?php
$this->load->view('site/templates/header', $this->data);
$this->load->view('site/popup/feature_product');
?>
<style>
    .promoted_products img{
        width: 25%;
    }    
</style>
<link rel="stylesheet" href="<?php echo base_url(); ?>css/site/<?php echo SITE_COMMON_DEFINE ?>timeline.css" type="text/css" media="all"/>
<link rel="stylesheet" media="all" type="text/css" href="css/site/<?php echo SITE_COMMON_DEFINE ?>setting.css">
<!-- Section_start -->
<div class="lang-en no-subnav wider winOS">
    <!-- Section_start -->
    <div id="container-wrapper">
        <div class="container set_area">
            <?php if ($flash_data != '') { ?>
                <div class="errorContainer" id="<?php echo $flash_data_type; ?>">
                    <script>setTimeout("hideErrDiv('<?php echo $flash_data_type; ?>')", 3000);</script>
                    <p><span><?php echo $flash_data; ?></span></p>
                </div>
            <?php } ?>


            <div id="content">
                <h2 class="ptit"><?php if ($this->lang->line('featured_seller') != '') {
                    echo stripslashes($this->lang->line('featured_seller'));
                } else echo "Featured Seller"; ?></h2>
<?php if ($is_featured->num_rows == 0) { ?>
                    <div class=" section shipping no-data">
                        <p><?php if ($this->lang->line('no_featured_plan') != '') {
                            echo stripslashes($this->lang->line('no_featured_plan'));
                        } else echo "You dont have a featured plan"; ?></p>

                        <a href="<?php echo base_url() . 'featured_plan' ?>" ><button class="btn-shipping add_"> <?php if ($this->lang->line('make_featured') != '') {
                    echo stripslashes($this->lang->line('make_featured'));
                } else echo "Make my profile featured"; ?></button></a>
                    </div>                
<?php }else { ?>
                    <div class="section shipping">
                        <h3><?php if ($this->lang->line('current_featured_plan') != '') {
                            echo stripslashes($this->lang->line('current_featured_plan'));
                        } else echo "Current Featured Plan"; ?></h3>
                        <div class="chart-wrap">
                            <table class="chart">
                                <thead>
                                    <tr>
                                        <th>Featured From</th>
                                        <th>Featured To</th>
                                        <th>Plan Price</th>
                                        <th>Plan Period</th>
                                        <th>Max Featured Product</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?php echo $is_featured->row()->featuredOn ?></td>
                                        <td><?php echo $is_featured->row()->expiresOn ?></td>
                                        <td><?php echo $currencySymbol.$is_featured->row()->planPrice ?></td>
                                        <td><?php echo $is_featured->row()->planPeriod ?></td>
                                        <td id="featuredProduct_count"><?php echo $is_featured->row()->productCount ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <button class="btn-shipping add_ show_feature_products" id="show_featured_products"><i class="ic-plus"></i> Add featured product</button>
                        <br><br><br>
                        <h2>Promoted Products</h2>
                        <div class="promoted_products">
                        <?php if(count($featuredProducts) > 0){
                                foreach($featuredProducts as $product){ 
                                    $prodImg = 'dummyProductImage.jpg';
                                    if(file_exists('../images/product/'.trim($product->image,','))){
                                        $prodImg = trim($product->image,',');
                                    }
                                    ?>
                        <img src="<?php echo DESKTOPURL.'images/product/'.$prodImg; ?>" />
                      <?php          }
                        }else{
                            echo 'No Promoted products';
                        } ?>
                        </div>
                    </div>
<?php } ?>
            </div>


<?php
//$this->load->view('site/user/settings_sidebar');
//$this->load->view('site/templates/side_footer_menu');
?>

        </div>
        <!-- / container -->
    </div>
</div>

<!-- Section_start -->
<script type="text/javascript" src="js/site/jquery.validate.js"></script>

<?php
$this->load->view('site/templates/footer', $this->data);
?>
