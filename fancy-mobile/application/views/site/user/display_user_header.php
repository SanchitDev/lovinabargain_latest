<?php $this->load->view('site/templates/header',$this->data);
if($userProfileDetails->row()->thumbnail == ''){
	$thumbImg = 'user-thumb1.png';
}else{
	$thumbImg = $userProfileDetails->row()->thumbnail;
} 
			 $followClass = 'follow';
        	if ($loginCheck != ''){
	        $followingListArr = explode(',', $userDetails->row()->following);
	        if (in_array($userProfileDetails->row()->id, $followingListArr)){
	        	$followClass = 'follow2';
	        }
        } 	
        //echo "<pre>";print_r($userProfileDetails->result());die;	
 ?>
<div id="content">
	<div class="figure-product">
		<div class="follower-index">
			<img class="member" src="<?php echo DESKTOPURL;?>images/users/<?php echo $thumbImg;?>" /><h2 class="memname"><?php echo $userProfileDetails->row()->user_name;?></h2>
			<?php if ($userProfileDetails->row()->web_url!=''){?>
                <a target="_blank" class="online" href="<?php echo prep_url($userProfileDetails->row()->web_url);?>"><i class="icon"></i><?php echo prep_url($userProfileDetails->row()->web_url);?></a>
			<?php }?>
		</div> 
		<dl class="explain">
			<dt><a href="javascript:void(0);"><i class="icon arrow"></i></a></dt>
			<?php 
	          $fb_link = $userProfileDetails->row()->facebook;
	          if (substr($fb_link, 0,4)!='http') $fb_link='http://'.$fb_link;
	          $tw_link = $userProfileDetails->row()->twitter;
	          if (substr($tw_link, 0,4)!='http') $tw_link='http://'.$tw_link;
	          $go_link = $userProfileDetails->row()->google;
	          if (substr($go_link, 0,4)!='http') $go_link='http://'.$go_link;
	          ?>
			<ul>
			<?php if ($tw_link!='http://'){?>
				<li><a href="<?php echo $tw_link;?>" target="_blank"><i class="icon"></i><?php echo site_lg('lg_twitter', 'Twitter');?></a></li>
			<?php 
			}
			if ($fb_link!='http://'){
			?>
				<li><a href="<?php echo $fb_link;?>" target="_blank"><i class="icon"></i><?php echo site_lg('lg_facebook', 'Facebook');?></a></li>
			<?php 
			}
			if ($go_link!='http://'){
			?>
				<li><a href="<?php echo $go_link;?>" target="_blank"><i class="icon"></i><?php echo site_lg('lg_google+', 'Google+');?></a></li>
			<?php }?>	
			</ul>
		</dl>
		<div style="margin:0 0 4px 0" class="following">
			<a href="user/<?php echo $userProfileDetails->row()->user_name;?>/followers"><b style="margin:0 5px 0 0"><?php echo $userProfileDetails->row()->followers_count;?></b><?php echo site_lg('lg_followers', 'Followers');?></a>
			<a href="user/<?php echo $userProfileDetails->row()->user_name;?>/following"><b style="margin:0 5px 0 0"><?php echo $userProfileDetails->row()->following_count;?></b><?php echo site_lg('lg_following', 'Following');?></a>
            <?php if($userProfileDetails->row()->id != $loginCheck){ ?>
            <button <?php if ($loginCheck==''){?> onclick="javascript:window.location.href = 'login';" <?php }?> style="float:right;"  class="button-<?php echo $followClass; ?>" uid="<?php echo $userProfileDetails->row()->id;?>"><i class="icon"></i></button>
            
            
           <!-- <button class="button-follow2"><i class="icon"></i></button>  -->
            <?php } ?>
            <?php if($userProfileDetails->row()->id == $loginCheck && $userProfileDetails->row()->store_payment != 'Paid' && $userProfileDetails->row()->group == 'Seller'){ ?>
            <a href="<?php echo base_url(); ?>open-store"><button style="float:right;"><i class="icon"></i>Open-Store</button></a>
            
            
           <!-- <button class="button-follow2"><i class="icon"></i></button>  -->
            <?php } ?>            
		</div>
           
	</div>
	<div class="figure-product">
		<ul class="tab-menu" style="overflow-x: scroll;white-space: nowrap;">
            <li><a href="user/<?php echo $userProfileDetails->row()->user_name;?>"><?php if($this->uri->segment(3) == ''){?> <b style="color:#3C71B5;"><?php }else{?><b><?php } ?><?php echo LIKED_BUTTON;?></b></a></li>
            <li><a href="user/<?php echo $userProfileDetails->row()->user_name;?>/added"> <?php if($this->uri->segment(3) == 'added'){?> <b style="color:#3C71B5;"><?php }else{?><b><?php } ?>  <?php if($this->lang->line('display_added') != '') { echo stripslashes($this->lang->line('display_added')); } else echo "Added"; ?> </b></a></li>
            <li><a href="user/<?php echo $userProfileDetails->row()->user_name;?>/lists"><?php if($this->uri->segment(3) == 'lists'){?> <b style="color:#3C71B5;"><?php }else{?><b><?php } ?><?php if($this->lang->line('display_lists') != '') { echo stripslashes($this->lang->line('display_lists')); } else echo "Lists"; ?></b></a></li>
            <li><a href="user/<?php echo $userProfileDetails->row()->user_name;?>/owns"><?php if($this->uri->segment(3) == 'owns'){?> <b style="color:#3C71B5;"><?php }else{?><b><?php } ?><?php if($this->lang->line('display_owns') != '') { echo stripslashes($this->lang->line('display_owns')); } else echo "Owns"; ?></b></a></li>
            <li><a href="user/<?php echo $userProfileDetails->row()->user_name;?>/wants"><?php if($this->uri->segment(3) == 'wants'){?> <b style="color:#3C71B5;"><?php }else{?><b><?php } ?><?php if($this->lang->line('display_wants') != '') { echo stripslashes($this->lang->line('display_wants')); } else echo "Wants"; ?></b></a></li>
			<li><a href="user/<?php echo $userProfileDetails->row()->user_name;?>/activity"><?php if($this->uri->segment(3) == 'activity'){?> <b style="color:#3C71B5;"><?php }else{?><b><?php } ?><?php echo site_lg('lg_activity', 'Activity');?></b></a></li>
		</ul>  
	</div>
<script type="text/javascript">
$(".explain ul").hide();
$(".arrow").click(function(){
	$(".explain ul").toggle("slow");
});
</script>