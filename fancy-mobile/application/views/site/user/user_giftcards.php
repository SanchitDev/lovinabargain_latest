<?php
$this->load->view('site/templates/header');
?>
<link rel="stylesheet" media="all" type="text/css" href="css/site/<?php echo SITE_COMMON_DEFINE ?>setting.css">
<!-- Section_start -->
    
    
    <div class="giftcard account-page" id="content">
	<div style=" margin-top: 20px;" class="figure-product">
		<h2 class="comments-title"><?php if($this->lang->line('giftcard_cards') != '') { echo stripslashes($this->lang->line('giftcard_cards')); } else echo "Gift Cards"; ?></span></h2>
		
        
<?php 
                if($giftcardsList->num_rows()>0){
                ?>	
                <div class="chart-wrap">
				<div class="fullpay">      
					<ul  class="chart cart-head">
                <li><?php if($this->lang->line('giftcard_code') != '') { echo stripslashes($this->lang->line('giftcard_code')); } else echo "Code"; ?></li>
                        <li><?php if($this->lang->line('giftcard_sendername') != '') { echo stripslashes($this->lang->line('giftcard_sendername')); } else echo "Sender Name"; ?></li>
                        <li><?php if($this->lang->line('giftcard_sender_mail') != '') { echo stripslashes($this->lang->line('giftcard_sender_mail')); } else echo "Sender Mail"; ?></li>
                        <li><?php if($this->lang->line('giftcard_price') != '') { echo stripslashes($this->lang->line('giftcard_price')); } else echo "Price"; ?></li>
                        <li><?php if($this->lang->line('giftcard_expireson') != '') { echo stripslashes($this->lang->line('giftcard_expireson')); } else echo "Expires on"; ?></li>
                        <li><?php if($this->lang->line('giftcard_card_stats') != '') { echo stripslashes($this->lang->line('giftcard_card_stats')); } else echo "Card Status"; ?></li>
					</ul>   
					
			<ul  class="chart">
						<?php foreach ($giftcardsList->result() as $row){
                $status = $row->card_status;
                    	if ($status == 'not used'){
                    		$expDate = strtotime($row->expiry_date);
                    		if ($expDate<time()){
                    			$status = 'expired';
                    		}
                    	}?>
						<li>#<?php echo $row->code;?></li>
                        <li><?php echo $row->sender_name;?></li>
                        <li><?php echo $row->sender_mail;?></li>
                        <li><?php echo $row->price_value;?></li>
                        <li><?php echo $row->expiry_date;?></li>
                        <li><?php echo ucwords($status);?>
							</li>
						<?php } ?>
					</ul> 
               
				</div>
			</div>
			 <p style="margin-left:30px;">
				<a href="gift-cards"><?php if($this->lang->line('giftcard_send') != '') { echo stripslashes($this->lang->line('giftcard_send')); } else echo "Send a Gift Card"; ?></a>
			</p>
			<?php } else {?>
			<div class=" section giftcard no-data">
				<span class="icon"><i class="ic-card"></i></span>
				<p><?php if($this->lang->line('giftcard_not_receive') != '') { echo stripslashes($this->lang->line('giftcard_not_receive')); } else echo "You haven't received any gift cards yet"; ?>.<br>
				<a href="gift-cards"><?php if($this->lang->line('giftcard_send') != '') { echo stripslashes($this->lang->line('giftcard_send')); } else echo "Send a Gift Card"; ?></a></p>
			</div>
			<?php } ?>
	



           
			</div>
            </div>    


<!-- Section_start -->
<script type="text/javascript" src="js/site/<?php echo SITE_COMMON_DEFINE ?>address_helper.js"></script>
<script>
	jQuery(function($) {
		var $select = $('.gift-recommend select.select-round');
		$select.selectBox();
		$select.each(function(){
			var $this = $(this);
			if($this.css('display') != 'none') $this.css('visibility', 'visible');
		});
	});
</script>
<script>
<?php 
$this->load->view('site/templates/footer');
?>
