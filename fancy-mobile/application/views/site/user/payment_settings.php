<?php 
$this->load->view('site/templates/header');?>
<style>
.set_area .chart {
    width: 100%;
}
.set_area .chart thead th {
    background: none repeat scroll 0 0 #F5F6F8;
    border-top: 1px solid #D9DADD;
    color: #606163;
    font-size: 12px;
    font-weight: bold;
    height: 20px;
    padding: 5px 0 5px 9px;
    vertical-align: middle;
	text-align:left;
}
.set_area .chart tbody tr:hover td {
    background: none repeat scroll 0 0 #F8F9FA;
}
.set_area .chart tbody td {
    border-bottom: 1px solid #DBDCDE;
    color: #6E7176;
    font-size: 12px;
    line-height: 20px;
    padding: 8px 0 8px 9px;
    vertical-align: middle;
}
.set_area .chart tbody td b, .set_area .chart tbody td.date, .set_area .chart tbody td .persent, .set_area .chart tbody td .cancel {
    color: #393D4D;
    font-style: normal;
}
.set_area tbody td .ic-check {
    background-position: -155px -50px;
    display: inline-block;
    height: 10px;
    width: 14px;
}
.set_area .section .chart-wrap {
    border: 1px solid #D9DBE1;
    border-radius: 3px 3px 3px 3px;
    margin: 28px 0 24px;
}
.set_area .section .chart-wrap thead th, .set_area .section .chart-wrap tbody tr:last-child td {
    border: 0 none;
}
.set_area .section .chart tbody tr td {
    border-bottom-color: #D9DBE1;
    color: #393D4D;
    line-height: 16px;
    padding: 15px 11px;
    vertical-align: top;
}
.set_area .section .chart tbody tr td small {
    color: #6E7176;
    font-size: 12px;
}
.set_area .section .chart tbody tr td.btns {
    padding: 15px 0;
    text-align: center;
    width: 105px;
}
.set_area .section .chart tbody tr:hover td {
    background: none repeat scroll 0 0 transparent;
}



.payment-list .tit a {
    color: #588CC7;
    float: right;
    font-size: 0.94em;
    font-weight: normal;
}
</style>
<?php if(isset($countryList) && $this->uri->segment(2) == 'payment-method' || isset($countryList) && $this->uri->segment(1) == 'cart'){
?>
<?php if($flash_data != '') { ?>
		<div class="errorContainer" id="<?php echo $flash_data_type;?>">
			<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
            
			<p><span><?php echo $flash_data;?></span></p>
		</div>
		<?php } ?>
	<div id="content" class="account-page">
        <div class="account-wrap">

 <dl class="profile-account set_area">
       <?php if ($paymentList->num_rows() == 0){?> <p class="effete">
        <label class="label" style="margin: 0px 13px;"><?php if($this->lang->line('save_credit_cards') != '') { echo stripslashes($this->lang->line('save_credit_cards')); } else echo "Saved Credit Cards"; ?><button class="btn-payment add_payment" onclick="showForm();" style="float:right; color: #588CC7;float: right;font-size: 0.94em;font-weight: normal;margin: 0px 13px;
}"><i class="ic-plus"></i> <?php if($this->lang->line('add_payment_method') != '') { echo stripslashes($this->lang->line('add_payment_method')); } else echo "Add Payment Method"; ?></button></label><br /></p>
        
        <?php 
	   }else{
	   
	   ?>
	   <div class="section payment">
            <h3><?php if($this->lang->line('save_payment_details') != '') { echo stripslashes($this->lang->line('save_payment_details')); } else echo "Your Saved Payment Details"; ?></h3>
                	<div class="chart-wrap">
            <table class="chart">
                <thead>
                    <tr>
                        <th><?php if($this->lang->line('name_on_card') != '') { echo stripslashes($this->lang->line('name_on_card')); } else echo "Name on card"; ?></th>
                        <th><?php if($this->lang->line('card_number') != '') { echo stripslashes($this->lang->line('card_number')); } else echo "Card number"; ?></th>
                        <th><?php if($this->lang->line('shipping_address_comm') != '') { echo stripslashes($this->lang->line('shipping_address_comm')); } else echo "Address"; ?></th>
                        <th><?php if($this->lang->line('payment_created') != '') { echo stripslashes($this->lang->line('payment_created')); } else echo "Created"; ?></th>
						<th><?php if($this->lang->line('product_actioin') != '') { echo stripslashes($this->lang->line('product_actioin')); } else echo "Action"; ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($paymentList->result() as $row){?>
                    <tr aid="<?php echo $row->id;?>">
                        <td><?php echo $row->full_name;?></td>
                        <td><?php 
                        $key = 'team-clone-tweaks';
                        echo $this->encrypt->decode($row->card_no, $key);?></td>
                        <td><?php echo $row->address1.', '.$row->address2.'<br/>'.$row->city.'<br/>'.$row->state.'<br/>'.$row->country.'-'.$row->postal_code;?></td>
						<td><?php $date = $row->created;
						          $date = strtotime($date);
								  echo date('M d, Y, h:i', $date);
						?></td>
                        <td><a  href="payment-method-edit/<?php echo $row->id;?>" ><?php if($this->lang->line('shipping_edit') != '') { echo stripslashes($this->lang->line('shipping_edit')); } else echo "Edit"; ?></a> / <a class="payment-remove" aid="<?php echo $row->id;?>"><?php if($this->lang->line('shipping_delete') != '') { echo stripslashes($this->lang->line('shipping_delete')); } else echo "Delete"; ?></a></td>
                    </tr>
                    <?php }?>
                    
                </tbody>
            </table>
			</div>
            	 <button class="btn-payment add_payment" onclick="showForm();" style="float:right; color: #588CC7;float: right;font-size: 0.94em;font-weight: normal;margin: -20px 0;><i class="ic-plus"></i> <?php if($this->lang->line('add_payment_method') != '') { echo stripslashes($this->lang->line('add_payment_method')); } else echo "Add Payment Method"; ?></button>
			</div>
	<?php 
	}
	?>
</dl>

             
        <dl class="profile-account" id="payment_setting_form_hide">
        
         <form class="ltxt" action="site/user_settings/insertEdit_payment_method" method="post" name ="paymentForm" id="paymentForm" onsubmit="return payment_validation()">
        <dd>
       
        <p class="effete">
        <label class="label"><?php if($this->lang->line('payment_info') != '') { echo stripslashes($this->lang->line('payment_info')); } else echo "Payment Info"; ?></label><br /></p>
                
        <p class="name-account"><label class="label"><?php if($this->lang->line('name_on_card') != '') { echo stripslashes($this->lang->line('name_on_card')); } else echo "Name on card"; ?></label>
		<input id="full_name" name="full_name" class="popup_add_input" type="text" placeholder="<?php if($this->lang->line('payment_home_work') != '') { echo stripslashes($this->lang->line('payment_home_work')); } else echo "E.g. Aunt Jane"; ?>">
		</p>
        
        <p class="name-account"><label class="label"><?php if($this->lang->line('card_number') != '') { echo stripslashes($this->lang->line('card_number')); } else echo "Card number"; ?></label>
		<input id="card_no" name="card_no" class="popup_add_input width180" type="text">
		</p>
        
        <p class="name-account"><label class="label"><?php if($this->lang->line('payment_cvv') != '') { echo stripslashes($this->lang->line('payment_cvv')); } else echo "CVV "; ?></label>
		<input id="card_cvv" name="card_cvv" class="popup_add_input width70" type="text">
		</p>
        
      <p class="name-account"><label class="label"><?php if($this->lang->line('expiration_date') != '') { echo stripslashes($this->lang->line('expiration_date')); } else echo "Expiration date"; ?></label>

        
		<select id="CCExpDay" name="expiray-day" class="popup_add_input width70 select-round select-white select-date selectBox required">
						<option value="01" <?php if(date('m')=='01'){ echo $Sel;} ?>>01</option>
						<option value="02" <?php if(date('m')=='02'){ echo $Sel;} ?>>02</option>
						<option value="03" <?php if(date('m')=='03'){ echo $Sel;} ?>>03</option>
						<option value="04" <?php if(date('m')=='04'){ echo $Sel;} ?>>04</option>
						<option value="05" <?php if(date('m')=='05'){ echo $Sel;} ?>>05</option>
						<option value="06" <?php if(date('m')=='06'){ echo $Sel;} ?>>06</option>
						<option value="07" <?php if(date('m')=='07'){ echo $Sel;} ?>>07</option>
						<option value="08" <?php if(date('m')=='08'){ echo $Sel;} ?>>08</option>
						<option value="09" <?php if(date('m')=='09'){ echo $Sel;} ?>>09</option>
						<option value="10" <?php if(date('m')=='10'){ echo $Sel;} ?>>10</option>
						<option value="11" <?php if(date('m')=='11'){ echo $Sel;} ?>>11</option>
						<option value="12" <?php if(date('m')=='12'){ echo $Sel;} ?>>12</option>
				   </select>
				   <select id="CCExpMnth" name="expiray-year" class="popup_add_input width80 select-round select-white select-date selectBox required">
						<?php for($i=date('Y');$i< (date('Y') + 25);$i++){ ?>	
							<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
						<?php } ?>    
				   </select>
		</p>
        
        <p class="name-account"><label class="label"><?php if($this->lang->line('checkout_billing_addr') != '') { echo stripslashes($this->lang->line('checkout_billing_addr')); } else echo "Billing Address"; ?></label>
		</p>
        
        <p class="name-account"><label class="label"><?php if($this->lang->line('shipping_address_comm') != '') { echo stripslashes($this->lang->line('shipping_address_comm')); } else echo "Address"; ?></label>
		<input class="popup_add_input" id="address1" name="address1" type="text" placeholder="<?php if($this->lang->line('header_address1') != '') { echo stripslashes($this->lang->line('header_address1')); } else echo "Address Line1"; ?>">
		<input class="popup_add_input" id="address2" name="address2" type="text" placeholder="<?php if($this->lang->line('header_address2') != '') { echo stripslashes($this->lang->line('header_address2')); } else echo "Address Line2"; ?>">
		</p>
        
        <p class="name-account"><label class="label"><?php if($this->lang->line('header_country') != '') { echo stripslashes($this->lang->line('header_country')); } else echo "Country"; ?></label>
		<select name="country" id="country" class="popup_add_input width455">
						<?php 
						if ($countryList->num_rows()>0){
							foreach ($countryList->result() as $country){
						?>
						<option value="<?php echo $country->country_code;?>"><?php echo $country->name;?></option>
						<?php 
							}
						}
						?>
					</select>
		</p>
        
          <p class="name-account"><label class="label"><?php if($this->lang->line('header_city') != '') { echo stripslashes($this->lang->line('header_city')); } else echo "City"; ?></label>
		<input class="popup_add_input width180" name="city" type="text" id="city">
		</p>
        
          <p class="name-account"><label class="label"><?php if($this->lang->line('header_state_province') != '') { echo stripslashes($this->lang->line('header_state_province')); } else echo "State / Province"; ?></label>
		<input class="popup_add_input width108" name="state" type="text" id="state">
		</p>
        
          <p class="name-account"><label class="label"><?php if($this->lang->line('header_zip_postal') != '') { echo stripslashes($this->lang->line('header_zip_postal')); } else echo "Zip / Postal Code"; ?></label>
		<input name="postal_code" class="popup_add_input width108" id="postal_code" type="text">
		</p>
        
        
        
        
        
        <dl class="profile-account">
        
        <dd style=" padding: 1em;">
       
           <button style="width:100%" class="profile-button"><?php if($this->lang->line('apply') != '') { echo stripslashes($this->lang->line('apply')); } else echo "Apply"; ?></button>
           <input type="hidden" id="row_id" name="row_id" value=""/>
			<input type="hidden" name="user_id" value="<?php echo $loginCheck;?>"/>
		</form>
        <?php }?> 
         </dd>
        </dl>
        </div>
     
     
        </div>
<style>
.user-image{background: none repeat scroll 0 0 #FFFFFF; border: 1px solid #E5E5E5; position: relative; width: 10%; height:130px;  margin: 0 auto;}
.user-image .hidden {display: block !important; height: 150px; opacity: 0; position: relative; width: 100%; z-index: 1;}
.user-image #img_prev{width:131px; height:132px;}
</style>

<script>
$(document).ready(function(){
	$('.payment-remove').click(function(event){
			var $row = $(this).closest('tr');
			event.preventDefault();
			if(!confirm('Do you really want to remove this shipping address?')) return;
			$.ajax({
				type : 'post',
				url  : baseURL+'site/user_settings/remove_payment_method',
				data : {id:$row.attr('aid')},
				dataType : 'json',
				success  : function(json){
					if(json.status_code === 1){
						$row.fadeOut('fast', function(){$row.remove()});
					} else if (json.status_code === 0){
						if(json.message) alert(json.message);
					}
				}
			})
		});
	});
</script>

<script>
$(document).ready(function(){
    var popup = $.dialog('newpayment');
	$('.add_payment').click(function(event){
	    popup.open();
		event.preventDefault();
	});
    $('.payment-edit').click(function(event){
		     var $row = $(this).closest('tr');
			 popup.open();
			 event.preventDefault();
			 var paymentID = $(this).attr('aid');
			 $.ajax({
				type:'POST',
				url:baseURL+'site/user_settings/get_upayment_details',
				data:{'paymentID':paymentID},
				dataType:'json',
				success:function(response){
				    $("#row_id").val(response.row_id);
					$('#full_name').val(response.full_name);
					$('#card_no').val(response.card_no);
					$('#card_cvv').val(response.card_cvv);
					$('#address1').val(response.address1);
					$('#address2').val(response.address2);
					$('#city').val(response.city);
					$('#state').val(response.state);
					$('#country').val(response.country);
					$('#postal_code').val(response.postal_code);
					$('#phone').val(response.phone);
					$('#ship_id').val(shipID);
				}
			});
   });	
});
function payment_validation(){
var full_name=document.forms["paymentForm"]["full_name"].value;
var card_no=document.forms["paymentForm"]["card_no"].value;
var card_cvv=document.forms["paymentForm"]["card_cvv"].value;
var address1=document.forms["paymentForm"]["address1"].value;
var city=document.forms["paymentForm"]["city"].value;
var state=document.forms["paymentForm"]["state"].value;
var postal_code=document.forms["paymentForm"]["postal_code"].value;
if (full_name==null || full_name=="")
  {
  alert("Please Enter your Card Name");
  return false;
  }
  else if(isNaN(card_no) || card_no=="" || card_no.length==16){
     alert("Please Enter your Card No Correctly");
     return false;
  }
  else if(card_cvv==""){
     alert(card_cvv.length);
     alert("Please Enter your Card CVV Correctly");
     return false;
  }
  else if(address1==null || address1==""){
     alert("Please Enter your Address");
     return false;
  }
  else if(city==null || city==""){
     alert("Please Enter your City");
     return false;
  }
  else if(state==null || state==""){
     alert("Please Enter your State");
     return false;
  }
  else if(postal_code==null || postal_code==""){
     alert("Please Enter your ZipCode");
     return false;
  }
  
}
       
          function hideForm()
          {
            $("#payment_setting_form_hide").hide();
          }
          function showForm()
          {
            $("#payment_setting_form_hide").toggle();
          }
          hideForm();
</script>