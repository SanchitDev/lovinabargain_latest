<?php $this->load->view('site/templates/header'); 
?>
<script	src="http://connect.facebook.net/en_US/all.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#search-keyword').click(function(){
		window.location.href = "find-friends?key="+$('#search-value').val();
	});
	FB.init({
	    appId:'<?php echo $this->config->item('facebook_app_id');?>',
	    cookie:true,
	    status:true,
	    xfbml:true,
		oauth : true
    });
	$('.find-friend-facebook').click(function(){
		FB.api('/me/friends', function(response){
		if(response.data){
		   var fb_id = []
		   $.each(response.data,function(index,friend) {
			 fb_id.push(friend.id); 
		  });
		  var url = '<?php echo base_url();?>site/user/find_friend_fb';
			$.post(url,{'ids':fb_id},function(data){
				$('#social-links, #suggest-links').hide();
				$('.figure-producta').html(data);
			});
		}else{
		  alert("Error");
		}
		}); 
	});
	$('.find-friend-gmail').click(function(){
		var loc = location.protocol+'//'+location.host;
		var param = {'location':loc};
		var popup = window.open('about:blank', '_blank', 'height=550,width=900,left=250,top=100,resizable=yes', true);
		var $btn = $(this);
		$.post(
			baseURL+'site/user/find_friends_ingmail',
			param, 
			function(json){
			if (json.status_code==1) {
				popup.location.href = json.url;	
			}
			else if (json.status_code==0) {
				alert(json.message);
			}  
			},
			'json'
		);
	});
	$('.find-friend-twitter').click(function(){
		var loc = baseURL;
		var param = {'location':loc};
		var popup = window.open(null, '_blank', 'height=400,width=800,left=250,top=100,resizable=yes', true);			
		var $btn = $(this);
		$.post(
			baseURL+'site/user/twitter_friends',
			param, 
			function(json){
				if (json.status_code==1) {
					popup.location.href = json.url;						
				}else if (json.status_code==0) {
					alert(json.message);
				}  
			},
			'json'
		);
	});
});
</script>
<?php
if($tab =='pick'){
     $social_link = 'display:none';
	 $pick = 'display:block';
	 $brand = 'display:none';
	 $similars = 'display:none';
	 $key = 'display:none';
  }elseif($tab =='brand'){
     $social_link = 'display:none';
	 $pick = 'display:none';
	 $brand = 'display:block';
	 $similars = 'display:none';
	 $key = 'display:none';
  }elseif($tab =='similar'){
     $social_link = 'display:none';
	 $pick = 'display:none';
	 $brand = 'display:none';
	 $similars = 'display:block';
	 $key = 'display:none';
  }elseif($tab =='key'){
     $social_link = 'display:none';
	 $pick = 'display:none';
	 $brand = 'display:none';
	 $similars = 'display:none';
	 $key = 'display:block';
  }else{
     $social_link = 'display:block';
	 $pick = 'display:none';
	 $brand = 'display:none';
	 $similars = 'display:none';
	 $key = 'display:none';
  }  
/*  
if($tab =='key'){
     $social_link = 'display:none';
	 $sugest = 'display:none';
	 $pick = 'display:none';
	 $brand = 'display:none';
	 $similars = 'display:block';
	 $search = 'display:block';
	 $key = 'display:block';
}else{
	$key = 'display:none';
}*/
?>
<div class="findfriends" id="content">
	<ul class="setting-page" style="<?php echo $search;?>">
		<li class="headlines"><?php echo site_lg('lg_search_people', 'Search people on Fancy');?><li>
		<li class="search-place">
			<i class="zoom-search"></i>
			<input type="text" name="" id="search-value" placeholder="Search for people">
			<button class="grey-search" id="search-keyword"><?php echo site_lg('lg_search', 'Search');?></button>
		</li>
    
	</ul>
	<ul id="social-links" class="setting-page" style="<?php echo $social_link;?>">
		<li class="headlines"><?php echo site_lg('lg_social_network', 'Find people on Social network');?><li>
        <li><a class="fab find-friend-facebook" href="javascript:void(0);"><i class="identy"></i><?php echo site_lg('facebbok_friends', 'Facebook friends');?><i class="arrow"></i></a></li>   
        <li><a class="twi find-friend-twitter" href="javascript:void(0);"><i class="identy"></i><?php echo site_lg('lg_twitter_friends', 'Twitter Friends');?><i class="arrow"></i></a></li> 
		<!--<li><a class="gog find-friend-googlepl" href="javascript:void(0);"><i class="identy"></i>Google+ Friends<i class="arrow"></i></a></li>-->
		<!--<li><a class="gmi find-friend-gmail" href="javascript:void(0);"><i class="identy"></i><?php echo site_lg('lg_gmail_friends', 'Gmail Friends');?><i class="arrow"></i></a></li>-->
	</ul>
	<ul id="suggest-links" class="setting-page" style="<?php echo $sugest;?>">
        <li class="headlines"><?php echo site_lg('lg_suggest_you', 'Suggest to you');?><li>   
        <li><a class="Cred" href="find-friends?tab=pick"><?php if($this->lang->line('oliver_picks') != '') { echo $this->lang->line('oliver_picks'); } else echo "Oliver Picks" ?><i class="arrow"></i></a></li>  
		<li><a class="Ref" href="find-friends?tab=brand"><?php if($this->lang->line('featured_brands') != '') { echo $this->lang->line('featured_brands'); } else echo "Featured Brands" ?><i class="arrow"></i></a></li>
		<li><a class="Inv" href="find-friends?tab=similar"><?php if($this->lang->line('similar_users') != '') { echo $this->lang->line('similar_users'); } else echo "Similar Users" ?><i class="arrow"></i></a></li>
	</ul>
	<?php if($tab =='key'){?>
	<div class="figure-product" style="<?php echo $key;?>">
		
		<h2 class="comments-title"><?php echo site_lg('find_friends_people', 'People');?><li>  <?php if($profileUserDetails != ''){echo $profileUserDetails->num_rows();}else {echo 0;}?></h2>
		<?php if($profileUserDetails != '' && $profileUserDetails->num_rows() >0){
			 foreach($profileUserDetails->result() as $_profileUserDetails){
				$userImg = 'user-thumb1.png';
				if($_profileUserDetails->thumbnail != ''){
					$userImg = $_profileUserDetails->thumbnail;
				}
				$followClass = 'follow';
	             if($loginCheck != ''){
		            $followingListArr = explode(',', $_profileUserDetails->followers);
		            if(in_array($loginCheck, $followingListArr)){
		        	   $followClass = 'follow2';
				}}?>
		<dl style="border:none" class="userinformation">
			<dt>
				<button class="button-<?php echo $followClass; ?>" uid="<?php echo $_profileUserDetails->id;?>"><i class="icon"></i></button>
				<a href="user/<?php echo $_profileUserDetails->user_name;?>"><img height="60" width="60" src="<?php echo DESKTOPURL;?>images/users/<?php echo $userImg;?>" class="member">
				<b><?php echo $_profileUserDetails->user_name;?></b></a>
			</dt>
			<dd class="follow-image">
				<?php if($product_details[$_profileUserDetails->id]!= '' && count($product_details[$_profileUserDetails->id]) >0){
					foreach($product_details[$_profileUserDetails->id] as $likeProdRow){
						$img = 'dummyProductImage.jpg';
						$imgArr = explode(',', $likeProdRow->image);
						if(count($imgArr)>0){
							foreach($imgArr as $imgRow){
								if($imgRow != ''){
									$img = $imgRow;
									break;
						}}}
						if($likeProdRow->web_link!=''){
							$product_link = 'user/'.$_profileUserDetails->user_name.'/things/'.$likeProdRow->seller_product_id.'/'.url_title($likeProdRow->product_name,'-');
						}else{
							$product_link = 'things/'.$likeProdRow->pid.'/'.url_title($likeProdRow->product_name,'-');
						}?>
				 <a href="<?php echo $product_link;?>">
					<img height="60" width="60" src="<?php echo DESKTOPURL;?>images/product/<?php echo $img;?>" >
				 </a>
				<?php }}?> 
			</dd>
		</dl>
		<?php }}else{?>
			 <dl><dt><?php if($this->lang->line('no_results_found') != '') { echo $this->lang->line('no_results_found'); } else echo "No Results Found" ?></dt></dl>
		<?php }?>
	</div>

<?php }?>
<?php if($tab =='pick'){?>
	<!-----------------------------fancy picks----------------------------------------------->
<div class="figure-product" style="<?php echo $pick;?>">
   <h3 class="comments-title"><b><?php if($this->lang->line('oliver_picks') != '') { echo $this->lang->line('oliver_picks'); } else echo "Oliver Picks" ?></b>
       <br/>
       <small><?php if($this->lang->line('oliver_picks_text1') != '') { echo $this->lang->line('oliver_picks_text1'); } else echo "Follow top contributors from topics you are interested in." ?></small>
   </h3>
   <?php
     if($this->data['mainCategories']->num_rows()>0){
        foreach($this->data['mainCategories']->result() as $catRow){
        	?>
          <div class="find_selected userinformation  <?php echo url_title($catRow->cat_name,'_',TRUE);?>">
			  <h4><b><?php echo $catRow->cat_name; ?></b></h4>
<!-- 			  <button style="float:right; margin: 19px 0 0;" class="btns-blue-embo btn-followall"><?php if($this->lang->line('onboarding_follow_all') != '') { echo $this->lang->line('onboarding_follow_all'); } else echo "Follow All" ?></button> -->
			  <?php 
				  $limitCount = 0;
				  foreach($userCountArr[$catRow->id] as $user_id => $products){
					if($user_id!=''){
					  if($user_details[$catRow->id][$user_id] && $user_details[$catRow->id][$user_id]->num_rows()==1){
						if($limitCount<5){
						    $userImg = $user_details[$catRow->id][$user_id]->row()->thumbnail;
							   if($userImg == ''){
                                   $userImg = 'user-thumb1.png';
							   }
							   $followClass = 'follow';
	                           $followText = 'Follow';
	                           if($loginCheck != ''){
		                          $followingListArr = array_filter(explode(',', $user_details[$catRow->id][$user_id]->row()->following));
								foreach($followingListArr as $followingArr){
		                          if(in_array($followingArr, $followingListArr)){
		        	                 $followClass = 'following';
		        	                 $followText = 'Following';
		                          }}
	             }?>
				  <li>
				  
				  <dl style="border:none" class="userinformation">
						<dt>
							<button class="button-<?php echo $followClass; ?>" uid="<?php echo $user_details[$catRow->id][$user_id]->row()->id;?>" style="float:right;"><i class="icon"></i><?php echo $followText;?></button>
							<a href="user/<?php echo $user_details[$catRow->id][$user_id]->row()->user_name;?>"><img height="60" width="60" src="<?php echo DESKTOPURL;?>images/users/<?php echo $userImg;?>" class="member">
							<b><?php echo $user_details[$catRow->id][$user_id]->row()->user_name;?></b></a>
						</dt>
						<dd class="follow-image">
							<?php 
							if($userProductDetails[$catRow->id][$user_id]->num_rows()>0){
							  foreach($userProductDetails[$catRow->id][$user_id]->result() as $likeProdRow){
									$img = 'dummyProductImage.jpg';
									$imgArr = explode(',', $likeProdRow->image);
									if(count($imgArr)>0){
										foreach($imgArr as $imgRow){
											if($imgRow != ''){
												$img = $imgRow;
												break;
									}}}
									if($likeProdRow->web_link!=''){
										$product_link = 'user/'.$user_details[$catRow->id][$user_id]->row()->user_name.'/things/'.$likeProdRow->seller_product_id.'/'.url_title($likeProdRow->product_name,'-');
									}else{
										$product_link = 'things/'.$likeProdRow->pid.'/'.url_title($likeProdRow->product_name,'-');
									}?>
							 <a href="<?php echo $product_link;?>">
								<img height="60" width="60" src="<?php echo DESKTOPURL;?>images/product/<?php echo $img;?>" >
							 </a>
							<?php }}?> 
						</dd>
					</dl>
				  
                 </li>									  
			<?php $limitCount++; }}
			else{?>
			   <h2><?php if($this->lang->line('no_results_found') != '') { echo $this->lang->line('no_results_found'); } else echo "No Results Found" ?></h2>
			<?php }
			} }?>
		 </ul>
	 </div>
  <?php }} ?>   
</div>                  
<!-----------------------------fancy picks ends----------------------------------------------->
<?php }?>
<!-----------------------------Featured Brands----------------------------------------------->
<?php if($tab =='brand'){?>
<div class="figure-product" style="<?php echo $brand;?>">
   <h3 class="comments-title">
      <b><?php if($this->lang->line('featured_brands') != '') { echo $this->lang->line('featured_brands'); } else echo "Featured Brands" ?></b>
      <br/>
      <small><?php if($this->lang->line('featuredbrands_text1') != '') { echo $this->lang->line('featuredbrands_text1'); } else echo "Find and follow your favorite brands and stores." ?></small>
   </h3>
   
   <?php if($brandUserDetails && $brandUserDetails->num_rows() >0){
			 foreach($brandUserDetails->result() as $_profileUserDetails){
				$userImg = 'user-thumb1.png';
				if($_profileUserDetails->thumbnail != ''){
					$userImg = $_profileUserDetails->thumbnail;
				}
				$followClass = 'follow';
	             if($loginCheck != ''){
		            $followingListArr = explode(',', $_profileUserDetails->followers);
		            if(in_array($loginCheck, $followingListArr)){
		        	   $followClass = 'follow2';
				}}?>
		<dl style="border:none" class="userinformation">
			<dt>
				<button class="button-<?php echo $followClass; ?>" uid="<?php echo $_profileUserDetails->id;?>"><i class="icon"></i></button>
				<a href="user/<?php echo $_profileUserDetails->user_name;?>"><img height="60" width="60" src="<?php echo DESKTOPURL;?>images/users/<?php echo $userImg;?>" class="member">
				<b><?php echo $_profileUserDetails->user_name;?></b></a>
			</dt>
			<dd class="follow-image">
				<?php 
				if($brandproduct_details[$_profileUserDetails->id]!= '' && $brandproduct_details[$_profileUserDetails->id]->num_rows() >0){
					foreach($brandproduct_details[$_profileUserDetails->id]->result() as $likeProdRow){
						$img = 'dummyProductImage.jpg';
						$imgArr = explode(',', $likeProdRow->image);
						if(count($imgArr)>0){
							foreach($imgArr as $imgRow){
								if($imgRow != ''){
									$img = $imgRow;
									break;
						}}}
						if($likeProdRow->web_link!=''){
							$product_link = 'user/'.$_profileUserDetails->user_name.'/things/'.$likeProdRow->seller_product_id.'/'.url_title($likeProdRow->product_name,'-');
						}else{
							$product_link = 'things/'.$likeProdRow->pid.'/'.url_title($likeProdRow->product_name,'-');
						}?>
				 <a href="<?php echo $product_link;?>">
					<img height="60" width="60" src="<?php echo DESKTOPURL;?>images/product/<?php echo $img;?>" >
				 </a>
				<?php }}?> 
			</dd>
		</dl>
		<?php }}else{?>
			 <dl><dt><?php if($this->lang->line('no_results_found') != '') { echo $this->lang->line('no_results_found'); } else echo "No Results Found" ?></dt></dl>
		<?php }?>
   
</div>
<!-----------------------------Featured Brands end----------------------------------------------->
<?php }?> 
<?php if($tab =='similar'){?>
<!-----------------------------similar user-----------------------------------------------> 
<div class="figure-product" style="<?php echo $similars;?>">
    <h3 class="comments-title"><b><?php if($this->lang->line('similar_users') != '') { echo $this->lang->line('similar_users'); } else echo "Similar Users" ?></b>
        <br/>
        <small><?php if($this->lang->line('similaruser_text1') != '') { echo $this->lang->line('similaruser_text1'); } else echo "Find people with similar taste as yourself." ?></small>
    </h3>
    <?php if($similarUserDetails >0){
			 foreach($similarUserDetails as $_profileUserDetails){
				$userImg = 'user-thumb1.png';
				if($_profileUserDetails->thumbnail != ''){
					$userImg = $_profileUserDetails->thumbnail;
				}
				$followClass = 'follow';
	             if($loginCheck != ''){
		            $followingListArr = explode(',', $_profileUserDetails->followers);
		            if(in_array($loginCheck, $followingListArr)){
		        	   $followClass = 'follow2';
				}}?>
		<dl style="border:none" class="userinformation">
			<dt>
				<button class="button-<?php echo $followClass; ?>" uid="<?php echo $_profileUserDetails->id;?>"><i class="icon"></i></button>
				<a href="user/<?php echo $_profileUserDetails->user_name;?>"><img height="60" width="60" src="<?php echo DESKTOPURL;?>images/users/<?php echo $userImg;?>" class="member">
				<b><?php echo $_profileUserDetails->user_name;?></b></a>
			</dt>
			<dd class="follow-image">
				<?php 
				 if($similarproduct_details[$_profileUserDetails->id]->num_rows() >0){
	    	          foreach($similarproduct_details[$_profileUserDetails->id]->result() as $likeProdRow){
						$img = 'dummyProductImage.jpg';
						$imgArr = explode(',', $likeProdRow->image);
						if(count($imgArr)>0){
							foreach($imgArr as $imgRow){
								if($imgRow != ''){
									$img = $imgRow;
									break;
						}}}
						if($likeProdRow->web_link!=''){
							$product_link = 'user/'.$_profileUserDetails->user_name.'/things/'.$likeProdRow->seller_product_id.'/'.url_title($likeProdRow->product_name,'-');
						}else{
							$product_link = 'things/'.$likeProdRow->id.'/'.url_title($likeProdRow->product_name,'-');
						}?>
				 <a href="<?php echo $product_link;?>">
					<img height="60" width="60" src="<?php echo DESKTOPURL;?>images/product/<?php echo $img;?>" >
				 </a>
				<?php }}?> 
			</dd>
		</dl>
		<?php }}else{?>
			 <dl><dt><?php if($this->lang->line('no_results_found') != '') { echo $this->lang->line('no_results_found'); } else echo "No Results Found" ?></dt></dl>
		<?php }?>
		
</div>
<!-----------------------------similar user end-----------------------------------------------> 
<?php }?>
	<div class="figure-producta">
	</div>
