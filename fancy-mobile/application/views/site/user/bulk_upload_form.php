<?php $this->load->view('site/templates/header');?>
<link rel="stylesheet" href="<?php echo base_url();?>css/site/<?php echo SITE_COMMON_DEFINE ?>timeline.css" type="text/css" media="all"/>
<link rel="stylesheet" media="all" type="text/css" href="<?php echo base_url();?>css/site/<?php echo SITE_COMMON_DEFINE ?>setting.css">
<style type="text/css">
ol.stream {position: relative;}
ol.stream.use-css3 li.anim {transition:all .25s;-webkit-transition:all .25s;-moz-transition:all .25s;-ms-transition:all .25s;visibility:visible;opacity:1;}
ol.stream.use-css3 li {visibility:hidden;}
ol.stream.use-css3 li.anim.fadeout {opacity:0;}
ol.stream.use-css3.fadein li {opacity:0;}
ol.stream.use-css3.fadein li.anim.fadein {opacity:1;}
</style>
<!-- Section_start -->

<div class="lang-en no-subnav wider winOS">

<div id="container-wrapper">
	<div class="container set_area">
		<?php if($flash_data != '') { ?>
		<div class="errorContainer" id="<?php echo $flash_data_type;?>">
			<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 4000);</script>
			<p><span><?php echo $flash_data;?></span></p>
		</div>
		<?php } ?>
        <div id="content">
		<div class="notification-bar" style="display:none"></div>
		<?php 
		if ($userDetails->row()->is_verified == 'No'){
		?>
        <div class="confirm-email">
            
            <p><?php if($this->lang->line('settings_check_mail') != '') { echo stripslashes($this->lang->line('settings_check_mail')); } else echo "Check your email"; ?> <b>(<?php echo $userDetails->row()->email;?>)</b> <?php if($this->lang->line('settings_toconfirm') != '') { echo stripslashes($this->lang->line('settings_toconfirm')); } else echo "to confirm."; ?></p>
            <a id="resend_confirmation" href="javascript:void(0)" onclick="javascript:resendConfirmation('<?php echo $userDetails->row()->email;?>')"><?php if($this->lang->line('settings_resendconfm') != '') { echo stripslashes($this->lang->line('settings_resendconfm')); } else echo "Resend confirmation"; ?></a>
            
		</div>
		<?php 
		}
		?>
  		<form class="myform" method="post" action="site/user_settings/bulk_upload_action" enctype="multipart/form-data">
		<div class="section profile">
			<h3 class="stit"><?php if($this->lang->line('bulk_upload') != '') { echo stripslashes($this->lang->line('bulk_upload')); } else echo "Bulk Upload"; ?></h3>
			<fieldset class="frm">
				<label><?php if($this->lang->line('choose_file') != '') { echo stripslashes($this->lang->line('choose_file')); } else echo "Choose File"; ?></label>
				<input class="setting_fullname" name="product_name" type="file" accept=".csv">
				<small class="comment">Choose a csv file to upload</small>
			</fieldset>
		</div>

		<div class="btn-area">
			<input type="submit" name="profile" style="cursor:pointer;" class="btn-save" id="save_account" value="Upload" />
			<span class="checking" style="display:none"><i class="ic-loading"></i></span>
		</div>                
		</form>

<div class="section shipping" style="border-top: 1px solid #EBECEF;">
    <a href="<?php echo DESKTOPURL.'csv_sample/seller.csv' ?>" target="_blank"><button class="btn-shipping add_"> Download sample</button></a>
        <br>
            <h3>Category Name and Id</h3>
            
                	<div class="chart-wrap">
            <table class="chart">
                <thead>
                    <tr>
                        <th>category id</th>
                        <th>category name</th>
                        <th>Parent name</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if($categories->num_rows > 0){ foreach($categories->result() as $category){ ?>
                    <tr>
                        <td><?php echo $category->id; ?></td>
                        <td><?php echo $category->cat_name; ?></td>
                        <td><?php echo $category->parent_name;   ?></td>
                    </tr>
                    <?php } } ?>
                </tbody>
            </table>
			</div>
            	
			</div>                
	</div>
											

	<?php 
     //$this->load->view('site/user/settings_sidebar');
     //$this->load->view('site/templates/side_footer_menu');
     ?>
	</div>
	<!-- / container -->
</div>

</div>


<!-- Section_start -->




<script src="<?php echo base_url();?>js/site/<?php echo SITE_COMMON_DEFINE ?>shoplist.js" type="text/javascript"></script>

<script>
	jQuery(function($) {
		var $select = $('.gift-recommend select.select-round');
		$select.selectBox();
		$select.each(function(){
			var $this = $(this);
			if($this.css('display') != 'none') $this.css('visibility', 'visible');
		});
	});
</script>
<script>
    //emulate behavior of html5 textarea maxlength attribute.
    jQuery(function($) {
        $(document).ready(function() {
            var check_maxlength = function(e) {
                var max = parseInt($(this).attr('maxlength'));
                var len = $(this).val().length;
                if (len > max) {
                    $(this).val($(this).val().substr(0, max));
                }
                if (len >= max) {
                    return false;
                }
            }
            $('textarea[maxlength]').keypress(check_maxlength).change(check_maxlength);
            
            
        });
    });
</script>
<?php $this->load->view('site/templates/footer');?>		