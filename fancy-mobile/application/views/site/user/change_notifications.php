<?php 
$this->load->view('site/templates/header');
?>
<?php if($flash_data != '') { ?>
		<div class="errorContainer" id="<?php echo $flash_data_type;?>">
			<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
			<p><span><?php echo $flash_data;?></span></p>
		</div>
		<?php } ?>
	<div id="content" class="account-page">
        <div class="account-wrap">
                <form method="post" action="site/user_settings/update_notifications">      
        <dl class="profile-account">
        
        <dd>
       
        <p class="effete">
        <label class="label"><?php if($this->lang->line('notify_email_sett') != '') { echo stripslashes($this->lang->line('notify_email_sett')); } else echo "Email settings"; ?></label><br />
        
      
        <?php 
                $emailNoty = explode(',', $userDetails->row()->email_notifications);
                if (is_array($emailNoty)){
                	$emailNotifications = $emailNoty;
                }
                ?>
        <input type="checkbox" <?php if (in_array('following', $emailNotifications)){echo 'checked="checked"';}?> name="following">
        <label style="margin:0 10px 0 0" class="radio-button" for="men"><?php if($this->lang->line('notify_some_follu') != '') { echo stripslashes($this->lang->line('notify_some_follu')); } else echo "When someone follows you"; ?></label><br /><br />
        
       <input type="checkbox" <?php if (in_array('comments_on_fancyd', $emailNotifications)){echo 'checked="checked"';}?> name="comments_on_fancyd">
         <label style="margin:0 10px 0 0" class="radio-button" for="men"><?php if($this->lang->line('notify_comm_things') != '') { echo stripslashes($this->lang->line('notify_comm_things')); } else echo "When someone comments on a thing you"; ?> <?php echo LIKED_BUTTON;?></label><br /><br />
         
        <input type="checkbox" <?php if (in_array('featured', $emailNotifications)){echo 'checked="checked"';}?> name="featured" >
          <label style="margin:0 10px 0 0" class="radio-button" for="men"><?php if($this->lang->line('notify_thing_feature') != '') { echo stripslashes($this->lang->line('notify_thing_feature')); } else echo "When one of your things is featured"; ?> </label><br /><br />
         
        <input type="checkbox" name="comments" <?php if (in_array('comments', $emailNotifications)){echo 'checked="checked"';}?>>
         <label style="margin:0 10px 0 0" class="radio-button" for="men"><?php if($this->lang->line('cmt_on_ur_thing') != '') { echo stripslashes($this->lang->line('cmt_on_ur_thing')); } else echo "When someone comments on your thing"; ?> </label><br /><br />
       
       
        </p>
        </dd>
        </dl>
        
        <dl class="profile-account">
        
        <dd>
       
        <p class="effete">
        <label class="label"><?php if($this->lang->line('notify_web_sett') != '') { echo stripslashes($this->lang->line('notify_web_sett')); } else echo "Web settings"; ?></label>
        <small style=" padding-bottom: 1.22em;"><?php if($this->lang->line('notify_notify_showup') != '') { echo stripslashes($this->lang->line('notify_notify_showup')); } else echo "The web notifications show up in the topbar of the"; ?> <?php echo $siteTitle;?> <?php if($this->lang->line('settings_website') != '') { echo stripslashes($this->lang->line('settings_website')); } else echo "website"; ?>.</small><br />
         <label class="label"><?php if($this->lang->line('notify_active_involves') != '') { echo stripslashes($this->lang->line('notify_active_involves')); } else echo "Activity that involves you"; ?></label><br />
         <?php 
                $noty = explode(',', $userDetails->row()->notifications);
                if (is_array($noty)){
                	$notifications = $noty;
                }
                ?>
        <input type="checkbox" name="wmn-follow" <?php if (in_array('wmn-follow', $notifications)){echo 'checked="checked"';}?> >
         <label style="margin:0 10px 0 0" class="radio-button" for="men"><?php if($this->lang->line('notify_some_follu') != '') { echo stripslashes($this->lang->line('notify_some_follu')); } else echo "When someone follows you"; ?> </label><br /><br />
         
         <input type="checkbox" name="wmn-comments_on_fancyd" <?php if (in_array('wmn-comments_on_fancyd', $notifications)){echo 'checked="checked"';}?>>
         <label style="margin:0 10px 0 0" class="radio-button" for="men"><?php if($this->lang->line('notify_comm_things') != '') { echo stripslashes($this->lang->line('notify_comm_things')); } else echo "When someone comments on a thing you"; ?> <?php echo LIKED_BUTTON;?> </label><br /><br />
         
         <input type="checkbox" name="wmn-fancyd" <?php if (in_array('wmn-fancyd', $notifications)){echo 'checked="checked"';}?>>
         <label style="margin:0 10px 0 0" class="radio-button" for="men"><?php if($this->lang->line('notify_when_some') != '') { echo stripslashes($this->lang->line('notify_when_some')); } else echo "When someone"; ?> <?php echo LIKE_BUTTON;?> <?php if($this->lang->line('notify_ur_thing') != '') { echo stripslashes($this->lang->line('notify_ur_thing')); } else echo "one of your things"; ?> </label><br /><br />
         
         <input type="checkbox" name="wmn-featured" <?php if (in_array('wmn-featured', $notifications)){echo 'checked="checked"';}?>>
         <label style="margin:0 10px 0 0" class="radio-button" for="men"><?php if($this->lang->line('notify_thing_feature') != '') { echo stripslashes($this->lang->line('notify_thing_feature')); } else echo "When one of your things is featured"; ?> </label><br /><br />
         
         <input type="checkbox" name="wmn-comments" <?php if (in_array('wmn-comments', $notifications)){echo 'checked="checked"';}?>>
         <label style="margin:0 10px 0 0" class="radio-button" for="men"><?php if($this->lang->line('cmt_on_ur_thing') != '') { echo stripslashes($this->lang->line('cmt_on_ur_thing')); } else echo "When someone comments on your thing"; ?> </label><br /><br />
       
        </p>
        </dd>
        </dl>
        <dl class="profile-account">
        
        <dd>
       
        <p class="effete">
        <label class="label"><?php if($this->lang->line('notify_update') != '') { echo stripslashes($this->lang->line('notify_update')); } else echo "Updates"; ?></label><br />
        
      
        <input type="checkbox" <?php if ($userDetails->row()->updates == '1'){echo 'checked="checked"';}?> id="updates" name="updates">
        <label style="margin:0 10px 0 0" class="radio-button" for="men"><?php if($this->lang->line('notify_update_from') != '') { echo stripslashes($this->lang->line('notify_update_from')); } else echo "Updates from"; ?> <?php echo $siteTitle;?></label><br /><br />
        
        </p>
        </dd>
        </dl>
        
        <dl class="profile-account">
        
        <dd style=" padding: 1em;">
       
   	<!-- <button id="save_notifications" class="btn-save"><?php if($this->lang->line('notify_save_change') != '') { echo stripslashes($this->lang->line('notify_save_change')); } else echo "Save Changes"; ?></button>-->
        <button style="width:100%" class="profile-button"><?php if($this->lang->line('notify_save_change') != '') { echo stripslashes($this->lang->line('notify_save_change')); } else echo "Save Changes"; ?></button>
         </form>
         </dd>
        </dl>
        </div>
     
     
        </div>
<style>
.user-image{background: none repeat scroll 0 0 #FFFFFF; border: 1px solid #E5E5E5; position: relative; width: 10%; height:130px;  margin: 0 auto;}
.user-image .hidden {display: block !important; height: 150px; opacity: 0; position: relative; width: 100%; z-index: 1;}
.user-image #img_prev{width:131px; height:132px;}
</style>
