<?php
$this->load->view('site/templates/header',$this->data);
?>
<link rel="stylesheet" media="all" type="text/css" href="css/site/<?php echo SITE_COMMON_DEFINE ?>setting.css">
<!-- Section_start -->
<div class="lang-en no-subnav wider winOS">
<!-- Section_start -->
<div id="container-wrapper">
	<div class="container set_area">
		<?php if($flash_data != '') { ?>
		<div class="errorContainer" id="<?php echo $flash_data_type;?>">
			<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
			<p><span><?php echo $flash_data;?></span></p>
		</div>
		<?php } ?>
        <div id="content">
		<h2 class="ptit"><?php if($this->lang->line('featured_seller_plan') != '') { echo stripslashes($this->lang->line('featured_seller_plan')); } else echo "Featured Seller Plan"; ?></h2>
                <div class="section shipping">
            <div class="chart-wrap">
            <table class="chart">
                <thead>
                    <tr>
                        <th>Plan Name</th>
                        <th>Plan Price</th>
                        <th>Plan Period</th>
                        <th>No of Promotable Products</th>
                    </tr>
                </thead>
                <tbody>
                   <tr>
                        <td><?php echo $plan->row()->plan_name; ?></td>
                        <td><?php echo $currencySymbol.$plan->row()->plan_price; ?></td>
                        
                        <td><?php echo $plan->row()->plan_period; ?> days</td>
                        <td><?php echo $plan->row()->plan_product_count; ?></td>
                    </tr>
                                        
                </tbody>
            </table>                
			</div>
                    
                <input type="radio" name="payment_method" value="1" checked > Paypal <br>
                <input type="radio" name="payment_method" value="2"> Credit Card(Paypal) <br>
                <input type="radio" name="payment_method" value="3"> Credit Card(Authorize.net) <br>
                <input type="radio" name="payment_method" value="4"> Stripe Payment <br>
                <input type="radio" name="payment_method" value="5"> 2CO Payment <br><br>               
                    <a href="javascript:pay_url()"><button class="btn-shipping add_">Pay</button></a>
			</div>
        </div>
            
		<?php 
		//$this->load->view('site/user/settings_sidebar');
     //$this->load->view('site/templates/side_footer_menu');
     ?>

	</div>
	<!-- / container -->
</div>
</div>


<!-- Section_start -->
<script type="text/javascript" src="js/site/jquery.validate.js"></script>
<script>
function pay_url(){
var url = "<?php echo base_url().'featured_plan_pay'; ?>?payment="+$('input[name="payment_method"]:checked').val();
window.location.href = url;
}
</script>
<?php 
$this->load->view('site/templates/footer',$this->data);
?>
