<?php 
$this->load->view('site/templates/header');?>
<style>
.set_area .chart {
    width: 100%;
}
.set_area .chart thead th {
    background: none repeat scroll 0 0 #F5F6F8;
    border-top: 1px solid #D9DADD;
    color: #606163;
    font-size: 12px;
    font-weight: bold;
    height: 20px;
    padding: 5px 0 5px 9px;
    vertical-align: middle;
	text-align:left;
}
.set_area .chart tbody tr:hover td {
    background: none repeat scroll 0 0 #F8F9FA;
}
.set_area .chart tbody td {
    border-bottom: 1px solid #DBDCDE;
    color: #6E7176;
    font-size: 12px;
    line-height: 20px;
    padding: 8px 0 8px 9px;
    vertical-align: middle;
}
.set_area .chart tbody td b, .set_area .chart tbody td.date, .set_area .chart tbody td .persent, .set_area .chart tbody td .cancel {
    color: #393D4D;
    font-style: normal;
}
.set_area tbody td .ic-check {
    background-position: -155px -50px;
    display: inline-block;
    height: 10px;
    width: 14px;
}
.set_area .section .chart-wrap {
    border: 1px solid #D9DBE1;
    border-radius: 3px 3px 3px 3px;
    margin: 28px 0 24px;
}
.set_area .section .chart-wrap thead th, .set_area .section .chart-wrap tbody tr:last-child td {
    border: 0 none;
}
.set_area .section .chart tbody tr td {
    border-bottom-color: #D9DBE1;
    color: #393D4D;
    line-height: 16px;
    padding: 15px 11px;
    vertical-align: top;
}
.set_area .section .chart tbody tr td small {
    color: #6E7176;
    font-size: 12px;
}
.set_area .section .chart tbody tr td.btns {
    padding: 15px 0;
    text-align: center;
    width: 105px;
}
.set_area .section .chart tbody tr:hover td {
    background: none repeat scroll 0 0 transparent;
}



.payment-list .tit a {
    color: #588CC7;
    float: right;
    font-size: 0.94em;
    font-weight: normal;
}
</style>
<?php if($paymentDetails->num_rows()==1){
?>
<?php if($flash_data != '') { ?>
		<div class="errorContainer" id="<?php echo $flash_data_type;?>">
			<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
            
			<p><span><?php echo $flash_data;?></span></p>
		</div>
		<?php } ?>
	<div id="content" class="account-page">
        <div class="account-wrap">


        <dl class="profile-account">
        
         <form class="ltxt" action="site/user_settings/insertEdit_payment_method" method="post" name ="paymentForm" id="paymentForm" onsubmit="return payment_validation()">
        <dd>
       
        <p class="effete">
        <label class="label"><?php if($this->lang->line('payment_info') != '') { echo stripslashes($this->lang->line('payment_info')); } else echo "Payment Info"; ?></label><br /></p>
                
        <p class="name-account"><label class="label"><?php if($this->lang->line('name_on_card') != '') { echo stripslashes($this->lang->line('name_on_card')); } else echo "Name on card"; ?></label>
		<input id="full_name" name="full_name" class="popup_add_input" type="text"  value="<?php echo $paymentDetails->row()->full_name;?>" placeholder="<?php if($this->lang->line('payment_home_work') != '') { echo stripslashes($this->lang->line('payment_home_work')); } else echo "E.g. Aunt Jane"; ?>">
		</p>
        
        <p class="name-account"><label class="label"><?php if($this->lang->line('card_number') != '') { echo stripslashes($this->lang->line('card_number')); } else echo "Card number"; ?></label>
        <?php 
        $key = 'team-clone-tweaks';
        ?>
		<input id="card_no" name="card_no" class="popup_add_input width180" type="text" value="<?php echo $this->encrypt->decode($paymentDetails->row()->card_no, $key);;?>">
		</p>
        
        <p class="name-account"><label class="label"><?php if($this->lang->line('payment_cvv') != '') { echo stripslashes($this->lang->line('payment_cvv')); } else echo "CVV "; ?></label>
		<input id="card_cvv" name="card_cvv" class="popup_add_input width70" type="text" value="<?php echo $this->encrypt->decode($paymentDetails->row()->card_cvv, $key);?>">
		</p>
        
      <p class="name-account"><label class="label"><?php if($this->lang->line('expiration_date') != '') { echo stripslashes($this->lang->line('expiration_date')); } else echo "Expiration date"; ?></label>

        
		<select id="CCExpDay" name="expiray_day" class="popup_add_input width70 select-round select-white select-date selectBox required">
						<option value="01" <?php if(date('m')=='01'){ echo $Sel;} ?>>01</option>
						<option value="02" <?php if(date('m')=='02'){ echo $Sel;} ?>>02</option>
						<option value="03" <?php if(date('m')=='03'){ echo $Sel;} ?>>03</option>
						<option value="04" <?php if(date('m')=='04'){ echo $Sel;} ?>>04</option>
						<option value="05" <?php if(date('m')=='05'){ echo $Sel;} ?>>05</option>
						<option value="06" <?php if(date('m')=='06'){ echo $Sel;} ?>>06</option>
						<option value="07" <?php if(date('m')=='07'){ echo $Sel;} ?>>07</option>
						<option value="08" <?php if(date('m')=='08'){ echo $Sel;} ?>>08</option>
						<option value="09" <?php if(date('m')=='09'){ echo $Sel;} ?>>09</option>
						<option value="10" <?php if(date('m')=='10'){ echo $Sel;} ?>>10</option>
						<option value="11" <?php if(date('m')=='11'){ echo $Sel;} ?>>11</option>
						<option value="12" <?php if(date('m')=='12'){ echo $Sel;} ?>>12</option>
				   </select>
				   <select id="CCExpMnth" name="expiray_year" class="popup_add_input width80 select-round select-white select-date selectBox required">
						<?php for($i=date('Y');$i< (date('Y') + 25);$i++){ ?>	
							<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
						<?php } ?>    
				   </select>
		</p>
        
        <p class="name-account"><label class="label"><?php if($this->lang->line('checkout_billing_addr') != '') { echo stripslashes($this->lang->line('checkout_billing_addr')); } else echo "Billing Address"; ?></label>
		</p>
        
        <p class="name-account"><label class="label"><?php if($this->lang->line('shipping_address_comm') != '') { echo stripslashes($this->lang->line('shipping_address_comm')); } else echo "Address"; ?></label>
		<input class="popup_add_input" id="address1" name="address1" type="text" placeholder="<?php if($this->lang->line('header_address1') != '') { echo stripslashes($this->lang->line('header_address1')); } else echo "Address Line1"; ?>" value="<?php echo $paymentDetails->row()->address1;?>">
		<input class="popup_add_input" id="address2" name="address2" type="text" placeholder="<?php if($this->lang->line('header_address2') != '') { echo stripslashes($this->lang->line('header_address2')); } else echo "Address Line2"; ?>" value="<?php echo $paymentDetails->row()->address2;?>">
		</p>
        
        <p class="name-account"><label class="label"><?php if($this->lang->line('header_country') != '') { echo stripslashes($this->lang->line('header_country')); } else echo "Country"; ?></label>
		<select name="country" id="country" class="popup_add_input width455">
						<?php 
						if ($countryList->num_rows()>0){
							foreach ($countryList->result() as $country){
						?>
						<option value="<?php echo $country->country_code;?>" <?php if ($paymentDetails->row()->country == $country->country_code){?>selected="selected"<?php }?>><?php echo $country->name;?></option>
						<?php 
							}
						}
						?>
					</select>
		</p>
        
          <p class="name-account"><label class="label"><?php if($this->lang->line('header_city') != '') { echo stripslashes($this->lang->line('header_city')); } else echo "City"; ?></label>
		<input class="popup_add_input width180" name="city" type="text" id="city" value="<?php echo $paymentDetails->row()->city;?>">
		</p>
        
          <p class="name-account"><label class="label"><?php if($this->lang->line('header_state_province') != '') { echo stripslashes($this->lang->line('header_state_province')); } else echo "State / Province"; ?></label>
		<input class="popup_add_input width108" name="state" type="text" id="state" value="<?php echo $paymentDetails->row()->state;?>">
		</p>
        
          <p class="name-account"><label class="label"><?php if($this->lang->line('header_zip_postal') != '') { echo stripslashes($this->lang->line('header_zip_postal')); } else echo "Zip / Postal Code"; ?></label>
		<input name="postal_code" class="popup_add_input width108" id="postal_code" type="text" value="<?php echo $paymentDetails->row()->postal_code;?>">
		</p>
        
        
        
        
        
        <dl class="profile-account">
        
        <dd style=" padding: 1em;">
       
           <button style="width:100%" class="profile-button"><?php if($this->lang->line('apply') != '') { echo stripslashes($this->lang->line('apply')); } else echo "Apply"; ?></button>
           <input type="hidden" id="row_id" name="row_id" value="<?php echo $paymentDetails->row()->id;?>"/>
			<input type="hidden" name="user_id" value="<?php echo $loginCheck;?>"/>
		</form>
       
         </dd>
        </dl>
         <?php }?> 
        </div>
     
     
        </div>
<style>
.user-image{background: none repeat scroll 0 0 #FFFFFF; border: 1px solid #E5E5E5; position: relative; width: 10%; height:130px;  margin: 0 auto;}
.user-image .hidden {display: block !important; height: 150px; opacity: 0; position: relative; width: 100%; z-index: 1;}
.user-image #img_prev{width:131px; height:132px;}
</style>

<script>

function payment_validation(){
var full_name=document.forms["paymentForm"]["full_name"].value;
var card_no=document.forms["paymentForm"]["card_no"].value;
var card_cvv=document.forms["paymentForm"]["card_cvv"].value;
var address1=document.forms["paymentForm"]["address1"].value;
var city=document.forms["paymentForm"]["city"].value;
var state=document.forms["paymentForm"]["state"].value;
var postal_code=document.forms["paymentForm"]["postal_code"].value;
if (full_name==null || full_name=="")
  {
  alert("Please Enter your Card Name");
  return false;
  }
  else if(isNaN(card_no) || card_no=="" || card_no.length==16){
     alert("Please Enter your Card No Correctly");
     return false;
  }
  else if(card_cvv==""){
     alert(card_cvv.length);
     alert("Please Enter your Card CVV Correctly");
     return false;
  }
  else if(address1==null || address1==""){
     alert("Please Enter your Address");
     return false;
  }
  else if(city==null || city==""){
     alert("Please Enter your City");
     return false;
  }
  else if(state==null || state==""){
     alert("Please Enter your State");
     return false;
  }
  else if(postal_code==null || postal_code==""){
     alert("Please Enter your ZipCode");
     return false;
  }
  
}
       
</script>