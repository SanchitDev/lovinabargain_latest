<?php 
$this->load->view('site/templates/header');
?>
<?php if($flash_data != '') { ?>
		<div class="errorContainer" id="<?php echo $flash_data_type;?>">
			<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
			<p><span><?php echo $flash_data;?></span></p>
		</div>
		<?php } ?>
	<div id="content" class="account-page">
        <div class="account-wrap">
		<form onsubmit="return change_user_password();" method="post" action="<?php echo base_url().'site/user_settings/change_user_password'?>" enctype="multipart/form-data">
			<dl class="profile-account">
				<dt class="tit-profile"><?php if($this->lang->line('signup_password') != '') { echo stripslashes($this->lang->line('signup_password')); } else echo "Password"; ?></dt>
				<dd><p class="name-account"><label class="label"><?php if($this->lang->line('change_new_pwd') != '') { echo stripslashes($this->lang->line('change_new_pwd')); } else echo "New Password"; ?></label>
						<input id="pass" class="setting_fullname" type="password" name="pass">
						<small><?php if($this->lang->line('change_pwd_limt') != '') { echo stripslashes($this->lang->line('change_pwd_limt')); } else echo "New password, at least 6 characters."; ?></small>
					</p>
				<dd><p class="name-account"><label class="label"><?php if($this->lang->line('change_conf_pwd') != '') { echo stripslashes($this->lang->line('change_conf_pwd')); } else echo "Confirm Password"; ?></label>
						<input id="confirmpass" class="setting_fullname" type="password" name="confirmpass">
						<small><?php if($this->lang->line('change_ur_pwd') != '') { echo stripslashes($this->lang->line('change_ur_pwd')); } else echo "Confirm your new password."; ?></small>
					</p>
				</dd>
			</dl>
			<dl class="profile-account">
				<dd> </dd>
			</dl>
			<div class="btton-container">
				<div class="btn-list">
               
					<button class="profile-button" id="save_password" type="submit"><?php if($this->lang->line('change_password') != '') { echo stripslashes($this->lang->line('change_password')); } else echo "Change Password"; ?></button>
				</div>
			</div>
			</form>
		</div>
	</div>
<style>
.user-image{background: none repeat scroll 0 0 #FFFFFF; border: 1px solid #E5E5E5; position: relative; width: 10%; height:130px;  margin: 0 auto;}
.user-image .hidden {display: block !important; height: 150px; opacity: 0; position: relative; width: 100%; z-index: 1;}
.user-image #img_prev{width:131px; height:132px;}
</style>
<script type="text/javascript">
function change_user_password(){

	//$('#save_password').disable();
	var pwd = $('#pass').val();
	var cfmpwd = $('#confirmpass').val();
	
	if(pwd == ''){
		alert('Enter new password');
		$('#save_password').removeAttr('disabled');
		$('#pass').focus();
		return false;
	}else if(pwd.length < 6){
		alert('Password must be minimum of 6 characters');
		$('#save_password').removeAttr('disabled');
		$('#pass').focus();
		return false;
	}else if(cfmpwd == ''){
		alert('Confirm password required');
		$('#save_password').removeAttr('disabled');
		$('#confirmpass').focus();
		return false;
	}else if(pwd != cfmpwd){
		alert('Passwords doesnot match');
		$('#save_password').removeAttr('disabled');
		$('#confirmpass').focus();
		return false;
	}else{
		return true;
	}
}
</script>