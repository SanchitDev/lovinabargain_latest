<?php 
//echo "<pre>"; print_r($userProductLikeDetails->result());die;

$this->load->view('site/user/display_user_header',$this->data); ?>
	<div class="figure-product wants-product">
		<h2 class="comments-title"><?php echo LIKED_BUTTON;?><span style="color:#999; margin:0 0 0 3px" ><?php echo $userProfileDetails->row()->likes;?></span></h2>
		<ul class="wants">
	
		<?php if ($productLikeDetails->num_rows()>0 || $userProductLikeDetails->num_rows()>0){?>
		<?php  
          if ($productLikeDetails->num_rows()>0){
          foreach ($productLikeDetails->result() as $productLikeDetailsRow){
				//echo "<pre>"; print_r($productLikeDetailsRow); die;
          		$imgName = 'dummyProductImage.jpg';
         		$imgArr = explode(',', $productLikeDetailsRow->image);
         		if (count($imgArr)>0){
         			foreach ($imgArr as $imgRow){
         				if ($imgRow != ''){
         					$imgName = $imgRow;
         					break;
         				}
         			}
         		}
			if($productLikeDetailsRow->web_link!=''){
				$product_link = 'user/'.$userProfileDetails->row()->user_name.'/things/'.$productLikeDetailsRow->seller_product_id.'/'.url_title($productLikeDetailsRow->product_name);
			}else{
				$product_link = 'things/'.$productLikeDetailsRow->id.'/'.url_title($productLikeDetailsRow->product_name);
			}
		?>
			<li><a href="<?php echo $product_link;?>">
					<img alt="" src="<?php echo DESKTOPURL;?>images/product/<?php echo $imgName;?>">
					<span class="wants-title"><?php echo $productLikeDetailsRow->product_name;?></span>
				</a>
				<span class="userlist"><b><?php echo $currencySymbol.$productLikeDetailsRow->sale_price;?></b>
				<a href="<?php if ($productLikeDetailsRow->user_name == ''){echo 'user/administrator';}else {echo 'user/'.$productLikeDetailsRow->user_name;}?>">
				<?php if ($productLikeDetailsRow->user_name == ''){echo 'administrator';}else {echo $productLikeDetailsRow->full_name;}?></a>+ <?php echo $productLikeDetailsRow->likes;?></span>
			</li>
		<?php }}
          if ($userProductLikeDetails->num_rows()>0){
          foreach ($userProductLikeDetails->result() as $productLikeDetailsRow){
		 
          		$imgName = 'dummyProductImage.jpg';
         		$imgArr = explode(',', $productLikeDetailsRow->image);
         		if (count($imgArr)>0){
         			foreach ($imgArr as $imgRow){
         				if ($imgRow != ''){
         					$imgName = $imgRow;
         					break;
         				}
         			}
         		}
          ?>
			<li><a href="<?php echo 'user/'.$productLikeDetailsRow->user_name.'/things/'.$productLikeDetailsRow->seller_product_id.'/'.url_title($productLikeDetailsRow->product_name);?>">
					<img alt="" src="<?php echo DESKTOPURL;?>images/product/<?php echo $imgName;?>">
					<span class="wants-title"><?php echo $productLikeDetailsRow->product_name;?></span>
				</a>
                
				<span class="userlist"><?php  if (!isset($productLikeDetailsRow->web_link)){ ?><b><?php echo $currencySymbol.$productLikeDetailsRow->sale_price;?></b><?php } ?>
				<a href="<?php if ($productLikeDetailsRow->user_name == ''){echo 'user/administrator';}else {echo 'user/'.$productLikeDetailsRow->user_name;}?>">
				<?php if ($productLikeDetailsRow->user_name == ''){echo 'administrator';}else {echo $productLikeDetailsRow->full_name;}?></a>+ <?php echo $productLikeDetailsRow->likes;?></span>
			</li>
		<?php }}?>
		<?php }else {?>
			<div class="no-result">
				<?php if($userProfileDetails->row()->likes>0){?>
				<b><?php if($this->lang->line('prod_det_not_avail') != '') { echo stripslashes($this->lang->line('prod_det_not_avail')); } else echo "Product details not available"; ?></b>
				<?php }else {?>
				<b><?php echo $userProfileDetails->row()->full_name;?></b> <?php if($this->lang->line('display_has_not') != '') { echo stripslashes($this->lang->line('display_has_not')); } else echo "has not"; ?> <?php echo LIKED_BUTTON;?> <?php if($this->lang->line('display_any_yet') != '') { echo stripslashes($this->lang->line('display_any_yet')); } else echo "anything yet"; ?>.
				<?php }?>
			</div>
		<?php }?>
		</ul>
	</div>  
</div>