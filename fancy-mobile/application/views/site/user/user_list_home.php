<?php
$this->load->view('site/templates/header.php');
?>
<?php if($flash_data != '') { ?>
	<div class="errorContainer" id="<?php echo $flash_data_type;?>">
		<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
		<p><span><?php echo $flash_data;?></span></p>
	</div>
<?php } ?>

        <div id="content" class="brands-stores"> 
            	<div class="figure-product">
                <dl class="profile-account"><dd>
               <strong class="fn" style="color:black;text-shadow:none;"><?php echo $list_details->row()->name;?></strong><br />
               
               <?php if($this->lang->line('user_by') != '') { echo stripslashes($this->lang->line('user_by')); } else echo "by"; ?> <?php echo $user_profile_details->row()->full_name?>
               <?php 
					$followClass = 'follow';
			        if ($loginCheck != ''){
				        $followingListArr = explode(',', $userDetails->row()->following_user_lists);
						
				        if (in_array($list_details->row()->id, $followingListArr)){
							//echo 'hai';die;
				        	$followClass = 'follow2';
				        }
			        } 
					?>
                   
        <button lid="<?php echo $list_details->row()->id;?>" <?php if ($loginCheck==''){?>require_login="true"<?php }?> class="list-<?php echo $followClass;?>"><i class="icon"></i></button>
               
               
               
               </dd></dl> </div>
        
          	<div class="figure-product">
            <ul class="purchaces-page">
                <li>
                   <a class="current"><?php if($this->lang->line('user_things') != '') { echo stripslashes($this->lang->line('user_things')); } else echo "Things"; ?> <?php echo $totalProducts;?></a>
                </li>
                <li>
                 <a href="user/<?php echo $user_profile_details->row()->user_name;?>/lists/<?php echo $list_details->row()->id;?>/followers"><?php if($this->lang->line('display_followers') != '') { echo stripslashes($this->lang->line('display_followers')); } else echo "Followers"; ?> <strong><?php echo $list_details->row()->followers_count;?></strong></a>	
                </li>
            </ul>
            
            </div>
        	 <div class="figure-product">
                    
                 
           <div style="position:relative" class="color-container">
         <h2 class="color-title"><?php 
		echo "Things";?>
		 </h2>
      <?php 
		if (($product_details != '' && $product_details->num_rows()>0) || ($notsell_product_details != '' && $notsell_product_details->num_rows()>0)){
		?>
        <ul class="color-shop">
        
        <?php 
		//print_r($product_details->result());die;
			if ($product_details != '' && $product_details->num_rows()>0){
			foreach ($product_details->result() as $productRow){
				$imgArr = array_filter(explode(',', $productRow->image));
          		$img = 'dummyProductImage.jpg';
          		foreach ($imgArr as $imgVal){
          			if ($imgVal != ''){
						$img = $imgVal;
						break;
          			}
          		}
          		$fancyClass = 'fancy';
          		$fancyText = LIKE_BUTTON;
          		if (count($likedProducts)>0 && $likedProducts->num_rows()>0){
          			foreach ($likedProducts->result() as $likeProRow){
          				if ($likeProRow->product_id == $productRow->seller_product_id){
          					$fancyClass = 'fancyd';$fancyText = LIKED_BUTTON;break;
          				}
          			}
          		}
			?>
        
        <li class="color_item">
        <a href="things/<?php echo $productRow->id;?>/<?php echo url_title($productRow->product_name,'-');?>">
        <img alt="" src="<?php echo DESKTOPURL;?>images/product/<?php echo $img;?>">
        <?php echo $productRow->product_name;?><b><?php echo $currencySymbol;?><?php echo $productRow->sale_price;?> </b>
        </a>
        </li>
        
      <?php 
			}
			}?>
     <?php  if ($notsell_product_details != '' && $notsell_product_details->num_rows()>0){
			foreach ($notsell_product_details->result() as $productRow){
				$imgArr = array_filter(explode(',', $productRow->image));
          		$img = 'dummyProductImage.jpg';
          		foreach ($imgArr as $imgVal){
          			if ($imgVal != ''){
						$img = $imgVal;
						break;
          			}
          		}
          		$fancyClass = 'fancy';
          		$fancyText = LIKE_BUTTON;
          		if (count($likedProducts)>0 && $likedProducts->num_rows()>0){
          			foreach ($likedProducts->result() as $likeProRow){
          				if ($likeProRow->product_id == $productRow->seller_product_id){
          					$fancyClass = 'fancyd';$fancyText = LIKED_BUTTON;break;
          				}
          			}
          		}
			?>
             <li class="color_item">
        <a href="user/<?php echo $productRow->user_name;?>/things/<?php echo $productRow->seller_product_id;?>/<?php echo url_title($productRow->product_name,'-');?>">
        <img alt="" src="<?php echo DESKTOPURL;?>images/product/<?php echo $img;?>">
        <?php echo $productRow->product_name;?>
        </a>
        </li>
        
      <?php 
			}
			}?>
            
    </ul>
            <?php } ?>
        </div>
                  
              </div>            
        </div>
        
        <style>
.list-follow{
float: right;
margin-top: 0.15em;

display: inline-block;
border: 1px solid #c3c3c3;
 padding: 0 1em 0.5em;
 height:30px; width:30px;
border-radius: 0.15em;
color: #4c4f53;
font-weight: bold;
background: #f0f0f0;
background: -webkit-linear-gradient(top, #fcfcfc,#f0f0f0);
background: -ms-linear-gradient(top, #fafafa,#f0f0f0);
background: -moz-linear-gradient(top, #fafafa,#f0f0f0);
background: -o-linear-gradient(top, #fafafa,#f0f0f0);
filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fafafa',endColorstr='#f0f0f0');
box-shadow: 0 1px 1px rgba(0,0,0,0.1);
line-height: 2.45em;
font-size: 0.94em;
text-shadow: 0 1px 0 #fff;
border-color: #c3c3c3 #bebebe #b6b6b6;

}
.list-follow2{
float: right;
margin-top: 0.15em;
display: inline-block;
border: 1px solid #c3c3c3;
 padding: 0 1em 0.5em;
 height:30px; width:30px;
border-radius: 0.15em;
color: #4c4f53;
font-weight: bold;
background: #f0f0f0;
background: -webkit-linear-gradient(top, #fcfcfc,#f0f0f0);
background: -ms-linear-gradient(top, #fafafa,#f0f0f0);
background: -moz-linear-gradient(top, #fafafa,#f0f0f0);
background: -o-linear-gradient(top, #fafafa,#f0f0f0);
filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fafafa',endColorstr='#f0f0f0');
box-shadow: 0 1px 1px rgba(0,0,0,0.1);
line-height: 2.45em;
font-size: 0.94em;
text-shadow: 0 1px 0 #fff;
border-color: #c3c3c3 #bebebe #b6b6b6;
}
.list-follow .icon{
	 background-image: url("images/search.4a1f857d36be.png");
display: inline-block;
width: 1.18em;
height: 1.1em;
background-position: 0 -1.25em;
vertical-align: top !important;
background-size: 50px 50px;
margin-top: 0.48em;
}
.list-follow2 .icon{
    background-position: -1.6em -1.2em;

    display: inline-block;
    height: 1.1em;
    margin-top: 0.48em;
    vertical-align: top !important;
    width: 1.25em;
}
.list-follow2 .icon{
    background-size: 50px 50px;
}
.list-follow2 .icon{
    background-image: url("images/search.4a1f857d36be.png") !important;
}
</style>
    