<?php  $this->load->view('site/user/display_user_header',$this->data); ?>
<div class="figure-product wants-product">
	<h2 class="comments-title"><?php echo site_lg('lg_activity', 'Activity');?><span style="color:#999; margin:0 0 0 3px" ></span></h2>
	<?php if($recentActivityDetails->num_rows()>0){?>
	<ul class="activity">
		<?php 
          foreach($recentActivityDetails->result() as $recentActivityDetailsRow){
		  //echo "<pre>"; print_r($recentActivityDetailsRow); die;
          	$activityTime = strtotime($recentActivityDetailsRow->activity_time);
			//$actTime = humanTiming($activityTime) . " ago" ;
 			$actTime = timespan($activityTime).' ago';
          	if($recentActivityDetailsRow->activity_name == 'fancy'){
          		$icon = 'ic-fancy';
          		$actTxt = LIKED_BUTTON;
          		if($recentActivityDetailsRow->product_name != ''){
	          		$link = 'things/'.$recentActivityDetailsRow->productID.'/'.url_title($recentActivityDetailsRow->product_name);
	          		$linkTxt = $recentActivityDetailsRow->product_name;
          		}else{
	          		$link = 'user/'.$userProfileDetails->row()->user_name.'/things/'.$recentActivityDetailsRow->activity_id.'/'.url_title($recentActivityDetailsRow->user_product_name);
	          		$linkTxt = $recentActivityDetailsRow->user_product_name;
          		}
          	}else if($recentActivityDetailsRow->activity_name == 'unfancy'){
          		$icon = 'ic-fancy';
          		$actTxt = UNLIKE_BUTTON;
          		if($recentActivityDetailsRow->product_name != ''){
	          		$link = 'things/'.$recentActivityDetailsRow->productID.'/'.url_title($recentActivityDetailsRow->product_name);
	          		$linkTxt = $recentActivityDetailsRow->product_name;
          		}else{
	          		$link = 'user/'.$userProfileDetails->row()->user_name.'/things/'.$recentActivityDetailsRow->activity_id.'/'.url_title($recentActivityDetailsRow->user_product_name);
	          		$linkTxt = $recentActivityDetailsRow->user_product_name;
          		}
          	}else if($recentActivityDetailsRow->activity_name == 'follow'){
          		$icon = 'ic-follow';
          		$actTxt = 'Followed';
          		if($recentActivityDetailsRow->user_name == ''){
    	      		$link = 'user/administrator';
	          		$linkTxt = 'administrator';
          		}else{
    	      		$link = 'user/'.$recentActivityDetailsRow->user_name;
	          		$linkTxt = $recentActivityDetailsRow->full_name;
          		}
          	}else if($recentActivityDetailsRow->activity_name == 'Owned'){
          		$icon = 'ic-follow';
          		$actTxt = 'Owned';
          		if($recentActivityDetailsRow->user_name == ''){
    	      		$link = 'user/administrator';
	          		$linkTxt = 'administrator';
          		}else{
    	      		$link = 'user/'.$recentActivityDetailsRow->user_name;
	          		$linkTxt = $recentActivityDetailsRow->full_name;
          		}
			}else if($recentActivityDetailsRow->activity_name == 'unfollow'){
          		$icon = 'ic-follow';
          		$actTxt = 'Unfollowed';
          		if($recentActivityDetailsRow->user_name == ''){
    	      		$link = 'user/administrator';
	          		$linkTxt = 'administrator';
          		}else{
    	      		$link = 'user/'.$recentActivityDetailsRow->user_name;
	          		$linkTxt = $recentActivityDetailsRow->full_name;
          		}
          	}else if($recentActivityDetailsRow->activity_name == 'Own'){
          		$icon = 'ic-follow';
          		$actTxt = 'Own';
          		if($recentActivityDetailsRow->user_name == ''){
    	      		$link = 'user/administrator';
	          		$linkTxt = 'administrator';
          		}else{
    	      		$link = 'user/'.$recentActivityDetailsRow->user_name;
	          		$linkTxt = $recentActivityDetailsRow->full_name;
          		}
          	}
			else if($recentActivityDetailsRow->activity_name == 'Want'){
          		$icon = 'ic-follow';
          		$actTxt = 'Want';
          		if($recentActivityDetailsRow->user_name == ''){
    	      		$link = 'user/administrator';
	          		$linkTxt = 'administrator';
          		}else{
    	      		$link = 'user/'.$recentActivityDetailsRow->user_name;
	          		$linkTxt = $recentActivityDetailsRow->full_name;
          		}
          	}
			else if($recentActivityDetailsRow->activity_name == 'Wanted'){
          		$icon = 'ic-follow';
          		$actTxt = 'Wanted';
          		if($recentActivityDetailsRow->user_name == ''){
    	      		$link = 'user/administrator';
	          		$linkTxt = 'administrator';
          		}else{
    	      		$link = 'user/'.$recentActivityDetailsRow->user_name;
	          		$linkTxt = $recentActivityDetailsRow->full_name;
          		}
          	}
			$user_img = 'user-thumb1.png';
			if($userProfileDetails->row()->thumbnail != ''){
				$user_img = $userProfileDetails->row()->thumbnail;
			}
          ?>
		<li><i class="icon"></i>
				<a href="<?php echo $link; ?>"><img src="<?php echo DESKTOPURL;?>images/users/<?php echo $user_img; ?>"></a><?php echo $actTxt;?>
				<a href="<?php echo $link; ?>"><?php echo $userProfileDetails->row()->user_name;?></a>
				<small class="time"><?php echo $actTime;?></small>
		</li>
		<?php }?>
	</ul>
	<?php }?>
</div>
                   