<?php
$this->load->view('site/templates/header');
?>
<style type="text/css" media="screen">


#edit-details {
    color: #FF3333;
    font-size: 11px;
}
.option-area select.option {
    border: 1px solid #D1D3D9;
    border-radius: 3px 3px 3px 3px;
    box-shadow: 1px 1px 1px #EEEEEE;
    height: 22px;
    margin: 5px 0 12px;
}
a.selectBox.option {
    margin: 5px 0 10px;
    padding: 3px 0;
}
a.selectBox.option .selectBox-label {
    font: inherit !important;
    padding-left: 10px;

}
form label.error{
	color:red;
}
.button{
	width: 95px;
	overflow: visible;
	margin: 0;
	padding: 8px 8px 10px 7px;
	border: 0;
	border-radius: 4px;
	font-weight: bold;
	font-size: 15px;
	line-height: 22px;
	text-align: center;
	color: #fff;
	background: #588cc7;
}
.button:hover{
	background: #3e73b7;
}
</style>
<div class="lang-en wider no-subnav thing signed-out winOS">
    <div id="container-wrapper">
	   <div class="container ">
	    <?php if($flash_data != '') { ?>
		<div class="errorContainer" id="<?php echo $flash_data_type;?>">
			<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
			<p><span><?php echo $flash_data;?></span></p>
		</div>
		<?php } ?>
        <style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
   
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    //background-color: #dddddd;
}
.unread span { 
color: #000000;
font-size: 17px;
}
.read span {
color: rgb(112, 114, 128);
font-size: 14px;
}

.unreadmsg{
	color: red !important;
	font-size: 14px !important;
}
.new_auction{
	float:left
}

.open_before{
	float: left;
    margin-left: 25%;
}
.new_offer {
	float:left;
}

.price{
	float:left;
	margin-left: 30%;
}
.view{
	float: left;
    margin-left: 30%;
}
.auction_sent{
	float: left;
    margin-left: 30%;
}
.offer{
	float: left;
   
}
.new_msg{
	float: left;
}
.bidmessage{
	float: left;
	margin-left: 30%;
}
</style>
		<div class="wrapper-content">					
            <div class="profile-list">            
                <div class="page-header padding_all15 margin_all0">
                    <h2> <?php if($this->lang->line('Inbox') != '') { echo stripslashes($this->lang->line('Inbox')); } else echo "Inbox"; ?></h2>
             	    <h2 style="text-align:left;" class="border_bottom padding_bottom15">	</h2>		 
                </div>
                <div class="box-content">
                    <form accept-charset="utf-8" method="post" action="site/product/sell_it/seo" id="sellerProdEdit1">
                    <section class="left-section min_height" style="height: 924px;">	
                        <div class="person-lists bs-docs-example">
                       
    <div class="dun-data" id="dun_requests">
        <ul class="bids_ul">
            <?php
            if($bid_messages->num_rows() > 0){
            // echo '<pre>';print_r($bid_messages->result());die;
                    foreach($bid_messages->result() as $bid){
                        
                        
                        $img_scr = DESKTOPURL."images/users/user-thumb1.png";
                        if($bid->thumbnail !=''){
                            $img_scr = DESKTOPURL.'images/users/'.$bid->thumbnail;
                                        
                        }
                        $s_time = $bid->start_time;
                        $start_time = date('Y-m-d H:i:s',strtotime($s_time));
                        
                        $end_time = $bid->expire_time;
                        $expire_time = date('Y-m-d H:i:s',strtotime($end_time));
                        
                        $c_time  = date('Y-m-d H:i:s', time());
                        $currnt_time = strtotime( $c_time.' + 3 minute');
                        $current_time  = date('Y-m-d H:i:s', $currnt_time);
                       
                        if($start_time > $current_time){
                            $start_time_status = 'waiting';
                        }else{
                            $start_time_status = 'over';
                        }
                         
                        $expire_time_status = 'bablu';
                        if($current_time > $expire_time) {
                            $expire_time_status = 'expired';
						}
                    
            ?>              
                        <li>
                            <?php
                            if($bid->msg_read == "No"){ 
                            
                                if($bid->type == 'auction'){
                                    
                                    if($expire_time_status != 'expired'){
                                        
                                        if($start_time_status != 'waiting') { ?><a href='conversation/<?php echo $bid->BidId; ?>' > <?php } ?>
                                        
                                            <span class="new_auction" > New Auction </span>
                                            
                                            <?php if($start_time_status == 'waiting'){ ?>
                                                
                                                <div class="starttime">
                                                    Start <div  style="color: red;font-size: 15px; display: inline-block; margin-left: 8px;" class="count_down" data-countdown="<?php echo $start_time; ?>"></div>
                                                </div>
                                                <div class="expiretime">
                                                    End <div style="color:red;font-size:15px; display: inline-block; margin-left: 8px;"><?php echo date('d F Y',strtotime($expire_time)); ?></div>
                                                </div>
                                                <div style="" class="view_class"> View </div>
                                            <?php } else if($start_time_status == 'over'){ ?>
                                                
                                                <div class="open_before" >
                                               
                                                    <div  style="color:red;font-size:15px;display: inline-block; margin-left: 18px;" class="count_down" data-countdown="<?php echo $expire_time; ?>"></div>
                                                </div>
                                                <div class="view"> View </div>
                                            <?php } ?>
                                        
                                        <?php if($start_time_status != 'waiting'){ ?></a><?php } 
                                    }else{ 
											$this->user_model->update_details(BID_MESSAGE,array('auction_FR_status'=>'Expired'),array('BidId'=>$bid->BidId));
										?>
                                        <div class="read_color">
                                            <span class="new_auction"> New Auction </span>
                                            <span class="starttime"> Start : <?php echo date('d M Y',strtotime($start_time));?></span>
                                            <span class="expiretime"> End : <?php echo date('d M Y',strtotime($expire_time));?></span>
                                            <span class="view_class"> Expired </span>
                                        </div>
                                    <?php
                                    }
                                    
                                }else if( $bid->type == 'offer' ){ ?>
                                    <a  href='conversation/<?php echo $bid->BidId; ?>' class="unreadmsg" >
                                        <span class="new_offer"> New Offer </span>
                                        <div class="price"> Price : <?php  echo $bid->offer_price.' '.$currencySymbol; ?> </div>
                                        <div class="view"> View </div>
                                    </a>
                                    
                                <?php   
                                }
                                else if( $bid->type == 'bid' ){ ?>
                                    
                                    <a  href='conversation/<?php echo $bid->BidId; ?>' class="<?php if($bid->msg_read == 'No'){ echo 'unreadmsg'; } ?>">
                                        <span class="new_msg">New Message</span>
                                        <span class="bidmessage"><?php echo substr($bid->message,0,12).'...'; ?></span>
                                        <div class="view" > View</div>
                                    </a>
                                <?php   
                                }
                                
                            }else{ 
                                
                                if($bid->type == 'auction'){ ?>
                                    <a href='conversation/<?php echo $bid->BidId; ?>' class="<?php if($bid->rspn_read == 'NO'){ echo 'unreadmsg'; }?>">
                                    <span class="new_auction">Auction</span>
									<span class="auction_sent"> Sent : <?php echo date('M d Y',strtotime($bid->dateAdded)); ?></span>
                                    <div class="view">View</div>
                                    </a>
                                <?php
                                }else if( $bid->type == 'offer' ){ ?>
                                    <a href='conversation/<?php echo $bid->BidId; ?>' >
                                        <span class="offer"> Offer </span>
                                        <span class="price"> Price : <?php  echo $bid->offer_price.' '.$currencySymbol;; ?> </span>
                                        <span class="view"> View </span>
                                    </a>
                                <?php   
                                }
                                else if( $bid->type == 'bid' ){ ?>
                                    <a href='conversation/<?php echo $bid->BidId; ?>' >
                                        <span class="new_msg"> Message </span>
										 <span class="bidmessage"><?php echo substr($bid->message,0,12).'...'; ?></span>
                                        <div class="view">View</div>
                                    </a>
                                <?php   
                                }
                            }
                            
                            ?>
                        </li>
                        
            <?php
                    }
                }else{
            ?>
                <li class="dun-list"> <span>No Request Yet<span></li>
            <?php }?>
                <script type="text/javascript">
                    $(document).ready(function(e) {

                        
                        
                        $('[data-countdown]').each(function() {
                            var day  = "Day";
                            var days = "Days";
                            var $this = $(this), finalDate = $(this).data('countdown');
                            $this.countdown(finalDate, function(event) {
                                if( event.strftime('%D') != 00 && event.strftime('%D') == 01 ){
                                    $this.html(event.strftime('%D '+day+' %H:%M:%S'));
                                }
                                else if( event.strftime('%D') > 01 ){
                                    $this.html(event.strftime('%D '+days+' %H:%M:%S'));
                                }
                                else{
                                    $this.html(event.strftime('%H:%M:%S'));
                                }
                           });
                        });
                       
                    });
                </script>
        </ul>
    </div>
</div>
                        </div>
                    </section>
                        
                    </form>
                </div>
            </div>
        </div>

<script type="text/javascript">
    $(document).ready(function(e) {
        $('[data-countdown]').each(function() {
            var day  = "Day";
            var days = "Days";
            var $this = $(this), finalDate = $(this).data('countdown');
            $this.countdown(finalDate, function(event) {
                if( event.strftime('%D') != 00 && event.strftime('%D') == 01 ){
                    $this.html(event.strftime('%D '+day+' %H:%M:%S'));
                }
                else if( event.strftime('%D') > 01 ){
                    $this.html(event.strftime('%D '+days+' %H:%M:%S'));
                }
                else{
                    $this.html(event.strftime('%H:%M:%S'));
                }
           });
        });
       
    });
</script>
<!-- COUNTDOWN -->
<script type="text/javascript" src="js/assets/lib/countdown/jquery.plugin.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/site/jquery.countdown.js"></script>
<!-- ./COUNTDOWN -->

            
	   </div>
    </div>
</div>

<script src="js/site/<?php echo SITE_COMMON_DEFINE ?>filesjquery_zoomer.js" type="text/javascript"></script>
<script type="text/javascript" src="js/site/<?php echo SITE_COMMON_DEFINE ?>selectbox.js"></script>
<script type="text/javascript" src="js/site/thing_page.js"></script>
<script type="text/javascript" src="js/site/jquery.validate.js"></script>
<script>
	$("#sellerProdEdit1").validate();
</script>
