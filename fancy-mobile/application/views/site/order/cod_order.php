<?php 
$this->load->view('site/templates/header.php');
?>
<div class="purchaces" id="content">
<?php if($flash_data != '') { ?>
		<div class="errorContainer" id="<?php echo $flash_data_type;?>">
			<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
			<p><span><?php echo $flash_data;?></span></p>
		</div>
<?php } ?>

<div style=" margin-top: 20px;" class="figure-product">
	<h2 class="comments-title"><?php if($this->lang->line('cart_ord_confirm') != '') { echo stripslashes($this->lang->line('cart_ord_confirm')); } else echo "Order Confirmation"; ?></h2>			
					<div class="chart-wrap">
						<?php if($Confirmation =='Success'){ ?>                    
							<div class="cart-payment-wrap card-payment new-card-payment">
								<strong><?php if($this->lang->line('order_tran_sucss_cod') != '') { echo stripslashes($this->lang->line('order_tran_sucss_cod')); } else echo "Thank you, we will contact you shortly.<br>Please wait for purchase details"; ?></strong>
							</div>   
			            <?php
						$this->output->set_header('refresh:5;url='.base_url().'purchases');
						
							}elseif($Confirmation =='Failure'){ ?>        
							<div class="cart-payment-wrap card-payment new-card-payment">
								<strong><?php if($this->lang->line('order_tran_failure') != '') { echo stripslashes($this->lang->line('order_tran_failure')); } else echo "Your Transaction Failure<br>Please wait for cart details"; ?></strong>
								<div class="payment_success"><b><?php echo urldecode($errors); ?></b></div>
								<div class="payment_success"><img src="images/site/failure_payment.png" /></div>
							</div>
							<?php $this->output->set_header('refresh:5;url='.base_url().'cart');  }?>
					</div>
	</div>
</div>