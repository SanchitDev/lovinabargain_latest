<?php $this->load->view('site/templates/header');?>
    	
        <div class="boxybox" id="content">
        
        <div class="color-container">
        <h2 style="margin-bottom:0" class="color-title"><?php echo site_lg('lg_fancy_Box', 'Fancy Box');?></h2>
        <p class="banner2">
            <small><?php echo site_lg('lg_month_subscription', 'The  Box Monthly Subscription');?></small>
            <span><?php echo site_lg('lg_every_month', 'Wonderful things from  delivered to your door every month');?></span>
            </p>
            
            	<?php 
                       if ($fancyboxList->num_rows()>0){
				?>
            <ul class="boxy-list">
            
             <?php 
                       	foreach ($fancyboxList->result() as $fancyboxRow){
                       		$image = 'dummyProductImage.jpg';
                       		$imgArr = explode(',', $fancyboxRow->image);
                       		if (count($imgArr)>0){
                       			foreach ($imgArr as $imgRow){
                       				if ($imgRow != ''){
                       					$image = $imgRow;
                       					break;
                       				}
                       			}
                       		}
                       ?>      
            <li>
            
            <a href="fancybox/<?php echo $fancyboxRow->id; ?>/<?php echo $fancyboxRow->name; ?>">
            <img alt="" src="<?php echo DESKTOPURL;?>images/fancyybox/<?php echo $image;?>">
           
            <b><?php echo $fancyboxRow->name; ?></b>
            <?php echo $fancyboxRow->description; ?>.
            </a>
            
            </li>
            <?php } ?>
          
            </ul>
            <?php } ?>
      
        </div>
           
        </div>
    </div>
</div>

</body>
</html>
