<?php $this->load->view('site/templates/header'); ?>
<div class="lang-en wider no-subnav thing signed-out winOS">

<?php if($gift_ProductDetails->num_rows() ==1){?>
<?php
	$img = 'dummyProductImage.jpg';
	$imgArr = explode(',', $gift_ProductDetails->row()->image);
	if (count($imgArr)>0){
		foreach ($imgArr as $imgRow){
			if ($imgRow != ''){
				$img = $pimg = $imgRow;
				break;
			}
		}
	}
	$user_img = 'user-thumb1.png';
	$user_imgArr = explode(',', $gift_ProductDetails->row()->thumbnail);
	if(count($user_imgArr)>0){
		foreach ($user_imgArr as $user_imgRow){
			if ($user_imgRow != ''){
				$user_img = $user_pimg = $user_imgRow;
				break;
			}
		}
	}
	$recipient_img = 'user-thumb1.png';
	$recp_imgArr = explode(',', $gift_ProductDetails->row()->recipient_image);
	if (count($recp_imgArr)>0){
		foreach ($recp_imgArr as $recp_imgRow){
			if ($recp_imgRow != ''){
				$recipient_img = $recp_pimg = $recp_imgRow;
				break;
			}
		}
	}
?>
  <form name="contribute_payment" id="contribute_payment" method="post" action="site/groupgift/PaymentCredit">
	<div id="container-wrapper">
		<div class="container">
			<div class="wrapper-content right-sidebar" style="background:none; box-shadow:none;">
			<?php if($flash_data != '') { ?>
					<div class="errorContainer" id="<?php echo $flash_data_type;?>">
						<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
						<p><span><?php echo $flash_data;?></span></p>
					</div>
			<?php } ?>
				<div id="content" style="padding:0px; background:none;width: 680px;">
					<div class="detail_leftbar4">
						<h3 class="detail_link_list"><?php if($this->lang->line('gift_contribution') != '') { echo stripslashes($this->lang->line('gift_contribution')); } else echo "Gift Contribution"; ?></h3>
						<div class="contribution_box">
							<h4><?php if($this->lang->line('my_contribution') != '') { echo stripslashes($this->lang->line('my_contribution')); } else echo "My contribution"; ?></h4>
							<p><?php if($this->lang->line('contributor_amount_text') != '') { echo stripslashes($this->lang->line('contributor_amount_text')); } else echo "You are free to enter any amount from"; ?><b>$<?php echo $this->config->item('default_amount');?></b><?php if($this->lang->line('contributor_amount_text1') != '') { echo stripslashes($this->lang->line('contributor_amount_text1')); } else echo "up to the maximum of the group gift. Please note that you wont be charged if the campaign is unsuccessful."; ?></p> 
							<ul class="contribution_amount">
								<li class= "list1" id="5.00">$5</li>
								<li class= "list2" id="15.00">$15</li>
								<li class= "list3" id="25.00">$25</li>
								<li class= "list4" id="35.00">$35</li>
								<li class= "list5" id="50.00">$50</li>
							</ul>
							<a class="contribution_otheramount"><?php if($this->lang->line('other_amount') != '') { echo stripslashes($this->lang->line('other_amount')); } else echo "other amount"; ?></a>
							<?php if($contribute_details >0){
									$contribute_amount = 0;
									foreach($contribute_details->result() as $_contributedetails){
										$contribute_amount += $_contributedetails->amount;
											if($contribute_amount!=''){
												$amount = $contribute_amount; 
											}else{
												$amount = "0";
											}
									}							  
								}else{}					
							    $total_value= $gift_ProductDetails->row()->gift_total;
								$contribute_value = round($amount, 2);
								$remain_value = $total_value - $contribute_value;
							?>
							<div class="total_contribution"><input type="text" name="total_price" value="" id="amount" class="total_scroll" onkeyup="getamount(this.value,'<?php echo $remain_value;?>');"> <small><?php echo site_lg('lg_usd', 'USD');?></small></div>
							<div class="contribution_maximum"><?php if($this->lang->line('maximum_amount_contribute') != '') { echo stripslashes($this->lang->line('maximum_amount_contribute')); } else echo "Maximum amount to contribute:"; ?> <strong>$<?php echo $gift_ProductDetails->row()->gift_total;?> USD</strong></div>
						</div>
						<div class="contribution_msg">
							<div class="contribution_msg_box">
								<h5><?php if($this->lang->line('giftcard_person_msg') != '') { echo stripslashes($this->lang->line('giftcard_person_msg')); } else echo "Personal message"; ?></h5>
								<textarea class="contribution_textbox"><?php if($this->lang->line('write_something_nice_to') != '') { echo stripslashes($this->lang->line('write_something_nice_to')); } else echo "Write something nice to"; ?> <?php echo $gift_ProductDetails->row()->user_name;?>.</textarea>	
							</div>
						</div>
					</div>
					<div class="detail_leftbar4">
						<h3 class="detail_link_list"><?php if($this->lang->line('cart_pay_mthd') != '') { echo stripslashes($this->lang->line('cart_pay_mthd')); } else echo "Payment Method"; ?></h3>
						<div class="contribution_payment">
							<h5><?php if($this->lang->line('pay_with_creditcard') != '') { echo stripslashes($this->lang->line('pay_with_creditcard')); } else echo "pay with credit card."; ?></h5>
							<table width="100%" cellpadding="0" cellspacing="0" border="0" class="card_payment">
								<tr>
									<th width="7%" style="background:#F4F5F6; color: #83878F; font-size: 12px; padding: 6px 0 6px 10px; text-align: left;"></th>
									<th width="27%" style="background:#F4F5F6; color: #83878F; font-size: 12px; padding: 6px 0 6px 10px; text-align: left;"><?php if($this->lang->line('card') != '') { echo stripslashes($this->lang->line('card')); } else echo "Card"; ?></th>
									<th width="40%" style="background:#F4F5F6; color: #83878F; font-size: 12px; padding: 6px 0 6px 10px; text-align: left;">&nbsp;</th>
									<th width="16%" style="background:#F4F5F6; color: #83878F; font-size: 12px; padding: 6px 0 6px 10px; text-align: left;"><?php if($this->lang->line('name_on_card') != '') { echo stripslashes($this->lang->line('name_on_card')); } else echo "Name on card"; ?></th>
									<th width="23%" style="background:#F4F5F6; color: #83878F; font-size: 12px; padding: 6px 0 6px 10px; text-align: left;"><?php if($this->lang->line('expiration_date') != '') { echo stripslashes($this->lang->line('expiration_date')); } else echo "My contribution"; ?><?php echo site_lg('lg_expiration_date', 'Expiration date');?></th>
								</tr>
								<tr>
									<td style="padding:20px 0 20px 13px;"><input type="radio"  style="float:left;" checked="checked"/></td>
									<td colspan="4" style="padding:20px 20px 20px 10px;">
										<div class="new_payment_method">
											<span class="icon_card"></span>
											<strong><?php if($this->lang->line('new_payment_method') != '') { echo stripslashes($this->lang->line('new_payment_method')); } else echo "New payment method"; ?></strong>
										</div>
									</td>
								</tr>
								<tr>
									<td></td>
									<td colspan="4" style="padding:20px 20px 20px 5px;"><h5><?php if($this->lang->line('cart_pay_mthd') != '') { echo stripslashes($this->lang->line('cart_pay_mthd')); } else echo "Payment Method"; ?></h5></td>
								</tr>
								<tr>
									<td></td>
									<td>
										<label><?php if($this->lang->line('card_number') != '') { echo stripslashes($this->lang->line('card_number')); } else echo "Card number"; ?></label>
										<input type="text" value="" name="cardNumber" id="card_no" size="16" maxlength="16" class="required card_input width160" />
									</td>
									<td>
							<label><?php if($this->lang->line('groupgift_expiration') != '') { echo stripslashes($this->lang->line('groupgift_expiration')); } else echo "Expiration"; ?></label>
							<select id="CCExpDay" name="CCExpDay" class="card_scroll width55 select-round select-white select-date selectBox required">		
								<option value="01" <?php if(date('m')=='01'){ echo $Sel;} ?>>01</option>
								<option value="02" <?php if(date('m')=='02'){ echo $Sel;} ?>>02</option>
								<option value="03" <?php if(date('m')=='03'){ echo $Sel;} ?>>03</option>
								<option value="04" <?php if(date('m')=='04'){ echo $Sel;} ?>>04</option>
								<option value="05" <?php if(date('m')=='05'){ echo $Sel;} ?>>05</option>
								<option value="06" <?php if(date('m')=='06'){ echo $Sel;} ?>>06</option>
								<option value="07" <?php if(date('m')=='07'){ echo $Sel;} ?>>07</option>
								<option value="08" <?php if(date('m')=='08'){ echo $Sel;} ?>>08</option>
								<option value="09" <?php if(date('m')=='09'){ echo $Sel;} ?>>09</option>
								<option value="10" <?php if(date('m')=='10'){ echo $Sel;} ?>>10</option>
								<option value="11" <?php if(date('m')=='11'){ echo $Sel;} ?>>11</option>
								<option value="12" <?php if(date('m')=='12'){ echo $Sel;} ?>>12</option>
						  </select>
						  <select id="CCExpMnth" name="CCExpMnth" class="card_scroll width70 select-round select-white select-date selectBox required">
								<?php for($i=date('Y');$i< (date('Y') + 25);$i++){ ?>	
									<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
								<?php } ?>    
						  </select>
									</td>
									<td>
										<label><?php if($this->lang->line('cvc_code') != '') { echo stripslashes($this->lang->line('cvc_code')); } else echo "CVC code"; ?></label>
										<input type="text" name="creditCardIdentifier" placeholder="CVC" id="cvc_no" value="" class="required card_input width70" />
									</td>
									<td>
										<label><?php if($this->lang->line('name_on_card') != '') { echo stripslashes($this->lang->line('name_on_card')); } else echo "Name on card"; ?></label>
										<input type="text" name="full_name" value="" class="card_input width140 required" />
									</td>
								</tr>
								<tr>
									<td></td>
									<td colspan="4" style="padding:20px 20px 20px 5px;"><h5><?php if($this->lang->line('checkout_billing_addr') != '') { echo stripslashes($this->lang->line('checkout_billing_addr')); } else echo "Billing Address"; ?></h5></td>
								</tr>
								<tr>
									<td></td>
									<td colspan="4">
										<table width="100%" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td><label><?php if($this->lang->line('shipping_address_comm') != '') { echo stripslashes($this->lang->line('shipping_address_comm')); } else echo "Address"; ?></label></td>
												<td><input type="text" name="address1" placeholder="Address Line 1" value="" class="card_input required" /></td>
											</tr>
											<tr>
												<td><label>&nbsp;</label></td>
												<td><input type="text" placeholder="Address Line 2" value="" name="address2" class="card_input" /></td>
											</tr>
											<tr>
												<td><label><?php if($this->lang->line('header_country') != '') { echo stripslashes($this->lang->line('header_country')); } else echo "Country"; ?></label></td>
												<td><select class="card_scroll" name="country">
												 <?php if($country_list->num_rows()>0){
												    foreach($country_list->result() as $countries){
													if($countries->country_code =="US"){?>
													  <option value="<?php echo $countries->country_code;?>" selected="selected"><?php echo $countries->name;?></option>
													<?php } else{?>
													<option value="<?php echo $countries->country_code;?>"><?php echo $countries->name;?></option>
												 <?php }}
                                                 }?>
												</select></td>
											</tr>
											<tr>
												<td><label><?php if($this->lang->line('checkout_state') != '') { echo stripslashes($this->lang->line('checkout_state')); } else echo "State"; ?></label></td>
												<td><input type="text" name="state" placeholder="Please enter state." value="" class="card_input required" /></td>
											</tr>
											<tr>
												<td><label><?php if($this->lang->line('grougift_city') != '') { echo stripslashes($this->lang->line('grougift_city')); } else echo "City"; ?></label></td>
												<td><input type="text" name="city" placeholder="Please enter city." value="" class="card_input required" />
												<input id="email1" name="email" value="<?php echo $gift_ProductDetails->row()->email; ?>" type="hidden">
												<input type="hidden" name="creditvalue" value="authorize">
												<input type="hidden" name="groupgift_id" value="<?php echo $gift_ProductDetails->row()->id; ?>">
												<input type="hidden" name="product_id" value="<?php echo $gift_ProductDetails->row()->gift_seller_id; ?>">
												</td>
											</tr>
											<tr>
												<td><label><?php if($this->lang->line('header_zip_postal') != '') { echo stripslashes($this->lang->line('header_zip_postal')); } else echo "Zip / Postal code"; ?></label></td>
												<td><input type="text" placeholder="Please enter zip code." value="" name="zipcode" class="card_input ValidZipCode" /></td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</div>
						<div class="contribution_btn_area">
							<input type="submit" value="Submit Contribution" style="padding:9px 12px 9px 15px; float:left; cursor:pointer;" class="contribute_btn" />
							<div class="security">
								<i class="ic-security"></i>
								<?php if($this->lang->line('secure_transaction') != '') { echo stripslashes($this->lang->line('secure_transaction')); } else echo "Secure Transaction"; ?>
							</div>
						</div>
					</div>
				</div>
			<aside style="padding:0px; width: 255px;" id="sidebar">
				<section class="thing-section gift-section">
					<div class="detail_sidebar_list">
						<h3 class="detail_link_list"><?php if($this->lang->line('contribute_to') != '') { echo stripslashes($this->lang->line('contribute_to')); } else echo "Contribute to"; ?></h3>
						<div class="contribute_img"><img alt="" src="<?php echo DESKTOPURL;?>images/product/<?php echo $img;?>" /></div>
						<div class="contribute_summary">
							<h5><a href="gifts/<?php echo $gift_ProductDetails->row()->gift_seller_id;?>"><?php echo $gift_ProductDetails->row()->gift_name;?></a>
							<small><?php if($this->lang->line('header_small_for') != '') { echo stripslashes($this->lang->line('header_small_for')); } else echo "for"; ?> <?php echo $gift_ProductDetails->row()->user_name;?></small></h5>
							<p class="contribute_message"><?php echo $gift_ProductDetails->row()->gift_description;?></p>
						</div>
						<div class="statistics">
							<ul>
							<li class="time"><span><b>$<?php echo round($amount, 2);?></b> <?php if($this->lang->line('contributed') != '') { echo stripslashes($this->lang->line('contributed')); } else echo "Contributed"; ?></span></li>
							<li class="people"><span>
								<?php if($expiry_date->num_rows() ==1) {
										$start_date = $gift_ProductDetails->row()->created;
										$start_date = strtotime($start_date);
										$end_date = strtotime("+".$this->config->item('expiry_days')." day", $start_date);
										$timeleft = $end_date-$start_date;
										$daysleft = round((($timeleft/24)/60)/60); 
								?>		
								<b><?php echo $daysleft;}?></b> <?php if($this->lang->line('days_left') != '') { echo stripslashes($this->lang->line('days_left')); } else echo "Days left"; ?></span>
							</li>
							</ul>
						</div>
						<div class="contribute_prize_zone">
							<div class="progress-bar">
					  <span class="fst"><?php if($this->lang->line('gift_goal') != '') { echo stripslashes($this->lang->line('gift_goal')); } else echo "Gift Goal"; ?></span>
					  <span class="lst">$<?php echo $gift_ProductDetails->row()->gift_total;?></span>
					<?php 
					function progressBar($percentage){
						print "<div id=\"progress-bar\" class=\"all-rounded\">\n";
						print "<div id=\"progress-bar-percentage\" class=\"all-rounded\" style=\"width: $percentage%\">";
								if ($percentage > 0) {/* print "$percentage%"; */} else {print "<div class=\"spacer\">&nbsp;</div>";}
						print "</div></div>";
			       }
					$val1 = $gift_ProductDetails->row()->gift_total;
					$val2 = round($amount, 2);
					$res = (($val1 - $val2)/($val1))*100;
					$res1 = 100 - $res;
					$res1 = round($res1, 2); 
					progressBar($res1);
					?>
					</div>
						</div>
					</div>
				</section>
				<!-- / thing-section -->
				<hr>
			 </aside>
		  </div>
	  </div>
   </div>
   </form>
<script type="text/javascript" src="js/site/jquery.validate.js"></script>
<script>
//$("#contribute_payment").validate();
$(document).ready(function(){
	$(".total_contribution").hide();
	$(".contribution_amount li:first-child").addClass("on_select");
	var amount = $(".contribution_amount li:first-child").attr('id');
	$("#amount").val(amount);
	$(".contribution_otheramount").click(function(){
		$(".contribution_otheramount").hide();
		$(".total_contribution").show();
	});
	$(".contribution_amount li").click(function(){
            $(".contribution_amount li").removeClass('on_select');
            $(this).addClass('on_select');
			var amounts = $(this).attr('id');
				$("#amount").val(amounts);
        });
});
function getamount(value,total){
		if(parseFloat(value) > parseFloat(total)){
			$("#amount").val(total);
		}
	}
</script>
<script>
$(document).ready(function(){	
    var total_value = '<?php echo $gift_ProductDetails->row()->gift_total;?>';
	    contributor_value = '<?php echo round($amount, 2);?>';
		remain_value = parseFloat(total_value) - parseFloat(contributor_value);
		if($('.list1').attr('id') > remain_value){
		    $('.contribution_amount li').hide();
		}else if($('.list2').attr('id') > remain_value){
			$('.list1').show(); $('.list2').hide();$('.list3').hide(); $('.list4').hide();$('.list5').hide();
		}else if($('.list3').attr('id') > remain_value){
		    $('.list1').show(); $('.list2').show();$('.list3').hide(); $('.list4').hide();$('.list5').hide();
		}else if($('.list4').attr('id') > remain_value){
		    $('.list1').show(); $('.list2').show();$('.list3').show(); $('.list4').hide();$('.list5').hide();
			
		}else if($('.list5').attr('id') > remain_value){
		   $('.list1').show(); $('.list2').show();$('.list3').show(); $('.list4').show();$('.list5').hide();
		}else{
		  $('.contribution_amount li').show();
		}
/* 	$("#contribute_payment").validate();
	$(".ValidZipCode", function( value, element ) {
		var result = this.optional(element) || value.length >= 3;
		if (!result) {
			return false;
		}
		else{
			return true;
		}
	}, "Please Enter the Correct ZipCode"); */
 
});
</script>
<style>
#progress-bar {width:225px; margin:10px 0px 0px 0px; background:#cccccc; border:3px solid #f2f2f2; height:5px;}
#progress-bar-percentage {background:#3063A5; padding:5px 0px 0px 0px; color:#FFF; font-weight:bold;
   text-align: center; }
</style>
<?php }?>
</div>
<?php
$this->load->view('site/templates/footer');
?>