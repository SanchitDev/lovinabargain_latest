<?php
$this->load->view('site/templates/header',$this->data);
?>

<!-- Section_start -->

		<?php if($flash_data != '') { ?>
		<div class="errorContainer" id="<?php echo $flash_data_type;?>">
			<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
			<p><span><?php echo $flash_data;?></span></p>
		</div>
		<?php } ?>
      
        	<div id="content" class="account-page">
        <div class="account-wrap">

 <dl class="profile-account">
<dd>
		<h2 class="ptit"><?php if($this->lang->line('group_gifts') != '') { echo stripslashes($this->lang->line('group_gifts')); } else echo "Group Gifts"; ?></h2>
	<?php if ($groupgiftList->num_rows() == 0){?>
		<div class=" section shipping no-data">
			
			<span class="icon"><i class="ic-ship"></i></span>
			<p><?php if($this->lang->line('Your_havent_group_gift_history') != '') { echo stripslashes($this->lang->line('Your_havent_group_gift_history')); } else echo "You haven't group gift history yet."; ?></p>
			
			<!--<button class="btn-shipping add_"><i class="ic-plus"></i> <?php if($this->lang->line('Your_group_gift_history') != '') { echo stripslashes($this->lang->line('Your_group_gift_history')); } else echo "Your Fancy group gift history."; ?></button>-->
		</div>
	<?php 
	}else {
	?>
	<div class="section shipping">
            <h3><?php if($this->lang->line('Your_group_gift_history') != '') { echo stripslashes($this->lang->line('Your_group_gift_history')); } else echo "Your group gift history."; ?></h3>
                	<div class="chart-wrap">
            <table class="chart">
                <thead>
                    <tr>
                        <th><?php if($this->lang->line('groupgift_name') != '') { echo stripslashes($this->lang->line('groupgift_name')); } else echo "Gift Name"; ?></th>
                        <th><?php if($this->lang->line('groupgift_status') != '') { echo stripslashes($this->lang->line('groupgift_status')); } else echo "Status"; ?></th>
                        <th><?php if($this->lang->line('groupgift_end_date') != '') { echo stripslashes($this->lang->line('groupgift_end_date')); } else echo "End Date"; ?></th>
                        <th><?php if($this->lang->line('groupgift_contribution') != '') { echo stripslashes($this->lang->line('groupgift_contribution')); } else echo "Contribution"; ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if($groupgiftList->num_rows() > 0){?>
					      <?php foreach ($groupgiftList->result() as $row){?>
                               <tr>
                                  <td><a style="text-decoration:underline;" href="gifts/<?php echo $row->gift_seller_id;?>"><?php echo $row->gift_name;?></a><br/>
								  <span><?php echo "#".$row->gift_seller_id;?></span>
								  </td>
                                  <?php if($row->status == "Cancelled") { ?>
                                  <td><?php echo "<b>Canceled(by creator)</b>";?></td>
								  <?php } elseif($row->status == "Cancelled Admin") {?>
								     <td><?php echo "<b>Canceled(by admin)</b>";?></td>
								  <?php } elseif($row->status == "Unsuccessful"){?>
								     <td><?php echo "<b>".$row->status."</b>";?></td>
								 <?php } else{?>
								      <td><?php echo "<b>".$row->status."</b>";?></td>
								  <?php }?>
                                  <td>
								  <?php $date = $row->created;
								        $date = strtotime($date);
                                        $date = strtotime("+7 day", $date);
                                        echo date('M d, Y, h:i', $date);
								  ?>
								  </td>
								 <?php if($row->status == "Cancelled" || $row->status == "Cancelled Admin" || $row->status == "Unsuccessful") {
								 ?>
                                 <td>
								  <?php echo "Cancelled";?><br/>
								  <?php if($contributeDetails[$row->id]->num_rows()>0){
								     foreach($contributeDetails[$row->id]->result() as $_contributeDetails){
									   $id = $_contributeDetails->groupgift_id;
									 }
								  ?>
								  <a href="refund/<?php echo $id;?>"><?php if($this->lang->line('payment_refund') != '') { echo stripslashes($this->lang->line('payment_refund')); } else echo "Refund"; ?></a>
								  <?php }?>
								 </td>
								 <?php } elseif($row->status == "Successful") {?>
								 <td><?php echo $row->status ;?></td>
								 <?php }else{?>
								 <td></td>
								 <?php }?>
                              </tr>
                        <?php }?>
					<?php } ?>
                    
                </tbody>
            </table>
			</div>
			</div>
	<?php 
	}
	?>
	</div>
    </div>
    </dd>
    </dl>
		<?php 
		//$this->load->view('site/user/settings_sidebar');
     //$this->load->view('site/templates/side_footer_menu');
     ?>
	</div>
	<!-- / container -->
</div>
</div>
<!-- Section_start -->
<script type="text/javascript" src="js/site/<?php echo SITE_COMMON_DEFINE ?>address_helper.js"></script>
<script type="text/javascript" src="js/site/jquery.validate.js"></script>
<?php 
$this->load->view('site/templates/footer',$this->data);
?>
