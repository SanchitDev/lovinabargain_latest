<?php if($this->uri->segment(1) == 'things'){ ?>
<script	src="http://connect.facebook.net/en_US/all.js"></script>
<script type="text/javascript">
$(document).ready(function(){

	var popup1 = $.dialog('group_gifts');
	var popup2 = $.dialog('group_giftsa');
	var popup3 = $.dialog('group_giftsb');
/************ONLOAD SHOW & HIDE FUNCTION******************/	
	$('#pickafriend').show();
	$('.enter_manually_box1').hide();
	$('#entermanually').hide();
/*************************END*****************************/
/************RECIPIENT FORM SUBMIT FUNCTION***************/
$('.group-gift').click(function(event){

	event.preventDefault();
	<?php if($loginCheck == ''){ ?>
		window.location.href = baseURL+'login?next=things/<?php echo $productDetails->row()->id?>/<?php echo $productDetails->row()->seourl;?>'
	<?php }else{?>
		var url = '<?php echo base_url();?>/site/groupgift/getCountry';
		sug_url = '<?php echo base_url();?>/site/groupgift/suggest_friends';
		$.get(url,function(data){
			$("#gift_country").html(data);
		});
		$.get(sug_url,function(data){
			$("#suggested_friends ul").html(data);
		});
		popup1.open();
	<?php } ?>
});
/*************************END*****************************/
/************PERSONALIZE BACK BUTTON FUNCTION*************/
$('#btn-back').click(function(event){
	value = $('#seller-id').val();
	$('#sellers-id').val(value);
	$('#seller_id').val(value); 		
	popup1.open();
	event.preventDefault();
});
/*************************END*****************************/
/************PERSONALIZE FORM SUBMIT FUNCTION*************/
$('#btn-create').click(function(event){
	url = '<?php echo base_url();?>/site/groupgift/InsertProduct';
	value = $('#seller-id').val();
	ship_value = $('#total-val').val();
	gifttotal_value = $('#goal-total-input').val();
	name = $('#gift_name').val();
	description = $('#gift_description').val();
	notes = $('#gift_notes').val();
	if(name == ''){
		alert("Please Enter Gift Name");
		return false;
	}else if(description == ''){
		alert("Please Enter Gift Description");
		return false;
	}
	$.post(url,{'seller_id':value,'gift_name':name,'gift_description':description,'gift_notes':notes,'country':ship_value,'gift_total':gifttotal_value},function(data){
		var split_val = data.split('+');
		$('#gift_names').text(split_val[4]); $('#gift_descriptions').text(split_val[5]);$('#recipient_name').text(split_val[0]);$('#recipient_address').text(split_val[1]+','+split_val[2]+','+split_val[3]); $('#seller_id').val(value);
		var total_value = '<?php echo $productDetails->row()->sale_price;?>';
		if(total_value == ''){
			price = '<?php echo $productDetails->row()->price;?>';
		}else{
			price = '<?php echo $productDetails->row()->sale_price;?>';
		}
		$("#contb-shipping-cost").html("$"+split_val[7]+"<small><?php echo $currencyType;?></small>");
		tax = parseInt(split_val[8])/100;
		shipping_tax = parseInt(price) * tax;
		$("#contb-shipping-tax").html("$"+shipping_tax.toFixed(2)+"<small><?php echo $currencyType;?></small>");
		goal_total = parseInt(price) + parseInt(split_val[7]) + shipping_tax;
		$("#contb-goal-total").html("$"+goal_total.toFixed(2)+"<small><?php echo $currencyType;?></small>");
		$('#sellers-ids').val(split_val[6]);
		popup3.open();
		event.preventDefault();
	});
});
/*************************END*****************************/
/************RECIPIENT CANCEL FUNCTION********************/
$('#btn-cancel').click(function(event){
	popup1.close();
});
/*************************END*****************************/
/************CONTRIBUTOR DONE FUNCTION****************/
$('#btn-done').click(function(event){
	window.location.href = baseURL+'gifts/'+$('#sellers-ids').val();
});
$('.change_friend').click(function(event){
	$('.friends-lists').show();
	$('.connent_share_links').show();
	$('.search-social-friends').show();
	$('.enter_manually_box1').hide();
	popup1.open();
	return false;
});
/*************************END*****************************/
/************RECIPIENT SEARCH FUNCTION********************/
$('.search-social-friends').keyup(function(){
	var value = $(this).val();
	url= "<?php echo base_url();?>/site/groupgift/search_user";
	if(value != ''){
		$.post(url,{'search':value},function(html){
			$("#result").html(html).show();
		});
	}else{
		$("#result").hide();
	}return false;
});
jQuery("#result").live("click",function(e){ 
	var $clicked = $(e.target);
	var $name = $clicked.find('.name').html();
	if(!$name){
		$name = $clicked.html();
	}
	var decoded = $("<div/>").html($name).text();
	$('#searchid').val(decoded);
	url = "<?php echo base_url();?>/site/groupgift/get_user_result";
	$.post(url,{'user_name':decoded},function(data){
		$('.connent_share_links').hide();
		$('.search-social-friends').hide();
		$('#friends-lists').hide();
		
		$('.enter_manually_box1').show();
		var split_val = data.split('+');
		$('#get_gift_fullname').val(split_val[0]);$('#get_gift_recipient').val(split_val[1]);$('#get_gift_address1').val(split_val[3]);$('#get_gift_address2').val(split_val[4]);$('#get_gift_city').val(split_val[5]);$('#get_gift_phone').val(split_val[9]);
		if(split_val[8] == 0){
			$('#get_gift_zipcode').val('');
		}else{
			$('#get_gift_zipcode').val(split_val[8]);
		}
		$('#get_gift_zipcode').val();
		if(split_val[7] == ''){
			country = '<label for=""><?php if($this->lang->line('grougift_country') != '') { echo stripslashes($this->lang->line('grougift_country')); } else echo "Country"; ?></label><select id="get_gift_country" class="manually_input manually_select" name="country"></select>';
		}else{
			country = '<label for=""><?php if($this->lang->line('grougift_country') != '') { echo stripslashes($this->lang->line('grougift_country')); } else echo "Country"; ?></label><input type="" id="gets_gift_country" class="manually_input" name="country" value ="'+split_val[7]+'" >';
		}
		if(split_val[6] == ''){
			state = '<label for=""><?php if($this->lang->line('grougift_country') != '') { echo stripslashes($this->lang->line('grougift_state')); } else echo "State"; ?></label><input type="text" id="get_gift_text_state" class="manually_input" name="state" value=""/>';
		}else{
			state = '<label for=""><?php if($this->lang->line('grougift_country') != '') { echo stripslashes($this->lang->line('grougift_state')); } else echo "State"; ?></label><input type="text" id="get_gift_text_state" class="manually_input" name="state" value="'+split_val[6]+'"/>';
		}
		if(split_val[2] == ''){
			image = '<img width="190" height="190" src="images/users/user-thumb1.png"/>';
			user_image = '';
		}else{
			image = '<img width="190" height="190" src="<?php echo DESKTOPURL;?>images/users/'+split_val[2]+'"/>';
			user_image = split_val[2];
		}
		$("#user_image").val(user_image);
		$(".enter_manually_photos1").html(image);
		$("#append-country").html(country);
		$("#append-state").html(state);
	});
	var url = '<?php echo base_url();?>/site/groupgift/getCountry';
	$.get(url,function(data){
		$("#get_gift_country").html(data);
	});	   
});
jQuery(".show").live("click", function(e) { 
	var $clicked = $(e.target);
	if(!$clicked.hasClass("search-social-friends")){
		jQuery("#result").fadeOut(); 
	}
}); 
/************************END**********************************/
FB.init({
	appId:'<?php echo $this->config->item('facebook_app_id');?>',
	cookie:true,
	status:true,
	xfbml:true,
	oauth : true
});
$('.face_connect_links').click(function(){
	FB.login(function(response){
		if(response.authResponse){
			FB.api('/me/friends', function(response){
				if(response.data) {
				    $(".friends-lists dd ul").html('');
					$.each(response.data,function(index,friend) {
					html = '<li id="https://graph.facebook.com/'+friend.id+'/picture?width=150&height=150" onclick="friend_name(this.title,this.id);" title="'+friend.name+'"><a><div class="img-wrap"><img src="https://graph.facebook.com/'+friend.id+'/picture?width=150&height=150"/><div>'+friend.name+'</div></div></a></li>';
					$(".friends-lists dd ul").append(html); 
					});
				}else{
					alert("Error");
				}
			});
		}else{
			alert('User cancelled login or did not fully authorize.');
		}
	});
});
$('.go_connect_links').click(function(){
	var loc = location.protocol+'//'+location.host;
	var param = {'location':loc};
	var popup = window.open('about:blank', '_blank', 'height=550,width=900,left=250,top=100,resizable=yes', true);
	var $btn = $(this);
	$.post(
		baseURL+'site/user/get_friend_list',
		param, 
		function(json){
			if (json.status_code==1) {
				popup.location.href = json.url;	
			}
			else if (json.status_code==0) {
				alert(json.message);
			}  
		},
		'json'
	);
});
});
function friend_name(title,img){
	$('.connent_share_links').hide();
	$('.search-social-friends').hide();
	$('.friends-lists').hide();
	$('.enter_manually_box1').show();
	$('#get_gift_fullname').val(title);
	$('#get_gift_recipient').val(title);
	$('#fb_image').val(img);
	country = '<label for=""><?php if($this->lang->line('grougift_country') != '') { echo stripslashes($this->lang->line('grougift_country')); } else echo "Country"; ?></label><select id="get_gift_country" class="manually_input manually_select" name="country"></select>';
	state = '<label for=""><?php if($this->lang->line('grougift_country') != '') { echo stripslashes($this->lang->line('grougift_state')); } else echo "State"; ?></label><input type="text" id="get_gift_text_state" class="manually_input" name="state" value=""/>';
	if(img ==''){
	   images = '<img src="<?php echo DESKTOPURL;?>images/users/default_user.jpg" style="width:150px; height:150px;"/>';
	}else{
	   images = '<img src="'+img+'"/>';
	}
	$('.enter_manually_photos1').html(images);
	$("#append-country").html(country);
	$("#append-state").html(state);
	var url = '<?php echo base_url();?>/site/groupgift/getCountry';
	$.get(url,function(data){
		$("#get_gift_country").html(data);
	});
}
function pickfriend(){
	$('#pickafriend').show();
	$('.connent_share_links').show();
	$('.search-social-friends').show();
	$('#entermanually').hide();
	$('.enter_manually_box1').hide();
	$("#dep4").attr("class","depth4 current");
	$("#dep5").attr("class","depth5");
	$("#dep4 a").attr("class","current");
	$("#dep5 a").attr("class","");	
}
function entermanually(){
	$('#pickafriend').hide();
	$('#entermanually').show();
	$('.enter_manually_photos img').hide();
	$("#dep5").attr("class","depth5 current");
	$("#dep4").attr("class","depth4");
	$("#dep5 a").attr("class","current");
	$("#dep4 a").attr("class","");	
}
function sug_fri_li(id){
	var img = $("#img-"+id).attr('src');
	name = $("#user-"+id).text();
	$("#gift_recipient").val(name);
	$("#gift_fullname").val(name);
	$("#img_prev").attr("src", img).width(119).height(115);
	$('#img_prev').css('display','block');
	$(".btn-upload").text('');
	$(".btn-upload span").hide();
	$("#recipient_image").attr('value',img);
	
}
function readURL(input){
	if(input.files && input.files[0]){
	var reader = new FileReader();
	reader.onload = function(e){
		$('#img_prev').attr('src',e.target.result).width(119).height(115);
		$('#img_prev').css('display','block');
		$(".btn-upload").text('');
	    $(".btn-upload span").hide();
	};
		reader.readAsDataURL(input.files[0]);
	}
}
</script>
<script>
$(document).ready(function(){
    var popup2 = $.dialog('group_giftsa');
	function getDoc(frame){
		var doc = null; // IE8 cascading access check
		try{
			if(frame.contentWindow){
			doc = frame.contentWindow.document;
			}
		}catch(err){
		}
		if(doc){ // successful getting content
		return doc;
		}
		try{ // simply checking may throw in ie8 under ssl or mismatched protocol
			doc = frame.contentDocument ? frame.contentDocument : frame.document;
		}catch(err){// last attempt
			doc = frame.document;
		}return doc;
	}
	$("#form1").submit(function(e){
		var formObj = $(this);
		var formURL = $(this).attr("action");
		if($('#gift_recipient').val() == ''){
		   alert("Please Enter Recipient Name or Email Address");
		   return false;
		}else if($('#gift_fullname').val() == ''){
		   alert("Please Enter Fullname");
		   return false;
		}else if($('#gift_address1').val() == ''){
		   alert("Please Enter Address");
		   return false;
		}else if($('#gift_text_state').val() == ''){
		   alert("Please Enter State");
		   return false;
		}else if($('#gift_city').val() == ''){
		   alert("Please Enter City");
		   return false;
		}else if($('#gift_zipcode').val() == ''){
		   alert("Please Enter Zipcode");
		   return false;
		}
		if(window.FormData !== undefined)  // for HTML5 browsers
		{
			var formData = new FormData(this);
			$.ajax({
				url: formURL,
				type: 'POST',
				data:  formData,
					mimeType:"multipart/form-data",
					contentType: false,
					cache: false,
					processData:false,
				success: function(data, textStatus, jqXHR)
				{
					var val_split = data.split('+');
					var total_value = '<?php echo $productDetails->row()->sale_price;?>';
					if(total_value == ''){
						price = '<?php echo $productDetails->row()->price;?>';
					}else{
						price = '<?php echo $productDetails->row()->sale_price;?>';
					}
					if($('#seller-id').val()!=''){
						shipping_cost = $("#shipping-cost").html("$"+val_split[7]+"<small><?php echo $currencyType;?></small>");
						tax = parseInt(val_split[8])/100;
						shipping_tax = parseInt(price) * tax;
						$("#shipping-tax").html("$"+shipping_tax.toFixed(2)+"<small><?php echo $currencyType;?></small>");
						goal_total = parseInt(price) + parseInt(val_split[7]) + shipping_tax;
						$("#goal-total").html("$"+goal_total.toFixed(2)+"<small><?php echo $currencyType;?></small>");
						$("#goal-total-input").val(goal_total.toFixed(2));
						$("#total-val").val(val_split[3]);
						$('#seller-id').val(val_split[6]);
					}else{
						shipping_cost = $("#shipping-cost").html("$"+val_split[0]+"<small><?php echo $currencyType;?></small>");
						tax = parseInt(val_split[1])/100;
						shipping_tax = parseInt(price) * tax;
						$("#shipping-tax").html("$"+shipping_tax.toFixed(2)+"<small><?php echo $currencyType;?></small>");
						goal_total = parseInt(price) + parseInt(val_split[0]) + shipping_tax;
						$("#goal-total").html("$"+goal_total.toFixed(2)+"<small><?php echo $currencyType;?></small>");
						$("#goal-total-input").val(goal_total.toFixed(2));
						$("#total-val").val(val_split[2]);
						$('#seller-id').val(val_split[3]);	
					}
					popup2.open();
					event.preventDefault();
				} 	        
			});
			e.preventDefault();
			e.unbind();
		}
		else { //for olden browsers
			//generate a random id
			var  iframeId = 'unique' + (new Date().getTime());
			//create an empty iframe
			var iframe = $('<iframe src="javascript:false;" name="'+iframeId+'" />');
			//hide it
			iframe.hide();
			//set form target to iframe
			formObj.attr('target',iframeId);
			//Add iframe to body
			iframe.appendTo('body');
			iframe.load(function(e)
			{
				var doc = getDoc(iframe[0]);
				var docRoot = doc.body ? doc.body : doc.documentElement;
				var data = docRoot.innerHTML;
				var val_split = data.split('+');
				var total_value = '<?php echo $productDetails->row()->sale_price;?>';
				if(total_value == ''){
					price = '<?php echo $productDetails->row()->price;?>';
				}else{
					price = '<?php echo $productDetails->row()->sale_price;?>';
				}
				if($('#seller-id').val()!=''){
					shipping_cost = $("#shipping-cost").html("$"+val_split[7]+"<small><?php echo $currencyType;?></small>");
					tax = parseInt(val_split[8])/100;
					shipping_tax = parseInt(price) * tax;
					$("#shipping-tax").html("$"+shipping_tax.toFixed(2)+"<small><?php echo $currencyType;?></small>");
					goal_total = parseInt(price) + parseInt(val_split[7]) + shipping_tax;
					$("#goal-total").html("$"+goal_total.toFixed(2)+"<small><?php echo $currencyType;?></small>");
					$("#goal-total-input").val(goal_total.toFixed(2));
					$("#total-val").val(val_split[3]);
					$('#seller-id').val(val_split[6]);
				}else{
					shipping_cost = $("#shipping-cost").html("$"+val_split[0]+"<small><?php echo $currencyType;?></small>");
					tax = parseInt(val_split[1])/100;
					shipping_tax = parseInt(price) * tax;
					$("#shipping-tax").html("$"+shipping_tax.toFixed(2)+"<small><?php echo $currencyType;?></small>");
					goal_total = parseInt(price) + parseInt(val_split[0]) + shipping_tax;
					$("#goal-total").html("$"+goal_total.toFixed(2)+"<small><?php echo $currencyType;?></small>");
					$("#goal-total-input").val(goal_total.toFixed(2));
					$("#total-val").val(val_split[2]);
					$('#seller-id').val(val_split[3]);	
				}
				popup2.open();
				event.preventDefault();
			});
		}
	});
	$("#form2").submit(function(e){
		var formObj = $(this);
		var formURL = $(this).attr("action");
		if($('#get_gift_recipient').val() == ''){
		   alert("Please Enter Recipient Name or Email Address");
		   return false;
		}else if($('#get_gift_fullname').val() == ''){
		   alert("Please Enter Fullname");
		   return false;
		}else if($('#get_gift_address1').val() == ''){
		   alert("Please Enter Address");
		   return false;
		}else if($('#get_gift_text_state').val() == ''){
		   alert("Please Enter State");
		   return false;
		}else if($('#get_gift_city').val() == ''){
		   alert("Please Enter City");
		   return false;
		}else if($('#get_gift_zipcode').val() == ''){
		   alert("Please Enter Zipcode");
		   return false;
		} 
		if(window.FormData !== undefined)  // for HTML5 browsers
		{
			var formData = new FormData(this);
			$.ajax({
				url: formURL,
				type: 'POST',
				data:  formData,
					mimeType:"multipart/form-data",
					contentType: false,
					cache: false,
					processData:false,
				success: function(data, textStatus, jqXHR)
				{
					var val_split = data.split('+');
					var total_value = '<?php echo $productDetails->row()->sale_price;?>';
					if(total_value == ''){
						price = '<?php echo $productDetails->row()->price;?>';
					}else{
						price = '<?php echo $productDetails->row()->sale_price;?>';
					}
					if($('#seller-id').val()!=''){
						shipping_cost = $("#shipping-cost").html("$"+val_split[7]+"<small><?php echo $currencyType;?></small>");
						tax = parseInt(val_split[8])/100;
						shipping_tax = parseInt(price) * tax;
						$("#shipping-tax").html("$"+shipping_tax.toFixed(2)+"<small><?php echo $currencyType;?></small>");
						goal_total = parseInt(price) + parseInt(val_split[7]) + shipping_tax;
						$("#goal-total").html("$"+goal_total.toFixed(2)+"<small><?php echo $currencyType;?></small>");
						$("#goal-total-input").val(goal_total.toFixed(2));
						$("#total-val").val(val_split[3]);
						$('#seller-id').val(val_split[6]);
					}else{
						shipping_cost = $("#shipping-cost").html("$"+val_split[0]+"<small><?php echo $currencyType;?></small>");
						tax = parseInt(val_split[1])/100;
						shipping_tax = parseInt(price) * tax;
						$("#shipping-tax").html("$"+shipping_tax.toFixed(2)+"<small><?php echo $currencyType;?></small>");
						goal_total = parseInt(price) + parseInt(val_split[0]) + shipping_tax;
						$("#goal-total").html("$"+goal_total.toFixed(2)+"<small><?php echo $currencyType;?></small>");
						$("#goal-total-input").val(goal_total.toFixed(2));
						$("#total-val").val(val_split[2]);
						$('#seller-id').val(val_split[3]);
					}
					popup2.open();
					event.preventDefault();
				} 	        
			});
			e.preventDefault();
			e.unbind();
		}
		else { //for olden browsers
			//generate a random id
			var  iframeId = 'unique' + (new Date().getTime());
			//create an empty iframe
			var iframe = $('<iframe src="javascript:false;" name="'+iframeId+'" />');
			//hide it
			iframe.hide();
			//set form target to iframe
			formObj.attr('target',iframeId);
			//Add iframe to body
			iframe.appendTo('body');
			iframe.load(function(e)
			{
				var doc = getDoc(iframe[0]);
				var docRoot = doc.body ? doc.body : doc.documentElement;
				var data = docRoot.innerHTML;
				var val_split = data.split('+');
					var total_value = '<?php echo $productDetails->row()->sale_price;?>';
					if(total_value == ''){
						price = '<?php echo $productDetails->row()->price;?>';
					}else{
						price = '<?php echo $productDetails->row()->sale_price;?>';
					}
					if($('#seller-id').val()!=''){
						shipping_cost = $("#shipping-cost").html("$"+val_split[7]+"<small><?php echo $currencyType;?></small>");
						tax = parseInt(val_split[8])/100;
						shipping_tax = parseInt(price) * tax;
						$("#shipping-tax").html("$"+shipping_tax.toFixed(2)+"<small><?php echo $currencyType;?></small>");
						goal_total = parseInt(price) + parseInt(val_split[7]) + shipping_tax;
						$("#goal-total").html("$"+goal_total.toFixed(2)+"<small><?php echo $currencyType;?></small>");
						$("#goal-total-input").val(goal_total.toFixed(2));
						$("#total-val").val(val_split[3]);
						$('#seller-id').val(val_split[6]);
					}else{
						shipping_cost = $("#shipping-cost").html("$"+val_split[0]+"<small><?php echo $currencyType;?></small>");
						tax = parseInt(val_split[1])/100;
						shipping_tax = parseInt(price) * tax;
						$("#shipping-tax").html("$"+shipping_tax.toFixed(2)+"<small><?php echo $currencyType;?></small>");
						goal_total = parseInt(price) + parseInt(val_split[0]) + shipping_tax;
						$("#goal-total").html("$"+goal_total.toFixed(2)+"<small><?php echo $currencyType;?></small>");
						$("#goal-total-input").val(goal_total.toFixed(2));
						$("#total-val").val(val_split[2]);
						$('#seller-id').val(val_split[3]);
					}
				popup2.open();
				event.preventDefault();
			});
		}
	});
	$("#btn-continue").click(function(){	
		$("#form1").submit();
	});
	$("#btn-existuser").click(function(){	
		$("#form2").submit();
	});
});
</script>
<div class="popup group_gifts ly-title" style="display:none;">
	<p class="ltit"><?php if($this->lang->line('create_a_group_gift') != '') { echo stripslashes($this->lang->line('create_a_group_gift')); } else echo "Create a Group Gift"; ?></p>
	<div class="ltxt">
		<ol class="gift-group-create" style="position:relative;">
		   <li class="depth1 current" id="dep1"><a onclick="javascript:recipient();" class="current"><?php if($this->lang->line('group_gift_recipient') != '') { echo stripslashes($this->lang->line('group_gift_recipient')); } else echo "Choose Recipient"; ?></a><span></span></li>
		   <li class="depth2" id="dep2"><a onclick="javascript:personalize();"><?php if($this->lang->line('group_gift_personalize') != '') { echo stripslashes($this->lang->line('group_gift_personalize')); } else echo "Personalize"; ?></a><span></span></li>
		   <li class="depth3" id="dep3"><a onclick="javascript:contributions();"><?php if($this->lang->line('ask_for_contributions') != '') { echo stripslashes($this->lang->line('ask_for_contributions')); } else echo "Ask for Contributions"; ?></a></li>
		</ol>
		<div style="float:left; margin-top:25px; border-bottom:1px solid #ECEEF4; width:100%;">
		   <ol class="gift-group-tab" style="position:relative; border-bottom-style:none;">
		      <li style="background:none;" class="depth4 current" id="dep4"><a onclick="javascript:pickfriend();" class="current"><?php if($this->lang->line('pick_a_friend') != '') { echo stripslashes($this->lang->line('pick_a_friend')); } else echo "Pick a Friend"; ?></a></li>
		      <li style="background:none;" class="depth5" id="dep5"><a onclick="javascript:entermanually();"><?php if($this->lang->line('enter_manually') != '') { echo stripslashes($this->lang->line('enter_manually')); } else echo "Enter Manually"; ?></a></li>
		   </ol>
	  </div>
	  <div class="pickafriend" id="pickafriend">
	      <input class="search-social-friends" id="searchid" type="text" placeholder="Search friends" style="width:100%;margin-top:5px;float:left;">
		  <div id="result"></div>
		  <ul class="connent_share_links">
      	     <li>
        	     <a class="go_connect_links current">
            	    <span class="icon"></span>
                    <strong><?php if($this->lang->line('connect_to_google') != '') { echo stripslashes($this->lang->line('connect_to_google')); } else echo "Connect to Google+"; ?></strong>
                </a>
             </li>
             <li>
        	    <a class="face_connect_links" id="facebook">
            	   <span class="icon"></span>
                   <strong><?php if($this->lang->line('facebbok_friends') != '') { echo stripslashes($this->lang->line('facebbok_friends')); } else echo "Facebook friends"; ?></strong>
                </a>
            </li>
         </ul>
		 <div class="friends-lists" id="friends-lists"><dd><ul></ul></dd></div>
		 <form action="site/groupgift/InsertProduct" method="post" id="form2" enctype="multipart/form-data" style="padding:0px!important;">
		  <div class="enter_manually_box1">
		            <!--<input type="hidden" name="image" value="" id="user_image"/>-->
			        <input type="hidden" name="product_id" value="<?php echo $this->uri->segment(2);?>">
					<input type="hidden" name="long_url" value="<?php echo base_url();?>gifts/">
               		<ul>
                        <li>
						    <input type="hidden" name="seller_id" id ="seller_id" value=""/>
                        	<label for=""><?php if($this->lang->line('grougift_recipient') != '') { echo stripslashes($this->lang->line('grougift_recipient')); } else echo "Recipient"; ?></label>
                        	<input type="text" value="" class="manually_input" name="recipient_name" id="get_gift_recipient" placeholder="Enter a username or an email address">
                        </li>
                        <li>
                        	<label for=""><?php if($this->lang->line('grougift_fullname') != '') { echo stripslashes($this->lang->line('grougift_fullname')); } else echo "Full name"; ?></label>
                        	 <input type="text" value="" class="manually_input" name="recipient_fullname" id="get_gift_fullname" placeholder="">
                        </li>
                         <li>
                        	<label for=""><?php if($this->lang->line('grougift_address1') != '') { echo stripslashes($this->lang->line('grougift_address1')); } else echo "Address1"; ?></label>
                        	<input type="text" value="" class="manually_input" name="address1" id ="get_gift_address1" placeholder="Address line 1">
                        </li>
                        <li>
                        	<label for="">&nbsp;</label>
                        	<input type="text" value="" id="get_gift_address2" class="manually_input" name="address2" placeholder="Address line 2">
                        </li>
                        <li id="append-country">
                        	
                        </li>
                        <li id="append-state">
                        	 
                        </li>
                         <li>
                        	 <label for=""><?php if($this->lang->line('grougift_city') != '') { echo stripslashes($this->lang->line('grougift_city')); } else echo "City"; ?></label>
                        	 <input type="text" value="" id="get_gift_city" name="city" class="manually_input manually_city" placeholder="e.g Newyork"><input type="text" value="" class="manually_input manually_zip" id="get_gift_zipcode" name="zip_code" placeholder="ZIP Code">
                        </li>
                          <li>
                        	 <label for=""><?php if($this->lang->line('grougift_telephone') != '') { echo stripslashes($this->lang->line('grougift_telephone')); } else echo "Telephone"; ?></label>
                        	 <input type="text" id="get_gift_phone" value="" class="manually_input" name="telephone" placeholder="10 digits, no dashes">
                        </li>
						 <input type="hidden" name="fb_image" id="fb_image" value="">
                    </ul>
                    <div class="enter_manually_photos1">
                    </div><br>
					<div style="float:left; padding:20px 0px 0px 80px;"><button class="change_friend"><?php if($this->lang->line('change_friend') != '') { echo stripslashes($this->lang->line('change_friend')); } else echo "Change Friend"; ?></button></div>
			   </div>
	      <div class="btn-area">
              <div class="howit_works"><i></i><a href="<?php echo base_url();?>pages/group-gift"><?php if($this->lang->line('how_it_works') != '') { echo stripslashes($this->lang->line('how_it_works')); } else echo "How it works"; ?></a></div>
		      <button class="btn-back" id="btn-cancel" type="button"><?php if($this->lang->line('header_cancel') != '') { echo stripslashes($this->lang->line('header_cancel')); } else echo "Cancel"; ?></button>
		      <button class="btn-done" id= "btn-existuser" type="button"><?php if($this->lang->line('continue') != '') { echo stripslashes($this->lang->line('continue')); } else echo "Continue"; ?></button>
	      </div>
		</form>
	  </div>
	  <form  name="form1" action="site/groupgift/InsertProduct" method="post" id="form1" enctype="multipart/form-data" style="padding:0px!important;">
	  <div class="entermanually" id ="entermanually" style="float:left; width:100%">
	       <div class="enter_manually">
		       <div class="enter_manually_box">
			        <input type="hidden" name="product_id" value="<?php echo $this->uri->segment(2);?>">
					<input type="hidden" name="long_url" value="<?php echo base_url();?>gifts/">
               		<ul>
                        <li>
						    <input type="hidden" name="seller_id" id ="sellers-id" value=""/>
                        	<label for=""><?php if($this->lang->line('grougift_recipient') != '') { echo stripslashes($this->lang->line('grougift_recipient')); } else echo "Recipient"; ?></label>
                        	<input type="text" value="" class="manually_input" name="recipient_name" id="gift_recipient" placeholder="Enter a username or an email address">
                        </li>
                        <li>
                        	<label for=""><?php if($this->lang->line('grougift_fullname') != '') { echo stripslashes($this->lang->line('grougift_fullname')); } else echo "Full name"; ?></label>
                        	 <input type="text" value="" class="manually_input" name="recipient_fullname" id="gift_fullname" placeholder="">
                        </li>
                         <li>
                        	<label for=""><?php if($this->lang->line('grougift_address1') != '') { echo stripslashes($this->lang->line('grougift_address1')); } else echo "Address1"; ?></label>
                        	<input type="text" value="" class="manually_input" name="address1" id ="gift_address1" placeholder="Address line 1">
                        </li>
                        <li>
                        	<label for="">&nbsp;</label>
                        	<input type="text" value="" class="manually_input" name="address2" placeholder="Address line 2">
                        </li>
                        <li>
                        	<label for=""><?php if($this->lang->line('grougift_country') != '') { echo stripslashes($this->lang->line('grougift_country')); } else echo "Country"; ?></label>
                        	 <select id="gift_country" class="manually_input manually_select" name="country">
				                 
				             </select>
                        </li>
                        <li>
                        	 <label for=""><?php if($this->lang->line('grougift_state') != '') { echo stripslashes($this->lang->line('grougift_state')); } else echo "State"; ?></label>
                             <input type="text" id="gift_text_state" class="manually_input" name="state" value=""/>
                        </li>
                         <li>
                        	 <label for=""><?php if($this->lang->line('grougift_city') != '') { echo stripslashes($this->lang->line('grougift_city')); } else echo "City"; ?></label>
                        	 <input type="text" value="" id="gift_city" name="city" class="manually_input manually_city" placeholder="e.g Newyork"><input type="text" value="" class="manually_input manually_zip" id="gift_zipcode" name="zip_code" placeholder="ZIP Code">
                        </li>
                          <li>
                        	 <label for=""><?php if($this->lang->line('grougift_telephone') != '') { echo stripslashes($this->lang->line('grougift_telephone')); } else echo "Telephone"; ?></label>
                        	 <input type="text" value="" class="manually_input" name="telephone" placeholder="10 digits, no dashes">
                        </li>
                    </ul>
                    <div class="enter_manually_photos">
                    
                    	<input type="file" class="hidden" name="recipient_image" id="recipient_image"onchange="readURL(this);"/>
						<img id="img_prev" src="#" alt="your image" style="margin-top:-120px;"/>
						<button class="btn-upload" type="button"><span class="icon"></span><?php if($this->lang->line('grougift_upload') != '') { echo stripslashes($this->lang->line('grougift_upload')); } else echo "Upload Photo"; ?></button>
                    </div>
			   </div> 
		   </div>
		   <div class="suggested_box">
		         <h4><?php if($this->lang->line('sugessted_friends') != '') { echo stripslashes($this->lang->line('sugessted_friends')); } else echo "Suggested friends"; ?></h4>
				 <div id="suggested_friends">
				   <ul>
				   </ul>
				 </div>
		   </div>
	  
	    <div class="btn-area">
    <div class="howit_works"><i></i><a href="<?php echo base_url();?>pages/group-gift"><?php if($this->lang->line('how_it_works') != '') { echo stripslashes($this->lang->line('how_it_works')); } else echo "How it works"; ?></a></div>
		<button class="btn-back" id="btn-cancel" type="button"><?php if($this->lang->line('header_cancel') != '') { echo stripslashes($this->lang->line('header_cancel')); } else echo "Cancel"; ?></button>
		<button class="btn-done" id= "btn-continue" type="button"><?php if($this->lang->line('continue') != '') { echo stripslashes($this->lang->line('continue')); } else echo "Continue"; ?></button>
	</div></div>
	  </form>
	</div>
	
	<button title="Close" class="ly-close"><i class="ic-del-black"></i></button>
</div>
<?php } ?>