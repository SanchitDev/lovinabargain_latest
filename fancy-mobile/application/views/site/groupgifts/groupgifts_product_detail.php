<?php $this->load->view('site/templates/header'); ?>
<link rel="stylesheet" href="css/site/my-account.css" type="text/css" media="all"/>
<script type="text/javascript" src="js/site/SpryTabbedPanels.js"></script>
 <!-- Section_start -->
<div class="lang-en wider no-subnav thing signed-out winOS">
<?php if($flash_data != '') { ?>
		<div class="errorContainer" id="<?php echo $flash_data_type;?>">
			<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
			<p><span><?php echo $flash_data;?></span></p>
		</div>
		<?php } ?>
<?php if($gift_ProductDetails->num_rows() ==1){
      $img = 'dummyProductImage.jpg';
		$imgArr = explode(',', $gift_ProductDetails->row()->image);
		if (count($imgArr)>0){
			foreach ($imgArr as $imgRow){
				if ($imgRow != ''){
					$img = $pimg = $imgRow;
					break;
				}
			}
		}
		  $user_img = 'user-thumb1.png';
		$user_imgArr = explode(',', $gift_ProductDetails->row()->thumbnail);
		if (count($user_imgArr)>0){
			foreach ($user_imgArr as $user_imgRow){
				if ($user_imgRow != ''){
					$user_img = $user_pimg = $user_imgRow;
					break;
				}
			}
		}
		$recipient_img = 'user-thumb1.png';
		$recp_imgArr = explode(',', $gift_ProductDetails->row()->recipient_image);
		if (count($recp_imgArr)>0){
			foreach ($recp_imgArr as $recp_imgRow){
				if ($recp_imgRow != ''){
					$recipient_img = $recp_pimg = $recp_imgRow;
					break;
				}
			}
		}
?>
<div id="container-wrapper">
	<div class="container ">
	<div class="wrapper-content right-sidebar" style="background:none; box-shadow:none;">
		<div id="content" style="padding:0px; background:none;width: 680px;">
            <div class="detail_leftbar3">
			<div class="wrapper figure-section">
				<div class="figure-product figure-640">
					<figure>
						<span class="wrapper-fig-image">
							<span class="fig-image">
								<img src="<?php echo DESKTOPURL;?>images/product/<?php echo $img;?>" alt="<?php echo $gift_ProductDetails->row()->product_name;?>" height="640" width="630">
							</span>
						</span>
					</figure>
                    <div class="contributions_share_link">
					<a target="_blank" href="http://plus.google.com/share?url=<?php echo base_url().'t/'.$gift_ProductDetails->row()->short_url.'?ref='.$gift_ProductDetails->row()->user_name;?>" class="ask_go_share"><span class="icon"></span> <?php if($this->lang->line('ask_for_contributions') != '') { echo stripslashes($this->lang->line('ask_for_contributions')); } else echo "Ask for contributions"; ?></a>
					
					<a target="_blank" href="http://www.facebook.com/sharer.php?u=<?php echo base_url().'t/'.$gift_ProductDetails->row()->short_url.'?ref='.$gift_ProductDetails->row()->user_name;?>" class="share_links"><span class="icon"></span><?php if($this->lang->line('header_share') != '') { echo stripslashes($this->lang->line('header_share')); } else echo "Share"; ?></a>
					<?php
					 $url = urlencode(base_url().'t/'.$gift_ProductDetails->row()->short_url);
					?>
					<a target="_blank" href="http://twitter.com/share?text=<?php echo $gift_ProductDetails->row()->product_name;?>&url=<?php echo $url.'?ref='.$gift_ProductDetails->row()->user_name;?>" class="tweet_links"><span class="icon"></span><?php if($this->lang->line('header_tweet') != '') { echo stripslashes($this->lang->line('header_tweet')); } else echo "Tweet"; ?></a>
					
                    <div class="link"><?php if($this->lang->line('header_share') != '') { echo stripslashes($this->lang->line('header_share')); } else echo "Share"; ?><input type="text" value="<?php echo base_url().'t/'.$gift_ProductDetails->row()->short_url?>" class="text share_links"  ></div>
                    </div>
				</div>
				
			</div>
            <?php $date = $gift_ProductDetails->row()->created;
			      $created_date = date('M d, Y', strtotime($date));
			?>
            <div class="gifts-summary">
			   
                                <h2><?php echo $gift_ProductDetails->row()->gift_name;?>
                                <small><?php if($this->lang->line('header_small_for') != '') { echo stripslashes($this->lang->line('header_small_for')); } else echo "for"; ?><?php echo $gift_ProductDetails->row()->recipient_name;?></small></h2>
                                <p class="message"><?php echo $gift_ProductDetails->row()->gift_description;?></p>
                                <div class="user">
                                    <a href=""><img width="33" height="33" src="<?php echo DESKTOPURL;?>images/users/<?php //echo $user_img;?>" alt="" class="photo"></a>
                                    <b class="username"><?php echo $gift_ProductDetails->row()->user_name?></b>
                                    <span class="date"><?php echo $created_date;?></span>
                                </div>
                            </div>
       
		</div>
        <div class="detail_leftbar4">
            <h3 class="detail_link_list"><?php echo $group_count->num_rows();?><?php if($this->lang->line('comments') != '') { echo stripslashes($this->lang->line('comments')); } else echo "Comments"; ?></h3>
                <div class="wrapper">
				     <section class="comments comments-list comments-list-new" style="padding-bottom: 0;">
					    <ol user_id="">
                            <li class="loading"><span><?php if($this->lang->line('display_loading') != '') { echo stripslashes($this->lang->line('display_loading')); } else echo "Loading"; ?>...</span></li>
					    </ol>
					    <ol user_id="">
						    <?php 
						        if ($giftproductComment->num_rows() > 0){
							        foreach ($giftproductComment->result() as $cmtrow){
								       if ($cmtrow->status == 'Active'){ ?>
						                  <li class="comment">
							                   <a class="milestone" id="comment-1866615"></a>
							                   <span class="vcard"><a href="<?php echo DESKTOPURL;?>user/<?php echo $cmtrow->user_name;?>" class="url"><img src="images/users/<?php if($cmtrow->thumbnail!=''){ echo $cmtrow->thumbnail;}else{echo 'user-thumb.png';}?>" alt="" class="photo"><span class="fn nickname"><?php echo ucfirst($cmtrow->user_name);?></span></a></span>
							                   <p class="c-text"><?php echo $cmtrow->comments;?></p>
						                  </li>
						          <?php }else { if ($loginCheck == $gift_ProductDetails->row()->user_id){ ?>
						                  <li class="comment">
							                  <a class="milestone" id="comment-1866615"></a>
							                  <span class="vcard"><a href="<?php echo DESKTOPURL;?>user/<?php echo $cmtrow->user_name;?>" class="url"><img src="images/users/<?php if($cmtrow->thumbnail!=''){ echo $cmtrow->thumbnail;}else{echo 'user-thumb.png';}?>" alt="" class="photo"><span class="fn nickname"><?php echo ucfirst($cmtrow->user_name);?></span></a></span>
							                 <p class="c-text"><?php echo $cmtrow->comments;?></p>
							                 <p style="float:left;width:100%;text-align:left;">
								                   <a style="font-size: 11px; color: #188A0E;" onclick="javascript:approvegrogiftCmt(this);" data-uid="<?php echo $cmtrow->CUID;?>" data-tid="<?php echo $gift_ProductDetails->row()->seller_product_id;?>" data-cid="<?php echo $cmtrow->id;?>"><?php if($this->lang->line('approve') != '') { echo stripslashes($this->lang->line('approve')); } else echo "Approve"; ?></a>
								                   <a style="font-size: 11px; color: #f33;" onclick="javascript:deletegrogiftCmt(this);" data-tid="<?php echo $gift_ProductDetails->row()->seller_product_id;?>" data-cid="<?php echo $cmtrow->id;?>"><?php if($this->lang->line('shipping_delete') != '') { echo stripslashes($this->lang->line('shipping_delete')); } else echo "Delete"; ?></a>
							                </p>	
						                 </li>
						        <?php }else { if ($loginCheck == $cmtrow->CUID){ ?>
						                 <li class="comment">
							                 <a class="milestone" id="comment-1866615"></a>
							                 <span class="vcard"><a href="<?php echo base_url();?>user/<?php echo $cmtrow->user_name;?>" class="url"><img src="<?php echo DESKTOPURL;?>images/users/<?php if($cmtrow->thumbnail!=''){ echo $cmtrow->thumbnail;}else{echo 'user-thumb.png';}?>" alt="" class="photo"><span class="fn nickname"><?php echo ucfirst($cmtrow->user_name);?></span></a></span>
							                 <p class="c-text"><?php echo $cmtrow->comments;?></p>
							                 <p style="float:left;width:100%;text-align:left;font-size: 11px; color: #188A0E;">
								                  <?php if($this->lang->line('wait_for_approve') != '') { echo stripslashes($this->lang->line('wait_for_approve')); } else echo "Waiting for approval"; ?>
								                  <a style="font-size: 11px; color: #f33;margin-left:10px" onclick="javascript:deletegrogiftCmt(this);" data-tid="<?php echo $gift_ProductDetails->row()->seller_product_id;?>" data-cid="<?php echo $cmtrow->id;?>"><?php if($this->lang->line('shipping_delete') != '') { echo stripslashes($this->lang->line('shipping_delete')); } else echo "Delete"; ?></a>
							                 </p>	  
						                 </li>
						      <?php }}}}}?>
					   </ol>
				</section>
                       <div class="comments-list-new">
                            <form action="#" class="comment-form">
                                <label class="hidden"><?php if($this->lang->line('header_comment') != '') { echo stripslashes($this->lang->line('header_comment')); } else echo "Comment"; ?>:</label>
								<div id="flash"></div>
                                <span class="vcard"><a href="<?php echo base_url()?>/user/<?php echo $gift_ProductDetails->row()->user_name?>" class="url"><img src="<?php echo DESKTOPURL;?>images/users/<?php echo $user_img;?>" alt="<?php echo $gift_ProductDetails->row()->user_name?>" class="photo"></a></span>
                                <textarea id="comments-send" class="text detail_input" placeholder="Write a comment......" name="comments"></textarea>
								 <button class="submit button" id= "btn-comment" type="button"><?php if($this->lang->line('gift_post') != '') { echo stripslashes($this->lang->line('gift_post')); } else echo "Post"; ?></button>
                            </form>
                        </div>
                    </div>  
                </div>
                
              <div class="detail_leftbar4">
              <?php if($contribute_details >0){
				          $contribute_amount = 0;
						  $people = $contribute_details->num_rows();
						  foreach($contribute_details->result() as $_contributedetails){
						         $contribute_amount += $_contributedetails->amount;
								 
								 if($contribute_amount!=''){
								    $amount = $contribute_amount; 
								 }else{
								    $amount = "0";
								 }
                          }							  
						}else{
						  $people =0; 
                        }						
			?>
              <h3 class="detail_link_list"><?php echo $people; ?> <?php if($this->lang->line('people_contributed') != '') { echo stripslashes($this->lang->line('people_contributed')); } else echo "People Contributed"; ?></h3>
					<div class="wrapper find-people">
				
				<ul class="select-list">
				    
				</ul>
			</div>
                </div>
        
        </div>
		<!-- / content -->
		<aside id="sidebar" style="padding:0px; width: 255px;">
        
        	<div class="detail_sidebar_list">
            <h3 class="detail_link_list"><?php if($this->lang->line('grougift_recipient') != '') { echo stripslashes($this->lang->line('grougift_recipient')); } else echo "Recipient"; ?> 
			<?php if($gift_ProductDetails->row()->status == 'Cancelled' || $gift_ProductDetails->row()->status == 'Unsuccessful' || $gift_ProductDetails->row()->status == 'Cancelled Admin'){?>
			<?php } else{?>
			    <a class="edit_btn_list" title="Edit group gift details" href="#edit_group_gift"></a> </h3>
			<?php } ?>
               <div class="recipient_box">
				<div class="reciver_text">
				
					<div class="reciver_icon"><img src="<?php echo DESKTOPURL;?>images/groupgifts/<?php echo $recipient_img;?>"/></div>
					<small><?php if($this->lang->line('group_gift_to') != '') { echo stripslashes($this->lang->line('group_gift_to')); } else echo "Gift to"; ?></small>
					<b class="username"><?php echo $gift_ProductDetails->row()->recipient_name;?></b>
					<?php echo $gift_ProductDetails->row()->address1.$gift_ProductDetails->row()->address2;?>, <?php echo $gift_ProductDetails->row()->city;?><br><?php echo $gift_ProductDetails->row()->state;?>,<?php echo $gift_ProductDetails->row()->country;?>
				</div>
				<!--div class="gate">
					<span class="icon gg"></span> <a href="#">Jonah Wellington</a>
				</div-->
				
				<div class="notify">
					<?php if($this->lang->line('sucessfully_recipient_refund') != '') { echo stripslashes($this->lang->line('sucessfully_recipient_refund')); } else echo "This gift will be shipped to the recipient if successfully funded."; ?>
				</div>
                
                </div>
				
            </div>
            <div class="detail_sidebar_list">
            <h3 class="detail_link_list"><?php if($this->lang->line('contributions') != '') { echo stripslashes($this->lang->line('contributions')); } else echo "Contributions"; ?></h3>
				<div class="prize">
					<strong>$<?php echo round($amount, 2);?></strong>
					<div class="progress-bar">
					  <span class="fst"><?php if($this->lang->line('raised') != '') { echo stripslashes($this->lang->line('raised')); } else echo "Raised"; ?></span>
					  <span class="lst">$<?php echo $gift_ProductDetails->row()->gift_total;?></span>
					<?php 
					function progressBar($percentage){
						print "<div id=\"progress-bar\" class=\"all-rounded\">\n";
						print "<div id=\"progress-bar-percentage\" class=\"all-rounded\" style=\"width: $percentage%\">";
								if ($percentage > 0) {/* print "$percentage%"; */} else {print "<div class=\"spacer\">&nbsp;</div>";}
						print "</div></div>";
			       }
					$val1 = $gift_ProductDetails->row()->gift_total;
					$val2 = round($amount, 2);
					$res = (($val1 - $val2)/($val1))*100;
					$res1 = 100 - $res;
					$res1 = round($res1, 2); 
					progressBar($res1);
					?>
					</div>
				</div>
				<?php 
				  $db_date = $gift_ProductDetails->row()->created;
				  $db_date = strtotime($db_date);
				  $current_date = strtotime(date('Y-m-d'));
				  $expired_date = strtotime("+".$this->config->item('expiry_days')." day", $db_date);
				?>
				<ul class="remaining_date">
				<?php if($gift_ProductDetails->row()->status == 'Cancelled' || $gift_ProductDetails->row()->status == 'Unsuccessful' || $gift_ProductDetails->row()->status == 'Cancelled Admin'){?>
				<?php } else {?>
					<li>
					<?php if($expiry_date->num_rows() ==1) {?>
						<b><?php 
                              $db_date = $gift_ProductDetails->row()->created;
							  $db_date = strtotime($db_date);
							  $current_date = strtotime(date('Y-m-d'));
							  $expired_date = strtotime("+".$this->config->item('expiry_days')." day", $db_date);						
							  $val = $expired_date - $current_date;
							  $temp=$val/86400;
							  $days = floor($temp);
							  if($days<0){?>
							    <script>
								  $(document).ready(function(){
								     var status = "Unsuccessful";
									 id = '<?php echo $gift_ProductDetails->row()->id?>';
									 gift_seller_id = '<?php echo $gift_ProductDetails->row()->gift_seller_id ?>'
									 url ='<?php echo base_url()?>site/groupgift/update_expire';
								     $.post(url,{'status':status,'id':id,'ids':gift_seller_id},function(){
									 });
								  });
								</script>
							  <?php }
                              echo $days;							  
						?> <?php if($this->lang->line('groupgifts_days') != '') { echo stripslashes($this->lang->line('groupgifts_days')); } else echo "days"; ?></b> <?php if($this->lang->line('remaining') != '') { echo stripslashes($this->lang->line('remaining')); } else echo "Remaining"; ?>
						<?php } ?>
					</li>
					<?php }?>
					<li>
						<b><?php echo $people; ?> <?php if($this->lang->line('onboarding_people') != '') { echo stripslashes($this->lang->line('onboarding_people')); } else echo "people"; ?></b> <?php echo $people; ?> <?php if($this->lang->line('contributed') != '') { echo stripslashes($this->lang->line('contributed')); } else echo "Contributed"; ?>
					</li>
				</ul>
				<div class="btn-area">
					<?php if($gift_ProductDetails->row()->status == 'Cancelled'){?>
						<a class="contribute_btn" disabled="disabled" ><?php if($this->lang->line('cancel_by_creator') != '') { echo stripslashes($this->lang->line('cancel_by_creator')); } else echo "Canceled (by Creator)"; ?></a>
					<?php }elseif($gift_ProductDetails->row()->status == 'Unsuccessful'){?>
						<a class="contribute_btn" disabled="disabled" ><?php if($this->lang->line('unsuccessful') != '') { echo stripslashes($this->lang->line('unsuccessful')); } else echo "Unsuccessful"; ?></a>
					<?php } elseif($gift_ProductDetails->row()->status == 'Cancelled Admin'){?>
					    <a class="contribute_btn" disabled="disabled" ><?php if($this->lang->line('cancel_by_admin') != '') { echo stripslashes($this->lang->line('cancel_by_admin')); } else echo "Canceled (by admin)"; ?></a>
					<?php } elseif($gift_ProductDetails->row()->gift_total <= $amount || $gift_ProductDetails->row()->status == 'Successful'){?>
					    <script>
						  $(document).ready(function(){
							 var status = "Successful";
							 id = '<?php echo $gift_ProductDetails->row()->id?>';
							 gift_seller_id = '<?php echo $gift_ProductDetails->row()->gift_seller_id ?>'
							 url ='<?php echo base_url()?>site/groupgift/update_expire';
							 $.post(url,{'status':status,'id':id,'ids':gift_seller_id},function(){
							 });
						  });
						</script>
					    <a class="contribute_btn" disabled="disabled" ><?php if($this->lang->line('successful') != '') { echo stripslashes($this->lang->line('successful')); } else echo "Successful"; ?></a>
					<?php } else{?>
                    <a <?php if($loginCheck==''){ ?>require-login='true'<?php }?> href="gifts/contribute/<?php echo $gift_ProductDetails->row()->gift_seller_id;?>" class="contribute_btn"><?php if($this->lang->line('contribute') != '') { echo stripslashes($this->lang->line('contribute')); } else echo "Contribute"; ?></a>					
                    <?php } ?>					
					<span>$<?php echo $this->config->item('default_amount');?> <?php if($this->lang->line('minimum_contribution') != '') { echo stripslashes($this->lang->line('minimum_contribution')); } else echo "minimum contribution"; ?></span>
				</div>
				
				<div class="notify1">
				<?php if($gift_ProductDetails->row()->status == 'Cancelled' || $gift_ProductDetails->row()->status == 'Active' || $gift_ProductDetails->row()->status == 'Cancelled Admin'){ ?>
					<?php if($this->lang->line('receive_contribute_text') != '') { echo stripslashes($this->lang->line('receive_contribute_text')); } else echo "This group gift must receive all contributions by"; ?> <?php echo date('m/d/Y', $expired_date);?> <?php if($this->lang->line('receive_contribute_text1') != '') { echo stripslashes($this->lang->line('receive_contribute_text1')); } else echo "to be successful."; ?>
				<?php } else{ ?>
				    <?php if($this->lang->line('campagin_reaches_goal') != '') { echo stripslashes($this->lang->line('campagin_reaches_goal')); } else echo "This campaign reached the deadline without achieving its funding goal on"; ?> <?php echo date('M d', $expired_date);?>. 
				<?php } ?>
				</div>
            </div>
            <div class="detail_sidebar_list">
            
			<div class="wrapper product-info">
            <h3 class="detail_link_list"><?php if($this->lang->line('product_info') != '') { echo stripslashes($this->lang->line('product_info')); } else echo "Product Info"; ?></h3>
				<div class="store-name"><figcaption><a href="things/<?php echo $gift_ProductDetails->row()->pid;?>/<?php echo $gift_ProductDetails->row()->seourl;?>"><?php echo $gift_ProductDetails->row()->product_name;?></a></figcaption>
				<?php echo $fancyText = LIKED_BUTTON; ?> <?php if($this->lang->line('user_by') != '') { echo stripslashes($this->lang->line('user_by')); } else echo "by"; ?> <?php echo $gift_ProductDetails->row()->likes;?> <?php if($this->lang->line('onboarding_people') != '') { echo stripslashes($this->lang->line('onboarding_people')); } else echo "people"; ?></div>
				
				<ul class="product-list">
				  	<?php 
						$limitCount = 0;
						$imgArr = explode(',', $gift_ProductDetails->row()->image);
						if (count($imgArr)>0){
							foreach ($imgArr as $imgRow){
								if ($limitCount>5)break;
								if ($imgRow != '' && $imgRow != $pimg){
									$limitCount++;
						?>
						  <li><a href="things/<?php echo $gift_ProductDetails->row()->pid;?>/<?php echo $gift_ProductDetails->row()->seourl;?>"><img src="<?php echo DESKTOPURL;?>images/product/<?php echo $imgRow;?>" /></a></li>
						<?php 
								}
							}
						}
						?>
				</ul>
				
				<div class="description">
					<div class="less">
					<p><?php echo $gift_ProductDetails->row()->description;?></p>
					
					<strong><?php if($this->lang->line('twothree_for_shipping') != '') { echo stripslashes($this->lang->line('twothree_for_shipping')); } else echo "Please allow 2-3 weeks for shipping."; ?></strong></p>
					</div>
					<a href="#" class="more" onclick="$('div.description &gt; div').removeClass('less');$(this).hide().next().show();return false"><?php echo site_lg('lg_more', 'More');?></a>
					<a href="#" class="less" style="display:none" onclick="$('div.description &gt; div').addClass('less');$(this).hide().prev().show();return false"><?php echo site_lg('lg_less', 'Less');?></a>
				</div>
			</div>
            </div>
		</aside>
		<!-- / sidebar -->
	</div>

<script>
  $(document).ready(function(){
      $('#btn-comment').click(function(){
	     var comments = $('#comments-send').val();
		     product_id = '<?php echo $gift_ProductDetails->row()->gift_seller_id?>';
		     url = '<?php echo base_url();?>site/groupgift/groupgift_comment';
			 if(comments=='')
             {
                alert('<?php if($this->lang->line('your_cmt_empty') != '') { echo stripslashes($this->lang->line('your_cmt_empty')); } else echo "Your comment is empty"; ?>');
             }else{
			    $("#flash").show();
	            $("#flash").fadeIn(400).html('<img src="images/ajax-loader.gif" align="absmiddle">&nbsp;<span class="loading"><?php if($this->lang->line('load_cmt') != '') { echo stripslashes($this->lang->line('load_cmt')); } else echo "Loading Comment"; ?>...</span>');
				$.post(url,{'comments':comments,'group_product_id':product_id},function(data){
		           if(data == 1){
				      alert('<?php if($this->lang->line('your_cmt_wait_aprove') != '') { echo stripslashes($this->lang->line('your_cmt_wait_aprove')); } else echo "Your comment is waiting for approval"; ?>');
				      window.location.reload();  
				   }
				   $('#comments-send').val('');
	               $("#flash").hide();
                });
			 }return false;	 
	  });
	$(".contribute_btn").click(function() {
	var requirelogin = $(this).attr('require-login');
	if(requirelogin){
		var thingURL = $(this).parent().next().find('a:first').attr('href');
		window.location = baseURL+thingURL;
		return false;
	}
  });
function approvegrogiftCmt(evt){
	if($(evt).hasClass('approving'))return;
	$(evt).addClass('approving');
	$(evt).text('Approving...');
	var cfm = window.confirm('Are you sure to approve this comment ?\n This action cannot be done.');
	if(cfm){
		var cid = $(evt).data('cid');
		var tid = $(evt).data('tid');
		var uid = $(evt).data('uid');
		$.ajax({
			type:'post',
			url:baseURL+'site/groupgift/approve_comment',
			data:{'cid':cid,'tid':tid,'uid':uid},
			dataType:'json',
			success:function(json){
				if(json.status_code == '1'){
					$(evt).parent().remove();
				}else{
					$(evt).removeClass('approving');
					$(evt).text('Approve');
				}
			}
		});
	}else{
		$(evt).removeClass('approving');
		$(evt).text('Approve');
	}
}
function deletegrogiftCmt(evt){
	if($(evt).hasClass('deleting'))return;
	$(evt).addClass('deleting');
	$(evt).text('Deleting...');
	var cfm = window.confirm('Are you sure to delete this comment ?\n This action cannot be undone.');
	if(cfm){
		var cid = $(evt).data('cid');
		$.ajax({
			type:'post',
			url:baseURL+'site/groupgift/delete_comment',
			data:{'cid':cid},
			dataType:'json',
			success:function(json){
				if(json.status_code == '1'){
					$(evt).parent().parent().remove();
					window.location.reload();
				}else{
					$(evt).removeClass('deleting');
					$(evt).text('Delete');
				}
			}
		});
	}else{
		$(evt).removeClass('deleting');
		$(evt).text('Delete');
	}
	//window.location.reload();
}
</script>
	<?php } ?>
		</div>
		<!-- / wrapper-content -->
	<!-- / container -->
</div>
</div>
<script src="js/site/<?php echo SITE_COMMON_DEFINE ?>filesjquery_zoomer.js" type="text/javascript"></script>
<script type="text/javascript" src="js/site/<?php echo SITE_COMMON_DEFINE ?>selectbox.js"></script>
<script type="text/javascript" src="js/site/thing_page.js"></script>
<?php
$this->load->view('site/templates/footer');
?>