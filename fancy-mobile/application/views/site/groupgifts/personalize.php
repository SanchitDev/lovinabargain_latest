<?php 
   if($this->uri->segment(1) == 'things'){
?>
<div class="popup group_giftsa ly-title" style="display:none;">
	<p class="ltit"><?php if($this->lang->line('create_a_group_gift') != '') { echo stripslashes($this->lang->line('create_a_group_gift')); } else echo "Create a Group Gift"; ?></p>
	<div class="ltxt">
		<ol class="gift-group-personal" style="position:relative;">
		   <li class="depth1" id="dep1"><a onclick="javascript:recipient();" class="current"><?php if($this->lang->line('group_gift_recipient') != '') { echo stripslashes($this->lang->line('group_gift_recipient')); } else echo "Choose Recipient"; ?></a><span></span></li>
		   <li class="depth2 current" id="dep2"><a onclick="javascript:personalize();"><?php if($this->lang->line('group_gift_personalize') != '') { echo stripslashes($this->lang->line('group_gift_personalize')); } else echo "Personalize"; ?></a><span></span></li>
		   <li class="depth3" id="dep3"><a onclick="javascript:contributions();"><?php if($this->lang->line('ask_for_contributions') != '') { echo stripslashes($this->lang->line('ask_for_contributions')); } else echo "Ask for Contributions"; ?></a></li>
		</ol>
		<div class="popup_details_gift">
		   <h3><?php if($this->lang->line('fill_details_group_gift') != '') { echo stripslashes($this->lang->line('fill_details_group_gift')); } else echo "Fill in details for group gift"; ?></h3>
	  </div>
	  
	  <div class="title_entermanually" id ="entermanually" style="float:left; width:100%">
      
	       <div class="title_enter_manually">
           
		       <div class="title_enter_manually_box">
               		<ul>
                        <li>
                        	<label for=""><?php if($this->lang->line('groupgift_title') != '') { echo stripslashes($this->lang->line('groupgift_title')); } else echo "Title"; ?></label>
                        	<input type="text" value="" name= "gift_name" class="title_manually_input" id="gift_name" placeholder="Ex. Jenny’s birthday present">
                        </li>
                        <li>
                        	<label for=""><?php if($this->lang->line('groupgift_description') != '') { echo stripslashes($this->lang->line('groupgift_description')); } else echo "Description"; ?></label>
                            <textarea class="title_manually_input title_manually_textarea" placeholder="Ex. Lets celebrate Jenny and give her an amazing gift!" name="gift_description" id="gift_description" ></textarea>
                        </li>
                         <li>
                        	<span><?php if($this->lang->line('groupgift_description_text') != '') { echo stripslashes($this->lang->line('groupgift_description_text')); } else echo "Use your group gift description to share more about who you’re raising contributions for and why."; ?></span>
                        </li>
                        <li>
                        	<label for=""><?php if($this->lang->line('groupgift_note') != '') { echo stripslashes($this->lang->line('groupgift_note')); } else echo "Note"; ?></label>
                        	<input type="text" value="" name="gift_notes" id="gift_notes" class="title_manually_input" placeholder="You can leave a personalized note to merchant here.">
                        </li>
                    </ul>
                    
				  
				  
			   </div> 
		   </div>
		   <?php if ($productDetails->num_rows()==1){
		         $img = 'dummyProductImage.jpg';
		         $imgArr = explode(',', $productDetails->row()->image);
		             if (count($imgArr)>0){
			             foreach ($imgArr as $imgRow){
				            if ($imgRow != ''){
					           $img = $pimg = $imgRow;
					           break;
				            }
			            }
		            }
					if($productDetails->row()->sale_price == ''){
		                $price = $productDetails->row()->price;
		            }else{
		                $price = $productDetails->row()->sale_price;
		            }
		   ?>
		   <div class="group_gift_total">
                 <input type="hidden" name="seller_id" id ="seller-id" value=""/>
				 <input type="hidden" name="country" id ="total-val" value=""/>
				 
		         <div class="group_gift_total_img"><img src="<?php echo DESKTOPURL;?>images/product/<?php echo $img;?>"
                                                    alt="<?php echo $productDetails->row()->product_name;?>"/></div>
                 
                 <div class="group_gift_total_tab">
                 
                 	<table width="100%" cellpadding="0" cellspacing="0" border="0">
                    
                    	<tr style="border-top: 1px solid #F4F4F4;">
                        
                        	<td><span><?php if($this->lang->line('groupgift_itemtotal') != '') { echo stripslashes($this->lang->line('groupgift_itemtotal')); } else echo "Item Total"; ?></span></td>
                            
                            <td><strong>$<?php echo $price?><small><?php echo site_lg('lg_usd', 'USD');?></small></strong></td>
                        
                        </tr>
                        
                        
                        <tr style="border-top: 1px solid #F4F4F4;">
                        
                        	<td><span><?php if($this->lang->line('groupgift_shipping') != '') { echo stripslashes($this->lang->line('groupgift_shipping')); } else echo "Shipping"; ?></span></td>
                            
                            <td><strong id="shipping-cost"></strong></td>
                        
                        </tr>
                        
                        
                        <tr style="border-top: 1px solid #F4F4F4;">
                        
                        	<td><span><?php if($this->lang->line('groupgift_tax') != '') { echo stripslashes($this->lang->line('groupgift_tax')); } else echo "Tax"; ?></span></td>
                            
                            <td><strong id ="shipping-tax"></strong></td>
                        
                        </tr>
                    
                    	<tr style="border-top: 1px solid #F4F4F4;">
                        
                        	<td><span><?php if($this->lang->line('goal_total') != '') { echo stripslashes($this->lang->line('goal_total')); } else echo "Goal Total"; ?></span></td>
                            <input type="hidden" id="goal-total-input" name="gift_total" value=""/>
                            <td><strong id ="goal-total"></strong></td>
                        
                        </tr>
                    
                    
                    </table>
                 
                 </div>
                 
                 
		   </div>
		   <?php } ?>
	  </div>
	</div>
	<div class="btn-area">
    <div class="howit_works"><i></i><a href="<?php echo base_url();?>pages/group-gift"><?php if($this->lang->line('how_it_works') != '') { echo stripslashes($this->lang->line('how_it_works')); } else echo "How it works"; ?></a></div>
		<button class="btn-done" id= "btn-create"><?php if($this->lang->line('create_group_gift') != '') { echo stripslashes($this->lang->line('create_group_gift')); } else echo "Create Group Gift"; ?></button>
        <button class="btn-back" id= "btn-back"><?php if($this->lang->line('go_back') != '') { echo stripslashes($this->lang->line('go_back')); } else echo "Go Back"; ?></button>
	</div>
	<button title="Close" class="ly-close"><i class="ic-del-black"></i></button>
</div>
<?php } ?>