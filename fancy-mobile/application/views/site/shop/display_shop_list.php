<?php $this->load->view('site/templates/header'); ?>
<div id="content" class="shop">
	<div class="color-container" style="display: inline-block; text-align: center; width: 100%;">
		<?php  if($bannerList->num_rows()>0){
			$limitCount = 0;
			foreach ($bannerList->result() as $bannerListRow){
				if($limitCount<1){
				$limitCount++;
				$link = $bannerListRow->link;
				//echo '<pre>';print_r($bannerListRow);exit;
					if ($link != ''){
						if (substr($link,0,4) != 'http'){
							$link = 'http://'.$link;
						}
					}
				?>
			<?php if ($link!=''){?><a target="_blank" href="<?php echo $link;?>"><?php }?><img src="<?php echo DESKTOPURL;?>images/category/banner/<?php echo $bannerListRow->image;?>" alt="<?php echo $bannerListRow->name;?>" /><?php if ($link!=''){?></a><?php }?>
		<?php }}}?>
	</div>	        
	<div class="color-container">
		<h2 class="color-title"><?php if($this->lang->line('whats_new') != '') { echo stripslashes($this->lang->line('whats_new')); } else echo "What's New"; ?><a href="shopby/all"><?php if($this->lang->line('see_more') != '') { echo stripslashes($this->lang->line('see_more')); } else echo "See more"; ?></a> </h2>
		<ul class="color-shop">
			<?php if($recentProducts->num_rows()>0){
					foreach ($recentProducts->result() as $relatedRow){
					$img = 'dummyProductImage.jpg';
						$imgArr = array_filter(explode(',', $relatedRow->image));
						if (count($imgArr)>0){
							foreach ($imgArr as $imgRow){
								if ($imgRow != ''){
									$img = $imgRow;
									break;
								}
							}
						}?>
						<li class="color_item">
							<a href="<?php echo base_url();?>things/<?php echo $relatedRow->id;?>/<?php echo url_title($relatedRow->product_name,'-');?>">
								<img alt="" src="<?php echo DESKTOPURL;?>images/product/<?php echo $img;?>">
								<?php echo $relatedRow->product_name;?><b><?php echo $relatedRow->sale_price;?> </b>
							</a>
						</li>
			<?php }}?>
		</ul>
	</div>
	<div class="color-container">
		<h3 class="catagory-title"><a class="first-click" href="javascript:void(0);"><?php echo site_lg('lg_category', 'Category');?><i class="arrow"></i></a></h3>
		<ul class="catagory-list selective">
		<?php foreach ($mainCategories->result() as $mainCategoriesRow){ 
			$cat_img = base_url().'images/no_image.gif';
                	$cat_img = '';
                	if ($mainCategoriesRow->image != ''){
                		if (file_exists(DESKTOPURL.'images/category/'.$mainCategoriesRow->image)){
	                		$cat_img = DESKTOPURL.'images/category/'.$mainCategoriesRow->image;
                		}	
                	}
		?>
			<li>
				<a class="<?php echo $mainCategoriesRow->cat_name;?>" href="shopby/<?php echo $mainCategoriesRow->seourl;?>">
					<img style="width:20px; height:15px;" src="<?php echo $cat_img;?>" alt="<?php //echo $mainCategoriesRow->cat_name;?>" title="<?php echo $mainCategoriesRow->cat_name;?>" />
					<?php echo $mainCategoriesRow->cat_name;?>
				</a>
            </li>
		<?php }?>
		</ul> 
		<h3 class="catagory-title"><a href="javascript:void(0);" class="price-arrow"><?php echo site_lg('giftcard_price', 'Price');?> <i class="arrow price1-arrow"></i></a></h3>
		<ul class="catagory-list selective2"> <?php foreach ($pricefulllist->result() as $priceRangeRow){ ?>
			<li><a href="shopby/all?p=<?php echo url_title($priceRangeRow->price_range); ?>"><?php echo $currencySymbol;?> <?php echo ucfirst($priceRangeRow->price_range);  ?></a></li>
		   <?php } ?>
		</ul>
		<h3 class="catagory-title"><a href="javascript:void(0);" class="color-arrow"><?php echo site_lg('lg_color', 'Color');?><i class="arrow color1-arrow"></i></a></h3>
		<ul class="coloring-list">
			<?php foreach ($mainColorLists->result() as $colorRow){
					if ($colorRow->list_value != ''){?>
                        <li>
							<a class="<?php echo strtolower($colorRow->list_value);?>"href="shopby/all?c=<?php echo strtolower($colorRow->list_value);?>">
							<span class="icon"></span> 
							<?php echo ucfirst($colorRow->list_value);?></a>
						</li>
			<?php }}?>
		</ul>
	</div>  
</div>
<script>
	$('.coloring-list, .selective2').hide();
	$('.price-arrow, .price-arrow i').click(function(){
	
		$('.selective2').toggle();
	});
	$('.color-arrow, .color-arrow i').click(function(){
		$('.coloring-list').toggle();
	});
	
	$('.catagory-list, .selective').hide();
	$('.first-click, .first-click i').click(function(){
		$('.selective').toggle();
	});	
</script>