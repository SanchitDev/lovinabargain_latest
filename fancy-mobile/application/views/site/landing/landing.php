<?php $this->load->view('site/templates/header');?>
<div style="padding: 0.75em;">
<div >

<div id="content">
<div class="figure-product figured_def_pro ">
    <div class="welcome">
    <h3>Featured Products</h3>
    </div>





<?php
if($featured_products->num_rows > 0) {
	$i = 0;
	foreach($featured_products->result() as $product){
		if (isset($product->id)){
			$imgArr = explode(',', $product->image);
			$img = 'dummyProductImage.jpg';
			foreach ($imgArr as $imgVal){
				if ($imgVal != ''){
					$img = $imgVal;
					break;
				}
			}
			$fancyClass = 'fancy';
			$fancyText = LIKE_BUTTON;
			if (count($likedProducts)>0 && $likedProducts->num_rows()>0){
				foreach ($likedProducts->result() as $likeProRow){
					if ($likeProRow->product_id == $product->seller_product_id){
						$fancyClass = 'fancyd';$fancyText = LIKED_BUTTON;break;
					}
				}
			}
			if (isset($product->web_link)){
				$prodLink = "user/".$product->user_name."/things/".$product->seller_product_id."/".url_title($product->product_name,'-');
			}else {
				$prodLink = "things/".$product->id."/".url_title($product->product_name,'-');
			}
?>
	<div class="figure-product">
		<div class="figure">
			<a href="<?php echo $prodLink ?>" class="thing-url">
				<span>
					<img src="<?php echo DESKTOPURL.'images/product/'.$img; ?>">
				</span>
				<figcaption>
					<?php echo $product->product_name;?>
				</figcaption>
			</a>
			<b class="price"><?php echo $currencySymbol.$product->sale_price;?></b>
			<a class="username" href="user/<?php  echo $product->user_name; ?>">
				<?php  echo $product->full_name; ?>
			</a> + <?php echo $product->likes; ?>
		</div>
		<div class="option">
			<a href="#" item_img_url="<?php echo DESKTOPURL;?>images/product/<?php echo $img;?>" tid="<?php echo $productDetailRow->seller_product_id;?>" class="fancyy <?php echo $fancyClass;?>" <?php if ($loginCheck==''){?>require_login="true"<?php }?>><i class="icon"></i><?php echo $fancyText;?></a>
			<a href="#" onclick="return show_list_pop(this);" data-tid="<?php echo $productDetailRow->seller_product_id;?>" require_login="<?php if (count($userDetails)>0){echo 'false';}else {echo 'true';}?>" class="list"><i class="want_icon"></i><?php if($this->lang->line('header_add_list') != '') { echo stripslashes($this->lang->line('header_add_list')); } else echo "Add to List"; ?></a>
			<a href="<?php echo $prodLink;?>" class="thing-url comment"><i class="icon"></i><?php if ($productDetailRow->total_comments>0){echo $productDetailRow->total_comments;}else {echo 0;}?></a>
		</div>
	</div>
<?php
		}
	}
}
?>






    </div>
	</div>
</div>
<div class="clear"></div>
<div id="content">


<div class="welcome">
    <?php
        if($loginCheck==''){
            foreach($layoutfulllist->result() as $layoutListRow){
                if($layoutListRow->place == 'welcome text'){
    ?>
                    <h2><?php echo  $layoutListRow->text;?> </h2>
    <?php
            }
                if($layoutListRow->place == 'welcome tag' ){
    ?>
                    <p><?php echo  $layoutListRow->text;?></p>
    <?php
                }
            }
        }
    ?>
</div>


	<?php //echo '<pre>'; print_r($productDetails);die; ?>
	<div id="home-timeline">
    	<?php
		if (count($productDetails)>0){
			foreach($productDetails as $productDetailRow){
		?>
        <div class="figure-product">
        	<?php
			if($productDetailRow->user_id == 0){
				$userName = 'administrator';
			   	$fullName = 'Administrator';
			}else{
				$userName = $productDetailRow->user_name;
				$fullName = $productDetailRow->full_name;
			}
			$fancyClass = 'fancy';
			$fancyText = LIKE_BUTTON;
			if (count($likedProducts)>0 && $likedProducts->num_rows()>0){
				foreach ($likedProducts->result() as $likeProRow){
					if ($likeProRow->product_id == $productDetailRow->seller_product_id){
						$fancyClass = 'fancyd';$fancyText = LIKED_BUTTON;break;
					}
				}
			}
			if ($userName == ''){
				$userName = 'anonymous';
				$fullName = 'Anonymous';
			}
			if (isset($productDetailRow->web_link)){
				$prodLink = "user/".$userName."/things/".$productDetailRow->seller_product_id."/".url_title($productDetailRow->product_name,'-');
			}else {
				$prodLink = "things/".$productDetailRow->id."/".url_title($productDetailRow->product_name,'-');
			}
			if ($fullName == ''){
				$fullName = $userName;
			}
			$imgArr = explode(',', $productDetailRow->image);
			$img = 'dummyProductImage.jpg';
			foreach ($imgArr as $imgVal){
				if ($imgVal != ''){
					$img = $imgVal;
					break;
				}
			}
	//	echo("<pre>");print_r(DESKTOPURL);die;
			?>
            <div class="figure">
            	<a href="<?php echo $prodLink;?>" class="thing-url">
                	<span><img src="<?php echo DESKTOPURL;?>images/product/<?php echo $img;?>" /></span>
                	<figcaption><?php echo $productDetailRow->product_name; ?></figcaption>
                </a>
                <?php  if (!isset($productDetailRow->web_link)){ ?>
                <b class="price"><?php echo $currencySymbol.$productDetailRow->sale_price; ?></b>
                <?php } ?>
				<a class="username" href="user/<?php  echo $userName; ?>" ><?php  echo $fullName; ?></a> + <?php  echo $productDetailRow->likes; ?>
			</div>
            <div class="option">
            	<a href="#" item_img_url="<?php echo DESKTOPURL;?>images/product/<?php echo $img;?>" tid="<?php echo $productDetailRow->seller_product_id;?>" class="fancyy <?php echo $fancyClass;?>" <?php if ($loginCheck==''){?>require_login="true"<?php }?>><i class="icon"></i><?php echo $fancyText;?></a>
				<a href="#" onclick="return show_list_pop(this);" data-tid="<?php echo $productDetailRow->seller_product_id;?>" require_login="<?php if (count($userDetails)>0){echo 'false';}else {echo 'true';}?>" class="list"><i class="want_icon"></i><?php if($this->lang->line('header_add_list') != '') { echo stripslashes($this->lang->line('header_add_list')); } else echo "Add to List"; ?></a>
                <a href="<?php echo $prodLink;?>" class="thing-url comment"><i class="icon"></i><?php if ($productDetailRow->total_comments>0){echo $productDetailRow->total_comments;}else {echo 0;}?></a>
			</div>
		</div>
        <?php
			}
		?>
		<div id="infscr-loading" style="width: 100%;text-align: center;display: none;">
			<span style="background: url('images/ajax-loader/ajax-loader12.gif') no-repeat;width: 35px;height: 35px;display: block;margin: 0 auto;" class="loading"></span>
		</div>
		<div class="pagination"><?php echo $paginationDisplay;?></div>
		<?php
		}else{
		?>
		<h3><?php if($this->lang->line('product_noavail') != '') { echo stripslashes($this->lang->line('product_noavail')); } else echo "No products available"; ?></h3>
		<?php
		}
		?>
	</div>
</div>

<script type="text/javascript">
	var loading=false;
	var $win     = $(window),
	$stream  = $('#home-timeline');
	$(window).scroll(function() { //detect page scroll
		if($(window).scrollTop() + $(window).height() == $(document).height()) { //user scrolled to bottom of the page?
			var $url = $('.btn-more').attr('href');
			if(!$url) $url='';
			if($url != '' && loading==false) //there's more data to load
			{
				loading = true; //prevent further ajax loading
				$('#infscr-loading').show(); //show loading image
				$.ajax({
					type:'post',
					url:$url,
					success:function(html){
						var $html = $($.trim(html)),
					    $more = $('.pagination > a'),
					    $new_more = $html.find('.pagination > a');

						if($html.find('#home-timeline').text() == ''){
							//$stream.append('<ul class="product_main_thumb"><li style="width: 100%;"><p class="noproducts">No more products available</p></li></ul>');
						}else {
							if($html.find('#home-timeline').html().length > 100){
								$stream.append( $html.find('#home-timeline').html());
							}
						}
						if($new_more.length) $('.pagination').append($new_more);
						$more.remove();

						//hide loading image
						$('#infscr-loading').hide(); //hide loading image once data is received

						loading = false;
						after_ajax_load();

					},
					fail:function(xhr, ajaxOptions, thrownError) { //any errors?

						alert(thrownError); //alert with HTTP error
						$('#infscr-loading').hide(); //hide loading image
						loading = false;

					}
				});

			}
		}
	});
</script>
<?php $this->load->view('site/templates/footer');?>
