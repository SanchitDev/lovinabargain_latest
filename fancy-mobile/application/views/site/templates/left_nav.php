<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left bgcolo" id="cbp-spmenu-s1">
	<div class="zoomer-top" id="navigation-test">
              <form action="<?php base_url();?>shopby/all" class="search">
    	<fieldset>
		    <i class="icon"></i>
		  <!--  <input type="text" placeholder="Search" name="q" class="text" id="search-query" value="" autocomplete="off"/>-->
            <input type="text" name="q" id="search-query" placeholder="<?php if($this->lang->line('header_search') != '') { echo stripslashes($this->lang->line('header_search')); } else echo "Search"; ?>" value="" autocomplete="off"/>
            <div class="feed-search" style="display: none;top: 40px;position: absolute;width: 100%;left: 0px;">
			</div>
	    </fieldset>
          </form>
		    <button class="cancel-black"><?php echo site_lg('lg_cancel', 'Cancel');?></button>
    </div>
    <div class="sidebar-menu">
    	<div class="user-id">
		<?php if($loginCheck ==''){?>
			<a href="login" class="new-buttns" ><?php echo site_lg('login_signto', 'Sign in to');?> <?php echo $siteTitle;?></a>
			<?php
			}else{
				if($userDetails->row()->thumbnail == ''){
					$thumbImg = 'user-thumb1.png';
				}else{
					$thumbImg = $userDetails->row()->thumbnail;
				}
			?>
    		<a class="area-id" href="user/<?php echo $userDetails->row()->user_name;?>">
				<span class="identy"><img src="<?php echo DESKTOPURL;?>images/users/<?php echo $thumbImg;?>"></span>
				<b class="fullname"><?php echo $userDetails->row()->full_name;?></b><span class="user1"><?php echo $userDetails->row()->user_name;?></span>
		    </a>
    		<a class="place_setting" href="<?php echo base_url(); ?>settings"><i class="icon"></i></a>
			<?php }?>
    	</div>
	    <dl class="quick-menu">
			<dt class="quick-menu-tit"><?php echo site_lg('lg_quick_menu', 'QUICK MENU');?></dt>
			<dd><a href="<?php echo base_url(); ?>"><span class="icon home"></span><?php echo site_lg('lg_home', 'Home');?></a></dd>
			<dd><a href="shop"><span class="icon shop"></span><?php echo site_lg('lg_shop', 'Shop');?></a></dd>
			<?php if($loginCheck!=''){?>

			<dd>
				<a href="<?php if($userDetails->row()->group == 'Seller'){ echo "seller-product"; }else { echo "add"; }?>"><span class="icon add"></span><?php echo site_lg('header_add_to', 'Add to');?> <?php echo $siteTitle;?>
				</a>
			</dd>



			<dd><a href="notifications"><span class="icon notify"></span><?php echo site_lg('lg_notifications', 'Notifications');?><!--<span class="numb">0</span>--></a></dd>
			<?php }?>
	    </dl>
    	<dl class="quick-menu">
		    <dt class="quick-menu-tit"><?php echo site_lg('lg_shop', 'SHOP');?></dt>
                    <dd><a href="stores"><?php echo 'Stores';?></a></dd>
		    <dd><a href="fancybox"><?php echo site_lg('lg_subscription_box', 'Subscription Box');?></a></dd>
			<?php if ($this->config->item('giftcard_status') == 'Enable'){?>
		    <dd><a href="gift-cards"><?php if($this->lang->line('giftcard_cards') != '') { echo stripslashes($this->lang->line('giftcard_cards')); } else echo "Gift Cards"; ?></a></dd><?php
}?>

		    <!--<dd><a href="group-gifts">Group Gifts</a></dd>
		    <dd><a href="same-day-delivery">Same-Day Delivery</a></dd>-->
		    <!--<dd><a href="gifts/recommendations"><?php echo site_lg('lg_recommendations', 'Recommendations');?></a></dd>
		    <dd><a href="gifts/index">Gift Guides</a></dd>-->
    	</dl>
		<?php if($loginCheck!=''){?>
	    <dl class="quick-menu">
			<dt class="quick-menu-tit"><?php echo site_lg('lg_accounts', 'ACCOUNTS');?></dt>
			<!--<dd><a href="find-friends"><?php echo site_lg('onboarding_find_frds', 'Find friends');?></a></dd>-->
			<dd><a href="invite"><?php echo site_lg('invite_frds', 'Invite friends');?></a></dd>
			<dd><a href="purchases"><?php echo site_lg('lg_order_history', 'Order History');?></a></dd>
	    </dl>
		<?php }?>

	    <dl class="quick-menu">
	    	<dt class="quick-menu-tit"><?php if($this->lang->line('prference_language') != '') { echo stripslashes($this->lang->line('prference_language')); } else echo "Language"; ?></dt>
                <select onchange="javascript:window.location='<?php echo base_url();?>lang/'+this.value" style="width: 95%;
height: 1.95em;
background-repeat: no-repeat;
background-size: 17px auto;
background-position: 100% 50%;
border: 0.1em solid #CFD0D2;
border-radius: 0.15em;
box-shadow: 0 0.1em 0.1em #F2F2F2;
font-size: 1em;
text-indent: 0.01px;
vertical-align: inherit;
padding: 3px;
margin: 0.6em;">
                <?php
                $selectedLangCode = $this->session->userdata('language_code');
                if ($selectedLangCode == ''){
                	$selectedLangCode = $defaultLg[0]['lang_code'];
                }
                if (count($activeLgs)>0){
                	foreach ($activeLgs as $activeLgsRow){
                ?>
                    <option value="<?php echo $activeLgsRow['lang_code'];?>" <?php if ($selectedLangCode == $activeLgsRow['lang_code']){echo 'selected="selected"';}?>><?php echo $activeLgsRow['name'];?></option>
                <?php
                	}
                }
                ?>
                </select>

          </dl>

          <dl class="quick-menu">
			<dt class="quick-menu-tit"><?php echo $siteTitle;?></dt>



             <?php if (count($cmsPages)>0){ ?>
             <?php	foreach ($cmsPages as $cmsRow){
			 	if ($cmsRow['category'] == 'Main'){ ?>
			<dd><a href="pages/<?php echo $cmsRow['seourl'];?>"><?php echo $cmsRow['page_name'];?></a></dd>
            <?php
			}}}?>
			<?php if($loginCheck!=''){?>
			<dd><a href="logout"><?php echo site_lg('header_signout', 'Sign out');?></a></dd>
			<?php } ?>
	    </dl>
    </div>
</nav>
