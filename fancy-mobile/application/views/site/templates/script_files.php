<script type="text/javascript">
	var baseURL = '<?php echo base_url();?>';
	var BaseURL = '<?php echo base_url();?>';
	var likeTXT = '<?php echo addslashes(LIKE_BUTTON);?>';
	var likedTXT = '<?php echo addslashes(LIKED_BUTTON);?>';
	var unlikeTXT = '<?php echo addslashes(UNLIKE_BUTTON);?>';
	var currencySymbol = '<?php echo $currencySymbol;?>';
	var siteTitle = '<?php echo $siteTitle;?>';
	var can_show_signin_overlay = false;
	if (navigator.platform.indexOf('Win') != -1) {document.write("<style>::-webkit-scrollbar, ::-webkit-scrollbar-thumb {width:7px;height:7px;border-radius:4px;}::-webkit-scrollbar, ::-webkit-scrollbar-track-piece {background:transparent;}::-webkit-scrollbar-thumb {background:rgba(255,255,255,0.3);}:not(body)::-webkit-scrollbar-thumb {background:rgba(0,0,0,0.3);}::-webkit-scrollbar-button {display: none;}</style>");}
</script>
<script type="text/javascript" src="js/html5shiv.js"></script>
<script type="text/javascript" src="js/latest.js"></script>
<script type="text/javascript" src="js/modiriser2.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript">
function showView(val){
	if($('.showlist'+val).css('display')=='block'){
		$('.showlist'+val).hide('');	
	}else{
		$('.showlist'+val).show('');
	}	
}
</script>
<script src="js/jquery.colorbox.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(".cboxClose1").click(function(){
		$("#cboxOverlay,#colorbox").hide();
	});
	$("#button-done").click(function(){
		$('#cboxOverlay,#colorbox').trigger('click');
	});
	$(".reg-popup").colorbox({width:"91%", height:"auto", inline:true, href:"#product_container"});
	$(".list_popup").colorbox({width:"90%", height:"auto", inline:true, href:"#list_container"});
	$(".giftcard-popup").colorbox({width:"91%", height:"auto", inline:true, href:"#giftcard_popup"});
        $(".show_feature_products").colorbox({width:"1100px", height:"250px", inline:true, href:"#featured_product"});
	//This is for add new shipping address popup
	//$(".add-shipping-addr").colorbox({width:"90%", height:"auto", inline:true, href:"#shpping_container"});
	
	/*$('.add-shipping-addr').click(function(){
	var url = '<?php echo base_url();?>site/cart/add_new_shipping_address';
	$.get(url,function(data){
			$(".sddlyshow dl").html(data);
		$('.sdly-city-name, .sddlyshow strong, .sddlyhide').hide();
		$('.sddlycfc').show();
	});
});*/
	
	
	
	//Example of preserving a JavaScript event for inline calls.
	$("#onLoad").click(function(){ 
		$('#onLoad').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
		return false;
	});
$('.sdly-city-name, .sddlycfc').hide();

$('.more-cit').click(function(){
	var url = '<?php echo base_url();?>/site/samedaydelivery/more_countries';
	$.get(url,function(data){
		$(".sddlyshow dl").html(data);
		$('.sdly-city-name, .sddlyshow strong, .sddlyhide').hide();
		$('.sddlycfc').show();
	});
});
$('#search-sameday-zip').keyup(function(){
   var value = $(this).val();
	   url= "<?php echo base_url();?>/site/samedaydelivery/search_city";
   if(value != ''){
	   $.post(url,{'search':value},function(html){
		   $("#result").html(html).show();
	   }) ;
   }else{
	   $("#result").hide();
   }return false;
});
$(document).on("click", "#result", function(e){ 
   var $clicked = $(e.target);
   var $name = $clicked.find('.name').html();
   if(!$name){
		$name = $clicked.html();
   }
   var decoded = $("<div/>").html($name).text();
   //alert(decoded);
   $('#search-sameday-zip').val(decoded);
   if(decoded.indexOf('-') != -1){
	 split_values = decoded.split('-');
	 split_value = split_values[1];
   }else{
	 split_value = decoded;
   }
   s_value = encodeURI(split_value);
   window.location.href= "<?php echo base_url();?>same-day-delivery?q="+s_value;
});
$(document).on("click", ".show", function(e) { 
      var $clicked = $(e.target);
      if (! $clicked.hasClass("search-social-friends")){
       jQuery("#result").fadeOut(); 
      }
});
$('.view dd a').click(function(){
	var city = $(this).text().toLowerCase();
	window.location.href= "<?php echo base_url();?>same-day-delivery?q="+city;
});   
});
//search start
	// top menu bar
jQuery(function($){
	var $nav = $('#navigation-test'), $cur = null, cur_len = 0;

	$nav
		.on(
			{
				mouseover : function(){ $(this).addClass('hover'); },
				mouseout  : function(){ $(this).removeClass('hover'); }
			},
			'li.gnb, .menu-contain-gift li'
		)
		.find('li > a').each(function(){
			var $this = $(this), path = $this.attr('href');

			if(path == '/' || path == '#') return;
			if(location.pathname.indexOf(path) == 0 && path.length > cur_len){
				$cur = $this;
				cur_len =  path.length;
			}
		});
		if($cur) $cur.addClass('current');

	// browse menu
	(function(item_w){
		$('.menu-contain-things')
			.show()
			.find('>ul>li')
				.each(function(){ item_w = Math.max(item_w, this.offsetWidth) })
				.width(item_w)
				.parent().width(item_w * 2).end()
			.end()
			.css('display','');
	})(0);

	// live support layer open when live support opened at previous page
	try {
		if( $.jStorage.get('live_support') == 'on' ){
			open_chat( $.jStorage.get('live_support_minimum') );
		}
	} catch(e){};

	// search form
	(function(){
		var $search_form = $nav.find('form.search'),
		    $textbox = $search_form.find('#search-query'),
			$suggest = $search_form.find('.feed-search'),
		    $loading = $search_form.find('.loading'),
		    $things  = $search_form.find('ul.thing'),
			$users   = $search_form.find('ul.user'),
		    $tpl_thing   = $('#tpl-search-suggestions-things').remove(),
		    $tpl_user    = $('#tpl-search-suggestions-users').remove(),
			prev_keyword = $.trim($textbox.val()), timer = null,
			keys = {
				13 : 'ENTER',
				27 : 'ESC',
				38 : 'UP',
				40 : 'DOWN'
			};

		$search_form.on('submit', function(){
			var v = $.trim($textbox.val());
			if(!v) return false;
		});

		$textbox
			// highlight submit button when the textbox is focused.
			.on({
				focus : function(){ $nav.find('.search .btn-submit').addClass('focus') },
				blur  : function(){ $nav.find('.search .btn-submit').removeClass('focus') }
			})
			// search things and users as user types
			.on({
				keyup : function(event){
					var kw = $.trim($textbox.val());

					if(keys[event.which]) return;
					if(!kw.length) return $suggest.hide();
					if(kw.length && kw != prev_keyword) {
						prev_keyword = kw;

						$things.hide();
						$users.hide();
						$loading.show();
						$suggest.show();

						clearTimeout(timer);
						timer = setTimeout(function(){ find(kw) }, 500);
					}
				},
				keydown : function(event){
					var k = keys[event.which];

					if($suggest.is(':hidden') || !k) return;

					event.preventDefault();

					var $items = $suggest.find('a'), $selected = $items.filter('.hover'), idx;

					if(k == 'ESC') return $suggest.hide();
					if(k == 'ENTER') {
						if($selected.length) {
							window.location.href = $suggest.find('a.hover').attr('href');
						} else {
							$search_form.submit();
						}
						return;
					}

					if(!$selected.length) {
						$selected = $items.eq(0).mouseover();
						return;
					}

					idx = $items.index($selected);

					if(k == 'UP' && idx > 0) return $items.eq(idx-1).mouseover();
					if(k == 'DOWN' && idx < $items.length-1) return $items.eq(idx+1).mouseover();
				}
			});

		$suggest.delegate('a', 'mouseover', function(){ $suggest.find('a.hover').removeClass('hover'); $(this).addClass('hover'); });

		function find(word){
			$suggest.show();
//			$search_form.find('a.more').attr('href', baseURL+'shopby/all?q='+encodeURIComponent(word));
			$.ajax({
				type : 'GET',
				url  : baseURL+'site/searchShop/search_suggestions',
				data : {q:word},
				dataType : 'json',
				success  : function(json){
					$suggest.html(json.things).show();
				}
			});
		};

		function highlight(str, substr){
			var regex = new RegExp('('+encodeURIComponent(substr.replace(/ /g,'-')).replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&')+')', 'i');
			return str.replace(regex, '<strong>'+substr+'</strong>');
		};
		$('.cancel-black').click(function(){
			$('.feed-search').hide();
			$('#search-query').val('');
			$('#showLeftPush').trigger('click');
		});
		$(document).click(function(){ if($suggest.is(':visible')) $suggest.hide() });
	})();
 });
//search end
</script>
<script type="text/javascript">
function open_win(){
	window.open("<?php echo base_url();?>twtest/redirect");
	location.reload();
}
function get_cities(id){
	$('#city-'+id).toggle();
}
/*
 * Language Settings
 */
<?php if ($this->lang->line('shipping_add_ship')!=''){?> 
var lg_add_ship_addr = '<?php echo $this->lang->line('shipping_add_ship');?>';
<?php }else {?>
var lg_add_ship_addr = 'Add Shipping Address';
<?php }?>
<?php if ($this->lang->line('header_new_ship')!=''){?> 
var lg_new_ship_addr = '<?php echo $this->lang->line('header_new_ship');?>';
<?php }else {?>
var lg_new_ship_addr = 'New Shipping Address';
<?php }?>
<?php if ($this->lang->line('header_ships_wide')!=''){?> 
var lg_ships_wide = '<?php echo $this->lang->line('header_ships_wide');?>';
<?php }else {?>
var lg_ships_wide = 'We ships worldwide with global delivery services.';
<?php }?>
/*
 * ******************
 */


</script>