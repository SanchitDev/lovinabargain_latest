<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, user-scalable=no">
		<?php 
		if($this->config->item('google_verification')){ echo stripslashes($this->config->item('google_verification')); }
		if ($heading == ''){
		?>
		<title><?php echo $title;?></title>
		<?php }else {?>
		<title><?php echo $heading;?></title>
		<?php }?>
		<meta name="Title" content="<?php echo $meta_title;?>" />
		<meta name="keywords" content="<?php echo $meta_keyword; ?>" />
		<meta name="description" content="<?php echo $meta_description; ?>" />
		<base href="<?php echo base_url(); ?>" />
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo DESKTOPURL;?>images/logo/<?php echo $fevicon;?>"/>
		
		<!-- Loading Css Files -->
		<?php $this->load->view('site/templates/css_files');?>
		
		<!-- Loading Script Files -->
		<?php $this->load->view('site/templates/script_files');?>
		
		<!-- Loading Left Navigation -->
		<?php $this->load->view('site/templates/left_nav');?>
		
		<!-- Loading Right Navigation -->
		<?php //$this->load->view('site/templates/right_nav');?>
		
	</head>
	<body class="cbp-spmenu-push">
		<div id="global">
			<div id="container">
    			<div id="header">
					<?php if(($this->uri->segment(1)!= 'login') && ($this->uri->segment(1)!= 'signup')){?>
					<a class="icon nav" id="showLeftPush"  href="#"><?php echo site_lg('lg_navigation', 'Navigation');?></a>
					<?php }?>
        			<h1 class="logo"><a href="<?php echo base_url();?>"><img src="<?php echo DESKTOPURL;?>images/logo/<?php echo $logo;?>" alt="<?php echo $siteTitle;?>" title="<?php echo $siteTitle;?>" /></a></h1>
					<!--<a id="showRightPush" class="signin" href="/login">Sign up</a>-->
					<?php if($this->uri->segment(1)== 'login'){?>
					<a class="signin" href="signup"><?php echo site_lg('login_signup', 'Sign up');?></a>
					<?php 
					}else{
						if($loginCheck ==''){
					?>
					<a class="signin" href="login"><?php echo site_lg('signup_sign_in', 'Sign in');?></a>
					<?php }else{ echo $MiniCartViewSet; echo $auctionMiniCartViewSet;} }?>
        		</div>
        
				<!-- Loading Navigation Script Files -->
				<?php $this->load->view('site/templates/nav_script');?>
				        
				<!-- Loading Popup Templates -->
				<?php $this->load->view('site/templates/popup_templates'); ?>

				<!-- Loading Google Verification Code -->
				<?php if($this->config->item('google_verification_code')){ echo stripslashes($this->config->item('google_verification_code'));}?>
				