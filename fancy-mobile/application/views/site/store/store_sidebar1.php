<?php if(isset($productDetails) && $productDetails->num_rows()>0){
	$clear ='left';}else{$clear ='both';}?>
<div id="sidebar" style="margin-top:20px; clear:<?php echo $clear;?>;">
	<dl class="set_menu">
		<dt><?php if($this->lang->line('store_display_category') != '') { echo stripslashes($this->lang->line('store_display_category')); } else echo "CATEGORIES"; ?></dt>
		<?php
			$checked = '';
		?>
		<dd><a href="store/<?php echo $user_Details->row()->user_name;?>/products" <?php if ($this->uri->segment(1)=='store'  && $this->uri->segment(4)== ''){?>class="current"<?php }?>><?php echo 'All Category';?></a></dd>
		<?php if ($mainCategories->num_rows()>0){
				foreach ($mainCategories->result() as $row){
					if ($row->cat_name != ''){ 
						if (in_array($row->id, $store_category)){
		?>
			<dd><a href="store/<?php echo $user_Details->row()->user_name;?>/products/<?php echo $row->cat_name;?>" <?php if ($this->uri->segment(1)=='store' && $this->uri->segment(4)== $row->cat_name){?>class="current"<?php }?>><?php echo $row->cat_name; ?></a></dd>
		<?php }}}} ?>
	</dl>
	<dl class="set_menu">
		<dt><?php if($this->lang->line('store_display_sort') != '') { echo stripslashes($this->lang->line('store_display_sort')); } else echo "SORT"; ?></dt>
		
		<dd><input type="radio"  id="sort_by_price" onchange="search_value(this.id,this.value);" checked value="newest" name="sort_by_price"><?php if($this->lang->line('store_display_newest') != '') { echo stripslashes($this->lang->line('store_display_newest')); } else echo "Newest"; ?></dd>
		
		<dd><input type="radio" <?php if($this->input->get('sort_by_price')=='desc'){echo "checked";}?> id="sort_by_price" onchange="search_value(this.id,this.value);" value="desc" name="sort_by_price"><?php if($this->lang->line('store_display_highest') != '') { echo stripslashes($this->lang->line('store_display_highest')); } else echo "Highest price"; ?></dd>
		
		<dd><input type="radio" <?php if($this->input->get('sort_by_price')=='asc'){echo "checked";}?> id="sort_by_price" onchange="search_value(this.id,this.value);" value="asc" name="sort_by_price"><?php if($this->lang->line('store_display_lowest') != '') { echo stripslashes($this->lang->line('store_display_lowest')); } else echo "Lowest price"; ?></dd>
	</dl>
	<?php if ($immediate_shipping){?>
	<dl class="set_menu">
		<dt><?php if($this->lang->line('store_display_option') != '') { echo stripslashes($this->lang->line('store_display_option')); } else echo "OPTION"; ?></dt>
		<dd><input type="checkbox" <?php if($this->input->get('is')=='true'){echo "checked";}?> id ="is" onclick="search_value(this.id,this.value);" value="true" name="is"><?php if($this->lang->line('store_display_imshipping') != '') { echo stripslashes($this->lang->line('store_display_imshipping')); } else echo "Immediate Shipping"; ?></dd>
	</dl>
	<?php }?>
	<dl class="set_menu">
		<?php $price = $this->input->get('p');
				$price_value = explode('-',$price);
		?>
		<dt><?php if($this->lang->line('store_display_price') != '') { echo stripslashes($this->lang->line('store_display_price')); } else echo "PRICE"; ?></dt>
		<?php echo $currencySymbol;?><input style="width:30px;padding:6px 5px;" id="min_price" class="text" type="text" value="<?php echo $price_value[0];?>" name="min_price">
		&nbsp;<?php echo 'to';?>&nbsp;<?php echo $currencySymbol;?><input style="width:30px;padding:6px 5px;" id="max_price" class="text" type="text" value="<?php echo $price_value[1];?>" name="max_price">
		<button class="btn-save" style="padding: 0 5px;" onclick="store_price();"><?php if($this->lang->line('apply') != '') { echo stripslashes($this->lang->line('apply')); } else echo "Apply"; ?></button>
	</dl>
	<?php if (count($list_ids)>0){?>
	<dl class="set_menu">
		<dt><?php if($this->lang->line('store_display_color') != '') { echo stripslashes($this->lang->line('store_display_color')); } else echo "COLOR"; ?></dt>
		<?php if ($mainColorLists->num_rows()>0){
			 foreach ($mainColorLists->result() as $colorRow){
                      	if ($colorRow->list_value != ''){
                      		if (in_array($colorRow->id, $list_ids)){
		?>
		<dd><input id="c<?php echo $colorRow->id;?>" <?php if($this->input->get('c')== $colorRow->list_value){echo "checked";}?> onclick="search_value(this.id,this.value);" type="checkbox" value="<?php echo $colorRow->list_value; ?>" name="color">&nbsp;<?php echo $colorRow->list_value;?></dd>
		<?php }}}}?>
	</dl>
	<?php }?>
	<dl class="set_menu">
		<input style="width:110px;padding:5px;" id="q" type="text"  placeholder="<?php if($this->lang->line('product_filter_key') != '') { echo stripslashes($this->lang->line('product_filter_key')); } else echo "Filter by keyword"; ?>" onkeydown="if (event.keyCode == 13)search_value(this.id,this.value);" value="<?php echo $this->input->get('q');?>"/>
	</dl>
</div>