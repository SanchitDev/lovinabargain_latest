<?php 
$this->load->view('site/templates/header',$this->data);
$this->load->view('site/store/store_header',$this->data);
$this->load->view('site/store/store_sidebar'); 
?>
			<div class="store_rightbar">
				<div style="height: auto;" class="figure-row fancy-suggestions anim might_box">
				<?php $rownum = count($product_feedback); 
					if ($rownum>0){
						foreach($product_feedback as $feedback) { 
							$pimg = 'dummyproductimage.jpg';
							$pimg_arr = array_filter(explode(',', $feedback['image']));
							if (count($pimg_arr)>0){
								foreach ($pimg_arr as $pimg_row){
									if (file_exists('images/product/'.$pimg_row)){
										$pimg = $pimg_row;break;
									}
								}
							}
							$thumb_img = 'user-thumb1.png';
							if($feedback['thumbnail']!=''){
								$thumb_img = $feedback['thumbnail'];
							}
							$total = $total+$feedback['rating'];?>
							<div class="tabbed_review">
							<div class="tabbed_left">
							<a href="user/<?php echo $feedback['user_name']; ?>"><img src="images/users/<?php echo $thumb_img; ?>" width="30px" height="30px" /></a>
							<span><?php if($this->lang->line('reviewed_by') != '') { echo stripslashes($this->lang->line('reviewed_by')); } else echo "Reviewed By"; ?></span>
							<p><a href="user/<?php echo $feedback['user_name']; ?>"><?php echo $feedback['full_name']; ?></a></p>
							</div>
							<div class="tabbed_right">
							<div class="tabbed_top">
							<div class="rating_star">
							<div class="rat_star1" style="width:<?php echo $feedback['rating']*20; ?>%"></div>
							</div>
							<span class="date"><?php echo date("M d Y", strtotime($feedback['dateAdded'])); ?></span>
							</div>    
							<span class="tab_rev_title"><?php echo $feedback['title']; ?></span>
							<a style="float: left;margin: 0px 0 0 20px;width: 30px;" href="things/<?php echo $feedback['product_id']; ?>/<?php echo url_title($feedback['product_name'],'-');?>">
							<img src="images/product/<?php echo $pimg; ?>" width="30px" />
							</a>
							<span class="tab_rev_txt"><?php echo $feedback['description']; ?> </span>
							</div>
							</div>
				<?php }}else{?>
					<ol class="stream">
						<li style="width: 100%;"><p class="noproducts"><?php if($this->lang->line('store_review_no_more') != '') { echo stripslashes($this->lang->line('store_review_no_more')); } else echo "No Reviews available"; ?></p></li>
					</ol>
				<?php }?>
				</div>
			</div>
			<?php 
			if($this->uri->segment(3,0) == 'products' || $this->uri->segment(3,0) == ''){
				$this->load->view('site/store/store_sidebar1');
			}
			?>
			<?php 
		     $this->load->view('site/templates/footer_menu');
		     ?>
			<a href="#header" id="scroll-to-top"><span><?php if($this->lang->line('signup_jump_top') != '') { echo stripslashes($this->lang->line('signup_jump_top')); } else echo "Jump to top"; ?></span></a>
					
		</div>
	</div>
</div>
<?php 
$this->load->view('site/templates/footer',$this->data);
?>
