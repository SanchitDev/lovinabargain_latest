<div id="sidebar">
	<dl class="set_menu">
		<dt><?php if($this->lang->line('header_store_setting') != '') { echo stripslashes($this->lang->line('header_store_setting')); } else echo "STORE"; ?></dt>
		<dd><a href="seller/dashboard/profile" <?php if ($this->uri->segment(1)=='seller' && $this->uri->segment(3)=='profile' || $this->uri->segment(3)==''){?>class="current"<?php }?>><i class="ic-user"></i> <?php if($this->lang->line('referrals_profile') != '') { echo stripslashes($this->lang->line('referrals_profile')); } else echo "Profile"; ?></a></dd>
		<dd><a href="seller/dashboard/privacy" <?php if ($this->uri->segment(3)=='privacy'){?>class="current"<?php }?>><i class="ic-pre"></i> <?php if($this->lang->line('terms_condition') != '') { echo stripslashes($this->lang->line('terms_condition')); } else echo "Terms and Conditions"; ?></a></dd>
		<dd><a href="seller/dashboard/about" <?php if ($this->uri->segment(3)=='about'){?>class="current"<?php }?>><i class="ic-group"></i> <?php if($this->lang->line('store_about') != '') { echo stripslashes($this->lang->line('store_about')); } else echo "About"; ?></a></dd>
		<dd><a href="seller/dashboard/products" <?php if ($this->uri->segment(3)=='products'){?>class="current"<?php }?>><i class="ic-group"></i> <?php if($this->lang->line('store_products') != '') { echo stripslashes($this->lang->line('store_products')); } else echo "Products"; ?></a></dd>
		<dd><a href="seller/dashboard/orders" <?php if ($this->uri->segment(3)=='orders'){?>class="current"<?php }?>><i class="ic-group"></i> <?php if($this->lang->line('store_orders') != '') { echo stripslashes($this->lang->line('store_orders')); } else echo "Orders"; ?></a></dd>
	</dl>
</div>