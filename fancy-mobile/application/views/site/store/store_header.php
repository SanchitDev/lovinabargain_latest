<link rel="stylesheet" href="css/site/<?php echo SITE_COMMON_DEFINE ?>timeline.css" type="text/css" media="all"/>
<link rel="stylesheet" media="all" type="text/css" href="css/site/<?php echo SITE_COMMON_DEFINE ?>setting.css">	
<link rel="stylesheet" media="all" type="text/css" href="css/site/my-account.css">	
<link rel="stylesheet" media="all" type="text/css" href="css/site/<?php echo SITE_COMMON_DEFINE ?>list-page.css"/>
<link rel="stylesheet" media="all" type="text/css" href="css/site/<?php echo SITE_COMMON_DEFINE ?>profile.css"/>
<?php
$this->load->view('site/store/store_about');
$this->load->view('site/store/store_privacy');
$this->load->view('site/store/store_contact');
?>
<!-- Section_start -->
<div class="lang-en list winOS">
	<div id="container-wrapper">
		<div class="container timeline normal set_area usersection storepage">
			<div id="summary" class="wrapper-content">
			<?php if($user_Details->num_rows() == 1){
				$bannerimg = 'banner-dummy.jpg';
				$logoimg ='dummy-logo.png';
				if($user_Details->row()->cover_image !=''){
					$bannerimg = $user_Details->row()->cover_image;
				}
				if($user_Details->row()->logo_image !=''){
					$logoimg = $user_Details->row()->logo_image;
				}
				 if($user_Details->row()->store_name !=''){
					$username = $user_Details->row()->store_name;
				}else if ($user_Details->row()->full_name !=''){
					$username = $user_Details->row()->full_name;
				}else {
					$username = $user_Details->row()->user_name;
				}
				$followClass = 'follow';
				$followText = 'Follow';
				 if($this->lang->line('onboarding_follow') != '') { 
					$followText = $this->lang->line('onboarding_follow'); 
				}
				if ($loginCheck != ''){
					$followingListArr = explode(',', $user_Details->row()->followers);
					//print_r($followingListArr); die;
					if (in_array($loginCheck, $followingListArr)){
						$followClass = 'following';
						$followText = 'Following';
						if($this->lang->line('display_following') != '') { 
							$followText = $this->lang->line('display_following'); 
						}
					}
				} 
			?>
				<div class="inner-wrapper">
					<div class="icon-cache"></div>
					<div class="cover-image">
						<div class="image">
							<img width="930" height="240" src="<?php echo base_url();?>images/store/<?php echo $bannerimg;?>" alt="<?php echo $username;?>">
						</div>
					</div>
					<div class="info">
						<div class="logo-image">
							<img width="150" height="150" src="<?php echo base_url();?>images/store/<?php echo $logoimg;?>" alt="<?php echo $username;?>">
						</div>
						<h3 style="width:auto;">
						<?php echo $username;?>
						</h3>
						<div class="rating_star" style="float:left; margin-left:200px !important; margin-top:7px;">
							<?php foreach($all_feedback as $feedbacks) {  $totals = $totals+$feedbacks['rating']; }  $totalratingstars = $totals/count($all_feedback);  ?>
							<div class="rat_star1" style="width:<?php echo round($totalratingstars) * 20; ?>%"></div>
							<div style="margin-left:95px;">(<?php  echo $rownum = count($all_feedback); ?>)</div>			
                        </div>
                        <p style="padding-bottom: 0;"><?php echo $user_Details->row()->city;?>, <?php echo $user_Details->row()->state;?>, <?php echo $user_Details->row()->country;?>.</p>
						<p><?php echo $user_Details->row()->followers_count?> Followers - <?php echo $user_Details->row()->following_count?> <?php echo 'Following';?></p>
						<div class="store-follow">
							<ul class="actions">
								<li>
									<a uid="<?php echo $user_Details->row()->id;?>" <?php if ($loginCheck==''){?>require_login="true"<?php }?> class="follow follow-user-link btn <?php echo $followClass;?>"><i class="icon"></i><?php echo $followText; ?></a>
								</li>
							</ul>
							 <div class="setting">
							<div class="trick" onClick="$(this).parents('.setting').removeClass('opened');"></div>
							<a href="#" class="btns-gray-embo btn-setting" onClick="$(this).parents('.setting').toggleClass('opened');return false;"><i class="icon ic-cok"></i></a>
							<div class="menu-content">
							<?php 
							$ref_link = '';
							if ($loginCheck != ''){
								$ref_link = '?ref='.$userDetails->row()->user_name;
							}
							?>
								<ul>
									<li><a href="<?php echo base_url();?>store/<?php echo $user_Details->row()->user_name.$ref_link;?>" username="<?php echo $username;?>" <?php if ($loginCheck == ''){?>require_login="true"<?php }?> class="btn-user-share"><?php if($this->lang->line('display_share_prof') != '') { echo stripslashes($this->lang->line('display_share_prof')); } else echo "Share Profile"; ?></a></li>
									<?php if ($loginCheck != '' && $user_Details->row()->user_name==$userDetails->row()->user_name){?>
									<li><a href="seller/dashboard" class=""><?php if($this->lang->line('onboarding_dashboard') != '') { echo stripslashes($this->lang->line('onboarding_dashboard')); } else echo "Dashboard";?></a></li>
									<?php }?>
								</ul>
							</div>
						</div>
						</div>
					</div>
					<div class="store-description">
						<?php echo $user_Details->row()->tagline; ?>
					</div>
				</div>
				
			<?php } ?>
			</div>