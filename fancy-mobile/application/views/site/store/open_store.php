<?php
$this->load->view('site/templates/header');
$paypalProcess = unserialize($paypal_ipn_settings['settings']); 
?>
<style type="text/css" media="screen">
#edit-details {color:#FF3333; font-size:11px;}
.option-area select.option { border:1px solid #D1D3D9; border-radius:3px; box-shadow:1px 1px 1px #EEEEEE;
    height:22px; margin:5px 0 12px; }
a.selectBox.option { margin:5px 0 10px; padding:3px 0;}
a.selectBox.option .selectBox-label { font:inherit !important; padding-left:10px;}
form label.error{ color:red;}
.button{ width:95px; overflow:visible; margin:0; padding:8px 8px 10px 7px; border:0; color:#fff; background:#588cc7;
	border-radius:4px; font-weight:bold; font-size:15px; line-height:22px; text-align:center;}
.button:hover{ background: #3e73b7; }
</style>
<div class="lang-en wider no-subnav thing signed-out winOS">
<div id="container-wrapper">
<div class="container ">
<?php if($flash_data != '') { ?>
	<div class="errorContainer" id="<?php echo $flash_data_type;?>">
		<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
		<p><span><?php echo $flash_data;?></span></p>
	</div>
<?php } ?>
<div class="wrapper-content">					
<div class="profile-list">                 
<div class="page-header padding_all15 margin_all0">
	<h2> <?php if($this->lang->line('store_front_payment') != '') { echo stripslashes($this->lang->line('store_front_payment')); } else echo "Store fees payment"; ?></h2>		 
</div>
<div class="box-content">
<section class="left-section min_height" style="height: 924px;">	
<div class="person-lists bs-docs-example">
<div class="tab-content border_right width_100 pull-left cart-list chept2" id="myTabContent" style="padding:0"> 
<div id="product_info" class="tab-pane active">
<div class="form_fields">
    <label><h4>Select your plan</h4></label>
    <input type="radio" name="plan_value" required checked value="<?php echo $this->config->item('storefront_fees_month'); ?>"><label><?php if($this->lang->line('product_need_pay') != '') { echo stripslashes($this->lang->line('product_need_pay')); } else echo "You need to pay"; ?> <?php echo $currencySymbol;?><?php echo $this->config->item('storefront_fees_month'); ?></b> <?php if($this->lang->line('store_up_fees') != '') { echo stripslashes($this->lang->line('store_up_fees')); } else echo "for store fees per month"; ?></label><br>
    <input type="radio" name="plan_value" required value="<?php echo $this->config->item('storefront_fees_year'); ?>"><label><?php if($this->lang->line('product_need_pay') != '') { echo stripslashes($this->lang->line('product_need_pay')); } else echo "You need to pay"; ?> <?php echo $currencySymbol;?><?php echo $this->config->item('storefront_fees_year'); ?></b> <?php if($this->lang->line('store_up_fees') != '') { echo stripslashes($this->lang->line('store_up_fees')); } else echo "for store fees per year"; ?></label>


        <ol class="cart-order-depth" style="position:relative;">
        <?php 
        $payMethodCount = 1;
        ?>
        <?php if ($paypal_ipn_settings['status'] == 'Enable'){?>
      <li class="depth1 current" id="dep1" style="background:none;"><span><?php echo $payMethodCount;?></span><a onclick="javascript:paypal();" class="current"><?php if($this->lang->line('checkout_paypal') != '') { echo stripslashes($this->lang->line('checkout_paypal')); } else echo "Paypal"; ?></a></li>
        <?php 
        $payMethodCount++;
        }
       // if ($authorize_net_settings['status'] == 'Enable'){
        ?>
      <!--<li class="depth2" id="dep2" style="background:none;"><span><?php// echo $payMethodCount;?></span><a onclick="javascript:creditcard();"><?php //if($this->lang->line('checkout_credit_card') != '') { echo stripslashes($this->lang->line('checkout_credit_card')); } else echo "Credit Card"; ?></a></li>-->
    <?php 
        //}
	    ?>
 	    </ol>
<div class="clear"></div>
		
<!--- Paypal subscription start ********************* -->
<div class="cart-payment-wrap card-payment new-card-payment" id="PaypalPay" style="display:<?php if ($paypal_ipn_settings['status'] == 'Enable'){echo 'block';}else {echo 'none';}?>;">
                      <script>
                      $(document).ready(function(){	$("#PaymentPaypalForm").validate();
					  
					  $.validator.addMethod("ValidZipCode", function( value, element ) {
								var result = this.optional(element) || value.length >= 3;
												if (!result) {
													return false;
												}
												else{
												return true;
												}
							}, "Please Enter the Correct ZipCode");
					  
					   });
					   </script> 
<form name="PaymentPaypalForm" id="PaymentPaypalForm" method="post" enctype="multipart/form-data" action="site/checkout/PaymentProcessStorefront"  autocomplete="off">
<input type="hidden" name="paypalmode" id="paypalmode" value="<?php echo $paypalProcess['mode']; ?>"  />
                    <input type="hidden" name="paypalEmail" id="paypalEmail" value="<?php echo $paypalProcess['merchant_email']; ?>"  />                        
						
						<div id="complete-payment">
						
	
							<div class="hotel-booking-left">
								<dl class="payment-personal">
                                    <dt><b><?php if($this->lang->line('checkout_billing_addr') != '') { echo stripslashes($this->lang->line('checkout_billing_addr')); } else echo "Billing Address"; ?></b> <small><?php if($this->lang->line('checkout_enter_bill') != '') { echo stripslashes($this->lang->line('checkout_enter_bill')); } else echo "Enter your billing address"; ?></small></dt>
                                   
									<dd>
                                    <label for="payment-personal-name-fst"><?php if($this->lang->line('header_name') != '') { echo stripslashes($this->lang->line('header_name')); } else echo "Name"; ?> <b>*</b></label>
						<input name="full_name" id="full_name" type="text" class="required" value="<?php echo $userDetails->row()->full_name; ?>" />
										</dd>
										<dd>
                                        <label for="payment-adds-1"><?php if($this->lang->line('shipping_address_comm') != '') { echo stripslashes($this->lang->line('shipping_address_comm')); } else echo "Address"; ?> <b>*</b></label>

						<input id="address" name="address" type="text" class="required" value="<?php echo $userDetails->row()->address; ?>">

										</dd>
                                        <dd>
                                        <label for="payment-adds-1"><?php if($this->lang->line('shipping_address_comm') != '') { echo stripslashes($this->lang->line('shipping_address_comm')); } else echo "Address"; ?> 2</label>

						<input id="address2" name="address2" type="text" class="" value="<?php echo $userDetails->row()->address2; ?>">

										</dd>
										<dd>
                                        <label for="payment-city"><?php if($this->lang->line('header_city') != '') { echo stripslashes($this->lang->line('header_city')); } else echo "City"; ?> <b>*</b></label>
						<input id="city" name="city" type="text" class="required" value="<?php echo $userDetails->row()->city; ?>">
										</dd>
										
									</dl>
									<dl class="payment-card">
                                        <dt><b>&nbsp;</b> <small>&nbsp;</small></dt>
										
										<dd>
                                        <label for="payment-state"><?php if($this->lang->line('checkout_state') != '') { echo stripslashes($this->lang->line('checkout_state')); } else echo "State"; ?> <b>*</b></label>
						<input id="state" name="state" type="text" class="required" value="<?php echo $userDetails->row()->state; ?>">
										</dd>
                                        <dd>
                                        <label for="payment-state"><?php if($this->lang->line('header_country') != '') { echo stripslashes($this->lang->line('header_country')); } else echo "Country"; ?> <b>*</b></label>
						<select id="country" name="country" class="select-round select-white select-country selectBox required">
							<option value="">-------------------- <?php if($this->lang->line('checkout_select') != '') { echo stripslashes($this->lang->line('checkout_select')); } else echo "SELECT"; ?> --------------------</option>											
                                            <?php foreach($countryList->result() as $cntyRow){ ?>	
							<option value="<?php echo $cntyRow->country_code; ?>" <?php if($cntyRow->country_code == $userDetails->row()->country){ echo 'selected="selected"';} ?> ><?php echo $cntyRow->name; ?></option>
                                            <?php } ?>    
											</select>
										</dd>
										<dd>
                                    <label for="payment-zipcode"><?php if($this->lang->line('checkout_zip_code') != '') { echo stripslashes($this->lang->line('checkout_zip_code')); } else echo "Zip Code"; ?> <b>*</b></label> 
				<input id="postal_code" name="postal_code" type="text" class="required ValidZipCode" value="<?php echo $userDetails->row()->postal_code; ?>">
								</dd>
                                <dd>
                                    <label for="payment-phone"><?php if($this->lang->line('checkout_phone_no') != '') { echo stripslashes($this->lang->line('checkout_phone_no')); } else echo "Phone No"; ?> <b>*</b></label> 
				<input id="phone_no" name="phone_no" type="text" class="required number" value="<?php echo $userDetails->row()->phone_no; ?>">
								</dd>
								</dl>
                                <div class="hotel-booking-noti"><big><?php if($this->lang->line('checkout_secure_trans') != '') { echo stripslashes($this->lang->line('checkout_secure_trans')); } else echo "Secure Transaction"; ?></big><?php if($this->lang->line('checkout_ssl') != '') { echo stripslashes($this->lang->line('checkout_ssl')); } else echo "SSL Encrypted transaction powered by"; ?> <?php echo $siteTitle;?></div>
		</div>

		<div class="cart-payment">	
<!--			<dl class="cart-coupon">
				<dt><?php //if($this->lang->line('coupon_codes') != '') { echo stripslashes($this->lang->line('coupon_codes')); } else echo "Coupon Code";?></dt>
                <dd class="hastext"><input id="is_coupon" style="width:150px;" name="is_coupon" class="text coupon-code" placeholder="<?php //if($this->lang->line('have_coupon_code') != '') { echo stripslashes($this->lang->line('have_coupon_code')); } else echo "Have a coupon code?";?>" data-sid="616001" type="text">
				<input id="CheckCodeButton" type="button" class="btn-blue-apply apply-coupon" onclick="javascript:checkCodeSubscription();" value="<?php //if($this->lang->line('apply') != '') { echo stripslashes($this->lang->line('apply')); } else echo "Apply";?>" style="cursor:pointer;float:left;margin-left:5px;"></dd>
				<dd><span id="CouponErr" style="color:#FF0000;"></span></dd>
                
			</dl>							-->
			<dl class="cart-payment-order">
				<dt><?php if($this->lang->line('checkout_order') != '') { echo stripslashes($this->lang->line('checkout_order')); } else echo "Order"; ?></dt>
				<dd>
					<ul>
						<li class="first">
							<span class="order-payment-type"><?php if($this->lang->line('checkout_item_total') != '') { echo stripslashes($this->lang->line('checkout_item_total')); } else echo "Item total"; ?></span>
							<span class="order-payment-usd"><b><?php echo $currencySymbol;?><span id="sAmtVal"><?php echo number_format($this->config->item('storefront_fees_month'),2); ?></span></b> <?php echo $currencyType;?></span>
						</li>
						<li class="first" id="disAmtValDiv" style="display: none;">
							<span class="order-payment-type"><?php if($this->lang->line('checkout_discount') != '') { echo stripslashes($this->lang->line('checkout_discount')); } else echo "Discount"; ?></span>
							<span class="order-payment-usd"><b><?php echo $currencySymbol;?><span id="disAmtVal">0.00</span></b> <?php echo $currencyType;?></span>
						</li>
						<li class="total">
							<span class="order-payment-type"><b><?php if($this->lang->line('purchases_total') != '') { echo stripslashes($this->lang->line('purchases_total')); } else echo "Total"; ?></b></span>
							<span class="order-payment-usd"><b><?php echo $currencySymbol;?><span id="gAmtVal"><?php echo number_format($this->config->item('storefront_fees_month'),2); ?></span></b> <?php echo $currencyType;?></span>
						</li>
					</ul>
				</dd>
			</dl>
		</div>
		<input id="total_price" name="total_price" value="<?php echo $this->config->item('storefront_fees_month'); ?>" type="hidden">
<!--		<input name="discount_Amt" id="discount_Amt" value="0" type="hidden">
		<input name="CouponCode" id="CouponCode" value="" type="hidden">
		<input name="Coupon_id" id="Coupon_id" value="0" type="hidden">
		<input name="couponType" id="couponType" value="" type="hidden">-->
		<input id="email" name="email" value="<?php echo $userDetails->row()->email; ?>" type="hidden">
		<input id="invoiceNumber" name="invoiceNumber" value="<?php echo $this->session->userdata('InvoiceNo'); ?>" type="hidden"> 
		<input name="PaypalSubmit" id="PaypalSubmit" class="button-complete" type="submit" value="<?php if($this->lang->line('checkout_comp_pay') != '') { echo stripslashes($this->lang->line('checkout_comp_pay')); } else echo "Complete Payment"; ?>" style="cursor:pointer;"  />
		<div class="waiting"><?php if($this->lang->line('checkout_processing') != '') { echo stripslashes($this->lang->line('checkout_processing')); } else echo "Processing"; ?>...</div>
		<div class="card-payment-foot"><?php if($this->lang->line('checkout_by_place') != '') { echo stripslashes($this->lang->line('checkout_by_place')); } else echo "By placing your order, you agree to the Terms"; ?> &amp; <?php if($this->lang->line('checkout_codtn_privacy') != '') { echo stripslashes($this->lang->line('checkout_codtn_privacy')); } else echo "Conditions and Privacy Policy"; ?>.</div>


						</div>
                         
                        </form> 
					</div>
<!--- Paypal subscription End ********************* -->

<!--- Credit cart payment start ********************* -->
<div style="margin-top: 20px !important;display:none;" class="cart-payment-wrap card-payment new-card-payment" id="CreditCardPay" > 
 <script>$(document).ready(function(){	$("#PaymentCreditForm").validate();
$.validator.addMethod("ValidZipCode", function( value, element ) {
	var result = this.optional(element) || value.length >= 3;
		if (!result) {
			return false;
		}
		else{
		return true;
		}
	}, "Please Enter the Correct ZipCode");
});
</script> 
<form name="PaymentCreditForm" id="PaymentCreditForm" method="post" enctype="multipart/form-data" action="site/checkout/PaymentCreditStorefront" autocomplete="off">
	<input type="hidden" name="store_payment" id="store_payment" value="Pending"  />
	<div id="complete-payment cart-list">
		<div class="hotel-booking-left">
			<dl class="payment-personal">
				<dt><b><?php if($this->lang->line('checkout_cc_info') != '') { echo stripslashes($this->lang->line('checkout_cc_info')); } else echo "Credit Card Information"; ?></b> <small><?php if($this->lang->line('checkout_visa_mster') != '') { echo stripslashes($this->lang->line('checkout_visa_mster')); } else echo "Visa, MasterCard, Discover or American Express"; ?></small></dt>
				<dd class="comment"><b>*</b>  = <?php if($this->lang->line('checkout_mand_fields') != '') { echo stripslashes($this->lang->line('checkout_mand_fields')); } else echo "Mandatory fields"; ?></dd>
				<dd>
					<label for="payment-personal-name-fst"><?php if($this->lang->line('header_firstname') != '') { echo stripslashes($this->lang->line('header_firstname')); } else echo "First Name"; ?> <b>*</b></label>
					<input name="full_name" id="full_name" type="text" class="required" value="<?php echo $userDetails->row()->full_name;?>" />
				</dd>
				<dd>
					<label for="payment-personal-name-fst"><?php if($this->lang->line('header_lastname') != '') { echo stripslashes($this->lang->line('header_lastname')); } else echo "Last Name"; ?> <b>*</b></label>
					<input name="last_name" id="last_name" type="text" class="required" value="" />
				</dd>
				<dd>
					<label for="payment-card-number"><?php if($this->lang->line('checkout_card_no') != '') { echo stripslashes($this->lang->line('checkout_card_no')); } else echo "Card Number"; ?> <b>*</b></label>
					<input id="cardNumber" name="cardNumber" class="required" maxlength="16" size="16" type="text">
					<p class="error"><?php if($this->lang->line('checkout_enter_cardno') != '') { echo stripslashes($this->lang->line('checkout_enter_cardno')); } else echo "Please enter valid card number"; ?>.</p>
				</dd>
				 <dd>
					<label for="payment-card-number"><?php if($this->lang->line('checkout_card_type') != '') { echo stripslashes($this->lang->line('checkout_card_type')); } else echo "Card Type"; ?> <b>*</b></label>
					<select id="cardType" name="cardType" class="select-round select-white select-country selectBox required">
						<option value="Amex">American Express</option>
						<option value="Visa">Visa</option>
						<option value="MasterCard">Master Card</option>
						<option value="Discover">Discover</option>
					</select>
					<p class="error"><?php if($this->lang->line('checkout_select_card') != '') { echo stripslashes($this->lang->line('checkout_select_card')); } else echo "Please select card"; ?>.</p>
				</dd>
				<dd>
					<label for="payment-card-expiration"><?php if($this->lang->line('checkout_exp_date') != '') { echo stripslashes($this->lang->line('checkout_exp_date')); } else echo "Expiration Date"; ?> <b>*</b></label>
					<?php $Sel ='selected="selected"';  ?>
					<select id="CCExpDay" name="CCExpDay" class="select-round select-white select-date selectBox required">
						<option value="01" <?php if(date('m')=='01'){ echo $Sel;} ?>>01</option>
						<option value="02" <?php if(date('m')=='02'){ echo $Sel;} ?>>02</option>
						<option value="03" <?php if(date('m')=='03'){ echo $Sel;} ?>>03</option>
						<option value="04" <?php if(date('m')=='04'){ echo $Sel;} ?>>04</option>
						<option value="05" <?php if(date('m')=='05'){ echo $Sel;} ?>>05</option>
						<option value="06" <?php if(date('m')=='06'){ echo $Sel;} ?>>06</option>
						<option value="07" <?php if(date('m')=='07'){ echo $Sel;} ?>>07</option>
						<option value="08" <?php if(date('m')=='08'){ echo $Sel;} ?>>08</option>
						<option value="09" <?php if(date('m')=='09'){ echo $Sel;} ?>>09</option>
						<option value="10" <?php if(date('m')=='10'){ echo $Sel;} ?>>10</option>
						<option value="11" <?php if(date('m')=='11'){ echo $Sel;} ?>>11</option>
						<option value="12" <?php if(date('m')=='12'){ echo $Sel;} ?>>12</option>
					</select>
					<select id="CCExpMnth" name="CCExpMnth" class="select-round select-white select-date selectBox required">
						<?php for($i=date('Y');$i< (date('Y') + 25);$i++){ ?>	
							<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
						<?php } ?>    
					</select>
				</dd>
				<dd>
					<label for="payment-card-security"><?php if($this->lang->line('checkout_security_code') != '') { echo stripslashes($this->lang->line('checkout_security_code')); } else echo "Security Code"; ?></label>
					<input style="width:63px;" id="payment-card-security" name="creditCardIdentifier" class="input-code required number" type="password">
					<a href="#" class="tooltip" onClick="$('.card-back').show();return false;"><?php if($this->lang->line('checkout_what_this') != '') { echo stripslashes($this->lang->line('checkout_what_this')); } else echo "What is this?"; ?></a>
					<p class="error"><?php if($this->lang->line('checkout_enter_sc') != '') { echo stripslashes($this->lang->line('checkout_enter_sc')); } else echo "Please enter security code"; ?>.</p>
					<dl class="card-back">
						<dt><?php if($this->lang->line('checkout_cvc_cvs') != '') { echo stripslashes($this->lang->line('checkout_cvc_cvs')); } else echo "Security Code (CVC or CVS)"; ?></dt>
						<dd>
							<img src="images/card-back.gif" alt="">
							<?php if($this->lang->line('checkout_last_three') != '') { echo stripslashes($this->lang->line('checkout_last_three')); } else echo "Last three digits on the back of your card is the CVC or CVV number"; ?>.<br>&nbsp;<br><?php if($this->lang->line('checkout_for_ameri') != '') { echo stripslashes($this->lang->line('checkout_for_ameri')); } else echo "For American Express there is a four digit code on the front"; ?>.
							<a href="#" onClick="$('.card-back').hide();return false;" title="Close"></a>
						</dd>
					</dl>
				</dd>
			</dl>
			<dl class="payment-card">
				<dt><b><?php if($this->lang->line('checkout_billing_addr') != '') { echo stripslashes($this->lang->line('checkout_billing_addr')); } else echo "Billing Address"; ?></b> <small><?php if($this->lang->line('checkout_billing_enter') != '') { echo stripslashes($this->lang->line('checkout_billing_enter')); } else echo "Enter your billing and shipping address"; ?></small></dt>
				<dd>
					<label for="payment-adds-1"><?php if($this->lang->line('shipping_address_comm') != '') { echo stripslashes($this->lang->line('shipping_address_comm')); } else echo "Address"; ?> <b>*</b></label>
					<input id="address" name="address" type="text" class="required" value="<?php echo $userDetails->row()->address;?>">
				</dd>
				 <dd>
					<label for="payment-adds-1"><?php if($this->lang->line('shipping_address_comm') != '') { echo stripslashes($this->lang->line('shipping_address_comm')); } else echo "Address"; ?> 2</label>
					<input id="address2" name="address2" type="text" class="" value="<?php echo $userDetails->row()->address2;?>">
				</dd>
				<dd>
					<label for="payment-city"><?php if($this->lang->line('header_city') != '') { echo stripslashes($this->lang->line('header_city')); } else echo "City"; ?> <b>*</b></label>
					<input id="city" name="city" type="text" class="required" value="<?php echo $userDetails->row()->city;?>">
				</dd>
				<dd>
					<label for="payment-state"><?php if($this->lang->line('checkout_state') != '') { echo stripslashes($this->lang->line('checkout_state')); } else echo "State"; ?> <b>*</b></label>
					<input id="state" name="state" type="text" class="required" value="<?php echo $userDetails->row()->state;?>">
				</dd>
				<dd>
					<label for="payment-state"><?php if($this->lang->line('header_country') != '') { echo stripslashes($this->lang->line('header_country')); } else echo "Country"; ?> <b>*</b></label>
					<select id="country" name="country" class="select-round select-white select-country selectBox required">
						<option value="">-------------------- <?php if($this->lang->line('checkout_select') != '') { echo stripslashes($this->lang->line('checkout_select')); } else echo "SELECT"; ?> --------------------</option>											
						<?php foreach($countryList->result() as $cntyRow){ ?>	
							<option value="<?php echo $cntyRow->country_code; ?>" <?php if($cntyRow->country_code == $userDetails->row()->country){ echo 'selected="selected"';} ?> ><?php echo $cntyRow->name; ?></option>
						<?php } ?>    
					</select>
				</dd>
				<dd>
					<label for="payment-zipcode"><?php if($this->lang->line('checkout_zip_code') != '') { echo stripslashes($this->lang->line('checkout_zip_code')); } else echo "Zip Code"; ?> <b>*</b></label> 
					<?php if($userDetails->row()->postal_code == '0'){?>
						<input id="postal_code" name="postal_code" type="text" class="required ValidZipCode" value="">
					<?php }else{ ?>
						<input id="postal_code" name="postal_code" type="text" class="required ValidZipCode" value="<?php echo $userDetails->row()->postal_code;?>">
					<?php }?>
				</dd>
				<dd>
					<label for="payment-phone"><?php if($this->lang->line('checkout_phone_no') != '') { echo stripslashes($this->lang->line('checkout_phone_no')); } else echo "Phone No"; ?> <b>*</b></label> 
					<input id="phone_no" name="phone_no" type="text" class="required number" value="<?php echo $userDetails->row()->phone_no;?>">
				</dd>
			</dl>
			<div class="hotel-booking-noti"><big><?php if($this->lang->line('checkout_secure_trans') != '') { echo stripslashes($this->lang->line('checkout_secure_trans')); } else echo "Secure Transaction"; ?></big><?php if($this->lang->line('checkout_ssl') != '') { echo stripslashes($this->lang->line('checkout_ssl')); } else echo "SSL Encrypted transaction powered by"; ?> <?php echo $siteTitle;?></div>
		</div>
		<div class="cart-payment">								
			<dl class="cart-payment-order">
				<dt><?php if($this->lang->line('checkout_order') != '') { echo stripslashes($this->lang->line('checkout_order')); } else echo "Order"; ?></dt>
				<dd>
					<ul>
						<li class="first">
							<span class="order-payment-type"><?php if($this->lang->line('checkout_item_total') != '') { echo stripslashes($this->lang->line('checkout_item_total')); } else echo "Item total"; ?></span>
                                                        <span class="order-payment-usd"><b><?php echo $currencySymbol;?><span><?php echo number_format($this->config->item('storefront_fees_month'),2); ?></span></b> <?php echo $currencyType;?></span>
						</li>
						<li class="total">
							<span class="order-payment-type"><b><?php if($this->lang->line('purchases_total') != '') { echo stripslashes($this->lang->line('purchases_total')); } else echo "Total"; ?></b></span>
							<span class="order-payment-usd"><b><?php echo $currencySymbol;?><span><?php echo number_format($this->config->item('storefront_fees_month'),2); ?></span></b> <?php echo $currencyType;?></span>
						</li>
					</ul>
				</dd>
			</dl>
		</div>
		<input id="total_price" name="total_price" value="<?php echo $this->config->item('storefront_fees'); ?>" type="hidden">
		<input id="email" name="email" value="<?php echo $userDetails->row()->email; ?>" type="hidden">
		<input id="invoiceNumber" name="invoiceNumber" value="<?php echo $this->session->userdata('InvoiceNo'); ?>" type="hidden">
		<input id="CreditSubmit" class="button-complete" type="submit" value="<?php if($this->lang->line('checkout_comp_pay') != '') { echo stripslashes($this->lang->line('checkout_comp_pay')); } else echo "Complete Payment"; ?>" style="cursor:pointer;"  />	
		<div class="waiting"><?php if($this->lang->line('checkout_processing') != '') { echo stripslashes($this->lang->line('checkout_processing')); } else echo "Processing"; ?>...</div>
		<div class="card-payment-foot"><?php if($this->lang->line('checkout_by_place') != '') { echo stripslashes($this->lang->line('checkout_by_place')); } else echo "By placing your order, you agree to the Terms"; ?> &amp; <?php if($this->lang->line('checkout_codtn_privacy') != '') { echo stripslashes($this->lang->line('checkout_codtn_privacy')); } else echo "Conditions and Privacy Policy"; ?>.</div>
	</div>
</form>
</div></div></div></div></div></section></div></div></div>
<?php //$this->load->view('site/templates/footer_menu');?>	
<a id="scroll-to-top" href="#header" style="display: none;"><span><?php if($this->lang->line('signup_jump_top') != '') { echo stripslashes($this->lang->line('signup_jump_top')); } else echo "Jump to top"; ?></span></a>
</div></div></div></div> 
<script src="js/site/<?php echo SITE_COMMON_DEFINE ?>filesjquery_zoomer.js" type="text/javascript"></script>
<script type="text/javascript" src="js/site/<?php echo SITE_COMMON_DEFINE ?>selectbox.js"></script>
<script type="text/javascript" src="js/site/thing_page.js"></script>
<script type="text/javascript" src="js/site/jquery.validate.js"></script>
<script> $("#sellerProdEdit1").validate(); </script>
<script>
    $(document).on('click','input[name="plan_value"]',function(){
        var amount = parseFloat($(this).val()).toFixed(2);
        $('.order-payment-usd b span').text(amount);
        //$('#gAmtVal').text(amount);
        $('input[name="total_price"]').val($(this).val());
    });
</script>
<?php //$this->load->view('site/templates/footer'); ?>