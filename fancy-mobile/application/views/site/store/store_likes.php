<?php 
$this->load->view('site/templates/header',$this->data);
$this->load->view('site/store/store_header',$this->data);
$this->load->view('site/store/store_sidebar'); 
?>
			<div class="store_rightbar">
				<div style="height: auto;" class="figure-row fancy-suggestions anim might_box">
				<?php if(isset($productLikeDetails) && $productLikeDetails->num_rows()>0 || $userProductLikeDetails->num_rows()>0){
					if($productLikeDetails->num_rows()>0){
					foreach($productLikeDetails->result() as $_productDetails){
						$is_exists = true;
			          	if ($_productDetails->user_name == '' && $_productDetails->user_id>0){
			          		$is_exists = false;
			          	}
			          	if ($is_exists){
							$img = 'dummyProductImage.jpg';
							$imgArr = explode(',', $_productDetails->image);
							if (count($imgArr)>0){
								foreach ($imgArr as $imgRow){
									if ($imgRow != ''){
										$img = $pimg = $imgRow;
										break;
									}
								}
							}
							$fancyClass = 'fancy';
							$fancyText = LIKE_BUTTON;
							if (count($likedProducts)>0 && $likedProducts->num_rows()>0){
								foreach ($likedProducts->result() as $likeProRow){
									if ($likeProRow->product_id == $_productDetails->seller_product_id){
										$fancyClass = 'fancyd';$fancyText = LIKED_BUTTON;break;
									}
								}
							}
							$puser_name = $_productDetails->user_name;
							if ($puser_name == ''){
								$puser_name = 'administrator';
							}
							if (isset($_productDetails->web_link)){
								$prod_url = base_url().'user/'.$puser_name.'/things/'.$_productDetails->seller_product_id.'/'.url_title($_productDetails->product_name,'-');
							}else {
								$prod_url = base_url().'things/'.$_productDetails->seller_product_id.'/'.url_title($_productDetails->product_name,'-');
							}
				?>
					<div class="figure-product figure-200 might_box_list">
						<a href="<?php echo $prod_url;?>">
							<figure>
								<span class="wrapper-fig-image">
									<span class="fig-image">
										<img style="width: 200px; height: 200px;" src="<?php echo base_url();?>images/product/<?php echo $img;?>">
									</span>
								</span>
								<figcaption><?php echo $_productDetails->product_name;?></figcaption>
							</figure>
						</a>
						<span class="username"><b class="price"><?php echo $currencySymbol;?><?php echo $_productDetails->sale_price;?></b></span>
						<a href="#" item_img_url="images/product/<?php echo $img;?>" tid="<?php echo $_productDetails->seller_product_id;?>" class="button <?php echo $fancyClass;?>" <?php if ($loginCheck==''){?>require_login="true"<?php }?>><span><i></i></span><?php echo $fancyText;?></a>
					</div>
				<?php }}}?>
				<?php 
					if($userProductLikeDetails->num_rows()>0){
					foreach($userProductLikeDetails->result() as $_productDetails){
					$img = 'dummyProductImage.jpg';
					$imgArr = explode(',', $_productDetails->image);
					if (count($imgArr)>0){
						foreach ($imgArr as $imgRow){
							if ($imgRow != ''){
								$img = $pimg = $imgRow;
								break;
							}
						}
					}
					$fancyClass = 'fancy';
					$fancyText = LIKE_BUTTON;
					if (count($likedProducts)>0 && $likedProducts->num_rows()>0){
							foreach ($likedProducts->result() as $likeProRow){
								if ($likeProRow->product_id == $_productDetails->seller_product_id){
									$fancyClass = 'fancyd';$fancyText = LIKED_BUTTON;break;
								}
							}
						}
					$puser_name = $_productDetails->user_name;
						if ($puser_name == ''){
							$puser_name = 'administrator';
						}
						if (isset($_productDetails->web_link)){
							$prod_url = base_url().'user/'.$puser_name.'/things/'.$_productDetails->seller_product_id.'/'.url_title($_productDetails->product_name,'-');
						}else {
							$prod_url = base_url().'things/'.$_productDetails->seller_product_id.'/'.url_title($_productDetails->product_name,'-');
						}
				?>
				<div class="figure-product figure-200 might_box_list">
						<a href="<?php echo $prod_url;?>">
							<figure>
								<span class="wrapper-fig-image">
									<span class="fig-image">
										<img style="width: 200px; height: 200px;" src="<?php echo base_url();?>images/product/<?php echo $img;?>">
									</span>
								</span>
								<figcaption><?php echo $_productDetails->product_name;?></figcaption>
							</figure>
						</a>
						<?php 
						if ($_productDetails->product_type=='prosperent'){
						?>
						<span class="username"><b class="price"><?php echo $currencySymbol;?><?php echo $_productDetails->sale_price;?></b></span>
						<?php }?>
						<a href="#" item_img_url="images/product/<?php echo $img;?>" tid="<?php echo $_productDetails->seller_product_id;?>" class="button <?php echo $fancyClass;?>" <?php if ($loginCheck==''){?>require_login="true"<?php }?>><span><i></i></span><?php echo $fancyText;?></a>
					</div>
				<?php }}}else{?>
					<ol class="stream">
						<li style="width: 100%;"><p class="noproducts"><?php if($this->lang->line('store_product_no_more') != '') { echo stripslashes($this->lang->line('store_product_no_more')); } else echo "No Products available"; ?></p></li>
					</ol>
				<?php }?>
				</div>
			</div>
			<?php 
			if($this->uri->segment(3,0) == 'products' || $this->uri->segment(3,0) == ''){
				$this->load->view('site/store/store_sidebar1');
			}
			?>
			<?php 
     $this->load->view('site/templates/footer_menu');
     ?>
		<a href="#header" id="scroll-to-top"><span><?php if($this->lang->line('signup_jump_top') != '') { echo stripslashes($this->lang->line('signup_jump_top')); } else echo "Jump to top"; ?></span></a>
			
		</div>
	</div>
</div>
<?php 
$this->load->view('site/templates/footer',$this->data);
?>
