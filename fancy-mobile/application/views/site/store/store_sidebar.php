<div id="sidebar">
	<dl class="set_menu">
		<?php
				if($liked_count!='' && $liked_count->num_rows()>0){
					$liked_count = $liked_count->num_rows();
				}else{
					$liked_count = "0";
				}
				?>
		<dd><a href="store/<?php echo $user_Details->row()->user_name;?>" <?php if ($this->uri->segment(1)=='store' && $this->uri->segment(3)== '' && $this->uri->segment(4)== ''){?>class="current"<?php }?>> <?php if($this->lang->line('store_display_products') != '') { echo stripslashes($this->lang->line('store_display_products')); } else echo "Products "; echo " ".$product_count->num_rows(); ?></a></dd>
<!-- 		<dd><a href="store/<?php echo $user_Details->row()->user_name;?>/liked" <?php if ($this->uri->segment(1)=='store' && $this->uri->segment(3)== 'liked'){?>class="current"<?php }?>> <?php if($this->lang->line('store_display_liked') != '') { echo stripslashes($this->lang->line('store_display_liked')); } else echo "Liked "; echo " ".$liked_count; ?></a></dd>
 -->		<dd><a href="store/<?php echo $user_Details->row()->user_name;?>/reviews" <?php if ($this->uri->segment(1)=='store' && $this->uri->segment(3)== 'reviews'){?>class="current"<?php }?>> <?php if($this->lang->line('store_display_reviews') != '') { echo stripslashes($this->lang->line('store_display_reviews')); } else echo "Reviews"; ?></a></dd>
		<dd><a class="display_about"> <?php if($this->lang->line('store_display_about') != '') { echo stripslashes($this->lang->line('store_display_about')); } else echo "About"; ?></a></dd>
		<dd><a class="display_privacy"> <?php if($this->lang->line('terms_condition') != '') { echo stripslashes($this->lang->line('terms_condition')); } else echo "Terms and Conditions"; ?></a></dd>
		<dd><a class="display_contact"> <?php if($this->lang->line('store_display_contact') != '') { echo stripslashes($this->lang->line('store_display_contact')); } else echo "Contact"; ?></a></dd>
	</dl>
</div>
<style>
#footer{position: relative !important;bottom: 0 !important;}
.timeline #sidebar{display:block !important;}
</style>
<script type="text/javascript">
function store_price(){
	max_price = document.getElementById('max_price').value;
	min_price = document.getElementById('min_price').value;
	id = 'p';
	if(max_price=='' && min_price==''){
		value= '-1';
	}else{
		value = min_price+'-'+max_price;
	}
	search_value(id,value);
}
function search_value(id,value){
	var range = value;
	var cur_url = '<?php echo current_url()?>';
	var args = $.extend({}, location.args), query;
	var cur_arg = id;
	if(cur_arg == 'sort_by_price'){
		if(range != '-1'){
			args.sort_by_price = range;
		} else {
			delete args.sort_by_price;
		}
	}else if(cur_arg == 'is'){
		if($('#'+id).is(':checked')){
			args.is = range;
		} else {
			delete args.is;
		}
	}else if(cur_arg == 'p'){
		if(range != '-1'){
			args.p = range;
		} else {
			delete args.p;
		}
	}else if(cur_arg.charAt(0) == 'c'){
		if($('#'+id).is(':checked')){
			args.c = range;
		} else {
			delete args.c;
		}
	}else if(cur_arg == 'q'){
		if(range != ''){
			args.q = range;
		} else {
			delete args.q;
		}
	}
	if(query = $.param(args)) cur_url += '?'+query;
	window.location.href = cur_url;
}
$('.store-follow ul li .follow').click(function() {
        var uid = $(this).attr('uid');
        var $this = $(this), login_require = $this.attr('require_login');
        if (typeof(login_require) != undefined && login_require === 'true')  return require_login();
        var param = {};
        param['user_id']=uid;
        var btn = $(this);
        if (btn.hasClass('following')) {
            $.post(baseURL+"site/user/delete_follow",param,
                function(json){
                    if (json.status_code==1) {
                       btn.html('<i class="icon"></i>'+lg_follow);
                       btn.removeClass('following');
                    }
                }, "json");
        } else {
            $.post(baseURL+"site/user/add_follow",param,
                function(json){
                    if (json.status_code==1) {
                       btn.html('<i class="icon"></i>'+lg_following);
                       btn.addClass('following');
                    }
                }, "json");
        }
});
</script>