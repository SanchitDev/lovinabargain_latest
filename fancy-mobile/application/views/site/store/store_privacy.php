<?php if($this->uri->segment(1,0)=='store'){ 
	if ($user_Details!=''){
	if ($user_Details->num_rows()>0){
	?>
<div style="display:none;">
<div class="popup display-privacy ly-title" id="store_privacy_popup">
	<p class="ltit" ><?php if($this->lang->line('terms_condition') != '') { echo stripslashes($this->lang->line('terms_condition')); } else echo "Terms and Conditions"; ?></p>
	<div class="ltxt">
		<p class="figcaption">
		<?php if($user_Details->row()->store_name!=''){
			echo $user_Details->row()->store_name;
		}else{
			echo $user_Details->row()->user_name;
		}?></p>
		<div class="inner-ltxt">
			<?php if($user_Details->row()->privacy!=''){?>
				<?php echo $user_Details->row()->privacy;?>
			<?php } else{ ?>
				<?php if($this->lang->line('store_details_no_more') != '') { echo stripslashes($this->lang->line('store_details_no_more')); } else echo "No Details available";?>
			<?php } ?>
		</div>
	</div>
	<button title="Close" class="ly-close"><i class="ic-del-black"></i></button>
</div>
</div>
<?php }}} ?>
<script type="text/javascript"> 
$(document).ready(function(){
    //var popup = $.dialog('display-privacy');
//	$('.display_privacy').click(function(event){
//	    popup.open();
//		event.preventDefault();
//	});
        $(".display_privacy").colorbox({width:"50%", height:"auto", inline:true, href:"#store_privacy_popup"});
});
</script>
<style type="text/css">
#store_privacy_popup .display-privacy{ width: 0px !important; margin-top: 0px !important;}
#store_privacy_popup .ltxt .figcaption{margin:10px 0px 0px 20px; font-weight:bold; font-size:16px;}
#store_privacy_popup.display-privacy .ltxt { position: relative; border-radius: 0; background: #fff; color: #393d4d; float:left; width:100%; padding:0px; min-height:230px;}
#store_privacy_popup .ltxt .inner-ltxt{margin:10px 20px ; color: #4D515B; font-size: 13px; line-height: 20px;
    padding-bottom: 15px; text-align:justify;}
</style>