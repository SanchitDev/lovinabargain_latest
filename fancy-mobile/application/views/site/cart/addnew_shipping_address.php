<?php 
$this->load->view('site/templates/header');?>

<?php if($flash_data != '') { ?>
		<div class="errorContainer" id="<?php echo $flash_data_type;?>">
			<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
            
			<p><span><?php echo $flash_data;?></span></p>
		</div>
		<?php } ?>
	<div id="content" class="account-page shipping-addr-hide">
        <div class="account-wrap" style="border: 1px solid #BCBFC3;">

        <dl class="profile-account">
        
         <form class="ltxt" action="site/cart/insert_shipping_address" method="post" name ="cartForm" id="cartForm" onsubmit="return cart_validation()">
        <dd>
       
        <p class="effete">
        <label class="label"><?php if($this->lang->line('shipping_add_ship') != '') { echo stripslashes($this->lang->line('shipping_add_ship')); } else echo "Add Shipping Address"; ?></label><br /></p>
                
        <p class="name-account"><label class="label"><?php if($this->lang->line('signup_full_name') != '') { echo stripslashes($this->lang->line('signup_full_name')); } else echo "Full Name"; ?></label>
		<input id="full_name" name="full_name" class="popup_add_input" type="text" >
        </p>
        
        <p class="name-account"><label class="label"><?php if($this->lang->line('shipping_nickname') != '') { echo stripslashes($this->lang->line('shipping_nickname')); } else echo "Nickname"; ?></label>
		<input id="nick_name" name="nick_name" class="popup_add_input width180" type="text" placeholder="<?php if($this->lang->line('header_home_work') != '') { echo stripslashes($this->lang->line('header_home_work')); } else echo "E.g. Home, Work, Aunt Jane"; ?>">
		</p>
        
            <p class="name-account"><label class="label"><?php if($this->lang->line('shipping_address_comm') != '') { echo stripslashes($this->lang->line('shipping_address_comm')); } else echo "Address"; ?></label>
		<input class="popup_add_input" id="address1" name="address1" type="text" placeholder="<?php if($this->lang->line('header_address1') != '') { echo stripslashes($this->lang->line('header_address1')); } else echo "Address Line1"; ?>">
		<input class="popup_add_input" id="address2" name="address2" type="text" placeholder="<?php if($this->lang->line('header_address2') != '') { echo stripslashes($this->lang->line('header_address2')); } else echo "Address Line2"; ?>">
		</p>
    
          <p class="name-account"><label class="label"><?php if($this->lang->line('header_city') != '') { echo stripslashes($this->lang->line('header_city')); } else echo "City"; ?></label>
		<input class="popup_add_input width180" name="city" type="text" id="city">
		</p>
        <p class="name-account"><label class="label"><?php if($this->lang->line('header_zip_postal') != '') { echo stripslashes($this->lang->line('header_zip_postal')); } else echo "Zip / Postal Code"; ?></label>
		<input name="postal_code" class="popup_add_input width108" id="postal_code" type="text">
		</p>
         <p class="name-account"><label class="label"><?php if($this->lang->line('header_state_province') != '') { echo stripslashes($this->lang->line('header_state_province')); } else echo "State / Province"; ?></label>
		<input class="popup_add_input width108" name="state" type="text" id="state">
		</p>
        
        <p class="name-account"><label class="label"><?php if($this->lang->line('header_country') != '') { echo stripslashes($this->lang->line('header_country')); } else echo "Country"; ?></label>
		<select name="country" class="full required">
						<?php 
						if ($countryList->num_rows()>0){
							foreach ($countryList->result() as $country){
						?>
						<option value="<?php echo $country->country_code;?>"><?php echo $country->name;?></option>
						<?php 
							}
						}
						?>
					</select>
		</p>
      
       <p class="name-account"><label class="label"><?php if($this->lang->line('header_telephone') != '') { echo stripslashes($this->lang->line('header_telephone')); } else echo "Telephone"; ?></label>
              <input class="popup_add_input width180" name="phone" type="text" id="phone">
		</p><br>
        <p class="effete">
        <label class="radio-button" for="men"><input name="set_default" id="make_this_primary_addr" value="true" type="checkbox"><?php if($this->lang->line('header_make_primary') != '') { echo stripslashes($this->lang->line('header_make_primary')); } else echo "Make this my primary shipping address"; ?></label>
        
        </p>
        
               </dd></dl>   
        
    <div class="address-frm">
        <div class="btn-wrap">
<div class="btn-area">
       
           <button type="submit" class="btn-blue btn-save"><?php if($this->lang->line('header_save') != '') { echo stripslashes($this->lang->line('header_save')); } else echo "Save"; ?></button>
            <button type="reset" class="btn-gray btn-cancel"><?php if($this->lang->line('header_cancel') != '') { echo stripslashes($this->lang->line('header_cancel')); } else echo "Cancel"; ?></button>
           
           </div>
        </div>
        </div>
           
           <input type="hidden" class="ship_id" name="ship_id" value="0"/>
			<input type="hidden" name="user_id" value="<?php echo $loginCheck;?>"/>
		</form>
      	</div>
     
        </div>
<style>
.address-frm{
width:100%;
}
.address-frm .btn-area button{ font-size: 1.1em;
    height: 2.65em;
    line-height: 2.6em;
    width: 49.5%;}
.btn-gray {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background: -moz-linear-gradient(center top , #FAFAFA, #F0F0F0) repeat scroll 0 0 rgba(0, 0, 0, 0);
    border-color: #C3C3C3 #BEBEBE #B6B6B6;
    border-image: none;
    border-radius: 0.15em;
    border-style: solid;
    border-width: 1px;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
    color: #4C4F53;
    display: inline-block;
    font-size: 0.94em;
    font-weight: bold;
    line-height: 2.45em;
    padding: 0 1em;
    text-shadow: 0 1px 0 #FFFFFF;}
.user-image{background: none repeat scroll 0 0 #FFFFFF; border: 1px solid #E5E5E5; position: relative; width: 10%; height:130px;  margin: 0 auto;}
.user-image .hidden {display: block !important; height: 150px; opacity: 0; position: relative; width: 100%; z-index: 1;}
.user-image #img_prev{width:131px; height:132px;}
</style>



<script>

function cart_validation(){

var full_name=document.forms["cartForm"]["full_name"].value;
var nick_name=document.forms["cartForm"]["nick_name"].value;
var address1=document.forms["cartForm"]["address1"].value;
var city=document.forms["cartForm"]["city"].value;
var state=document.forms["cartForm"]["state"].value;
var postal_code=document.forms["cartForm"]["postal_code"].value;
var phone=document.forms["cartForm"]["phone"].value;
if (full_name==null || full_name=="")
  {
  alert("Please Enter your Full Name");
  return false;
  }
  else if(nick_name==""){
     alert("Please enter the shipping Nickname");
     return false;
  }
  else if(address1==null || address1==""){
     alert("Please Enter your Address");
     return false;
  }
  else if(city==null || city==""){
     alert("Please Enter your City");
     return false;
  }
  else if(postal_code==null || postal_code==""){
     alert("Please Enter your ZipCode");
     return false;
  }
  else if(state==null || state==""){
     alert("Please Enter your State");
     return false;
  }
   else if(isNaN(phone) || phone==""){
     alert("Please Enter your Phone no");
     return false;
  }
  
}
       
</script>