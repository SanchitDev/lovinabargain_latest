<?php $this->load->view('site/templates/header',$this->data); ?>

<div id="content" class="sameday-delivary">
<?php if($flash_data != '') { ?>
	<div class="errorContainer" id="<?php echo $flash_data_type;?>">
		<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
		<p><span><?php echo $flash_data;?></span></p>
	</div>
<?php } ?>
	<div class="color-container  mar-bot">
		<fieldset>
			<i class="ic-search"></i>
			<input id="search-sameday-zip" type="text" placeholder="Enter your postal code or city">
			<a class="del" href="#"></a>
				<button class="btn-blue"><?php echo site_lg('lg_search', 'Search');?></button>
				<div id="result"></div>
		</fieldset>
	</div>
	<div class="color-container  mar-bot sddlyhide">
        <h2 style="text-align:center" class="color-title sameday"><span class="icon"></span>
		<b><?php echo $siteTitle;?>&nbsp;<?php if($this->lang->line('same_day_delivery') != '') { echo stripslashes($this->lang->line('same_day_delivery')); } else echo "Same-Day Delivery"; ?></b>
		<small><?php if($this->lang->line('discover_same_day_delivery') != '') { echo stripslashes($this->lang->line('discover_same_day_delivery')); } else echo "Discover amazing products from local stores, hand-delivered to you the same day."; ?></small></h2>
		<dl>
			<dt><span class="icon local"></span><?php if($this->lang->line('find_product_near_u') != '') { echo stripslashes($this->lang->line('find_product_near_u')); } else echo "Find products near you."; ?></dt>
			<dd><?php if($this->lang->line('product_available_samedaydly') != '') { echo stripslashes($this->lang->line('product_available_samedaydly')); } else echo "Use Oliver to find products available for same-day delivery in your area."; ?></dd>
		</dl>   
		<dl>
			<dt><span class="icon timin"></span><?php if($this->lang->line('same_daydly_order_noon') != '') { echo stripslashes($this->lang->line('same_daydly_order_noon')); } else echo "Order by noon."; ?></dt>
			<dd><?php if($this->lang->line('samedaydly_order_timing_text') != '') { echo stripslashes($this->lang->line('samedaydly_order_timing_text')); } else echo "Place your order by 12pm, and we'll get you everything by 5pm."; ?></dd>
		</dl>     
		<dl>
			<dt><span class="icon rece"></span><?php if($this->lang->line('samedaydly_receive_time_text') != '') { echo stripslashes($this->lang->line('samedaydly_receive_time_text')); } else echo "Receive everything by 5pm."; ?></dt>
			<dd><?php if($this->lang->line('samedaydly_unbox_packages') != '') { echo stripslashes($this->lang->line('samedaydly_unbox_packages')); } else echo "Unbox your packages and enjoy your new Oliver things."; ?></dd>
		</dl>   
        
	</div>
	<div class="color-container  mar-bot viaemail sddlyhide">
        <h2 class="color-title"><?php if($this->lang->line('find_amazing_area_samedaydly') != '') { echo stripslashes($this->lang->line('find_amazing_area_samedaydly')); } else echo "Find amazing things in your area"; ?></h2>
		<ul class="same-day-list">
			<?php if($get_image!='' && $get_image->num_rows()>0){
			        $i=1;
			        foreach($get_image->result() as $images){
					$img = 'dummyProductImage.jpg';
					$imgArr = explode(',', $images->image);
					if (count($imgArr)>0){
						foreach ($imgArr as $imgRow){
							if ($imgRow != ''){
								$img = $pimg = $imgRow;
								break;
							}
					}}
					?>
					    <li class="img-<?php echo $i?>">
						  <a href="<?php echo base_url();?>same-day-delivery/">
							
						     <img src="<?php echo DESKTOPURL;?>images/product/<?php echo $img;?>" />
							 <?php $split_city = explode(',',$images->city)?>
							 <span><?php echo $split_city[0];?></span>
                          </a>						  
						</li>
					<?php $i++;
					}
			}?>
		</ul>
	</div>      
	<div class="color-container  mar-bot viaemail sddlyshow">
		<h2 class="color-title"><?php if($this->lang->line('available_cities') != '') { echo stripslashes($this->lang->line('available_cities')); } else echo "Available cities"; ?></h2>
		<p class="countrie"><?php if($this->lang->line('same_day_available_text') != '') { echo stripslashes($this->lang->line('same_day_available_text')); } else echo "Oliver Same-Day Delivery is currently available in the"; ?>
		<?php if($top_countries!='' && $top_countries->num_rows()>0){
			
		    foreach($top_countries->result() as $topcountries){?>
			     <!-- <i class="country-us"></i>-->
			      <b><?php echo $topcountries->name;?></b>,<?php }}?><?php echo site_lg('header_and', 'and');?>
          <?php if($this->lang->line('sameday_manymore') != '') { echo stripslashes($this->lang->line('sameday_manymore')); } else echo "many more."; ?>
        </p>
		<dl class="view">
			<?php $i=1; ?>
			<?php foreach($country_list->result() as $countries){?>
			<dt><a href="javascript:vold(0);" id="<?php echo $i;?>" onclick="get_cities(this.id);"><?php echo $countries->name;?><small><?php if($get_cities[$countries->name]->num_rows()>0){echo $get_cities[$countries->name]->num_rows();}else{echo 0;}?></small></a></dt>
			<dd class="sdly-city-name" id="city-<?php echo $i;?>">
			<?php if($get_cities[$countries->name]!='' && $get_cities[$countries->name]->num_rows()>0){
			foreach($get_cities[$countries->name]->result() as $rows){?>
			<em><a href="javascript:void(0);"><?php echo $rows->city_name;?></a><i>·</i></em>
			<?php }}?>
			</dd>
			<?php $i++;} ?>
		</dl>
		<strong><a class="more-cit" href="javascript:vold(0);"><?php echo site_lg('lg_see', 'See all the cities');?><i class="icon"></i></a></strong>
	</div>
	<div class="color-container  mar-bot viaemail sddlycfc"><dl>
		<dt class="color-title"><?php if($this->lang->line('cant_sameday_findcountry') != '') { echo stripslashes($this->lang->line('cant_sameday_findcountry')); } else echo "Can’t find your country?"; ?></dt>
		<dd>
            <a href="mailto:<?php echo $this->config->item('site_contact_mail')?>?subject=Add my city to same-day delivery"><?php if($this->lang->line('drop_us_sameday') != '') { echo stripslashes($this->lang->line('drop_us_sameday')); } else echo "Drop us a line"; ?></a>
            <?php if($this->lang->line('drop_us_sameday_text') != '') { echo stripslashes($this->lang->line('drop_us_sameday')); } else echo "and we’ll let you know when we’re available in your country."; ?>
		</dd></dl>
	</div>
</div>
