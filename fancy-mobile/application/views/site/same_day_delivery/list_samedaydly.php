<?php $this->load->view('site/templates/header'); ?>
<div id="content" class="sameday-delivary">
<script src="js/site/category-shoplist.js" type="text/javascript"></script>
<?php if($flash_data != '') { ?>
	<div class="errorContainer" id="<?php echo $flash_data_type;?>">
		<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
		<p><span><?php echo $flash_data;?></span></p>
	</div>
<?php } ?>
	<div class="color-container  mar-bot">
		<fieldset>
			<i class="ic-search"></i>
			<input id="search-sameday-zip" type="text" placeholder="Enter your postal code or city">
			<a class="del" href="#"></a>
				<button class="btn-blue"><?php echo site_lg('lg_search', 'Search');?></button>
				<div id="result"></div>
		</fieldset>
	</div>
	<div class="color-container">
		<fieldset class="filtering">
			<p class="listing-top">
				<?php echo $listSubCatSelBox;?>
			</p>
			<p class="next">
				<select class="amount-range price-range">
				 <option value=""><?php if($this->lang->line('product_any_price') != '') { echo stripslashes($this->lang->line('product_any_price')); } else echo "Any Price"; ?></option>	
				  <?php foreach ($pricefulllist->result() as $priceRangeRow){ ?>
					 
				   <option <?php if($_GET['p']==url_title($priceRangeRow->price_range)){ echo 'selected="selected"'; } ?> value="<?php echo url_title($priceRangeRow->price_range); ?>"><?php echo $currencySymbol;?> <?php echo $priceRangeRow->price_range; ?></option>
			   <?php } ?>
				</select>
				<select class="color-range color-filter">
				  <option value="" selected="selected"><?php if($this->lang->line('product_any_color') != '') { echo stripslashes($this->lang->line('product_any_color')); } else echo "Any Color"; ?></option>
				  <?php 
					  foreach ($mainColorLists->result() as $colorRow){
						if ($colorRow->list_value != ''){
				  ?>
				  <option <?php if($_GET['c']==url_title($colorRow->list_value)){ echo 'selected="selected"'; } ?> value="<?php echo strtolower($colorRow->list_value);?>"><?php echo ucfirst($colorRow->list_value);?></option>
				  <?php }}?>
				</select>
				<select class="sort-amount sort-by-price">
				  <option <?php if ($this->input->get('sort_by_price')==''){?>selected="selected"<?php }?> value=""><?php if($this->lang->line('product_newest') != '') { echo stripslashes($this->lang->line('product_newest')); } else echo "Newest"; ?></option>
				  <option <?php if ($this->input->get('sort_by_price')=='asc'){?>selected="selected"<?php }?> value="asc"><?php if($this->lang->line('product_low_high') != '') { echo stripslashes($this->lang->line('product_low_high')); } else echo "Price: Low to High"; ?></option>
				  <option <?php if ($this->input->get('sort_by_price')=='desc'){?>selected="selected"<?php }?> value="desc"><?php if($this->lang->line('product_high_low') != '') { echo stripslashes($this->lang->line('product_high_low')); } else echo "Price: High to Low"; ?></option>
				</select>
			</p>
		</fieldset>
		<?php 
		if($productList->num_rows() >0){
			$count = $productList->num_rows();
		}else{
			$count =0;
		}
		?>
		<dl><dt><b><?php echo ucfirst($_GET['q']);?></b>,&nbsp;<?Php echo $count;?>&nbsp;<?php if($this->lang->line('count_products') != '') { echo stripslashes($this->lang->line('count_products')); } else echo "Products"; ?></dt>
<!-- 			<dt><a href="javascript:void(0);" class="filter-s-h"><?php echo site_lg('lg_filter', 'Filter');?></a></dt> -->
		</dl>
		<?php if ($productList->num_rows()>0){?>
        <ul class="color-shop">
			<?php foreach ($productList->result() as $productListVal) {
					$img = 'dummyProductImage.jpg';
					$imgArr = explode(',', $productListVal->image);
					if (count($imgArr)>0){
						foreach ($imgArr as $imgRow){
							if ($imgRow != ''){
								$img = $pimg = $imgRow;
								break;
							}
						}
		}?>
				<li class="color_item">
				<a href="things/<?php echo $productListVal->id;?>/<?php echo url_title($productListVal->product_name,'-');?>">
				<img src="<?php echo DESKTOPURL;?>images/product/<?php echo $img; ?>">
				<?php echo $productListVal->product_name;?><b><?php echo $currencySymbol;?><?php echo $productListVal->sale_price;?></b>
				</a>
				</li>
        <?php }?>
			<div class="pagination" style="display:none">
				<?php echo $paginationDisplay; ?>
			</div>
        </ul>
		<?php }else {?>
			<ul>
				<li style="width: 100%;"><p class="noproducts"><?php if($this->lang->line('product_no_more') != '') { echo stripslashes($this->lang->line('product_no_more')); } else echo "No more products available"; ?></p></li>
			</ul>
		<?php }?>
	</div>
</div>
<script type="text/javascript">
//$('.next').hide();
$('.filter-s-h').click(function(){
	$('.next').toggle();
});
/*$('.sub-listing').change(function(){
	var value = $('.sub-listing option:selected').val();
	id = 'cy';
	search_value(id,value);
});
function search_value(id,value){
	//alert('<?php echo $this->input->server('QUERY_STRING');?>');
	var range = value;
	var cur_arg = id;
	var cur_url = '<?php echo current_url()?>?'+'<?php echo $this->input->server('QUERY_STRING');?>';
	var args = $.extend({}, location.args), query;
	if(cur_arg == 'cy'){
		if(range != ''){
			args.cy = range;
		}else{
			delete args.cy;
		}		
	}
	if(query = $.param(args)) cur_url += '&'+query;
	window.location.href = cur_url;
}*/
</script>