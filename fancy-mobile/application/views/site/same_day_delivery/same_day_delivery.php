<?php 
if($this->uri->segment(1) == 'things' && $this->uri->segment(3) != 'edit') {
?>
<div class="popup ly-title same-daydly">
	<p class="ltit"><?php if($this->lang->line('same_day_delivery') != '') { echo stripslashes($this->lang->line('same_day_delivery')); } else echo "Same-day Delivery"; ?></p>
     <div class="ltxt">
	   <?php if($productDetails->num_rows()==1){
        $city_values = explode(',',$productDetails->row()->city);?>
	    <div class="inner">
	       <div class="top">
		     <span>
			     <b><?php if($this->lang->line('available_locations') != '') { echo stripslashes($this->lang->line('available_locations')); } else echo "Available locations"; ?>&nbsp;<?php echo count($city_values);?></b>
				 <small>
				 </small>
			 </span>
			 <div class="bottom">
			     <ul>
				    <?php foreach($city_values as $_result){?>
				    <li>
					   <span class="location"><i class="ic-location"></i><?php echo $_result;?></span>
					   <small class="time"><?php echo site_lg('lg_order_before', 'Order Before');?>&nbsp;<b>12 PM</b></small>
					</li>
					<?php }?>
                 </ul>				 
			 </div>
		   </div>
		   <div class="center">
		     <span>
			     <b><?php if($this->lang->line('zip_code_eligible') != '') { echo stripslashes($this->lang->line('zip_code_eligible')); } else echo "Check if your zip code is eligible"; ?></b>
			 </span>
			 <div class="bottom">
			      <input id="zipcode" type="text" placeholder="Enter zip code">
				  <select id="country">
                      <?php foreach($countryList->result() as $_countryList){?>
					    <option value="<?php echo $_countryList->country_code;?>"><?php echo $_countryList->name;?></option>
					  <?php }?>
				  </select>
				  <button class="check-availability"><?php if($this->lang->line('same_day_check') != '') { echo stripslashes($this->lang->line('same_day_check')); } else echo "Check"; ?></button>
			 </div>
		   </div>
		   <div class="inner-bottom" id="bottom-text">
		     <span><?php if($this->lang->line('same_day_text') != '') { echo stripslashes($this->lang->line('same_day_text')); } else echo "(ex: 94043) This will help us determine if you’re located in our current delivery area."; ?></span>
		   </div>
		   <div class="validate">
		   </div>
		</div>
	 </div>
<button title="Close" class="ly-close"><i class="ic-del-black"></i></button>
</div>
<script>
$(document).ready(function(){
    var popup = $.dialog('same-daydly');
	$('.same-day-delivery').click(function(event){
	    popup.open();
		event.preventDefault();
	});
	$('.check-availability').click(function(){
	   if($('#zipcode').val() ==''){
	      $('#bottom-text').hide();
		  $('.validate').text('Please enter your zipcode.');
		  return false;
	   }
	   var url = '<?php echo base_url();?>site/samedaydelivery/check_zipcode';
	       zip_code = $('#zipcode').val();
		   country = $('#country option:selected').val();
	   $.post(url,{'zip_code':zip_code,'country':country},function(data){
	      $('#bottom-text').hide();
		  $('.validate').text(data);
	   });
	});
});
</script>
<?php }}?> 