<?php 
if($this->uri->segment(1) == 'same-day-delivery') {
?>
<div class="popup ly-title available-cities">
   <p class="ltit"><?php if($this->lang->line('available_cities') != '') { echo stripslashes($this->lang->line('available_cities')); } else echo "Available cities"; ?></p>
   <div class="ltxt">
     <?php foreach($country_list->result() as $countries){?>
	     <dt>
		    <?php echo $countries->name;?>
			<dd>
			   <ul>
			      <?php if($get_cities[$countries->name]!='' && $get_cities[$countries->name]->num_rows()>0){
				          foreach($get_cities[$countries->name]->result() as $rows){?>
			   <li><a class="city-list" href="#"><?php echo $rows->city_name;?></a></li>      
             <?php }}?>
			   </ul>
			</dd>
		 </dt>
		 
	 <?php } ?>
   </div>
</div>
<script>
  $(document).ready(function(){
     var popup = $.dialog('available-cities');
	 $('.available-cities').click(function(event){
	    popup.open();
		event.preventDefault();
	});
	$('a.city-list').click(function(){
	  var city = $(this).text().toLowerCase();
	  window.location.href= "<?php echo base_url();?>same-day-delivery?q="+city;
	});
  });
</script>
<?php }?>