<?php
//echo "<pre>"; print_r($relatedProductsArr);die;
 $this->load->view('site/templates/header'); ?>
<div id="content">
	<div id="home-timeline">
		<div class="figure-product">
		<?php if($flash_data != '') { ?>
			<div class="errorContainer" id="<?php echo $flash_data_type;?>">
				<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
				<p><span><?php echo $flash_data;?></span></p>
			</div>
		<?php } ?>
					<div class="figure">
				<span class="detail-product"><img src="<?php echo DESKTOPURL;?>images/giftcards/<?php echo $this->config->item('giftcard_image'); ?>" alt="<?php echo $this->config->item('giftcard_title'); ?>" height="640" width="640"></span>
                
				<figcaption><?php echo $this->config->item('giftcard_title'); ?></figcaption>
			  
			</div>
			
			<div class="addtocart">
            				<button class="greenbutton buyclick giftcard-popup" type="button"><?php echo site_lg('lg_buy_now', 'Buy Now');?></button>
               
     		</div>

				<?php 
				if (count($relatedProductsArr)>0){
				?>
		<div class="figure-product">
			<h2 class="comments-title"><?php if($this->lang->line('giftcard_you_might') != '') { echo stripslashes($this->lang->line('giftcard_you_might')); } else echo "You might also"; ?> <?php echo $siteTitle;?>...</h2>
			<ul class="product-image">
			<?php 
					$limitCount = 0;
					foreach ($relatedProductsArr as $relatedRow){
						if ($limitCount<3){
							$limitCount++;
						$img = 'dummyProductImage.jpg';
						$imgArr = explode(',', $relatedRow->image);
						if (count($imgArr)>0){
							foreach ($imgArr as $imgRow){
								if ($imgRow != ''){
									$img = $imgRow;
									break;
								}
							}
						}
						$fancyClass = 'fancy';
						$fancyText = LIKE_BUTTON;
						if (count($likedProducts)>0 && $likedProducts->num_rows()>0){
							foreach ($likedProducts->result() as $likeProRow){
								if ($likeProRow->product_id == $relatedRow->seller_product_id){
									$fancyClass = 'fancyd';$fancyText = LIKED_BUTTON;break;
								}
							}
						}
					?>

				<li><a class="listing-img" href="<?php echo base_url();?>things/<?php echo $relatedRow->id;?>/<?php echo url_title($relatedRow->product_name,'-');?>"><img src="<?php echo DESKTOPURL;?>images/product/<?php echo $img;?>">
					<?php echo $relatedRow->product_name?>
					<b><?php echo $sale_price;?></b>
					</a>
				</li>
				<?php }}?>
			</ul>
		</div>
		<?php } ?>
		
        
        
		
	
		
	</div>      
</div>