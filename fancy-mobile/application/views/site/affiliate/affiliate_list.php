<?php
$this->load->view('site/templates/header');
?>
<link rel="stylesheet" media="all" type="text/css" href="css/site/<?php echo SITE_COMMON_DEFINE ?>setting.css">
<!-- Section_start -->
<div class="lang-en no-subnav wider winOS">
<!-- Section_start -->
<div id="container-wrapper">
	<div class="container set_area">
		<div id="content">
		<h2 class="ptit"><?php if($this->lang->line('affiliate_common') != '') { echo stripslashes($this->lang->line('affiliate_common')); } else echo "Affiliate"; ?></h2>
       <div style="float:left; margin:10px; padding:3px; line-height:20px; font-weight:bold; font-size:12px;" ><ul>
        	<li><?php echo site_lg('lg_your_total_ commission', 'Your Total Commission ($)');?> &nbsp;&nbsp;:&nbsp;&nbsp;<span><?php echo ($getUserDetails->row()->affilate_purchase_amt) + ($getUserDetails->row()->affiliate_remain_amt); ?></span></li>
            <li><?php echo site_lg('lg_your_total_ purchase', 'Your Total Purchase ($)');?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<span><?php echo $getUserDetails->row()->affilate_purchase_amt; ?></span></li>
            <li><?php echo site_lg('lg_your_total_ remaining', 'Your Total Remaining ($)');?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<span><?php echo $getUserDetails->row()->affiliate_remain_amt; ?></span></li></ul>
        </div>
        
        
        <?php if(count($getReferalList->result()) > 0){
                ?>	
                <div class=" section gifts">
            <h3><?php if($this->lang->line('affiliate_history') != '') { echo stripslashes($this->lang->line('affiliate_history')); } else echo "Your affiliate history."; ?></h3>
                	<div class="chart-wrap">
            <table class="chart">
                <thead>
                    <tr>
                        <th><?php if($this->lang->line('affiliate_thumbnail') != '') { echo stripslashes($this->lang->line('affiliate_thumbnail')); } else echo "Thumbnail"; ?></th>
                        <th><?php if($this->lang->line('signup_user_name') != '') { echo stripslashes($this->lang->line('signup_user_name')); } else echo "Username"; ?></th>
                        <th><?php if($this->lang->line('affiliate_email') != '') { echo stripslashes($this->lang->line('affiliate_email')); } else echo "Email"; ?></th>
                        <th><?php if($this->lang->line('affiliate_date') != '') { echo stripslashes($this->lang->line('affiliate_date')); } else echo "Purchase Date"; ?></th>
                        <th><?php if($this->lang->line('affiliate_commission_point') != '') { echo stripslashes($this->lang->line('affiliate_commission_point')); } else echo "Commission ($)"; ?></th>
                        <th><?php if($this->lang->line('affiliate_cn_percentage') != '') { echo stripslashes($this->lang->line('affiliate_cn_percentage')); } else echo "Commission Percentage (%)"; ?></th>                       
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($getReferalList->result() as $referalList){
                    	$img = 'user-thumb1.png';
                    	if ($referalList->thumbnail != ''){
                    		$img = $referalList->thumbnail;
                    	}
                    ?>
                    <tr>
                        <td><img src="<?php echo DESKTOPURL;?>images/users/<?php echo $img;?>" width="50px"/></td>
                        <td><?php echo $referalList->user_name;?></td>                        
                        <td><?php echo $referalList->email;?></td>
                        <td><?php echo date("jS M, Y", strtotime($referalList->dateAdded));?></td>
                        <td><?php echo $referalList->commission_point;?></td>
                        <td><?php echo $referalList->cn_percentage;?></td>
                        
                    </tr>
                    <?php }?>
                    
                </tbody>
            </table>
            
			</div>
             
			</div>
           
                <?php	
                }else {
                ?>
               <div class="section referral no-data">
            <span class="icon"><i class="ic-referral"></i></span>
            <p><?php if($this->lang->line('affiliate_not_invite') != '') { echo stripslashes($this->lang->line('affiliate_not_invite')); } else echo "You havn't invited anyone yet."; ?></p>
        </div>
                <?php 
                }
                ?>

        

	</div>

		
		<?php 
	 $this->load->view('site/user/settings_sidebar');
     $this->load->view('site/templates/side_footer_menu');
     ?>
	</div>
	<!-- / container -->
</div>
</div>

<!-- Section_start -->
<script type="text/javascript" src="js/site/<?php echo SITE_COMMON_DEFINE ?>address_helper.js"></script>
<script>
	jQuery(function($) {
		var $select = $('.gift-recommend select.select-round');
		$select.selectBox();
		$select.each(function(){
			var $this = $(this);
			if($this.css('display') != 'none') $this.css('visibility', 'visible');
		});
	});
</script>
<?php 
$this->load->view('site/templates/footer');
?>
