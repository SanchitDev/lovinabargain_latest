<?php if(isset($relatedProductsArr) && $this->uri->segment(1) == 'gift-cards'){ ?>
<div style='display:none'>
	<div id='giftcard_popup' style='background:#fff;'>  
		<div style="background:#fff" class="popup-inner">
                       
            
            
            <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo site_lg('lg_fancyyy_mobile', 'Fancyyy Mobile');?></title>
<link type="text/css" rel="stylesheet" href="css/global.css" />

</head>

<body>

<div id="global">
	<div id="container">
    	
        <div id="content" class="account-page">
        <div class="account-wrap">
        <dl class="profile-account">
        <dt class="tit-profile"><?php echo site_lg('lg_fancy_gift', 'Fancy Gift Card');?></dt>
        <dd>
        <p class="years">
        <label class="label"><?php if($this->lang->line('giftcard_value') != '') { echo stripslashes($this->lang->line('giftcard_value')); } else echo "Value"; ?></label>
         <?php $amtVal = explode(',',$this->config->item('giftcard_amounts'));  ?>
        <select name="price_value" id="price_value">
        <?php foreach($amtVal as $amts){ ?>
	                   	<option value="<?php echo $amts; ?>" <?php if($amts == $this->config->item('giftcard_default_amount')){ echo 'selected="selected"'; }?> ><?php echo $currencySymbol.$amts; ?></option>									
						<?php }	?>
        </select>
        </p>
        <p class="places">
        <label class="label"><?php if($this->lang->line('giftcard_reci_name') != '') { echo stripslashes($this->lang->line('giftcard_reci_name')); } else echo "Recipient name"; ?></label>
        <input type="text" value="" id="recipient_name" class="option required" autocapitalize="off" autocorrect="off" name="recipient_name">
        </p>
        <p class="twit">
        <label class="label"><?php if($this->lang->line('giftcard_rec_email') != '') { echo stripslashes($this->lang->line('giftcard_rec_email')); } else echo "Recipient e-mail"; ?></label>
        <input type="text" value="" autocapitalize="off" autocorrect="off" name="recipient_mail" id="recipient_mail" class="option required email" >
        </p>
        <p class="abo">
        <label class="label"><?php if($this->lang->line('giftcard_person_msg') != '') { echo stripslashes($this->lang->line('giftcard_person_msg')); } else echo "Personal message"; ?></label>
        <textarea type="text" id="description" name="description" class="option required"></textarea>
        </p><br />
         <?php if($loginCheck!=''){ ?>
                        	<input type="hidden" name="sender_name" id="sender_name" value="<?php echo $this->session->userdata('session_user_name'); ?>" />
                            <input type="hidden" name="sender_mail" id="sender_mail" value="<?php echo $this->session->userdata('session_user_email'); ?>" />
                        <?php } ?>
         <p class="abo">
      <button style="width:100%" type="submit" name="addtocart" id="addtocart" <?php if ($loginCheck==''){echo 'require_login="true"';}?>class="btn-green btn-payment greencart create-gift-card" onclick="javascript:ajax_add_gift_card();"><?php if($this->lang->line('header_add_cart') != '') { echo stripslashes($this->lang->line('header_add_cart')); } else echo "Add to Cart"; ?></button>
       
 
        </p>	
        
        </dd>
        </dl>
        
        </div></div>
    </div>
</div>
<style> 
.btn-green {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background: -moz-linear-gradient(center top , #6FBF4A, #5DB535) repeat scroll 0 0 rgba(0, 0, 0, 0);
    border-color: #569D37 #549839 #51923D;
    border-image: none;
    border-radius: 0.15em;
    border-style: solid;
    border-width: 1px;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);
    color: #FFFFFF;
    font-size: 1.1em;
    font-weight: bold;
    line-height: 2.65em;
    text-shadow: 0 -1px 1px rgba(0, 0, 0, 0.26);
}
</style>
</body>
</html>

            
            
          
		</div>    
	</div>      
</div>

  <?php }?>