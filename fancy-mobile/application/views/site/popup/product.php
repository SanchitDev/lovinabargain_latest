<?php 
if(isset($productDetails) && $this->uri->segment(1) == 'things'){ ?>
<div style='display:none'>
	<div id='product_container' style='background:#fff;'>  
		<div style="background:#fff" class="popup-inner">
            <div id="home-timeline">
            	<div style="border:none; box-shadow:none" class="figure-product">
                	<div style="padding:0"  class="figure">
						<div style="background:#F8F9F9;">
							<?php 
								$img = 'dummyProductImage.jpg';
								$imgArr = explode(',', $productDetails->row()->image);
								if (count($imgArr)>0){
									foreach ($imgArr as $imgRow){
										if ($imgRow != ''){
											$img = $pimg = $imgRow;
											break;
										}
								}}?>
							<span class="detail-product"><img src="<?php echo DESKTOPURL;?>images/product/<?php echo $img;?>" /></span>
						</div>
						<figcaption class="fig-detail"><?php echo $productDetails->row()->product_name;?></figcaption>
						<b class="amount"><?php echo $currencySymbol;?><span><?php echo $productDetails->row()->sale_price;?></span></b>
					</div>
					<input type="hidden" id="product_type" value="<?php echo $productDetails->row()->product_type;?>"/>
				</div>
                <div class="from">
					<fieldset>
						 <?php  
						$attrValsSetLoad = ''; //echo '<pre>'; print_r($PrdAttrVal->result_array()); 
						if($PrdAttrVal->num_rows>0){ 
							$attrValsSetLoad = $PrdAttrVal->row()->pid; 
						?>
						<p class="left-side">
							<label class="text-Option"><?php echo site_lg('lg_option', 'Option');?></label>
							<!--<select class="click" name="option_id">
								<option value="0"><?php if($this->lang->line('checkout_select') != '') { echo stripslashes($this->lang->line('checkout_select')); } else echo "Select"; ?> </option>
								<?php foreach($PrdAttrVal->result_array() as $Prdattrvals ){ ?>
								<option value="<?php echo $Prdattrvals['pid']; ?>"><?php echo $Prdattrvals['attr_type'].'/'.$Prdattrvals['attr_name']; ?></option>
								<?php } ?>							
							</select>-->
							<input type="hidden" id="attribute_must" name="" value="<?php echo $productDetails->row()->attribute_must;?>">
							<?php 
							foreach($attributes as $key => $_attributes){ ?>
						<div><label for="<?php echo $key?>"><?php echo $key?></label>
						<select name="attr_name_id" id="attr_name_id" class="option selectBox attr_name_id" style="border:1px solid #D1D3D9;width: 190px;" onchange="ajaxCartAttributeChange(this,'<?php echo $productDetails->row()->id; ?>');" >
						<option value="0">--------------- <?php if($this->lang->line('checkout_select') != '') { echo stripslashes($this->lang->line('checkout_select')); } else echo "Select"; ?> ---------------</option>
						<?php foreach($_attributes as $_attributeset){?>
							<option value="<?php echo $_attributeset->pid; ?>"><?php echo $_attributeset->attr_name."/".$_attributeset->attr_price; ?></option>
						<?php } ?>
						</select>
						</div>
					<?php } ?>
						</p>
						<div style="color:#FF0000;" id="AttrErr"></div>
						<div id="loadingImg_<?php echo $productDetails->row()->id; ?>"></div>
						<?php }?>
						<?php if($productDetails->row()->product_type == "physical"){?>
						<p class="left-side">
							<label class="text-Option"><?php echo site_lg('lg_quantity', 'Quantity');?></label>
							<select id="quantity" data-mqty="<?php echo $productDetails->row()->quantity;?>" class="num quantity" name="quantity">
								<?php for($i = 1; $i <= $productDetails->row()->quantity; $i ++){?>
									<option value="<?php echo $i?>"><?php echo $i?></option>
								<?php } ?>
							</select>
					   </p><?php }?>
					   
						<div style="color:#FF0000;" id="ADDCartErr"></div>
						<input data-mqty="<?php echo $productDetails->row()->quantity;?>"  type="hidden">
						<input type="hidden" class="option number" name="product_id" id="product_id" value="<?php echo $productDetails->row()->id;?>">
						<input type="hidden" class="option number" name="cateory_id" id="cateory_id" value="<?php echo $productDetails->row()->category_id;?>">                
						<input type="hidden" class="option number" name="sell_id" id="sell_id" value="<?php echo $productDetails->row()->user_id;?>">
						<input type="hidden" class="option number" name="price" id="price" value="<?php echo $productDetails->row()->sale_price;?>">
						<input type="hidden" class="option number" name="product_shipping_cost" id="product_shipping_cost" value="<?php echo $productDetails->row()->shipping_cost;?>"> 
						<input type="hidden" class="option number" name="product_tax_cost" id="product_tax_cost" value="<?php echo $productDetails->row()->tax_cost;?>">
						<input type="hidden" class="option number" name="attribute_values" id="attribute_values" value="<?php echo $attrValsSetLoad; ?>">
						
						<?php if($removeProduct->num_rows >0 && $productDetails->row()->product_type=='digital'){?>
							<input  style="  width: 1317px;" type="button" class="greenbutton " <?php if ($loginCheck==''){echo 'require_login="true"';}?> name="removefromcart" value="<?php if($this->lang->line('header_remove_cart') != '') { echo stripslashes($this->lang->line('header_remove_cart')); } else echo "Remove from Cart"; ?>" onclick="ajax_remove_cart('<?php echo $productDetails->row()->id;?>','<?php echo $loginCheck;?>');" />
						<?php }else{ ?>
							<button style="margin:10px 0 0 0" <?php if ($loginCheck==''){echo 'require_login="true"';}?> class="greenbutton buyclick reg-popup cboxElement" type="button" name="addtocart" value="<?php if($this->lang->line('header_add_cart') != '') { echo stripslashes($this->lang->line('header_add_cart')); } else echo "Add to Cart"; ?>" onclick="ajax_add_cart('<?php echo $PrdAttrVal->num_rows; ?>');">Add to Cart</button>
							<?php if( $productDetails->row()->sku_category != ''){ ?>
							<button style="margin:10px 0 0 0" <?php if ($loginCheck==''){echo 'require_login="true"';}?> class="greenbutton buyclick reg-popup cboxElement" type="button" name="addtocart" value="<?php if($this->lang->line('header_create_auction') != '') { echo stripslashes($this->lang->line('header_create_auction')); } else echo "Create auction"; ?>" onclick="auction_add_cart('<?php echo $PrdAttrVal->num_rows; ?>');">Create Auction</button>
						<?php } }?>
						
						
                    </fieldset>
                </div>
                <div class="details">
					<?php 
						$first_paragraph = substr($productDetails->row()->description, 0, 220);
						$second_paragraph = substr($productDetails->row()->description, 221, 400);
					?>
					<div><?php echo $first_paragraph;?></div>
					<div class="dd"><?php echo $second_paragraph;?></div>
                </div>
                <?php if ($second_paragraph!=''){?>
                <a class="more-button" id="more" href="javascript:void(0);"><?php echo site_lg('lg_read_more', 'Read More');?><i></i></a>
				<a class="more-button" id="hide" href="javascript:void(0);"><?php echo site_lg('lg_hide', 'Hide');?><i></i></a>
				<?php }?>
			</div>
		</div>    
		<div style="margin-top: 1em;" class="figure-product">
			<h2 class="comments-title"><?php if($this->lang->line('giftcard_you_might') != '') { echo stripslashes($this->lang->line('giftcard_you_might')); } else echo "You might also"; ?> <?php echo $siteTitle;?>...</h2>
			<ul class="figure-list1">
			<?php if(count($relatedProductsArr)>0){
				$limitCount = 0;
				foreach($relatedProductsArr as $relatedRow){
					if($productDetails->row()->seller_product_id!=$relatedRow->seller_product_id){
				if($limitCount<10){
				$limitCount++;
				$img = 'dummyProductImage.jpg';
				$imgArr = explode(',', $relatedRow->image);
				if(count($imgArr)>0){
					foreach ($imgArr as $imgRow){
						if ($imgRow != ''){
							$img = $imgRow;
							break;
						}
					}
				}
				if($relatedRow->web_link!=''){
					$product_link = 'user/'.$relatedRow->user_name.'/things/'.$relatedRow->seller_product_id.'/'.url_title($relatedRow->product_name,'-');
					$sale_price = '';
				}else{
					$product_link ='things/'.$relatedRow->id.'/'.url_title($relatedRow->product_name,'-'); 
					$sale_price = $currencySymbol.''.$relatedRow->sale_price;
				}
			?>
				<li>
					<a class="figure-img" href="<?php echo $product_link;?>"><img src="<?php echo DESKTOPURL;?>images/product/<?php echo $img;?>">
					<?php echo $relatedRow->product_name?>
					<b><?php echo $sale_price;?></b>
					</a>
				</li>
				<?php }}}}?>
			</ul>
		</div> 
	</div>      
</div>
<script type="text/javascript">
$(".dd").hide();
$("#hide").hide();
$("#more").click(function(){
	$("#more").hide();
	$("#hide").show();
	$(".dd").show();
});
$("#hide").click(function(){
	$("#hide").hide();
	$("#more").show();
	$(".dd").hide();
});
</script>
  <?php }?>