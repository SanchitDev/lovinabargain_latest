<div class="popup feature-product" style="display: none;">
	<div id="featured_product" class="popup_wrap">
		
		<h3 class="stit"><?php if($this->lang->line('featured_seller_products') != '') { echo stripslashes($this->lang->line('featured_seller_products')); } else echo "Featured Seller Products"; ?></h3>
                <?php if($featured_seller_products->num_rows > 0){ ?>
                    <ul class="featured-list">
			<?php
                        $featured = '';
                        if(count($featuredProducts) > 0){
                        foreach($featuredProducts as $products){
                            $featured[] = $products->seller_product_id;
                        }   
                        }
                        foreach($featured_seller_products->result() as $product){ $dummyProductImg = 'dummyProductImage.jpg';
                        if(file_exists(DESKTOPURL.'images/product/'.trim($product->image,','))){
                            $prodImg = DESKTOPURL.'images/product/'.trim($product->image,',');
                        }else{
                            $prodImg = DESKTOPURL.'images/product/'.$dummyProductImg;
                        }
                        ?>
                        <li class="featured_image" pid="<?php echo $product->seller_product_id; ?>"><a href="javascript:;" class="figure-img">
                                <span style="background-image: url(<?php echo $prodImg; ?>);"></span>
                                <img class="tick_featured" style="display: <?php echo in_array($product->seller_product_id,$featured) ?  'block' : 'none'; ?>;" src="<?php echo DESKTOPURL.'images/green_tick.png'; ?>">
<!--                                <i class="green_tick"></i>-->
                        </a></li>
                        <?php } ?>
                    </ul>
                    <div class="btn-area" id="add_featured_products">
                    <button clas="btn-save">ok</button>
                    </div>
                <?php }else{ ?>
                <h2>No products Available</h2>
                <?php } ?>
	</div>
</div>
<style>
/***** Contact Form *****/
/*.tick_featured{
    display: none;
}
.seller-details{
    box-shadow: none !important
}
.seller-details ul{
    margin: 20px 11px 0px 36px !important;
    height:auto;
    width:auto;
}
.feature-product .popup_wrap{
	background: none repeat scroll 0 0 #FFFFFF;
    margin: 0 auto !important;
    width: 650px;
	-moz-border-radius:10px;-webkit-border-radius:10px;border-radius:10px;
	position: relative;
}
.feature-product .popup_wrap h3{cursor:auto;}
.feature-product .popup_wrap .ly-close{
	-moz-border-radius:10px;-webkit-border-radius:10px;border-radius:10px;
}
.feature-product .popup_wrap .frm{
	padding:15px;
	-moz-border-radius:0px 0px 10px 10px;-webkit-border-radius:0px 0px 10px 10px;border-radius:0px 0px 10px 10px;
}
.feature-product .popup_login_box{
	float:left;
	width:100%;
}
.feature-product .popup_login_box li{
	float:left;
	width:100%;
	margin-bottom:10px;
}
.feature-product .popup_login_box li ul{
	float:left;
	width:100%;
}
.feature-product .popup_login_box li ul li{
	float:left;
	width:48%;
}
.feature-product .popup_login_box label{
	float:left;
	width:100%;
	font-size: 13px;
    font-style: normal;
    line-height: 18px;
}


.feature-product .popup_login_box span{
	float:left;
	width:100%;
	color:#FF0000;
    font-style: normal;
}*/

/************************/
</style>
<script>
    $(document).on('click','.featured_image',function(e){
        var image_tick = $(this).find('.tick_featured');
        image_tick.toggle();
    });
    $(document).on('click','#add_featured_products',function(){
        var pids = new Array()
        $('.featured_image').each(function(){
            if($(this).find('.tick_featured').is(':visible')){
                pids.push($(this).attr('pid'));
            }
        });
        var max_products = $('#featuredProduct_count').text();
        if(pids.length <= max_products){
            if(pids.length == 0){ alert('kindly select a product'); return false; }
            $.ajax({
                url:baseURL+"site/user_settings/addSellerFeaturedProducts",
                type:'post',
                dataType:'json',
                data: {pid:pids},
                success: function(json){
                    if(json.message == 1){
                        window.location.reload();
                    }else{
                        alert(json.errormsg);
                    }
                }
            });
        }else{
            alert('You are allowed to select only '+max_products+' products');
            return false;
        }
    });
</script>
