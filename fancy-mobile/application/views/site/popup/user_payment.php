<?php 
if(isset($countryList) && $this->uri->segment(2) == 'payment-method' || isset($countryList) && $this->uri->segment(1) == 'cart'){
/* if($this->uri->segment(1) == 'cart'){
	$acURL = 'site/cart/insert_shipping_address';
}else{
	$acURL = 'site/user_settings/insertEdit_shipping_address';
} */
?> 
    <div class="popup ly-title newpayment" >
		<p class="ltit"><?php if($this->lang->line('add_payment_method') != '') { echo stripslashes($this->lang->line('add_payment_method')); } else echo "Add Payment Method"; ?></p>
		<form class="ltxt" action="site/user_settings/insertEdit_payment_method" method="post" name ="paymentForm" id="paymentForm" onsubmit="return payment_validation()">
        
        	<div class="popup_add_method">
			<dl>
				<dt><b><?php if($this->lang->line('payment_info') != '') { echo stripslashes($this->lang->line('payment_info')); } else echo "Payment Info"; ?></b></dt>
				<dd class="left">
					<p><label><?php if($this->lang->line('name_on_card') != '') { echo stripslashes($this->lang->line('name_on_card')); } else echo "Name on card"; ?></label>
					<input id="full_name" name="full_name" class="popup_add_input" type="text" placeholder="<?php if($this->lang->line('payment_home_work') != '') { echo stripslashes($this->lang->line('payment_home_work')); } else echo "E.g. Aunt Jane"; ?>"></p>
					
					<p><label><?php if($this->lang->line('card_number') != '') { echo stripslashes($this->lang->line('card_number')); } else echo "Card number"; ?></label><input id="card_no" name="card_no" class="popup_add_input width180" type="text"></p>
					<p>
					<label><?php if($this->lang->line('payment_cvv') != '') { echo stripslashes($this->lang->line('payment_cvv')); } else echo "CVV "; ?></label><input id="card_cvv" name="card_cvv" class="popup_add_input width70" type="text"></p>
					<p>
					<label><?php if($this->lang->line('expiration_date') != '') { echo stripslashes($this->lang->line('expiration_date')); } else echo "Expiration date"; ?></label>
					<select id="CCExpDay" name="expiray-day" class="popup_add_input width70 select-round select-white select-date selectBox required">
						<option value="01" <?php if(date('m')=='01'){ echo $Sel;} ?>>01</option>
						<option value="02" <?php if(date('m')=='02'){ echo $Sel;} ?>>02</option>
						<option value="03" <?php if(date('m')=='03'){ echo $Sel;} ?>>03</option>
						<option value="04" <?php if(date('m')=='04'){ echo $Sel;} ?>>04</option>
						<option value="05" <?php if(date('m')=='05'){ echo $Sel;} ?>>05</option>
						<option value="06" <?php if(date('m')=='06'){ echo $Sel;} ?>>06</option>
						<option value="07" <?php if(date('m')=='07'){ echo $Sel;} ?>>07</option>
						<option value="08" <?php if(date('m')=='08'){ echo $Sel;} ?>>08</option>
						<option value="09" <?php if(date('m')=='09'){ echo $Sel;} ?>>09</option>
						<option value="10" <?php if(date('m')=='10'){ echo $Sel;} ?>>10</option>
						<option value="11" <?php if(date('m')=='11'){ echo $Sel;} ?>>11</option>
						<option value="12" <?php if(date('m')=='12'){ echo $Sel;} ?>>12</option>
				   </select>
				   <select id="CCExpMnth" name="expiray-year" class="popup_add_input width80 select-round select-white select-date selectBox required">
						<?php for($i=date('Y');$i< (date('Y') + 25);$i++){ ?>	
							<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
						<?php } ?>    
				   </select>
				</p>
				</dd>
				 <dt><b><?php if($this->lang->line('checkout_billing_addr') != '') { echo stripslashes($this->lang->line('checkout_billing_addr')); } else echo "Billing Address"; ?></b></dt>
				 <dd class="left">
				    <p><label><?php if($this->lang->line('shipping_address_comm') != '') { echo stripslashes($this->lang->line('shipping_address_comm')); } else echo "Address"; ?></label>
					<input class="popup_add_input" id="address1" name="address1" type="text" placeholder="<?php if($this->lang->line('header_address1') != '') { echo stripslashes($this->lang->line('header_address1')); } else echo "Address Line1"; ?>">
					<input class="popup_add_input" id="address2" name="address2" type="text" placeholder="<?php if($this->lang->line('header_address2') != '') { echo stripslashes($this->lang->line('header_address2')); } else echo "Address Line2"; ?>">
					</p>
					<p><label><?php if($this->lang->line('header_country') != '') { echo stripslashes($this->lang->line('header_country')); } else echo "Country"; ?></label>
					<select name="country" id="country" class="popup_add_input width455">
						<?php 
						if ($countryList->num_rows()>0){
							foreach ($countryList->result() as $country){
						?>
						<option value="<?php echo $country->country_code;?>"><?php echo $country->name;?></option>
						<?php 
							}
						}
						?>
					</select></p>
					<p><label><?php if($this->lang->line('header_city') != '') { echo stripslashes($this->lang->line('header_city')); } else echo "City"; ?></label>
					<input class="popup_add_input width180" name="city" type="text" id="city"></p>
                    <p><label><?php if($this->lang->line('header_state_province') != '') { echo stripslashes($this->lang->line('header_state_province')); } else echo "State / Province"; ?></label>
					<input class="popup_add_input width108" name="state" type="text" id="state"></p>
					<p><label><?php if($this->lang->line('header_zip_postal') != '') { echo stripslashes($this->lang->line('header_zip_postal')); } else echo "Zip / Postal Code"; ?></label>
					<input name="postal_code" class="popup_add_input width108" id="postal_code" type="text"></p>
				</dd>
			</dl>
            </div>
			<div class="btn-area">
				<button type="submit" id="btn-apply" class="btn-done left"><?php if($this->lang->line('apply') != '') { echo stripslashes($this->lang->line('apply')); } else echo "Apply"; ?></button>
			</div>
			<input type="hidden" id="row_id" name="row_id" value=""/>
			<input type="hidden" name="user_id" value="<?php echo $loginCheck;?>"/>
		</form>
		<button class="ly-close" title="Close"><i class="ic-del-black"></i></button>
	</div>	
 <?php }?> 
<script>
$(document).ready(function(){
    var popup = $.dialog('newpayment');
	$('.add_payment').click(function(event){
	    popup.open();
		event.preventDefault();
	});
    $('.payment-edit').click(function(event){
		     var $row = $(this).closest('tr');
			 popup.open();
			 event.preventDefault();
			 var paymentID = $(this).attr('aid');
			 $.ajax({
				type:'POST',
				url:baseURL+'site/user_settings/get_upayment_details',
				data:{'paymentID':paymentID},
				dataType:'json',
				success:function(response){
				    $("#row_id").val(response.row_id);
					$('#full_name').val(response.full_name);
					$('#card_no').val(response.card_no);
					$('#card_cvv').val(response.card_cvv);
					$('#address1').val(response.address1);
					$('#address2').val(response.address2);
					$('#city').val(response.city);
					$('#state').val(response.state);
					$('#country').val(response.country);
					$('#postal_code').val(response.postal_code);
					$('#phone').val(response.phone);
					$('#ship_id').val(shipID);
				}
			});
   });	
});
function payment_validation(){
var full_name=document.forms["paymentForm"]["full_name"].value;
var card_no=document.forms["paymentForm"]["card_no"].value;
var card_cvv=document.forms["paymentForm"]["card_cvv"].value;
var address1=document.forms["paymentForm"]["address1"].value;
var city=document.forms["paymentForm"]["city"].value;
var state=document.forms["paymentForm"]["state"].value;
var postal_code=document.forms["paymentForm"]["postal_code"].value;
if (full_name==null || full_name=="")
  {
  alert("Please Enter your Card Name");
  return false;
  }
  else if(isNaN(card_no) || card_no=="" || card_no.length==16){
     alert("Please Enter your Card No Correctly");
     return false;
  }
  else if(card_cvv==""){
     alert(card_cvv.length);
     alert("Please Enter your Card CVV Correctly");
     return false;
  }
  else if(address1==null || address1==""){
     alert("Please Enter your Address");
     return false;
  }
  else if(city==null || city==""){
     alert("Please Enter your City");
     return false;
  }
  else if(state==null || state==""){
     alert("Please Enter your State");
     return false;
  }
  else if(postal_code==null || postal_code==""){
     alert("Please Enter your ZipCode");
     return false;
  }
  
}
 </script>