<?php if ($loginCheck != '' && $this->uri->segment(3) == 'confirmation') { ?>

<a class="js-open-modal auction_confirm" href="#" data-modal-id="popup"> </a>
    <div id="popup" class="modal-box auction_popup">  
	  <header>
	   
	    <h3><?php 	if ($this->lang->line('Create_your_auction_now') != '') {
								echo stripslashes($this->lang->line('Create_your_auction_now'));
							} else
								echo "Create your auction now";
                ?></h3>
	  </header>
	  <div class="modal-body">
	    <div class="auct-product-name">
					<?php echo $product_detail->row()->product_name; ?>
				</div>
				
                <div class="image-wrapper">
                    <div class="item-image"><?php $img = explode(',',$product_detail->row()->image)[0]; if( $img == ''){ $img  = 'dummy.gif'; } ?>
						<img src="<?php echo DESKTOPURL; ?>images/product/<?php echo $img; ?>">
					</div>
                </div>
				<form action="site/product/new_auctioon" method="post">
					<div class="auction-duration">
						<label>Auction duration</label>
						<select name="duration" required>
							<option value="">Select</option>
							<option value="1">1 day</option>
							<option value="2">2 days</option>
							<option value="3">3 days</option>
							<option value="4">4 days</option>
							<option value="5">5 days</option>
							<option value="6">6 days</option>
							<option value="7">7 days</option>
						</select>
                    </div>
					
					<input name="tnst" type="hidden" value="<?php echo $payment_detail->row()->paypal_transaction_id;?>" >
					<input name="pd" type="hidden" value="<?php echo $product_detail->row()->id;?>" >
					<input name="sku_cat" type="hidden" value="<?php echo $product_detail->row()->sku_category;?>" >
					<input name="action_price" type="hidden" value="<?php echo $payment_detail->row()->total;?>" >
					<div class="sub_mit_buttn">
						<input type="submit" class="btn-save" value="Create">
					</div>
				</form>
	  </div>
	</div>
<script>
	
	$(function(){
	
		var appendthis =  ("<div class='modal-overlay js-modal-close'></div>");

		  $('a[data-modal-id]').click(function(e) {
			e.preventDefault();
			$("body").append(appendthis);
			$(".modal-overlay").fadeTo(500, 0.7);
			//$(".js-modalbox").fadeIn(500);
			var modalBox = $(this).attr('data-modal-id');
			$('#'+modalBox).fadeIn($(this).data());
		  });  
		 
		 
		$(window).resize(function() {
		  $(".modal-box").css({
			top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
			left: ($(window).width() - $(".modal-box").outerWidth()) / 2
		  });
		});
		 
		$(window).resize();
		 
	});
	$(document).ready(function(){
				$(".auction_confirm").click();
				
				$(window).on('beforeunload', function(){
					return "Any changes will be lost";
				});

				$(document).on("submit", "form", function(event){
					$(window).off('beforeunload');
				});

	});			
		
</script>
    <?php
}?>