<?php 
if($this->uri->segment(1) == 'user' && $this->uri->segment(3) == 'things'){
?>
<div class="popup ly-title sell-it">
	<p class="ltit"><?php if($this->lang->line('product_want_sell') != '') { echo stripslashes($this->lang->line('product_want_sell')); } else echo "I want to sell it"; ?></p>
     <div class="ltxt">
	    <div class="left">
		   <a ntoid="15301425" ntid="<?php echo $productDetails->row()->seller_product_id;?>" class="sell-merchant" require_login="<?php if (count($userDetails)>0){echo 'false';}else {echo 'true';}?>">
		      <span> <?php echo site_lg('lg_for_merchants', 'For Merchants');?> </span>
		   </a>
		</div>
		<div class="right">
		   <a ntoid="15301425" ntid="<?php echo $productDetails->row()->seller_product_id;?>" class="sell-customer" require_login="<?php if (count($userDetails)>0){echo 'false';}else {echo 'true';}?>">
		      <span>  <?php echo site_lg('lg_for_customers', 'For Customers');?> </span>
		   </a>
		</div>
	 </div>


<button title="Close" class="ly-close"><i class="ic-del-black"></i></button>
</div>

 
<script>
$(document).ready(function(){
    var popup = $.dialog('sell-it');
	$('.sell-al').click(function(event){
	    popup.open();
		event.preventDefault();
	});
	$('.sell-merchant').click(function(){
	   var $this = $(this), ntid = $this.attr('ntid'), ntoid = $this.attr('ntoid'), login_require = $this.attr('require_login');
			if (login_require && login_require === 'true') return require_login();
			location.href='sales/create?ntid='+ ntid +'&ntoid='+ntoid;
	});
	$('.sell-customer').click(function(){
	   var $this = $(this), ntid = $this.attr('ntid'), login_require = $this.attr('require_login');
		 if (login_require && login_require === 'true') return require_login();
		 var url ='<?php echo base_url();?>site/product/customer_request_mail';
		 $.post(url,{'prod_id':ntid},function(data){
             if(data == 1){
			    alert("Thank you. Your request was successfully sent to Oliver.");
				window.location.href = 'user/<?php echo $productDetails->row()->user_name?>/things/'+ntid+'/<?php echo $productDetails->row()->seourl;?>'
             } 	 
		 });
	});
});
</script>
<style>
.popup.sell-it{ width: 450px; margin-top: 170px !important;}
.popup.sell-it .ltxt { position: relative; border-radius: 0; background: #fff; color: #393d4d; float:left; width:100%; padding:0px; min-height:230px;}
.popup.sell-it .ltit{ font-size:15px !important; height:47px !important; padding:15px;}
.popup.sell-it .ltxt .left, .popup.sell-it .ltxt .right{float:left; width:206px; margin:10px 0px 0px 10px; height:206px; border:1px solid #E5E5E5; border-radius:5px;}
.popup.sell-it .ltxt .left a, .popup.sell-it .ltxt .right a{width:206px; height:206px;}
.popup.sell-it .ltxt .left{padding-top:0px !important;}
.popup.sell-it .ltxt .left:hover, .popup.sell-it .ltxt .right:hover{background-color: #E5E5E5; cursor:pointer;}
</style>
<?php }?> 