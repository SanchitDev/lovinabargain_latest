<?php if ($loginCheck != ''){?>
<!-- add_to_list overlay -->
<div style="display:none;">
	<div id='list_container' style='background:#fff;' show_when_fancy="<?php if ($userDetails->row()->display_lists == 'Yes'){echo 'true';}else {echo 'false';}?>" tid="">
		<div style="background:#fff" class="popup-inner">
			<h3 class=""><?php if($this->lang->line('header_add_list') != '') { echo stripslashes($this->lang->line('header_add_list')); } else echo "Add to List"; ?></h3>
			<div class="item-categories">
					<fieldset class="list-categories">
						<div class="list-box">
							<ul></ul>
						</div>
					</fieldset>
					<fieldset class="new-list">
						<i class="ic-plus"></i>
						<input type="text" placeholder="<?php if($this->lang->line('header_create_nwlist') != '') { echo stripslashes($this->lang->line('header_create_nwlist')); } else echo "Create New List"; ?>" id="quick-create-list" name="list_name">
						<button id="create_list_form" class="btn-create" style="display: none;"><?php if($this->lang->line('header_create') != '') { echo stripslashes($this->lang->line('header_create')); } else echo "Create"; ?></button>
					</fieldset>
				<button id="i-want-this" class="profile-button" type="button"><i class="ic-plus"></i> <b><?php if($this->lang->line('header_want') != '') { echo stripslashes($this->lang->line('header_want')); } else echo "Want"; ?></b></button>
				<button class="profile-button" id="button-done" type="button"><?php if($this->lang->line('header_done') != '') { echo stripslashes($this->lang->line('header_done')); } else echo "Done"; ?></button>
			</div>
		</div>
	</div>	
</div>
<style>
.list-categories ul li{
 padding-bottom: 25px;
}
</style>
<script type="text/javascript">
$('#quick-create-list').keyup(function(){
	$.trim(this.value).length  ? $(this).next().show() : $(this).next().hide();
});
$('#create_list_form').click(function(){
	var list_name = $("#quick-create-list").val();
	var tid = $("#list_container").attr("tid");
	$.ajax({
		 type :'post',
		 url :baseURL+'site/user/create_list',
		 data :{'tid':tid,'list_name':list_name},
		 dataType:'json',
		 success:function(json){
			if(json.status_code != 1) {
				alert(response.message);
				return;
			}
		 }
	});
});
$('#i-want-this').click(function(){

	var $this = $(this), _CLASS_='wanted', params, url, selected;
	if(selected=$this.hasClass(_CLASS_)){
		url = baseURL+'site/product/delete_want_tag';
		params = {thing_id : $('#list_container').attr('tid')};
	}else{
		url = baseURL+'site/product/add_want_tag';
		params = {thing_id : $('#list_container').attr('tid')};
	}
	$.ajax({
		type : 'post',
		url  : url,
		data : params,
		dataType : 'json',
		success  : function(json){
			
			if(!json.status_code|| json.status_code != 1) return;
			if(selected){
				$this.removeClass(_CLASS_);
				$this.addClass('want');
				$this.html('<i class="ic-plus"></i><b><?php echo site_lg('lg_want', 'Want');?></b>');
				$('.social-icons li a.want').removeClass(_CLASS_);
				$('.social-icons li a.want').html('<span class="icon"></span>want it');
			} else {
				$this.removeClass('want');
				$this.addClass(_CLASS_);
				
				$this.html('<i class="ic-plus"></i><b><?php echo site_lg('lg_wanted', 'Wanted');?></b>');
				$('.social-icons li a.want').addClass(_CLASS_);
				$('.social-icons li a.want').html('<span class="icon"></span>You want it');
				
            if($('.social-icons li:eq(2)').find('a').hasClass('owned'))
				{
				$('.social-icons li:eq(2)').find('a').removeClass('owned');
				$('.social-icons li:eq(2)').find('a').addClass('own');
				$('.social-icons li:eq(2)').find('a').html('<span class="icon"></span>Own it')
				}
			}
		}
	});
});
function add_item_to_list(list_ids,evt){
	var tid = $(evt).parents('#list_container').attr('tid');
	if($(evt).is(':checked')) 
		var url = 'site/user/add_item_to_lists';
	else
		var url = 'site/user/remove_item_from_lists';
	$.ajax({
		type: 'post',
		url: baseURL+url,
		data: {list_ids:list_ids,tid:tid},
		dataType: 'json',
		success: function(json){
		}
	});
}
</script>
<!-- /change photo -->
<?php }?>