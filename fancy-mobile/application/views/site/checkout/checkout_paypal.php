<?php 
$this->load->view('site/templates/header.php');
?>
<?php if($flash_data != '') { ?>
	<div class="errorContainer" id="<?php echo $flash_data_type;?>">
		<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
		<p><span><?php echo $flash_data;?></span></p>
	</div>
<?php } ?>
<div id="content" class="cart-page">
	<div class="account-wrap">
		<div class="account-page">
       

        <dl class="profile-account">
       <script>
                      $(document).ready(function(){	$("#PaymentPaypalForm").validate();
					  
					  $.validator.addMethod("ValidZipCode", function( value, element ) {
								var result = this.optional(element) || value.length >= 3;
												if (!result) {
													return false;
												}
												else{
												return true;
												}
							}, "Please Enter the Correct ZipCode");
					  
					   });
					   </script> 
					   <?php 
					   $paypalProcess = unserialize($paypal_ipn_settings['settings']); 
		 
						$checkAmt = @explode('|',$checkoutViewResults);
						
					 ?>
        	<form name="PaymentPaypalForm" id="PaymentPaypalForm" method="post" enctype="multipart/form-data" action="site/checkout/PaymentProcess"  autocomplete="off">
					<input type="hidden" name="paypalmode" id="paypalmode" value="<?php echo $paypalProcess['mode']; ?>"  />
                    <input type="hidden" name="paypalEmail" id="paypalEmail" value="<?php echo $paypalProcess['merchant_email']; ?>"  />    
        <dd>
       
        <p class="effete">
        <label class="label"><?php if($this->lang->line('checkout_billing_addr') != '') { echo stripslashes($this->lang->line('checkout_billing_addr')); } else echo "Billing Address"; ?></b><br /> <small><?php if($this->lang->line('checkout_enter_bill') != '') { echo stripslashes($this->lang->line('checkout_enter_bill')); } else echo "Enter your billing address"; ?></small></p>
                
        <p class="name-account"><label class="label"><?php if($this->lang->line('header_name') != '') { echo stripslashes($this->lang->line('header_name')); } else echo "Name"; ?> <b>*</b></label>
		<input id="full_name" name="full_name" class="popup_add_input required" type="text" value="<?php echo $userDetails->row()->full_name; ?>" required>
        
        </p>
        
         <p class="name-account"><label class="label"><?php if($this->lang->line('shipping_address_comm') != '') { echo stripslashes($this->lang->line('shipping_address_comm')); } else echo "Address"; ?><b>*</b></label>
		<input class="popup_add_input width108 required" name="address" type="text" id="address" value="<?php echo $userDetails->row()->address; ?>" required>
		</p>
        
        
        <p class="name-account"><label class="label"><?php if($this->lang->line('shipping_address_comm') != '') { echo stripslashes($this->lang->line('shipping_address_comm')); } else echo "Address"; ?>2 </label>
		<input class="popup_add_input width108 required" name="address2" type="text" id="address2" value="<?php echo $userDetails->row()->address2; ?>">
		</p>
        
             <p class="name-account"><label class="label"><?php if($this->lang->line('header_city') != '') { echo stripslashes($this->lang->line('header_city')); } else echo "City"; ?><b>*</b></label>
		<input class="popup_add_input width180 required"  name="city" type="text" id="city" value="<?php echo $userDetails->row()->city; ?>" required>
       		</p>   
         <p class="name-account"><label class="label"> <?php if($this->lang->line('checkout_state') != '') { echo stripslashes($this->lang->line('checkout_state')); } else echo "State"; ?> <b>*</b></label>
		<input class="popup_add_input width108 required" name="state" type="text" id="state" value="<?php echo $userDetails->row()->state; ?>" required>
		</p>
             
         <p class="name-account"><label class="label"><?php if($this->lang->line('header_country') != '') { echo stripslashes($this->lang->line('header_country')); } else echo "Country"; ?><b>*</b></label>
		         
                    <select id="country" name="country" class="select-round select-white select-country selectBox required">
												<option value="">-------------------- <?php if($this->lang->line('checkout_select') != '') { echo stripslashes($this->lang->line('checkout_select')); } else echo "SELECT"; ?> --------------------</option>											
                                            <?php foreach($countryList->result() as $cntyRow){ ?>	
												<option value="<?php echo $cntyRow->country_code; ?>" <?php if($cntyRow->country_code == $userDetails->row()->country){ echo 'selected="selected"';} ?> ><?php echo $cntyRow->name; ?></option>
                                            <?php } ?>    
											</select>
              
		</p>
        
        
         <p class="name-account"><label class="label"><?php if($this->lang->line('checkout_zip_code') != '') { echo stripslashes($this->lang->line('checkout_zip_code')); } else echo "Zip Code"; ?><b>*</b></label>
		<input id="postal_code" name="postal_code" class="popup_add_input required ValidZipCode" type="text" value="<?php echo $userDetails->row()->postal_code; ?>" required>
        
        </p>
          
       <p class="name-account"><label class="label"><?php if($this->lang->line('checkout_phone_no') != '') { echo stripslashes($this->lang->line('checkout_phone_no')); } else echo "Phone No"; ?> <b>*</b></label>
              <input class="popup_add_input width180 required number" name="phone_no" type="text" id="phone_no" value="<?php echo $userDetails->row()->phone_no; ?>" required>
		</p>
        
        
        <div class="cart-payment" style="margin-top: 20px;">
                                
                                <dl class="cart-payment-order">
									<dt style="display: block;font-size: 1.1em;font-weight: bold;padding-bottom: 0.5em;margin-top: -0.2em;"><?php if($this->lang->line('checkout_order') != '') { echo stripslashes($this->lang->line('checkout_order')); } else echo "Order"; ?></dt>
									<dd>
										<ul style="text-align: right;line-height: 1.6;">
											<li class="first" style="text-align: right;">
												<span class="order-payment-type" style="float: left;text-align: left;color: #8a8f9c;"><?php if($this->lang->line('checkout_item_total') != '') { echo stripslashes($this->lang->line('checkout_item_total')); } else echo "Item total"; ?></span>
												<b><?php echo $currencySymbol;?><?php echo number_format($checkAmt[0],2,'.',''); ?></b> <?php echo $currencyType;?>
											</li>
											<li style="text-align: right;">
												<span class="order-payment-type" style="float: left;text-align: left;color: #8a8f9c;"><?php if($this->lang->line('referrals_shipping') != '') { echo stripslashes($this->lang->line('referrals_shipping')); } else echo "Shipping"; ?></span>
												<b><?php echo $currencySymbol;?><?php echo number_format($checkAmt[1],2,'.',''); ?></b> <?php echo $currencyType;?>
											</li>
											<li style="text-align: right;">
												<span class="order-payment-type" style="float: left;text-align: left;color: #8a8f9c;"><?php if($this->lang->line('checkout_tax') != '') { echo stripslashes($this->lang->line('checkout_tax')); } else echo "Tax"; ?></span>
												<b><?php echo $currencySymbol;?><?php echo number_format($checkAmt[2],2,'.',''); ?></b> <?php echo $currencyType;?>
											</li>
                                            <?php if($checkAmt[5] > 0){ ?>
                                            <li style="text-align: right;">
												<span class="order-payment-type" style="float: left;text-align: left;color: #8a8f9c;"><?php if($this->lang->line('checkout_discount') != '') { echo stripslashes($this->lang->line('checkout_discount')); } else echo "Discount"; ?></span>
												<b><?php echo $currencySymbol;?><?php echo number_format($checkAmt[5],2,'.',''); ?></b> <?php echo $currencyType;?>
											</li>
                                            <?php }else{ } ?>
                                            

											<li class="total" style="text-align: right;">
												<span class="order-payment-type" style="float: left;text-align: left;color: #8a8f9c;"><b><?php if($this->lang->line('purchases_total') != '') { echo stripslashes($this->lang->line('purchases_total')); } else echo "Total"; ?></b></span>
												<b><?php echo $currencySymbol;?><?php echo number_format($checkAmt[3],2,'.',''); ?></b> <?php echo $currencyType;?>
											</li>
										</ul>
									</dd>
								</dl>
							</div>





<input id="total_price" name="total_price" value="<?php echo number_format($checkAmt[3],2,'.',''); ?>" type="hidden">
                              <input id="email" name="email" value="<?php echo $userDetails->row()->email; ?>" type="hidden">
                              
							<input name="PaypalSubmit" id="PaypalSubmit" class="button-complete greenbutton buyclick greencart" type="submit" value="<?php if($this->lang->line('checkout_comp_pay') != '') { echo stripslashes($this->lang->line('checkout_comp_pay')); } else echo "Complete Payment"; ?>" style="cursor:pointer;"  />
							
							<div class="card-payment-foot"><?php if($this->lang->line('checkout_by_place') != '') { echo stripslashes($this->lang->line('checkout_by_place')); } else echo "By placing your order, you agree to the Terms"; ?> &amp; <?php if($this->lang->line('checkout_codtn_privacy') != '') { echo stripslashes($this->lang->line('checkout_codtn_privacy')); } else echo "Conditions and Privacy Policy"; ?>.</div>








                            
							
            
               </dd></dl>   
            </form>
     
     
	</div>
      
	</div>
</div>

