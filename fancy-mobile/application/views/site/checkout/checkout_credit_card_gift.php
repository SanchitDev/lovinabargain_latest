<?php 
$this->load->view('site/templates/header.php');
?>
<?php if($flash_data != '') { ?>
	<div class="errorContainer" id="<?php echo $flash_data_type;?>">
		<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
		<p><span><?php echo $flash_data;?></span></p>
	</div>
<?php } ?>
<div id="content" class="cart-page">
	<div class="account-wrap">
		<div class="account-page">
       

        <dl class="profile-account">
        <script>$(document).ready(function(){	$("#PaymentCreditForm").validate();
					  
					  $.validator.addMethod("ValidZipCode", function( value, element ) {
								var result = this.optional(element) || value.length >= 3;
												if (!result) {
													return false;
												}
												else{
												return true;
												}
							}, "Please Enter the Correct ZipCode");
					  
					   });</script> 
        	<form name="PaymentCreditForm" id="PaymentCreditForm" method="post" enctype="multipart/form-data" action="site/checkout/PaymentCreditGift" autocomplete="off">
        <!-- <form class="ltxt" action="site/cart/insert_shipping_address" method="post" name ="cartForm" id="cartForm">-->
        <dd>
       
        <p class="effete">
        <label class="label"><?php if($this->lang->line('checkout_cc_info') != '') { echo stripslashes($this->lang->line('checkout_cc_info')); } else echo "Credit Card Information"; ?><br /><small><?php if($this->lang->line('checkout_visa_mster') != '') { echo stripslashes($this->lang->line('checkout_visa_mster')); } else echo "Visa, MasterCard, Discover or American Express"; ?></small></p>
                
        <p class="name-account"><label class="label"><?php if($this->lang->line('header_name') != '') { echo stripslashes($this->lang->line('header_name')); } else echo "Name"; ?> <b>*</b></label>
		<input id="full_name" name="full_name" class="popup_add_input required" type="text" value="<?php echo $userDetails->row()->full_name; ?>" required >
        
        </p>
        
        <p class="name-account"><label class="label"><?php if($this->lang->line('checkout_card_no') != '') { echo stripslashes($this->lang->line('checkout_card_no')); } else echo "Card Number"; ?><b>*</b></label>
      
		<input id="cardNumber" name="cardNumber" class="popup_add_input width180 required" type="text" required >
		</p>
        
            <p class="name-account"><label class="label"><?php if($this->lang->line('checkout_card_type') != '') { echo stripslashes($this->lang->line('checkout_card_type')); } else echo "Card Type"; ?> <b>*</b></label>
		
       
        <select class="sub-listing select-round select-white select-country selectBox required" id="cardType" name="cardType"><b>*</b>
               <option value="Amex"><?php if($this->lang->line('american_express') != '') { echo stripslashes($this->lang->line('american_express')); } else echo "American Express"; ?></option>
              <option value="Visa"><?php if($this->lang->line('visa') != '') { echo stripslashes($this->lang->line('visa')); } else echo "Visa"; ?></option>
              <option value="MasterCard"><?php if($this->lang->line('master_card') != '') { echo stripslashes($this->lang->line('master_card')); } else echo "Master Card"; ?></option>
              <option value="Discover"><?php if($this->lang->line('discover') != '') { echo stripslashes($this->lang->line('discover')); } else echo "Discover"; ?></option>
         </select>
        </p>
       
       
          <p class="name-account"><label class="label"><?php if($this->lang->line('checkout_exp_date') != '') { echo stripslashes($this->lang->line('checkout_exp_date')); } else echo "Expiration Date"; ?> <b>*</b></label>
           <?php $Sel ='selected="selected"';  ?>
                <select id="CCExpDay" name="CCExpDay" class="select-round select-white select-date selectBox required">
                    
                    <option value="01" <?php if(date('m')=='01'){ echo $Sel;} ?>>01</option>
                    <option value="02" <?php if(date('m')=='02'){ echo $Sel;} ?>>02</option>
                    <option value="03" <?php if(date('m')=='03'){ echo $Sel;} ?>>03</option>
                    <option value="04" <?php if(date('m')=='04'){ echo $Sel;} ?>>04</option>
                    <option value="05" <?php if(date('m')=='05'){ echo $Sel;} ?>>05</option>
                    <option value="06" <?php if(date('m')=='06'){ echo $Sel;} ?>>06</option>
                    <option value="07" <?php if(date('m')=='07'){ echo $Sel;} ?>>07</option>
                    <option value="08" <?php if(date('m')=='08'){ echo $Sel;} ?>>08</option>
                    <option value="09" <?php if(date('m')=='09'){ echo $Sel;} ?>>09</option>
                    <option value="10" <?php if(date('m')=='10'){ echo $Sel;} ?>>10</option>
                    <option value="11" <?php if(date('m')=='11'){ echo $Sel;} ?>>11</option>
                    <option value="12" <?php if(date('m')=='12'){ echo $Sel;} ?>>12</option>
                </select>
			<select id="CCExpMnth" name="CCExpMnth" class="select-round select-white select-date selectBox required">
											<?php for($i=date('Y');$i< (date('Y') + 25);$i++){ ?>	
												<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                            <?php } ?>    
											</select>
          
		</p>
        
          <p class="name-account"><label class="label"><?php if($this->lang->line('checkout_security_code') != '') { echo stripslashes($this->lang->line('checkout_security_code')); } else echo "Security Code"; ?> <b>*</b></label>
          								<input style="width:63px;" id="payment-card-security" name="creditCardIdentifier" class="input-code required number" type="password" required >
                                        <img src="images/visa-cvv.png" style="margin:-12px 5px; padding:0px; width:65px; height:30px;"/>
                                        
      </p>
      
           
      
        
          <p class="effete">
        <label class="label"><?php if($this->lang->line('checkout_billing_enter') != '') { echo stripslashes($this->lang->line('checkout_billing_enter')); } else echo "Enter your billing and shipping address"; ?><br /><small><?php if($this->lang->line('checkout_billing_enter') != '') { echo stripslashes($this->lang->line('checkout_billing_enter')); } else echo "Enter your billing and shipping address"; ?></small></p>
        
        
        
      
         <p class="name-account"><label class="label"><?php if($this->lang->line('shipping_address_comm') != '') { echo stripslashes($this->lang->line('shipping_address_comm')); } else echo "Address"; ?><b>*</b></label>
		<input class="popup_add_input width108 required" name="address" type="text" id="address" value="<?php echo $userDetails->row()->address; ?>" required >
		</p>
        
        
        <p class="name-account"><label class="label"><?php if($this->lang->line('shipping_address_comm') != '') { echo stripslashes($this->lang->line('shipping_address_comm')); } else echo "Address"; ?>2 </label>
		<input class="popup_add_input width108 required" name="address2" type="text" id="address2" value="<?php echo $userDetails->row()->address2; ?>">
		</p>
        
             <p class="name-account"><label class="label"><?php if($this->lang->line('header_city') != '') { echo stripslashes($this->lang->line('header_city')); } else echo "City"; ?><b>*</b></label>
		<input class="popup_add_input width180 required"  name="city" type="text" id="city" value="<?php echo $userDetails->row()->city; ?>" required>
       		</p>   
         <p class="name-account"><label class="label"> <?php if($this->lang->line('checkout_state') != '') { echo stripslashes($this->lang->line('checkout_state')); } else echo "State"; ?> <b>*</b></label>
		<input class="popup_add_input width108 required" name="state" type="text" id="state" value="<?php echo $userDetails->row()->state; ?>" required >
		</p>
             
         <p class="name-account"><label class="label"><?php if($this->lang->line('header_country') != '') { echo stripslashes($this->lang->line('header_country')); } else echo "Country"; ?><b>*</b></label>
		         
                    <select id="country" name="country" class="select-round select-white select-country selectBox required"><b>*</b>
												<option value="">-------------------- <?php if($this->lang->line('checkout_select') != '') { echo stripslashes($this->lang->line('checkout_select')); } else echo "SELECT"; ?> --------------------</option>											
                                            <?php foreach($countryList->result() as $cntyRow){ ?>	
												<option value="<?php echo $cntyRow->country_code; ?>" <?php if($cntyRow->country_code == $userDetails->row()->country){ echo 'selected="selected"';} ?> ><?php echo $cntyRow->name; ?></option>
                                            <?php } ?>    
											</select>
              
		</p>
        
        
         <p class="name-account"><label class="label"><?php if($this->lang->line('checkout_zip_code') != '') { echo stripslashes($this->lang->line('checkout_zip_code')); } else echo "Zip Code"; ?> <b>*</b></label>
		<input id="postal_code" name="postal_code" class="popup_add_input required ValidZipCode" type="text" value="<?php echo $userDetails->row()->postal_code; ?>" required >
        
        </p>
          
       <p class="name-account"><label class="label"><?php if($this->lang->line('checkout_phone_no') != '') { echo stripslashes($this->lang->line('checkout_phone_no')); } else echo "Phone No"; ?> <b>*</b></label>
              <input class="popup_add_input width180 required number" name="phone_no" type="text" id="phone_no" value="<?php echo $userDetails->row()->phone_no; ?>" required>
		</p>
        
        
        <div class="cart-payment" style="margin-top: 20px;">
                                <dl class="cart-payment-order">
									<dt style="display: block;font-size: 1.1em;font-weight: bold;padding-bottom: 0.5em;margin-top: -0.2em;"><?php if($this->lang->line('checkout_order') != '') { echo stripslashes($this->lang->line('checkout_order')); } else echo "Order"; ?></dt>
									<dd>
										<ul style="text-align: right;line-height: 1.6;">
											<li class="first" style="text-align: right;">
												<span class="order-payment-type" style="float: left;text-align: left;color: #8a8f9c;"><?php if($this->lang->line('checkout_item_total') != '') { echo stripslashes($this->lang->line('checkout_item_total')); } else echo "Item total"; ?></span>
												<b><?php echo $currencySymbol;?><?php echo number_format($GiftViewTotal,2,'.',''); ?></b> <?php echo $currencyType;?>
											</li>
											<li class="total" style="text-align: right;">
												<span class="order-payment-type" style="float: left;text-align: left;color: #8a8f9c;"><b><?php if($this->lang->line('purchases_total') != '') { echo stripslashes($this->lang->line('purchases_total')); } else echo "Total"; ?></b></span>
												<b><?php echo $currencySymbol;?><?php echo number_format($GiftViewTotal,2,'.',''); ?></b> <?php echo $currencyType;?>
											</li>
										</ul>
									</dd>
								</dl>
							</div>

                             <input id="total_price" name="total_price" value="<?php echo number_format($GiftViewTotal,2,'.',''); ?>" type="hidden">
                              <input id="email" name="email" value="<?php echo $userDetails->row()->email; ?>" type="hidden">
                             <?php if ($authorize_net_settings['status'] == 'Enable'){ ?>
                              <input type="hidden" name="creditvalue" id="creditvalue" value="authorize" />
                              
                              <?php }elseif($paypal_credit_card_settings['status'] == 'Enable'){ ?>
                               <input type="hidden" name="creditvalue" id="creditvalue" value="paypaldodirect" />
                              <?php } ?>
							<input name="CreditSubmit" id="CreditSubmit" class="button-complete greenbutton buyclick greencart" type="submit" value="<?php if($this->lang->line('checkout_comp_pay') != '') { echo stripslashes($this->lang->line('checkout_comp_pay')); } else echo "Complete Payment"; ?>" style="cursor:pointer;" />
                            <div class="card-payment-foot"><?php if($this->lang->line('checkout_by_place') != '') { echo stripslashes($this->lang->line('checkout_by_place')); } else echo "By placing your order, you agree to the Terms"; ?> &amp; <?php if($this->lang->line('checkout_codtn_privacy') != '') { echo stripslashes($this->lang->line('checkout_codtn_privacy')); } else echo "Conditions and Privacy Policy"; ?>.</div>
            
               </dd></dl>   
            </form>
     
	</div>
      
	</div>
</div>


