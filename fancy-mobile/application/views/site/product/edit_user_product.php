<?php 
$this->load->view('site/templates/header.php');
?>
<?php if($flash_data != '') { ?>
	<div class="errorContainer" id="<?php echo $flash_data_type;?>">
		<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
		<p><span><?php echo $flash_data;?></span></p>
	</div>
<?php } ?>
<div id="content" class="cart-page">
	<div class="account-wrap">
		<div class="account-page">
 	 <dl class="profile-account"><dd>
        <form id="userProdEdit" action="site/product/edit_user_product_process" method="post" enctype="multipart/form-data" onsubmit="return validateProduct();">
      
        <p class="effete">
        <label class="label"><?php if($this->lang->line('product_edit_dtls') != '') { echo stripslashes($this->lang->line('product_edit_dtls')); } else echo "Edit details"; ?></label><br /></p>
                
        <p class="name-account"><label class="label">
        
       <fieldset class="pic_preview">
						<img alt="" style="max-height:200px;max-width:200px" src="<?php echo DESKTOPURL;?>images/product/<?php echo $productDetails->row()->image;?>">
						<input type="hidden" name="productID" value="<?php echo $productDetails->row()->seller_product_id;?>"/>
						<p><?php if($this->lang->line('product_diff_photo') != '') { echo stripslashes($this->lang->line('product_diff_photo')); } else echo "Use a different photo?"; ?></p>
						<br class="hidden">
						<input type="file" name="uploadphoto" id="uploadphoto"><br /><br />
						<input type="submit" name="submit" uid="<?php echo $loginCheck;?>" tid="<?php echo $productDetails->row()->seller_product_id;?>" class="upload profile-button" value="<?php if($this->lang->line('header_upload') != '') { echo stripslashes($this->lang->line('header_upload')); } else echo "Upload"; ?>">
					</fieldset></label> </p>
       
        
        <p class="name-account"><label class="label"><?php if($this->lang->line('product_added_by') != '') { echo stripslashes($this->lang->line('product_added_by')); } else echo "Added by"; ?> <a href="user/<?php echo $userDetails->row()->user_name;?>"><?php echo $userDetails->row()->full_name;?></a></label>
		</p>
        
            <p class="name-account"><label class="label"> <?php if($this->lang->line('header_title') != '') { echo stripslashes($this->lang->line('header_title')); } else echo "Title"; ?> <?php if($this->lang->line('product_what_photo') != '') { echo stripslashes($this->lang->line('product_what_photo')); } else echo "What's the thing in the photo?"; ?></label>
            		<input type="text" value="<?php echo $productDetails->row()->product_name;?>" class="text required popup_add_input width180" name="product_name" id="fancy-title">
		</p>
    
          <p class="name-account"><label class="label"><?php if($this->lang->line('header_weblink') != '') { echo stripslashes($this->lang->line('header_weblink')); } else echo "Web link"; ?> <em><?php if($this->lang->line('product_where_find') != '') { echo stripslashes($this->lang->line('product_where_find')); } else echo "Where can you buy it or find out more?"; ?></label>
          <input type="text" value="<?php echo $productDetails->row()->web_link;?>" class="text required popup_add_input width180" name="web_link" id="fancy-web-link">
		</p>
        <p class="name-account"><label class="label"><?php if($this->lang->line('header_comment') != '') { echo stripslashes($this->lang->line('header_comment')); } else echo "Comment"; ?> <em><?php if($this->lang->line('product_share_thoughy') != '') { echo stripslashes($this->lang->line('product_share_thoughy')); } else echo "Share your thoughts!"; ?></em></label>
        <textarea class="text popup_add_input width108" maxlength="200" rows="5" cols="30" name="excerpt" id="fancy-comment"><?php echo $productDetails->row()->excerpt;?></textarea>
		</p>
         <p class="name-account"><label class="label"><?php if($this->lang->line('header_category') != '') { echo stripslashes($this->lang->line('header_category')); } else echo "Category"; ?> <em><?php if($this->lang->line('product_how_should') != '') { echo stripslashes($this->lang->line('product_how_should')); } else echo "How should it be filed?"; ?></em></label>
				<select name="category_id" class="required" id="fancy-category">
									<option selected="" value=""><?php if($this->lang->line('product_put_cate') != '') { echo stripslashes($this->lang->line('product_put_cate')); } else echo "Put in category"; ?>...</option>
									<?php foreach ($mainCategories->result() as $catRow){?>
									<option <?php if ($catRow->id == $productDetails->row()->category_id){echo 'selected="selected"';}?> value="<?php echo $catRow->id;?>"><?php echo $catRow->cat_name;?></option>
									<?php }?>
        </select>     <br />
              
 
    <div class="address-frm">
        <div class="btn-wrap">
<div class="btn-area">
     <p class="name-account"><label class="label"></label>
           <input type="submit" name="submit" uid="<?php echo $loginCheck;?>" tid="<?php echo $productDetails->row()->seller_product_id;?>" class="button done btn-blue btn-save" value="<?php if($this->lang->line('header_save') != '') { echo stripslashes($this->lang->line('header_save')); } else echo "Save"; ?>" />
         </p>
           </div>
        </div>
        </div>
		   </form>
           	</div>
       </dd></dl>  
	</div>
</div>
</div>


<script type="text/javascript" src="js/site/jquery.validate.js"></script>
<script>
	$("#userProdEdit").validate();
</script>
