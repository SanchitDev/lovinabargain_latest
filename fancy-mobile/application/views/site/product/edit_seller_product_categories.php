<?php 
$this->load->view('site/templates/header.php');
?>
<style type="text/css" media="screen">


#edit-details {
    color: #FF3333;
    font-size: 11px;
}
.option-area select.option {
    border: 1px solid #D1D3D9;
    border-radius: 3px 3px 3px 3px;
    box-shadow: 1px 1px 1px #EEEEEE;
    height: 22px;
    margin: 5px 0 12px;
}
a.selectBox.option {
    margin: 5px 0 10px;
    padding: 3px 0;
}
a.selectBox.option .selectBox-label {
    font: inherit !important;
    padding-left: 10px;

}
form label.error{
	color:red;
}

.notetouser{
	float: left;
	width: 98%;
	padding: 1%;
	line-height: 2;
	font-family: verdana;
	font-size: 16px;
	color: #1DB3F0;
	border-bottom: 1px dotted;
	text-align: center;
}
</style>
<div class="lang-en wider no-subnav thing signed-out winOS">


 <!-- Section_start -->
  <div id="container-wrapper">
	<div class="container ">
	<?php if($flash_data != '') { ?>
		<div class="errorContainer" id="<?php echo $flash_data_type;?>">
			<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
			<p><span><?php echo $flash_data;?></span></p>
		</div>
		<?php } ?>

		<div id="content" class="cart-page">
	<div class="account-wrap">
		<div class="account-page">
        <dl class="profile-account"><dd>
      <h2> <?php if($this->lang->line('product_edit') != '') { echo stripslashes($this->lang->line('product_edit')); } else echo "Edit Product"; ?></h2>
        </dd></dl>
                          
             	
 <h2 style="text-align:left;" class="border_bottom padding_bottom15">	</h2>		 
            </div>
                <div class="box-content">
                    <form accept-charset="utf-8" method="post" action="site/product/sell_it/categories" onsubmit="return validate_cat();" id="sellerProdEdit1">
                    <section class="left-section min_height" style="height: 924px;">	
                        <div class="person-lists bs-docs-example">
                        <div class="figure-product">
                            <ul class="tab-menu">
          <li><a data-toggle="tab" <?php if ($editmode != '0'){?>href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit"<?php }?>><?php if($this->lang->line('product_details') != '') { echo stripslashes($this->lang->line('product_details')); } else echo "Details"; ?></a></li>
                                    <li><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('categories')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/categories"<?php }?>>
                                    <?php if($this->uri->segment(4) == 'categories'){?> <span style="color:#4169E1;"><?php } ?>
							<?php if($this->lang->line('product_categories') != '') { echo stripslashes($this->lang->line('product_categories')); } else echo "Categories"; ?></a></li>
                                    <li><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('list')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/list"<?php }?>><?php if($this->lang->line('display_lists') != '') { echo stripslashes($this->lang->line('display_lists')); } else echo "List"; ?></a></li>
                                    <li><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('images')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/images"<?php }?>><?php if($this->lang->line('product_images') != '') { echo stripslashes($this->lang->line('product_images')); } else echo "Images"; ?></a></li>
                                    
                                    <?php if ($productDetails->row()->product_type=='digital'){?>
                                    
                                    <li><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('pdf')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/doc"<?php }?>><?php if($this->lang->line('display_pdf') != '') { echo stripslashes($this->lang->line('display_pdf')); } else echo "Pdf"; ?></a></li>
                                    
                                    <?php } else {?>
									<li><a data-toggle="tab" href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/shipping"><?php if($this->lang->line('header_shipping') != '') { echo stripslashes($this->lang->line('header_shipping')); } else echo "Shipping"; ?></a></li>
                                    <li><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('attribute')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/attribute"<?php }?>><?php if($this->lang->line('header_attr') != '') { echo stripslashes($this->lang->line('header_attr')); } else echo "Attribute"; ?></a></li><?php }?>
                                    <li><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('seo')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/seo"<?php }?>><?php if($this->lang->line('product_seo') != '') { echo stripslashes($this->lang->line('product_seo')); } else echo "SEO"; ?></a></li>
		</ul>
        </div>
                                <script>
                                function saveDetails(mode){
                                    $('#nextMode').val(mode);
									$('#editDetailsSub').trigger('click');
                                }
                                </script>
                                <input type="hidden" name="nextMode" id="nextMode" value="categories"/>
                            <div class="tab-content border_right width_100 pull-left" id="myTabContent"> 
                            <p class="notetouser"><?php if($this->lang->line('product_more_choose') != '') { echo stripslashes($this->lang->line('product_more_choose')); } else echo "You can choose one or more categories here"; ?>.</p>
                                <div id="product_info" class="tab-pane active">
                                    <dl class="profile-account">
										<dd>
											<?php  //echo $categoryView; ?>
											<div>
												<span>Select List Type</span>
												<div class="col-sm-12 no_padd slect_mar_gin">
												<select name="category_id" class="required" id="category_id" >
													<option value="">Select</option>
													<?php foreach($mainCategories->result() as $row){ ?>
													<option value="<?php echo $row->id; ?>" <?php if( $row->id == $CatId[0]){echo 'selected="selected"';} ?>><?php echo $row->cat_name; ?></option>
													<?php } ?>
												</select>
												</div>
												<div class="" style="display:none" id="sub_cat_loading">
													<div id="loader1" class=""><img src="images/ajax-loader/ajax-loader(1).gif" alt="loading subcategory" /></div>
												</div>
											</div>
										
											<div class="sub_category" id="sub_category">
												<?php 
												// print_r($CatId);die;
												 // echo "<pre>";print_r($all_categories->result());
												$len = count($CatId);
												foreach($CatId as $key => $cat){?>
													<?php if ($key != $len - 1) { ?>
													<div class="col-sm-12 no_padd">
														<select onchange="show_subcategory(this)" class="required" class="label_sub_cat" name="subcategories[]">
															<option value="" >Select a sub  category</option>

																<?php foreach($all_categories->result() as $itemsubcat){ ?>
																	<?php if($itemsubcat->rootID == $cat){ echo $itemsubcat->rootID; echo $cat;?>
																		<option <?php $nextkey = $key+1; if($CatId[$nextkey] == $itemsubcat->id){ echo 'selected="selected"'; }?> value="<?php echo $itemsubcat->id; ?>"><?php echo $itemsubcat->cat_name;?></option>
																	<?php }?>
																<?php }?>
														</select>
													</div>
												<?php }?>
													  
												<?php }?>
											</div>
										</dd>
									</dl>
                                    
                                    <input type="hidden" name="PID" value="<?php echo $productDetails->row()->seller_product_id;?>"/>
                                         
									 <dl class="profile-account"><dd>
									  <p class="name-account">
									<input type="submit" id="editDetailsSub" value="<?php if($this->lang->line('header_save') != '') { echo stripslashes($this->lang->line('header_save')); } else echo "Save"; ?>" class="button btn-blue btn-sav"/>
									</p></dd></dl>
                                                                                
                                       
                                </div>
                                
                                
                            </div>
                        </div>
                    </section>
                        
                    </form>
                </div>
            </div>
        		
    
</div>
		
		<!-- / wrapper-content -->

		
		
<script type="text/javascript">
$(document).ready(function(){
	$('#product_info input[type="checkbox"]').click(function(){
		var cat = $(this).parent().attr('class');
		var curCat = cat;
		var catPos = '';
		var added = '';
		var curPos = curCat.substring(3);
		var newspan = $(this).parent().prev();
		if($(this).is(':checked')){
			while(cat != 'cat1'){
				cat = newspan.attr('class');
				catPos = cat.substring(3);
				if(cat != curCat && catPos<curPos){
					if (jQuery.inArray(catPos, added.replace(/,\s+/g, ',').split(',')) >= 0) {
					    //Found it!
					}else{
						newspan.find('input[type="checkbox"]').attr('checked','checked');
						added += catPos+',';
					}
				}
				newspan = newspan.prev(); 
			}
		}else{
			var newspan = $(this).parent().next();
			if(newspan.get(0)){
				var cat = newspan.attr('class');
				var catPos = cat.substring(3);
			}
			while(newspan.get(0) && cat != curCat && catPos>curPos){
				newspan.find('input[type="checkbox"]').attr('checked',this.checked);	
				newspan = newspan.next(); 	
				cat = newspan.attr('class');
				catPos = cat.substring(3);
			}
		}
	});
});
function validate_cat(){
/* 	var count = 0;
	$('#product_info input[type="checkbox"]:checked').each(function(){
		count++;
	});
	if(count == 0){
		alert('You must select atleast one category');
		return false;
	}*/
} 
</script>
		

	</div>
	<!-- / container -->
</div>
</div>

<script type="text/javascript" src="js/site/jquery.validate.js"></script>
<script>
	$("#sellerProdEdit1").validate();
</script>
<?php
$this->load->view('site/templates/footer');
?>