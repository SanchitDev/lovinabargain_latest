<?php 
$this->load->view('site/templates/header.php');
?>
<style type="text/css" media="screen">
.btn_30_blue .btn_link {
    border-left: 1px solid #05447f;
    box-shadow: 1px 0 0 rgba(255, 255, 255, 0.4) inset;
    display: inline-block;
    height: 30px;
    margin-left: 5px;
    margin-right: 3px;
    padding-left: 6px;
}
.btn_30_blue span {
    display: inline-block;
    float: left;
}
.btn_30_blue a {
    background: -moz-linear-gradient(center top , #19a6d3 0%, #0853a1 50%, #064492 51%, #02286e 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
    border: 1px solid #05447f;
    border-radius: 4px;
    box-shadow: 0 0 2px rgba(0, 0, 0, 0.8), 0 1px 1px -1px #fff inset;
    color: #fff;
    display: inline-block;
    height: 30px;
    line-height: 29px;
    padding: 0 5px;
    text-align: center;
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.5), 0 1px 0 rgba(255, 255, 255, 0.2);
}
.btn_30_light, .btn_30_dark, .btn_30_orange, .btn_30_blue {
    display: inline-block;
    margin: 5px;
    position: relative;
}

#edit-details {
    color: #FF3333;
    font-size: 11px;
}
.option-area select.option {
    border: 1px solid #D1D3D9;
    border-radius: 3px 3px 3px 3px;
    box-shadow: 1px 1px 1px #EEEEEE;
    height: 22px;
    margin: 5px 0 12px;
}
a.selectBox.option {
    margin: 5px 0 10px;
    padding: 3px 0;
}
a.selectBox.option .selectBox-label {
    font: inherit !important;
    padding-left: 10px;

}
form label.error{
	color:red;
}

.image_text select, .attribute_box  select{
	border: 1px solid gray;
}
.notetouser{
	float: left;
	width: 98%;
	padding: 1%;
	line-height: 2;
	font-family: verdana;
	font-size: 16px;
	color: #1DB3F0;
	text-align: center;
}
</style>
<?php if($flash_data != '') { ?>
	<div class="errorContainer" id="<?php echo $flash_data_type;?>">
		<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
		<p><span><?php echo $flash_data;?></span></p>
	</div>
<?php } ?>
<div id="content" class="cart-page">
	<div class="account-wrap">
		<div class="account-page">
        <dl class="profile-account"><dd>
       <h2> <?php if($this->lang->line('product_edit') != '') { echo stripslashes($this->lang->line('product_edit')); } else echo "Edit Product"; ?></h2>
        </dd></dl>
 <form accept-charset="utf-8" method="post" action="site/product/sell_it/attribute" id="sellerProdEdit1">
 <div class="figure-product">
<ul class="tab-menu">
          <li class="active"><a data-toggle="tab" <?php if ($editmode != '0'){?>href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit"<?php }?>><?php if($this->lang->line('product_details') != '') { echo stripslashes($this->lang->line('product_details')); } else echo "Details"; ?></a></li>
                                    <li class=""><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('categories')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/categories"<?php }?>><?php if($this->lang->line('product_categories') != '') { echo stripslashes($this->lang->line('product_categories')); } else echo "Categories"; ?></a></li>
                                    <li><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('list')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/list"<?php }?>><?php if($this->lang->line('display_lists') != '') { echo stripslashes($this->lang->line('display_lists')); } else echo "List"; ?></a></li>
                                    <li><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('images')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/images"<?php }?>><?php if($this->lang->line('product_images') != '') { echo stripslashes($this->lang->line('product_images')); } else echo "Images"; ?></a></li>
									  <?php if($productDetails->row()->product_type!='digital'){?>
										<li><a data-toggle="tab" href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/shipping"><?php if($this->lang->line('header_shipping') != '') { echo stripslashes($this->lang->line('header_shipping')); } else echo "Shipping"; ?></a></li>
									<?php }?>
                                    <li><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('attribute')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/attribute"<?php }?>><?php if($this->uri->segment(4) == 'attribute'){?> <span style="color:#4169E1;"><?php } ?><?php if($this->lang->line('header_attr') != '') { echo stripslashes($this->lang->line('header_attr')); } else echo "Attribute"; ?></a></li>
                                    <li><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('seo')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/seo"<?php }?>><?php if($this->lang->line('product_seo') != '') { echo stripslashes($this->lang->line('product_seo')); } else echo "SEO"; ?></a></li>
									
		</ul> </div>
             <script>
		   function saveDetails(mode){
           $('#nextMode').val(mode);
		   $('#editDetailsSub').trigger('click');
           }
         </script> 
<input type="hidden" name="nextMode" id="nextMode" value="attribute"/>
<div class="tab-content border_right width_100 pull-left" id="myTabContent"> 
                            <p class="name-account physical_item">      
								<label class="label"><?php if($this->lang->line('attribute_must') != '') { echo stripslashes($this->lang->line('attribute_must')); } else echo "Attribute must"; ?></label>
								<input type="checkbox" id="isattrSelected"  style="width:10px;" <?php if ( $SubPrdVal->num_rows() == 0){ ?>onclick="return check();" <?php } ?> <?php if ($editmode == '0' && $productDetails->row()->attribute_must =='yes'){?> checked="checked"<?php }?>value="yes" class=""  name="attribute_must" id="attribute_must"> 
							</p>
							<p class="notetouser"><?php if($this->lang->line('product_add_Attr_rem') != '') { echo stripslashes($this->lang->line('product_add_Attr_rem')); } else echo "You can add product attributes here."; ?></p>
                                <div id="product_info" class="tab-pane active">
                                    <div class="form_fields">
                                        <div class="inputs" style="float: left;width:100%; border:1px dashed #1DB3F0;">
							            <div style="margin:12px;">
								            <div class="btn_30_blue">
												<a href="javascript:void(0)" id="addAttr" class="tipTop" title="<?php if($this->lang->line('add_attr') != '') { echo stripslashes($this->lang->line('add_attr')); } else echo "Add attribute"; ?>">
													<span class="btn_link"><?php if($this->lang->line('header_add') != '') { echo stripslashes($this->lang->line('header_add')); } else echo "Add"; ?></span>
												</a>
											</div>
							            </div>
                                         
 <?php  //onchange="javascript:changeListValues123(this,'<?php echo $SubPrdValS['attr_price'];');"
	if (count($SubPrdVal)>0){
			foreach ($SubPrdVal->result_array() as $SubPrdValS){ ?>
		<div style="float: left; margin: 12px 10px 10px; width:97%;" class="field">
			<div class="image_text" style="float: left;margin: 5px;margin-right:30px;">
				<span><?php if($this->lang->line('attribute_type') != '') { echo stripslashes($this->lang->line('attribute_type')); } else echo "Attribute Type"; ?>:</span>
				<select required name="attr_type1[]" style="width:120px;color:gray;" onchange="javascript:ajaxChangeproductAttribute(this.value,'<?php echo $SubPrdValS['attr_name']; ?>','<?php echo $SubPrdValS['attr_price']; ?>','<?php echo $SubPrdValS['pid']; ?>');" >
				<?php foreach ($PrdattrVal->result() as $prdattrRow){ ?>
				<option value="<?php echo $prdattrRow->id; ?>" <?php if( $prdattrRow->id == $SubPrdValS['attr_id'] ){ echo 'selected="selected"';}?> ><?php echo $prdattrRow->attr_name; ?></option>
				<?php } ?>
				</select>
			</div>
			<div class="attribute_box attrInput" style="float: left;margin: 5px;margin-right:30px;" >
				<span><?php if($this->lang->line('attribute_name') != '') { echo stripslashes($this->lang->line('attribute_name')); } else echo "Attribute Name"; ?>:</span>&nbsp;<input type="text" required name="attr_name1[]" style="width:150px;color:gray;" value="<?php echo $SubPrdValS['attr_name']; ?>" onchange="javascript:ajaxChangeproductAttribute('<?php echo $SubPrdValS['attr_id']; ?>',this.value,'<?php echo $SubPrdValS['attr_price']; ?>','<?php echo $SubPrdValS['pid']; ?>');" />
			</div>
			<div class="attribute_box attrInput" style="float: left;margin: 5px;" >
				<span><?php if($this->lang->line('attribute_price') != '') { echo stripslashes($this->lang->line('attribute_price')); } else echo "Attribute Price"; ?>:</span>&nbsp;
				<input required type="text" name="attr_val1[]" style="width:75px;color:gray;" value="<?php echo $SubPrdValS['attr_price']; ?>" onchange="javascript:ajaxChangeproductAttribute('<?php echo $SubPrdValS['attr_id']; ?>','<?php echo $SubPrdValS['attr_name']; ?>',this.value,'<?php echo $SubPrdValS['pid']; ?>');" />
			</div>
            <div id="loadingImg_<?php echo $SubPrdValS['pid']; ?>"></div>
            <div class="btn_30_blue">
				<a href="javascript:void(0)" onclick="removeAttrDb('<?php echo $SubPrdValS['pid']; ?>',this);" class="removeAttr" class="tipTop" title="<?php if($this->lang->line('remove_this_attr') != '') { echo stripslashes($this->lang->line('remove_this_attr')); } else echo "Remove this attribute"; ?>">
					<span class="btn_link"><?php if($this->lang->line('product_remove') != '') { echo stripslashes($this->lang->line('product_remove')); } else echo "Remove"; ?></span>
				</a>
			</div>
		</div>
	<?php } } ?>
                     			        </div>
                                    </div>
                                    <input type="hidden" name="PID" value="<?php echo $productDetails->row()->seller_product_id;?>"/>
                                             <dl class="profile-account"><dd>
                                            <input type="submit" id="editDetailsSub" value="<?php if($this->lang->line('header_save') != '') { echo stripslashes($this->lang->line('header_save')); } else echo "Save"; ?>" class="button btn-blue"/>
                                                 </dd></dl>                                
                                                                        
                                </div>
                            </div></form>
</div>
</div></div>
<script>
$(document).ready(function(){

	var j = 1;
	$('#addAttr').click(function() {
		$('<div style="float: left; margin: 12px 10px 10px; width:97%;" class="field">'+
				'<div class="image_text" style="float: left;margin: 5px;margin-right:30px;">'+
					'<span>Attribute Type:</span>&nbsp;'+
					'<select required name="product_attribute_type[]" style="width:120px;color:gray;" class="chzn-select">'+
						'<option value="">--Select--</option>'+
						<?php foreach ($PrdattrVal->result() as $prdattrRow){ ?>
						'<option value="<?php echo $prdattrRow->id; ?>"><?php echo $prdattrRow->attr_name; ?></option>'+
						<?php } ?>
					 '</select>'+
				'</div>'+
				'<div class="image_text attrInput" style="float: left;margin: 5px;margin-right:30px;">'+
					'<span>Attribute Name:</span>&nbsp;'+
					'<input required type="text" name="product_attribute_name[]" style="width:150px;color:gray;" class="chzn-select"/>'+
				'</div>'+
				'<div class="attribute_box attrInput" style="float: left;margin: 5px;" >'+
					 '<span>Attribute Price :</span>&nbsp;'+
					 '<input required type="text" name="product_attribute_val[]" style="width:75px;color:gray;" class="chzn-select" />'+
				'</div>'+
				'<div class="btn_30_blue">'+
					'<a href="javascript:void(0)" onclick="removeAttr(this);" class="removeAttr" class="tipTop" title="Remove this attribute">'+
						'<span class="btn_link"><?php if($this->lang->line("product_remove") != "") { echo stripslashes($this->lang->line("product_remove")); } else echo "Remove"; ?></span>'+
					'</a>'+
				'</div>'+
		'</div>').fadeIn('slow').appendTo('.inputs');
		j++;
	});
	
});
function removeAttr(evt){
	$(evt).parent().parent().remove();
}
function removeAttrDb(pid,evt){
	if(pid != ''){
		$.post(baseURL+'site/product/remove_attr',{pid:pid});
	}
	$(evt).parent().parent().remove();
}
</script>

<script> 
	function check(){
		
		if($("#isattrSelected").is(':checked'))
		$('#addAttr').click();  // checked
		
	}

</script>