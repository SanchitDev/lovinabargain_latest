<?php
$this->load->view('site/templates/header');
?>
<link rel="stylesheet" type="text/css" media="all" href="css/data-table.css"/> 
<div class="lang-en wider no-subnav thing signed-out winOS" id="content">
<!-- Section_start -->
  <div id="container-wrapper" class="account-wrap">
	<div class="container account-page">
      <dl class="profile-account"><dd>
	<?php if($flash_data != '') { ?>
		<div class="errorContainer" id="<?php echo $flash_data_type;?>">
			<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
			<p><span><?php echo $flash_data;?></span></p>
		</div>
		<?php } ?>
		<div class="wrapper-content">					
            <div class="profile-list">            
                <dl class="profile-account"><dd>
                            <h2> <?php if($this->lang->line('product_edit') != '') { echo stripslashes($this->lang->line('product_edit')); } else echo "Edit Product"; ?></h2>
 </dd></dl>
                <div class="box-content">
                    <form accept-charset="utf-8" method="post" action="site/product/sell_it/images" id="sellerProdEdit1" onsubmit="return validate_desc();" enctype="multipart/form-data">
                    <section class="left-section min_height" style="height: 924px;">	
                        <div class="person-lists bs-docs-example">
                        <div class="figure-product">
                           <ul class="tab-menu">
          <li><a data-toggle="tab" <?php if ($editmode != '0'){?>href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit"<?php }?>><?php if($this->lang->line('product_details') != '') { echo stripslashes($this->lang->line('product_details')); } else echo "Details"; ?></a></li>
                                    <li class=""><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('categories')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/categories"<?php }?>><?php if($this->lang->line('product_categories') != '') { echo stripslashes($this->lang->line('product_categories')); } else echo "Categories"; ?></a></li>
                                    <li><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('list')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/list"<?php }?>><?php if($this->lang->line('display_lists') != '') { echo stripslashes($this->lang->line('display_lists')); } else echo "List"; ?></a></li>
                                    <li><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('images')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/images"<?php }?>> <?php if($this->uri->segment(4) == 'images'){?> <span style="color:#4169E1;"><?php } ?><?php if($this->lang->line('product_images') != '') { echo stripslashes($this->lang->line('product_images')); } else echo "Images"; ?></a></li>
                                       <?php if ($productDetails->row()->product_type=='digital'){?>
                                    <li><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('pdf')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/doc"<?php }?>><?php if($this->lang->line('display_pdf') != '') { echo stripslashes($this->lang->line('display_pdf')); } else echo "Pdf"; ?></a></li>
                                    <?php } else {?>
									<li><a data-toggle="tab" href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/shipping"><?php if($this->lang->line('header_shipping') != '') { echo stripslashes($this->lang->line('header_shipping')); } else echo "Shipping"; ?></a></li>
                                    <li><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('attribute')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/attribute"<?php }?>><?php if($this->lang->line('header_attr') != '') { echo stripslashes($this->lang->line('header_attr')); } else echo "Attribute"; ?></a></li><?php }?>
                                    <li><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('seo')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/seo"<?php }?>><?php if($this->lang->line('product_seo') != '') { echo stripslashes($this->lang->line('product_seo')); } else echo "SEO"; ?></a></li>
		</ul> </div> 
             <script>
		   function saveDetails(mode){
           $('#nextMode').val(mode);
		   $('#editDetailsSub').trigger('click');
           }
         </script>
                                <input type="hidden" name="nextMode" id="nextMode" value="images"/>
                            <div class="tab-content border_right width_100 pull-left" id="myTabContent"> <br />
                                <div id="product_info" class="tab-pane active">
                                 <dl class="profile-account"><dd>  </dd></dl>
                                    <div class="form_fields">
                                        <label><?php if($this->lang->line('product_prod_image') != '') { echo stripslashes($this->lang->line('product_prod_image')); } else echo "Product Image"; ?></label>
                                        <div class="form_fieldsgroup validation-input">
                                            <input type="file" class="global-input multi" name="product_image[]">   
                                            <p style="color:#407A2A;"><?php if($this->lang->line('product_mult_imgs') != '') { echo stripslashes($this->lang->line('product_mult_imgs')); } else echo "You can upload multiple images"; ?></p> <br /></div>
                                    </div>
                                    <div class="widget_content">
							<table class="display display_tbl" id="image_tbl">
							<thead>
							<tr>
								<th class="center">
									<?php if($this->lang->line('product_sno') != '') { echo stripslashes($this->lang->line('product_sno')); } else echo "Sno"; ?>
								</th>
								<th>
									<?php if($this->lang->line('product_img') != '') { echo stripslashes($this->lang->line('product_img')); } else echo "Image"; ?>
								</th>
								<th title="Change the order in which your images need to be displayed Eg : 0 is the main image">
									<?php if($this->lang->line('product_position') != '') { echo stripslashes($this->lang->line('product_position')); } else echo "Position"; ?> 
								</th>
								<th>
									  <?php if($this->lang->line('product_actioin') != '') { echo stripslashes($this->lang->line('product_actioin')); } else echo "Action"; ?>
								</th>
							</tr>
							</thead>
							<tbody>
							<?php 
							$imgArr = explode(',', $productDetails->row()->image);
							if (count($imgArr)>0){
								$i=0;$j=1;
								$this->session->set_userdata(array('product_image_'.$productDetails->row()->id => $productDetails->row()->image));
								foreach ($imgArr as $img){
									if ($img != ''){
							?>
							<tr id="img_<?php echo $i ?>">
								<td class="center tr_select ">
									<input type="hidden" name="imaged[]" value="<?php echo $img; ?>"/>
									<?php echo $j;?>
								</td>
								<td class="center">
									<img src="<?php echo DESKTOPURL;?>images/product/<?php echo $img; ?>"  height="80px" width="80px" />
								</td>
								<td class="center">
								<span>
									<input type="text" style="width: 15%;" name="changeorder[]" class="ordr" value="<?php echo $i; ?>" size="3" />
								</span>
								</td>
								<td class="center">
									<ul class="action_list" style="background:none;border-top:none;"><li style="width:100%;"><a class="p_del tipTop" href="javascript:void(0)" onclick="editPictureProductsUser(<?php echo $i; ?>,<?php echo $productDetails->row()->id;?>);" title="Delete this image"><?php if($this->lang->line('product_remove') != '') { echo stripslashes($this->lang->line('product_remove')); } else echo "Remove"; ?></a></li></ul>
								</td>
							</tr>
							<?php 
							$j++;
									}
									$i++;
								}
							}
							?>
							</tbody>
							<tfoot>
							<tr>
								<th class="center">
									<?php if($this->lang->line('product_sno') != '') { echo stripslashes($this->lang->line('product_sno')); } else echo "Sno"; ?>
								</th>
								<th>
									<?php if($this->lang->line('product_img') != '') { echo stripslashes($this->lang->line('product_img')); } else echo "Image"; ?>
								</th>
								<th title="Change the order in which your images need to be displayed Eg : 0 is the main image">
									<?php if($this->lang->line('product_position') != '') { echo stripslashes($this->lang->line('product_position')); } else echo "Position"; ?>
								</th>
								<th>
									 <?php if($this->lang->line('product_actioin') != '') { echo stripslashes($this->lang->line('product_actioin')); } else echo "Action"; ?>
								</th>
							</tr>
							</tfoot>
							</table>
						</div>
                                    <input type="hidden" name="PID" value="<?php echo $productDetails->row()->seller_product_id;?>"/>
                                    <br/>
                                    <div class="form_fields">
                                            <label></label>
                                            <div class="form_fieldsgroup">
                                            <input type="submit" id="editDetailsSub" value="<?php if($this->lang->line('header_save') != '') { echo stripslashes($this->lang->line('header_save')); } else echo "Save"; ?>" class="button btn-blue"/>
                                                                                  </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    </form></dd></dl>
                </div>
            </div>
    </div>
		<!-- / wrapper-content -->
		<a id="scroll-to-top" href="#header" style="display: none;"><span><?php if($this->lang->line('signup_jump_top') != '') { echo stripslashes($this->lang->line('signup_jump_top')); } else echo "Jump to top"; ?></span></a>
	</div>
	<!-- / container -->
</div>
</div>
<script type="text/javascript" src="js/site/jquery.validate.js"></script>
<script>
	$("#sellerProdEdit1").validate();
	function validate_desc(){
		var inputs = document.getElementsByClassName( 'ordr' ),
		myControls  = [].map.call(inputs, function( input ) {
			return input.value;
		});length1= inputs.length;
		for (var j=0; j<length1; j++) {		
			for(var k=j+1; k<length1; k++){
				if (myControls[j] == myControls[k]){ 
					alert('Duplicate Position exists: '+myControls[j]);
					return false;
			}
			}
		}
	
	}
</script>
</script>
<?php
$this->load->view('site/templates/footer');
?>