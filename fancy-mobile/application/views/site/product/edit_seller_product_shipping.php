<?php
$this->load->view('site/templates/header');
?>
<style type="text/css" media="screen">
.btn_30_blue .btn_link {
    border-left: 1px solid #05447f;
    box-shadow: 1px 0 0 rgba(255, 255, 255, 0.4) inset;
    display: inline-block;
    height: 30px;
    margin-left: 5px;
    margin-right: 3px;
    padding-left: 6px;
}
.btn_30_blue span {
    display: inline-block;
    float: left;
}
.btn_30_blue a {
    background: -moz-linear-gradient(center top , #19a6d3 0%, #0853a1 50%, #064492 51%, #02286e 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
    border: 1px solid #05447f;
    border-radius: 4px;
    box-shadow: 0 0 2px rgba(0, 0, 0, 0.8), 0 1px 1px -1px #fff inset;
    color: #fff;
    display: inline-block;
    height: 30px;
    line-height: 29px;
    padding: 0 5px;
    text-align: center;
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.5), 0 1px 0 rgba(255, 255, 255, 0.2);
}
.btn_30_light, .btn_30_dark, .btn_30_orange, .btn_30_blue {
    display: inline-block;
    margin: 5px;
    position: relative;
}

#edit-details {
    color: #FF3333;
    font-size: 11px;
}
.option-area select.option {
    border: 1px solid #D1D3D9;
    border-radius: 3px 3px 3px 3px;
    box-shadow: 1px 1px 1px #EEEEEE;
    height: 22px;
    margin: 5px 0 12px;
}
a.selectBox.option {
    margin: 5px 0 10px;
    padding: 3px 0;
}
a.selectBox.option .selectBox-label {
    font: inherit !important;
    padding-left: 10px;

}
form label.error{
	color:red;
}

.image_text select, .attribute_box  select{
	border: 1px solid gray;
}
.notetouser{
	float: left;
	width: 98%;
	padding: 1%;
	line-height: 2;
	font-family: verdana;
	font-size: 16px;
	color: #1DB3F0;
	text-align: center;
}
</style>



 <!-- Section_start -->
<div id="content" class="cart-page">
	<div class="account-wrap">
		<div class="account-page">
			<dl class="profile-account"><dd>
			<h2> <?php if($this->lang->line('product_edit') != '') { echo stripslashes($this->lang->line('product_edit')); } else echo "Edit Product"; ?></h2>
        </dd></dl>
	<form accept-charset="utf-8" method="post" action="site/product/sell_it/shipping" id="sellerProdEdit1" enctype="multipart/form-data" onsubmit="return editShippingValidate();">
		<section class="left-section min_height" style="height: 924px;">	
			<div class="person-lists bs-docs-example">
			<ul class="tab-menu">
					<li ><a data-toggle="tab" href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit"><?php if($this->lang->line('product_details') != '') { echo stripslashes($this->lang->line('product_details')); } else echo "Details"; ?></a></li>
					<li class=""><a data-toggle="tab" href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/categories"><?php if($this->lang->line('product_categories') != '') { echo stripslashes($this->lang->line('product_categories')); } else echo "Categories"; ?></a></li>
					<li><a data-toggle="tab" href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/list"><?php if($this->lang->line('display_lists') != '') { echo stripslashes($this->lang->line('display_lists')); } else echo "List"; ?></a></li>
					<li><a data-toggle="tab" href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/images"><?php if($this->lang->line('product_images') != '') { echo stripslashes($this->lang->line('product_images')); } else echo "Images"; ?></a></li>
					<?php if ($productDetails->row()->product_type=='digital'){?>
						<li class="pdf_upload"><a data-toggle="tab" href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/doc"><?php if($this->lang->line('display_pdf') != '') { echo stripslashes($this->lang->line('display_pdf')); } else echo "Pdf"; ?></a></li>
					<?php } else{ ?>
						<li><a data-toggle="tab" href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/attribute"><?php if($this->lang->line('header_attr') != '') { echo stripslashes($this->lang->line('header_attr')); } else echo "Attribute"; ?></a></li>
						<li class="active"><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('shipping')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/shipping"<?php }?>><?php if($this->lang->line('header_shipping') != '') { echo stripslashes($this->lang->line('header_shipping')); } else echo "Shipping"; ?></a></li>
					<?php } ?>
					<li ><a data-toggle="tab" href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/seo"><?php if($this->lang->line('product_seo') != '') { echo stripslashes($this->lang->line('product_seo')); } else echo "SEO"; ?></a></li>
				</ul>
				 <script>
		   function saveDetails(mode){
           $('#nextMode').val(mode);
		   $('#editDetailsSub').trigger('click');
           }
         </script> 
<input type="hidden" name="nextMode" id="nextMode" value="shipping"/>
<div class="tab-content border_right width_100 pull-left" id="myTabShipping"> 
<div id="product_info" class="tab-pane active">
<div class="form_fields">
	<label>Processing Time<span style="color:red;"> *</span></label>
	<div class="form_fieldsgroup validation-input">
		<select onchange="return processing_time_shipping(this.value);" name="ship_duration" id="ship_duration" tabindex="9" class="full required" style="border: #d8d8d8 1px solid; width: 340px; height: 31px;">
			<option value="">Ready to ship in...</option>
			<optgroup label="----------------------------"></optgroup>
			<option class="range-1-1" <?php if($productDetails->row()->ship_duration=='1 business day'){?>selected="selected"<?php } ?>>1 business day</option>
			<option class="range-1-2" <?php if($productDetails->row()->ship_duration=='1-2 business days'){?>selected="selected"<?php } ?>>1-2 business days</option>
			<option class="range-1-3" <?php if($productDetails->row()->ship_duration=='1-3 business days'){?>selected="selected"<?php } ?>>1-3 business days</option>
			<option class="range-3-5" <?php if($productDetails->row()->ship_duration=='3-5 business days'){?>selected="selected"<?php } ?>>3-5 business days</option>
			<option class="range-5-10" <?php if($productDetails->row()->ship_duration=='1-2 weeks'){?>selected="selected"<?php } ?>>1-2 weeks</option>
			<option class="range-10-15" <?php if($productDetails->row()->ship_duration=='2-3 weeks'){?>selected="selected"<?php } ?>>2-3 weeks</option>
			<option class="range-15-20" <?php if($productDetails->row()->ship_duration=='3-4 weeks'){?>selected="selected"<?php } ?>>3-4 weeks</option>
			<option class="range-20-30" <?php if($productDetails->row()->ship_duration=='4-6 weeks'){?>selected="selected"<?php } ?>>4-6 weeks</option>
			<option class="range-30-40" <?php if($productDetails->row()->ship_duration=='6-8 weeks'){?>selected="selected"<?php } ?>>6-8 weeks</option>
			<!--<option value="custom">Custom range</option>-->
		</select> 
	</div>
</div>
<div class="form_fields">
	<label>Ships From<span style="color:red;"> *</span></label>
	<div class="form_fieldsgroup validation-input">
		<select name="country_code" id="country_code" tabindex="9" class="full required" style="border: #d8d8d8 1px solid; width: 340px; height: 31px;">
			<option value=""><?php if($this->lang->line('shop_sellocation') != '') { echo stripslashes($this->lang->line('shop_sellocation')); } else echo 'Select a location'; ?></option>
			<?php if($countryList->num_rows()>0){
			foreach ($countryList->result() as $countryVal){
			?>
				<option value="<?php echo $countryVal->id.'|'.$countryVal->country_code;  ?>" <?php if($productDetails->row()->country_code==$countryVal->country_code){?>selected="selected"<?php } ?>><?php echo $countryVal->name; ?></option> 
			<?php }}?>
		</select>
	</div>
</div>
<div class="form_fields">
	<label>Shipping Cost<span style="color:red;"> *</span></label>
</div>
<div class="form_fields">
	<table class="table table-striped" id="tbNames">
		<tbody>
			<tr>
				<th><?php if($this->lang->line('shop_shipsto') != '') { echo stripslashes($this->lang->line('shop_shipsto')); } else echo 'Ships to'; ?></th>
				<th><?php if($this->lang->line('shop_byitself') != '') { echo stripslashes($this->lang->line('shop_byitself')); } else echo 'By itself'; ?></th>
				<th><?php if($this->lang->line('shop_anotheritem') != '') { echo stripslashes($this->lang->line('shop_anotheritem')); } else echo 'With another item'; ?></th>
				<th></th>
			</tr>
			<?php $i=0; 
				if($PrdShipping->num_rows() >0){
				foreach($PrdShipping->result() as $shipdetail){  $i++; ?>
				 <tr id="tab_<?php echo $i;?>">
					<td>
						<p class="country-text" id="shipping_to_1_lab"><?php echo $shipdetail->ship_name;?></p><span><?php echo " : ".$currencySymbol;?></span>
						<input type="hidden" name="shipping_to[]" value="<?php echo $shipdetail->ship_id.'|'.$shipdetail->ship_code.'|'.$shipdetail->ship_name;?>" id="shiping_to_default" />
						<input type="hidden" name="ship_to_id[]" value="<?php echo $shipdetail->ship_id; ?>"  id="shipping_to_3_id"/>                       
					</td>
					<td>
						<input type="text" value="<?php echo number_format($shipdetail->ship_cost,2,'.','');?>" name="shipping_cost[]" class="form-control shipping_txt_bax" placeholder="<?php echo $currencySymbol;?>:"/>
					</td>
					<td>
						<input type="text" value="<?php echo number_format($shipdetail->ship_other_cost,2,'.','');?>" name="shipping_with_another[]" class="form-control shipping_txt_bax"  placeholder="<?php echo $currencySymbol;?>:"/>
					</td>
					<td>
						<?php if($i > 1) {
						echo '<a class="close_icon" style="margin:7px 0 0 5px" href="javascript:void(0)" id="'.($i).'"></a>';
						}?>
					</td>
				</tr>
			<?php }
			}else{?>
				<tr>
							<td>
								<p class="country-text" id="shipping_to_1_lab"><?php if($this->lang->line('shop_countryname') != '') { echo stripslashes($this->lang->line('shop_countryname')); } else echo 'Country Name'; ?></p>
								<input type="hidden" name="shipping_to[]" id="shiping_to_default">
								<select id="shipping_to_1" style="display:none;" class="shipping_to">
									<option value="" id="shiping_to_default"><?php if($this->lang->line('shop_countryname') != '') { echo stripslashes($this->lang->line('shop_countryname')); } else echo 'Country Name'; ?></option>
									<?php foreach($countryList->result() as $countryVal) {?>   
										<option value="<?php echo $countryVal->id.'|'.$countryVal->name;  ?>"><?php echo $country->name; ?></option> 
									<?php }?>
								</select>      
								<input type="hidden" name="ship_to_id[]" id="shipping_to_1_id">
							</td>
							<td>
								<input type="text" value="" name="shipping_cost[]" class="form-control shipping_txt_bax" placeholder="<?php echo $currencySymbol;?>:">
							</td>
							<td>
								<input type="text" value="" name="shipping_with_another[]" class="form-control shipping_txt_bax" placeholder="<?php echo $currencySymbol;?>:">
							</td>
							<td></td>
						</tr>
						<tr id="tab_1">
							<td>
								<p class="country-text" id="shipping_to_1_lab1"><?php if($this->lang->line('shop_everywhere') != '') { echo stripslashes($this->lang->line('shop_everywhere')); } else echo 'Everywhere Else'; ?></p>
								<input type="hidden" name="shipping_to[]" id="shipping_to_2" value="Everywhere Else">
								<input type="hidden" id="shipping_to_2_id" name="ship_to_id[]" value="232">
							</td>
							<td>
								<input type="text" value="" class="form-control shipping_txt_bax" name="shipping_cost[]" placeholder="<?php echo $currencySymbol;?>:">
							</td>
							<td>
								<input type="text" value="" class="form-control shipping_txt_bax" name="shipping_with_another[]" placeholder="<?php echo $currencySymbol;?>:">
							</td>
							<td>
								<a class="close_icon" href="javascript:void(0)" id="1"></a>
							</td>
						</tr>
						<tr id="tab_2">
							<td> 
								<p class="country-text" id="shipping_to_3_lab" ></p>
								<select id="shipping_to_3" class="shipping_to" onchange="display_sel_val(this);" name="shipping_to[]">
									<option value=""><?php if($this->lang->line('shop_sellocation') != '') { echo stripslashes($this->lang->line('shop_sellocation')); } else echo 'Select a location'; ?></option>
									<?php foreach($countryList->result() as $countryVal) {?>   
										<option value="<?php echo $countryVal->id.'|'.$countryVal->name; ?>"><?php echo $countryVal->name; ?></option> 
									<?php }?>
									<option value="232|Everywhere Else">Everywhere Else</option>
								</select>
								<input type="hidden" name="ship_to_id[]" id="shipping_to_3_id">
								<input type="hidden" name="shipping_to[]" id="shipping_to_3_name">
							</td>
							<td>
								<input type="text" value="" class="form-control shipping_txt_bax" name="shipping_cost[]" placeholder="<?php echo $currencySymbol;?>:">
							</td>
							<td>
								<input type="text" value="" class="form-control shipping_txt_bax" name="shipping_with_another[]" placeholder="<?php echo $currencySymbol;?>:">
							</td>
							<td>
								<a class="close_icon" href="javascript:void(0)" id="2"></a>
							</td>
						</tr>
			<?php } ?>
</tbody>
	</table>
	<p id="selected_country" style="display:none;"></p>
	<input type="button" value="Add location" class="btn_small btn_blue" style="width:100px;" id="btnAdd">
	<img src="images/ajax-loader/ajax-loader(1).gif" alt="Loading" class="search_btn" id="stimg">
	<div>
</div>
						<input type="hidden" name="PID" value="<?php echo $productDetails->row()->seller_product_id;?>"/>
						<br/>
						<div class="form_fields">
						<label></label>
							<div class="form_fieldsgroup">
								<input type="submit" id="editDetailsSub" value="<?php if($this->lang->line('header_save') != '') { echo stripslashes($this->lang->line('header_save')); } else echo "Save"; ?>" class="button"/>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>             
	</form>
</div>
</div>
</div>
		
		<!-- / wrapper-content -->

		
		<?php 
     $this->load->view('site/templates/footer_menu');
     ?>
		
		<a id="scroll-to-top" href="#header" style="display: none;"><span><?php if($this->lang->line('signup_jump_top') != '') { echo stripslashes($this->lang->line('signup_jump_top')); } else echo "Jump to top"; ?></span></a>

	</div>

<script src="js/site/<?php echo SITE_COMMON_DEFINE ?>filesjquery_zoomer.js" type="text/javascript"></script>
<script type="text/javascript" src="js/site/<?php echo SITE_COMMON_DEFINE ?>selectbox.js"></script>
<script type="text/javascript" src="js/site/thing_page.js"></script>
<script type="text/javascript" src="js/jquery.MultiFile.js"></script>
<script type="text/javascript" src="js/site/jquery.validate.js"></script>

<script>
	$("#sellerProdEdit1").validate();
</script>
<style>
.table{ width: 100%;
    max-width: 100%;
    margin-bottom: 20px;     border-collapse: collapse;
    border-spacing: 0;
}
.table-striped{border:1px solid #ccc;}
#custom_shipping_time_li{display:none; margin-top:10px;}
table, th {
    border: 1px solid #ccc; min-height: 1px;
    padding-left: 15px;
    padding-right: 15px;
    position: relative;
}	
.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td{
 border-top: 1px solid #ddd;
    line-height: 1.42857;
    padding: 8px;
    vertical-align: top;
}
.table-striped > tbody > tr:nth-child(2n+1) > td, .table-striped > tbody > tr:nth-child(2n+1) > th{
	background-color: #f9f9f9;
}
.errors{border:1px solid red !important}
.shipping_to{width:200px; padding: 3px 4px; box-shadow: none; margin: 0px; border: 1px solid rgb(205, 205, 205); display:block;}
.country-text{margin:0px;}
.close_icon {
    float: right;
    background: url(images/close.png) no-repeat;
    height: 15px;
    width: 15px;
    margin:6px 7px 0px 0px;
}
.left {
    float: left !important;
}
#stimg {display:none;width:15px; height:15px; background:none;border:none;}
.btn_blue {
    background-position: 0 -340px !important;
    font-size: 11px;
    padding: 0 10px;
    color: #fff;
    border: #093868 1px solid;
    text-shadow: 1px 1px 0px #333;
    margin-right: 5px;
}
.btn_small {
    height: 30px;
    display: inline-block;
    background: url(images/button-sprite.png) repeat-x;
    cursor: pointer;
    line-height: normal !important;
    line-height: 28px;
    box-shadow: 0 0 3px #ddd;
    text-transform: uppercase;
}
</style>
<?php
$this->load->view('site/templates/footer');
?>