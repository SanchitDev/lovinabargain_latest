<?php
 $this->load->view('site/templates/header'); ?>
<div id="content">
	<div id="home-timeline">
		<div class="figure-product">
		<?php if($flash_data != '') { ?>
			<div class="errorContainer" id="<?php echo $flash_data_type;?>">
				<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
				<p><span><?php echo $flash_data;?></span></p>
			</div>
		<?php } ?>
		<?php 
		//echo"<pre>"; print_r($productDetails->result()); die;
			if($productDetails->num_rows()==1){
				$img = 'dummyProductImage.jpg';
				$imgArr = explode(',', $productDetails->row()->image);
				if (count($imgArr)>0){
					foreach ($imgArr as $imgRow){
						if ($imgRow != ''){
							$img = $pimg = $imgRow;
							break;
						}
				}}
				$fancyClass = 'fancyy';
				$fancyText = LIKE_BUTTON;
				if (count($likedProducts)>0 && $likedProducts->num_rows()>0){
					foreach ($likedProducts->result() as $likeProRow){
						if ($likeProRow->product_id == $productDetails->row()->seller_product_id){
							$fancyClass = 'fancyd';
							$fancyText = LIKED_BUTTON;
							break;
						}
				}}
				$featureClass = 'feature';
				$featureText = 'Feature on my profile';
				if($loginCheck !=''){
					$featureArr = explode(',', $userDetails->row()->feature_product);
					if (in_array($productDetails->row()->seller_product_id, $featureArr)){
						$featureClass = 'featured';
						$featureText ="Featured on my profile";	
				} }
				$wantClass = 'want';
				$wantlisText = "Want";
				$wantText ="Want it";
				$want_icon = 'icon';
				if($loginCheck !=''){
				
				

					$ownArr = explode(',', $wntdet->row()->product_id);
//echo $productDetails->row()->seller_product_id.'<br>';					
	//				print_r($ownArr);die;
					if (in_array($productDetails->row()->seller_product_id, $ownArr)){
						$wantClass = 'wanted';
						$wantText ="You want it";
						$want_icon = 'icon';
						$wantlisText = "Wanted";
						
				} }
				$ownClass = 'own';
				$ownText ="Own it";
				$own_icon = 'icon';
				if($loginCheck !=''){
					$wantArr = explode(',', $userDetails->row()->own_products);	
					if (in_array($productDetails->row()->seller_product_id, $wantArr)){
						$ownClass = 'owned';
						$ownText = "I Own it";
						$own_icon = 'icon';
						
				} }
				$url_link = 'things/'.$productDetails->row()->id.'/'.$productDetails->row()->seourl;
		?>
			<div class="figure">
							<span class="wrapper-fig-image">
							
							<?php 

							
	$slider_skin = 'light-4';?>
	
	<!-- Master Slider Start -->

<!-- Base MasterSlider style sheet -->
<link rel="stylesheet" href="<?php echo DESKTOPURL; ?>masterslider/style/masterslider.css" />
 
<!-- MasterSlider default skin -->
<link rel="stylesheet" href="<?php echo DESKTOPURL; ?>masterslider/skins/<?php echo $slider_skin;?>/style.css" />
 
<!-- MasterSlider main JS file -->
<script src="<?php echo DESKTOPURL; ?>masterslider/masterslider.min.js"></script>

<!-- masterslider -->
	
					<div class="master-slider ms-skin-<?php echo $slider_skin;?>" id="masterslider">
							<?php 
							//print_r($image);die;
							foreach ($image as $img){
								if($img != '')
								{							?>
                                <div class="ms-slide">
								<img src="<?php echo DESKTOPURL;?>images/product/<?php echo $img;?>" alt="<?php echo $productDetails->row()->product_name;?>" height="640" width="630">
								<img class="ms-thumb" src="<?php echo DESKTOPURL;?>images/product/<?php echo $img;?>" alt="<?php echo $productDetails->row()->product_name;?>"/>
								
								
								</div>
								<?php }}?>
								<?php if(($productDetails->row()->youtube) != ''){?>
        <div class="ms-slide">
								<!-- youtube video -->
								
								
					 
			<?php 	$vid= $productDetails->row()->youtube;  ?>

<img src="http://img.youtube.com/vi/<?php echo $vid;?>/0.jpg" alt="<?php echo $productDetails->row()->product_name;?>" height="640" width="630">
	<img class="ms-thumb" src="http://img.youtube.com/vi/<?php echo $vid;?>/0.jpg" alt="<?php echo $productDetails->row()->product_name;?>"/>
          <a href="https://www.youtube.com/embed/<?php echo $vid;?>?hd=1&wmode=opaque&controls=1&showinfo=0" data-type="video"><img src="http://img.youtube.com/vi/<?php echo $vid;?>/0.jpg"/> </a>
    </div> <?php }?>
								</div>
		</span>	 
				<figcaption><?php echo $productDetails->row()->product_name;?></figcaption>
				<b class="price"><?php echo $currencySymbol;?><span><?php echo $productDetails->row()->sale_price;?></span></b>
					<span>∙</span>
				<a class="username" href="user/<?php echo $productDetails->row()->user_name;?>"><?php echo $productDetails->row()->user_name;?></a> + <?php echo $productDetails->row()->likes;?>                             
			</div>
			<div class="option">
				<a href="javascript:void(0);" tid= "<?php echo $productDetails->row()->seller_product_id;?>" class="<?php echo $fancyClass;?>" <?php if ($loginCheck==''){?>require_login="true"<?php }?>><i class="icon"></i><?php echo $fancyText;?></a>
				<!--<a require_login="<?php if (count($userDetails)>0){echo 'false';}else {echo 'true';}?>" href="#" class="list list_popup"><i class="icon"></i>Add to list</a>-->
                
                <a href="#"  data-tid="<?php echo $productDetailRow->seller_product_id;?>" require_login="<?php if (count($userDetails)>0){echo 'false';}else {echo 'true';}?>" class="list list_popup"><i class="want_icon"></i><?php if($this->lang->line('header_add_list') != '') { echo stripslashes($this->lang->line('header_add_list')); } else echo "Add to List"; ?></a>
                
                
                 <?php if ($loginCheck != ''  && ($userDetails->row()->id == $productDetails->row()->user_id)){?>
						<a id="edit-details" class="" href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit" style="float:left;"><?php if($this->lang->line('product_edit_dtls') != '') { echo stripslashes($this->lang->line('product_edit_dtls')); } else echo "Edit details"; ?><i class="won_icon"></i></a>
                        
						<a uid="<?php echo $productDetails->row()->user_id;?>" thing_id="<?php echo $productDetails->row()->seller_product_id;?>" ntid="7220865" class="remove_new_thing" href="things/<?php echo $productDetails->row()->seller_product_id;?>/delete" onclick="return confirm('Are you sure?');" style="float:left;"><?php if($this->lang->line('shipping_delete') != '') { echo stripslashes($this->lang->line('shipping_delete')); } else echo "Delete"; ?><i class="color_icon"></i></a>
                        <?php }?>  
                
				<a href="#" class="thing-url comment" style="text-indent:-1000px"></a>
			</div>
			<div class="addtocart">
				<?php if($productDetails->row()->product_type == "digital"){?>
					<button class="greenbutton buyclick "  id="product-download-code" type="button" style="margin-bottom:10px;" <?php if($loginCheck==''){ ?>require-login='true'<?php }?>><?php if($this->lang->line('header_product_download') != '') { echo stripslashes($this->lang->line('header_product_download')); } else echo "Download";?></button>
				<?php }?>
				
				<button class="greenbutton buyclick reg-popup" type="button"><?php echo site_lg('lg_buy_now', 'Buy Now');?></button>
				<?php if( $productDetails->row()->sku_category != ''){ ?>
					<button class="greenbutton buyclick reg-popup" type="button"><?php echo site_lg('lg_create_auction', 'Create auction');?></button>
					<i>
						<div class="auction_hover">
							<p>why not create an auction and get this item for less. Sellers will bid against each other to sell to you.</p>
						</div>                                       
					</i>
				<?php } ?>
				<ul class="social-icons">
					<li><a class="<?php echo $featureClass;?>" href="javascript:void(0);" tid= "<?php echo $productDetails->row()->seller_product_id;?>" <?php if ($loginCheck==''){?>require_login="true"<?php }?>><span class="icon"></span><?php echo $featureText;?></a></li>
					<li><a class="<?php echo $wantClass;?>" href="javascript:void(0);" tid= "<?php echo $productDetails->row()->seller_product_id;?>" <?php if ($loginCheck==''){?>require_login="true"<?php }?>><span class="<?php echo $want_icon;?>"></span><?php echo $wantText;?></a></li>
					<li><a class="<?php echo $ownClass; ?>" href="javascript:void(0);" tid= "<?php echo $productDetails->row()->seller_product_id;?>" <?php if ($loginCheck==''){?>require_login="true"<?php }?>><span class="<?php echo $own_icon;?>"></span><?php echo $ownText;?></a></li>
				</ul>
				<div class="responce">
					<a target="_blank" href="http://www.stumbleupon.com/submit?url=<?php echo current_url()?>"><img src="images/steelfire.png" /></a>
					<a target="_blank" href="https://twitter.com/share" class="twitter-share-button" data-via="ganesh29491460"><img src="images/tweet.png" /></a>
					<a href="https://plus.google.com/share?url={<?php echo current_url()?>}" onclick="javascript:window.open(this.href,
						'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img
						src="images/g+.png" alt="Share on Google+"/></a>
					<!--<iframe src="//www.facebook.com/plugins/like.php?href=<?php echo current_url()?>;width&amp;layout=button_count&amp;action=like&amp;show_faces=true&amp;share=false&amp;height=21&amp;appId=293302254177375" 
					scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px;" 
					allowTransparency="true"></iframe>-->
					<!-- Place this tag in your head or just before your close body tag. -->
					
				
<!-- 				<a class="report-page" href="#"><span class="icon"></span>Report inappropriate</a> -->
			</div>
		</div>
<script>
    var slider = new MasterSlider();
    slider.setup('masterslider' , {
            width:400,    // slider standard width
            height:350,   // slider standard height
            space:5,
            fullwidth:true,
           // autoHeight:true,
            smoothHeight: true,
            swipe:true,
            autoplay:true,
            loop:true,
            shuffle:false,
            overPause:true,
            dir:"h",
//             fillMode:"stretch", //values are "fill", "fit", "stretch", "center" and "tile".
             layout:"fillwidth",
            view:"flow" //basic, fade, mask, wave, flow, scale
        });
    // adds Arrows navigation control to the slider.
    slider.control('arrows');
    slider.control('thumblist' , {autohide:false ,dir:'h',arrows:false});
    
</script>                       
<script type="text/javascript">
$(document).ready(function(){
	$(".comment-post").click(function() {
		var requirelogin = $(this).attr('require-login');
		if(requirelogin){
			var thingURL = 'signup?next=<?php echo $url_link ;?>';
			alert("Login required");
			window.location = baseURL+thingURL;
			return false;
		}
		var comments = $("#comments").val();
		var product_id = $("#cproduct_id").val();
		var dataString = '&comments=' + comments + '&cproduct_id=' + product_id;
		if(comments==''){
			alert('Your comment is empty');
		}else{
			$("#flash").show();
			$("#flash").fadeIn(400).html('<img src="images/ajax-loader.gif" align="absmiddle">&nbsp;<span class="loading">Loading Comment...</span>');
			$.ajax({
			type: "POST",
			url: baseURL+'site/order/insert_product_comment',
			data: dataString,
			cache: false,
			dataType:'json',
			success: function(json){
					if(json.status_code == 1){
						alert('Your comment is waiting for approval');
						window.location.reload();
					}
					document.getElementById('comments').value='';
					$("#flash").hide();
				}
			});
		}
		return false;
	});
	$('.list_popup').click(function(){
		$('#list_container').attr("tid","<?php echo $productDetails->row()->seller_product_id;?>");
/* 		$('#i-want-this').attr("class","<?php echo $wantClass;?>");
		$('#i-want-this b').text("<?php echo $wantlisText;?>"); */
		$('.list-categories ul').html('Loading...');
		$.ajax({
			type:'POST',
			url:baseURL+'site/user/add_list_when_fancyy',
			data:{tid:"<?php echo $productDetails->row()->seller_product_id;?>"},
			dataType:'json',
			success:function(response){
				if(response.status_code == 1){
					$('.list-categories ul').html(response.listCnt);	
				}
				if(response.wanted == 1){
					$('.btn-want').addClass('wanted').find('b').text('Wanted');
				}
			}
		});
	}); 
});


function moreComments(pid){
	$.ajax({
			//alert('123 from ajax fn');
			type:'POST',
			url:baseURL+'site/product/get_more_comments',
			data:{'pid':pid},
			dataType:'json',
			success:function(response){
				$('.user-comment').html(response.cnt);
				$('.show-comment').hide();
				return false;
			},
			complete:function(){
				return false;
			}
		});
		return false;
}
</script> 
<?php 
if($productComment->num_rows() > 0){
	$commentCount = $productComment->num_rows();
}else{
	$commentCount = 0;
}
?>
		<div class="figure-product">
			<h2 class="comments-title"><?php echo site_lg('lg_comments', 'Comments');?></h2>
           <?php if($activeCount->num_rows()> 5){
		   
		   
		   
		   ?>
			<a class="show-comment" href="#" onclick="return moreComments('<?php echo $productDetails->row()->seller_product_id;?>')"><?php echo site_lg('lg_view', 'View');?><span class="number_comment">&nbsp;<!--<?php echo $commentCount;?>-->&nbsp;</span><?php echo site_lg('lg_more_comments', 'More Comments');?></a>
            <?php } ?>
			<ul class="user-comment">
			<?php 
			//echo "<pre>";print_r($productComment->result());die;
			if($productComment->num_rows() > 0){
					$limitComment = 0;
					foreach($productComment->result() as $cmtrow){
							if($cmtrow->status == 'Active'){ 
								if($limitComment<5){
									$limitComment++;
							?>
				<li><a href="<?php echo base_url();?>user/<?php echo $cmtrow->user_name;?>">
                    <img src="<?php echo DESKTOPURL;?>images/users/<?php if($cmtrow->thumbnail!=''){ echo $cmtrow->thumbnail;}else{echo 'user-thumb.png';}?>" />
                    <b class="user-name"><?php echo ucfirst($cmtrow->user_name);?></b>
                    </a><?php echo $cmtrow->comments;?>
				</li> 
			<?php }}else{
				if($loginCheck != '' && $loginCheck == $productDetails->row()->user_id){ ?>
				<li><a href="<?php echo base_url();?>user/<?php echo $cmtrow->user_name;?>">
                    <img src="<?php echo DESKTOPURL;?>images/users/<?php if($cmtrow->thumbnail!=''){ echo $cmtrow->thumbnail;}else{echo 'user-thumb.png';}?>" />
                    <b class="user-name"><?php echo ucfirst($cmtrow->user_name);?></b>
                    </a><?php echo $cmtrow->comments;?>
					<p style="float:left;width:100%;text-align:left;">
						<a style="font-size: 11px; color: #188A0E;cursor:pointer;" onclick="javascript:approveCmt(this);" data-uid="<?php echo $cmtrow->CUID;?>" data-tid="<?php echo $productDetails->row()->seller_product_id;?>" data-cid="<?php echo $cmtrow->id;?>"><?php if($this->lang->line('product_approve') != '') { echo stripslashes($this->lang->line('product_approve')); } else echo "Approve"; ?></a>
						<a style="font-size: 11px; color: #f33;cursor:pointer;" onclick="javascript:deleteCmt(this);" data-tid="<?php echo $productDetails->row()->seller_product_id;?>" data-cid="<?php echo $cmtrow->id;?>"><?php if($this->lang->line('product_delte') != '') { echo stripslashes($this->lang->line('product_delte')); } else echo "Delete"; ?></a>
					</p>
				</li>
			<?php }else{
				if($loginCheck == $cmtrow->CUID){ ?>
				<li><a href="<?php echo base_url();?>user/<?php echo $cmtrow->user_name;?>">
                    <img src="<?php echo DESKTOPURL;?>images/users/<?php if($cmtrow->thumbnail!=''){ echo $cmtrow->thumbnail;}else{echo 'user-thumb.png';}?>" />
                    <b class="user-name"><?php echo ucfirst($cmtrow->user_name);?></b>
                    </a><?php echo $cmtrow->comments;?>
					<p style="float:left;width:100%;text-align:left;font-size: 11px; color: #188A0E; cursor:pointer;">
						<?php if($this->lang->line('product_wait_appr') != '') { echo stripslashes($this->lang->line('product_wait_appr')); } else echo "Waiting for approval"; ?>
						<a style="font-size: 11px; color: #f33;margin-left:10pxcursor:pointer;" onclick="javascript:deleteCmt(this);" data-tid="<?php echo $productDetails->row()->seller_product_id;?>" data-cid="<?php echo $cmtrow->id;?>"><?php if($this->lang->line('product_delte') != '') { echo stripslashes($this->lang->line('product_delte')); } else echo "Delete"; ?></a>
					</p>
				</li>
				<?php }}}
				}}?>
			</ul>                             
			<form class="post-comment" action="#" method="post" style="margin:7px 0px 0px;">
				<fieldset>
					<input type="hidden" id="cproduct_id" value="<?php echo $productDetails->row()->seller_product_id;?>"/>
					<input type="hidden"  id="sell_id" value="<?php echo $productDetails->row()->user_id; ?>">
					 <input type="hidden" class="option number" name="login_id" id="login_id" value="<?php echo $loginCheck; ?>">
					<input type="text" id="comments" placeholder="Write a comment..." name="comments">
					<button <?php if($loginCheck==''){ ?>require-login='true'<?php }?> class="comment-post"><?php echo site_lg('lg_post', 'Post');?></button>
				</fieldset>
			</form>
		</div>
		<div class="figure-product">
			<h2 class="comments-title"><?php if($this->lang->line('giftcard_you_might') != '') { echo stripslashes($this->lang->line('giftcard_you_might')); } else echo "You might also"; ?> <?php echo $siteTitle;?>...</h2>
			<ul class="product-image">
			<?php if(count($relatedProductsArr)>0){
				$limitCount = 0;
				foreach($relatedProductsArr as $relatedRow){
					if($productDetails->row()->seller_product_id!=$relatedRow->seller_product_id){
				if($limitCount<3){
				$limitCount++;
				$img = 'dummyProductImage.jpg';
				$imgArr = explode(',', $relatedRow->image);
				if(count($imgArr)>0){
					foreach ($imgArr as $imgRow){
						if ($imgRow != ''){
							$img = $imgRow;
							break;
						}
					}
				}
				if($relatedRow->web_link!=''){
					$product_link = 'user/'.$relatedRow->user_name.'/things/'.$relatedRow->seller_product_id.'/'.url_title($relatedRow->product_name,'-');
					$sale_price = '';
				}else{
					$product_link ='things/'.$relatedRow->id.'/'.url_title($relatedRow->product_name,'-'); 
					$sale_price = $currencySymbol.$relatedRow->sale_price;
				}
			?>
				<li><a class="listing-img" href="<?php echo $product_link;?>"><img src="<?php echo DESKTOPURL;?>images/product/<?php echo $img;?>">
					<?php echo $relatedRow->product_name?>
					<b><?php echo $sale_price;?></b>
					</a>
				</li>
				<?php }}}}?>
			</ul>
		</div>
		<?php $liked = LIKED_BUTTON;?>
		<div class="figure-product">
			<h2 class="comments-title"><?php echo $liked;?><?php echo site_lg('user_by', 'by');?></h2>
				<?php if($recentLikeArr->num_rows()>0){
					//echo "<pre>"; print_r($recentLikeArr->result()); 
					foreach($recentLikeArr->result() as $userRow){
						if($userRow->user_id != '' && $userRow->user_id != $loginCheck){
							$userImg = 'user-thumb1.png';
							if ($userRow->thumbnail != ''){
								$userImg = $userRow->thumbnail;
							}
						/* $followClass = 'follow';
						$followText = 'Follow';
						if ($loginCheck != ''){
							$followingListArr = explode(',', $userDetails->row()->following);
							if (in_array($userRow->user_id, $followingListArr)){
								$followClass = 'following';
								$followText = 'Following';
							}
						} */
						
						$followClass = 'follow';
						if ($loginCheck != ''){
						$followingListArr = explode(',', $userDetails->row()->following);
						if (in_array($userRow->user_id, $followingListArr)){
							$followClass = 'follow2';
						}
					} 
						
						
				?>
				<dl style="border:none" class="userinformation">
				<dt>
                <?php if($userRow->user_id != $loginCheck){ ?>	
					<button <?php if ($loginCheck==''){?> onclick="javascript:window.location.href = 'login';" <?php }?> class="button-<?php echo $followClass; ?>" uid="<?php echo $userRow->user_id;?>"><i class="icon"></i></button>   
                    <?php }?>          
					<a href="<?php echo base_url().'user/'.$userRow->user_name;?>"><img height="60" width="60" src="<?php echo DESKTOPURL;?>images/users/<?php echo $userImg;?>" class="member">
					<b><?php echo $userRow->full_name;?></b></a>
				</dt>
				<dd class="follow-image">
				<?php 
				//echo "<pre>"; print_r($recentUserLikes[$userRow->user_id]->result());die;
					if($recentUserLikes[$userRow->user_id]->num_rows()>0){
						foreach($recentUserLikes[$userRow->user_id]->result() as $userLikeRow){
							if($userLikeRow->product_name != ''){
								$img = 'dummyProductImage.jpg';
								$imgArr = explode(',',$userLikeRow->image);
								if(count($imgArr)>0){
									foreach ($imgArr as $imgRow){
										if ($imgRow != ''){
											$img = $imgRow;
											break;
										}
									}
								}
				if($userLikeRow->web_link!=''){
					$product_link = 'user/'.$userRow->user_name.'/things/'.$userLikeRow->seller_product_id.'/'.url_title($userLikeRow->product_name,'-');
				}else{
					$product_link ='things/'.$userLikeRow->PID.'/'.url_title($userLikeRow->product_name,'-'); 
				}
				?>
					<a href="<?php echo $product_link;?>"><img height="60" width="60" src="<?php echo DESKTOPURL;?>images/product/<?php echo $img;?>" alt="<?php echo $userLikeRow->product_name;?>" ></a>
				
				<?php }}}?>
				</dd>
				</dl>
				<?php }}}?>
		</div>
		<?php } else{ ?>
			<p style=""><?php if($this->lang->line('fancy_prod_unavail') != '') { echo stripslashes($this->lang->line('fancy_prod_unavail')); } else echo "This product details not available"; ?></p>
			<a href="#header" id="scroll-to-top"><span><?php if($this->lang->line('signup_jump_top') != '') { echo stripslashes($this->lang->line('signup_jump_top')); } else echo "Jump to top"; ?></span></a>
		<?php } ?>
	</div>      
</div>
<script>
$('#product-download-code').click(function(){
	var requirelogin = $(this).attr('require-login');

	if(requirelogin){
	
		var thingURL = 'login?next=things/<?php echo $productDetails->row()->id;?>/<?php echo $productDetails->row()->seourl;?>';
		
		window.location = baseURL+thingURL;
		
		return false;
	}
	
	
	var msg = window.prompt("Enter your Product Code", "");
	url = '<?php echo base_url()?>site/product/checkDownload';
	if(msg){
		$.post(url,{'code':msg},function(data){
			if(data == 0){
				alert("This code Already Expired");
			}else{
				window.location = '<?php echo current_url();?>?pfile='+data+'&id='+msg;
			}
		});
	}else{
		return false;
	}	
});

function pls_login(){

}
</script>