<?php 
$this->load->view('site/templates/header.php');
?>


<?php if($flash_data != '') { ?>
	<div class="errorContainer" id="<?php echo $flash_data_type;?>">
		<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
		<p><span><?php echo $flash_data;?></span></p>
	</div>
<?php } ?>
<div id="content" class="cart-page">
	<div class="account-wrap">
		<div class="account-page">
        <dl class="profile-account"><dd>
       <h2> <?php if($this->lang->line('product_edit') != '') { echo stripslashes($this->lang->line('product_edit')); } else echo "Edit Product"; ?></h2>
        </dd></dl>
         <form accept-charset="utf-8" method="post" action="site/product/sell_it/list" id="sellerProdEdit1">
         <section class="left-section min_height" style="height: 924px;">	
                        <div class="person-lists bs-docs-example">
                        <div class="figure-product">
		<ul class="tab-menu">
          <li class="active"><a data-toggle="tab" <?php if ($editmode != '0'){?>href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit"<?php }?>><?php if($this->lang->line('product_details') != '') { echo stripslashes($this->lang->line('product_details')); } else echo "Details"; ?></a></li>
                                    <li class=""><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('categories')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/categories"<?php }?>><?php if($this->lang->line('product_categories') != '') { echo stripslashes($this->lang->line('product_categories')); } else echo "Categories"; ?></a></li>
                                    <li><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('list')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/list"<?php }?>><?php if($this->uri->segment(4) == 'list'){?> <span style="color:#4169E1;"><?php } ?><?php if($this->lang->line('display_lists') != '') { echo stripslashes($this->lang->line('display_lists')); } else echo "List"; ?></a></li>
                                    <li><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('images')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/images"<?php }?>><?php if($this->lang->line('product_images') != '') { echo stripslashes($this->lang->line('product_images')); } else echo "Images"; ?></a></li>
                                    
                                    <?php if ($productDetails->row()->product_type=='digital'){?>
                                    <li><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('pdf')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/doc"<?php }?>><?php if($this->lang->line('display_pdf') != '') { echo stripslashes($this->lang->line('display_pdf')); } else echo "Pdf"; ?></a></li>
                                    <?php } else {?>
									<li><a data-toggle="tab" href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/shipping"><?php if($this->lang->line('header_shipping') != '') { echo stripslashes($this->lang->line('header_shipping')); } else echo "Shipping"; ?></a></li>
                                    <li><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('attribute')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/attribute"<?php }?>><?php if($this->lang->line('header_attr') != '') { echo stripslashes($this->lang->line('header_attr')); } else echo "Attribute"; ?></a></li>
                                    <?php }?>
                                    <li><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('seo')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/seo"<?php }?>><?php if($this->lang->line('product_seo') != '') { echo stripslashes($this->lang->line('product_seo')); } else echo "SEO"; ?></a></li>
		</ul> </div> 
         <script>
		   function saveDetails(mode){
           $('#nextMode').val(mode);
		   $('#editDetailsSub').trigger('click');
           }
         </script>
         
          
	</div>
    
     <dl class="profile-account"><dd>
        
         <input type="hidden" name="nextMode" id="nextMode" value="list"/>
		     <div class="tab-content border_right width_100 pull-left" id="myTabContent"> 
                            <p class="name-account"><label class="label"><?php if($this->lang->line('product_add_rem') != '') { echo stripslashes($this->lang->line('product_add_rem')); } else echo "You can add and remove list values here. This product will comes under the added lists"; ?></label></p>
                                <div id="product_info" class="tab-pane active">
                                    <div class="form_fields">
                                        <div class="inputs" style="float: left;width:100%; border:1px dashed #1DB3F0;">
							            <div style="margin:12px;">
								            <div class="btn_30_blue">
												<a href="javascript:void(0)" id="add" class="tipTop" title="Add new attribute">
													<span class="btn_link"><?php if($this->lang->line('header_add') != '') { echo stripslashes($this->lang->line('header_add')); } else echo "Add"; ?></span>
												</a>
											</div>
								            <div class="btn_30_blue">
												<a href="javascript:void(0)" id="remove" class="tipTop" title="Remove last attribute">
													<span class="btn_link"><?php if($this->lang->line('product_remove') != '') { echo stripslashes($this->lang->line('product_remove')); } else echo "Remove"; ?></span>
												</a>
											</div>
							            </div>
							            <?php 
										$list_names = $productDetails->row()->list_name;
										$list_names_arr = explode(',', $list_names);
										$list_values = $productDetails->row()->list_value;
										$list_values_arr = explode(',', $list_values);
										if (count($list_names_arr)>0){
											foreach ($list_names_arr as $list_names_key=>$list_names_val){
							            ?>
							            <div style="float: left; margin: 12px 10px 10px; width:85%;" class="field">
							            	<div class="image_text" style="float: left;margin: 5px;margin-right:50px;">
							            		<span><?php if($this->lang->line('product_list_name') != '') { echo stripslashes($this->lang->line('product_list_name')); } else echo "List Name"; ?>:</span>
							            		<select name="attribute_name[]" onchange="javascript:changeListValuesUser(this,'<?php echo $list_values_arr[$list_names_key];?>')" style="width:200px;color:gray;width:206px;" class="">
													<option value="">--<?php if($this->lang->line('checkout_select') != '') { echo stripslashes($this->lang->line('checkout_select')); } else echo "Select"; ?>--</option>
							            			<?php 
							            			if ($mainColorLists->num_rows()>0){
							            			?>
								            		<option <?php if ($list_names_val == 1){echo 'selected="selected"';}?> value="<?php echo 1; ?>"><?php if($this->lang->line('color') != '') { echo stripslashes($this->lang->line('color')); } else echo "Color"; ?></option>
								            		<?php } ?>
							            		</select>
							            	</div>
							            	<div class="attribute_box attrInput" style="float: left;margin: 5px;" >
												 <span><?php if($this->lang->line('product_list_value') != '') { echo stripslashes($this->lang->line('product_list_value')); } else echo "List Value"; ?> :</span>&nbsp;
												 <select name="attribute_val[]" style="width:200px;color:gray;width:206px;">
												 <?php 
							            			if ($list_values_arr[$list_names_key] == ''){
							            			?>
													<option value="">--<?php if($this->lang->line('checkout_select') != '') { echo stripslashes($this->lang->line('checkout_select')); } else echo "Select"; ?>--</option>
													<?php }?>
												 </select>
											</div>
											<script type="text/javascript">
							            	$('.image_text').find('select').trigger('change');
							            	</script>
							            </div>
							            <?php 
							            	}
							            }
							            ?>	
							        </div>
                                    </div>
                                    
                                    
                                    <input type="hidden" name="PID" value="<?php echo $productDetails->row()->seller_product_id;?>"/>
                                    <div class="form_fields">
                                            <label></label>
                                            <div class="form_fieldsgroup">
                                            <input type="submit" id="editDetailsSub" value="<?php if($this->lang->line('header_save') != '') { echo stripslashes($this->lang->line('header_save')); } else echo "Save"; ?>" class="button btn-blue"/>
                                                                                  </div>
                                        </div>
                                    
                                                                        
                                </div>
                                
                            </div>
                        </div>
                    </section>
                                    
                                    
                                                  </form>                      
                               


         </dd></dl>
        
        </div></div>
<style>
.btn_30_blue .btn_link {
    border-left: 1px solid #05447f;
    box-shadow: 1px 0 0 rgba(255, 255, 255, 0.4) inset;
    display: inline-block;
    height: 30px;
    margin-left: 5px;
    margin-right: 3px;
    padding-left: 6px;
}
.btn_30_blue span {
    display: inline-block;
    float: left;
}
.btn_30_blue a {
    background: -moz-linear-gradient(center top , #19a6d3 0%, #0853a1 50%, #064492 51%, #02286e 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
    border: 1px solid #05447f;
    border-radius: 4px;
    box-shadow: 0 0 2px rgba(0, 0, 0, 0.8), 0 1px 1px -1px #fff inset;
    color: #fff;
    display: inline-block;
    height: 30px;
    line-height: 29px;
    padding: 0 5px;
    text-align: center;
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.5), 0 1px 0 rgba(255, 255, 255, 0.2);
}
		

.btn_30_light, .btn_30_dark, .btn_30_orange, .btn_30_blue {
    display: inline-block;
    margin: 5px;
    position: relative;
}
		</style>
<script type="text/javascript" src="js/site/jquery.validate.js"></script>
<script>
$(document).ready(function(){


	var i = 1;
	
	
	$('#add').click(function() { 
		$('<div style="float: left; margin: 12px 10px 10px; width:85%;" class="field">'+
				'<div class="image_text" style="float: left;margin: 5px;margin-right:50px;">'+
					'<span><?php if($this->lang->line('product_list_name') != '') { echo stripslashes($this->lang->line('product_list_name')); } else echo "List Name"; ?>:</span>&nbsp;'+
					'<select name="attribute_name[]" onchange="javascript:loadListValuesUser(this)" style="width:200px;color:gray;width:206px;" class="chzn-select">'+
						'<option value="">--<?php if($this->lang->line('checkout_select') != '') { echo stripslashes($this->lang->line('checkout_select')); } else echo "Select"; ?>--</option>'+
						<?php foreach ($atrributeValue->result() as $attrRow){ 
							if (strtolower($attrRow->attribute_name) != 'price'){
						?>
						'<option value="<?php echo $attrRow->id; ?>"><?php echo $attrRow->attribute_name; ?></option>'+
						<?php }} ?>
					 '</select>'+
				'</div>'+
				'<div class="attribute_box attrInput" style="float: left;margin: 5px;" >'+
					 '<span><?php if($this->lang->line('product_list_value') != '') { echo stripslashes($this->lang->line('product_list_value')); } else echo "List Value"; ?> :</span>&nbsp;'+
					 '<select name="attribute_val[]" style="width:200px;color:gray;width:206px;" class="chzn-select">'+
					 '<option value="">--<?php if($this->lang->line('checkout_select') != '') { echo stripslashes($this->lang->line('checkout_select')); } else echo "Select"; ?>--</option>'+
					 '</select>'+
				'</div>'+
		'</div>').fadeIn('slow').appendTo('.inputs');
		i++;
	});
	
	$('#remove').click(function() {
		$('.field:last').remove();
	});
	
	$('#reset').click(function() {
		$('.field').remove();
		$('#add').show();
		i=0;
	
	
	});
	
	
});
</script>