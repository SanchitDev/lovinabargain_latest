<?php 
$this->load->view('site/templates/header'); ?>
<script type="text/javascript">
$(document).ready(function(){
	$(".account-page").hide();
}); 
function readURL(input){
	if(input.files && input.files[0]){
		var reader = new FileReader();
		reader.onload = function(e){
			$('#img_prev').attr('src',e.target.result).width(135).height(136);
			$('#img_prev').css('display','block');
			$('.frame b').hide();
			$('.updating').hide();
			$(".btn-upload").text('');
			$(".btn-upload span").hide();
			$(".viaemail").hide();
			$(".account-page").show();
			
		};
		reader.readAsDataURL(input.files[0]);
	}
}
</script>
<div id="content" class="add">
	<form name="" method="post" action="site/product/add_new_thing" enctype="multipart/form-data">
	<div class="color-container  mar-bot">
        <h2 class="color-title"><?php echo site_lg('header_upload_to', 'Upload to');?> <?php echo $siteTitle;?></h2>
        <div class="frame">   
			<span class="icon" style="background: none;"> <input type="file" class="hidden" name="prod_image" id="prod_image"onchange="readURL(this);"/>
			<img id="img_prev" src="<?php echo DESKTOPURL;?>images/blank.gif" alt="" style="margin:-151px -71px 90px 0px; "/></span>
			<b><?php echo site_lg('lg_choose_photo', 'Choose a photo');?></b>
        </div>
        <div class="photo-update">
			<p class="updating"><span><?php echo site_lg('lg_no', 'No');?></span><?php echo site_lg('lg_file_selected', 'File Selected');?></p>
        </div>
	</div>
	<div class="color-container  mar-bot viaemail">
		<h2 class="color-title"><?php if($this->lang->line('via_email') != '') { echo stripslashes($this->lang->line('via_email')); } else echo "Via Email"; ?></h2>
		<p><?php if($this->lang->line('product_longtext_2') != '') { echo stripslashes($this->lang->line('product_longtext_2')); } else echo "You can email photos of things from any email program to this email address"; ?></p>
		<p class="send-email">
			<a href="mailto:<?php echo $siteContactMail;?>"><?php echo $siteContactMail;?></a>
		</p>
		<p><?php if($this->lang->line('product_longtext_1') != '') { echo stripslashes($this->lang->line('product_longtext_1')); } else echo "Use the email subject line to add a title to what you're posting, and the body to add a comment. The attached photo will be automatically posted to your collection."; ?></p>
	</div>             
	<!--<div class="color-container  mar-bot viaemail">
        <h2 class="color-title"><?php echo $siteTitle.' ';?><?php if($this->lang->line('bookmarklets') != '') { echo stripslashes($this->lang->line('bookmarklets')); } else echo "Bookmarklets"; ?> </h2>
        <p><?php if($this->lang->line('please_fillow_this_steps') != '') { echo stripslashes($this->lang->line('please_fillow_this_steps')); } else echo "Please follow the steps 1-3 to install the "; ?><?php echo $siteTitle;?> 
		<?php if($this->lang->line('bookmark_ipnone_ipad') != '') { echo stripslashes($this->lang->line('bookmark_ipnone_ipad')); } else echo " Bookmarklet for iPhone and iPod Touch."; ?></p>
		<p><b class="steps"><?php if($this->lang->line('step_1') != '') { echo stripslashes($this->lang->line('step_1')); } else echo "STEP 1"; ?>
			</b> <?php if($this->lang->line('bookmark_page_tapping') != '') { echo stripslashes($this->lang->line('bookmark_page_tapping')); } else echo "Bookmark this page by tapping the"; ?>
			<span class="share-icon"></span><?php if($this->lang->line('icon_below_select') != '') { echo stripslashes($this->lang->line('icon_below_select')); } else echo "icon below. Select"; ?>
			<b><?php if($this->lang->line('add_bookmark') != '') { echo stripslashes($this->lang->line('add_bookmark')); } else echo "Add Bookmark"; ?>
			</b><?php if($this->lang->line('and_then') != '') { echo stripslashes($this->lang->line('and_then')); } else echo "and then"; ?>
			<b><?php if($this->lang->line('header_save') != '') { echo stripslashes($this->lang->line('header_save')); } else echo "Save"; ?></b>
		</p>
		<p><b class="steps"><?php if($this->lang->line('step_2') != '') { echo stripslashes($this->lang->line('step_2')); } else echo "STEP 2"; ?></b>
			<?php if($this->lang->line('select_following_codet_hold') != '') { echo stripslashes($this->lang->line('select_following_codet_hold')); } else echo "Select the following code by tap and hold. Tap"; ?>
			<b><?php if($this->lang->line('select_all') != '') { echo stripslashes($this->lang->line('select_all')); } else echo "Select All"; ?></b>
			<?php if($this->lang->line('and_t') != '') { echo stripslashes($this->lang->line('and_t')); } else echo "and"; ?>
			<b><?php if($this->lang->line('copy') != '') { echo stripslashes($this->lang->line('copy')); } else echo "Copy"; ?></b>
			<?php if($this->lang->line('to_clipboard') != '') { echo stripslashes($this->lang->line('to_clipboard')); } else echo "to clipboard."; ?>
			<textarea class="text-areass">javascript:(function(){thefancy_username="dc636d7c6226b52b0dc06a98441f1116155da7a4";var script_id = "thefancy_tagger_bookmarklet_helper_js";var s = document.getElementById(script_id);var can_continue = true;if (s) {var t = window;try {if (t.thefancy_bookmarklet){t.thefancy_bookmarklet.tagger.clean_listeners();s.parentNode.removeChild(s);} else {can_continue = false;}} catch (e5) {can_continue = false;}};if (can_continue) {_my_script = document.createElement("SCRIPT");_my_script.type = "text/javascript";_my_script.id = script_id;_my_script.src ="http://www.thefancy.com/bookmarklet/fancy_tagger.js?x=" + (Math.random());document.getElementsByTagName("head")[0].appendChild(_my_script);}})();</textarea>
		</p>
		<p><b class="steps"><?php if($this->lang->line('step_3') != '') { echo stripslashes($this->lang->line('step_3')); } else echo "STEP 3"; ?></b>
			<?php if($this->lang->line('tap_the_t') != '') { echo stripslashes($this->lang->line('tap_the_t')); } else echo "Tap the"; ?>
			<span class="book-icon"></span><?php if($this->lang->line('icon_below_then') != '') { echo stripslashes($this->lang->line('icon_below_then')); } else echo "icon below and then"; ?>
			<b><?php if($this->lang->line('shipping_edit') != '') { echo stripslashes($this->lang->line('shipping_edit')); } else echo "Edit"; ?></b>
			<?php if($this->lang->line('select_bookmark_t') != '') { echo stripslashes($this->lang->line('select_bookmark_t')); } else echo ". Select the bookmark"; ?>
			<b><?php echo $siteTitle;?></b>
		</p>
		<p><?php if($this->lang->line('tap_address_hold_remove_it') != '') { echo stripslashes($this->lang->line('tap_address_hold_remove_it')); } else echo "Tap the address field and remove its content. Tap and hold in the address field to"; ?>
			<b><?php if($this->lang->line('paste') != '') { echo stripslashes($this->lang->line('paste')); } else echo "Paste"; ?></b>
			<?php if($this->lang->line('the_previous_code') != '') { echo stripslashes($this->lang->line('the_previous_code')); } else echo "the previous code. Tap"; ?>
			<b><?php if($this->lang->line('gift_done') != '') { echo stripslashes($this->lang->line('gift_done')); } else echo "Done"; ?></b>
			<?php if($this->lang->line('to_save_new_address_your') != '') { echo stripslashes($this->lang->line('to_save_new_address_your')); } else echo "to save the new address for your"; ?>
			<b><?php echo $siteTitle;?></b>
			<?php if($this->lang->line('bookmark_t') != '') { echo stripslashes($this->lang->line('bookmark_t')); } else echo "bookmark."; ?>
		</p>
		<p><?php if($this->lang->line('bookmark_things_fancyy') != '') 
		{ echo stripslashes($this->lang->line('bookmark_things_fancyy')); } else echo "From now on you just have to use your new bookmark to add things to"; ?><?php echo $siteTitle;?></p>
	</div>-->
	<div class="account-page">
			<dl class="profile-account">
				<dd>
					<p class="name-account"><label class="label"><?php echo site_lg('lg_title', 'Title');?></label>
						<input type="text" value="" name="name" required>
					</p>
					<p class="weblink"><label class="label"><?php echo site_lg('lg_web_link', 'Web Link');?></label>
						<input type="text" value="" name="link" required>
					</p>
					<p class="abo"><label class="label"><?php echo site_lg('lg_note', 'Note');?></label>
						<textarea id="setting-bio" name="note"></textarea>
						<small><?php echo site_lg('lg_optional', 'Optional');?></small>
					</p>
					<p class="years"><label class="label"><?php echo site_lg('lg_category', 'Category');?></label>
						<select name="category_id" required>
							<option value=""><?php if($this->lang->line('header_choose_categry') != '') { echo stripslashes($this->lang->line('header_choose_categry')); } else echo "Choose a category"; ?></option>
							<?php if ($mainCategories->num_rows()>0){
								foreach ($mainCategories->result() as $mainCat){
							?>
							<option value="<?php echo $mainCat->id;?>"><?php echo $mainCat->cat_name;?></option>
							<?php }}?>
						</select>
					</p>
				</dd>
				<div class="btn-list">
					<button class="profile-button" type="submit"><?php echo site_lg('lg_upload', 'Upload');?></button>
				</div>
			</dl>
		</div>
	</form>
</div>
</div>
</div>
</body>
<style type="text/css">
.hidden {display: block !important; height: 150px; opacity: 0; position: relative; width: 100%; z-index: 1;}
.account-page{background:#fff;}
</style>
</html>