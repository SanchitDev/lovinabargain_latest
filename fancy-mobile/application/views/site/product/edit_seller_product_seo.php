<?php 
$this->load->view('site/templates/header.php');
?>
<?php if($flash_data != '') { ?>
	<div class="errorContainer" id="<?php echo $flash_data_type;?>">
		<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
		<p><span><?php echo $flash_data;?></span></p>
	</div>
<?php } ?>
<div id="content" class="cart-page">
	<div class="account-wrap">
		<div class="account-page">
        <dl class="profile-account"><dd>
       <h2> <?php if($this->lang->line('product_edit') != '') { echo stripslashes($this->lang->line('product_edit')); } else echo "Edit Product"; ?></h2>
        </dd></dl>
 <form accept-charset="utf-8" method="post" action="site/product/sell_it/seo" id="sellerProdEdit1">
 <div class="figure-product">
<ul class="tab-menu">
          <li class="active"><a data-toggle="tab" <?php if ($editmode != '0'){?>href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit"<?php }?>><?php if($this->lang->line('product_details') != '') { echo stripslashes($this->lang->line('product_details')); } else echo "Details"; ?></a></li>
                                    <li class=""><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('categories')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/categories"<?php }?>><?php if($this->lang->line('product_categories') != '') { echo stripslashes($this->lang->line('product_categories')); } else echo "Categories"; ?></a></li>
                                    <li><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('list')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/list"<?php }?>><?php if($this->lang->line('display_lists') != '') { echo stripslashes($this->lang->line('display_lists')); } else echo "List"; ?></a></li>
                                    <li><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('images')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/images"<?php }?>><?php if($this->lang->line('product_images') != '') { echo stripslashes($this->lang->line('product_images')); } else echo "Images"; ?></a></li>
                                    
                                   <?php if ($productDetails->row()->product_type=='digital'){?>
                                    <li><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('pdf')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/doc"<?php }?>><?php if($this->lang->line('display_pdf') != '') { echo stripslashes($this->lang->line('display_pdf')); } else echo "Pdf"; ?></a></li>
                                    <?php } else {?>
									<li><a data-toggle="tab" href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/shipping"><?php if($this->lang->line('header_shipping') != '') { echo stripslashes($this->lang->line('header_shipping')); } else echo "Shipping"; ?></a></li>
                                    <li><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('attribute')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/attribute"<?php }?>><?php if($this->lang->line('header_attr') != '') { echo stripslashes($this->lang->line('header_attr')); } else echo "Attribute"; ?></a></li><?php }?>
                                    <li><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('seo')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/seo"<?php }?>><?php if($this->uri->segment(4) == 'seo'){?> <span style="color:#4169E1;"><?php } ?><?php if($this->lang->line('product_seo') != '') { echo stripslashes($this->lang->line('product_seo')); } else echo "SEO"; ?></a></li>
		</ul> </div> 
             <script>
		   function saveDetails(mode){
           $('#nextMode').val(mode);
		   $('#editDetailsSub').trigger('click');
           }
         </script>
          <input type="hidden" name="nextMode" id="nextMode" value="seo"/>
          <dl class="profile-account"><dd>
         <div class="tab-content border_right width_100 pull-left" id="myTabContent"> 
                                <div id="product_info" class="tab-pane active">
                                    <div class="form_fields">
                                       <p class="name-account"><label class="label"><?php if($this->lang->line('product_meta_ttle') != '') { echo stripslashes($this->lang->line('product_meta_ttle')); } else echo "Meta Title"; ?></label></p>
                                        <div class="form_fieldsgroup validation-input">
                                            <input type="text" class="global-input" placeholder="<?php if($this->lang->line('product_meta_ttle') != '') { echo stripslashes($this->lang->line('product_meta_ttle')); } else echo "Meta Title"; ?>" value="<?php echo $productDetails->row()->meta_title;?>" name="meta_title">                                        </div>
                                    </div>
                                    
                                    <div class="form_fields">
                                        <p class="name-account"><label class="label"><?php if($this->lang->line('product_meta_key') != '') { echo stripslashes($this->lang->line('product_meta_key')); } else echo "Meta Keyword"; ?></label></p>
                                        <div class="form_fieldsgroup validation-input">
                                            <input type="text" class="global-input" placeholder="<?php if($this->lang->line('product_meta_key') != '') { echo stripslashes($this->lang->line('product_meta_key')); } else echo "Meta Keyword"; ?>" value="<?php if ($editmode == '1'){echo $productDetails->row()->meta_keyword;}?>" name="meta_keyword">                                        </div>
                                    </div>
                                    
                                    <div class="form_fields">
                                            <p class="name-account"><label class="label"><?php if($this->lang->line('product_meta_desc') != '') { echo stripslashes($this->lang->line('product_meta_desc')); } else echo "Meta Description"; ?></label></p>
                                            <div class="form_fieldsgroup">
                                            <textarea class="global-input" placeholder="<?php if($this->lang->line('product_meta_desc') != '') { echo stripslashes($this->lang->line('product_meta_desc')); } else echo "Meta Description"; ?>" rows="5" cols="40" name="meta_description"><?php echo $productDetails->row()->meta_description;?></textarea><br /><br />                                            </div>
                                        </div>
                                    
                                    
                                    <input type="hidden" name="PID" value="<?php echo $productDetails->row()->seller_product_id;?>"/>
                                    <div class="form_fields">
                                            <label></label>
                                            <div class="form_fieldsgroup">
                                            <input type="submit" id="editDetailsSub" value="<?php if($this->lang->line('header_save') != '') { echo stripslashes($this->lang->line('header_save')); } else echo "Save"; ?>" class="button btn-blue"/>
                                                 </div>
                                        </div>
                                    
                                                                        
                                </div>
                                
                                
                            </div>
         </form>
          
          
          
          
          
          </dd></dl>
          </div</div></div>