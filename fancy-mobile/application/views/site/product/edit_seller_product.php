<?php 
$this->load->view('site/templates/header.php');
?>
<style>
li .active{ color:#3C71B5;

}



</style>
<?php if($flash_data != '') { ?>
	<div class="errorContainer" id="<?php echo $flash_data_type;?>">
		<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
		<p><span><?php echo $flash_data;?></span></p>
	</div>
<?php } ?>
<div id="content" class="cart-page">
	<div class="account-wrap">
		<div class="account-page">
        <dl class="profile-account"><dd>
       <h2> <?php if($this->lang->line('product_edit') != '') { echo stripslashes($this->lang->line('product_edit')); } else echo "Edit Product"; ?></h2>
        </dd></dl>
        <form accept-charset="utf-8" method="post" action="site/product/sell_it/1" onsubmit="return validate_desc();" id="sellerProdEdit1">
        <div class="figure-product">
		<ul class="tab-menu">
          <li><a data-toggle="tab" <?php if ($editmode != '0'){?>href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit"<?php }?>><?php if($this->uri->segment(4) == ''){?> <span style="color:#4169E1;"><?php } ?><?php if($this->lang->line('product_details') != '') { echo stripslashes($this->lang->line('product_details')); } else echo "Details"; ?></a></li>
                                    <li class=""><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('categories')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/categories"<?php }?>><?php if($this->lang->line('product_categories') != '') { echo stripslashes($this->lang->line('product_categories')); } else echo "Categories"; ?></a></li>
                                    <li><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('list')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/list"<?php }?>><?php if($this->lang->line('display_lists') != '') { echo stripslashes($this->lang->line('display_lists')); } else echo "List"; ?></a></li>
                                    <li><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('images')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/images"<?php }?>><?php if($this->lang->line('product_images') != '') { echo stripslashes($this->lang->line('product_images')); } else echo "Images"; ?></a></li>
                                   
                                    
                                    <li class="pdf_upload" <?php  if($editmode==0 || $productDetails->row()->product_type=='physical'){?>style="display: none;"<?php }?>><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('doc')"<?php }else {?>href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/doc"<?php }?>><?php if($this->lang->line('display_pdf') != '') { echo stripslashes($this->lang->line('display_pdf')); } else echo "Pdf"; ?></a></li>
                                    
                                    <?php if($productDetails->row()->product_type!='digital'){?>
										<li><a data-toggle="tab" href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/shipping"><?php if($this->lang->line('header_shipping') != '') { echo stripslashes($this->lang->line('header_shipping')); } else echo "Shipping"; ?></a></li>
									<?php }?>
                                    <li <?php if($productDetails->row()->product_type=='digital'){?>style="display: none;"<?php }?>><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('attribute')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/attribute"<?php }?>><?php if($this->lang->line('header_attr') != '') { echo stripslashes($this->lang->line('header_attr')); } else echo "Attribute"; ?></a></li>
                                    <li><a data-toggle="tab" <?php if ($editmode == '0'){?>onclick="return saveDetails('seo')"<?php }else {?> href="things/<?php echo $productDetails->row()->seller_product_id;?>/edit/seo"<?php }?>><?php if($this->lang->line('product_seo') != '') { echo stripslashes($this->lang->line('product_seo')); } else echo "SEO"; ?></a></li>
		</ul>  
        </div>
         <script>
		   function saveDetails(mode){
			
           $('#nextMode').val(mode);
		   $('#editDetailsSub').trigger('click');
           }
         </script>
         
          
    
     <dl class="profile-account"><dd>
        
         <input type="hidden" name="nextMode" id="nextMode" value=""/>

		          <p class="name-account"><label class="label"><?php if($this->lang->line('header_name') != '') { echo stripslashes($this->lang->line('header_name')); } else echo "Name"; ?><span style="color:red;"> *</span></label>
                   <input type="text" class="global-input required popup_add_input" placeholder="<?php if($this->lang->line('header_name') != '') { echo stripslashes($this->lang->line('header_name')); } else echo "Name"; ?>" value="<?php echo $productDetails->row()->product_name;?>" name="product_name">
          		</p>


                                        <?php if($this->config->item('aff_option') == 'true'){ ?>

              <p class="name-account"><label class="label"><?php if($this->lang->line('aff_product') != '') { echo stripslashes($this->lang->line('aff_product')); } else echo "Affiliate Option"; ?></label>
                   
                                        	<input type="radio" name="aff_product" <?php if ($editmode == '1'){if($productDetails->row()->aff_product == 'true'){echo 'checked="checked"';}}?> value="true"/><?php if($this->lang->line('prference_yes') != '') { echo stripslashes($this->lang->line('prference_yes')); } else echo "Yes"; ?>&nbsp;&nbsp;&nbsp;
                                        	<input type="radio" name="aff_product" <?php if ($editmode == '1'){if($productDetails->row()->aff_product == 'false'){echo 'checked="checked"';}}else{echo 'checked="checked"';}?> value="false"/><?php if($this->lang->line('prference_no') != '') { echo stripslashes($this->lang->line('prference_no')); } else echo "No"; ?>&nbsp;&nbsp;&nbsp;
                                      
                  
                    </p>
             <?php } ?>
             <p class="name-account"><label class="label"><?php if($this->lang->line('header_description') != '') { echo stripslashes($this->lang->line('header_description')); } else echo "Description"; ?><span style="color:red;"> *</span></label>
                    <textarea class="global-input required mceEditor" placeholder="<?php if($this->lang->line('header_description') != '') { echo stripslashes($this->lang->line('header_description')); } else echo "Description"; ?>" id="description" rows="10" cols="40" name="description"><?php if ($productDetails->row()->description == ''){echo $productDetails->row()->excerpt;}else {echo $productDetails->row()->description;}?></textarea>  
                    </p>
                    
                    
                    
              <p class="name-account physical_item"><label class="label"><?php if($this->lang->line('shipping_policies') != '') { echo stripslashes($this->lang->line('shipping_policies')); } else echo "Shipping & policies"; ?><span style="color:red;"> </span></label>
                    <textarea class="global-input mceEditor" placeholder="<?php if($this->lang->line('shipping_policies') != '') { echo stripslashes($this->lang->line('header_description')); } else echo "Shipping & policies"; ?>" id="shipping_policies" rows="10" cols="40" name="shipping_policies"><?php if ($productDetails->row()->shipping_policies == ''){echo $productDetails->row()->shipping_policies;}else {echo $productDetails->row()->shipping_policies;}?></textarea> 
                    </p>
                          
                    
                    
             <p class="name-account"><label class="label"><?php if($this->lang->line('product_excerpt') != '') { echo stripslashes($this->lang->line('product_excerpt')); } else echo "Excerpt"; ?><span style="color:red;"> </span></label>
                     <textarea class="global-input" placeholder="<?php if($this->lang->line('product_excerpt') != '') { echo stripslashes($this->lang->line('product_excerpt')); } else echo "Excerpt"; ?>" rows="5" cols="40" name="excerpt"><?php echo $productDetails->row()->excerpt;?></textarea>
                    </p>    
                    
                    <?php //print_r($productDetails->row()->product_type);die;?>
                    
                    <p class="name-account"><label class="label">Product Type</label>
                     <input type="radio" id="physical_type" class="product_type"  name="product_type"  value="physical" <?php if($productDetails->row()->product_type == 'physical'){echo 'checked="checked" ';}?>/>Physical &nbsp;&nbsp;&nbsp;
                     <input type="radio"  id="digital_type" class="product_type" name="product_type" value="digital" <?php if($productDetails->row()->product_type == 'digital'){echo 'checked="checked" ';}?>/>Digital &nbsp;&nbsp;&nbsp;
                    </p>        
                    
            <p class="name-account physical_item"><label class="label"><?php if($this->lang->line('product_quantity') != '') { echo stripslashes($this->lang->line('product_quantity')); } else echo "Quantity"; ?><span style="color:red;"> *</span></label>
                     <input type="text" class="global-input required number" placeholder="<?php if($this->lang->line('product_quantity') != '') { echo stripslashes($this->lang->line('product_quantity')); } else echo "Quantity"; ?>" value="<?php if ($editmode == '1'){echo $productDetails->row()->quantity;}?>" name="quantity">
                    </p>                      		
                                		
   									<?php /*?>  <div class="form_fields">
                                        <p class="name-account"><label class="label"><?php if($this->lang->line('samedaydelivery') != '') { echo stripslashes($this->lang->line('samedaydelivery')); } else echo "Same Day Delivery"; ?></label>
                                        <div class="form_fieldsgroup validation-input product_samedaydly">
                                        	<input type="radio" name="same_day_delivery" <?php if ($editmode == '1'){if($productDetails->row()->same_day_delivery == 'true'){echo 'checked="checked"';}}?> value="true"/><?php if($this->lang->line('prference_yes') != '') { echo stripslashes($this->lang->line('prference_yes')); } else echo "Yes"; ?>&nbsp;&nbsp;&nbsp;
                                        	<input type="radio" name="same_day_delivery" <?php if ($editmode == '1'){if($productDetails->row()->same_day_delivery == 'false'){echo 'checked="checked"';}}else{echo 'checked="checked"';}?> value="false"/><?php if($this->lang->line('prference_no') != '') { echo stripslashes($this->lang->line('prference_no')); } else echo "No"; ?>&nbsp;&nbsp;&nbsp;
                                        </div></p>
                                    </div>    <?php */?>                                    		
             
<?php if($productDetails->row()->city!='' && $productDetails->row()->zip_code!=''){ 
										     $city_values = explode(',',$productDetails->row()->city);
											 $zip_codes = explode(',',$productDetails->row()->zip_code);
											 $i = 0;
											 echo '<div id="append_city">';
									foreach(array_combine($city_values, $zip_codes)as $city => $zipcode){?>
											   <div class="form_fields" id="<?php echo $i; ?>">
                                                  <label><?php if($this->lang->line('product_city') != '') { echo stripslashes($this->lang->line('product_city')); } else echo "City"; ?><span style="color:red;"> *</span></label>
                                                  <div class="form_fieldsgroup validation-input" style="position:relative;">
                                                      <input type="text" id="searchcity<?php echo $i; ?>" onkeyup="searchcity(this.id,this.value,this);" class="global-input required" placeholder="<?php if($this->lang->line('product_city') != '') { echo stripslashes($this->lang->line('product_city')); } else echo "City"; ?>" value="<?php echo $city;?>" name="city[]"><div class="result"></div>
													  <input type="text" id="searchzip_code<?php echo $i; ?>" class="global-input required" placeholder="<?php if($this->lang->line('product_zipcode') != '') { echo stripslashes($this->lang->line('product_zipcode')); } else echo "Zip Code"; ?>" value="<?php echo $zipcode;?>" name="zip_code[]">
											           <input type="button" id="<?php echo $i; ?>" onclick="addcities(this.id);" value="<?php if($this->lang->line('add_more_cities') != '') { echo stripslashes($this->lang->line('add_more_cities')); } else echo "Add City"; ?>">
													   <input type="button" id="<?php echo $i; ?>" onclick="removecity(this.id);" value="<?php if($this->lang->line('remove_city') != '') { echo stripslashes($this->lang->line('remove_city')); } else echo "Remove City"; ?>">
										          </div>
                                               </div> 
											<?php $i++; }
										echo"</div>"; 
									 } else{?>
									<div id="append_city">
									<div class="form_fields" style="display:none;" id="product-city">
                                        <label><?php if($this->lang->line('product_city') != '') { echo stripslashes($this->lang->line('product_city')); } else echo "City"; ?><span style="color:red;"> *</span></label>
                                        <div class="form_fieldsgroup validation-input" style="position:relative;">
                                            <input type="text" id="searchcity0" onkeyup="searchcity(this.id,this.value,this);" class="global-input required" placeholder="<?php if($this->lang->line('product_city') != '') { echo stripslashes($this->lang->line('product_city')); } else echo "City"; ?>" value="" name="city[]"><div class="result"></div>
											<input type="text" id="searchzip_code0" class="global-input required" placeholder="<?php if($this->lang->line('product_zipcode') != '') { echo stripslashes($this->lang->line('product_zipcode')); } else echo "Zip Code"; ?>" value="" name="zip_code[]">
											<input type="button" id="0" onclick="addcities(this.id);" value="<?php if($this->lang->line('add_more_cities') != '') { echo stripslashes($this->lang->line('add_more_cities')); } else echo "Add City"; ?>">
										</div>
										
                                    </div></div>
									<?php } ?>             
             <p class="name-account physical_item"><label class="label"><?php if($this->lang->line('product_ship_imd') != '') { echo stripslashes($this->lang->line('product_ship_imd')); } else echo "Shipping Immediately"; ?></label>
                     <input type="radio" name="ship_immediate" <?php if ($editmode == '1'){if($productDetails->row()->ship_immediate == 'true'){echo 'checked="checked"';}}?> value="true"/><?php if($this->lang->line('prference_yes') != '') { echo stripslashes($this->lang->line('prference_yes')); } else echo "Yes"; ?>&nbsp;&nbsp;&nbsp;
                     <input type="radio" name="ship_immediate" <?php if ($editmode == '1'){if($productDetails->row()->ship_immediate == 'false'){echo 'checked="checked"';}}else{echo 'checked="checked"';}?> value="false"/><?php if($this->lang->line('prference_no') != '') { echo stripslashes($this->lang->line('prference_no')); } else echo "No"; ?>&nbsp;&nbsp;&nbsp;
                    </p>                      		
             
             
                          
				<p class="name-account physical_item"><label class="label"><?php if($this->lang->line('product_sku') != '') { echo stripslashes($this->lang->line('product_sku')); } else echo "SKU"; ?></label>
                    <input type="text" class="global-input " placeholder="<?php if($this->lang->line('product_sku') != '') { echo stripslashes($this->lang->line('product_sku')); } else echo "SKU"; ?>" value="<?php if ($editmode == '1'){echo $productDetails->row()->sku;}?>" name="sku"> 
                </p>                      		
             

				<p class="name-account physical_item"><label class="label"><?php if($this->lang->line('product_weight') != '') { echo stripslashes($this->lang->line('product_weight')); } else echo "Weight"; ?></label>
                    <input type="text" class="global-input " placeholder="<?php if($this->lang->line('product_weight') != '') { echo stripslashes($this->lang->line('product_weight')); } else echo "Weight"; ?>" value="<?php if ($editmode == '1'){echo $productDetails->row()->weight;}?>" name="weight">
                </p>
				
				<p class="name-account physical_item"><label class="label"><?php if($this->lang->line('Product_Brand') != '') { echo stripslashes($this->lang->line('Product_Brand')); } else echo "Product Brand"; ?></label>
                   
					<select name="brand_seo"  class="required large tipTop">
						<option value="">Select</option>
						<?php foreach($brandslist->result() as $brand){?>
						<option value="<?php echo $brand->brand_seourl; ?>" <?php if ($editmode == '1' && $brand->brand_seourl == $productDetails->row()->brand_seo){echo 'selected';}?> ><?php echo $brand->brand_name; ?></option>
						<?php } ?>
					</select>
				</p>
                    
                    <p class="name-account"><label class="label"><?php if($this->lang->line('product_sku') != '') { echo stripslashes($this->lang->line('product_sku')); } else echo "SKU"; ?></label>
                      <input type="text" class="global-input " placeholder="<?php if($this->lang->line('product_sku') != '') { echo stripslashes($this->lang->line('product_sku')); } else echo "SKU"; ?>" value="<?php if ($editmode == '1' && $productDetails->row()->sku != ''){echo $productDetails->row()->sku;}?>" name="sku">
                    </p>                    
                    
               <p class="name-account"><label class="label"><?php if($this->lang->line('product_youtube') != '') { echo stripslashes($this->lang->line('product_youtube')); } else echo "Youtube Link"; ?></label>
                       <input type="text" class="global-input " placeholder="<?php if($this->lang->line('product_youtube') != '') { echo stripslashes($this->lang->line('product_youtube')); } else echo "Youtube Link"; ?>" value="<?php if ($editmode == '1' && $productDetails->row()->youtube != ''){echo 'https://www.youtube.com/watch?v='.$productDetails->row()->youtube;}?>" name="youtube">
                    </p>                    
             
             <p class="name-account"><label class="label"><?php if($this->lang->line('giftcard_price') != '') { echo stripslashes($this->lang->line('giftcard_price')); } else echo "Price"; ?><span style="color:red;"> *</span></label>
                       <input type="text" class="global-input required number minStrict price" placeholder="<?php if($this->lang->line('giftcard_price') != '') { echo stripslashes($this->lang->line('giftcard_price')); } else echo "Price"; ?>" value="<?php if ($editmode == '1'){echo $productDetails->row()->price;}?>" name="price" id="price">
                    </p> 
             
                   <p class="name-account"><label class="label"><?php if($this->lang->line('product_sale_price') != '') { echo stripslashes($this->lang->line('product_sale_price')); } else echo "Sale Price"; ?><span style="color:red;"> *</span></label>
                       <input type="text" class="global-input required number minStrict smallerThan" data-min="price" placeholder="<?php if($this->lang->line('product_sale_price') != '') { echo stripslashes($this->lang->line('product_sale_price')); } else echo "Sale Price"; ?>" value="<?php if ($editmode == '1'){echo $productDetails->row()->sale_price;}?>" name="sale_price" id="sale_price">
                    </p> 
                    
                   
					
                                    
                                    
                    
                  <!--  <p class="name-account physical_item"><label class="label"> Default Shipping Cost<span style="color:red;"> *</span></label>
                   <input type="text" class="global-input required number  " data-min="price" placeholder="<?php if($this->lang->line('default_shipping_cost') != '') { echo stripslashes($this->lang->line('default_shipping_cost')); } else echo "Default Shipping Cost"; ?>" value="<?php if ($editmode == '1'){echo $productDetails->row()->shipping_cost;}?>" name="default_ship_cost" id="default_ship_cost">
                   <a href="javascript:void(0)" id="hide_show_cost">Add specific country shipping cost</a>   
                    </p>-->
                    
                           <!-- <div class="shipp-add-remove name-account physical_item">
                                 <div class="inputs" style="float: left;width:100%; border:1px dashed #1DB3F0;">
							            <div style="margin:12px; float:right; margin:10px;">
								            <div class="btn_30_blue">
												<a href="javascript:void(0)" id="add" class="tipTop" title="Add new attribute">
													<span class="btn_link"><?php if($this->lang->line('header_add') != '') { echo stripslashes($this->lang->line('header_add')); } else echo "Add"; ?></span>
												</a>
											</div>
								            <div class="btn_30_blue">
												<a href="javascript:void(0)" id="remove" class="tipTop" title="Remove last attribute">
													<span class="btn_link"><?php if($this->lang->line('product_remove') != '') { echo stripslashes($this->lang->line('product_remove')); } else echo "Remove"; ?></span>
												</a>
											</div>
							            </div>
							                                                    
                                        <?php //echo "<pre>"; print_r($shippingCostDetails->result());die;
										/* if($countryList->num_rows()>0){
										?>
                                        <?php  
													
												foreach ($shippingCostDetails->result() as $shippingCostRow){ ?>
							            <div style="float: left; margin: 9px 0px 0px 10px; width:81%;" class="field" >
                                                <div class="image_text name-account" style="float: left;margin: 5px;margin-right:50px;">
                                                <span><?php if($this->lang->line('header_country') != '') { echo stripslashes($this->lang->line('header_country')); } else echo "Country"; ?>:</span>
							            		<select name="country_code[]" class="ship_country_code" style="width:200px;color:gray; height:27px; background: none repeat scroll 0 0 #fcfcfc; border: 1px solid #cccccc;" >
													<option value="">--<?php if($this->lang->line('checkout_select') != '') { echo stripslashes($this->lang->line('checkout_select')); } else echo "Select"; ?>--</option>
							            			<?php 
												    foreach ($countryList->result() as $row){
							            			?>
								            		<option <?php if ($row->country_code == $shippingCostRow->country_code){?> selected="selected" <?php }?> value="<?php echo $row->country_code;?>"><?php echo $row->name;?></option>
								            		<?php } ?>
							            		</select>
							            	</div>
                                            
							            	<div class="image_text name-account" style="float: left;margin: 5px;margin-right:50px;">
												 <span><?php if($this->lang->line('product_separate_ship_cost') != '') { echo stripslashes($this->lang->line('product_separate_ship_cost')); } else echo "Cost"; ?> :</span>&nbsp;
												 <input type="text" placeholder="<?php if($this->lang->line('product_separate_ship_cost') != '') { echo stripslashes($this->lang->line('product_separate_ship_cost')); } else echo "Cost"; ?>" value="<?php echo $shippingCostRow->separate_ship_cost;?>" name="separate_cost[]" id="separate_cost" style="padding: 1px 5px; width:193px; height:27px; background: none repeat scroll 0 0 #fcfcfc; border: 1px solid #cccccc;"> 
                                                 </div>
                                                 
											
											<script type="text/javascript">
							            	$('.image_text').find('select').trigger('change');
							            	</script>
							            </div>
							            <?php 
											 }
						            ?><?php } */ ?>
							       </div>
                                   </div>-->
                                   
                                   
                                   <br /><br />
             
             <p class="name-account">
		<input type="hidden" name="PID" value="<?php echo $productDetails->row()->seller_product_id;?>"/>
 <input type="submit" id="editDetailsSub" value="<?php if($this->lang->line('header_save') != '') { echo stripslashes($this->lang->line('header_save')); } else echo "Save"; ?>" class="button btn-blue btn-save"/>

                    </p> 


         </dd></dl>
        </form>
        </div></div>
        
        
<script type="text/javascript" src="js/site/jquery.validate.js"></script>

<script>
$.validator.addMethod("smallerThan", function (value, element, param) {
	//alert("sophia");
	//alert("sale price"+$('#sale_price').val());
	
	 //$va= $(".price").val();
	//	alert("Price"+$va);
    var $element = $(element)
        , $min;

    if (typeof(param) === "string") {
        $min = $(param);
    } else {
        $min = $("." + $element.data("min"));
		//alert($element.data("min"));
		
    }

    if (this.settings.onfocusout) {
        $min.off(".validate-smallerThan").on("blur.validate-smallerThan", function () {
            $element.valid();
        });
    }
    return parseFloat(value) <= parseFloat($min.val());
},  "Sale price must be smaller than price");
$.validator.addClassRules({
	smallerThan: {
    	smallerThan: true
    }
});
$.validator.addMethod('minStrict', function (value, el, param) {
    return value > param;
},"Price must be greater than zero");
$.validator.addClassRules({
	minStrict: {
		minStrict: true,
		minStrict: 0
    }
});
$("#sellerProdEdit1").validate();
function validate_desc(){
	if(tinyMCE.get('description').getContent() == ''){
		$('.desc_con .desc_error').show().focus();
		return false;
	}else{
		$('.desc_con .desc_error').hide();
		return true;
	}
}
function addcities(id){
      var ids = ++id;
      var html = '<div class="form_fields" id="'+ids+'"><label><?php if($this->lang->line('product_city') != '') { echo stripslashes($this->lang->line('product_city'));} else echo "City"; ?><span style="color:red;"> *</span></label><div class="form_fieldsgroup validation-input" style="position:relative;"><input type="text" id="searchcity'+ids+'" onkeyup="searchcity(this.id,this.value,this);" class="global-input required" placeholder="<?php if($this->lang->line('product_city') != '') { echo stripslashes($this->lang->line('product_city')); } else echo "City"; ?>" value="" name="city[]"><div class="result"></div>&nbsp;<input type="text" id="searchzip_code'+ids+'" class="global-input required" placeholder="<?php if($this->lang->line('product_zipcode') != '') { echo stripslashes($this->lang->line('product_zipcode')); } else echo "Zip Code"; ?>" value="" name="zip_code[]"><input type="button" id="'+ids+'" onclick="addcities(this.id);" value="<?php if($this->lang->line('add_more_cities') != '') { echo stripslashes($this->lang->line('add_more_cities')); } else echo "Add City"; ?>"> <input type="button" id="'+ids+'" onclick="removecity(this.id);" value="<?php if($this->lang->line('remove_city') != '') { echo stripslashes($this->lang->line('remove_city')); } else echo "Remove City"; ?>"></div></div>';
	  
	  $('#append_city').append(html);
}
function removecity(id){
   document.getElementById(id).remove();
}
function searchcity(id,value,evt){
   url= "<?php echo base_url();?>/site/samedaydelivery/search_city";
   if(value != ''){
	  $.post(url,{'search':value},function(html){
         $(evt).next().html(html).show();
      });
   }else{
	  $(".result").hide();
   }return false;   
}
$(".result").live("click",function(e){  
       var $clicked = $(e.target);
       var $name = $clicked.find('.name').html();
	   if(!$name){
			$name = $clicked.html();
	   }
       var decoded = $("<div/>").html($name).text();
	   $(this).prev().val(decoded);
   });
   $(".show").live("click", function(e) { 
      var $clicked = $(e.target);
      if (! $clicked.hasClass("search-social-friends")){
       $(".result").fadeOut(); 
      }
   });	

   
	
   </script>
  
<script type="text/javascript">
$(document).ready(function(){
   $("input:radio:first").prop("checked", true).trigger("click");
   $(".product_samedaydly input:radio").click(function(){
      var value = $(this).val();
	  if(value == 'true'){
	     $("#product-city").css('display','block');
	  }
	  else{
	     $("#product-city").css('display','none');
	  }
   }); 

   <?php if($shippingCostDetails->num_rows()>0){ 
	   ?>
	   var i = <?php echo $shippingCostDetails->num_rows();?>;
	   <?php }else{
	   ?>
	   	
	   	var i = 0;
	   	<?php } ?>

	   	$(".shipp-add-remove").css("display", "none");
	   	<?php if($shippingCostDetails->num_rows()>0){ ?>
	   		$( ".shipp-add-remove" ).show(); <?php } ?>
   $('#hide_show_cost').click(function() { 
	    $(".shipp-add-remove").toggle();
	  	});

   $('#add').click(function() { 
		
		/*if(i==5){
			alert('Only 5 countries allowed');
			return false;
		} else{*/
		
		$(' <div style="float: left; margin: 9px 0px 0px 10px; width:81%;" class="field">'+
				'<div class="image_text name-account" style="float: left;margin: 5px;margin-right:50px;">'+
				
					'<span><?php if($this->lang->line('header_country') != '') { echo stripslashes($this->lang->line('header_country')); } else echo "Country"; ?>:</span>&nbsp;'+
					
					'<select name="country_code[]" style="width:200px;color:gray; height:27px; background: none repeat scroll 0 0 #fcfcfc; border: 1px solid #cccccc;" class="ship_country_code">'+
						'<option value="">--<?php if($this->lang->line('checkout_select') != '') { echo stripslashes($this->lang->line('checkout_select')); } else echo "Select"; ?>--</option>'+
						<?php foreach ($countryList->result() as $row){ 
							
						?>
						'<option value="<?php echo $row->country_code; ?>"><?php echo $row->name; ?></option>'+
						<?php } ?>
					 '</select>'+
					
				'</div>'+
				
				'<div class="image_text name-account" style="float: left;margin: 5px;margin-right:50px;">'+
					 '<span><?php if($this->lang->line('product_separate_ship_cost') != '') { echo stripslashes($this->lang->line('product_separate_ship_cost')); } else echo "Cost"; ?> :</span>&nbsp;'+
					 '<input type="text" placeholder="<?php if($this->lang->line('product_separate_ship_cost') != '') { echo stripslashes($this->lang->line('product_separate_ship_cost')); } else echo "Cost"; ?>" value="" name="separate_cost[]" id="separate_cost" style="padding: 1px 5px; width:193px; height:27px; background: none repeat scroll 0 0 #fcfcfc; border: 1px solid #cccccc;"> '+
					 
				'</div>'+ 
				
		'</div>').fadeIn('slow').appendTo('.inputs');
		//i++; }
		
	});
	

	/* **********************  */
	$('#remove').click(function() {
	
		$('.field:last').remove();
		
	});

	$('.product_type').change(function(){
		   
			if($(this).val()=='digital'){
				$('.name-account.physical_item').hide();
			//	$('.form_fields input#quantity').removeClass('required');
				$('.tab-menu .pdf_upload').next().hide();
				$('.tab-menu .pdf_upload').show();
			} else {
				$('.name-account.physical_item').show();
				//$('.form_fields input#quantity').addClass('required');
				$('.tab-menu .pdf_upload').hide();
				$('.tab-menu .pdf_upload').next().show();
			}
		});	

	<?php if ($productDetails->row()->product_type=='digital'){?>
	$('#digital_type').trigger('click');
	<?php }?>
	 
	
});

</script>