<?php $this->load->view('site/templates/header'); ?> 
<script type="text/javascript">

		var can_show_signin_overlay = false;
		if (navigator.platform.indexOf('Win') != -1) {document.write("<style>::-webkit-scrollbar, ::-webkit-scrollbar-thumb {width:7px;height:7px;border-radius:4px;}::-webkit-scrollbar, ::-webkit-scrollbar-track-piece {background:transparent;}::-webkit-scrollbar-thumb {background:rgba(255,255,255,0.3);}:not(body)::-webkit-scrollbar-thumb {background:rgba(0,0,0,0.3);}::-webkit-scrollbar-button {display: none;}</style>");}
	</script>
    <script src="js/site/category-shoplist.js" type="text/javascript"></script>

<div class="shop-browse" id="content">

	<div style="padding:0" class="color-container browse1 search-frm">
     <div class="search">
        <dl class="visibli">
			<dt><?php echo trim_slashes($breadCumps); ?></dt>
		</dl>




        
        <fieldset class="filtering">
        <p class="listing-top">
         <?php echo $listSubCatSelBox;  ?>
        </p>
        <p class="next">
        <select class="amount-range shop-select price-range selectBox">
        <option value=""><?php if($this->lang->line('product_any_price') != '') { echo stripslashes($this->lang->line('product_any_price')); } else echo "Any Price"; ?></option>
         <?php foreach ($pricefulllist->result() as $priceRangeRow){ ?>
       <option <?php if($_GET['p']==url_title($priceRangeRow->price_range)){ echo 'selected="selected"'; } ?> value="<?php echo url_title($priceRangeRow->price_range); ?>"><?php echo $currencySymbol;?> <?php echo $priceRangeRow->price_range; ?></option>
           <?php } ?>
            </select>
        <select class="color-range shop-select color-filter selectBox">
        <option value=""><?php if($this->lang->line('product_any_color') != '') { echo stripslashes($this->lang->line('product_any_color')); } else echo "Any Color"; ?></option>
        <?php 
                      foreach ($mainColorLists->result() as $colorRow){
                      	if ($colorRow->list_value != ''){
                      ?>
              <option <?php if($_GET['c']==url_title($colorRow->list_value)){ echo 'selected="selected"'; } ?> value="<?php echo strtolower($colorRow->list_value);?>"><?php echo ucfirst($colorRow->list_value);?></option>
              <?php 
                      	}
                      }
              ?>
       
        </select>
        <select class="sort-amount shop-select sort-by-price selectBox">
        <option value=""><?php if($this->lang->line('product_newest') != '') { echo stripslashes($this->lang->line('product_newest')); } else echo "Newest"; ?></option>
        <option value="asc"><?php if($this->lang->line('product_low_high') != '') { echo stripslashes($this->lang->line('product_low_high')); } else echo "Price: Low to High"; ?></option>
        <option value="desc"><?php if($this->lang->line('product_high_low') != '') { echo stripslashes($this->lang->line('product_high_low')); } else echo "Price: High to Low"; ?></option>
        </select>
        </p>
        <p class="keyword" style="background: none;">
        <i class="ic-search"></i>
        <input class="search-string" type="text" placeholder="<?php if($this->lang->line('product_filter_key') != '') { echo stripslashes($this->lang->line('product_filter_key')); } else echo "Filter by keyword"; ?>">
        <button class="button-remove">
        <i class="but-remove"></i>
        </button>
        </p>
        </fieldset>
        
        
        
             
        </div>
        
        <div class="color-container">
        <h2 class="color-title"><?php echo site_lg('everything', 'Everything');?> </h2>
         <?php if ($productList->num_rows()>0){?>
        <ul class="color-shop stream">
         <?php foreach ($productList->result() as $productListVal) { 
        	$img = 'dummyProductImage.jpg';
			$imgArr = explode(',', $productListVal->image);
			if (count($imgArr)>0){
				foreach ($imgArr as $imgRow){
					if ($imgRow != ''){
						$img = $pimg = $imgRow;
						break;
					}
				}
			}
	        $fancyClass = 'fancy';
			$fancyText = LIKE_BUTTON;
			if (count($likedProducts)>0 && $likedProducts->num_rows()>0){
				foreach ($likedProducts->result() as $likeProRow){
					if ($likeProRow->product_id == $productListVal->seller_product_id){
						$fancyClass = 'fancyd';$fancyText = LIKED_BUTTON;break;
					}
				}
			}
		?>
        <li class="color_item">
        <a href="things/<?php echo $productListVal->id;?>/<?php echo url_title($productListVal->product_name,'-');?>">
       <img src="<?php echo DESKTOPURL;?>images/product/<?php echo $img; ?>">
        <?php echo $productListVal->product_name;?><b><?php echo $currencySymbol;?><?php echo $productListVal->sale_price;?></b>
        </a>
        </li>
        <?php } ?>
               
        </ul>
        <div class="pagination" style="display:none">
                    <?php echo $paginationDisplay; ?>
        </div> 
       <?php }else {?> <?php if($this->lang->line('product_no_more') != '') { echo stripslashes($this->lang->line('product_no_more')); } else echo "No more products available"; ?><?php }?>
        </div>
        </div></div>
        
   
		