<?php 
$this->load->view('site/templates/header.php');
?>
<script type="text/javascript" src="js/jquery.MultiFile.js"></script>
<link href="css/layout-new.css" rel="stylesheet" type="text/css" >
<link href="css/styles-new.css" rel="stylesheet" type="text/css" >
<script src="js/jquery.idTabs.min.js"></script>
<script src="js/fg.menu.js"></script>

<script>
$(document).ready(function(){
	$('.nxtTab').click(function(){
		var cur = $(this).parent().parent().parent().parent().parent();
		cur.hide();
		var tab = cur.parent().parent().prev();
		tab.find('a.active_tab').removeClass('active_tab').parent().nextUntil().not('.remove').first().find('a').addClass('active_tab');
		$(tab.find('a.active_tab').attr('href')).show();
	});
	$('.prvTab').click(function(){
		var cur = $(this).parent().parent().parent().parent().parent();
		cur.hide();
		var tab = cur.parent().parent().prev();
		tab.find('a.active_tab').removeClass('active_tab').parent().prevUntil().not('.remove').first().find('a').addClass('active_tab');
		$(tab.find('a.active_tab').attr('href')).show();
	});
	$('#tab2 input[type="checkbox"]').click(function(){
		var cat = $(this).parent().attr('class');
		var curCat = cat;
		var catPos = '';
		var added = '';
		var curPos = curCat.substring(3);
		var newspan = $(this).parent().prev();
		if($(this).is(':checked')){
			while(cat != 'cat1'){
				cat = newspan.attr('class');
				catPos = cat.substring(3);
				if(cat != curCat && catPos<curPos){
					if (jQuery.inArray(catPos, added.replace(/,\s+/g, ',').split(',')) >= 0) {
					    //Found it!
					}else{
						newspan.find('input[type="checkbox"]').attr('checked','checked');
						added += catPos+',';
					}
				}
				newspan = newspan.prev(); 
			}
		}else{
			var newspan = $(this).parent().next();
			if(newspan.get(0)){
				var cat = newspan.attr('class');
				var catPos = cat.substring(3);
			}
			while(newspan.get(0) && cat != curCat && catPos>curPos){
				newspan.find('input[type="checkbox"]').attr('checked',this.checked);	
				newspan = newspan.next(); 	
				cat = newspan.attr('class');
				catPos = cat.substring(3);
			}
		}
	});
		
});
</script>
<script language="javascript">
function viewAttributes(Val){

	if(Val == 'show'){
		document.getElementById('AttributeView').style.display = 'block';
	}else{
		document.getElementById('AttributeView').style.display = 'none';
	}

}
</script>
<script>
$(document).ready(function(){
	$('#widget_tab ul').idTabs();

	var i = 1;
	
	
	$('#add').click(function() { 
		$('<div style="float: left; margin: 12px 10px 10px; width:100%;" class="field">'+
				'<div class="image_text" style="float: left;margin: 5px;margin-right:50px;">'+
					'<span>List Name:</span>&nbsp;'+
					'<select name="attribute_name[]" onchange="javascript:loadListValuesUser(this)" style="width:170px;color:gray;width:206px;" class="chzn-select">'+
						'<option value="">--Select--</option>'+
						<?php foreach ($atrributeValue->result() as $attrRow){ 
							if (strtolower($attrRow->attribute_name) != 'price'){
						?>
						'<option value="<?php echo $attrRow->id; ?>"><?php echo $attrRow->attribute_name; ?></option>'+
						<?php }} ?>
					 '</select>'+
				'</div>'+
				'<div class="attribute_box attrInput" style="float: left;margin: 5px;" >'+
					 '<span>List Value :</span>&nbsp;'+
					 '<select name="attribute_val[]" style="width:200px;color:gray;width:206px;" class="chzn-select">'+
					 '<option value="">--Select--</option>'+
					 '</select>'+
				'</div>'+
		'</div>').fadeIn('slow').appendTo('.inputs');
		i++;
	});
	
	$('#remove').click(function() {
		$('.field:last').remove();
	});
	
	$('#reset').click(function() {
		$('.field').remove();
		$('#add').show();
		i=0;
	
	
	});
	
	var j = 1;
	$('#addAttr').click(function() { 
		$('<div style="float: left; margin: 12px 10px 10px; width:100%;" class="field">'+
				'<div class="image_text" style="float: left; margin: 5px 13px 5px 5px;">'+
					'<span>Attribute Type:</span>&nbsp;'+
					'<select name="product_attribute_type[]" style="width:170px;color:gray;width:206px;" class="chzn-select">'+
						'<option value="">--Select--</option>'+
						<?php foreach ($PrdattrVal->result() as $prdattrRow){ ?>
						'<option value="<?php echo $prdattrRow->id; ?>"><?php echo $prdattrRow->attr_name; ?></option>'+
						<?php } ?>
					 '</select>'+
				'</div>'+
				'<div class="attribute_box attrInput" style="float: left;margin: 5px;" >'+
					 '<span>Attribute Name :</span>&nbsp;'+
					 '<input type="text" name="product_attribute_name[]" style="width:110px;color:gray;" class="chzn-select" />'+
				'</div>'+
				'<div class="attribute_box attrInput" style="float: left;margin: 5px;" >'+
					 '<span>Attribute Price :</span>&nbsp;'+
					 '<input type="text" name="product_attribute_val[]" style="width:100px;color:gray;" class="chzn-select" />'+
				'</div>'+
				'<div class="attribute_box attrInput" style="float: left;margin: 5px;" >'+
					 '<span>Attribute Quantity :</span>&nbsp;'+
					 '<input type="text" name="product_attribute_qty[]" style="width:50px; color:gray;" class="chzn-select" />'+
				'</div>'+
				'<div class="btn_30_blue">'+
				'<a href="javascript:void(0)" onclick="removeAttr(this)" id="removeAttr" class="tipTop" title="Remove this attribute">'+
					'<span class="icon cross_co"></span>'+
					'<span class="btn_link">Remove</span>'+
				'</a>'+
			'</div>'+
		'</div>').fadeIn('slow').appendTo('.inputss');
		j++;
	});
	
});
function removeAttr(evt){
	$(evt).parent().parent().remove();
}
function removeAttrDb(pid,evt){
	if(pid != ''){
		$.post(baseURL+'site/product/remove_attr',{pid:pid});
	}
	$(evt).parent().parent().remove();
}
</script>
<br />
<div id="content">
<div id="main" class="seller-product">
<!--<div id="content">
		<div class="grid_container">
			<div class="grid_12">-->
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6>Add New Product</h6>
                        <div id="widget_tab">
              				<ul id="myTab">
               					 <li class="add"><a href="#tab1" class="active_tab link_tab1">Content</a></li>
               					 <li class="add"><a href="#tab2" class="link_tab2">Category</a></li>
               					 <li class="add"><a href="#tab3" class="link_tab3">Images</a></li>
               					 <li class="add"><a href="#tab4" class="link_tab4">List</a></li>
								 <li class="pdf_upload remove"><a href="#tab5" class="link_tab5">PDF</a></li>
               					 <li class="product_attr add"><a href="#tab6" class="link_tab6">Atrribute</a></li>
								 <li class="sub_shipping add"><a href="#tab7" class="link_tab7">Shipping</a></li>
               					 <li><a href="#tab8" class="link_tab8">SEO</a></li>
             				 </ul>
            			</div>
					</div>
					<div class="widget_content">
					<?php 
						$attributes = array('class' => 'form_container left_label', 'id' => 'addproduct_form', 'enctype' => 'multipart/form-data');
						echo form_open_multipart('site/product/insertEditProduct',$attributes) 
					?>
                    
                     <div id="tab1" class="tab_con" data-tab="link_tab1">
						<ul>
							<li>
							<div class="form_grid_12">
							<label class="field_title" for="product_name">Product Name <span class="req">*</span></label>
							<div class="form_input">
								<input name="product_name" id="product_name" type="text" tabindex="1" class="required large tipTop" title="Please enter the product name"/>
							</div>
							</div>
							</li>
						
	 						<li>
								<div class="form_grid_12">
								<label class="field_title" for="description">Description<span class="req">*</span></label>
								<div class="form_input">
								<textarea name="description" id="description" tabindex="2" style="width:370px;" class="required large tipTop mceEditor" title="Please enter the product description"></textarea>
								</div>
								</div>
							</li>
                            
	 						<li>
								<div class="form_grid_12">
								<label class="field_title" for="description">Excerpt</label>
								<div class="form_input">
								<textarea name="excerpt" id="excerpt" tabindex="3" style="width:370px;" class="large tipTop" title="Please enter the product Excerpt"></textarea>
								</div>
								</div>
							</li>
							<li>
								<div class="form_grid_12">
									<label class="field_title"><?php if($this->lang->line('lg_product_type') != '') { echo stripslashes($this->lang->line('lg_product_type')); } else echo "Product Type"; ?></label>
									<div class="form_input">
										<input type="radio" class="product_type" name="product_type" value="physical" id="physical_type" checked="checked" />
										<label for="physical_type" style="cursor:pointer;">Physical</label>&nbsp;&nbsp;
										<input type="radio" class="product_type" name="product_type" value="digital" id="digital_type" />
										<label for="digital_type" style="cursor:pointer;">Digital</label>
									</div>
								</div>
							</li>
                            <li id="shipping_rfq" class="physical_item">
								<div class="form_grid_12">
								<label class="field_title" for="shipping_policies">Shipping &amp; Policies<span class="req">*</span></label>
								<div class="form_input">
								<textarea name="shipping_policies" id="shipping_policies" tabindex="2" style="width:370px;" class="large tipTop mceEditor" title="Please enter the product shipping &amp; policies"></textarea>
								</div>
								</div>
							</li>

                            <li id="qnty_rfq">
								<div class="form_grid_12">
								<label class="field_title" for="quantity">Quantity<span class="req">*</span></label>
								<div class="form_input">
							<!--<input type="text" name="quantity" id="quantity" tabindex="4" class="required number large tipTop" title="Please enter the product quantity" />-->
                            <input type="text" name="quantity" id="quantity" tabindex="4" class="required number large tipTop" title="Invalid quantity" />
								</div>
								</div>
							</li>
                            
                             <li class="physical_item">
								<div class="form_grid_12">
								<label class="field_title" for="shipping_cost">Immediate Shipping</label>
								<div class="form_input">
								<input type="radio" name="ship_immediate" value="true" />Yes&nbsp;&nbsp;&nbsp;
								<input type="radio" name="ship_immediate" value="false" checked="checked" />No
								</div>
								</div>
							</li>
                            
							<li>
								<div class="form_grid_12">
								<label class="field_title" for="sku">SKU</label>
								<div class="form_input">
								<input type="text" name="sku" id="sku" tabindex="7" class="large tipTop" title="Please enter the product sku" />
								</div>
								</div>
							</li>
                            
							<li id="weight_rfq">
								<div class="form_grid_12">
									<label class="field_title" for="weight">Weight</label>
									<div class="form_input">
										<input type="text" name="weight" id="weight" tabindex="8" class="large tipTop" title="Please enter the product weight" />
									</div>
								</div>
							</li>
							<li>
								<div class="form_grid_12">
									<label class="field_title"><?php if($this->lang->line('header_youtube') != '') { echo stripslashes($this->lang->line('header_youtube')); } else echo "Youtube Link"; ?></label>
									<div class="form_input validation-input">
										<input type="text" class="large tipTop" placeholder="<?php if($this->lang->line('header_youtube') != '') { echo stripslashes($this->lang->line('header_youtube')); } else echo "Youtube Link"; ?>" value="" name="youtube">
									</div>
								</div>
							</li>
							
							<li>
								<div class="form_grid_12">
									<label class="field_title">Product Brand</label>
									<div class="form_input validation-input">
										<select name="brand_seo"  class="required large tipTop">
												<option value="">Select</option>
												<?php foreach($brandslist->result() as $brand){?>
												<option value="<?php echo $brand->brand_seourl; ?>"><?php echo $brand->brand_name; ?></option>
												<?php } ?>
										</select>
									</div>
								</div>
							</li>
							
                            <li id="price_rfq">
								<div class="form_grid_12">
								<label class="field_title" for="description">Price<span class="req">*</span></label>
								<div class="form_input">
								<input type="text" name="price" id="price" tabindex="9" class="required number large tipTop" title="Please enter the product price" />
								</div>
								</div>
							</li>
                             <li id="sale_rfq">
								<div class="form_grid_12">
								<label class="field_title" for="sale_price">Sale Price<span class="req">*</span></label>
								<div class="form_input">
								<!--<input type="text" name="sale_price" id="sale_price" tabindex="10" class="required number smallerThan large tipTop" data-min="price" title="Please enter the product sale price" />-->
                                <input type="text" name="sale_price" id="sale_price" tabindex="10" class="required number smallerThan large tipTop" data-min="price" title="Sale price must be smaller than price" />
								
								</div>
								</div>
							</li>
								<li>
								<div class="form_grid_12">
									<div class="form_input">
										<input type="button" class="btn_small btn_blue nxtTab" tabindex="9" value="Next"/>
									</div>
								</div>
								</li>
							</ul>
						</div>
						<div id="tab2" class="tab_con" data-tab="link_tab2">
							<div class="cateogryView">
								<?php  //echo $categoryView; ?>
							
							<div>
								<span>Select List Type</span>
								<div class="col-sm-12 no_padd slect_mar_gin">
								<select name="category_id" class="required" id="category_id" >
									<option value="">Select</option>
									<?php foreach($mainCategories->result() as $row){ ?>
									<option value="<?php echo $row->id; ?>" <?php if( $row->id == $CatId[0]){echo 'selected="selected"';} ?>><?php echo $row->cat_name; ?></option>
											<?php } ?>
								</select>
								</div>
								<div class="" style="display:none" id="sub_cat_loading">
									<div id="loader1" class=""><img src="images/ajax-loader/ajax-loader(1).gif" alt="loading subcategory" /></div>
								</div>
							</div>
									
							<div class="sub_category" id="sub_category">
								<?php 
								// print_r($CatId);die;
								//echo "<pre>";print_r($all_categories->result());
								$len = count($CatId);
								foreach($CatId as $key => $cat){?>
									<?php if ($key != $len - 1) { ?>
									<div class="col-sm-2">
										<select onchange="show_subcategory(this)" class="required" class="label_sub_cat" name="subcategories[]">
											<option value="" >Select a sub  category</option>
											<?php foreach($all_categories->result() as $itemsubcat){ ?>
											<?php if($itemsubcat->rootID == $cat){ echo $itemsubcat->rootID; echo $cat;?>
											<option <?php $nextkey = $key+1; if($CatId[$nextkey] == $itemsubcat->id){ echo 'selected="selected"'; }?> value="<?php echo $itemsubcat->id; ?>"><?php echo $itemsubcat->cat_name;?></option>
											<?php }?>
											<?php }?>
										</select>
									</div>
									<?php }?>
											  
								<?php }?>
							</div>
							</div>
							<ul style="float:left;">
								<li style="padding-left:0px;width:100%;">
									<div class="form_grid_12">
										<div class="form_input" style="margin:0px;width:100%;">
											<input type="button" class="btn_small btn_blue prvTab" tabindex="9" value="Prev"/>
											<input type="button" class="btn_small btn_blue nxtTab" tabindex="9" value="Next"/>
										</div>
									</div>
								</li>
							</ul>
						</div>
						
						<div id="tab3" class="tab_con" data-tab="link_tab3">
                        <ul>
                            <li>
                                <div class="form_grid_12">
                                <label class="field_title" for="product_image">Product Image</label>
                                <div class="form_input">
                                <input name="product_image[]" id="product_image" type="file" tabindex="7" class="large required multi tipTop" title="Please select product image"/><span class="input_instruction green">You Can Upload Multiple Images</span>
                                </div>
                                </div>
                            </li>
                            <li>
                                <div class="form_grid_12">
                                <div class="form_input">
                                <input type="button" class="btn_small btn_blue prvTab" tabindex="9" value="Prev"/>
                                <input type="button" class="btn_small btn_blue nxtTab" tabindex="9" value="Next"/>
                                </div>
                                </div>
                            </li>     
                        </ul>
                      </div>
                      <div id="tab4" class="tab_con" data-tab="link_tab4">
                      	<ul id="AttributeView">
                     	<li>
                        <div class="inputs" style="float: left;width:100%; border:1px dashed #1DB3F0;">
                            <div style="margin:12px;">
                                <div class="btn_30_blue">
                                    <a href="javascript:void(0)" id="add" class="tipTop" title="Add new attribute">
                                        <span class="icon add_co"></span>
                                        <span class="btn_link">Add</span>
                                    </a>
                                </div>
                                <div class="btn_30_blue">
                                    <a href="javascript:void(0)" id="remove" class="tipTop" title="Remove last attribute">
                                        <span class="icon cross_co"></span>
                                        <span class="btn_link">Remove</span>
                                    </a>
                                </div>
                            </div>
                        </div>
									
						<div class="form_grid_12">
						<div class="form_input" style="margin:0px;width:100%;">
                        <input type="button" class="btn_small btn_blue prvTab" tabindex="9" value="Prev"/>
                        <input type="button" class="btn_small btn_blue nxtTab" tabindex="9" value="Next"/>
                        </div>
                        </div>
                      </li>
                      </ul>
                      </div>
						<div id="tab6" class="tab_con" data-tab="link_tab6">
							<ul id="AttributeView">
								<li id="price_rfq" class="physical_item">
									<div class="form_grid_12">
									<label class="field_title" for="description">Attribute must<span class="req">*</span></label>
									<div class="form_input">
									<input type="checkbox" onclick="return check();" id="attribute_must" name="attribute_must" class="" value="yes">
									</div>
									</div>
								</li>
								<li>
									<div class="inputss" style="float: left;width:100%; border:1px dashed #1DB3F0;">
										<div style="margin:12px;">
											<div class="btn_30_blue">
												<a href="javascript:void(0)" id="addAttr" class="tipTop" title="Add new attribute">
													<span class="icon add_co"></span>
													<span class="btn_link">Add</span>
												</a>
											</div>
										</div>
									</div>
											
									<div class="form_grid_12">
										<div class="form_input" style="margin:0px;width:100%;">
											<input type="button" class="btn_small btn_blue prvTab" tabindex="9" value="Prev"/>
											<input type="button" class="btn_small btn_blue nxtTab" tabindex="9" value="Next"/>
										</div>
									</div>
								</li>
							</ul>
						</div>
<div id="tab7" class="tab_con" data-tab="link_tab7">
	<ul>
		<li>
			<div class="form_grid_12">
				<label class="field_title" for="country_code">Processing Time</label>
				<div class="form_input">
					<select onchange="return processing_time_shipping(this.value);" name="ship_duration" id="ship_duration" tabindex="9" class="full required" style="border: #d8d8d8 1px solid; width: 340px; height: 31px;">
						<option value="">Ready to ship in...</option>
						<optgroup label="----------------------------"></optgroup>
						<option class="range-1-1">1 business day</option>
						<option class="range-1-2">1-2 business days</option>
						<option class="range-1-3">1-3 business days</option>
						<option class="range-3-5">3-5 business days</option>
						<option class="range-5-10">1-2 weeks</option>
						<option class="range-10-15">2-3 weeks</option>
						<option class="range-15-20">3-4 weeks</option>
						<option class="range-20-30">4-6 weeks</option>
						<option class="range-30-40">6-8 weeks</option>
						<!--<option value="custom">Custom range</option>-->
					</select>
				</div>
			</div>
			<div id="custom_shipping_time_li" class="form_grid_12">
				<div class="form_input">
					<input checked="checked" id="business-days" name="processing_time_units" type="radio" value="business days">
					<label for="business-days" style="float:none;">business days</label>
					<input id="weeks" name="processing_time_units" type="radio" value="weeks">
					<label for="weeks" style="float:none;">weeks</label>
				</div>
				<div class="form_input" style="margin-top: 10px;">
					<select id="processing_min" name="processing_min" class="full" style="border: #d8d8d8 1px solid; width: 100px; height: 31px;">
						<option value="">From...</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="6">6</option>
						<option value="7">7</option>
						<option value="8">8</option>
						<option value="9">9</option>
						<option value="10">10</option>
					</select>
					<select id="processing_max" name="processing_max" class="full" style="border: #d8d8d8 1px solid; width: 100px; height: 31px;">
						<option value="">To...</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="6">6</option>
						<option value="7">7</option>
						<option value="8">8</option>
						<option value="9">9</option>
						<option value="10">10</option>
					</select>
				</div>
			</div>
		</li>
		<li>
			<div class="form_grid_12">
				<label class="field_title" for="country_code">Ships From<span class="req">*</span></label>
				<div class="form_input">
					<select name="country_code" id="country_code" tabindex="9" class="full required" style="border: #d8d8d8 1px solid; width: 340px; height: 31px;">
						<option value=""><?php if($this->lang->line('shop_sellocation') != '') { echo stripslashes($this->lang->line('shop_sellocation')); } else echo 'Select a location'; ?></option>
						<?php if($countryList->num_rows()>0){
						foreach ($countryList->result() as $countryVal){
						?>
							<option value="<?php echo $countryVal->id.'|'.$countryVal->country_code;  ?>"><?php echo $countryVal->name; ?></option> 
						<?php }}?>
					</select>
				</div>
			</div>
		</li>
		<li>
			<div class="form_grid_12">
				<label class="field_title" for="shipping-cost">Shipping Cost</label>
			</div>
			<div class="form_grid_12">
				<table class="table table-striped" id="tbNames">
					<tbody>
						<tr>
							<th><?php if($this->lang->line('shop_shipsto') != '') { echo stripslashes($this->lang->line('shop_shipsto')); } else echo 'Ships to'; ?></th>
							<th><?php if($this->lang->line('shop_byitself') != '') { echo stripslashes($this->lang->line('shop_byitself')); } else echo 'By itself'; ?></th>
							<th><?php if($this->lang->line('shop_anotheritem') != '') { echo stripslashes($this->lang->line('shop_anotheritem')); } else echo 'With another item'; ?></th>
							<th></th>
						</tr>
						<tr>
							<td>
								<p class="country-text" id="shipping_to_1_lab"><?php if($this->lang->line('shop_countryname') != '') { echo stripslashes($this->lang->line('shop_countryname')); } else echo 'Country Name'; ?></p>
								<input type="hidden" name="shipping_to[]" id="shiping_to_default">      
								<input type="hidden" name="ship_to_id[]" id="shipping_to_1_id">
							</td>
							<td>
								<input type="text" value="" name="shipping_cost[]" class="form-control shipping_txt_bax" placeholder="<?php echo $currencySymbol;?>:">
							</td>
							<td>
								<input type="text" value="" name="shipping_with_another[]" class="form-control shipping_txt_bax" placeholder="<?php echo $currencySymbol;?>:">
							</td>
							<td></td>
						</tr>
						<tr id="tab_1">
							<td>
								<p class="country-text" id="shipping_to_1_lab1"><?php if($this->lang->line('shop_everywhere') != '') { echo stripslashes($this->lang->line('shop_everywhere')); } else echo 'Everywhere Else'; ?></p>
								<input type="hidden" name="shipping_to[]" id="shipping_to_2" value="Everywhere Else">
								<input type="hidden" id="shipping_to_2_id" name="ship_to_id[]" value="232">
							</td>
							<td>
								<input type="text" value="" class="form-control shipping_txt_bax" name="shipping_cost[]" placeholder="<?php echo $currencySymbol;?>:">
							</td>
							<td>
								<input type="text" value="" class="form-control shipping_txt_bax" name="shipping_with_another[]" placeholder="<?php echo $currencySymbol;?>:">
							</td>
							<td>
								<a class="close_icon" href="javascript:void(0)" id="1"></a>
							</td>
						</tr>
						<tr id="tab_2">
							<td> 
								<p class="country-text" id="shipping_to_3_lab"></p>
								<select id="shipping_to_3" class="shipping_to" onchange="display_sel_val(this);" name="shipping_to[]">
									<option value=""><?php if($this->lang->line('shop_sellocation') != '') { echo stripslashes($this->lang->line('shop_sellocation')); } else echo 'Select a location'; ?></option>
									<?php foreach($countryList->result() as $countryVal) {?>   
										<option value="<?php echo $countryVal->id.'|'.$countryVal->country_code; ?>"><?php echo $countryVal->name; ?></option> 
									<?php }?>
									<option value="232|Everywhere Else">Everywhere Else</option>
								</select>
								<input type="hidden" name="ship_to_id[]" id="shipping_to_3_id">
								<input type="hidden" name="shipping_to[]" id="shipping_to_3_name">
							</td>
							<td>
								<input type="text" value="" class="form-control shipping_txt_bax" name="shipping_cost[]" placeholder="<?php echo $currencySymbol;?>:">
							</td>
							<td>
								<input type="text" value="" class="form-control shipping_txt_bax" name="shipping_with_another[]" placeholder="<?php echo $currencySymbol;?>:">
							</td>
							<td>
								<a class="close_icon" href="javascript:void(0)" id="2"></a>
							</td>
						</tr>
					</tbody>
				</table>
				<input type="button" value="Add location" class="btn_small btn_blue" style="width:100px;" id="btnAdd">
				<img src="images/ajax-loader/ajax-loader(1).gif" alt="Loading" class="search_btn" id="stimg">
			</div>
		</li>
		<li>
			<div class="form_grid_12">
				<div class="form_input">
					<input type="button" class="btn_small btn_blue prvTab" tabindex="9" value="Prev"/>
					<input type="button" class="btn_small btn_blue nxtTab" tabindex="9" value="Next"/>
				</div>
			</div>
		</li>
	</ul>
</div>
<div id="tab5" class="tab_con" data-tab="link_tab5">
	<ul>
		<li>
			<div class="form_grid_12">
				<label class="field_title" for="product_image"><?php if($this->lang->line('product_prod_doc') != '') { echo stripslashes($this->lang->line('product_prod_doc')); } else echo "PDF / Document file"; ?></label>
				<div class="form_input">
					<input type="file" title="Please select pdf or doc" class="large multi tipTop" name="pdfupload" id="pdfupload">
				</div>
			</div>
		</li>
		<li>
			<div class="form_grid_12">
				<div class="form_input">
					<input type="button" class="btn_small btn_blue prvTab" tabindex="9" value="Prev"/>
					<input type="button" class="btn_small btn_blue nxtTab" tabindex="9" value="Next"/>
				</div>
			</div>
		</li>     
	</ul>
</div>
                      <div id="tab8" class="tab_con" data-tab="link_tab8">
                      	<ul>
                            <li>
                              <div class="form_grid_12">
                                <label class="field_title" for="meta_title">Meta Title</label>
                                <div class="form_input">
                                  <input name="meta_title" id="meta_title" type="text" tabindex="1" class="large tipTop" title="Please enter the page meta title"/>
                                </div>
                              </div>
                            </li>
                            <li>
                              <div class="form_grid_12">
                                <label class="field_title" for="meta_tag">Meta Keyword</label>
                                <div class="form_input">
                                  <textarea name="meta_keyword" id="meta_keyword"  tabindex="2" class="large tipTop" title="Please enter the page meta keyword"></textarea>
                                </div>
                              </div>
                            </li>
                            <li>
                              <div class="form_grid_12">
                                <label class="field_title" for="meta_description">Meta Description</label>
                                <div class="form_input">
                                  <textarea name="meta_description" id="meta_description" tabindex="3" class="large tipTop" title="Please enter the meta description"></textarea>
                                </div>
                              </div>
                            </li>
                          </ul>
                                  <ul><li><div class="form_grid_12">
                            <div class="form_input">
                                <input type="button" class="btn_small btn_blue prvTab" tabindex="9" value="Prev"/>
                                <button type="submit" class="btn_small btn_blue" tabindex="4"><span>Submit</span></button>
								<div id="show-msg">
									<p style="color:red;">Please fill the required fields and submit..especially Shipping, attributes.</p>
								</div>
                            </div>
                        </div></li>
                      </ul>
                      </div>
				 
                    <!--<input type="hidden" name="userID" value="0"/>-->  
            
						</form>
					</div>
				</div>
                </div>
                </div>
			<!--</div>
		</div>
		<span class="clear"></span>
	</div>-->
<style>
.uploader{
	overflow: visible !important;
}
.uploader .error{
	position: absolute;
	width: 200px;
}
label.error{
	color:red;
}
.table{ width: 100%;
    max-width: 100%;
    margin-bottom: 20px;     border-collapse: collapse;
    border-spacing: 0;
}
.table-striped{border:1px solid #ccc;}
#custom_shipping_time_li{display:none; margin-top:10px;}
table, th {
    border: 1px solid #ccc; min-height: 1px;
    padding-left: 15px;
    padding-right: 15px;
    position: relative;
}	
.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td{
 border-top: 1px solid #ddd;
    line-height: 1.42857;
    padding: 8px;
    vertical-align: top;
}
.table-striped > tbody > tr:nth-child(2n+1) > td, .table-striped > tbody > tr:nth-child(2n+1) > th{
	background-color: #f9f9f9;
}
.errors{border:1px solid red !important}
.shipping_to{width:200px; padding: 3px 4px; box-shadow: none; margin: 0px; border: 1px solid rgb(205, 205, 205); display:block;}
.country-text{margin:0px;}
</style>
<script type="text/javascript">
$.validator.addMethod("smallerThan", function (value, element, param) {
    var $element = $(element)
        , $min;

    if (typeof(param) === "string") {
        $min = $(param);
    } else {
        $min = $("#" + $element.data("min"));
    }

    if (this.settings.onfocusout) {
        $min.off(".validate-smallerThan").on("blur.validate-smallerThan", function () {
            $element.valid();
        });
    }
    return parseFloat(value) <= parseFloat($min.val());
}, "Sale price must be smaller than price");
$.validator.addClassRules({
	smallerThan: {
    	smallerThan: true
    }
});
$('#addproduct_form').validate({
	ignore : '',
	errorPlacement: function(error, element) {
	    error.appendTo( element.parent());
	    $('.tab_con').hide();
	    $('#widget_tab ul li a').removeClass('active_tab');
	    var $link_tab = $('label.error:first').parents('.tab_con').show().data('tab');
	    $('.'+$link_tab).addClass('active_tab');
	}
});
</script>
 <script> 
	function check(){
		if($("#attribute_must").is(':checked')){
			$('#addAttr').click();  // checked
			$("#attribute_must").prop('onclick',null).off('click');
			$("#show-msg").show();
		}
		else{
			$("#show-msg").hide();
		}
	}
</script>
