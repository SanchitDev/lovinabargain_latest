<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Twtest extends MY_Controller {
public function __construct(){
	parent::__construct();
	$this->load->helper(array('cookie','date','form','email'));
	$this->load->library(array('encrypt','form_validation'));
	$this->load->model(array('user_model','product_model'));
	$this->load->library('twconnect');
	$this->load->helper('url');
}
public function index(){
	echo '<p><a href="' . base_url() . 'twtest/redirect">Connect to Twitter</a></p>';
	echo '<p><a href="' . base_url() . 'twtest/clearsession">Clear session</a></p>';
	echo 'Session data:<br/><pre>';
	print_r($this->session->all_userdata());
	echo '</pre>';
}
/* redirect to Twitter for authentication */
public function redirect(){
	$twitter_data = $this->session->userdata('tw_access_token');
	$twitter_data_status = $this->session->userdata('tw_status');
	$fc_session_temp_id = $this->session->userdata('fc_session_temp_id');
	$this->session->unset_userdata($twitter_data);
	$this->session->unset_userdata($twitter_data_status);
	$this->session->unset_userdata($fc_session_temp_id);
	//echo "<pre>";print_r($this->session->all_userdata);
	//redirect('twtest/redirect');
	$this->load->library('twconnect');
	/* twredirect() parameter - callback point in your application */
	/* by default the path from config file will be used */
	$ok = $this->twconnect->twredirect('twtest/callback');
	//		$ok = $this->twconnect->twredirect('twtest/callback');
	if(!$ok){
		echo 'Could not connect to Twitter. Refresh the page or try again later.';
	}
}
/* return point from Twitter */
/* you have to call $this->twconnect->twprocess_callback() here! */
public function callback(){
	$this->load->library('twconnect');
	$ok = $this->twconnect->twprocess_callback();
	if( $ok ){ redirect('twtest/success'); }
	else redirect('twtest/failure');
}
/* authentication successful */
/* it should be a different function from callback */
/* twconnect library should be re-loaded */
/* but you can just call this function, not necessarily redirect to it */
public function success(){
	echo 'Twitter connect succeded<br/>';
	echo '<p><a href="' . base_url() . 'twtest/clearsession">Do it again!</a></p>';
	$this->load->library('twconnect');
	$this->load->library('session');
	// saves Twitter user information to $this->twconnect->tw_user_info
	// twaccount_verify_credentials returns the same information
	$this->twconnect->twaccount_verify_credentials();
	echo 'Authenticated user info ("GET account/verify_credentials"):<br/><pre>';
	$twConnectId = $this->twconnect->tw_user_info;
	$twitterName = $twConnectId->screen_name;
	$twitterId = $twConnectId->id;
	$this->load->model('user_model');
	if($twitterId!=''){
		$social_check = $this->user_model->social_network_login_check($twitterId);
		$a = $this->session->all_userdata();
	    $aa= $this->session->userdata('tw_access_token');
		if($social_check > 0){
			$getLoginDetails = $this->user_model->get_social_login_details($twitterId);
			$userdata = array(
				'fc_session_user_id' => $getLoginDetails['id'],
				'session_user_name' => $getLoginDetails['user_name'], 
				'session_user_email' => $getLoginDetails['email'] 
			);
			$this->session->set_userdata($userdata);
			if($this->data['login_succ_msg'] != '')
				$lg_err_msg = $this->data['login_succ_msg'];
			else
				$lg_err_msg = 'You are Logged In ...';
				$this->setErrorMessage('success',$lg_err_msg);
				redirect(base_url());
		}else{
			$getFileNameArray = explode('/',$twConnectId->profile_image_url);
			$fileNameDetails = $getFileNameArray[5];
			if($fileNameDetails != ''){
				$fileNameDetails = $getFileNameArray[5];
			}else{
				$fileNameDetails = '';
			}
			$twitter_login_details = array('social_login_name'=>$twConnectId->name,'social_login_unique_id'=>$twConnectId->id,'screen_name'=>$twConnectId->screen_name,'social_image_name'=>$fileNameDetails,'social_tw_id'=>$twConnectId->id,'social_tw_username'=>$twConnectId->screen_name);
			$url = $twConnectId->profile_image_url;
			$img = DESKTOPURL.'images/users/'.$fileNameDetails ;
			file_put_contents($img, file_get_contents($url));
			//echo "redirect to registration page";
			$social_login_name = $twConnectId->name;
			$this->session->set_userdata($twitter_login_details);
			//echo $a =$this->session->userdata($twise);
			redirect('signup');
			//redirect("signup#");
		}
	}else{
		redirect('');
	}
}
public function failure() {
	redirect('signup');
}
/* clear session */
public function clearsession() {
	//$this->session->sess_destroy();
	$tw_data_arr = array(
	'tw_access_token'=>'',
	'tw_status'=>'',
	'social_login_name'=>'',
	'social_login_unique_id'=>'',
	'social_image_name'=>'',
	'tw_request_token'=>'',
	'screen_name'=>''
	);
	$this->session->unset_userdata($tw_data_arr);
}
/****************For Invite Friends Start************Vinu********Nov-5-2013************/
public function invite_friends(){
	$this->clearsession();
	$ok = $this->twconnect->twredirect('twtest/invite_callback');
	if (!$ok) {
		echo 'Could not connect to Twitter. Refresh the page or try again later.';
	}
}
public function invite_callback(){
	$ok = $this->twconnect->twprocess_callback();
	if ( $ok )
		redirect('twtest/invite_success');
	else
		redirect ('twtest/invite_failure');
}
public function invite_success(){
	$this->twconnect->twaccount_verify_credentials();
	$tw_user_info = $this->twconnect->tw_user_info;
	$twitterId = $tw_user_info->id;
	$twitterName = $tw_user_info->screen_name;
	$twitterFriends = $tw_user_info->friends_count;
	if ($twitterFriends>0){
		$param_arr = array(
			'screen_name'=>$twitterName
		);
		$tw_friends_list = $this->twconnect->tw_get('https://api.twitter.com/1.1/followers/ids.json',$param_arr);
		$tw_friends_list_arr = $tw_friends_list->ids;
		if (count($tw_friends_list_arr)>0){
			$invite_text = 'Invites you to join on '.$this->data['siteTitle'].' ('.base_url().'?ref='.$this->data['userDetails']->row()->user_name.')';
			foreach ($tw_friends_list_arr as $tw_friend_id){
				if ($tw_friend_id != ''){
					$param_arr = array(
					'text'=>$invite_text,
					'user_id'=>$tw_friend_id
					);
					$msg_res = $this->twconnect->tw_post('https://api.twitter.com/1.1/direct_messages/new.json',$param_arr);
				}	
			}
			echo "<script> alert('Invitations sent successfully'); window.close(); </script>";
		}
	}else{
		echo "<script>alert('No followers in your twitter account');window.close();</script>";
	}
}
public function invite_failure(){
	echo "<script> window.close(); </script>";
}
public function get_twitter_user(){
	$this->clearsession();
	$ok = $this->twconnect->twredirect('twtest/get_user_callback');
	if (!$ok) {
		echo 'Could not connect to Twitter. Refresh the page or try again later.';
	}
}
public function get_user_callback(){
	$ok = $this->twconnect->twprocess_callback();
	if($ok)
		redirect('twtest/get_user_success');
	else
		redirect ('twtest/get_user_failure');
}
public function get_user_success(){
	$this->twconnect->twaccount_verify_credentials();
	$tw_user_info = $this->twconnect->tw_user_info;
	$twitterId = $tw_user_info->id;
	$twitterName = $tw_user_info->screen_name;
	if($twitterId!=''){
		$condition = "select * from ".USERS." where twitter_id=".$twitterId." and id != ".$this->checkLogin('U')."";
		$user_details = $this->user_model->ExecuteQuery($condition);
		if($user_details->num_rows()==1){
			echo "<script type='text/javascript'>
					alert('This Twitter account is already linked with another Oliver account');
					window.close();
				</script>";
		}else{
			$condition = array('id'=>$this->checkLogin('U'));
			$dataArr = array('twitter_id'=> $twitterId,'tw_username'=> $twitterName);
			$this->user_model->update_details(USERS,$dataArr,$condition);
			echo "<script type='text/javascript'>
					window.opener.location.href = '".base_url()."settings/apps';
					window.close();
				</script>";
		}
	}
}
public function get_user_failure(){
	echo "<script type='text/javascript'> window.close(); </script>";
}
public function find_friends(){
	$this->clearsession();
	$ok = $this->twconnect->twredirect('twtest/find_friends_callback');
	if (!$ok) {
		echo 'Could not connect to Twitter. Refresh the page or try again later.';
	}
}
public function find_friends_callback(){
	$ok = $this->twconnect->twprocess_callback();
	if ( $ok )
		redirect('twtest/find_friends_success');
	else
		redirect ('twtest/find_friends_failure');
}
public function find_friends_success(){
		$this->twconnect->twaccount_verify_credentials();
		$tw_user_info = $this->twconnect->tw_user_info;
		$twitterId = $tw_user_info->id;
		$twitterName = $tw_user_info->screen_name;
		$twitterFriends = $tw_user_info->friends_count;
		if ($twitterFriends>0){
			$param_arr = array(
			'screen_name'=>$twitterName
			);
			$tw_friends_list = $this->twconnect->tw_get('https://api.twitter.com/1.1/friends/ids.json',$param_arr);
			$tw_friends_list_arr = $tw_friends_list->ids;
			$friends_id = array();
			if(count($tw_friends_list_arr)>0){
			   foreach($tw_friends_list_arr as $tw_friend_id){
			      $friends_id[] = $tw_friend_id;
               }
			   if($friends_id!=''){
					$Query = "select * from ".USERS." where twitter_id IN (".implode(',',$friends_id).") AND id != ".$this->checkLogin('U')."";				
					$twitter_user = $this->product_model->ExecuteQuery($Query);
                    if($twitter_user != '' && $twitter_user->num_rows() >0){
						foreach($twitter_user->result() as $user_details){
							$userImg = 'user-thumb1.png';
							if($user_details->thumbnail != ''){
								$userImg = $user_details->thumbnail;
							}
		$html .='<dl style="border:none" class="userinformation"><dt>
			<button class="button-" uid="'.$user_details->id.'"><i class="icon"></i></button>
			<a href="user/'.$user_details->user_name.'"><img height="60" width="60" src="'.DESKTOPURL.'images/users/'.$userImg.'" class="member">
			<b>'.$user_details->user_name.'</b></a>';
		$html .='</dt>
			<dd class="follow-image">';
		$uproduct = "select * from ".USER_PRODUCTS." where user_id = ".$user_details->id."";
		$sproduct = "select * from ".PRODUCT." where user_id = ".$user_details->id."";
		$userProducts = $this->product_model->ExecuteQuery($uproduct);
		$sellerProducts = $this->product_model->ExecuteQuery($sproduct);
		$product_details[$user_details->id] = $this->product_model->get_sorted_array($userProducts,$sellerProducts,'created','desc');
		$this->data['product_details'][$user_details->id] = $this->product_model->ExecuteQuery($product_condition);
		if($product_details[$_profileUserDetails->id]!= '' && $product_details[$_profileUserDetails->id]->num_rows() >0){
			foreach($product_details[$_profileUserDetails->id]->result() as $likeProdRow){
				$img = 'dummyProductImage.jpg';
				$imgArr = explode(',', $likeProdRow->image);
				if(count($imgArr)>0){
					foreach($imgArr as $imgRow){
						if($imgRow != ''){
							$img = $imgRow;
							break;
						}
					}
				}
				if($likeProdRow->web_link!=''){
					$product_link = 'user/'.$user_details->user_name.'/things/'.$likeProdRow->seller_product_id.'/'.url_title($likeProdRow->product_name,'-');
				}else{
					$product_link = 'things/'.$likeProdRow->pid.'/'.url_title($likeProdRow->product_name,'-');
				}
					 
				$html .='<a href="'.$product_link.'">
							<img height="60" width="60" src="'.DESKTOPURL.'images/product/'.$img.'" >
						</a></dd>';
						}}
				$html .='</dl>';  
		}}}else{
			$html .='<h2>No Users Found</h2>';						
		}}else{
			$html .='<h2>No Users Found</h2>';	
		}}else{
			$html .='<h2>No Users Found</h2>';	
		}
		echo "
		<script type='text/javascript' src='".base_url()."js/site/jquery-1.9.0.min.js'></script>
		<script type='text/javascript'>
			window.opener.document.getElementById('friend_select_type').style.display = 'none';
			window.opener.document.getElementById('find_selected_google').innerHTML = '$html';
			window.close();
		</script>";
		
}
public function find_friends_failure(){
	echo "<script type='text/javascript'>
			window.close();
		</script>";
}
/*************For Invite Friends End************Vinu**********Nov-5-2013************/
}