<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Googlelogin extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
	}
	public function index()
	{
		$this->googleLoginProcess();
	}

	/* Job seeker login, registraiton and forgot password start*/

	function googleLoginProcess()
	{
        
		$getFileNameArray = explode('/',$profile_image_url);

		$fileNameDetails = $getFileNameArray[7];
			
		$url = $twConnectId->profile_image_url;
		$img = DESKTOPURL.'images/users/'.$fileNameDetails ;
		file_put_contents($img, file_get_contents($url));


		$url = $profile_image_url;
		$img = DESKTOPURL.'images/users/'.$fileNameDetails ;
		file_put_contents($img, file_get_contents($url));

		/*@mysql_query("INSERT INTO google_users (api_id, full_name,email, thumbnail) VALUES ($user_id, '$user_name','$email','$fileNameDetails')");*/

		$google_login_details = array('social_login_name'=>$user_name,'social_login_unique_id'=>$user_id,'screen_name'=>$user_name,'social_image_name'=>$fileNameDetails);

		$_SESSION['social_login_name']=$user_name;
		$_SESSION['social_login_unique_id']=$user_id;
		$_SESSION['screen_name']=$user_name;
		$_SESSION['social_image_name']=$fileNameDetails;
		//redirect('signup');
		header( 'Location: '.$originalBasePath.'signup' );

	}

	function googleRedirect()
	{
	    if($this->checkLogin('U')==''){
		require_once 'google-login-mats/index.php';
		$user_name  = '';
		$email = '';
		if (isset($_GET['code'])){
			$gClient->authenticate($_GET['code']);
			$_SESSION['token'] = $gClient->getAccessToken();
			//header('Location: ' . filter_var($google_redirect_url, FILTER_SANITIZE_URL));
			return;
		}
		if (isset($_SESSION['token'])){
			$gClient->setAccessToken($_SESSION['token']);
		}
		
		if ($gClient->getAccessToken()){
			//Get user details if user is logged in
			$user 				= $google_oauthV2->userinfo->get();
			$user_id 				= $user['id'];
			$user_name 			= filter_var($user['name'], FILTER_SANITIZE_SPECIAL_CHARS);
			$email 				= filter_var($user['email'], FILTER_SANITIZE_EMAIL);
			$profile_url 			= filter_var($user['link'], FILTER_VALIDATE_URL);
			$profile_image_url 	= filter_var($user['picture'], FILTER_VALIDATE_URL);
			$personMarkup 		= "$email<div><img src='$profile_image_url?sz=50'></div>";
				
			$_SESSION['token'] 	= $gClient->getAccessToken();
		}else{
			//get google login url
			$authUrl = $gClient->createAuthUrl();
		}
		if($email!='' && $user_id!=''){
			$social_check = $this->user_model->googleLoginCheck($user_id, $email);			
			if($social_check > 0){
				$getsocialLoginDetails = $this->user_model->google_user_login_details($user_id,$email);
				$userdata = array(
					'fc_session_user_id' => $getsocialLoginDetails['id'],
					'session_user_name' => $getsocialLoginDetails['user_name'], 
					'session_user_email' => $getsocialLoginDetails['email'] 
				);
				$this->session->set_userdata($userdata);
				$this->setErrorMessage('success','Login successfully');
				redirect(base_url()); 
            }else{
				$Query = "select * from ".USERS." where google_id = ".$user_id." AND email !='$email'";
				$get_google_id = $this->user_model->ExecuteQuery($Query);
				if($get_google_id->num_rows() ==1){
					$this->setErrorMessage('error','This Google account is already linked with another Oliver account');
					redirect('signup');
				}else{
					$user_details = $this->user_model->get_all_details(USERS,array('email'=>$email));
					if($user_details->num_rows() ==1){
						if($user_details->row()->google_id ==0){
							$Query = "UPDATE ".USERS." SET google_id = ".$user_id.", gl_username = '$user_name' WHERE email = '$email'";
							$this->user_model->ExecuteQuery($Query);
							$getsocialLoginDetails = $this->user_model->google_user_login_details($user_id,$email);
							$userdata = array(
								'fc_session_user_id' => $getsocialLoginDetails['id'],
								'session_user_name' => $getsocialLoginDetails['user_name'], 
								'session_user_email' => $getsocialLoginDetails['email'] 
							);
							$this->session->set_userdata($userdata);
							if($this->lang->line('log_succ_msg') != '')
								$lg_err_msg = $this->lang->line('log_succ_msg');
							else
								$lg_err_msg = 'You are Logged In ... Start Admiring !';
							$this->setErrorMessage('success',$lg_err_msg);
							redirect(base_url());
						}
					}else{
					   $google_login_details = array('social_login_name'=>$user_name,'social_login_unique_id'=>'','screen_name'=>$user_name,'social_image_name'=>'','social_email_name'=>$email,'loginUserType'=>'google','social_gl_id'=>$user_id,'social_gl_username'=>$user_name);
					   $social_login_name = $user_name;
					   $this->session->set_userdata($google_login_details);
					   redirect('signup');
				   }
                }				
            }			
		}else{
			redirect('');
		}
		}
	}
function facebookRedirect(){
	@session_start();
	if($_SESSION['fb_user_id']!='' && $_SESSION['email']!=''){
		$email = $_SESSION['email'];
		$user_id = $_SESSION['fb_user_id'];
		$user_name = $_SESSION['fb_user_name'];
		$social_check = $this->user_model->social_Check($user_id,$email);
		if($social_check > 0){
			$getsocialLoginDetails = $this->user_model->social_user_login_details($user_id,$email);
			$userdata = array(
				'fc_session_user_id' => $getsocialLoginDetails['id'],
				'session_user_name' => $getsocialLoginDetails['user_name'], 
				'session_user_email' => $getsocialLoginDetails['email'] 
		    );
		   $this->session->set_userdata($userdata);
		   $this->setErrorMessage('success','Login successfully');
		   redirect(base_url());
		   
		}else{
			$Query = "select * from ".USERS." where facebook_id = ".$user_id." AND email !='$email'";
			$get_facebook_id = $this->user_model->ExecuteQuery($Query);
			if($get_facebook_id->num_rows() ==1){
				$this->setErrorMessage('error','This Facebook account is already linked with another Oliver account');
			    redirect('signup');
			}else{
				$user_details = $this->user_model->get_all_details(USERS,array('email'=>$email));
				if($user_details->num_rows() ==1){
					if($user_details->row()->facebook_id ==0){
						$Query = "UPDATE ".USERS." SET facebook_id = ".$user_id.", fb_username = '$user_name' WHERE email = '$email'";
						$this->user_model->ExecuteQuery($Query);
						$getsocialLoginDetails = $this->user_model->social_user_login_details($user_id,$email);
						$userdata = array(
							'fc_session_user_id' => $getsocialLoginDetails['id'],
							'session_user_name' => $getsocialLoginDetails['user_name'], 
							'session_user_email' => $getsocialLoginDetails['email'] 
						);
						$this->session->set_userdata($userdata);
						$this->setErrorMessage('success','Login successfully');
						redirect(base_url());
					}
				}else{
					$google_login_details = array('social_login_name'=>$_SESSION['first_name'],'social_login_unique_id'=>$_SESSION['fb_user_id'],'screen_name'=>$_SESSION['first_name'],'social_image_name'=>$_SESSION['fb_image_name'],'social_email_name'=>$_SESSION['email'],'loginUserType'=>'facebook','social_fb_id'=>$user_id,'social_fb_username'=>$_SESSION['first_name']);
			        $social_login_name = $_SESSION['first_name'];
			        $this->session->set_userdata($google_login_details);
			        redirect('signup');
				}
			}
		}
	}else{
		redirect('');
	}
}
}