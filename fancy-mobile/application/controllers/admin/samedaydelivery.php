<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This controller contains the functions related to Same Day Delivery Management 
 * @author Teamtweaks
 *
 */
 class Samedaydelivery extends MY_Controller {
     function __construct(){
        parent::__construct();
		$this->load->helper(array('cookie','date','form'));
		$this->load->library(array('encrypt','form_validation'));		
		$this->load->model('samedaydelivery_model');
		if ($this->checkPrivileges('samedaydelivery',$this->privStatus) == FALSE){
			redirect('admin');
		}
    }
	public function index(){
	    if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			redirect('admin/samedaydelivery/same_day_delivery_list');
		}
	}
	public function same_day_delivery_list(){
	   if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
		    $this->data['heading'] = 'Same Day Delivery List';
			$condition = array('status'=>'Active');
			$this->data['samedaydlyList'] = $this->samedaydelivery_model->get_all_details(SAME_D_DLY,$condition);
			$this->load->view('admin/same_day_delivery/display_samedaydly',$this->data);
		}
	}
	public function same_day_delivery_create(){
	   if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
		    $this->data['heading'] = 'Add New City';
			$this->data['country_list'] = $this->samedaydelivery_model->get_all_details(COUNTRY_LIST,array('status'=>'Active'));
			$this->load->view('admin/same_day_delivery/add_samedaydly',$this->data);
		}
	}
	public function insertEditsamedaydly(){
	    if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
		    $city_id = $this->input->post('city_id');
		    $excludeArr = array('city_id');
			$dataArr = array('status'=> 'Active');
			if ($city_id == ''){
			    $condition = array('country_name'=>$this->input->post('country_name'),'city_name'=>$this->input->post('city_name'));
			    $get_details = $this->samedaydelivery_model->get_all_details(SAME_D_DLY,$condition);
                if($get_details->num_rows() >0){
				   $this->setErrorMessage('error','This City Name Already Exists');
				   redirect('admin/samedaydelivery/same_day_delivery_create');
				}else{
				  $this->samedaydelivery_model->commonInsertUpdate(SAME_D_DLY,'insert',$excludeArr,$dataArr);
				  $this->setErrorMessage('success','City added successfully');
				}
			}else {
				$this->samedaydelivery_model->commonInsertUpdate(SAME_D_DLY,'update',$excludeArr,$dataArr,array('id'=>$city_id));
			    $this->setErrorMessage('success','City updated successfully');
			}
			$cond = "select * from ".SAME_D_DLY." where status ='Active'";
			$get_details = $this->samedaydelivery_model->ExecuteQuery($cond);
            $config = '<?php ';
			$city_value = array();
		    foreach($get_details->result() as $key => $val){
               $city_value[] = "'".strtolower($val->city_name)."'";				   
		    }
			$config .= "\n\$config['cities'] = array(".implode(",",$city_value).");";
		    $config .= ' ?>';
		    $file = 'commonsettings/fc_samedaydly_settings.php';
		    file_put_contents($file, $config);
			redirect('admin/samedaydelivery/same_day_delivery_list');
		}
	}
	public function change_status(){
	    if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$mode = $this->uri->segment(4,0);
			$user_id = $this->uri->segment(5,0);
			$status = ($mode == '0')?'Inactive':'Active';
			$newdata = array('status' => $status);
			$condition = array('id' => $user_id);
			$this->samedaydelivery_model->update_details(SAME_D_DLY,$newdata,$condition);
			$this->setErrorMessage('success','City Status Changed Successfully');
			redirect('admin/samedaydelivery/same_day_delivery_list');
		}
	}
	public function edit_samedaydly(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Edit Same Day Delivery';
			$id = $this->uri->segment(4,0);
			$condition = array('id' => $id);
			$this->data['samedaydly_details'] = $this->samedaydelivery_model->get_all_details(SAME_D_DLY,$condition);
			if ($this->data['samedaydly_details']->num_rows() == 1){
				$this->load->view('admin/same_day_delivery/edit_samedaydly',$this->data);
			}else {
				redirect('admin');
			}
		}
	}
	public function delete_samedaydly(){
	    if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
		   $id = $this->uri->segment(4,0);
		   $this->samedaydelivery_model->commonDelete(SAME_D_DLY,array('id'=>$id));
		   $this->setErrorMessage('success','City Deleted successfully');
           redirect('admin/samedaydelivery/same_day_delivery_list');		   
		}
	}
 }