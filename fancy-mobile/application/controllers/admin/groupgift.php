<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This controller contains the functions related to groupgift management 
 * @author Teamtweaks
 *
 */
class Groupgift extends MY_Controller {
	function __construct(){
        parent::__construct();
		$this->load->helper(array('cookie','date','form'));
		$this->load->library(array('encrypt','form_validation'));		
		$this->load->model('groupgift_model');
		if ($this->checkPrivileges('groupgift',$this->privStatus) == FALSE){
			redirect('admin');
		}
    }
    
    /**
     * 
     * This function loads the groupgift list page
     */
   	public function index(){	
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			redirect('admin/groupgift/display_groupgift');
		}
	}
	
	/**
	 * 
	 * This function loads the giftcards dashboard
	 */
	public function display_groupgift_dashboard(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Group Gifts Dashboard';
			$condition = 'order by `created` desc';
			$this->data['groupGiftsList'] = $this->groupgift_model->get_giftcard_details($condition);
			$this->load->view('admin/groupgifts/display_groupgift_dashboard',$this->data);
		}
	}
	
	/**
	 * 
	 * This function loads the giftcards list page
	 */
	public function display_groupgift(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Group Gifts List';
			$condition = array();
			$this->data['GroupgiftList'] = $this->groupgift_model->get_all_details(GROUP_GIFTS,$condition);

			if($this->data['GroupgiftList']->num_rows() >0){
		   foreach($this->data['GroupgiftList']->result() as $_groupgiftlist){
               $this->data['contributeDetails'][$_groupgiftlist->id] = $this->groupgift_model->get_all_details(GROUPGIFT_PAYMENT,array('groupgift_id'=>$_groupgiftlist->id));
		   }
		}
			$this->load->view('admin/groupgifts/display_groupgift',$this->data);
		}
	}
	public function contributors_list(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Contributors List';
			$condition = array();
			$this->data['contributeDetails'] = $this->groupgift_model->get_all_details(GROUPGIFT_PAYMENT,$condition);
			$this->load->view('admin/groupgifts/display_contributors',$this->data);
		}
	}
	/**
	 * 
	 * Change the giftcards settings
	 */
	public function insertEditGroupgiftcard(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			//$excludeArr = array("gift_image");
			$dataArr = array();
			//$config['encrypt_name'] = TRUE;
			/* $config['overwrite'] = FALSE;
	    	$config['allowed_types'] = 'jpg|jpeg|gif|png';
		    $config['max_size'] = 2000;
		    $config['upload_path'] = './images/giftcards';
		    $this->load->library('upload', $config);
			if ( $this->upload->do_upload('gift_image')){
		    	$logoDetails = $this->upload->data();
		    	$dataArr['image'] = $logoDetails['file_name'];
			} */
			$condition = array('id' => '1');
			($this->config->item('groupgift_id') == '') ? $modeVal = 'insert' : $modeVal = 'update';
			$this->groupgift_model->commonInsertUpdate(GROUPGIFT_SETTINGS,$modeVal,$excludeArr,$dataArr,$condition);
			$cond = "select * from ".GROUPGIFT_SETTINGS." where id = '1'";
			$expiry_date = $this->groupgift_model->ExecuteQuery($cond);
            $config = '<?php ';
		       foreach($expiry_date->row() as $key => $val){
			       $value = addslashes($val);
			       $config .= "\n\$config['$key'] = '$value'; ";
		      }
		    $config .= ' ?>';
		    $file = 'commonsettings/fc_groupgift_settings.php';
		    file_put_contents($file, $config);
			$this->setErrorMessage('success','Groupgifts settings updated successfully');
			redirect('admin/groupgift/edit_groupgift_settings');
		}
	}
	
	/**
	 * 
	 * This function loads the edit giftcards settings
	 */
	public function edit_groupgift_settings(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Groupgifts Settings';
			$this->data['groupgift_settings'] = $this->groupgift_model->get_all_details(GROUPGIFT_SETTINGS,array());
			$this->load->view('admin/groupgifts/edit_groupgift_settings',$this->data);
		}
	}
	public function view_groupgift(){
	    if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'View Groupgifts';
			$this->data['view_groupgift'] = $this->groupgift_model->get_all_details(GROUP_GIFTS,array('id'=>$this->uri->segment(4)));
			$this->load->view('admin/groupgifts/view_groupgift',$this->data);
		}
	}
	public function view_contributors(){
	    if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'View Contributor';
			$this->data['view_contributors'] = $this->groupgift_model->get_all_details(GROUPGIFT_PAYMENT,array('id'=>$this->uri->segment(4)));
			$this->load->view('admin/groupgifts/view_contributors',$this->data);
		}
	}
	/**
	 * 
	 * This function delete the giftcard from db
	 */
	public function delete_groupgifts(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$gift_id = $this->uri->segment(4,0);
			$condition = array('id' => $gift_id);
			$this->groupgift_model->commonDelete(GROUP_GIFTS,$condition);
			$this->setErrorMessage('success','Groupgift deleted successfully');
			redirect('admin/groupgift/display_groupgift');
		}
	}
	
	/**
	 * 
	 * Changing giftcard mode as Active | Cancelled
	 */
	public function change_status(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$mode = $this->uri->segment(4,0);
			$status = ($mode == '1')?'Active':'Cancelled Admin';
			$condition = array('id' => $this->uri->segment(5,0));
			$data = array('status'=>$status);
			$this->groupgift_model->update_details(GROUP_GIFTS,$data,$condition);
			//$this->groupgift_model->saveGroupgiftSettings();
			$this->setErrorMessage('success','GroupGift '.$status.'d Successfully');
			redirect('admin/groupgift/display_groupgift');
		}
	}
	
	/**
	 * 
	 * This function delete the giftcards 
	 */
	public function change_groupgift_status_global(){
		if(count($_POST['checkbox_id']) > 0 &&  $_POST['statusMode'] != ''){
			$this->groupgift_model->activeInactiveCommon(GROUP_GIFTS,'id');
			$this->setErrorMessage('success','Groupgist deleted successfully');
			redirect('admin/groupgifts/display_groupgift');
		}
	}
    public function view_product_comments(){
	
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Product Comments List';
			$condition = array();
			$Query = "select gf.*,u.full_name,u.user_name,u.thumbnail,c.comments ,u.email,c.id,c.status,c.user_id as CUID,p.product_name from ".GROUPGIFT_COMMENTS." c LEFT JOIN ".USERS." u on u.id=c.user_id LEFT JOIN ".GROUP_GIFTS." gf on gf.gift_seller_id=c.group_product_id  LEFT JOIN ".PRODUCT." p on p.id = gf.product_id order by c.created desc";
			$this->data['commentsList'] = $this->groupgift_model->ExecuteQuery($Query);
			$this->load->view('admin/groupgifts/display_product_comments_list',$this->data);
		}
	}
	public function change_product_comment_status(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$mode = $this->uri->segment(4,0);
			$id = $this->uri->segment(5,0);
			$user_id = $this->uri->segment(6,0);
			$status = ($mode == '0')?'InActive':'Active';
			$newdata = array('status' => $status);
			$condition = array('id' => $id,'user_id'=>$user_id);
			$this->groupgift_model->update_details(GROUPGIFT_COMMENTS,$newdata,$condition);
			$this->setErrorMessage('success','Comment Status Changed Successfully');
			redirect('admin/groupgift/view_product_comments');
		}
	}
	public function view_product_comment(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'View Comments';
			$id = $this->uri->segment(4,0);
			$condition = "select gf.comments,u.user_name,p.product_name from ".GROUPGIFT_COMMENTS." gf left join ".USERS." u on (u.id = gf.user_id) left join ".GROUP_GIFTS." c on (c.gift_seller_id = gf.group_product_id) left join ".PRODUCT." p on (p.id = c.product_id) where gf.id = ".$id."";
			//$condition = array('id' => $id);
			$this->data['view_details'] = $this->groupgift_model->ExecuteQuery($condition);
			if ($this->data['view_details']->num_rows() == 1){
				$this->load->view('admin/groupgifts/view_comments',$this->data);
			}else {
				redirect('admin');
			}
		}
	}
		public function delete_product_comment(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$id = $this->uri->segment(4,0);
			$condition = array('id' => $id);
			$this->groupgift_model->commonDelete(GROUPGIFT_COMMENTS,$condition);
			$this->setErrorMessage('success','Comment deleted successfully');
			redirect('admin/groupgift/view_product_comments');
		}
	}
	public function refund(){
   echo $groupgift_id = $this->uri->segment(4);
   $getContributors = $this->groupgift_model->get_all_details(GROUPGIFT_PAYMENT,array('groupgift_id'=>$groupgift_id));
   if($getContributors->num_rows() >0){
	   foreach($getContributors->result() as $get_Contributors){
	      $transaction_id = $get_Contributors->transaction_id;
		  $amount = $get_Contributors->amount;
		  $email = $get_Contributors->email;
		  $Auth_Details=unserialize(API_LOGINID);
			$Auth_Setting_Details=unserialize($Auth_Details['settings']);
			 
			define("AUTHORIZENET_API_LOGIN_ID",$Auth_Setting_Details['Login_ID']);
			define("AUTHORIZENET_TRANSACTION_KEY",$Auth_Setting_Details['Transaction_Key']);
			define("API_MODE",$Auth_Setting_Details['mode']);
			if(API_MODE	=='sandbox'){
				define("AUTHORIZENET_SANDBOX",true);
			}else{
				define("AUTHORIZENET_SANDBOX",false);
			}
			define("TEST_REQUEST", "FALSE");
			require_once './authorize/AuthorizeNet.php';
			$transaction = new AuthorizeNetAIM;
			$transaction->setSandbox(AUTHORIZENET_SANDBOX);
			$transaction->setFields(
				array(
				'type' => 'refundTransaction',
				'amount' =>  $amount, 
				'card_num' =>  '4111111111111111', 
				'exp_date' => '122025',
				'email' =>  $email,
				'tran_key' => $transaction_id, 
				)
			);
			$response = $transaction->authorizeAndCapture();
			if($response->approved ){
			    echo "<pre>"; print_r($response);
			}else{
				//redirect('site/groupgift/failure/'.$response->response_reason_text.'/'.$product_id);
			}
	   }
   }
   
}
public function failure(){
    
	$this->data['Confirmation'] = 'Failure';
	$this->data['errors'] = $this->uri->segment(4);
	$this->load->view('site/groupgifts/failure',$this->data);
}
}

/* End of file giftcards.php */
/* Location: ./application/controllers/admin/giftcards.php */