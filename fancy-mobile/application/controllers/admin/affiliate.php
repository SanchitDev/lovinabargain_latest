<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * This controller contains the functions related to affiliate management
 * @author Teamtweaks
 *
 */

class affiliate extends MY_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array('cookie','date','form'));
		$this->load->library(array('encrypt','form_validation'));
		$this->load->model('affiliate_model');
		if ($this->checkPrivileges('affiliate',$this->privStatus) == FALSE){
			redirect('admin');
		}
	}

	/**
	 *
	 * This function loads the affiliate list page
	 */
	public function dashboard(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
		$Viewlimit='1';
		$this->data['heading'] = 'Affiliate Dashboard';
			$this->data['ProductList'] = $this->affiliate_model->admin_all_affiliate_list($Viewlimit);
			//echo '<pre>'.print_r($this->data['ProductList']->result());die;
			$this->load->view('admin/affiliate/display_affiliate_dashboard',$this->data);
		}
	}

	/**
	 *
	 * This function loads the selling affiliate list page
	 */
	public function display_affiliate_list(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Affiliate List';
			$this->data['affiliateList'] = $this->affiliate_model->admin_all_affiliate_list();
			$this->load->view('admin/affiliate/display_affiliate_userlist',$this->data);
		}
	}
	
	/**
	 *
	 * This function loads the selling affiliate list page
	 */
	public function view_affiliate_list(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Affiliate List';
			$Product_id = $this->uri->segment(4,0);
			$this->data['getUserDetails'] = $this->affiliate_model->get_all_details(USERS,array('id'=>$Product_id));
			$this->data['affiliateList'] = $this->affiliate_model->get_affiliate_list($Product_id);
			
			$this->load->view('admin/affiliate/view_affiliate',$this->data);
		}
	}
	
	/**
	 *
	 * This function loads the admin settings form
	 */
	public function affiliate_global_settings_form(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			if ($this->checkPrivileges('admin','2') == TRUE){
				$this->data['heading'] = 'Admin Settings';
				$this->data['affiliate_settings'] = $result = $this->affiliate_model->get_all_details(AFFILIATE_SETTING,array('id'=>'1'));
				$this->load->view('admin/affiliate/edit_affiliate_settings',$this->data);
			}else {
				redirect('admin');
			}
		}
	}
	
	
	
	/**
	 *
	 * This function validates the admin settings form
	 */
	public function affiliate_global_settings(){
		
				$dataArr = array();
				$condition = array('id'=>'1');
				$excludeArr = array('form_mode');
				$this->affiliate_model->commonInsertUpdate(AFFILIATE_SETTING,'update',$excludeArr,$dataArr,$condition);
				$this->affiliate_model->saveAffiliateSettings();
				$this->setErrorMessage('success','Affiliate details updated successfully');
				redirect('admin/affiliate/affiliate_global_settings_form');
			
	}

}

/* End of file affiliate.php */
/* Location: ./application/controllers/admin/affiliate.php */