<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Badges extends MY_Controller{

   function __construct(){
		parent::__construct();
		$this->load->helper(array('cookie','date','form'));
		$this->load->library(array('encrypt','form_validation'));
		$this->load->model('badges_model');
		if ($this->checkPrivileges('category',$this->privStatus) == FALSE){
			redirect('admin');
		}
	}
	public function index(){
	  if($this->checkLogin('A') == ''){
		 redirect('admin');
	  }else{
		 redirect('admin/badges/badges_list');
		}
	}
	public function users_list(){
	  if($this->checkLogin('A') == ''){
		 redirect('admin');
	  }else{
		 $this->data['heading'] = 'Users List';
		 $bages_id = $this->uri->segment(4,0);
		 $Query = "select * from ".USERS." where FIND_IN_SET(".$bages_id.",badges)";
		 $this->data["usersList"] = $this->badges_model->ExecuteQuery($Query);
		 //echo $this->data["usersList"]->num_rows();
		  //die;
		 $this->load->view('admin/badges/users_list',$this->data);
		}
	}
	public function badges_list(){
	  if($this->checkLogin('A') == ''){
		 redirect('admin');
	  }else{
	     $condition = array();
		 $this->data['badgesList'] = $this->badges_model->get_all_details(BADGES,$condition);
		 
		 $this->data['heading'] = 'Badges List';
		 $this->load->view('admin/badges/badges_list',$this->data);
		}
	}
	public function add_badges(){
	  if($this->checkLogin('A') == ''){
		 redirect('admin');
	  }else{
		 $this->data['heading'] = 'Add Bages';
		 $sortArr1 = array('field'=>'cat_position','type'=>'asc');
	     $sortArr = array($sortArr1);
		 $this->data['MainCategories'] = $this->badges_model->get_all_details(CATEGORY,array('rootID'=>'0','status'=>'Active'),$sortArr);
		 $this->load->view('admin/badges/add_badges',$this->data);
		}
   }
   public function insertEditbadges(){
     if($this->checkLogin('A') == ''){
		 redirect('admin');
	  }else{
	     $excludeArr = array('badge_id','category','badge_image');
	     if($this->input->post('badge_id') ==''){
		    
		    $condition = array('category'=>$this->input->post('category'),'badgethings'=>$this->input->post('badgethings'));
			$get_details = $this->badges_model->get_all_details(BADGES,$condition);
			if($get_details->num_rows() >0){
			   $this->setErrorMessage('error','This Badge Already Exists');
			   redirect('admin/badges/add_badges');
			}else{
		       $config['overwrite'] = FALSE;
			   $config['allowed_types'] = 'jpg|jpeg|gif|png';
			   $config['upload_path'] = './images/badges';
			   $this->load->library('upload', $config);
			   if($this->upload->do_upload('badge_image')){
			      $upload_data = $this->upload->data(); 
                  $file_name = $upload_data['file_name'];	
			   } 
		       $dataArr = array('status'=> 'Active','category'=>$this->input->post('category'),'badge_image'=>$file_name);
			   $this->badges_model->commonInsertUpdate(BADGES,'insert',$excludeArr,$dataArr);
			   $this->setErrorMessage('success','Badge added successfully');
			}
		 }else{
		       $config['overwrite'] = FALSE;
			   $config['allowed_types'] = 'jpg|jpeg|gif|png';
			   $config['upload_path'] = './images/badges';
			   $this->load->library('upload', $config);
			   if($this->upload->do_upload('badge_image')){
			      $upload_data = $this->upload->data(); 
                  $file_name = $upload_data['file_name'];	
			   }
		   if($file_name==''){
			  $dataArr = array('category'=>$this->input->post('category'));
		   }else{
		      $dataArr = array('category'=>$this->input->post('category'),'badge_image'=>$file_name);
		   }
		   $this->badges_model->commonInsertUpdate(BADGES,'update',$excludeArr,$dataArr,array('id'=>$this->input->post('badge_id')));
			    $this->setErrorMessage('success','Badge updated successfully');
		 }
		 redirect('admin/badges/badges_list');
	  }
   }
   public function change_status(){
	    if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$mode = $this->uri->segment(4,0);
			$user_id = $this->uri->segment(5,0);
			$status = ($mode == '0')?'Inactive':'Active';
			$newdata = array('status' => $status);
			$condition = array('id' => $user_id);
			$this->badges_model->update_details(BADGES,$newdata,$condition);
			$this->setErrorMessage('success','Badge Status Changed Successfully');
			redirect('admin/badges/badges_list');
		}
  }
  public function edit_badges(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Edit Badge';
			$id = $this->uri->segment(4,0);
			$condition = array('id' => $id);
			$this->data['badge_details'] = $this->badges_model->get_all_details(BADGES,$condition);
			$sortArr1 = array('field'=>'cat_position','type'=>'asc');
	        $sortArr = array($sortArr1);
		      $this->data['MainCategories'] = $this->badges_model->get_all_details(CATEGORY,array('rootID'=>'0','status'=>'Active'),$sortArr);
			if ($this->data['badge_details']->num_rows() == 1){
				$this->load->view('admin/badges/edit_badges',$this->data);
			}else {
				redirect('admin');
			}
		}
  }
  public function delete_badges(){
	    if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
		   $id = $this->uri->segment(4,0);
		   $this->badges_model->commonDelete(BADGES,array('id'=>$id));
		   $this->setErrorMessage('success','Badge Deleted successfully');
           redirect('admin/badges/badges_list');		   
		}
  }
  
}
