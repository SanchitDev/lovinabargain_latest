<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * User related functions
 * @author Teamtweaks
 *
 */

class Checkout extends MY_Controller { 
	function __construct(){
        parent::__construct();
		$this->load->helper(array('cookie','date','form','email'));
		$this->load->library(array('encrypt','form_validation'));	
		$this->load->library('paypal_ec');		
		$this->load->model('checkout_model');
		$this->load->model('cart_model');
		if($_SESSION['sMainCategories'] == ''){
			$sortArr1 = array('field'=>'cat_position','type'=>'asc');
			$sortArr = array($sortArr1);
			$_SESSION['sMainCategories'] = $this->checkout_model->get_all_details(CATEGORY,array('rootID'=>'0','status'=>'Active'),$sortArr);
		}
		$this->data['mainCategories'] = $_SESSION['sMainCategories'];
		
		if($_SESSION['sColorLists'] == ''){
			$_SESSION['sColorLists'] = $this->checkout_model->get_all_details(LIST_VALUES,array('list_id'=>'1'));
		}
		$this->data['mainColorLists'] = $_SESSION['sColorLists'];
		
		$this->data['loginCheck'] = $this->checkLogin('U');
		$this->data['countryList'] = $this->checkout_model->get_all_details(COUNTRY_LIST,array());
		define("API_LOGINID",$this->config->item('payment_2'));
		/***********************Adaptive Payment configuration***************/
		$this->config->load('paypal_adaptive');
		$config = array(
				'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
				'APIUsername' => $this->config->item('APIUsername'), 	// PayPal API username of the API caller
				'APIPassword' => $this->config->item('APIPassword'), 	// PayPal API password of the API caller
				'APISignature' => $this->config->item('APISignature'), 	// PayPal API signature of the API caller
				'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
				'APIVersion' => $this->config->item('APIVersion'), 		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
				'DeviceID' => $this->config->item('DeviceID'),
				'ApplicationID' => $this->config->item('ApplicationID'),
				'DeveloperEmailAccount' => $this->config->item('DeveloperEmailAccount')
		);
		$this->load->library('paypal/Paypal_adaptive', $config);
		/****************************END***********************/
    }
    
  
	/**
	 * 
	 * Loading Cart Page
	 */
	
	public function index(){
	 	
		if ($this->data['loginCheck'] != ''){ 
		  $checkout = $this->uri->segment(2);
			$this->data['heading'] = 'Checkout'; 
			
//			$this->data['checkoutViewResults'] = $this->checkout_model->mani_checkout_total($this->data['common_user_id']);	
//			$this->data['GiftViewTotal'] = $this->checkout_model->mani_gift_total($this->data['common_user_id']);				
			$this->data['SubCribViewTotal'] = $this->checkout_model->mani_subcribe_total($this->data['common_user_id']);							
			//echo '<pre>'; print_r($this->data['checkoutViewResults']); die;
			$this->data['countryList'] = $this->checkout_model->get_all_details(COUNTRY_LIST,array());	
			$this->data['AffUserDetails'] = $this->checkout_model->get_all_details(USERS,array('id' => $this->checkLogin('U')));
			
//			if( $checkout == 'gift'){
//			$this->load->view('site/checkout/checkout_paypal',$this->data);
//			}else{
			$this->load->view('site/checkout/checkout_credit_card_subscribe',$this->data);
//			}
		 	//$this->load->view('site/checkout/checkout.php',$this->data);
		}else{
			redirect('login');
		}	
	}
	
	
	/****************** Insert the checkout to user********************/
	
	public function PaymentProcess(){
	//echo "<pre>"; print_r($this->input->post());die;
		$excludeArr = array('paypalmode','paypalEmail','total_price','PaypalSubmit');
    	$dataArr = array();
    	$condition =array('id' => $this->checkLogin('U'));
		$this->checkout_model->commonInsertUpdate(USERS,'update',$excludeArr,$dataArr,$condition);
	
		
		//echo '<pre>';print_r($_POST); die;
	
			/*Paypal integration start */
			$this->load->library('paypal_class');
			
			$item_name = $this->config->item('email_title').' Products';
			
			$totalAmount = $this->input->post('total_price');
			//User ID
			$loginUserId = $this->checkLogin('U');
			//DealCodeNumber
			$lastFeatureInsertId = $this->session->userdata('randomNo');
			
			$quantity = 1;
			
			if($this->input->post('paypalmode') == 'sandbox'){
				$this->paypal_class->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';   // testing paypal url
			}else{
				$this->paypal_class->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';     // paypal url
			}
			
			$this->paypal_class->add_field('currency_code', $this->data['currencyType']);
			
			$this->paypal_class->add_field('business',$this->input->post('paypalEmail')); // Business Email
			
			$this->paypal_class->add_field('return',base_url().'order/success/'.$loginUserId.'/'.$lastFeatureInsertId); // Return URL
			
			$this->paypal_class->add_field('cancel_return', base_url().'order/failure'); // Cancel URL
			
			$this->paypal_class->add_field('notify_url', base_url().'order/notify'); // Notify url
			
			$this->paypal_class->add_field('custom', $loginUserId.'|'.$lastFeatureInsertId.'|Product'); // Custom Values			
			
			$this->paypal_class->add_field('item_name', $item_name); // Product Name
			
			$this->paypal_class->add_field('user_id', $loginUserId);
			
			$this->paypal_class->add_field('quantity', $quantity); // Quantity
			//echo $totalAmount;die;
			  $this->paypal_class->add_field('amount', $totalAmount); // Price
			//$this->paypal_class->add_field('amount', 1); // Price
			
//			echo base_url().'order/success/'.$loginUserId.'/'.$lastFeatureInsertId; die;
			
			$this->paypal_class->submit_paypal_post(); 
						
	}
	
	public function PaymentCredit(){
	//echo "<pre>"; print_r($this->input->post());die;
	    
		$excludeArr = array('creditvalue','shipping_id','cardType','email','cardNumber','CCExpDay','CCExpMnth','creditCardIdentifier','total_price','CreditSubmit','crt');
    	$dataArr = array();
    	$condition =array('id' => $this->checkLogin('U'));
		
		$this->checkout_model->commonInsertUpdate(USERS,'update',$excludeArr,$dataArr,$condition);
		
		//User ID
			$loginUserId = $this->checkLogin('U');
		//DealCodeNumber
			$lastFeatureInsertId = $this->session->userdata('randomNo');
		
		if($this->input->post('creditvalue')=='authorize'){	
			//Authorize.net Intergration

			$Auth_Details=unserialize(API_LOGINID); 
			$Auth_Setting_Details=unserialize($Auth_Details['settings']);	
			//echo $Auth_Setting_Details['Login_ID']."<br> ".$Auth_Setting_Details['Transaction_Key']."<br> ".$Auth_Setting_Details['mode'];die;
			
			 
			define("AUTHORIZENET_API_LOGIN_ID",$Auth_Setting_Details['Login_ID']);    // Add your API LOGIN ID
			define("AUTHORIZENET_TRANSACTION_KEY",$Auth_Setting_Details['Transaction_Key']); // Add your API transaction key
			define("API_MODE",$Auth_Setting_Details['mode']);

				if(API_MODE	=='sandbox'){
					define("AUTHORIZENET_SANDBOX",true);// Set to false to test against production
				}else{
					define("AUTHORIZENET_SANDBOX",false);
				}       
				define("TEST_REQUEST", "FALSE"); 
				require_once './authorize/AuthorizeNet.php';
				
				$transaction = new AuthorizeNetAIM;
				$transaction->setSandbox(AUTHORIZENET_SANDBOX);
				$transaction->setFields(
					array(
					'amount' =>  $this->input->post('total_price'), 
					'card_num' =>  $this->input->post('cardNumber'), 
					'exp_date' => $this->input->post('CCExpDay').'/'.$this->input->post('CCExpMnth'),
					'first_name' => $this->input->post('full_name'),
					'last_name' => '',
					'address' => $this->input->post('address'),
					'city' => $this->input->post('city'),
					'state' => $this->input->post('state'),
					'country' => $this->input->post('country'),
					'phone' => $this->input->post('phone_no'),
					'email' =>  $this->input->post('email'),
					'card_code' => $this->input->post('creditCardIdentifier'),
					)
				);
				$response = $transaction->authorizeAndCapture();
				//echo "<pre>";print_r($response);die;
			if($response->approved ){
				//$moveShoppingDataToPayment = $this->ibrandshopping_model->moveShoppingDataToPayment(); 
				//redirect('site/shopcart/returnpage/'.$loginUserId.'/'.$lastFeatureInsertId.'/'.$response->transaction_id);
				redirect('order/success/'.$loginUserId.'/'.$lastFeatureInsertId.'/'.$response->transaction_id);
 			}else{		
				//redirect('site/shopcart/cancel?failmsg='.$response->response_reason_text); 
				redirect('order/failure/'.$response->response_reason_text); 
			}

		}else if($this->input->post('creditvalue')=='paypaldodirect'){	
			
					$shipValID = $this->checkout_model->get_all_details(SHIPPING_ADDRESS,array( 'id' => $this->input->post('shipping_id')));	
					//echo '<pre>';print_r($shipValID->row()); die;
				
					$PaypalDodirect = unserialize($this->data['paypal_credit_card_settings']['settings']);
					$dodirects = array(
						'Sandbox' => $PaypalDodirect['mode'], 			// Sandbox / testing mode option.
						'APIUsername' =>$PaypalDodirect['Paypal_API_Username'], 	// PayPal API username of the API caller
						'APIPassword' => $PaypalDodirect['paypal_api_password'], 	// PayPal API password of the API caller
						'APISignature' => $PaypalDodirect['paypal_api_Signature'], 	// PayPal API signature of the API caller
						'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
						'APIVersion' => '85.0'		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
					);
					
					// Show Errors
					if($dodirects['Sandbox']){
						 
						ini_set('display_errors', '1');
					}
					
				
					$this->load->library('Paypal_pro', $dodirects);	
				
					$DPFields = array(
									'paymentaction' => '', 						// How you want to obtain payment.  Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
									'ipaddress' => $this->input->ip_address(), 							// Required.  IP address of the payer's browser.
									'returnfmfdetails' => '1'				// Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
								);
								
								
				$CCDetails = array(
									'creditcardtype' => $this->input->post('cardType'), 					// Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
									'acct' => $this->input->post('cardNumber'), 								// Required.  Credit card number.  No spaces or punctuation.  
									'expdate' => $this->input->post('CCExpDay').$this->input->post('CCExpMnth'), 	// Required.  Credit card expiration date.  Format is MMYYYY
									'cvv2' => $this->input->post('creditCardIdentifier'), 				// Requirements determined by your PayPal account settings.  Security digits for credit card.
									'startdate' => '', 							// Month and year that Maestro or Solo card was issued.  MMYYYY
									'issuenumber' => ''							// Issue number of Maestro or Solo card.  Two numeric digits max.
								);
								
				$PayerInfo = array(
									'email' => $this->input->post('email'), 	// Email address of payer.
									'payerid' => '', 							// Unique PayPal customer ID for payer.
									'payerstatus' => '', 	// Status of payer.  Values are verified or unverified
									'business' => '' 		
														// Payer's business name.
								);
								
				$PayerName = array(
									'salutation' => 'Mr.', 						// Payer's salutation.  20 char max.
									'firstname' => $this->input->post('full_name'), 							// Payer's first name.  25 char max.
									'middlename' => '', 						// Payer's middle name.  25 char max.
									'lastname' => '', 							// Payer's last name.  25 char max.
									'suffix' => ''								// Payer's suffix.  12 char max.
								);

					//'x_amount'				=> ,
					//			'x_email'				=> $this->input->post('email'),
								
				$BillingAddress = array(
										'street' => $this->input->post('address'), 						// Required.  First street address.
										'street2' => '', 						// Second street address.
										'city' => $this->input->post('city'), 							// Required.  Name of City.
										'state' => $this->input->post('state'), 							// Required. Name of State or Province.
										'countrycode' => $this->input->post('country'), 					// Required.  Country code.
										'zip' => $this->input->post('postal_code'), 							// Required.  Postal code of payer.
										'phonenum' => $this->input->post('phone_no') 						// Phone Number of payer.  20 char max.
									);
									
				$ShippingAddress = array(
										'shiptoname' => $shipValID->row()->full_name,		// Required if shipping is included.  Person's name associated with this address.  32 char max.
										'shiptostreet' => $shipValID->row()->address1,		// Required if shipping is included.  First street address.  100 char max.
										'shiptostreet2' => $shipValID->row()->address2,  	// Second street address.  100 char max.
										'shiptocity' => $shipValID->row()->city,			// Required if shipping is included.  Name of city.  40 char max.
										'shiptostate' => $shipValID->row()->state,			// Required if shipping is included.  Name of state or province.  40 char max.
										'shiptozip' => $shipValID->row()->postal_code, 		// Required if shipping is included.  Postal code of shipping address.  20 char max.
										'shiptocountry' => $shipValID->row()->country, 		// Required if shipping is included.  Country code of shipping address.  2 char max.
										'shiptophonenum' => $shipValID->row()->phone		// Phone number for shipping address.  20 char max.
										);
									
				$PaymentDetails = array(
										'amt' => $this->input->post('total_price'), 							// Required.  Total amount of order, including shipping, handling, and tax.  
										'currencycode' => $this->data['currencyType'], 					// Required.  Three-letter currency code.  Default is USD.
										'itemamt' => '', 						// Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
										'shippingamt' => '', 					// Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
										'insuranceamt' => '', 					// Total shipping insurance costs for this order.  
										'shipdiscamt' => '', 					// Shipping discount for the order, specified as a negative number.
										'handlingamt' => '', 					// Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
										'taxamt' => '', 						// Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax. 
										'desc' => '', 							// Description of the order the customer is purchasing.  127 char max.
										'custom' => '', 						// Free-form field for your own use.  256 char max.
										'invnum' => '', 						// Your own invoice or tracking number
										'buttonsource' => '', 					// An ID code for use by 3rd party apps to identify transactions.
										'notifyurl' => '', 						// URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
										'recurring' => ''						// Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
									);
				
				// For order items you populate a nested array with multiple $Item arrays.  
				// Normally you'll be looping through cart items to populate the $Item array
				// Then push it into the $OrderItems array at the end of each loop for an entire 
				// collection of all items in $OrderItems.
						
				$OrderItems = array();
					
				$Item	 = array(
									'l_name' => '', 						// Item Name.  127 char max.
									'l_desc' => '', 						// Item description.  127 char max.
									'l_amt' => '', 							// Cost of individual item.
									'l_number' => '', 						// Item Number.  127 char max.
									'l_qty' => '', 							// Item quantity.  Must be any positive integer.  
									'l_taxamt' => '', 						// Item's sales tax amount.
									'l_ebayitemnumber' => '', 				// eBay auction number of item.
									'l_ebayitemauctiontxnid' => '', 		// eBay transaction ID of purchased item.
									'l_ebayitemorderid' => '' 				// eBay order ID for the item.
							);
				
				array_push($OrderItems, $Item);
				
				$Secure3D = array(
								  'authstatus3d' => '', 
								  'mpivendor3ds' => '', 
								  'cavv' => '', 
								  'eci3ds' => '', 
								  'xid' => ''
								  );
								  
				$PayPalRequestData = array(
										'DPFields' => $DPFields, 
										'CCDetails' => $CCDetails, 
										'PayerInfo' => $PayerInfo, 
										'PayerName' => $PayerName, 
										'BillingAddress' => $BillingAddress, 
										'ShippingAddress' => $ShippingAddress, 
										'PaymentDetails' => $PaymentDetails, 
										'OrderItems' => $OrderItems, 
										'Secure3D' => $Secure3D
									);
									
				$PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);
				
			
				if(!$this->paypal_pro->APICallSuccessful($PayPalResult['ACK'])){
					$errors = array('Errors'=>$PayPalResult['ERRORS']);
					//$this->load->view('paypal_error',$errors);
					$newerrors = $errors['Errors'][0]['L_LONGMESSAGE'];
					redirect('order/failure/'.$newerrors); 
				}else{
					// Successful call.  Load view or whatever you need to do here.	
					redirect('order/success/'.$loginUserId.'/'.$lastFeatureInsertId.'/'.$PayPalResult['TRANSACTIONID']);
				}
					
					
		} else if($this->input->post('creditvalue') == 'dauthorization'){
            /***  authorization using credit card  ***/
           
			$cart_id = $this->input->post('crt');
            $cart_details = $this->checkout_model->get_all_details(SHOPPING_CART,array('id'=>$cart_id));
            if( $cart_details->num_rows() == '1'){
                
                $product_id  = $cart_details->row()->product_id;
                $user_id     = $cart_details->row()->user_id;
                $sell_id     = $cart_details->row()->sell_id;
				$total_price =  $cart_details->row()->authorised_amount;
				
                $user_details = $this->checkout_model->get_all_details(USERS,array('id'=>$user_id,'status'=>'Active'));

                

                $shipValID = $this->checkout_model->get_all_details(SHIPPING_ADDRESS,array( 'country' => $this->input->post('shipping_id') ));         

                        $PaypalDodirect = unserialize($this->data['paypal_credit_card_settings']['settings']);
                        
                        $dodirects = array(
                            'Sandbox' => $PaypalDodirect['mode'],           // Sandbox / testing mode option.
                            'APIUsername' =>$PaypalDodirect['Paypal_API_Username'],     // PayPal API username of the API caller
                            'APIPassword' => $PaypalDodirect['paypal_api_password'],    // PayPal API password of the API caller
                            'APISignature' => $PaypalDodirect['paypal_api_Signature'],  // PayPal API signature of the API caller
                            'APISubject' => '',                                     // PayPal API subject (email address of 3rd party user that has granted API permission for your app)
                            'APIVersion' => '85.0'      // API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
                        );
                      
                        // Show Errors
                        if($dodirects['Sandbox']){
                             
                            ini_set('display_errors', '1');
                        }


                        $this->load->library('paypal/Paypal_pro', $dodirects);

                        $DPFields = array(
                                        'paymentaction' => 'authorization',     // How you want to obtain payment.  Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
                                        'ipaddress' => $this->input->ip_address(),                          // Required.  IP address of the payer's browser.
                                        'returnfmfdetails' => '1'               // Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
                                    );


                    $CCDetails = array(
                                        'creditcardtype' => $this->input->post('cardType'), // Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
                                        'acct' => $this->input->post('cardNumber'), // Required.  Credit card number.  No spaces or punctuation.
                                        'expdate' => $this->input->post('CCExpDay') . $this->input->post('CCExpMnth'), // Required.  Credit card expiration date.  Format is MMYYYY
                                        'cvv2' => $this->input->post('creditCardIdentifier'), // Requirements determined by your PayPal account settings.  Security digits for credit card.
                                        'startdate' => '',                          // Month and year that Maestro or Solo card was issued.  MMYYYY
                                        'issuenumber' => ''                         // Issue number of Maestro or Solo card.  Two numeric digits max.
                                    );
                    
                    $PayerInfo = array(
                                        'email' => $user_details->row()->email,  // Email address of payer.
                                        'payerid' => '',                            // Unique PayPal customer ID for payer.
                                        'payerstatus' => '',    // Status of payer.  Values are verified or unverified
                                        'business' => ''
                                                            // Payer's business name.
                                    );
                    
                    $PayerName = array(
                                        'salutation' => 'Mr.',                      // Payer's salutation.  20 char max.
                                        'firstname' => $this->input->post('full_name'),                          // Payer's first name.  25 char max.
                                        'middlename' => '',                         // Payer's middle name.  25 char max.
                                        'lastname' => '',                           // Payer's last name.  25 char max.
                                        'suffix' => ''                              // Payer's suffix.  12 char max.
                                    );

                    $BillingAddress = array(
                                           'street' => $this->input->post('address'), // Required.  First street address.
                                            'street2' => '', // Second street address.
                                            'city' => $this->input->post('city'), // Required.  Name of City.
                                            'state' => $this->input->post('state'), // Required. Name of State or Province.
                                            'countrycode' => $this->input->post('country'), // Required.  Country code.
                                            'zip' => $this->input->post('postal_code'), // Required.  Postal code of payer.
                                            'phonenum' => $this->input->post('phone_no')       // Phone Number of payer.  20 char max.
                                        );
                    
                    $ShippingAddress = array(
                                             'shiptoname' => $shipValID->row()->full_name,       // Required if shipping is included.  Person's name associated with this address.  32 char max.
                                            'shiptostreet' => $shipValID->row()->address1,      // Required if shipping is included.  First street address.  100 char max.
                                            'shiptostreet2' => $shipValID->row()->address2,     // Second street address.  100 char max.
                                            'shiptocity' => $shipValID->row()->city,            // Required if shipping is included.  Name of city.  40 char max.
                                            'shiptostate' => $shipValID->row()->state,          // Required if shipping is included.  Name of state or province.  40 char max.
                                            'shiptozip' => $shipValID->row()->postal_code,      // Required if shipping is included.  Postal code of shipping address.  20 char max.
                                            'shiptocountry' => $shipValID->row()->country,      // Required if shipping is included.  Country code of shipping address.  2 char max.
                                            'shiptophonenum' => $shipValID->row()->phone        // Phone number for shipping address.  20 char max.
                                             );

                    $PaymentDetails = array(
                                            'amt' => $total_price, // Required.  Total amount of order, including shipping, handling, and tax.
                                            'currencycode' => $this->data['currencyType'], // Required.  Three-letter currency code.  Default is USD.
                                            'itemamt' => '',                        // Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
                                            'shippingamt' => '',                    // Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
                                            'insuranceamt' => '',                   // Total shipping insurance costs for this order.
                                            'shipdiscamt' => '',                    // Shipping discount for the order, specified as a negative number.
                                            'handlingamt' => '',                    // Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
                                            'taxamt' => '',                         // Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax.
                                            'desc' => '',                           // Description of the order the customer is purchasing.  127 char max.
                                            'custom' => '',                         // Free-form field for your own use.  256 char max.
                                            'invnum' => '',                         // Your own invoice or tracking number
                                            'buttonsource' => '',                   // An ID code for use by 3rd party apps to identify transactions.
                                            'notifyurl' => '',                      // URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
                                            'recurring' => ''                       // Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
                                        );

                    // For order items you populate a nested array with multiple $Item arrays.
                    // Normally you'll be looping through cart items to populate the $Item array
                    // Then push it into the $OrderItems array at the end of each loop for an entire
                    // collection of all items in $OrderItems.
                    
                    $OrderItems = array();

                    $Item    = array(
                                        'l_name' => '',                         // Item Name.  127 char max.
                                        'l_desc' => '',                         // Item description.  127 char max.
                                        'l_amt' => '',                          // Cost of individual item.
                                        'l_number' => '',                       // Item Number.  127 char max.
                                        'l_qty' => '',                          // Item quantity.  Must be any positive integer.
                                        'l_taxamt' => '',                       // Item's sales tax amount.
                                        'l_ebayitemnumber' => '',               // eBay auction number of item.
                                        'l_ebayitemauctiontxnid' => '',         // eBay transaction ID of purchased item.
                                        'l_ebayitemorderid' => ''               // eBay order ID for the item.
                                );

                    array_push($OrderItems, $Item);

                    $Secure3D = array(
                                      'authstatus3d' => '',
                                      'mpivendor3ds' => '',
                                      'cavv' => '',
                                      'eci3ds' => '',
                                      'xid' => ''
                                      );

                    $PayPalRequestData = array(
                                            'DPFields' => $DPFields,
                                            'CCDetails' => $CCDetails,
                                            'PayerInfo' => $PayerInfo,
                                            'PayerName' => $PayerName,
                                            'BillingAddress' => $BillingAddress,
                                            'ShippingAddress' => $ShippingAddress,
                                            'PaymentDetails' => $PaymentDetails,
                                            'OrderItems' => $OrderItems,
                                            'Secure3D' => $Secure3D
                                        );
                  
                    $PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);
                   
                        if( $PayPalResult['ACK'] == 'Failure'){
                            $errors = array('Errors'=>$PayPalResult['ERRORS']);
                            $newerrors = $errors['Errors'][0]['L_LONGMESSAGE'];
                             redirect('order/failure/' . $newerrors);
							
                        }else if( $PayPalResult['ACK'] == 'Success' ){
                            $authorize_amt  = $PayPalResult['AMT'];
                            $currency_code  = $PayPalResult['CURRENCYCODE'];
                            $transaction_id = $PayPalResult['TRANSACTIONID'];
                            $paymentType  = 'Paypal Authorize Payment';
							$this->checkout_model->addAuctionPayment($loginUserId, $paymentType, $product_id, 'auction', $authorize_amt, $transaction_id,$cart_id);
							redirect('site/checkout/confirmation/'.$PayPalResult['TRANSACTIONID']);
                        }
                        else{
                            $errors = array('Errors'=>$PayPalResult['ERRORS']);
                            $newerrors = $errors['Errors'][0]['L_LONGMESSAGE'];
							$this->setErrorMessage('error', $newerrors);
							redirect('auctioncart');
                        }
            }else{
                redirect('auctioncart');
            }

        } else if($this->input->post('creditvalue') == 'eauthorization'){
			
			/***  authorization using express checkout...means..website rediect to paypal ***/
           
			$cart_id = $this->input->post('crt');
            $cart_details = $this->checkout_model->get_all_details(SHOPPING_CART,array('id'=>$cart_id));
            if( $cart_details->num_rows() == '1'){
                
                $product_id  	= $cart_details->row()->product_id;
                $user_id     	= $cart_details->row()->user_id;
                $sell_id     	= $cart_details->row()->sell_id;
				$paymentAmount 	=  $cart_details->row()->authorised_amount;
				
                $user_details = $this->checkout_model->get_all_details(USERS,array('id'=>$user_id,'status'=>'Active'));

                $shipValID = $this->checkout_model->get_all_details(SHIPPING_ADDRESS,array( 'country' => $this->input->post('shipping_id') ));         
				
				$auctionpaypaldata = array(
										'pdctid' => $product_id,
										'usrid' => $user_id,
										'crtid' => $cart_id
									);
                $this->session->set_userdata($auctionpaypaldata);
						// ==================================
						// PayPal Express Checkout Module
						// ==================================
						/* $paypal_credentials = array(
													'API_username' => 'vinubusiness1-facilitator_api1.gmail.com',    // PayPal API username of the API caller
													'API_signature' => '1380197781',  // PayPal API password of the API caller
													'API_password' => 'AQU0e5vuZCvSg-XJploSa.sGUDlpAERCJ01cF3SVakQxc3HQqQXQ.e4d',   // PayPal API signature of the API caller
													'sandbox_status' => true
												);
							*/
						
						
						
						$to_buy = array(
									'desc' => 'Purchase from ACME Store', 
									'currency' => 'USD', 
									'type' => 'Authorization', 
									'return_URL' => base_url()."site/checkout/back", 
									'cancel_URL' => base_url()."auctioncart", 
									'shipping_amount' => $paymentAmount, 
									'get_shipping' => true,
									'amount' => $paymentAmount,
									'quantity' => 1
									);
							
								
								$set_ec_return = $this->paypal_ec->set_ec($to_buy);
								
								if (isset($set_ec_return['ec_status']) && ($set_ec_return['ec_status'] === true)) {
									
									$this->paypal_ec->redirect_to_paypal($set_ec_return['TOKEN'], true);
								} else {
									$this->_error($set_ec_return);
								}
						
            }else{
                redirect('auctioncart');
            }

        }else{
                redirect('auctioncart');
        }
	
	}
	
	public function back() {
		
		$token = $_GET['token'];
		$payer_id = $_GET['PayerID'];
		
		$get_ec_return = $this->paypal_ec->get_ec($token);
		
		if (isset($get_ec_return['ec_status']) && ($get_ec_return['ec_status'] === true)) {
			
			$ec_details = array(
				'token' => $token, 
				'payer_id' => $payer_id, 
				'currency' => 'USD', 
				'amount' => $get_ec_return['PAYMENTREQUEST_0_AMT'], 
				'IPN_URL' => base_url()."site/checkout/ipn", 
				'type' => 'Authorization');
				
			// DoExpressCheckoutPayment
			
			$do_ec_return = $this->paypal_ec->do_ec($ec_details);
			if (isset($do_ec_return['ec_status']) && ($do_ec_return['ec_status'] === true)) {
				
				/* echo "\nGetExpressCheckoutDetails Data\n" . print_r($get_ec_return, true);
				echo "\n\nDoExpressCheckoutPayment Data\n" . print_r($do_ec_return, true); */
				
					$successtoken  = $do_ec_return['TOKEN'];
					$authorize_amt  = $do_ec_return['PAYMENTINFO_0_AMT'];
					$currency_code  = $do_ec_return['PAYMENTINFO_0_CURRENCYCODE'];
					$transaction_id = $do_ec_return['PAYMENTINFO_0_TRANSACTIONID'];
					$paymentType  = 'Paypal Authorize Payment';
					
					$loginUserId 	= $this->session->userdata('usrid');
					$product_id     = $this->session->userdata('pdctid');
					$cart_id     	= $this->session->userdata('crtid');
					
					$this->checkout_model->addAuctionPayment($loginUserId, $paymentType, $product_id, 'auction', $authorize_amt, $transaction_id,$cart_id,$successtoken);
					
					$auctionpaypaldata = array(
										'pdctid' => '',
										'usrid' => '',
										'crtid' => ''
									);
					$this->session->set_userdata($auctionpaypaldata);
					
					redirect('site/checkout/confirmation/'.$transaction_id);
				
			} else {
				$newerrors = $get_ec_return['L_SHORTMESSAGE0'];
				$this->setErrorMessage('error', $newerrors);
				redirect('auctioncart');
			}
		} else {
			$newerrors = $get_ec_return['L_SHORTMESSAGE0'];
				$this->setErrorMessage('error', $newerrors);
				redirect('auctioncart');
		}
	}
	
	/* -------------------------------------------------------------------------------------------------
	* The location for your IPN_URL that you set for $this->paypal_ec->do_ec(). obviously more needs to
	* be done here. this is just a simple logging example. The /ipnlog folder should the same level as
	* your CodeIgniter's index.php
	* --------------------------------------------------------------------------------------------------
	*/
	function ipn() {
		$logfile = 'ipnlog/' . uniqid() . '.html';
		$logdata = "<pre>\r\n" . print_r($_POST, true) . '</pre>';
		file_put_contents($logfile, $logdata);
	}
	
	/* -------------------------------------------------------------------------------------------------
	* a simple message to display errors. this should only be used during development
	* --------------------------------------------------------------------------------------------------
	*/
	function _error($ecd) {
		echo '<html>
				<head>
					<meta http-equiv="refresh" content="3;url='.base_url().'auctioncart" />
				</head>
				<body>
					<h1>Redirecting in 3 seconds...</h1>
				</body>
			</html>';
		//echo "<br>error at Express Checkout<br>";
		//echo "<pre>" . print_r($ecd, true) . "</pre>";
		//echo "<br>CURL error message<br>";
		echo 'Message:' . $this->session->userdata('curl_error_msg') . '<br>';
		//echo 'Number:' . $this->session->userdata('curl_error_no') . '<br>';
	}
	
	
    public function confirmation(){
       
        $this->data['Confirmation']   = 'Success';
        $this->data['trasaction_id']  = $this->uri->segment(4);
        $this->data['payment_detail'] = $this->checkout_model->get_all_details(PAYMENT,array('paypal_transaction_id' =>$this->data['trasaction_id'])); 
        $this->data['product_detail'] = $this->checkout_model->get_all_details(PRODUCT,array('id' =>$this->data['payment_detail']->row()->product_id)); 
        $this->data['randomNo']       = $this->session->userdata('randomNo');
      
        $this->load->view('site/order/auction_confirmation',$this->data);             
    }
	
	
	
	// 3/5/14 edited by sam
	public function AffiliatePaymentProcess(){
	
		$excludeArr = array('total_price','PaypalSubmit');
    	$dataArr = array();
    	$condition =array('id' => $this->checkLogin('U'));
		$this->checkout_model->commonInsertUpdate(USERS,'update',$excludeArr,$dataArr,$condition);
		
		
		
		//User ID
		$loginUserId = $this->checkLogin('U');
		//DealCodeNumber
		$lastFeatureInsertId = $this->session->userdata('randomNo');
		
		redirect('order/affiliatesuccess/'.$loginUserId.'/'.$lastFeatureInsertId);
						
	}
	
	
	/************************** Gift Cart Submit Options  *******************************/
	
	
	public function PaymentProcessGift(){
	
		$excludeArr = array('paypalmode','paypalEmail','total_price','PaypalSubmit');
    	$dataArr = array();
    	$condition =array('id' => $this->checkLogin('U'));
		$this->checkout_model->commonInsertUpdate(USERS,'update',$excludeArr,$dataArr,$condition);
	
			/*Paypal integration start */
			$this->load->library('paypal_class');
			
			$item_name = $this->config->item('email_title').' Gifts';
			
			$totalAmount = $this->input->post('total_price');                
			//User ID
			$loginUserId = $this->checkLogin('U');
			
			$quantity = 1;
			
			if($this->input->post('paypalmode') == 'sandbox'){
				$this->paypal_class->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';   // testing paypal url
			}else{
				$this->paypal_class->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';     // paypal url
			}
			
			$this->paypal_class->add_field('currency_code', $this->data['currencyType']);
			
			$this->paypal_class->add_field('business',$this->input->post('paypalEmail')); // Business Email
			
			$this->paypal_class->add_field('return',base_url().'order/giftsuccess/'.$loginUserId); // Return URL
			
			$this->paypal_class->add_field('cancel_return', base_url().'order/failure'); // Cancel URL
			
			$this->paypal_class->add_field('notify_url', base_url().'order/notify'); // Notify url
			
			$this->paypal_class->add_field('custom', $loginUserId.'|Gift'); // Custom Values			
			
			$this->paypal_class->add_field('item_name', $item_name); // Product Name
			
			$this->paypal_class->add_field('user_id', $loginUserId);
			
			$this->paypal_class->add_field('quantity', $quantity); // Quantity
			//echo $totalAmount;die;
			  $this->paypal_class->add_field('amount', $totalAmount); // Price
			//$this->paypal_class->add_field('amount', 1); // Price
			
			$this->paypal_class->submit_paypal_post(); 
						
	}
	
	public function PaymentCreditGift(){
	
		
		$excludeArr = array('creditvalue','cardType','email','cardNumber','CCExpDay','CCExpMnth','creditCardIdentifier','total_price','CreditSubmit');
    	$dataArr = array();
    	$condition =array('id' => $this->checkLogin('U'));
		$this->checkout_model->commonInsertUpdate(USERS,'update',$excludeArr,$dataArr,$condition);
	
		//User ID
			$loginUserId = $this->checkLogin('U');
		if($this->input->post('creditvalue')=='authorize'){
			
			
			$Auth_Details=unserialize(API_LOGINID); 
			$Auth_Setting_Details=unserialize($Auth_Details['settings']);	

			 
			define("AUTHORIZENET_API_LOGIN_ID",$Auth_Setting_Details['Login_ID']);    // Add your API LOGIN ID
			define("AUTHORIZENET_TRANSACTION_KEY",$Auth_Setting_Details['Transaction_Key']); // Add your API transaction key
			define("API_MODE",$Auth_Setting_Details['mode']);

				if(API_MODE	=='sandbox'){
					define("AUTHORIZENET_SANDBOX",true);// Set to false to test against production
				}else{
					define("AUTHORIZENET_SANDBOX",false);
				}       
				define("TEST_REQUEST", "FALSE"); 
				require_once './authorize/AuthorizeNet.php';
				
				$transaction = new AuthorizeNetAIM;
				$transaction->setSandbox(AUTHORIZENET_SANDBOX);
				$transaction->setFields(
					array(
					'amount' =>  $this->input->post('total_price'), 
					'card_num' =>  $this->input->post('cardNumber'), 
					'exp_date' => $this->input->post('CCExpDay').'/'.$this->input->post('CCExpMnth'),
					'first_name' => $this->input->post('full_name'),
					'last_name' => '',
					'address' => $this->input->post('address'),
					'city' => $this->input->post('city'),
					'state' => $this->input->post('state'),
					'country' => $this->input->post('country'),
					'phone' => $this->input->post('phone_no'),
					'email' =>  $this->input->post('email'),
					'card_code' => $this->input->post('creditCardIdentifier'),
					)
				);
				$response = $transaction->authorizeAndCapture();
		
			if( $response->approved ){
				//$moveShoppingDataToPayment = $this->ibrandshopping_model->moveShoppingDataToPayment(); 
				redirect('order/giftsuccess/'.$loginUserId.'/'.$response->transaction_id);
 			}else{		
				//redirect('site/shopcart/cancel?failmsg='.$response->response_reason_text); 
				redirect('order/failure/'.$response->response_reason_text); 
			}
			
		}else if($this->input->post('creditvalue')=='paypaldodirect'){	
			
			$PaypalDodirect = unserialize($this->data['paypal_credit_card_settings']['settings']);
			$dodirects = array(
				'Sandbox' => $PaypalDodirect['mode'], 			// Sandbox / testing mode option.
				'APIUsername' =>$PaypalDodirect['Paypal_API_Username'], 	// PayPal API username of the API caller
				'APIPassword' => $PaypalDodirect['paypal_api_password'], 	// PayPal API password of the API caller
				'APISignature' => $PaypalDodirect['paypal_api_Signature'], 	// PayPal API signature of the API caller
				'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
				'APIVersion' => '85.0'		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
			);
			
			// Show Errors
			if($dodirects['Sandbox']){
				 
				ini_set('display_errors', '1');
			}
			
		
			$this->load->library('Paypal_pro', $dodirects);	
		
			$DPFields = array(
							'paymentaction' => '', 						// How you want to obtain payment.  Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
							'ipaddress' => $this->input->ip_address(), 							// Required.  IP address of the payer's browser.
							'returnfmfdetails' => '1'				// Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
						);
						
						
		$CCDetails = array(
							'creditcardtype' => $this->input->post('cardType'), 					// Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
							'acct' => $this->input->post('cardNumber'), 								// Required.  Credit card number.  No spaces or punctuation.  
							'expdate' => $this->input->post('CCExpDay').$this->input->post('CCExpMnth'), 	// Required.  Credit card expiration date.  Format is MMYYYY
							'cvv2' => $this->input->post('creditCardIdentifier'), 				// Requirements determined by your PayPal account settings.  Security digits for credit card.
							'startdate' => '', 							// Month and year that Maestro or Solo card was issued.  MMYYYY
							'issuenumber' => ''							// Issue number of Maestro or Solo card.  Two numeric digits max.
						);
						
		$PayerInfo = array(
							'email' => $this->input->post('email'), 	// Email address of payer.
							'payerid' => '', 							// Unique PayPal customer ID for payer.
							'payerstatus' => '', 	// Status of payer.  Values are verified or unverified
							'business' => '' 		
												// Payer's business name.
						);
						
		$PayerName = array(
							'salutation' => 'Mr.', 						// Payer's salutation.  20 char max.
							'firstname' => $this->input->post('full_name'), 							// Payer's first name.  25 char max.
							'middlename' => '', 						// Payer's middle name.  25 char max.
							'lastname' => '', 							// Payer's last name.  25 char max.
							'suffix' => ''								// Payer's suffix.  12 char max.
						);

//'x_amount'				=> ,
	//			'x_email'				=> $this->input->post('email'),
						
		$BillingAddress = array(
								'street' => $this->input->post('address'), 						// Required.  First street address.
								'street2' => '', 						// Second street address.
								'city' => $this->input->post('city'), 							// Required.  Name of City.
								'state' => $this->input->post('state'), 							// Required. Name of State or Province.
								'countrycode' => $this->input->post('country'), 					// Required.  Country code.
								'zip' => $this->input->post('postal_code'), 							// Required.  Postal code of payer.
								'phonenum' => $this->input->post('phone_no') 						// Phone Number of payer.  20 char max.
							);
							
		$ShippingAddress = array(
								'shiptoname' => $this->input->post('full_name'),		// Required if shipping is included.  Person's name associated with this address.  32 char max.
								'shiptostreet' => $this->input->post('address'),		// Required if shipping is included.  First street address.  100 char max.
								'shiptostreet2' => $this->input->post('address2'),  	// Second street address.  100 char max.
								'shiptocity' => $this->input->post('city'),			// Required if shipping is included.  Name of city.  40 char max.
								'shiptostate' => $this->input->post('state'),			// Required if shipping is included.  Name of state or province.  40 char max.
								'shiptozip' => $this->input->post('postal_code'), 		// Required if shipping is included.  Postal code of shipping address.  20 char max.
								'shiptocountry' => $this->input->post('country'), 		// Required if shipping is included.  Country code of shipping address.  2 char max.
								'shiptophonenum' => $this->input->post('phone_no')		// Phone number for shipping address.  20 char max.
								);
							
		$PaymentDetails = array(
								'amt' => $this->input->post('total_price'), 							// Required.  Total amount of order, including shipping, handling, and tax.  
								'currencycode' => $this->data['currencyType'], 					// Required.  Three-letter currency code.  Default is USD.
								'itemamt' => '', 						// Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
								'shippingamt' => '', 					// Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
								'insuranceamt' => '', 					// Total shipping insurance costs for this order.  
								'shipdiscamt' => '', 					// Shipping discount for the order, specified as a negative number.
								'handlingamt' => '', 					// Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
								'taxamt' => '', 						// Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax. 
								'desc' => '', 							// Description of the order the customer is purchasing.  127 char max.
								'custom' => '', 						// Free-form field for your own use.  256 char max.
								'invnum' => '', 						// Your own invoice or tracking number
								'buttonsource' => '', 					// An ID code for use by 3rd party apps to identify transactions.
								'notifyurl' => '', 						// URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
								'recurring' => ''						// Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
							);
		
		// For order items you populate a nested array with multiple $Item arrays.  
		// Normally you'll be looping through cart items to populate the $Item array
		// Then push it into the $OrderItems array at the end of each loop for an entire 
		// collection of all items in $OrderItems.
				
		$OrderItems = array();
			
		$Item	 = array(
							'l_name' => '', 						// Item Name.  127 char max.
							'l_desc' => '', 						// Item description.  127 char max.
							'l_amt' => '', 							// Cost of individual item.
							'l_number' => '', 						// Item Number.  127 char max.
							'l_qty' => '', 							// Item quantity.  Must be any positive integer.  
							'l_taxamt' => '', 						// Item's sales tax amount.
							'l_ebayitemnumber' => '', 				// eBay auction number of item.
							'l_ebayitemauctiontxnid' => '', 		// eBay transaction ID of purchased item.
							'l_ebayitemorderid' => '' 				// eBay order ID for the item.
					);
		
		array_push($OrderItems, $Item);
		
		$Secure3D = array(
						  'authstatus3d' => '', 
						  'mpivendor3ds' => '', 
						  'cavv' => '', 
						  'eci3ds' => '', 
						  'xid' => ''
						  );
						  
		$PayPalRequestData = array(
								'DPFields' => $DPFields, 
								'CCDetails' => $CCDetails, 
								'PayerInfo' => $PayerInfo, 
								'PayerName' => $PayerName, 
								'BillingAddress' => $BillingAddress, 
								'ShippingAddress' => $ShippingAddress, 
								'PaymentDetails' => $PaymentDetails, 
								'OrderItems' => $OrderItems, 
								'Secure3D' => $Secure3D
							);
							
		$PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);
		
	
		
		if(!$this->paypal_pro->APICallSuccessful($PayPalResult['ACK'])){
			$errors = array('Errors'=>$PayPalResult['ERRORS']);
			//$this->load->view('paypal_error',$errors);
			$newerrors = $errors['Errors'][0]['L_LONGMESSAGE'];
			redirect('order/failure/'.$newerrors); 
		}else{
			// Successful call.  Load view or whatever you need to do here.	
			redirect('order/success/'.$loginUserId.'/'.$lastFeatureInsertId.'/'.$PayPalResult['TRANSACTIONID']);
		}
			
			
		}
	
	}

	/******************************************* Subscribe Form *****************************************************************/
	
	public function PaymentCreditSubscribe(){
	
	
		$excludeArr = array('email','cardType','cardNumber','CCExpDay','CCExpMnth','creditCardIdentifier','total_price','CreditSubscribeSubmit','invoiceNumber');
    	$dataArr = array();
    	$condition =array('id' => $this->checkLogin('U'));
		$this->checkout_model->commonInsertUpdate(USERS,'update',$excludeArr,$dataArr,$condition);
		
		//Authorize.net Intergration
		$this->load->library('authorize_arb');
		
		// Start with a create object
		$this->authorize_arb->startData('create');
		
		// Locally-defined reference ID (can't be longer than 20 chars)
		$refId = substr(md5( microtime() . 'ref' ), 0, 20);
		$this->authorize_arb->addData('refId', $refId);
		
		// Data must be in this specific order
		// For full list of possible data, refer to the documentation:
		// http://www.authorize.net/support/ARB_guide.pdf
		
		
		$subscription_data = array(
			'name' => $this->config->item('email_title').' Subscription',
			'paymentSchedule' => array(
				'interval' => array(
					'length' => 1,
					'unit' => 'months',
					),
				'startDate' => date('Y-m-d'),
				'totalOccurrences' => 9999,
				'trialOccurrences' => 0,
				),
			'amount' => $this->config->item('total_price'),
			'trialAmount' => 0.00,
			'payment' => array(
				'creditCard' => array(
					'cardNumber' => $this->input->post('cardNumber'),
					'expirationDate' => $this->input->post('CCExpMnth').'-'.$this->input->post('CCExpDay'),
					'cardCode' => $this->input->post('creditCardIdentifier'),
					),
				),
			'order' => array(
				'invoiceNumber' => $this->config->item('invoiceNumber'),
				'description' =>  $this->config->item('email_title').' Subscription',
				),
			'customer' => array(
				'id' => $this->checkLogin('U'),
				'email' => $this->config->item('email'),
				'phoneNumber' => $this->config->item('phone_no'),
				),
			'billTo' => array(
				'firstName' => $this->config->item('full_name'),
				'lastName' => '',
				'address' => $this->config->item('address'),
				'city' => $this->config->item('city'),
				'state' => $this->config->item('state'),
				'zip' => $this->config->item('postal_code'),
				'country' => $this->config->item('country'),
				),
			);
			
		$this->authorize_arb->addData('subscription', $subscription_data);
		
		// Send request
		if( $this->authorize_arb->send() ){
			//echo '<h1>Success! ID: ' . $this->authorize_arb->getId() . '</h1>';
			redirect('order/subscribesuccess/'.$loginUserId.'/'.$this->authorize_arb->getId());
		}else{
			//echo '<h1>Epic Fail!</h1>';
			//echo '<p>' . $this->authorize_arb->getError() . '</p>';
			redirect('order/failure'); 
		}
		
		// Show debug data
		//$this->authorize_arb->debug();
		
	}
	
	/******************************************Payment Balance Zero Using Gift Card*******************************************************/
	
	public function PaymentGiftFree(){
	
		$excludeArr = array('total_price','PaypalSubmit');
    	$dataArr = array();
    	$condition =array('id' => $this->checkLogin('U'));
		$this->checkout_model->commonInsertUpdate(USERS,'update',$excludeArr,$dataArr,$condition);
	
		
		$item_name = $this->config->item('email_title').' Products';
			
		$totalAmount = $this->input->post('total_price');
			//User ID
		$loginUserId = $this->checkLogin('U');
			//DealCodeNumber
		$lastFeatureInsertId = $this->session->userdata('randomNo');
			
		redirect('order/successgift/'.$loginUserId.'/'.$lastFeatureInsertId); // Return URL
			
						
	}
	/***************************cash on delivery**************************************/
	public function CashonDelivery(){
		$excludeArr = array('total_price','CodSubmit','shipping_id');
		$dataArr = array();
		$condition =array('id' => $this->checkLogin('U'));
		$this->checkout_model->commonInsertUpdate(USERS,'update',$excludeArr,$dataArr,$condition);
		$loginUserId = $this->checkLogin('U');
		$lastFeatureInsertId = $this->session->userdata('randomNo');
		redirect('order/success/'.$loginUserId.'/'.$lastFeatureInsertId.'/0/cod');
	}
	
	
	public function cod_PaymentProcess(){
		$excludeArr = array('paypalmode','paypalEmail','total_price','PaypalSubmit');
		$dataArr = array();
		$condition =array('id' => $this->checkLogin('U'));
		$this->checkout_model->commonInsertUpdate(USERS,'update',$excludeArr,$dataArr,$condition);
		//echo '<pre>';print_r($_POST); die;
		/*Paypal integration start */
		$this->load->library('paypal_class');
		$item_name = $this->config->item('email_title').' Products';
		$totalAmount = $this->input->post('total_price');
		//User ID
		$loginUserId = $this->checkLogin('U');
		//DealCodeNumber
		$lastFeatureInsertId = $this->session->userdata('invoiceID');
		$quantity = 1;
		if($this->input->post('paypalmode') == 'sandbox'){
			$this->paypal_class->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';   // testing paypal url
		}else{
			$this->paypal_class->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';     // paypal url
		}
		$this->paypal_class->add_field('currency_code', $this->data['currencyType']);
		$this->paypal_class->add_field('business',$this->input->post('paypalEmail')); // Business Email
		$this->paypal_class->add_field('return',base_url().'order/cod/'.$loginUserId.'/'.$lastFeatureInsertId.'/'.$totalAmount); // Return URL
		$this->paypal_class->add_field('cancel_return', base_url().'order/failure'); // Cancel URL
		$this->paypal_class->add_field('notify_url', base_url().'site/order/ipnpayment'); // Notify url
		$this->paypal_class->add_field('custom', $loginUserId.'|'.$lastFeatureInsertId.'|Product'); // Custom Values
		$this->paypal_class->add_field('item_name', $item_name); // Product Name
		$this->paypal_class->add_field('user_id', $loginUserId);
		$this->paypal_class->add_field('quantity', $quantity); // Quantity
		$this->paypal_class->add_field('amount', $totalAmount); // Price
		$this->paypal_class->submit_paypal_post();
	}
	
	
	/*********************************************Adaptive Payment****************************************************/
	public function PaymentAdaptive(){
		
		//echo "<pre>"; print_r($this->input->post());die;
		
	//require_once  ("./application/libraries/paypal/Paypal_adaptive.php");
		$excludeArr = array('paypalmode','paypalEmail','total_price','PaypalSubmit');
		$dataArr = array();
		$condition =array('id' => $this->checkLogin('U'));
		$this->checkout_model->commonInsertUpdate(USERS,'update',$excludeArr,$dataArr,$condition);
		$loginUserId = $this->checkLogin('U');
		$lastFeatureInsertId = $this->session->userdata('randomNo');
		$Query = 'select sc.*,u.id as uid,u.commision as ucom,
		u.paypal_email as uemail from '.SHOPPING_CART.' as sc left join '.USERS.' as u
		on (u.id = sc.sell_id) where sc.user_id = '.$this->checkLogin('U').'';
		$get_details = $this->checkout_model->ExecuteQuery($Query);
		//echo("<pre>");print_r($get_details->result());die;
		$Details = array();
		$adminCommision = 0;
		if($get_details->num_rows() >0){
			foreach($get_details->result() as $_Details){
				$admin_commision = (($_Details->total - $_Details->discountAmount) * ($_Details->ucom/100));
	
				$adminCommision += $admin_commision;
	
				$ucom = ($_Details->total - $admin_commision - $_Details->discountAmount);
	
			}
			if(isset($Details[$_Details->uid]['amount'])){
					
				$Details[$_Details->uid]['amount'] += $ucom;
			}else{
	
				$Details[$_Details->uid]['amount'] = $ucom;
				$Details[$_Details->uid]['email'] = $_Details->uemail;
					
			}
		}
		
		$SuccessURL = 'order/success/'.$loginUserId.'/'.$lastFeatureInsertId.'/adaptive';
		$CancelURL = 'order/failure/'.$loginUserId.'/'.$lastFeatureInsertId.'/adaptive';
		$curreny_type = 'USD';
		$PayRequestFields = array(
				'ActionType' => 'PAY',                                  // Required.  Whether the request pays the receiver or whether the request is set up to create a payment request, but not fulfill the payment until the ExecutePayment is called.  Values are:  PAY, CREATE, PAY_PRIMARY
				'CancelURL' => site_url($CancelURL), // Required.  The URL to which the sender's browser is redirected if the sender cancels the approval for the payment after logging in to paypal.com.  1024 char max.
				'CurrencyCode' => $curreny_type,                                // Required.  3 character currency code.
				'FeesPayer' => 'EACHRECEIVER', 	                                // The payer of the fees.  Values are:  SENDER, PRIMARYRECEIVER, EACHRECEIVER, SECONDARYONLY
				'IPNNotificationURL' => '',                                     // The URL to which you want all IPN messages for this payment to be sent.  1024 char max.
				'Memo' => '', 				                                    // A note associated with the payment (text, not HTML).  1000 char max
				'Pin' => '', 	                                                // The sener's personal id number, which was specified when the sender signed up for the preapproval
				'PreapprovalKey' => '',                                         // The key associated with a preapproval for this payment.  The preapproval is required if this is a preapproved payment.
				'ReturnURL' => site_url($SuccessURL),                             // Required.  The URL to which the sener's browser is redirected after approvaing a payment on paypal.com.  1024 char max.
				'ReverseAllParallelPaymentsOnError' => '',                      // Whether to reverse paralel payments if an error occurs with a payment.  Values are:  TRUE, FALSE
				'SenderEmail' => '', 	                                        // Sender's email address.  127 char max.
				'TrackingID' => ''		                                        // Unique ID that you specify to track the payment.  127 char max.
		);
	
		$ClientDetailsFields = array(
				'CustomerID' => '', 	                                        // Your ID for the sender  127 char max.
				'CustomerType' => '', 	                                        // Your ID of the type of customer.  127 char max.
				'GeoLocation' => '',                                            // Sender's geographic location
				'Model' => '', 	                                                // A sub-identification of the application.  127 char max.
				'PartnerName' => ''		                                        // Your organization's name or ID
		);
	
		$FundingTypes = array('ECHECK', 'BALANCE', 'CREDITCARD');
		$Receivers = array();
		$Receiver = array(
				'Amount' => $this->input->post('total_price'),                                            // Required.  Amount to be paid to the receiver.
				'Email' =>  $this->input->post('paypalEmail'), 	                                                // Receiver's email address. 127 char max.
				'InvoiceID' => '', 					                            // The invoice number for the payment.  127 char max.
				'PaymentType' => 'SERVICE', 		                            // $this->input->post('paypalEmail')Transaction type.  Values are:  GOODS, SERVICE, PERSONAL, CASHADVANCE, DIGITALGOODS
				'PaymentSubType' => '', 			                            // The transaction subtype for the payment.
				'Phone' => array('CountryCode' => '', 'PhoneNumber' => '', 'Extension' => ''), // Receiver's phone number.   Numbers only.
				'Primary' => 'TRUE'	                                            // Whether this receiver is the primary receiver.  Values are boolean:  TRUE, FALSE
		);
		
		array_push($Receivers,$Receiver);
		//echo("<pre>");print_r($Details);die;
		foreach($Details as $_Details){
			$Receiver = array(
					'Amount' => $_Details['amount'],                            // Required.  Amount to be paid to the receiver.
					'Email' => $_Details['email'], 	                            // Receiver's email address. 127 char max.
					'InvoiceID' => '', 	                                        // The invoice number for the payment.  127 char max.
					'PaymentType' => 'SERVICE',                                 // Transaction type.  Values are:  GOODS, SERVICE, PERSONAL, CASHADVANCE, DIGITALGOODS
					'PaymentSubType' => '',                                     // The transaction subtype for the payment.
					'Phone' => array('CountryCode' => '', 'PhoneNumber' => '', 'Extension' => ''), // Receiver's phone number.   Numbers only.
					'Primary' => 'FALSE'                                        // Whether this receiver is the primary receiver.  Values are boolean:  TRUE, FALSE
			);
				
			array_push($Receivers,$Receiver);
				
		}
		$SenderIdentifierFields = array(
				'UseCredentials' => ''						                    // If TRUE, use credentials to identify the sender.  Default is false.
		);
		$AccountIdentifierFields = array(
				'Email' => '', 								                    // Sender's email address.  127 char max.
				'Phone' => array('CountryCode' => '', 'PhoneNumber' => '', 'Extension' => '')								// Sender's phone number.  Numbers only.
		);
		
		$PayPalRequestData = array(
				'PayRequestFields' => $PayRequestFields,
				'ClientDetailsFields' => $ClientDetailsFields,
				// 			'FundingTypes' => $FundingTypes,
				'Receivers' => $Receivers,
				'SenderIdentifierFields' => $SenderIdentifierFields,
				'AccountIdentifierFields' => $AccountIdentifierFields
		);
		
		//$transaction = new PayPal_Adaptive;
		//echo "<pre>";print_r($PayPalRequestData);die;
		$PayPalResult = $this->paypal_adaptive->Pay($PayPalRequestData);
		
		//echo "<pre>";print_r($PayPalResult);die;
	
		if(!$this->paypal_adaptive->APICallSuccessful($PayPalResult['Ack'])){
	
			//echo "<pre>"; print_r($PayPalResult['Errors']); die;
			redirect('order/failure/'.$loginUserId.'/'.$lastFeatureInsertId.'/adaptive/'.urlencode($PayPalResult['Errors'][0]['Message']));
		}
		else{
		
			$excludeArr =array('seller_id','product_type','paypalmode','paypalEmail','full_name','address','address2','city','state','country','postal_code','phone_no','total_price','email','PaypalSubmit');
			$trans_id = $PayPalResult['CorrelationID'];
			$paykey = $PayPalResult['PayKey'];
			$dataArr = array(
					'transaction_id' => $trans_id,
					'pay_key' => $paykey,
					'user_id' => $loginUserId,
					'dealCodeNumber' => $lastFeatureInsertId,
					'status' => "Pending"
			);
			$condition = array();
			$this->checkout_model->commonInsertUpdate(ADAPTIVE_PAYMENTS,'insert',$excludeArr,$dataArr,$condition);
			header('Location: '.$PayPalResult['RedirectURL']);
			exit();
		}
	}
	
	/*************************************Twocheckout Payment***************************************************************/
	public function twocheckout_payment(){
	//echo("<pre>");print_r($this->input->post());die;
		$excludeArr = array('creditvalue','card_name','seller_id','publishableKey','mode','shipping_id','email','cardNumber','cardType','CCExpDay','CCExpMnth','creditCardIdentifier','total_price','CreditSubmit','token');
		$dataArr = array();
		$condition =array('id' => $this->checkLogin('U'));
		$this->checkout_model->commonInsertUpdate(USERS,'update',$excludeArr,$dataArr,$condition);
		$this->load->library('Twocheckout');
	$paypal_settings = unserialize($this->config->item('payment_5'));
		$settings=unserialize($paypal_settings['settings']);
		Twocheckout::privateKey($settings['privateKey']);
		Twocheckout::sellerId($settings['sellerId']);
		 Twocheckout::sandbox(true);  #Uncomment to use Sandbox
		$tokenid=$this->input->post('token');
		//echo $tokenid;die;
		$loginUserId = $this->checkLogin('U');
		$lastFeatureInsertId = $this->session->userdata('randomNo');
			
		//$item_name = $this->config->item('email_title').' Products';
			
		$totalAmount = $this->input->post('total_price');
			
		$quantity = 1;
		
		
		
		try {
			$charge = Twocheckout_Charge::auth(array(
					"merchantOrderId" => "123",
					"token" => $tokenid,
					"currency" => 'USD',
					"total" => $totalAmount,
					"billingAddr" => array(
							"name" => 'Testing Tester',
							"addrLine1" => '123 Test St',
							"city" => 'Columbus',
							"state" => 'OH',
							"zipCode" => '43123',
							"country" => 'USA',
							"email" => 'testingtester@2co.com',
							"phoneNumber" => '555-555-5555'
					),
					"shippingAddr" => array(
							"name" => $this->input->post('full_name'),
							"addrLine1" => $this->input->post('address'),
							"city" => $this->input->post('city'),
							"state" =>$this->input->post('state'),
							"zipCode" => $this->input->post('postal_code'),
							"country" => $this->input->post('country'),
							"email" => $this->input->post('email'),
							"phoneNumber" => $this->input->post('phone_no')
					)
			), 'array');
			
			//echo("<pre>");print_r($charge);die;
			
			$result=(array)json_decode($charge);
			
			$rescod=$result['response'];
			if ($rescod->responseCode == 'APPROVED') {
				
				redirect('order/success/'.$loginUserId.'/'.$lastFeatureInsertId);
			}else{							
							$rescod=$result[exception]->errorMsg;
						
							redirect('order/failure/'.str_replace("-"," ",url_title($rescod)));
						}
		} catch (Twocheckout_Error $e) {
			$e->getMessage();
		}
	}
        /**
         * Seller subscription methods
         */
	public function PaymentProcessStorefront(){
		$excludeArr = array('paypalmode','paypalEmail','total_price','PaypalSubmit','email','invoiceNumber','is_coupon','discount_Amt','CouponCode','Coupon_id','couponType','plan_value');
		$dataArr = array();
		$condition =array('id' => $this->checkLogin('U'));
		$this->checkout_model->commonInsertUpdate(USERS,'update',$excludeArr,$dataArr,$condition);
		$this->data['totalAmount'] = $this->input->post('total_price');
		if($this->data['totalAmount'] == $this->config->item('storefront_fees_year')){ $this->data['plan_type'] = 'Y';}else{$this->data['plan_type'] = 'M';}
		$this->data['paypalEmail'] = $this->input->post('paypalEmail');
		//DealCodeNumber
		$this->data['lastFeatureInsertId'] = $this->input->post('invoiceNumber');
			
		if($this->input->post('paypalmode') == 'sandbox'){
			$this->data['paypal_url'] = 'https://www.sandbox.paypal.com/cgi-bin/webscr';   // testing paypal url
		}else{
			$this->data['paypal_url'] = 'https://www.paypal.com/cgi-bin/webscr';     // paypal url
		}
		
		$this->checkout_model->commonDelete(STORE_ARB,array('invoice_no'=>$this->session->userdata('InvoiceNo')));
		$dataARB = array(
			'user_id'=>$this->checkLogin('U'),
			'transaction_id'=>$this->session->userdata('InvoiceNo'),
			'invoice_no'=>$this->session->userdata('InvoiceNo'),
			'status'=>'Pending',
			'discount'=>$this->input->post('discount_Amt'),
			'couponcode'=>$this->input->post('CouponCode'),
			'couponid'=>$this->input->post('Coupon_id'),
			'coupontype'=>$this->input->post('couponType'),
			'amount'=>$this->data['totalAmount'],
			'payment_type'=>'Paypal'
		);
		$this->checkout_model->simple_insert(STORE_ARB,$dataARB);

		$key = 'team-fancyy-clone-tweaks';
		$this->session->set_userdata('cde',$this->encrypt->encode($this->session->userdata('InvoiceNo'), $key));
		$this->session->set_userdata('cde_user',$this->encrypt->encode($this->checkLogin('U'), $key));
		$this->load->view('site/store/paypal_subscription',$this->data);
	} /* Stripe integration */
	public function stripe(){
		//echo("<pre>");print_r($this->input->post());die;
		$loginUserId = $this->checkLogin('U');
		$lastFeatureInsertId = $this->session->userdata('randomNo');
		//echo $lastFeatureInsertId;die;
		$excludeArr = array('creditvalue','card_name','shipping_id','email','cardNumber','cardType','CCExpDay','CCExpMnth','creditCardIdentifier','exp_year','cvc_number1','total_price','CreditSubmit');
		$dataArr = array();
		$condition =array('id' => $this->checkLogin('U'));
		$this->checkout_model->commonInsertUpdate(USERS,'update',$excludeArr,$dataArr,$condition);
		$this->load->library('Stripe');
		$success = "";
		$error = "";
		$totalAmount = $this->input->post('total_price');
		$crdno=$this->input->post('cardNumber');
		$crdname=$this->input->post('cardType');
		$year=$this->input->post('CCExpMnth');
		$mnth=$this->input->post('CCExpDay');
		$cvv=$this->input->post('creditCardIdentifier');
		$crd=array('number'=>$crdno,'exp_month'=>$mnth,'exp_year'=>$year,'cvc'=>$cvv,'name'=>$crdname);
		$item_name = $this->config->item('email_title').' Products';
		if ($_POST) {
	//print_r($crd);die;
			$success = Stripe::charge_card($totalAmount,$crd);
			//echo "<pre>";print_r($result);die;
			if(isset($success))
			{
				$result_success = (array)json_decode($success);
				
					
				if($result_success['status']=="succeeded"){
					$transaction_idstripe=$result_success['id'];
					$statustrans['mgs']=$result_success['status'];
					redirect('order/success/'.$loginUserId.'/'.$lastFeatureInsertId.'/'.$transaction_idstripe);
				}
				else
				{
					$err=(array)$result_success['error'];
					$statustrans['msg']=$err['message'];
					redirect('order/failure/'.$statustrans['msg']);
				}
					
			}
		}
	
	}
	public function PaymentCreditStorefront(){
		$excludeArr = array('email','cardNumber','CCExpDay','CCExpMnth','creditCardIdentifier','total_price','cardType','invoiceNumber','last_name');
		$dataArr = array();
		$condition =array('id' => $this->checkLogin('U'));
		$this->checkout_model->commonInsertUpdate(USERS,'update',$excludeArr,$dataArr,$condition);
		$this->load->library('authorize_arb');
		$this->authorize_arb->startData('create');
		$refId = substr(md5( microtime() . 'ref' ), 0, 20);
		$this->authorize_arb->addData('refId', $refId);
		$loginUserId = $this->checkLogin('U');
		$subscription_data = array(
			'name' => $this->config->item('email_title').' Subscription',
			'paymentSchedule' => array(
				'interval' => array(
					'length' => 1,
					'unit' => 'months',
		),
				'startDate' => date('Y-m-d'),
				'totalOccurrences' => 9999,
				'trialOccurrences' => 0,
		),
			'amount' => $this->input->post('total_price'),
			'trialAmount' => 0.00,
			'payment' => array(
				'creditCard' => array(
					'cardNumber' => $this->input->post('cardNumber'),
					'expirationDate' => $this->input->post('CCExpMnth').'-'.$this->input->post('CCExpDay'),
					'cardCode' => $this->input->post('creditCardIdentifier'),
		),
		),
			'order' => array(
				'invoiceNumber' => $this->input->post('invoiceNumber'),
				'description' =>  $this->config->item('email_title').' Subscription',
		),
			'customer' => array(
				'id' => $this->checkLogin('U'),
				'email' => $this->input->post('email'),
				'phoneNumber' => $this->input->post('phone_no'),
		),
			'billTo' => array(
				'firstName' => $this->input->post('full_name'),
				'lastName' => $this->input->post('last_name'),
				'address' => $this->input->post('address').''.$this->input->post('address2'),
				'city' => $this->input->post('city'),
				'state' => $this->input->post('state'),
				'zip' => $this->input->post('postal_code'),
				'country' => $this->input->post('country'),
		),
		);
		$this->authorize_arb->addData('subscription', $subscription_data);
		if($this->authorize_arb->send()){
			$newsid='30';
			$template_values=$this->checkout_model->get_newsletter_template_details($newsid);
			$email = $this->input->post('email');
			$invoice_no = $this->input->post('invoiceNumber'); 
			$transaction = $this->authorize_arb->getId();
			$email_message = "Payment Success. Please update your store setting";
			$subject = $template_values['news_subject'];
			$adminnewstemplateArr=array('email_title'=> $this->config->item('email_title'),'logo'=> $this->data['logo']);
			extract($adminnewstemplateArr);
			//$ddd =htmlentities($template_values['news_descrip'],null,'UTF-8');
			$header .="Content-Type: text/plain; charset=ISO-8859-1\r\n";
			$message .= '<!DOCTYPE HTML>
				<html>
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
				<meta name="viewport" content="width=device-width"/><body>';
			include('./newsletter/registeration'.$newsid.'.php');
			$message .= '</body>
				</html>';
			if($template_values['sender_name']=='' && $template_values['sender_email']==''){
				$sender_email=$this->data['siteContactMail'];
				$sender_name=$this->data['siteTitle'];
			}else{
				$sender_name=$template_values['sender_name'];
				$sender_email=$template_values['sender_email'];
			}
			$email_values = array('mail_type'=>'html',
				'from_mail_id'=>$sender_email,
				'mail_name'=>$sender_name,
				'to_mail_id'=>$email,
				'cc_mail_id'=>$this->config->item('site_contact_mail'),
				'subject_message'=>$template_values['news_subject'],
				'body_messages'=>$message,
				'mail_id'=>'register mail'
				);
			$email_send_to_common = $this->product_model->common_email_send($email_values);
			$dataArr = array(
				'store_payment'=>'Paid'
//				'ref_comm_purchaser' => $this->config->item('ref_comm_purchaser_ug'),
//				'ref_comm_seller' => $this->config->item('ref_comm_seller_ug'),
//				'sell_commission' => $this->config->item('sell_commission_ug')
			);
			$condition = array('id'=>$this->checkLogin('U'));
			$this->checkout_model->update_details(USERS,$dataArr,$condition);
			$dataARB = array(
			'user_id'=>$this->checkLogin('U'),
			'transaction_id'=>$this->authorize_arb->getId(),
			'invoice_no'=>$this->session->userdata('InvoiceNo'),
			'status'=>'Paid',
			'payment_type'=>'Credit Card');
			$this->checkout_model->simple_insert(STORE_ARB,$dataARB);
			$this->checkout_model->simple_insert(STORE_FRONT,array('user_id'=>$this->checkLogin('U'),'store_name'=>$this->data['userDetails']->row()->brand_name));
			$this->session->unset_userdata('InvoiceNo');
			$this->data['Confirmation'] = 'subscription_success';
			$this->data['errors'] = 'Payment Success';
			$this->load->view('site/order/order.php',$this->data);
		}else{
			$this->data['Confirmation'] = 'subscription_fail';
			$this->data['errors'] = 'Payment Failed';
			$this->load->view('site/order/order.php',$this->data);
		}
	}  	

}

/* End of file checkout.php */
/* Location: ./application/controllers/site/checkout.php */