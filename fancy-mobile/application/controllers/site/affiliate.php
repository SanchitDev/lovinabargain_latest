<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * Landing page functions
 * @author Teamtweaks
 *
 */

class Affiliate extends MY_Controller {
	function __construct(){
		parent::__construct(); 
		$this->load->helper(array('cookie','date','form','email'));
		$this->load->library(array('encrypt','form_validation'));
		$this->load->model('affiliate_model');

		if($_SESSION['sMainCategories'] == ''){
			$sortArr1 = array('field'=>'cat_position','type'=>'asc');
			$sortArr = array($sortArr1);
			$_SESSION['sMainCategories'] = $this->affiliate_model->get_all_details(CATEGORY,array('rootID'=>'0','status'=>'Active'),$sortArr);
		}
		$this->data['mainCategories'] = $_SESSION['sMainCategories'];

		if($_SESSION['sColorLists'] == ''){
			$_SESSION['sColorLists'] = $this->affiliate_model->get_all_details(LIST_VALUES,array('list_id'=>'1'));
		}
		$this->data['mainColorLists'] = $_SESSION['sColorLists'];

		$this->data['loginCheck'] = $this->checkLogin('U');
		//		echo $this->session->userdata('fc_session_user_id');die;
		$this->data['likedProducts'] = array();
		if ($this->data['loginCheck'] != ''){
			$this->data['likedProducts'] = $this->affiliate_model->get_all_details(PRODUCT_LIKES,array('user_id'=>$this->checkLogin('U')));
		}
			
	}

	/**
	 *
	 *
	 */
	public function index(){
		$this->data['heading'] = 'Affiliate Details';
		$this->data['getUserDetails'] = $this->affiliate_model->get_all_details(USERS,array('id'=>$this->checkLogin('U')));
		$this->data['getReferalList'] = $this->affiliate_model->get_affiliate_list($this->checkLogin('U'));
		$this->load->view('site/affiliate/affiliate_list',$this->data);
	}
	public function affiliate_order(){
		$this->data['heading'] = 'Affiliate Orders';
		$this->data['getReferalList'] = $this->affiliate_model->get_affiliate_list($this->checkLogin('U'));
		$this->load->view('site/affiliate/affiliate_list',$this->data);
	}
}

/* End of file affiliate.php */
/* Location: ./application/controllers/site/affiliate.php */