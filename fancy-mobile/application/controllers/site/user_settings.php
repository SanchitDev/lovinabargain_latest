<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * User Settings related functions
 * @author Teamtweaks
 *
 */

class User_settings extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper(array('cookie','date','form','email'));
		$this->load->library(array('encrypt','form_validation','pagination'));
		$this->load->model('user_model');
		$this->load->model('commission_model','commission');

		if($_SESSION['sMainCategories'] == ''){
			$sortArr1 = array('field'=>'cat_position','type'=>'asc');
			$sortArr = array($sortArr1);
			$_SESSION['sMainCategories'] = $this->user_model->get_all_details(CATEGORY,array('rootID'=>'0','status'=>'Active'),$sortArr);
		}
		$this->data['mainCategories'] = $_SESSION['sMainCategories'];

		if($_SESSION['sColorLists'] == ''){
			$_SESSION['sColorLists'] = $this->user_model->get_all_details(LIST_VALUES,array('list_id'=>'1'));
		}
		$this->data['mainColorLists'] = $_SESSION['sColorLists'];

		$this->data['loginCheck'] = $this->checkLogin('U');
	}

	public function index(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$this->data['heading'] = 'Settings';
			$this->load->view('site/user/settings',$this->data);
		}
	}
	public function shop_settings() {
		
        if ($this->checkLogin('U') == '') {
            redirect('login');
        } else {
            $this->data['storeDetails'] = $this->user_model->get_all_details(STORE_FRONT, array('user_id' => $this->checkLogin('U')));
            $this->load->view('site/store/shop_settings', $this->data);
        }
    }
	public function account_settings(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$this->data['plan'] = $this->user_model->get_all_details(SELLER_PLAN, array('status'=> 'Active'));
			$this->data['heading'] = 'Settings';
			$this->load->view('site/user/account_settings',$this->data);
		}
	}
	public function update_profile(){
		$inputArr = array();
		$response['success'] = '0';
		if ($this->checkLogin('U') == ''){
			$response['msg'] = 'You must login';
		}else {
			$update = '0';
			$email = $this->input->post('email');
			if ($email!=''){
				if (valid_email($email)){
					$condition = array('email'=>$email,'id !='=>$this->checkLogin('U'));
					$duplicateMail = $this->user_model->get_all_details(USERS,$condition);
					if ($duplicateMail->num_rows()>0){
						$response['msg'] = 'Email already exists';
					}else {
						$inputArr['email'] = $email;
						$update = '1';
					}
				}else {
					$response['msg'] = 'Invalid email';
				}
			}else {
				$update = '1';
			}
			if ($update == '1'){
				$excludeArr = array('b_year','b_month','b_day','email');
				$inputArr['birthday'] = $birthday;
				$condition = array('id'=>$this->checkLogin('U'));
				$this->user_model->commonInsertUpdate(USERS,'update',$excludeArr,$inputArr,$condition);
				$config['overwrite'] = FALSE;
				$config['remove_spaces'] = TRUE;
				$config['allowed_types'] = 'jpg|jpeg|gif|png';
				$config['max_size'] = 2000;
				$config['max_width']  = '600';
				$config['max_height']  = '600';
				$config['upload_path'] = '../images/users';
				$this->load->library('upload', $config);
				if($this->upload->do_upload('upload-file')){
					$imgDetails = $this->upload->data();
					$dataArr['thumbnail'] = $imgDetails['file_name'];
					$condition = array('id'=>$this->checkLogin('U'));
					$this->user_model->update_details(USERS,$dataArr,$condition);
					redirect('image-crop/'.$this->checkLogin('U'));
				}else {
				$this->setErrorMessage('error',strip_tags($this->upload->display_errors()));
			}
				redirect(base_url().'settings/account');
			}
		}
	}

	public function changePhoto(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$config['overwrite'] = FALSE;
			$config['remove_spaces'] = TRUE;
			$config['allowed_types'] = 'jpg|jpeg|gif|png';
			$config['max_size'] = 2000;
			$config['max_width']  = '600';
			$config['max_height']  = '600';
			$config['upload_path'] = '../images/users';
			$this->load->library('upload', $config);
			if ( $this->upload->do_upload('upload-file')){
				$imgDetails = $this->upload->data();
				$dataArr['thumbnail'] = $imgDetails['file_name'];
				$condition = array('id'=>$this->checkLogin('U'));
				$this->user_model->update_details(USERS,$dataArr,$condition);
				redirect('image-crop/'.$this->checkLogin('U'));
			}else {
				$this->setErrorMessage('error',strip_tags($this->upload->display_errors()));
			}
			redirect(base_url().'settings');
		}
	}

	public function delete_user_photo(){
		$response['success'] = '0';
		if ($this->checkLogin('U')==''){
			$response['msg'] = 'You must login';
		}else {
			$condition = array('id'=>$this->checkLogin('U'));
			$dataArr = array('thumbnail'=>'');
			$this->user_model->update_details(USERS,$dataArr,$condition);
			if($this->lang->line('prof_photo_del') != '')
			$lg_err_msg = $this->lang->line('prof_photo_del');
			else
			$lg_err_msg = 'Profile photo deleted successfully';
			$this->setErrorMessage('success',$lg_err_msg);
			$response['success'] = '1';
		}
		echo json_encode($response);
	}

	public function delete_user_account(){
		if ($this->checkLogin('U')!=''){
			$datestring = "%Y-%m-%d %h:%i:%s";
			$time = time();
			$newdata = array(
	               'last_logout_date' => mdate($datestring,$time),
				   'status'=>'Inactive'
				   );
				   $condition = array('id' => $this->checkLogin('U'));
				   $this->user_model->update_details(USERS,$newdata,$condition);
				   $userdata = array(
							'fc_session_user_id'=>'',
							'session_user_name'=>'',
							'session_user_email'=>'',
							'fc_session_temp_id'=>''
							);
							$this->session->set_userdata($userdata);
							if($this->lang->line('prof_inact_succ') != '')
							$lg_err_msg = $this->lang->line('prof_inact_succ');
							else
							$lg_err_msg = 'Your account inactivated successfully';
							$this->setErrorMessage('success',$lg_err_msg);
		}
	}

	public function password_settings(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$this->data['heading'] = 'Password Settings';
			$this->load->view('site/user/changepassword',$this->data);
		}
	}

	public function change_user_password(){
	//echo "<pre>"; print_r($this->input->post());die;
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$pwd = $this->input->post('pass');
			$cfmpwd = $this->input->post('confirmpass');
			
			if ($pwd != '' && $cfmpwd != '' && strlen($pwd) > 5){
				if ($pwd == $cfmpwd){
					$dataArr = array('password'=>md5($pwd));
					$condition = array('id'=>$this->checkLogin('U'));
					$this->user_model->update_details(USERS,$dataArr,$condition);
					if($this->lang->line('pwd_cge_succ') != '')
					$lg_err_msg = $this->lang->line('pwd_cge_succ');
					else
					$lg_err_msg = 'Password changed successfully';
					$this->setErrorMessage('success',$lg_err_msg);
				}else {
					if($this->lang->line('pwd_donot_match') != '')
					$lg_err_msg = $this->lang->line('pwd_donot_match');
					else
					$lg_err_msg = 'Passwords does not match';
					$this->setErrorMessage('error',$lg_err_msg);
				}
			}else {
				if($this->lang->line('pwd_cfm_not_match') != '')
				$lg_err_msg = $this->lang->line('pwd_cfm_not_match');
				else
				$lg_err_msg = 'Password and Confirm password fields required';
				$this->setErrorMessage('error',$lg_err_msg);
			}
			redirect(base_url().'settings/password');
		}
	}

	public function preferences_settings(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$this->data['heading'] = 'Preference Settings';
			$this->data['languages'] = $this->user_model->get_all_details(LANGUAGES,array());
			$this->load->view('site/user/change_preferences',$this->data);
		}
	}

	public function update_preferences(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$this->user_model->commonInsertUpdate(USERS,'update',array(),array(),array('id'=>$this->checkLogin('U')));
			if($this->lang->line('pref_sav_succ') != '')
			$lg_err_msg = $this->lang->line('pref_sav_succ');
			else
			$lg_err_msg = 'Preferences saved successfully';
			$this->setErrorMessage('success',$lg_err_msg);
			redirect(base_url().'settings/preferences');
		}
	}

	public function notifications_settings(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$this->data['heading'] = 'Notifications Settings';
			$this->data['languages'] = $this->user_model->get_all_details(LANGUAGES,array());
			$this->load->view('site/user/change_notifications',$this->data);
		}
	}

	public function update_notifications(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$emailArr = $this->data['emailArr'];
			$notyArr = $this->data['notyArr'];
			$emailStr = '';
			$notyStr = '';
			foreach ($this->input->post() as $key=>$val){
				if (in_array($key, $emailArr)){
					$emailStr .= $key.',';
				}else if (in_array($key, $notyArr)){
					$notyStr .= $key.',';
				}
			}
			$updates = $this->input->post('updates');
			$updates = ($updates == '')?'0':'1';
			$emailStr = substr($emailStr, 0,strlen($emailStr)-1);
			$notyStr = substr($notyStr, 0,strlen($notyStr)-1);
			$dataArr = array(
	    		'email_notifications'	=>	$emailStr,
	    		'notifications'			=>	$notyStr,
	    		'updates'				=>	$updates
			);
			$condition = array('id'=>$this->checkLogin('U'));
			$this->user_model->update_details(USERS,$dataArr,$condition);
			if ($updates == 1){
				$checkEmail = $this->user_model->get_all_details(SUBSCRIBERS_LIST,array('subscrip_mail'=>$this->data['userDetails']->row()->email));
				if ($checkEmail->num_rows()==0){
					$this->user_model->simple_insert(SUBSCRIBERS_LIST,array('subscrip_mail'=>$this->data['userDetails']->row()->email,'active'=>1,'status'=>'Active'));
				}
			}else {
				$this->user_model->commonDelete(SUBSCRIBERS_LIST,array('subscrip_mail'=>$this->data['userDetails']->row()->email));
			}
			if($this->lang->line('noty_sav_succ') != '')
			$lg_err_msg = $this->lang->line('noty_sav_succ');
			else
			$lg_err_msg = 'Notifications settings saved successfully';
			$this->setErrorMessage('success',$lg_err_msg);
			redirect(base_url().'settings/notifications');
		}
	}

	public function user_purchases(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$this->data['heading'] = 'Purchases';
			$this->data['purchasesList'] = $this->user_model->get_purchase_details($this->checkLogin('U'));
			$this->load->view('site/user/user_purchases',$this->data);
		}
	}
	
	public function update_buyer_status(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$user_id = $this->uri->segment(2,0);
			//echo $user_id;
			$seller_id = $this->uri->segment(3,0);
			//echo $seller_id; die;
			$invoice_id = $this->uri->segment(4,0);
			$Query = "select p.*,u.commision,u.cash_on_delivery from ".PAYMENT." as p left join ".USERS." as u on u.id=p.sell_id where p.dealCodeNumber = '".$invoice_id."'";
			$seller_list = $this->user_model->ExecuteQuery($Query);
			//echo $this->db->last_query();
			//echo "<pre>"; print_r($seller_list->result());
			if($seller_list->num_rows()>0){
					
				$admin_commision = ($seller_list->row()->total * ($seller_list->row()->commision/100));
				//echo "$admin_commision"; echo "<br>";
				//echo $seller_list->row()->commision; echo "<br>";
				$cod = $seller_list->row()->cash_on_delivery + $admin_commision;
				//echo $cod; die;
				$this->user_model->update_details(USERS,array('cash_on_delivery'=>$cod),array('id'=>$seller_id));
			}
			$this->user_model->update_details(PAYMENT,array('status'=>'Paid'),array('user_id'=>$user_id,'sell_id'=>$seller_id,'dealCodeNumber'=>$invoice_id));
			redirect(base_url().'orders');
		}
	}
 
	public function user_orders(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$this->data['heading'] = 'Orders';
			$this->data['ordersList'] = $this->user_model->get_user_orders_list($this->checkLogin('U'));
			$this->load->view('site/user/user_orders_list',$this->data);
		}
	}

	public function manage_fancyybox(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$this->data['heading'] = 'Subscriptions';
			$this->data['subscribeList'] = $this->user_model->get_subscriptions_list($this->checkLogin('U'));
			$this->load->view('site/user/manage_fancyybox',$this->data);
		}
	}

	public function shipping_settings(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$this->data['heading'] = 'Shipping Address';
			$this->data['countryList'] = $this->user_model->get_all_details(COUNTRY_LIST,array(),array(array('field'=>'name','type'=>'asc')));
			$this->data['shippingList'] = $this->user_model->get_all_details(SHIPPING_ADDRESS,array('user_id'=>$this->checkLogin('U')));
			$this->load->view('site/user/shipping_settings',$this->data);
		}
	}
	
	
    
	public function payment_method(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$this->data['heading'] = 'Payment Methods';
			$this->data['countryList'] = $this->user_model->get_all_details(COUNTRY_LIST,array(),array(array('field'=>'name','type'=>'asc')));
			$this->data['paymentList'] = $this->user_model->get_all_details(USER_PAYMENT_DETAILS,array('user_id'=>$this->checkLogin('U')));
			$this->load->view('site/user/payment_settings',$this->data);
		}
	}
	public function insertEdit_payment_method(){
	//echo "<pre>"; print_r($this->input->post());die;
	   if ($this->checkLogin('U')==''){
	       redirect(base_url().'login');
	   }else{
	      if($this->input->post('row_id') !=''){
		     $excludeArr = array('user_id','row_id','card_no','card_cvv');
			  $key = 'team-clone-tweaks';
			  $card_no = $this->encrypt->encode($this->input->post('card_no'), $key);
			  $card_cvv = $this->encrypt->encode($this->input->post('card_cvv'), $key);
			 $dataArr = array('card_no'=>$card_no,'card_cvv'=>$card_cvv);
			 $condition =array('user_id'=>$this->checkLogin('U'),'id'=>$this->input->post('row_id'));
			  $this->user_model->commonInsertUpdate(USER_PAYMENT_DETAILS,'update',$excludeArr,$dataArr,$condition);
			    if($this->lang->line('payment_add_succ') != '')
				  $lg_err_msg = $this->lang->line('payment_add_succ');
				else
				  $lg_err_msg = 'Your payment Method is updated successfully !';
				  $this->setErrorMessage('success',$lg_err_msg);
		  }else{
	       $payment_details = $this->user_model->get_all_details(USER_PAYMENT_DETAILS,array('user_id'=>$this->checkLogin('U'),'card_no'=>$this->input->post('card_no'),'card_cvv'=>$this->input->post('card_cvv')));
		   if($payment_details->num_rows() ==1){
		       if($this->lang->line('payment_alredy_reg') != '')
				  $lg_err_msg = $this->lang->line('payment_alredy_reg');
				else
				  echo $lg_err_msg = 'This User Payment Details Already Registered';
				   $this->setErrorMessage('error',$lg_err_msg);
		   }else{
		      $excludeArr = array('user_id','row_id','card_no','card_cvv');
			   $key = 'team-clone-tweaks';
			  $card_no = $this->encrypt->encode($this->input->post('card_no'), $key);
			  $card_cvv = $this->encrypt->encode($this->input->post('card_cvv'), $key);
			  $dataArr = array('user_id'=>$this->checkLogin('U'),'card_no'=>$card_no,'card_cvv'=>$card_cvv);
			  $condition ='';
			  $this->user_model->commonInsertUpdate(USER_PAYMENT_DETAILS,'insert',$excludeArr,$dataArr,$condition);
			    if($this->lang->line('payment_add_succ') != '')
				  $lg_err_msg = $this->lang->line('payment_add_succ');
				else
				  $lg_err_msg = 'Your payment Method is added successfully !';
				  $this->setErrorMessage('success',$lg_err_msg); 	  
		   }
		   }
		   $this->data['heading'] = 'Payment Methods';
		   redirect('settings/payment-method');
	   }
	}
	public function get_upayment_details(){
		$payment_id = $this->uri->segment('2');
		$this->data['countryList'] = $this->user_model->get_all_details(COUNTRY_LIST,array(),array(array('field'=>'name','type'=>'asc')));
		$this->data['paymentDetails'] = $paymentDetails = $this->user_model->get_all_details(USER_PAYMENT_DETAILS,array('id'=>$payment_id));
		//print_r($paymentDetails->result());die;
		$this->load->view('site/user/payment_settings_edit',$this->data);
		
	}
	public function remove_payment_method(){
		$returnStr['status_code'] = 0;
		if ($this->checkLogin('U')==''){
			if($this->lang->line('u_must_login') != '')
			$returnStr['message'] = $this->lang->line('u_must_login');
			else
			$returnStr['message'] = 'You must login';
		}else {
			$payment_id = $this->input->post('id');
			$this->user_model->commonDelete(USER_PAYMENT_DETAILS,array('id'=>$payment_id));
			$returnStr['status_code'] = 1;
		}
		echo json_encode($returnStr);
	}
	public function insertEdit_shipping_address(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
		
		//	echo "<pre>"; print_r($this->input->post());die;
			$shipID = $this->input->post('ship_id');
			$user_id = $this->input->post('user_id');
			$is_default = $this->input->post('set_default');
			if ($is_default == ''){
				$primary = 'No';
			}else{
				$primary = 'Yes';
			}
			$checkAddrCount = $this->user_model->get_all_details(SHIPPING_ADDRESS,array('user_id'=>$this->checkLogin('U')));
			if ($checkAddrCount->num_rows == 0){
				$primary = 'Yes';
			}
			$excludeArr = array('ship_id','set_default','user_id');
			$dataArr = array('primary'=>$primary,'user_id'=>$user_id);
			$condition = array('id'=>$shipID);
			if ($shipID==''){
				$this->user_model->commonInsertUpdate(SHIPPING_ADDRESS,'insert',$excludeArr,$dataArr,$condition);
				$shipID = $this->user_model->get_last_insert_id();
				if($this->lang->line('ship_add_succ') != '')
				$lg_err_msg = $this->lang->line('ship_add_succ');
				else
				$lg_err_msg = 'Your Shipping address is added successfully !';
				$this->setErrorMessage('success',$lg_err_msg);
			}else {
				$this->user_model->commonInsertUpdate(SHIPPING_ADDRESS,'update',$excludeArr,$dataArr,$condition);
				if($this->lang->line('ship_updat_succ') != '')
				$lg_err_msg = $this->lang->line('ship_updat_succ');
				else
				$lg_err_msg = 'Shipping address updated successfully';
				$this->setErrorMessage('success',$lg_err_msg);
			}
			if ($primary == 'Yes'){
				$condition = array('id !='=>$shipID,'user_id'=>$this->checkLogin('U'));
				$dataArr = array('primary'=>'No');
				$this->user_model->update_details(SHIPPING_ADDRESS,$dataArr,$condition);
			}else {
				$condition = array('primary'=>'Yes','user_id'=>$this->checkLogin('U'));
				$checkPrimary = $this->user_model->get_all_details(SHIPPING_ADDRESS,$condition);
				if ($checkPrimary->num_rows()==0){
					$condition = array('id'=>$shipID,'user_id'=>$this->checkLogin('U'));
					$dataArr = array('primary'=>'Yes');
					$this->user_model->update_details(SHIPPING_ADDRESS,$dataArr,$condition);
				}
			}
			redirect(base_url().'settings/shipping');
		}
	}
	
	
	
	public function shipping_settings_edit(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
		$shipID = $this->uri->segment('3');
			$this->data['heading'] = 'Shipping Address Edit';
			$this->data['countryList'] = $this->user_model->get_all_details(COUNTRY_LIST,array(),array(array('field'=>'name','type'=>'asc')));
			$this->data['shipDetails'] = $shipDetails = $this->user_model->get_all_details(SHIPPING_ADDRESS,array('id'=>$shipID));
			//echo "<pre>"; print_r($shipDetails->result());die;
			$this->load->view('site/user/shipping_settings_edit',$this->data);
		}
	}

	public function get_shipping(){
		$shipID = $this->input->post('shipID');
		$shipDetails = $this->user_model->get_all_details(SHIPPING_ADDRESS,array('id'=>$shipID));
		$returnStr['full_name'] = $shipDetails->row()->full_name;
		$returnStr['nick_name'] = $shipDetails->row()->nick_name;
		$returnStr['address1'] = $shipDetails->row()->address1;
		$returnStr['address2'] = $shipDetails->row()->address2;
		$returnStr['city'] = $shipDetails->row()->city;
		$returnStr['state'] = $shipDetails->row()->state;
		$returnStr['country'] = $shipDetails->row()->country;
		$returnStr['postal_code'] = $shipDetails->row()->postal_code;
		$returnStr['phone'] = $shipDetails->row()->phone;
		$returnStr['primary'] = $shipDetails->row()->primary;
		echo json_encode($returnStr);
	}

	public function remove_shipping_addr(){
		$returnStr['status_code'] = 0;
		if ($this->checkLogin('U')==''){
			if($this->lang->line('u_must_login') != '')
			$returnStr['message'] = $this->lang->line('u_must_login');
			else
			$returnStr['message'] = 'You must login';
		}else {
			$shipID = $this->input->post('id');
			$this->user_model->commonDelete(SHIPPING_ADDRESS,array('id'=>$shipID));
			$returnStr['status_code'] = 1;
		}
		echo json_encode($returnStr);
	}

	public function user_credits(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$this->data['heading'] = 'My Earnings';
			$orderDetails = $this->data['orderDetails'] = $this->commission->get_total_order_amount($this->checkLogin('U'));
			$commission_to_admin = 0;
			$amount_to_vendor = 0;
			$total_amount = 0;
			$this->data['total_amount'] = $total_amount;
			$total_orders = 0;
			$this->data['except_refunded'] = 0;
			if ($orderDetails->num_rows()==1){
				$commission_percentage = $this->data['userDetails']->row()->commision;
				$total_amount = $orderDetails->row()->TotalAmt;
				$this->data['total_amount'] = $total_amount;
				$total_amount = $total_amount-$this->data['userDetails']->row()->refund_amount;
				$this->data['except_refunded'] = $total_amount;
				$commission_to_admin = $total_amount*($commission_percentage*0.01);
				if ($commission_to_admin<0)$commission_to_admin=0;
				$amount_to_vendor = $total_amount-$commission_to_admin;
				if ($amount_to_vendor<0)$amount_to_vendor=0;
				$total_orders = $orderDetails->row()->orders;
			}
			$paidDetails = $this->commission->get_total_paid_details($this->checkLogin('U'));
			$paid_to = 0;
			if ($paidDetails->num_rows()==1){
				$paid_to = $paidDetails->row()->totalPaid;
				if ($paid_to<0)$paid_to=0;
			}
			$paid_to_balance = $amount_to_vendor-$paid_to;
			if ($paid_to_balance<0)$paid_to_balance=0;
			
			$total_admin_commision = $commission_to_admin - $this->data['userDetails']->row()->cash_on_delivery;
			$codDetails = $this->data['codDetails'] = $this->commission->get_total_cod_amount($this->checkLogin('U'));
			if($codDetails->row()->codrow>0){
				$remamtcod = $this->data['userDetails']->row()->cash_on_delivery - $codDetails->row()->CODAmt;
				$paidamtcod = $codDetails->row()->CODAmt;
			}else{
				$remamtcod = $this->data['userDetails']->row()->cash_on_delivery;
				$paidamtcod = '0.00';
			}
			$this->data['cod_orders'] = $this->user_model->get_all_details(PAYMENT,array('sell_id'=>$this->checkLogin('U'),'payment_type'=>'Cash on Delivery'));
			$this->data['cod_total_amount'] = $this->data['userDetails']->row()->cash_on_delivery;
			//print_r($this->data['userDetails']->result()); die;
			$this->data['cod_amount_paid'] =  $paidamtcod;
			$this->data['cod_amount_remain'] = $remamtcod;
			
			
			$this->data['commission_to_admin'] = $commission_to_admin;
			$this->data['amount_to_vendor'] = $amount_to_vendor;
			$this->data['total_orders'] = $total_orders;
			$this->data['paid_to'] = $paid_to;
			$this->data['paid_to_balance'] = $paid_to_balance;
			
			$sortArr1 = array('field'=>'date','type'=>'desc');
			$sortArr = array($sortArr1);
			
			$this->data['paidDetailsList'] = $this->commission->get_all_details(VENDOR_PAYMENT,array('vendor_id'=>$this->checkLogin('U'),'status'=>'success'),$sortArr);
			$this->data['paidCodList'] = $this->commission->get_all_details(COD_PAYMENT,array('seller_id'=>$this->checkLogin('U'),'status'=>'success'),$sortArr);
			$this->load->view('site/user/user_credits',$this->data);
		}
	}

	
	public function user_cod_payment(){
		if ($this->data['loginCheck'] != ''){
	
			$this->data['heading'] = 'Checkout';
			$codDetails = $this->data['codDetails'] = $this->commission->get_total_cod_amount($this->checkLogin('U'));
			if($codDetails->row()->codrow>0){
				$remamtcod = $this->data['userDetails']->row()->cash_on_delivery - $codDetails->row()->CODAmt;
			}else{
				$remamtcod = $this->data['userDetails']->row()->cash_on_delivery;
			}
	
			$invoice_id = mktime();
			$checkId = $this->check_cod_id($invoice_id);
			while ($checkId->num_rows()>0){
				$invoice_id = mktime();
				$checkId = $this->check_cod_id($invoice_id);
			}
			$this->session->set_userdata('invoiceID',$invoice_id);
			$this->data['total_price'] = $remamtcod;
			$this->data['countryList'] = $this->user_model->get_all_details(COUNTRY_LIST,array());
	
			//echo "jhghj"; die;
			$this->load->view('site/checkout/cod_checkout.php',$this->data);
		}else{
			redirect('login');
		}
	}
	
	public function check_cod_id($ivd=''){
		$checkId = $this->user_model->get_all_details(COD_PAYMENT,array('invoice_id'=>$ivd));
		return $checkId;
	}
	
	public function user_referrals(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			//echo "hi";die;
			$paginationNo = $this->uri->segment('2');

			if($paginationNo == '')
			{
				$paginationNo = 0;
			}
			else
			{
				$paginationNo = $paginationNo;
			}

			$searchPerPage = $this->config->item('pagination_per_page');
			//echo "DSf".$this->uri->segment('2');die;

			$referalBaseUrl = base_url().'referrals';


			$getReferalListCount = $this->user_model->getReferalList();
			$getReferalList = $this->user_model->getReferalList($searchPerPage,$paginationNo);

			$config['base_url'] = $referalBaseUrl;
			$config['total_rows'] = count($getReferalListCount);
			$config["per_page"] = $searchPerPage;
			$config["uri_segment"] =2;
			$this->pagination->initialize($config);
			$paginationLink = $this->pagination->create_links();//die;



			$this->data['heading'] = 'Referrals';
			$this->data['getReferalList'] = $getReferalList;
			$this->data['paginationLink'] = $paginationLink;


			//	echo "<pre>";print_r($getReferalList);die;


			//	    	$this->data['purchasesList'] = $this->user_model->get_group_gifts_list($this->checkLogin('U'));
			$this->load->view('site/user/user_referrals',$this->data);
		}
	}

	public function user_giftcards(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$this->data['heading'] = 'Gift Cards';
			$this->data['giftcardsList'] = $this->user_model->get_gift_cards_list($this->data['userDetails']->row()->email);
			$this->load->view('site/user/user_giftcards',$this->data);
		}
	}

	public function change_photo(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$config['overwrite'] = FALSE;
			$config['remove_spaces'] = TRUE;
			$config['allowed_types'] = 'jpg|jpeg|gif|png';
			$config['max_size'] = 2000;
			$config['max_width']  = '600';
			$config['max_height']  = '600';
			$config['upload_path'] = DESKTOPURL.'images/users';
			$this->load->library('upload', $config);
			if ( $this->upload->do_upload('upload-file')){
				$imgDetails = $this->upload->data();
				$dataArr['thumbnail'] = $imgDetails['file_name'];
				$condition = array('id'=>$this->checkLogin('U'));
				$this->user_model->update_details(USERS,$dataArr,$condition);
				redirect('image-crop/'.$this->checkLogin('U'));
			}else {
				$this->setErrorMessage('error',strip_tags($this->upload->display_errors()));
			}
			echo "<script>window.history.go(-1);</script>";
		}
	}
	public function app_network(){
	  if($this->checkLogin('U')==''){
	     redirect(base_url().'login');
	  }else{
	    $user_details = $this->user_model->get_all_details(USERS,array('id'=>$this->checkLogin('U')));
		if($user_details->num_rows() ==1){
		   $this->data['user_details'] = $user_details;
		}
	    $this->data['heading'] = 'Settings';
	    $this->load->view('site/user/app_network',$this->data); 
	  }
	}
	public function facebook_connect(){
	   if($this->checkLogin('U')==''){
	     redirect(base_url().'login');
	  }else{
	    $facebook_user_id = $this->input->post('id');
		$facebook_user_name = $this->input->post('username');
		if($facebook_user_id!=''){
		   $condition = "select * from ".USERS." where facebook_id=".$facebook_user_id." and id != ".$this->checkLogin('U')."";
		   $user_details = $this->user_model->ExecuteQuery($condition);
		   if($user_details->num_rows()==1){
		      echo "0";
		   }else{
		      $condition = array('id'=>$this->checkLogin('U'));
		      $dataArr = array('facebook_id'=> $facebook_user_id,'fb_username'=> $facebook_user_name);
		      $this->user_model->update_details(USERS,$dataArr,$condition);
		      echo "1";
		   }
		}else{
		      $condition = array('id'=>$this->checkLogin('U'));
		      $dataArr = array('facebook_id'=> '0','fb_username'=> '');
		      $this->user_model->update_details(USERS,$dataArr,$condition);
		}
	  }
	}
public function google_connect(){
	if($this->checkLogin('U')==''){
		redirect(base_url().'login');
	}else{
		$google_user_id = $this->input->post('id');
		$google_user_name = $this->input->post('username');
		if($google_user_id!=''){
			$condition = "select * from ".USERS." where google_id=".$google_user_id." and id != ".$this->checkLogin('U')."";
			$user_details = $this->user_model->ExecuteQuery($condition);
			if($user_details->num_rows()==1){
				echo "0";
			}else{
				$condition = array('id'=>$this->checkLogin('U'));
				$dataArr = array('google_id'=> $google_user_id,'gl_username'=> $google_user_name);
				$this->user_model->update_details(USERS,$dataArr,$condition);
				echo "1";
			}
		}else{
			$condition = array('id'=>$this->checkLogin('U'));
			$dataArr = array('google_id'=> '0','gl_username'=> '');
			$this->user_model->update_details(USERS,$dataArr,$condition);
		}
	}
}
public function promotions(){
  if($this->checkLogin('U')==''){
		redirect(base_url().'login');
	}else{
		$this->data['heading'] = 'Promotions';
		$this->load->view('site/user/promotions',$this->data);
	}
}

/****Featured sellers*******/
	public function bulk_upload(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else if($this->data['userDetails']->row()->group == 'Seller') {
                        $query = "SELECT c.*,m.cat_name as parent_name FROM ".CATEGORY." c LEFT JOIN ".CATEGORY." m on c.rootID = m.id ORDER BY rootID ";
                        $this->data['categories'] = $this->user_model->ExecuteQuery($query);
			$this->load->view('site/user/bulk_upload_form',$this->data);
		}else{
			redirect(base_url());
		}
	}
	public function bulk_upload_action(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else if($this->data['userDetails']->row()->group == 'Seller') {
			$csv = array_map("str_getcsv", file($_FILES['product_name']['tmp_name'],FILE_SKIP_EMPTY_LINES));
			$keys = array_shift($csv);
			foreach ($csv as $i=>$row) {
			    $csv[$i] = array_combine($keys, $row);
			}
			if(count($csv) > 0){
				foreach ($csv as $product) {
							$product_name = $product['product_name'];
							//$product_id = $this->input->post('productID');
							if ($product_name == ''){
								$this->setErrorMessage('error','Product name required');
								//				redirect('admin/product/add_product_form');
								echo "<script>window.history.go(-1)</script>";exit();
							}
							$sale_price = $product['sale_price'];
							if ($sale_price == ''){
								$this->setErrorMessage('error','Sale price required');
								//				redirect('admin/product/add_product_form');
								echo "<script>window.history.go(-1)</script>";exit();
							}else if ($sale_price <= 0){
								$this->setErrorMessage('error','Sale price must be greater than zero');
								echo "<script>window.history.go(-1)</script>";exit();
								//redirect('admin/product/add_product_form');
							}
								$old_product_details = array();
								$condition = array('product_name' => $product_name);

							/*			$duplicate_name = $this->product_model->get_all_details(PRODUCT,$condition);
							 if ($duplicate_name->num_rows() > 0){
								$this->setErrorMessage('error','Product name already exists');
								echo "<script>window.history.go(-1)</script>";exit();
								}
								*/			
								$price_range = '';
							if ($sale_price>0 && $sale_price<21){
								$price_range = '1-20';
							}else if ($sale_price>20 && $sale_price<101){
								$price_range = '21-100';
							}else if ($sale_price>100 && $sale_price<201){
								$price_range = '101-200';
							}else if ($sale_price>200 && $sale_price<501){
								$price_range = '201-500';
							}else if ($sale_price>500){
								$price_range = '501+';
							}
							$excludeArr = array("gateway_tbl_length","imaged","productID","changeorder","status","category_id","attribute_name","profile","attribute_val","attribute_weight","attribute_price","product_image","userID","product_attribute_name","product_attribute_val","attr_name1","attr_val1","attr_type1","product_attribute_type");
							if ( strtolower($product['status']) == 'active'){
								$product_status = 'Publish';
							}else {
								$product_status = 'UnPublish';
							}

							$seourl = url_title($product_name, '-', TRUE);
							$checkSeo = $this->product_model->get_all_details(PRODUCT,array('seourl'=>$seourl));
							$seo_count = 1;
							while ($checkSeo->num_rows()>0){
								$seourl = $seourl.$seo_count;
								$seo_count++;
								$checkSeo = $this->product_model->get_all_details(PRODUCT,array('seourl'=>$seourl));
							}
							if ($product['category_id'] != ''){
								$category_id = $product['category_id'];
							}else {
								$category_id = '';
							}
							$ImageName = '';
							$list_name_str = $list_val_str = '';
							$list_name_arr = $this->input->post('attribute_name');
							$list_val_arr = $this->input->post('attribute_val');
							if (is_array($list_name_arr) && count($list_name_arr)>0){
								$list_name_str = implode(',', $list_name_arr);
								$list_val_str = implode(',', $list_val_arr);
							}
							//			$option['attribute_name'] = $this->input->post('attribute_name');
							//			$option['attribute_val'] = $this->input->post('attribute_val');
							//			$option['attribute_weight'] = $this->input->post('attribute_weight');
							//			$option['attribute_price'] = $this->input->post('attribute_price');
							$datestring = "%Y-%m-%d %h:%i:%s";
							$time = time();
                                                        $pid = mktime();
                                                        $checkId = $this->product_model->check_product_id($pid);
                                                        while ($checkId->num_rows()>0){
                                                        $pid = mktime();
                                                        $checkId = $this->product_model->check_product_id($pid);
                                                        }
								$inputArr = array(
											'modified' => mdate($datestring,$time),
											'seourl' => $seourl,
											'category_id' => $category_id,
											'status' => $product_status,
											'price_range'=> $price_range,
											'list_name' => $list_name_str,
											'list_value' => $list_val_str,
											'product_name' => $product_name,
											'quantity' => $product['quantity'],
											'price' => $product['price'],
											'sale_price' => $product['sale_price'],
											'sku' => $product['Sku'],
											'seller_product_id' =>$pid,
											'meta_title' => $product['meta_title'],
											'meta_description' => $product['meta_description'],
											'meta_keyword' => $product['meta_keyword'],
											'user_id' => $this->checkLogin('U')
								);

/*							//$config['encrypt_name'] = TRUE;
							$config['overwrite'] = FALSE;
							$config['allowed_types'] = 'jpg|jpeg|gif|png';
							//		    $config['max_size'] = 2000;
							$config['upload_path'] = './images/product';
							$this->load->library('upload', $config);
							//echo "<pre>";print_r($_FILES);die;
							if ( $this->upload->do_multi_upload('product_image')){
								$logoDetails = $this->upload->get_multi_upload_data();
								foreach ($logoDetails as $fileDetails){
									$this->imageResizeWithSpace(600, 600, $fileDetails['file_name'], './images/product/');
									$ImageName .= $fileDetails['file_name'].',';
								}
							}*/

							
											$url_headers=get_headers($product['image_url'],1);
											if(is_array($url_headers)){
												$image_type = $url_headers['Content-Type'];
												$valid_image_type=array();
												$valid_image_type['image/png']='png';
												$valid_image_type['image/jpg']='jpg';
												$valid_image_type['image/jpeg']='jpg';
												$valid_image_type['image/jpe']='jpg';
												$valid_image_type['image/gif']='gif';
												if(isset($valid_image_type[$image_type])){
													$image = file_get_contents($product['image_url'],'r');
													$filename = mktime().'.'.$valid_image_type[$image_type];
													while (file_exists('../images/product/'.$filename)) {
														$filename = mktime().'.'.$valid_image_type[$image_type];
													}
													$fp = fopen('../images/product/'.$filename,'w');
													$write_result = fwrite($fp, $image);
													fclose($fp);
													if ($write_result) {
														$this->imageResizeWithSpace(600, 600, $filename, '../images/product/');
													}else{
	                                                                                                        $this->setErrorMessage('error','Unable to retrieve image');
														echo "<script>window.history.go(-1)</script>";exit();
													}
												}else{
	                                                                                                        $this->setErrorMessage('error','Invalid Image type');
														echo "<script>window.history.go(-1)</script>";exit();													
												}
											}

							$product_data = array( 'image' => $filename);
							$dataArr = array_merge($inputArr,$product_data);
								$condition = array();
								$this->product_model->commonInsertUpdate(PRODUCT,'insert',$excludeArr,$dataArr,$condition);
								$product_id = $this->product_model->get_last_insert_id();
								//Generate short url
								$short_url = $this->get_rand_str('6');
								$checkId = $this->product_model->get_all_details(SHORTURL,array('short_url'=>$short_url));
								while ($checkId->num_rows()>0){
									$short_url = $this->get_rand_str('6');
									$checkId = $this->product_model->get_all_details(SHORTURL,array('short_url'=>$short_url));
								}
								$url = base_url().'things/'.$product_id.'/'.url_title($product_name,'-');
								$this->product_model->simple_insert(SHORTURL,array('short_url'=>$short_url,'long_url'=>$url));
								$urlid = $this->product_model->get_last_insert_id();
								$this->product_model->update_details(PRODUCT,array('short_url_id'=>$urlid),array('id'=>$product_id));
								$userDetails = $this->user_model->get_user_details_by_id($this->checkLogin('U'));
								$total_added = $userDetails->row()->products;
								$total_added++;
								$this->product_model->update_details(USERS,array('products'=>$total_added),array('id'=>$this->checkLogin('U')));
								////////////////////
								
/*								$Attr_name_str = $Attr_val_str = '';
								$Attr_type_arr = $this->input->post('product_attribute_type');
								$Attr_name_arr = $this->input->post('product_attribute_name');
								$Attr_val_arr = $this->input->post('product_attribute_val');
								if (is_array($Attr_name_arr) && count($Attr_name_arr)>0){
									for($k=0;$k<sizeof($Attr_name_arr);$k++){
										$dataSubArr = '';
										$dataSubArr = array('product_id'=> $product_id,'attr_id'=>$Attr_type_arr[$k],'attr_name'=>$Attr_name_arr[$k],'attr_price'=>$Attr_val_arr[$k]);
										//echo '<pre>'; print_r($dataSubArr);
										$this->product_model->add_subproduct_insert($dataSubArr);
									}
								}*/
/*								$product_id = $this->product_model->get_last_insert_id();
								$this->update_price_range_in_table('add',$price_range,$product_id,$old_product_details);*/
								}
								$this->setErrorMessage('success','Product added successfully');
								$this->load->library('user_agent');
								redirect($this->agent->referrer());				
			}else{
				$this->setErrorMessage('error','Kindly Upload a valid file');
				$this->load->library('user_agent');
				redirect($this->agent->referrer());
			}
		}else{
			redirect(base_url());
		}
	}
	
        public function view_featured_plan(){
            $this->load->model('seller_plan_model','seller_plan');
            $this->data['is_featured'] = $this->seller_plan->get_all_details(FEATURED_SELLER,array('userId'=>$this->checkLogin('U'),'isExpired'=>'0'));
            if($this->data['is_featured']->num_rows == 0){
            $this->data['plan'] = $this->seller_plan->get_all_details(SELLER_PLAN,array());
            $this->load->view('site/user/view_featured_plan',$this->data);
            }else{
                $this->setErrorMessage('danger','Already plan subscribed');
                redirect(base_url().'featured');
            }
        }
        public function seller_plan_pay(){
            $payment_method = $_GET['payment'];
            if($payment_method == ''){
                $payment_method = '1';
            }
            switch ($payment_method):
                case '1':
                    $redirect = 'paypal';
                    break;
                case '2':
                    $redirect = 'paypal_credit';
                    break;
                case '3':
                    $redirect = 'authorize';
                    break;
                case '4':
                    $redirect = 'stripe';
                    break;
                case '5':
                    $redirect = 'twocheckout';
                    break;               
            endswitch;
            redirect(base_url().'site/user_settings/'.$redirect);
//            $this->load->model('seller_plan_model','seller_plan');
//            $listCheck = $this->seller_plan->get_all_details(FEATURED_SELLER,array('userId' => $this->checkLogin('U'),'isExpired'=>'0'));
//            if($listCheck->num_rows == 0){
//                $plan = $this->seller_plan->get_all_details(SELLER_PLAN,array());
//                $registeredSellers = $this->seller_plan->get_all_details(FEATURED_SELLER,array('isExpired'=>'0'));
//                if($registeredSellers->num_rows < $plan->row()->plan_promoted_count){
//                $time = time();
//                $datestring = "%Y-%m-%d %h:%i:%s";                
//                $dataArr['userId'] = $this->checkLogin('U');
//                $dataArr['featuredOn'] = mdate($datestring,$time);
//                $dataArr['expiresOn'] = date("Y-m-d h:i:s", strtotime($dataArr['featuredOn'].'+ '.$plan->row()->plan_period.' days'));
//                $dataArr['planPrice'] = $plan->row()->plan_price;
//                $dataArr['planPeriod'] = $plan->row()->plan_period;
//                $dataArr['productCount'] = $plan->row()->plan_product_count;
//                $dataArr['createdOn'] = mdate($datestring,$time);
//                $this->seller_plan->simple_insert(FEATURED_SELLER,$dataArr);
//                redirect(base_url().'featured');
//                }else{
//                    $this->setErrorMessage('danger','Maximum Number of featured Sellers reached');
//                    redirect(base_url().'featured');
//                }
//            }else{
//                $this->setErrorMessage('danger','Already plan subscribed!!');
//                redirect(base_url().'featured');                
//            }
        }
        public function addSellerFeaturedProducts(){
            $this->load->model('seller_plan_model','seller_plan');
            $return['message'] = '0';
            $listCheck = $this->seller_plan->get_all_details(FEATURED_SELLER,array('userId' => $this->checkLogin('U'),'isExpired'=>'0'));
            if($listCheck->num_rows > 0){
                $featuredProducts = serialize($this->input->post('pid'));
                $dataArr['featuredProducts'] = $featuredProducts;
                $productExists = $this->seller_plan->get_all_details(FEATURED_PRODUCTS,array('userId'=>$this->checkLogin('U'),'sellerPlan_id'=>$listCheck->row()->id));
                if($productExists->num_rows > 0){
                    $this->seller_plan->update_details(FEATURED_PRODUCTS,$dataArr,array('userId'=>$this->checkLogin('U'),'sellerPlan_id'=>$listCheck->row()->id));
                }else{
                    $dataArr['userId'] = $this->checkLogin('U');
                    $dataArr['sellerPlan_id'] = $listCheck->row()->id;
                    $this->seller_plan->simple_insert(FEATURED_PRODUCTS,$dataArr);
                }
                $return['message'] = '1';
            }
            print_r(json_encode($return));exit;
        }
        public function twocheckout(){
            $this->load->model('seller_plan_model','seller_plan');
            $this->data['plan'] = $this->seller_plan->get_all_details(SELLER_PLAN,array());
            $this->load->view('site/user/featured_seller_twocheckout',$this->data);
        }
        public function twocheckout_payment(){
                $this->load->model('seller_plan_model','seller_plan');
                $listCheck = $this->seller_plan->get_all_details(FEATURED_SELLER,array('userId' => $this->checkLogin('U'),'isExpired'=>'0'));
                if($listCheck->num_rows == 0){
                    $plan = $this->seller_plan->get_all_details(SELLER_PLAN,array());
                    $registeredSellers = $this->seller_plan->get_all_details(FEATURED_SELLER,array('isExpired'=>'0'));
                    if($registeredSellers->num_rows < $plan->row()->plan_promoted_count){   
                $time = time();
                $datestring = "%Y-%m-%d %h:%i:%s";  
                $dataArr = array('userId'=>$this->checkLogin('U'),'planPrice'=>$plan->row()->plan_price,'planPeriod'=>$plan->row()->plan_period,'productCount'=>$plan->row()->plan_product_count,'paymentType'=>'2CO');
                $dataArr['createdOn'] = mdate($datestring,$time);
                $this->seller_plan->simple_insert('fc_featured_seller_temp',$dataArr);
                $temp_id = $this->db->insert_id();                        
		$this->load->library('Twocheckout');
                $paypal_settings = unserialize($this->config->item('payment_5'));
		$settings=unserialize($paypal_settings['settings']);
		Twocheckout::privateKey($settings['privateKey']);
		Twocheckout::sellerId($settings['sellerId']);
		Twocheckout::sandbox(true);  #Uncomment to use Sandbox
		$tokenid=$this->input->post('token');
		//echo $tokenid;die;
		$loginUserId = $this->checkLogin('U'); 
		$totalAmount = $this->input->post('total_price');
                $quantity = 1;
		try {
			$charge = Twocheckout_Charge::auth(array(
					"merchantOrderId" => "123",
					"token" => $tokenid,
					"currency" => 'USD',
					"total" => $totalAmount,
					"billingAddr" => array(
							"name" => $this->input->post('full_name'),
							"addrLine1" => $this->input->post('address'),
							"city" => $this->input->post('city'),
							"state" =>$this->input->post('state'),
							"zipCode" => $this->input->post('postal_code'),
							"country" => $this->input->post('country'),
							"email" => $this->input->post('email'),
							"phoneNumber" => $this->input->post('phone_no')
					),
					"shippingAddr" => array(
							"name" => $this->input->post('full_name'),
							"addrLine1" => $this->input->post('address'),
							"city" => $this->input->post('city'),
							"state" =>$this->input->post('state'),
							"zipCode" => $this->input->post('postal_code'),
							"country" => $this->input->post('country'),
							"email" => $this->input->post('email'),
							"phoneNumber" => $this->input->post('phone_no')
					)
			), 'array');
			
			$result=(array)json_decode($charge);
//			echo "<pre>";print_r($result);die;
			$rescod=$result['response'];
			if ($rescod->responseCode == 'APPROVED') {
                                    $temp_store = $this->seller_plan->get_all_details('fc_featured_seller_temp',array('id'=>$temp_id));
                                    $this->seller_plan->commonDelete('fc_featured_seller_temp',array('id'=>$temp_id));
                                    $time = time();
                                    $datestring = "%Y-%m-%d %h:%i:%s";                
                                    $dataArr_1['userId'] = $this->checkLogin('U');
                                    $dataArr_1['featuredOn'] = mdate($datestring,$time);
                                    $dataArr_1['expiresOn'] = date("Y-m-d h:i:s", strtotime($dataArr_1['featuredOn'].'+ '.$temp_store->row()->planPeriod.' days'));
                                    $dataArr_1['planPrice'] = $temp_store->row()->planPrice;
                                    $dataArr_1['planPeriod'] = $temp_store->row()->planPeriod;
                                    $dataArr_1['productCount'] = $temp_store->row()->productCount;
                                    $dataArr_1['paymentType'] = $temp_store->row()->paymentType;
                                    $dataArr_1['createdOn'] = mdate($datestring,$time);
                                    $this->seller_plan->simple_insert(FEATURED_SELLER,$dataArr_1);
                                    $this->setErrorMessage('success','Payment Successful');
                                    redirect(base_url().'featured');
			}else{	
                                $this->seller_plan->commonDelete('fc_featured_seller_temp',array('id'=>$temp_id));
                                $this->setErrorMessage('danger',$result['exception']->errorMsg);
				redirect(base_url().'featured');
						}
		} catch (Twocheckout_Error $e) {
			$e->getMessage();
		}   
            }else{
                $this->setErrorMessage('danger','Maximum Number of featured Sellers reached');
                redirect(base_url().'featured');
            }
        }else{
            $this->setErrorMessage('danger','Already plan subscribed!!');
            redirect(base_url().'featured');                
        }                
    }
    public function stripe(){
            $this->load->model('seller_plan_model','seller_plan');
            $this->data['plan'] = $this->seller_plan->get_all_details(SELLER_PLAN,array());
            $this->load->view('site/user/featured_seller_stripe',$this->data);        
    }
    public function stripe_payment(){
        $this->load->model('seller_plan_model','seller_plan');
        $listCheck = $this->seller_plan->get_all_details(FEATURED_SELLER,array('userId' => $this->checkLogin('U'),'isExpired'=>'0'));
        if($listCheck->num_rows == 0){
            $plan = $this->seller_plan->get_all_details(SELLER_PLAN,array());
            $registeredSellers = $this->seller_plan->get_all_details(FEATURED_SELLER,array('isExpired'=>'0'));
            if($registeredSellers->num_rows < $plan->row()->plan_promoted_count){     
                $time = time();
                $datestring = "%Y-%m-%d %h:%i:%s";  
                $dataArr = array('userId'=>$this->checkLogin('U'),'planPrice'=>$plan->row()->plan_price,'planPeriod'=>$plan->row()->plan_period,'productCount'=>$plan->row()->plan_product_count,'paymentType'=>'stripe');
                $dataArr['createdOn'] = mdate($datestring,$time);
                $this->seller_plan->simple_insert('fc_featured_seller_temp',$dataArr);
                $temp_id = $this->db->insert_id(); 
                
		$this->load->library('Stripe');
		$success = "";
		$error = "";
		$totalAmount = $this->input->post('total_price');
		$crdno=$this->input->post('cardNumber');
		$crdname=$this->input->post('cardType');
		$year=$this->input->post('CCExpMnth');
		$mnth=$this->input->post('CCExpDay');
		$cvv=$this->input->post('creditCardIdentifier');
		$crd=array('number'=>$crdno,'exp_month'=>$mnth,'exp_year'=>$year,'cvc'=>$cvv,'name'=>$crdname);
		$item_name = $this->config->item('email_title').' Products';
		if ($_POST) {
	//print_r($crd);die;
			$success = Stripe::charge_card($totalAmount,$crd);
			//echo "<pre>";print_r($result);die;
			if(isset($success))
			{
				$result_success = (array)json_decode($success);
				//print_r($result_success);die;
					
				if($result_success['status']=="succeeded"){
                                    $temp_store = $this->seller_plan->get_all_details('fc_featured_seller_temp',array('id'=>$temp_id));
                                    $this->seller_plan->commonDelete('fc_featured_seller_temp',array('id'=>$temp_id));                                    
                                    $time = time();
                                    $datestring = "%Y-%m-%d %h:%i:%s";                
                                    $dataArr_1['userId'] = $this->checkLogin('U');
                                    $dataArr_1['featuredOn'] = mdate($datestring,$time);
                                    $dataArr_1['expiresOn'] = date("Y-m-d h:i:s", strtotime($dataArr_1['featuredOn'].'+ '.$temp_store->row()->planPeriod.' days'));
                                    $dataArr_1['planPrice'] = $temp_store->row()->planPrice;
                                    $dataArr_1['planPeriod'] = $temp_store->row()->planPeriod;
                                    $dataArr_1['productCount'] = $temp_store->row()->productCount;
                                    $dataArr_1['paymentType'] = $temp_store->row()->paymentType;
                                    $dataArr_1['createdOn'] = mdate($datestring,$time);
                                    $this->seller_plan->simple_insert(FEATURED_SELLER,$dataArr_1);
                                    $this->setErrorMessage('success','Payment Successful');
                                    redirect(base_url().'featured');
				}
				else
				{
					$err=(array)$result_success['error'];
                                        $this->seller_plan->commonDelete('fc_featured_seller_temp',array('id'=>$temp_id));
					$statustrans['msg']=$err['message'];
                                        $this->setErrorMessage('danger',$err['message']);
                                        redirect(base_url().'featured');
				}
					
			}
		}
                }else{
                    $this->setErrorMessage('danger','Maximum Number of featured Sellers reached');
                    redirect(base_url().'featured');
                }
            }else{
                $this->setErrorMessage('danger','Already plan subscribed!!');
                redirect(base_url().'featured');                
            }                
    }
    public function authorize(){
            $this->load->model('seller_plan_model','seller_plan');
            $this->data['plan'] = $this->seller_plan->get_all_details(SELLER_PLAN,array());
            $this->load->view('site/user/featured_seller_authorize',$this->data);         
    }
    public function authorize_payment(){
            $this->load->model('seller_plan_model','seller_plan');
            $listCheck = $this->seller_plan->get_all_details(FEATURED_SELLER,array('userId' => $this->checkLogin('U'),'isExpired'=>'0'));
            if($listCheck->num_rows == 0){
                $plan = $this->seller_plan->get_all_details(SELLER_PLAN,array());
                $registeredSellers = $this->seller_plan->get_all_details(FEATURED_SELLER,array('isExpired'=>'0'));
                if($registeredSellers->num_rows < $plan->row()->plan_promoted_count){   
                $time = time();
                $datestring = "%Y-%m-%d %h:%i:%s";  
                $dataArr = array('userId'=>$this->checkLogin('U'),'planPrice'=>$plan->row()->plan_price,'planPeriod'=>$plan->row()->plan_period,'productCount'=>$plan->row()->plan_product_count,'paymentType'=>'authorize');
                $dataArr['createdOn'] = mdate($datestring,$time);
                $this->seller_plan->simple_insert('fc_featured_seller_temp',$dataArr);
                $temp_id = $this->db->insert_id();                    
                    
            $Auth_Details=unserialize(API_LOGINID); 
            $Auth_Setting_Details=unserialize($Auth_Details['settings']);	

             
            define("AUTHORIZENET_API_LOGIN_ID",$Auth_Setting_Details['Login_ID']);    // Add your API LOGIN ID
            define("AUTHORIZENET_TRANSACTION_KEY",$Auth_Setting_Details['Transaction_Key']); // Add your API transaction key
            define("API_MODE",$Auth_Setting_Details['mode']);

                    if(API_MODE	=='sandbox'){
                            define("AUTHORIZENET_SANDBOX",true);// Set to false to test against production
                    }else{
                            define("AUTHORIZENET_SANDBOX",false);
                    }       
                    define("TEST_REQUEST", "FALSE"); 
                    require_once './authorize/AuthorizeNet.php';

                    $transaction = new AuthorizeNetAIM;
                    $transaction->setSandbox(AUTHORIZENET_SANDBOX);
                    $transaction->setFields(
                            array(
                            'amount' =>  $this->input->post('total_price'), 
                            'card_num' =>  $this->input->post('cardNumber'), 
                            'exp_date' => $this->input->post('CCExpDay').'/'.$this->input->post('CCExpMnth'),
                            'first_name' => $this->input->post('full_name'),
                            'last_name' => '',
                            'address' => $this->input->post('address'),
                            'city' => $this->input->post('city'),
                            'state' => $this->input->post('state'),
                            'country' => $this->input->post('country'),
                            'phone' => $this->input->post('phone_no'),
                            'email' =>  $this->input->post('email'),
                            'card_code' => $this->input->post('creditCardIdentifier'),
                            )
                    );
                    $response = $transaction->authorizeAndCapture();

            if( $response->approved ){
                            $temp_store = $this->seller_plan->get_all_details('fc_featured_seller_temp',array('id'=>$temp_id));
                            $this->seller_plan->commonDelete('fc_featured_seller_temp',array('id'=>$temp_id));                                    
                            $time = time();
                            $datestring = "%Y-%m-%d %h:%i:%s";                
                            $dataArr_1['userId'] = $this->checkLogin('U');
                            $dataArr_1['featuredOn'] = mdate($datestring,$time);
                            $dataArr_1['expiresOn'] = date("Y-m-d h:i:s", strtotime($dataArr_1['featuredOn'].'+ '.$temp_store->row()->planPeriod.' days'));
                            $dataArr_1['planPrice'] = $temp_store->row()->planPrice;
                            $dataArr_1['planPeriod'] = $temp_store->row()->planPeriod;
                            $dataArr_1['productCount'] = $temp_store->row()->productCount;
                            $dataArr_1['paymentType'] = $temp_store->row()->paymentType;
                            $dataArr_1['createdOn'] = mdate($datestring,$time);
                            $this->seller_plan->simple_insert(FEATURED_SELLER,$dataArr_1);
                            $this->setErrorMessage('success','Payment Successful');
                            redirect(base_url().'featured');

            }else{		
                    //redirect('site/shopcart/cancel?failmsg='.$response->response_reason_text); 
                    //redirect('order/failure/'.$response->response_reason_text); 
                $this->seller_plan->commonDelete('fc_featured_seller_temp',array('id'=>$temp_id));
                    $this->setErrorMessage('danger',$response->response_reason_text);
                    redirect(base_url().'featured');                    
            }
                        }else{
                            $this->setErrorMessage('danger','Maximum Number of featured Sellers reached');
                            redirect(base_url().'featured');
                        }
                    }else{
                        $this->setErrorMessage('danger','Already plan subscribed!!');
                        redirect(base_url().'featured');                
                    }            
    }
    public function paypal_credit(){
            $this->load->model('seller_plan_model','seller_plan');
            $this->data['plan'] = $this->seller_plan->get_all_details(SELLER_PLAN,array());
            $this->load->view('site/user/featured_seller_paypal_credit',$this->data);          
    }
    public function paypal_credit_payment(){
            //$shipValID = $this->checkout_model->get_all_details(SHIPPING_ADDRESS,array( 'id' => $this->input->post('shipping_id')));	
            //echo '<pre>';print_r($shipValID->row()); die;
            $this->load->model('seller_plan_model','seller_plan');
            $listCheck = $this->seller_plan->get_all_details(FEATURED_SELLER,array('userId' => $this->checkLogin('U'),'isExpired'=>'0'));
            if($listCheck->num_rows == 0){
                $plan = $this->seller_plan->get_all_details(SELLER_PLAN,array());
                $registeredSellers = $this->seller_plan->get_all_details(FEATURED_SELLER,array('isExpired'=>'0'));
                if($registeredSellers->num_rows < $plan->row()->plan_promoted_count){
            $time = time();
            $datestring = "%Y-%m-%d %h:%i:%s";  
            $dataArr = array('userId'=>$this->checkLogin('U'),'planPrice'=>$plan->row()->plan_price,'planPeriod'=>$plan->row()->plan_period,'productCount'=>$plan->row()->plan_product_count,'paymentType'=>'Credit Card');
            $dataArr['createdOn'] = mdate($datestring,$time);
            $this->seller_plan->simple_insert('fc_featured_seller_temp',$dataArr);
            $temp_id = $this->db->insert_id();          
            
            $PaypalDodirect = unserialize($this->data['paypal_credit_card_settings']['settings']);
            
            $dodirects = array(
                    'Sandbox' => $PaypalDodirect['mode'], 			// Sandbox / testing mode option.
                    'APIUsername' =>$PaypalDodirect['Paypal_API_Username'], 	// PayPal API username of the API caller
                    'APIPassword' => $PaypalDodirect['paypal_api_password'], 	// PayPal API password of the API caller
                    'APISignature' => $PaypalDodirect['paypal_api_Signature'], 	// PayPal API signature of the API caller
                    'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
                    'APIVersion' => '85.0'		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
            );

            // Show Errors
            if($dodirects['Sandbox']){
                     
                    ini_set('display_errors', '1');
            }


            $this->load->library('paypal/Paypal_pro', $dodirects);	

            $DPFields = array(
                                            'paymentaction' => '', 						// How you want to obtain payment.  Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
                                            'ipaddress' => $this->input->ip_address(), 							// Required.  IP address of the payer's browser.
                                            'returnfmfdetails' => '1'				// Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
                                    );


            $CCDetails = array(
                                                    'creditcardtype' => $this->input->post('cardType'), 					// Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
                                                    'acct' => $this->input->post('cardNumber'), 								// Required.  Credit card number.  No spaces or punctuation.  
                                                    'expdate' => $this->input->post('CCExpDay').$this->input->post('CCExpMnth'), 	// Required.  Credit card expiration date.  Format is MMYYYY
                                                    'cvv2' => $this->input->post('creditCardIdentifier'), 				// Requirements determined by your PayPal account settings.  Security digits for credit card.
                                                    'startdate' => '', 							// Month and year that Maestro or Solo card was issued.  MMYYYY
                                                    'issuenumber' => ''							// Issue number of Maestro or Solo card.  Two numeric digits max.
                                            );

            $PayerInfo = array(
                                                    'email' => $this->input->post('email'), 	// Email address of payer.
                                                    'payerid' => '', 							// Unique PayPal customer ID for payer.
                                                    'payerstatus' => '', 	// Status of payer.  Values are verified or unverified
                                                    'business' => '' 		
                                                                                            // Payer's business name.
                                            );

            $PayerName = array(
                                                    'salutation' => 'Mr.', 						// Payer's salutation.  20 char max.
                                                    'firstname' => $this->input->post('full_name'), 							// Payer's first name.  25 char max.
                                                    'middlename' => '', 						// Payer's middle name.  25 char max.
                                                    'lastname' => '', 							// Payer's last name.  25 char max.
                                                    'suffix' => ''								// Payer's suffix.  12 char max.
                                            );

            //'x_amount'				=> ,
            //			'x_email'				=> $this->input->post('email'),

            $BillingAddress = array(
                                                            'street' => $this->input->post('address'), 						// Required.  First street address.
                                                            'street2' => '', 						// Second street address.
                                                            'city' => $this->input->post('city'), 							// Required.  Name of City.
                                                            'state' => $this->input->post('state'), 							// Required. Name of State or Province.
                                                            'countrycode' => $this->input->post('country'), 					// Required.  Country code.
                                                            'zip' => $this->input->post('postal_code'), 							// Required.  Postal code of payer.
                                                            'phonenum' => $this->input->post('phone_no') 						// Phone Number of payer.  20 char max.
                                                    );

            $ShippingAddress = array(
                                                            'shiptoname' => $this->input->post('full_name'),		// Required if shipping is included.  Person's name associated with this address.  32 char max.
                                                            'shiptostreet' => $this->input->post('address'),		// Required if shipping is included.  First street address.  100 char max.
                                                            'shiptostreet2' => '',  	// Second street address.  100 char max.
                                                            'shiptocity' => $this->input->post('city'),			// Required if shipping is included.  Name of city.  40 char max.
                                                            'shiptostate' => $this->input->post('state'),			// Required if shipping is included.  Name of state or province.  40 char max.
                                                            'shiptozip' => $this->input->post('postal_code'), 		// Required if shipping is included.  Postal code of shipping address.  20 char max.
                                                            'shiptocountry' => $this->input->post('country'), 		// Required if shipping is included.  Country code of shipping address.  2 char max.
                                                            'shiptophonenum' => $this->input->post('phone_no')		// Phone number for shipping address.  20 char max.
                                                            );

            $PaymentDetails = array(
                                                            'amt' => $this->input->post('total_price'), 							// Required.  Total amount of order, including shipping, handling, and tax.  
                                                            'currencycode' => $this->data['currencyType'], 					// Required.  Three-letter currency code.  Default is USD.
                                                            'itemamt' => '', 						// Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
                                                            'shippingamt' => '', 					// Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
                                                            'insuranceamt' => '', 					// Total shipping insurance costs for this order.  
                                                            'shipdiscamt' => '', 					// Shipping discount for the order, specified as a negative number.
                                                            'handlingamt' => '', 					// Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
                                                            'taxamt' => '', 						// Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax. 
                                                            'desc' => '', 							// Description of the order the customer is purchasing.  127 char max.
                                                            'custom' => '', 						// Free-form field for your own use.  256 char max.
                                                            'invnum' => '', 						// Your own invoice or tracking number
                                                            'buttonsource' => '', 					// An ID code for use by 3rd party apps to identify transactions.
                                                            'notifyurl' => '', 						// URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
                                                            'recurring' => ''						// Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
                                                    );

            // For order items you populate a nested array with multiple $Item arrays.  
            // Normally you'll be looping through cart items to populate the $Item array
            // Then push it into the $OrderItems array at the end of each loop for an entire 
            // collection of all items in $OrderItems.

            $OrderItems = array();

            $Item	 = array(
                                                    'l_name' => '', 						// Item Name.  127 char max.
                                                    'l_desc' => '', 						// Item description.  127 char max.
                                                    'l_amt' => '', 							// Cost of individual item.
                                                    'l_number' => '', 						// Item Number.  127 char max.
                                                    'l_qty' => '', 							// Item quantity.  Must be any positive integer.  
                                                    'l_taxamt' => '', 						// Item's sales tax amount.
                                                    'l_ebayitemnumber' => '', 				// eBay auction number of item.
                                                    'l_ebayitemauctiontxnid' => '', 		// eBay transaction ID of purchased item.
                                                    'l_ebayitemorderid' => '' 				// eBay order ID for the item.
                                    );

            array_push($OrderItems, $Item);

            $Secure3D = array(
                                              'authstatus3d' => '', 
                                              'mpivendor3ds' => '', 
                                              'cavv' => '', 
                                              'eci3ds' => '', 
                                              'xid' => ''
                                              );

            $PayPalRequestData = array(
                                                            'DPFields' => $DPFields, 
                                                            'CCDetails' => $CCDetails, 
                                                            'PayerInfo' => $PayerInfo, 
                                                            'PayerName' => $PayerName, 
                                                            'BillingAddress' => $BillingAddress, 
                                                            'ShippingAddress' => $ShippingAddress, 
                                                            //'ShippingAddress' => array(), 
                                                            'PaymentDetails' => $PaymentDetails, 
                                                            'OrderItems' => $OrderItems, 
                                                            'Secure3D' => $Secure3D
                                                    );

            $PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);
            if(!$this->paypal_pro->APICallSuccessful($PayPalResult['ACK'])){
                    $this->seller_plan->commonDelete('fc_featured_seller_temp',array('id'=>$temp_id));
                    $errors = array('Errors'=>$PayPalResult['ERRORS']);
                    //$this->load->view('paypal_error',$errors);
                    $newerrors = $errors['Errors'][0]['L_LONGMESSAGE'];
                    $this->setErrorMessage('danger', $newerrors);
                    redirect(base_url().'featured');
            }else{
                    // Successful call.  Load view or whatever you need to do here.	
                    $temp_store = $this->seller_plan->get_all_details('fc_featured_seller_temp',array('id'=>$temp_id));
                    $this->seller_plan->commonDelete('fc_featured_seller_temp',array('id'=>$temp_id));                                    
                    $time = time();
                    $datestring = "%Y-%m-%d %h:%i:%s";                
                    $dataArr_1['userId'] = $this->checkLogin('U');
                    $dataArr_1['featuredOn'] = mdate($datestring,$time);
                    $dataArr_1['expiresOn'] = date("Y-m-d h:i:s", strtotime($dataArr_1['featuredOn'].'+ '.$temp_store->row()->planPeriod.' days'));
                    $dataArr_1['planPrice'] = $temp_store->row()->planPrice;
                    $dataArr_1['planPeriod'] = $temp_store->row()->planPeriod;
                    $dataArr_1['productCount'] = $temp_store->row()->productCount;
                    $dataArr_1['paymentType'] = $temp_store->row()->paymentType;
                    $dataArr_1['createdOn'] = mdate($datestring,$time);
                    $this->seller_plan->simple_insert(FEATURED_SELLER,$dataArr_1);
                    $this->setErrorMessage('success','Payment Successful');  
                    redirect(base_url().'featured');
            }
            }else{
                $this->setErrorMessage('danger','Maximum Number of featured Sellers reached');
                redirect(base_url().'featured');
            }
        }else{
            $this->setErrorMessage('danger','Already plan subscribed!!');
            redirect(base_url().'featured');                
        }            
    }
    public function paypal(){
            $this->load->model('seller_plan_model','seller_plan');
            $this->data['plan'] = $this->seller_plan->get_all_details(SELLER_PLAN,array());
            $this->load->view('site/user/featured_seller_paypal',$this->data);         
    }
    public function paypal_payment(){
            $this->load->model('seller_plan_model','seller_plan');
            $listCheck = $this->seller_plan->get_all_details(FEATURED_SELLER,array('userId' => $this->checkLogin('U'),'isExpired'=>'0'));
            if($listCheck->num_rows == 0){
                    $plan = $this->seller_plan->get_all_details(SELLER_PLAN,array());
                    $registeredSellers = $this->seller_plan->get_all_details(FEATURED_SELLER,array('isExpired'=>'0'));
                    if($registeredSellers->num_rows < $plan->row()->plan_promoted_count){
                        $time = time();
                        $datestring = "%Y-%m-%d %h:%i:%s";                       
                        $dataArr = array('userId'=>$this->checkLogin('U'),'planPrice'=>$plan->row()->plan_price,'planPeriod'=>$plan->row()->plan_period,'productCount'=>$plan->row()->plan_product_count,'paymentType'=>'Paypal');
                        $dataArr['createdOn'] = mdate($datestring,$time); 
                        $this->seller_plan->simple_insert('fc_featured_seller_temp',$dataArr);
                        $temp_id = $this->db->insert_id();
                        $this->load->library('paypal_class');

                        $item_name = $this->config->item('email_title').' Products';

                        $totalAmount = $this->input->post('total_price');
                        //User ID
                        $loginUserId = $this->checkLogin('U');
                        //DealCodeNumber
                        //$lastFeatureInsertId = $this->session->userdata('randomNo');

                        $quantity = 1;

                        if($this->input->post('paypalmode') == 'sandbox'){
                                $this->paypal_class->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';   // testing paypal url
                        }else{
                                $this->paypal_class->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';     // paypal url
                        }

                        $this->paypal_class->add_field('currency_code', $this->data['currencyType']);

                        $this->paypal_class->add_field('business',$this->input->post('paypalEmail')); // Business Email

                        $this->paypal_class->add_field('return',base_url().'site/user_settings/paypal_return/'.$temp_id); // Return URL

                        $this->paypal_class->add_field('cancel_return', base_url().'site/user_settings/paypal_cancel/'.$temp_id); // Cancel URL

                        $this->paypal_class->add_field('notify_url', base_url().'site/user_settings/paypal_notify/'.$temp_id); // Notify url

                        $this->paypal_class->add_field('custom', $loginUserId.'|'.$temp_id.'|Product'); // Custom Values			

                        $this->paypal_class->add_field('item_name', $item_name); // Product Name

                        $this->paypal_class->add_field('user_id', $loginUserId);

                        $this->paypal_class->add_field('quantity', $quantity); // Quantity
                        //echo $totalAmount;die;
                          $this->paypal_class->add_field('amount', $totalAmount); // Price
                        //$this->paypal_class->add_field('amount', 1); // Price

            //			echo base_url().'order/success/'.$loginUserId.'/'.$lastFeatureInsertId; die;

                        $this->paypal_class->submit_paypal_post();                              
                        }else{
                            $this->setErrorMessage('danger','Maximum Number of featured Sellers reached');
                            redirect(base_url().'featured');                            
                        }           
            }else{
                        $this->setErrorMessage('danger','Already plan subscribed!!');
                        redirect(base_url().'featured');                  
            }
    }
    public function paypal_return($id){
        if(strtolower($this->input->post('payment_status')) == 'completed'){
        $this->load->model('seller_plan_model','seller_plan');
        $temp_store = $this->seller_plan->get_all_details('fc_featured_seller_temp',array('id'=>$id));
        if($temp_store->num_rows > 0){
            $this->seller_plan->commonDelete('fc_featured_seller_temp',array('id'=>$id));
            $userExists = $this->seller_plan->get_all_details(FEATURED_SELLER,array('userId' => $temp_store->row()->userId,'isExpired'=>'0'));
            if($userExists->num_rows == 0){
                $dataArr = array('userId'=>$temp_store->row()->userId,'planPrice'=>$temp_store->row()->planPrice,'planPeriod'=>$temp_store->row()->planPeriod,'productCount'=>$temp_store->row()->productCount,'paymentType'=>$temp_store->row()->paymentType);
                $time = time();
                $datestring = "%Y-%m-%d %h:%i:%s";
                $dataArr['featuredOn'] = mdate($datestring,$time);
                $dataArr['expiresOn'] = date("Y-m-d h:i:s", strtotime($dataArr['featuredOn'].'+ '.$temp_store->row()->planPeriod.' days'));                
                $dataArr['createdOn'] = mdate($datestring,$time);
                $this->seller_plan->simple_insert(FEATURED_SELLER,$dataArr); 
                $this->seller_plan->commonDelete('fc_featured_seller_temp',array('id'=>$id));
                $this->setErrorMessage('success','Payment Successfull');
            }            
        }
        }else{
            $this->setErrorMessage('danger','Payment Unsuccessfull');
        }redirect(base_url().'featured');
    }
    public function paypal_cancel($id){
        $this->load->model('seller_plan_model','seller_plan');
        $temp_store = $this->seller_plan->get_all_details('fc_featured_seller_temp',array('id'=>$id));
        if($temp_store->num_rows > 0){
            $this->seller_plan->commonDelete('fc_featured_seller_temp',array('id'=>$id));
        }
        $this->setErrorMessage('danger','Payment Cancelled');
        redirect(base_url().'featured');
    }
    public function paypal_notify($id){
        $this->load->model('seller_plan_model','seller_plan');
        $temp_store = $this->seller_plan->get_all_details('fc_featured_seller_temp',array('id'=>$id));
        if($temp_store->num_rows > 0){
            $this->seller_plan->commonDelete('fc_featured_seller_temp',array('id'=>$id));
            $userExists = $this->seller_plan->get_all_details(FEATURED_SELLER,array('userId' => $temp_store->row()->userId,'isExpired'=>'0'));
            if($userExists->num_rows == 0){
                $dataArr = array('userId'=>$temp_store->row()->userId,'planPrice'=>$temp_store->row()->planPrice,'planPeriod'=>$temp_store->row()->planPeriod,'productCount'=>$temp_store->row()->productCount);
                $time = time();
                $datestring = "%Y-%m-%d %h:%i:%s";
                $dataArr['featuredOn'] = mdate($datestring,$time);
                $dataArr['expiresOn'] = date("Y-m-d h:i:s", strtotime($dataArr['featuredOn'].'+ '.$temp_store->row()->planPeriod.' days'));                
                $dataArr['createdOn'] = mdate($datestring,$time);
                $this->seller_plan->simple_insert(FEATURED_SELLER,$dataArr); 
                $this->seller_plan->commonDelete('fc_featured_seller_temp',array('id'=>$id));
                $this->setErrorMessage('success','Payment Successfull');
            }            
        }
        redirect(base_url().'featured');
    }
/****Featured sellers*******/    
}

/* End of file user_settings.php */
/* Location: ./application/controllers/site/user_settings.php */