<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * Landing page functions
 * @author Teamtweaks
 *
 */

class Badges extends MY_Controller {
  function __construct(){
		parent::__construct(); 
		$this->load->helper(array('cookie','date','form','email'));
		$this->load->library(array('encrypt','form_validation'));
		$this->load->model('badges_model');
		$this->load->model('product_model');
		$this->load->model('category_model');$this->load->model('user_model');
		if($_SESSION['sMainCategories'] == ''){
			$sortArr1 = array('field'=>'cat_position','type'=>'asc');
			$sortArr = array($sortArr1);
			$_SESSION['sMainCategories'] = $this->badges_model->get_all_details(CATEGORY,array('rootID'=>'0','status'=>'Active'),$sortArr);
		}
		$this->data['mainCategories'] = $_SESSION['sMainCategories'];
		if($_SESSION['sColorLists'] == ''){
			$_SESSION['sColorLists'] = $this->badges_model->get_all_details(LIST_VALUES,array('list_id'=>'1'));
		}
		$this->data['mainColorLists'] = $_SESSION['sColorLists'];
		$this->data['loginCheck'] = $this->checkLogin('U');
		//		echo $this->session->userdata('fc_session_user_id');die;
		$this->data['likedProducts'] = array();
		if ($this->data['loginCheck'] != ''){
			$this->data['likedProducts'] = $this->badges_model->get_all_details(PRODUCT_LIKES,array('user_id'=>$this->checkLogin('U')));
		}
			
	}
	public function index(){
	   if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
		
		    $Query = "select b.*,n.* from ".BADGES." b join ".NOTIFICATIONS." n on n.activity_id = b.id where n.activity = 'badges' and n.user_id= ".$this->checkLogin('U')."";
		    $this->data["badgesList"] = $this->badges_model->ExecuteQuery($Query);
			
		    $userProfileDetails = $this->badges_model->get_all_details(USERS,array('id'=>$this->checkLogin('U'),'status'=>'Active'));
			if ($userProfileDetails->num_rows()==1){
			    $this->data['userProfileDetails'] = $userProfileDetails;
				$this->data['recentActivityDetails'] = $this->user_model->get_activity_details($userProfileDetails->row()->id);
				if ($userProfileDetails->row()->full_name != ''){
					$this->data['heading'] = $userProfileDetails->row()->full_name;
				}else {
					$this->data['heading'] = $userProfileDetails->row()->user_name;
				}
			}
       $this->load->view('site/user/display_user_badges',$this->data);
		}
	}
	
}