<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * User Settings related functions
 * @author Teamtweaks
 *
 */
 
class Groupgift extends MY_Controller {
function __construct(){
	parent::__construct();
	$this->load->helper(array('cookie','date','form','email'));
	$this->load->library(array('encrypt','form_validation','pagination'));
	$this->load->model('groupgift_model','group_gift');
	if($_SESSION['sMainCategories'] == ''){
		$sortArr1 = array('field'=>'cat_position','type'=>'asc');
		$sortArr = array($sortArr1);
		$_SESSION['sMainCategories'] = $this->group_gift->get_all_details(CATEGORY,array('rootID'=>'0','status'=>'Active'),$sortArr);
	}
	$this->data['mainCategories'] = $_SESSION['sMainCategories'];
	if($_SESSION['sColorLists'] == ''){
		$_SESSION['sColorLists'] = $this->group_gift->get_all_details(LIST_VALUES,array('list_id'=>'1'));
	}
	$this->data['mainColorLists'] = $_SESSION['sColorLists'];
	$this->data['loginCheck'] = $this->checkLogin('U');
	$this->data['likedProducts'] = array();
	if($this->data['loginCheck'] != ''){
	   $this->data['likedProducts'] = $this->group_gift->get_all_details(PRODUCT_LIKES,array('user_id'=>$this->checkLogin('U')));
	}
	define("API_LOGINID",$this->config->item('payment_2'));
}
public function index(){
	if ($this->checkLogin('U')==''){
		redirect(base_url().'login');
	}else {
		$this->data['heading'] = 'Group Gifts';
		$this->data['groupgiftList'] = $this->group_gift->get_groupgift_list($this->checkLogin('U'));
		if($this->data['groupgiftList']->num_rows() >0){
		   foreach($this->data['groupgiftList']->result() as $_groupgiftlist){
               $this->data['contributeDetails'][$_groupgiftlist->id] = $this->group_gift->get_all_details(GROUPGIFT_PAYMENT,array('groupgift_id'=>$_groupgiftlist->id));
		   }
		}
		$this->load->view('site/groupgifts/groupgifts_list',$this->data);
	} 
}
public function InsertProduct(){
	if($this->checkLogin('U')!=''){
		$country_list = $this->group_gift->get_all_details(COUNTRY_LIST,array('status'=>'Active','country_code'=>$this->input->post('country')));
		if($country_list->num_rows() ==1){
			$country_values = $country_list->row()->shipping_cost.'+'.$country_list->row()->shipping_tax.'+'.$country_list->row()->country_code;
		}
		if($this->input->post('seller_id') ==''){
			$gistseller_id = 0;
		}else{
			$giftseller_id = $this->input->post('seller_id');
		}
		$seller_id = $this->group_gift->get_all_details(GROUP_GIFTS,array('gift_seller_id'=>$giftseller_id));
		if($seller_id->num_rows() ==1){
			if($seller_id->row()->gift_name == ''){
				$excludeArr =array('seller_id','url','ship_value','recipient_image','gift_seller_id','long_url','fb_image','sug_img');
				if($this->input->post('sug_img')!=''){
				  $file_name = $this->input->post('sug_img');
				}elseif($this->input->post('fb_image')!=''){
					$picturtmp_name = 'fb-'.$this->input->post('recipient_name');
					$profile_Image = $this->input->post('fb_image');
					$file_name = $picturtmp_name.'.jpg';
					$savepath = DESKTOPURL.'images/groupgifts/';
					//$thumb_image = file_get_contents($profile_Image);
					$thumb_file = $savepath . $file_name;
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
					curl_setopt($ch, CURLOPT_HEADER, false);
					curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
					curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.52 Safari/537.17');
					curl_setopt($ch, CURLOPT_URL, $profile_Image);
					curl_setopt($ch, CURLOPT_REFERER, $profile_Image);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); //Set curl to return the data instead of printing it to the browser.
					$thumb_image = curl_exec($ch);
					curl_close($ch);
					file_put_contents($thumb_file, $thumb_image);
				}else{
					$config['overwrite'] = FALSE;
					$config['allowed_types'] = 'jpg|jpeg|gif|png';
					$config['upload_path'] = '../images/groupgifts';
					$this->load->library('upload', $config);
					if($this->upload->do_upload('recipient_image')){
						$upload_data = $this->upload->data(); 
						$file_name = $upload_data['file_name'];	
				    }
				}
				if($file_name!=''){
					$dataArr = array('status' => 'Active','recipient_image'	=>$file_name);
				}else{
				    $current_date = strtotime(date('Y-m-d'));
					$expired = strtotime("+".$this->config->item('expiry_days')." day", $current_date);
					$ecpired_date = date('Y-m-d', $expired);
					$dataArr = array('status' => 'Active','expired_date'=>$ecpired_date);
				}    
				$this->group_gift->commonInsertUpdate(GROUP_GIFTS,'update',$excludeArr,$dataArr,array('gift_seller_id'=>$seller_id->row()->gift_seller_id)); 

				echo $seller_id->row()->recipient_name.'+'.$seller_id->row()->address1.','.$seller_id->row()->address2.'+'.$seller_id->row()->city.'+'.$seller_id->row()->country.'+'.$this->input->post('gift_name').'+'.$this->input->post('gift_description').'+'.$seller_id->row()->gift_seller_id.'+'.$country_values;
			}		   
		}else{
			$excludeArr = array('recipient_image','gift_seller_id','seller_id','long_url','fb_image','sug_img');
			if($this->input->post('sug_img')!=''){
				  $file_name = $this->input->post('sug_img');
			}elseif($this->input->post('fb_image')!=''){
				$picturtmp_name = 'fb-'.$this->input->post('recipient_name');
				$profile_Image = $this->input->post('fb_image');
				$file_name = $picturtmp_name.'.jpg';
				$savepath = DESKTOPURL.'images/groupgifts/';
				//$thumb_image = file_get_contents($profile_Image);
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
				curl_setopt($ch, CURLOPT_HEADER, false);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
				curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.52 Safari/537.17');
				curl_setopt($ch, CURLOPT_URL, $profile_Image);
				curl_setopt($ch, CURLOPT_REFERER, $profile_Image);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); //Set curl to return the data instead of printing it to the browser.
				$thumb_image = curl_exec($ch);
				curl_close($ch);
				$thumb_file = $savepath . $file_name;
				file_put_contents($thumb_file, $thumb_image);
			}else{
				$config['overwrite'] = FALSE;
				$config['allowed_types'] = 'jpg|jpeg|gif|png';
				$config['upload_path'] = '../images/groupgifts';
				$this->load->library('upload', $config);
				if($this->upload->do_upload('recipient_image')){
					$upload_data = $this->upload->data(); 
					$file_name = $upload_data['file_name'];	
				}
			}		
			$gift_seller_id = mktime();
			$get_seller_id = $this->group_gift->get_all_details(GROUP_GIFTS,array('gift_seller_id'=>$gift_seller_id));
			while($get_seller_id->num_rows()>0){
				$gift_seller_id = mktime();
			}
			$check_url = $this->group_gift->get_all_details(SHORTURL,array('product_id'=>$gift_seller_id,'user_id'=>$this->checkLogin('U')));
			if($check_url->num_rows()>0){
				$short_url = $check_url->row()->short_url;
			}else{
				$short_url = $this->get_rand_str('6');
				$checkId = $this->group_gift->get_all_details(SHORTURL,array('short_url'=>$short_url));
				while($checkId->num_rows()>0){
					$short_url = $this->get_rand_str('6');
					$checkId = $this->group_gift->get_all_details(SHORTURL,array('short_url'=>$short_url));
				}
				$url = $this->input->post('long_url').$gift_seller_id;
				$this->group_gift->simple_insert(SHORTURL,array('short_url'=>$short_url,'long_url'=>$url,'product_id'=>$gift_seller_id,'user_id'=>$this->checkLogin('U')));
				$get_url = $this->group_gift->get_all_details(SHORTURL,array('short_url'=>$short_url,'long_url'=>$url,'product_id'=>$gift_seller_id,'user_id'=>$this->checkLogin('U')));
				if($get_url->num_rows() >0){
					$short_url_id = $get_url->row()->id; 
				}
			}
			if($file_name!=''){
				$dataArr = array('user_id'=>$this->checkLogin('U'),'gift_seller_id'=>$gift_seller_id,
				'status' =>'Inactive','short_url_id'=>$short_url_id,'recipient_image'=>$file_name);
			}else{
				$dataArr = array('user_id'=>$this->checkLogin('U'),'gift_seller_id' =>$gift_seller_id,
				'status' =>'Inactive','short_url_id'=>$short_url_id,);
			}
			$this->group_gift->commonInsertUpdate(GROUP_GIFTS,'insert',$excludeArr,$dataArr,array());
			$product_details = $this->group_gift->get_all_details(PRODUCT,array('id'=>$this->input->post('product_id')));
			if($product_details->num_rows()>0){
				$values = $country_values.'+'.$gift_seller_id;
				echo $values; 				
			}
		}
	}else{
		redirect('login');
	}
}
public function updateGrouptProduct(){
	if($this->input->post('cancelled')== ''){
		$excludeArr = array('seller_id');
		$dataArr =array();
		$this->group_gift->commonInsertUpdate(GROUP_GIFTS,'update',$excludeArr,$dataArr,array('gift_seller_id'=>$this->input->post('seller_id')));
	}else{
		$excludeArr = array('group_id','cancelled');
		$dataArr =array('status'=>'Cancelled');
		$this->group_gift->commonInsertUpdate(GROUP_GIFTS,'update',$excludeArr,$dataArr,array('gift_seller_id'=>$this->input->post('group_id')));
	}
}
public function search_user(){
	if($this->checkLogin('U')!=''){
		$search = $this->input->post('search');
		$Query = "SELECT * FROM ".USERS." WHERE user_name LIKE '%$search%' AND status ='Active'";
		$search_result = $this->group_gift->ExecuteQuery($Query);
		if($search_result->num_rows() >0){
			foreach($search_result->result() as $result){
				$imgArr = array_filter(explode(',', $result->thumbnail));
				$img = 'user-thumb1.png';
				foreach($imgArr as $imgVal){
					if($imgVal != ''){
						$img = $imgVal;
						break;
					}
				}
				echo '<div class="show" align="left">';
				echo '<span class="name"><img width="40" height="40" src="'.DESKTOPURL.'images/users/'.$img.'"/>'.$result->user_name.'</span>&nbsp;<br/></div>';
			}
		}
	}else{
		redirect('login');
	}
}
public function get_user_result(){
	if($this->checkLogin('U')!=''){
		$username = trim($this->input->post('user_name'));
		$Query = "SELECT * FROM ".USERS." WHERE user_name = '$username' AND status ='Active'";
		$get_user_details = $this->group_gift->ExecuteQuery($Query);
		if($get_user_details->num_rows() ==1){
			$values = $get_user_details->row()->full_name.'+'.$get_user_details->row()->user_name.'+'.$get_user_details->row()->thumbnail.'+'.$get_user_details->row()->address.'+'.$get_user_details->row()->address2.'+'.$get_user_details->row()->city.'+'.$get_user_details->row()->state.'+'.$get_user_details->row()->country.'+'.$get_user_details->row()->postal_code.'+'.$get_user_details->row()->phone_no;
			echo $values;
		} 
	}else{
		redirect('login');
	}
}
public function display_groupProduct_detail(){
	if($this->checkLogin('U')!=''){
		$p_id = $this->uri->segment(2,0);
		$Query = "select gf.*,p.id as pid,p.seller_product_id,p.product_name,p.seourl,p.likes,p.image,p.description,u.user_name,u.thumbnail,st.short_url from ".GROUP_GIFTS." as gf join ".PRODUCT." as p on (gf.product_id = p.id) join ".USERS." as u on (gf.user_id = u.id) join ".SHORTURL." as st on (st.id = gf.short_url_id) where gf.gift_seller_id = ".$p_id."";
		$gift_ProductDetails = $this->group_gift->ExecuteQuery($Query);
		//echo "<pre>";print_r($gift_ProductDetails->result()); die;
		if($gift_ProductDetails->num_rows() ==1){
			$this->data['gift_ProductDetails'] = $gift_ProductDetails;
			$condition = array('user_id'=> $this->checkLogin('U'),'group_product_id'=> $gift_ProductDetails->row()->gift_seller_id);
			$group_product_count = $this->group_gift->get_all_details(GROUPGIFT_COMMENTS,$condition);
			$this->data['group_count'] = $group_product_count;
			$Query = "select gf.*,u.full_name,u.user_name,u.thumbnail,c.comments ,u.email,c.id,c.status,c.user_id as CUID from ".GROUPGIFT_COMMENTS." c LEFT JOIN ".USERS." u on u.id=c.user_id LEFT JOIN ".GROUP_GIFTS." gf on gf.gift_seller_id=c.group_product_id where c.user_id = ".$this->checkLogin('U')." AND c.group_product_id = ".$gift_ProductDetails->row()->gift_seller_id."";
			$this->data['giftproductComment'] = $this->group_gift->ExecuteQuery($Query);
			
		}
		$contribute_details= $this->group_gift->get_all_details(GROUPGIFT_PAYMENT,array('groupgift_id'=>$gift_ProductDetails->row()->id));
		$this->data['contribute_details'] = $contribute_details;
		$cond = "select * from ".GROUPGIFT_SETTINGS." where id = '1'";
		$this->data['expiry_date'] = $this->group_gift->ExecuteQuery($cond);
		
		$this->data['heading'] = 'Group Gifts Product';
		$this->load->view('site/groupgifts/groupgifts_product_detail',$this->data);
	}else{
		redirect('login');
	}
}
public function payment_contribute_details(){
    if($this->checkLogin('U')!=''){
		$p_id = $this->uri->segment(3,0);
		$Query = "select gf.*,p.id as pid,p.seller_product_id,p.product_name,p.seourl,p.likes,p.image,p.description,u.user_name,u.thumbnail,u.email from ".GROUP_GIFTS." as gf join ".PRODUCT." as p on (gf.product_id = p.id) join ".USERS." as u on (gf.user_id = u.id) where gf.gift_seller_id = ".$p_id."";
		$gift_ProductDetails = $this->group_gift->ExecuteQuery($Query);
		//echo"<pre>"; print_r($gift_ProductDetails->result()); die;
		if($gift_ProductDetails->num_rows() ==1){
			$this->data['gift_ProductDetails'] = $gift_ProductDetails;
		}
		$contribute_details= $this->group_gift->get_all_details(GROUPGIFT_PAYMENT,array('groupgift_id'=>$gift_ProductDetails->row()->id));
		$this->data['contribute_details'] = $contribute_details;
		$cond = "select * from ".GROUPGIFT_SETTINGS." where id = '1'";
		$this->data['expiry_date'] = $this->group_gift->ExecuteQuery($cond);
		$this->data['country_list'] = $this->group_gift->get_all_details(COUNTRY_LIST,array('status'=>'Active'));
		$this->data['heading'] = 'Group Gift Contrbutes';
		$this->load->view('site/groupgifts/contribute_detail',$this->data);
	}else{
		redirect('login');
	}
}
public function groupgift_comment(){
	if($this->checkLogin('U')!=''){
		$excludeArr = array();
		$dataArr = array('user_id' =>$this->checkLogin('U'),'Status' => "Inactive");
		$this->group_gift->commonInsertUpdate(GROUPGIFT_COMMENTS,'insert',$excludeArr,$dataArr,array());
		echo $return['value'] = 1;
	}else{
		redirect('login');  
	}
}
public function approve_comment(){
	$returnStr['status_code'] = 0;
	if ($this->checkLogin('U')!=''){
		$cid = $this->input->post('cid');
		$this->group_gift->update_details(GROUPGIFT_COMMENTS,array('status'=>'Active'),array('id'=>$cid));
		$returnStr['status_code'] = 1;
	}
	echo json_encode($returnStr);
}	
public function delete_comment(){
	$returnStr['status_code'] = 0;
	if ($this->checkLogin('U')!=''){
		$cid = $this->input->post('cid');
		$this->group_gift->commonDelete(GROUPGIFT_COMMENTS,array('id'=>$cid));
		$returnStr['status_code'] = 1;
	}
	echo json_encode($returnStr);
}
public function load_short_url(){
	$short_url = $this->uri->segment(2,0);
	if ($short_url != ''){
		$url_details = $this->group_gift->get_all_details(GROUPGIFT_SHORTURL,array('short_url'=>$short_url));
		if ($url_details->num_rows()==1){
			redirect($url_details->row()->long_url);
		}else {
			show_error('Invalid short url provided. Make sure the url you typed is correct. <a href="'.base_url().'">Click here</a> to go home page.','404','Invalid Url');
		}
	}
}
public function getCountry(){
	$country_list = $this->group_gift->get_all_details(COUNTRY_LIST,array('status'=>'Active'));
	if($country_list->num_rows() >0){
		foreach($country_list->result() as $country){
			if($country->country_code == 'US'){
				echo "<option value =".$country->country_code." selected>".$country->name."</option>";
			}else{
				echo "<option value =".$country->country_code.">".$country->name."</option>";
			} 
		} 
	}
}
public function suggest_friends(){
	if($this->checkLogin('U')!=''){
	    $following_id = $this->data['userDetails']->row()->following;
		$ids = array_filter(explode(',',$following_id));
		$limit =0;
		foreach($ids as $id){
		   if($limit>=2){
		     break;
		   }
		   $Query = "select * from ".USERS." where id =".$id." AND status='Active'";
		   $sug_user_Details = $this->group_gift->ExecuteQuery($Query);
		   if($sug_user_Details->num_rows()==1){
		       if($sug_user_Details->row()->thumbnail!=''){
			      $user_image = $sug_user_Details->row()->thumbnail;
			   }else{
			      $user_image = "user-thumb1.png";
			   }
			   $html = '<li class="inner_suggested_friends" id="'.$limit.'" onclick="sug_fri_li(this.id);">';
			   $html .= '<img id="img-'.$limit.'" src="'.DESKTOPURL.'images/users/'.$user_image.'"/><br/>';
			   $html .= '<span id="user-'.$limit.'">'.$sug_user_Details->row()->user_name.'</span>';
			   $html .= '<input id="image-'.$limit.'" type="hidden" name="sug_img" value="'.$user_image.'">';
			   $html .='</li>';
			   echo $html;
		   }
           $limit++;		   
		}
	}else{
		redirect('login');
	}
}
public function PaymentCredit(){
    
	$loginUserId = $this->checkLogin('U');
	$groupgift_id = $this->input->post('groupgift_id');
	$product_id = $this->input->post('product_id');
	$gift_details= $this->group_gift->get_all_details(GROUP_GIFTS,array('id'=>$groupgift_id));
	$contribute_details= $this->group_gift->get_all_details(GROUPGIFT_PAYMENT,array('groupgift_id'=>$groupgift_id));
	if($contribute_details >0){
	$contribute_amount = 0;
		foreach($contribute_details->result() as $_contributedetails){
			  $contribute_amount += $_contributedetails->amount; 
		}
		$remain_amount = $gift_details->row()->gift_total - $contribute_amount;
		if($this->input->post('total_price') > $remain_amount){
			$this->setErrorMessage('error','Amount Exceed the Total Amount');
			redirect('gifts/contribute/'.$gift_details->row()->gift_seller_id);
		}else{
					if($this->input->post('creditvalue')=='authorize'){
				$Auth_Details=unserialize(API_LOGINID);
				$Auth_Setting_Details=unserialize($Auth_Details['settings']);
				 
				define("AUTHORIZENET_API_LOGIN_ID",$Auth_Setting_Details['Login_ID']);
				define("AUTHORIZENET_TRANSACTION_KEY",$Auth_Setting_Details['Transaction_Key']);
				define("API_MODE",$Auth_Setting_Details['mode']);
				if(API_MODE	=='sandbox'){
					define("AUTHORIZENET_SANDBOX",true);
				}else{
					define("AUTHORIZENET_SANDBOX",false);
				}
				define("TEST_REQUEST", "FALSE");
				require_once './authorize/AuthorizeNet.php';
				$transaction = new AuthorizeNetAIM;
				$transaction->setSandbox(AUTHORIZENET_SANDBOX);
				$transaction->setFields(
					array(
					'amount' =>  $this->input->post('total_price'), 
					'card_num' =>  $this->input->post('cardNumber'), 
					'exp_date' => $this->input->post('CCExpDay').'/'.$this->input->post('CCExpMnth'),
					'first_name' => $this->input->post('full_name'),
					'last_name' => '',
					'address' => $this->input->post('address1').$this->input->post('address2'),
					'city' => $this->input->post('city'),
					'state' => $this->input->post('state'),
					'country' => $this->input->post('country'),
					'phone' => '',
					'email' =>  $this->input->post('email'),
					'card_code' => $this->input->post('creditCardIdentifier'),
					)
				);
				$response = $transaction->authorizeAndCapture();
				if($response->approved ){
				   $excludeArr = array();
				   if($response->transaction_id !=''){
					  $dataArr = array(
						  'payment_type'=> "Credit Card", 'contributor_name'=> $response->first_name, 
						  'amount'=> $response->amount, 'email'=> $response->email_address, 'status'=> "Paid",
						  'transaction_id'=> $response->transaction_id, 'country'=> $response->country,
						  'address'=> $response->address, 'city'=> $response->city,'state'=> $response->state, 
						  'groupgift_id'=> $groupgift_id,'user_id'=>$this->checkLogin('U')	 		  
					  );
				   $this->group_gift->simple_insert(GROUPGIFT_PAYMENT,$dataArr);
				   
				   redirect('gifts/'.$product_id);
				}}else{
					redirect('site/groupgift/failure/'.$response->response_reason_text.'/'.$product_id);
				}
			}
		}
	}elseif($this->input->post('total_price') > $gift_details->row()->gift_total){
		$this->setErrorMessage('error','Amount Exceed the Total Amount');
		redirect('gifts/contribute/'.$gift_details->row()->gift_seller_id);
	}else{								
	if($this->input->post('creditvalue')=='authorize'){
		$Auth_Details=unserialize(API_LOGINID);
		$Auth_Setting_Details=unserialize($Auth_Details['settings']);
		 
		define("AUTHORIZENET_API_LOGIN_ID",$Auth_Setting_Details['Login_ID']);
		define("AUTHORIZENET_TRANSACTION_KEY",$Auth_Setting_Details['Transaction_Key']);
		define("API_MODE",$Auth_Setting_Details['mode']);
		if(API_MODE	=='sandbox'){
			define("AUTHORIZENET_SANDBOX",true);
		}else{
			define("AUTHORIZENET_SANDBOX",false);
		}
		define("TEST_REQUEST", "FALSE");
		require_once './authorize/AuthorizeNet.php';
		$transaction = new AuthorizeNetAIM;
		$transaction->setSandbox(AUTHORIZENET_SANDBOX);
		$transaction->setFields(
			array(
			'amount' =>  $this->input->post('total_price'), 
			'card_num' =>  $this->input->post('cardNumber'), 
			'exp_date' => $this->input->post('CCExpDay').'/'.$this->input->post('CCExpMnth'),
			'first_name' => $this->input->post('full_name'),
			'last_name' => '',
			'address' => $this->input->post('address1').$this->input->post('address2'),
			'city' => $this->input->post('city'),
			'state' => $this->input->post('state'),
			'country' => $this->input->post('country'),
			'phone' => '',
			'email' =>  $this->input->post('email'),
			'card_code' => $this->input->post('creditCardIdentifier'),
			)
		);
		$response = $transaction->authorizeAndCapture();
		if($response->approved ){
		   $excludeArr = array();
		   if($response->transaction_id !=''){
			  $dataArr = array(
				  'payment_type'=> "Credit Card", 'contributor_name'=> $response->first_name, 
				  'amount'=> $response->amount, 'email'=> $response->email_address, 'status'=> "Paid",
				  'transaction_id'=> $response->transaction_id, 'country'=> $response->country,
				  'address'=> $response->address, 'city'=> $response->city,'state'=> $response->state, 
				  'groupgift_id'=> $groupgift_id,'user_id'=>$this->checkLogin('U')	 		  
			  );
			$this->group_gift->simple_insert(GROUPGIFT_PAYMENT,$dataArr);
			$newsid='23';
			$template_values=$this->user_model->get_newsletter_template_details($newsid);
			$adminnewstemplateArr=array('email_title'=> $this->config->item('email_title'),'logo'=> $this->data[  'logo']);
			extract($adminnewstemplateArr);
			$subject = 'From: '.$this->config->item('email_title').' - '.$template_values['news_subject'];
			$message .= '<!DOCTYPE HTML>
				<html>
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
				<meta name="viewport" content="width=device-width"/>
				<title>'.$template_values['news_subject'].'</title>
				<body>';
			include('./newsletter/registeration'.$newsid.'.php');
			$message .= '</body>
				</html>';
			if($template_values['sender_name']=='' && $template_values['sender_email']==''){
				$sender_email=$this->config->item('site_contact_mail');
				$sender_name=$this->config->item('email_title');
			}else{
				$sender_name=$template_values['sender_name'];
				$sender_email=$template_values['sender_email'];
			}
			$email_values = array('mail_type'=>'html',
				'from_mail_id'=>$sender_email,
				'mail_name'=>$sender_name,
				'to_mail_id'=>$response->email_address,
				'subject_message'=>'Payment are Successfully Send',
				'body_messages'=>$message,
				'mail_id'=>'forgot'
			);
		   $email_send_to_common = $this->group_gift->common_email_send($email_values);
		   redirect('gifts/'.$product_id);
		}}else{
			redirect('site/groupgift/failure/'.$response->response_reason_text.'/'.$product_id);
		}
	}}
}
public function RefundPaymentCredit(){
   $groupgift_id = $this->uri->segment(2);
   $getContributors = $this->group_gift->get_all_details(GROUPGIFT_PAYMENT,array('groupgift_id'=>$groupgift_id));
   if($getContributors->num_rows() >0){
	   foreach($getContributors->result() as $get_Contributors){
	      $transaction_id = $get_Contributors->transaction_id;
		  $amount = $get_Contributors->amount;
		  $email = $get_Contributors->email;
		  $Auth_Details=unserialize(API_LOGINID);
			$Auth_Setting_Details=unserialize($Auth_Details['settings']);
			 
			define("AUTHORIZENET_API_LOGIN_ID",$Auth_Setting_Details['Login_ID']);
			define("AUTHORIZENET_TRANSACTION_KEY",$Auth_Setting_Details['Transaction_Key']);
			define("API_MODE",$Auth_Setting_Details['mode']);
			if(API_MODE	=='sandbox'){
				define("AUTHORIZENET_SANDBOX",true);
			}else{
				define("AUTHORIZENET_SANDBOX",false);
			}
			define("TEST_REQUEST", "FALSE");
			require_once './authorize/AuthorizeNet.php';
			$transaction = new AuthorizeNetAIM;
			$transaction->setSandbox(AUTHORIZENET_SANDBOX);
			$transaction->setFields(
				array(
				'type' => 'refundTransaction',
				'amount' =>  $amount, 
				'card_num' =>  '4111111111111111', 
				'exp_date' => '122025',
				'email' =>  $email,
				'tran_key' => $transaction_id, 
				)
			);
			$response = $transaction->authorizeAndCapture();
			if($response->approved ){
			    echo "<pre>"; print_r($response);
				$newsid='24';
				$template_values=$this->user_model->get_newsletter_template_details($newsid);
				$adminnewstemplateArr=array('email_title'=> $this->config->item('email_title'),'logo'=> $this->data[  'logo']);
				extract($adminnewstemplateArr);
				$subject = 'From: '.$this->config->item('email_title').' - '.$template_values['news_subject'];
				$message .= '<!DOCTYPE HTML>
					<html>
					<head>
					<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
					<meta name="viewport" content="width=device-width"/>
					<title>'.$template_values['news_subject'].'</title>
					<body>';
				include('./newsletter/registeration'.$newsid.'.php');

				$message .= '</body>
				</html>';
				if($template_values['sender_name']=='' && $template_values['sender_email']==''){
					$sender_email=$this->config->item('site_contact_mail');
					$sender_name=$this->config->item('email_title');
				}else{
					$sender_name=$template_values['sender_name'];
					$sender_email=$template_values['sender_email'];
				}
				$email_values = array('mail_type'=>'html',
					'from_mail_id'=>$sender_email,
					'mail_name'=>$sender_name,
					'to_mail_id'=>$response->email_address,
					'subject_message'=>'Payment are Successfully Send',
					'body_messages'=>$message,
					'mail_id'=>'forgot'
				);
				$email_send_to_common = $this->group_gift->common_email_send($email_values);
			}else{
				redirect('site/groupgift/failure/'.$response->response_reason_text.'/'.$product_id);
			}
	   }
   }
   
}
public function failure(){
    
	$this->data['Confirmation'] = 'Failure';
	$this->data['errors'] = $this->uri->segment(4);
	$this->load->view('site/groupgifts/failure',$this->data);
}
public function update_expire(){
	 $gift_seller_id = $this->input->post('ids');
     $excludeArr = array('ids');
     $dataArr =array();
	 $this->group_gift->commonInsertUpdate(GROUP_GIFTS,'update',$excludeArr,$dataArr,array('id'=>$this->input->post('id')));
	 redirect('gifts/'.$gift_seller_id);
}	
}
/* End of file user_settings.php */
/* Location: ./application/controllers/site/user_settings.php */