<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * User related functions
 * @author Teamtweaks
 *
 */

class Order extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper(array('cookie','date','form','email'));
		$this->load->library(array('encrypt','form_validation'));
		$this->load->model('order_model');
		$this->load->model('product_model');
		if($_SESSION['sMainCategories'] == ''){
			$sortArr1 = array('field'=>'cat_position','type'=>'asc');
			$sortArr = array($sortArr1);
			$_SESSION['sMainCategories'] = $this->order_model->get_all_details(CATEGORY,array('rootID'=>'0','status'=>'Active'),$sortArr);
		}
		$this->data['mainCategories'] = $_SESSION['sMainCategories'];

		if($_SESSION['sColorLists'] == ''){
			$_SESSION['sColorLists'] = $this->order_model->get_all_details(LIST_VALUES,array('list_id'=>'1'));
		}
		$this->data['mainColorLists'] = $_SESSION['sColorLists'];

		$this->data['loginCheck'] = $this->checkLogin('U');
	}


	/**
	 *
	 * Loading Order Page
	 */

	public function index(){

		if ($this->data['loginCheck'] != ''){
			$this->data['heading'] = 'Order Confirmation';
			if($this->uri->segment(2) == 'giftsuccess'){
				if($this->uri->segment(4)==''){
					$transId = $_REQUEST['txn_id'];
					$Pray_Email = $_REQUEST['payer_email'];
				}else{
					$transId = $this->uri->segment(4);
					$Pray_Email = '';
				}
				$this->data['Confirmation'] = $this->order_model->PaymentGiftSuccess($this->uri->segment(3),$transId,$Pray_Email);
				redirect("order/confirmation/gift");
			}elseif($this->uri->segment(2) == 'subscribesuccess'){
				$transId = $this->uri->segment(4);
				$Pray_Email = '';

				$this->data['Confirmation'] = $this->order_model->PaymentSubscribeSuccess($this->uri->segment(3),$transId);
				redirect("order/confirmation/subscribe");

			}elseif($this->uri->segment(2) == 'success'){
				//echo($this->uri->segment(5));die;
				if($this->uri->segment(5)==''){
					$transId = $_REQUEST['txn_id'];
					$Pray_Email = $_REQUEST['payer_email'];
				}elseif($this->uri->segment(5) == 'adaptive'){
					$get_Details = $this->order_model->get_all_details(ADAPTIVE_PAYMENTS,array('dealCodeNumber'=>$this->uri->segment(4)));
					if($get_Details->num_rows() == 1){
						$transId = $get_Details->row()->transaction_id;
						$paykey = $get_Details->row()->pay_key;
						$Pray_Email ='';
					}	
				}else{
					$transId = $this->uri->segment(5);
					$Pray_Email = '';
				}
				
				$this->data['Confirmation'] = $this->order_model->PaymentSuccess($this->uri->segment(3),$this->uri->segment(4),$transId,$Pray_Email,$this->uri->segment(6));
				//print_r(	$this->data['Confirmation'] );die;
				if($this->uri->segment(6) == 'cod'){
					redirect("order/cashondelivery/");
				}
				else{
					redirect("order/confirmation/cart");
				}//echo("<pre>");print_r($this->data['Confirmation']->result());die;
				//redirect("order/confirmation/cart");
				//$this->load->view('site/order/order.php',$this->data);
					
			}elseif($this->uri->segment(2) == 'cod'){
				$seller_id = $this->uri->segment(3,0);
				$invoice = $this->uri->segment(4,0);
				if($this->uri->segment(6)==''){
					$transId = '';
					$amount = $this->uri->segment(5,0);
					$dataArr1 = array('status' => 'success', 'transaction_id' => $transId, 'payment_type' => 'Paypal','invoice_id'=>$invoice,'seller_id'=>$seller_id,'amount'=>$amount);
				}else{
					$transId = $this->uri->segment(5,0);
					$amount = $this->uri->segment(6,0);
					$dataArr1 = array('status' => 'success', 'transaction_id' => $transId, 'payment_type' => 'Credit Cart','invoice_id'=>$invoice,'seller_id'=>$seller_id,'amount'=>$amount);
				}
				$this->product_model->simple_insert(COD_PAYMENT,$dataArr1);
				$this->session->unset_userdata('invoiceID');
				$this->data['Confirmation'] = 'Success';
				$this->data['payment'] = 'Cod';
				$this->load->view('site/order/order.php',$this->data);
			}
			elseif($this->uri->segment(2) == 'affiliatesuccess'){ //affiliate success page redirect
				
					$transId = '';
					$Pray_Email = '';
				
				$this->data['Confirmation'] = $this->order_model->PaymentSuccess($this->uri->segment(3),$this->uri->segment(4),$transId,$Pray_Email);
				redirect("order/confirmation/cart");
				//$this->load->view('site/order/order.php',$this->data);
				
			}elseif($this->uri->segment(2) == 'successgift'){

				$transId = 'GIFT'.$this->uri->segment(4);
				$Pray_Email = '';
				$this->data['Confirmation'] = $this->order_model->PaymentSuccess($this->uri->segment(3),$this->uri->segment(4),$transId,$Pray_Email);
				redirect("order/confirmation");
					
			}elseif($this->uri->segment(2) == 'failure'){
				$this->data['Confirmation'] = 'Failure';
				$this->data['errors'] = $this->uri->segment(3);
				$this->load->view('site/order/order.php',$this->data);
			}elseif($this->uri->segment(2) == 'notify'){
				$this->data['Confirmation'] = 'Failure';
				$this->load->view('site/order/order.php',$this->data);
			}elseif($this->uri->segment(2) == 'confirmation'){
				$this->data['Confirmation'] = 'Success';
				$this->load->view('site/order/order.php',$this->data);
			}elseif($this->uri->segment(2) == 'cashondelivery'){
				$this->data['Confirmation'] = 'Success';
				$this->load->view('site/order/cod_order.php',$this->data);
			}


		}else{
			redirect('login');
		}
	}

	public function insert_product_comment(){
		$uid= $this->checkLogin('U');
		$returnStr['status_code'] = 0;
		$comments = $this->input->post('comments');
		$product_id = $this->input->post('cproduct_id');
		$datestring = "%Y-%m-%d %h:%i:%s";
		$time = time();
		$conditionArr = array(
				'comments'=>$comments,
				'user_id'=>$uid,
				'product_id'=>$product_id,
				'status'=>'InActive',
				'dateAdded'=>mdate($datestring,$time)
		);
		$this->order_model->simple_insert(PRODUCT_COMMENTS,$conditionArr);
		$cmtID = $this->order_model->get_last_insert_id();
		$datestring = "%Y-%m-%d %h:%i:%s";
		$time = time();
		$createdTime = mdate($datestring,$time);
		$actArr = array(
					'activity'		=>	'own-product-comment',
					'activity_id'	=>	$product_id,
					'user_id'		=>	$this->checkLogin('U'),
					'activity_ip'	=>	$this->input->ip_address(),
					'created'		=>	$createdTime,
					'comment_id'	=> $cmtID
		);
		$this->order_model->simple_insert(NOTIFICATIONS,$actArr);
		$this->send_comment_noty_mail($cmtID,$product_id);
		$this->send_comment_noty_mail_to_admin($cmtID,$product_id);
		$returnStr['status_code'] = 1;
		echo json_encode($returnStr);
	}

	public function send_comment_noty_mail($cmtID='0',$pid='0'){
		if ($this->checkLogin('U')!=''){
			if ($cmtID != '0' && $pid != '0'){
				$productUserDetails = $this->product_model->get_product_full_details($pid);
				if ($productUserDetails->num_rows()==1){
					$emailNoty = explode(',', $productUserDetails->row()->email_notifications);
					if (in_array('comments', $emailNoty)){
						$commentDetails = $this->product_model->view_product_comments_details('where c.id='.$cmtID);
						if ($commentDetails->num_rows() == 1){
							if ($productUserDetails->prodmode == 'seller'){
								$prodLink = base_url().'things/'.$productUserDetails->row()->id.'/'.url_title($productUserDetails->row()->product_name,'-');
							}else {
								$prodLink = base_url().'user/'.$productUserDetails->row()->user_name.'/things/'.$productUserDetails->row()->seller_product_id.'/'.url_title($productUserDetails->row()->product_name,'-');
							}

							$newsid='1';
							$template_values=$this->order_model->get_newsletter_template_details($newsid);
							$adminnewstemplateArr=array('email_title'=> $this->config->item('email_title'),'logo'=> $this->data['logo'],'full_name'=>$commentDetails->row()->full_name,'product_name'=>$productUserDetails->row()->product_name,'user_name'=>$commentDetails->row()->user_name);
							extract($adminnewstemplateArr);
							$subject = $template_values['news_subject'];

							$message .= '<!DOCTYPE HTML>
								<html>
								<head>
								<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
								<meta name="viewport" content="width=device-width"/>
								<title>'.$template_values['news_subject'].'</title>
								<body>';
							include('./newsletter/registeration'.$newsid.'.php');

							$message .= '</body>
								</html>';

							if($template_values['sender_name']=='' && $template_values['sender_email']==''){
								$sender_email=$this->data['siteContactMail'];
								$sender_name=$this->data['siteTitle'];
							}else{
								$sender_name=$template_values['sender_name'];
								$sender_email=$template_values['sender_email'];
							}

							$email_values = array('mail_type'=>'html',
												'from_mail_id'=>$sender_email,
												'mail_name'=>$sender_name,
												'to_mail_id'=>$productUserDetails->row()->email,
												'subject_message'=>$subject,
												'body_messages'=>$message
							);
							$email_send_to_common = $this->product_model->common_email_send($email_values);
						}
					}
				}
			}
		}
	}

	public function send_comment_noty_mail_to_admin($cmtID='0',$pid='0'){
		if ($this->checkLogin('U')!=''){
			if ($cmtID != '0' && $pid != '0'){
				$productUserDetails = $this->product_model->get_product_full_details($pid);
				if ($productUserDetails->num_rows()==1){
					$commentDetails = $this->product_model->view_product_comments_details('where c.id='.$cmtID);
					if ($commentDetails->num_rows() == 1){
						if ($productUserDetails->prodmode == 'seller'){
							$prodLink = base_url().'things/'.$productUserDetails->row()->id.'/'.url_title($productUserDetails->row()->product_name,'-');
						}else {
							$prodLink = base_url().'user/'.$productUserDetails->row()->user_name.'/things/'.$productUserDetails->row()->seller_product_id.'/'.url_title($productUserDetails->row()->product_name,'-');
						}

						$newsid='20';
						$template_values=$this->order_model->get_newsletter_template_details($newsid);
						$adminnewstemplateArr=array('email_title'=> $this->config->item('email_title'),'logo'=> $this->data['logo'],'full_name'=>$commentDetails->row()->full_name,'product_name'=>$productUserDetails->row()->product_name,'user_name'=>$commentDetails->row()->user_name);
						extract($adminnewstemplateArr);
						$subject = $template_values['news_subject'];

						$message .= '<!DOCTYPE HTML>
								<html>
								<head>
								<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
								<meta name="viewport" content="width=device-width"/>
								<title>'.$template_values['news_subject'].'</title>
								<body>';
						include('./newsletter/registeration'.$newsid.'.php');

						$message .= '</body>
								</html>';

						if($template_values['sender_name']=='' && $template_values['sender_email']==''){
							$sender_email=$this->data['siteContactMail'];
							$sender_name=$this->data['siteTitle'];
						}else{
							$sender_name=$template_values['sender_name'];
							$sender_email=$template_values['sender_email'];
						}

						$email_values = array('mail_type'=>'html',
												'from_mail_id'=>$sender_email,
												'mail_name'=>$sender_name,
												'to_mail_id'=>$this->data['siteContactMail'],
												'subject_message'=>$subject,
												'body_messages'=>$message
						);
						$email_send_to_common = $this->product_model->common_email_send($email_values);
					}
				}
			}
		}
	}

}

