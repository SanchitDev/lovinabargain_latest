<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * Landing page functions
 * @author Teamtweaks
 *
 */

class Samedaydelivery extends MY_Controller {
  function __construct(){
		parent::__construct(); 
		$this->load->helper(array('cookie','date','form','email'));
		$this->load->library(array('encrypt','form_validation'));
		$this->load->model('samedaydelivery_model');
		$this->load->model('product_model');
		$this->load->model('category_model');
		if($_SESSION['sMainCategories'] == ''){
			$sortArr1 = array('field'=>'cat_position','type'=>'asc');
			$sortArr = array($sortArr1);
			$_SESSION['sMainCategories'] = $this->samedaydelivery_model->get_all_details(CATEGORY,array('rootID'=>'0','status'=>'Active'),$sortArr);
		}
		$this->data['mainCategories'] = $_SESSION['sMainCategories'];
		if($_SESSION['sColorLists'] == ''){
			$_SESSION['sColorLists'] = $this->samedaydelivery_model->get_all_details(LIST_VALUES,array('list_id'=>'1'));
		}
		$this->data['mainColorLists'] = $_SESSION['sColorLists'];
		$this->data['loginCheck'] = $this->checkLogin('U');
		//		echo $this->session->userdata('fc_session_user_id');die;
		$this->data['likedProducts'] = array();
		if ($this->data['loginCheck'] != ''){
			$this->data['likedProducts'] = $this->samedaydelivery_model->get_all_details(PRODUCT_LIKES,array('user_id'=>$this->checkLogin('U')));
		}
			
	}
	public function index(){
			if ($this->checkLogin('U') == ''){
			redirect(base_url());
			}else{
			if($this->input->get('q') ==''){
			   $Query = "select s.*,c.name from ".SAME_D_DLY." s 
			   JOIN ".COUNTRY_LIST." c on c.country_code = s.country_name
			   where s.status='Active' group by s.country_name DESC";
			   $get_country = $this->samedaydelivery_model->ExecuteQuery($Query);
			   if($get_country->num_rows()>0){
			      $this->data['country_list'] = $get_country;
			      foreach($get_country->result() as $row){
				     $this->data['get_cities'][$row->name] = $this->samedaydelivery_model->get_all_details(SAME_D_DLY,array('country_name'=>$row->country_name)); 
				  } 
			   }
			   $image_random = "SELECT * FROM ".PRODUCT." WHERE same_day_delivery='true' ORDER BY RAND() LIMIT 0,5";
			   $this->data['get_image'] = $this->samedaydelivery_model->ExecuteQuery($image_random);
			   $top_countries = "select s.*,c.name from ".SAME_D_DLY." s 
			                     JOIN ".COUNTRY_LIST." c on c.country_code = s.country_name
			                     where s.status='Active' group by s.country_name DESC LIMIT 0,4";
			   $this->data['top_countries'] = $this->samedaydelivery_model->ExecuteQuery($top_countries);
			   $Query = "select s.*,c.name from ".SAME_D_DLY." s 
			   JOIN ".COUNTRY_LIST." c on c.country_code = s.country_name
			   where s.status='Active' group by s.country_name DESC LIMIT 0,4";
			   $get_country = $this->samedaydelivery_model->ExecuteQuery($Query);
			   if($get_country->num_rows()>0){
			      $this->data['country_list'] = $get_country;
			      foreach($get_country->result() as $row){
				     $this->data['get_cities'][$row->name] = $this->samedaydelivery_model->get_all_details(SAME_D_DLY,array('country_name'=>$row->country_name)); 
				  } 
			   }
	           $this->data['heading'] = 'Same Day Delivery';
		       $this->load->view('site/same_day_delivery/search_samedaydly',$this->data);
		   }else{
			   $urlVal = $this->input->server('REQUEST_URI');
			   $urlVal = substr($urlVal, strpos($urlVal, '/same-day-delivery'));
			   $urlVal = substr($urlVal,1);
			   $urlValArrVal = explode('?',$urlVal);
			   $urlVal = $urlValArrVal[0];
			   
			   $searchResultPg = explode('?pg=', $searchResult[1]);
		       if($searchResult[1] != ''){
			      $finalValQry1 = '?'.$searchResult[1];
			      $finalValQry = $urlVal.$finalValQry1;
			      $finalValQryArr =explode('&pg=',$finalValQry);
			      $finalValQry = $finalValQryArr[0].'&pg';
		       }else{
			      $finalValQry = $urlVal	.'?pg';
               }
		       $newPage = 1;
		          if($this->input->get('pg') != ''){
				    $paginationVal = $this->input->get('pg')*20;
				    $limitPaging = ' limit '.$paginationVal.',20 ';
		          } else {
				    $limitPaging = ' limit 0,20';
		          }
		      $newPage = $this->input->get('pg')+1;
		      $paginationDisplay  = '<a title="'.$newPage.'" class="btn-more" href="'.base_url().$finalValQry.'='.$newPage.'" style="display: none;">See More Products</a>'; 
		      $this->data['paginationDisplay'] = $paginationDisplay;	
			   /* if($this->input->get('cat')!=''){
				  $condition = " where c.cat_name = '".addslashes($this->input->get('cat'))."'";
				  $catID = $this->product_model->getCategoryValues(' c.*,sbc.id as subcat_id,sbc.seourl as subcat_seourl,sbc.cat_name as subcat_sub_cat_name ',$condition);
				  $listSubCat = $catID->result();
				  $listSubCatSelBox = '<select style="display: none;" class="shop-select sub-category selectBox" edge="true">
	              <option value="">'.$catID->row()->cat_name.'</option>';
				  foreach ($listSubCat as $listSub){
					   $listSubCatSelBox.= '<option value="'.$listSub->subcat_sub_cat_name.'">'.$listSub->subcat_sub_cat_name.'</option>';
				  }
				  $listSubCatSelBox.= '</select>'; */
			  // }else{
			      $listSubCatSelBox = '<select  class="sub-listing" edge="true">
		          <option value="">All Category</option>';
				  foreach ($this->data['mainCategories']->result() as $listSub){
					  $sele_str = '';
					  if($this->input->get('cy') == $listSub->cat_name){
						 $sele_str = 'selected="selected"';
					  }
					  $listSubCatSelBox.= '<option '.$sele_str.' value="'.$listSub->cat_name.'">'.$listSub->cat_name.'</option>';
				 }
				 $listSubCatSelBox.= '</select>';
	//		   }
			   $this->data['listSubCatSelBox'] = $listSubCatSelBox;
			   if($this->input->get('cy')){
			      $condition = " where c.cat_name = '".addslashes($this->input->get('cy'))."'";
				  $catID = $this->product_model->getCategoryValues(' c.*,sbc.id as subcat_id,sbc.seourl as subcat_seourl,sbc.cat_name as subcat_sub_cat_name ',$condition);
				  $whereCond .= ' and FIND_IN_SET("'.$catID->row()->id.'",p.category_id)';
			   }
			   if($this->input->get('p')){
			      $price = explode('-',$this->input->get('p'));
				  $whereCond .= ' and p.sale_price >= "'.$price[0].'" and p.sale_price <= "'.$price[1].'"';
			   }
			   if($this->input->get('q')){
			       $dly_city_name = urldecode($this->input->get('q'));
		           $this->session->set_userdata('samedly_city_name',$dly_city_name);
				  $whereCond .= "and p.city LIKE '%".urldecode($this->input->get('q'))."%' and p.same_day_delivery ='true'";
			   }
			   if($this->input->get('c')){
				  $condition = " where list_value_seourl = '".$this->input->get('c')."'";
				  $catID = $this->category_model->getAttrubteValues($condition);
				  $whereCond .= ' and FIND_IN_SET("'.$catID->row()->id.'",p.list_value)';
			   }
			   if($this->input->get('sort_by_price')){
				  ($this->input->get('sort_by_price') == 'desc') ? $orderbyVal = $this->input->get('sort_by_price') : $orderbyVal ='';
				  $orderBy = ' order by p.sale_price '.$orderbyVal.'';
			   }else {
				  $orderBy = ' order by p.created desc ';
			   }
			   
			  $whereCond .= ' where p.id != "" '.$whereCond.' and p.quantity>0 and p.status="Publish" and u.group="Seller" and u.status="Active" or p.id != "" '.$whereCond.' and p.status="Publish" and p.quantity > 0 and p.user_id=0';
			  $searchProd = $whereCond.' '.$orderBy.' '.$limitPaging.' ';
			   
			   $productList = $this->product_model->searchShopyByCategory($searchProd);
			   $this->data['productList'] = $productList;
			   $Query = "select s.*,c.name from ".SAME_D_DLY." s 
			   JOIN ".COUNTRY_LIST." c on c.country_code = s.country_name
			   where s.status='Active' group by s.country_name";
			   $get_country = $this->samedaydelivery_model->ExecuteQuery($Query);
			   if($get_country->num_rows()>0){
			      $this->data['country_list'] = $get_country;
			      foreach($get_country->result() as $row){
				     $this->data['get_cities'][$row->name] = $this->samedaydelivery_model->get_all_details(SAME_D_DLY,array('country_name'=>$row->country_name)); 
				  } 
			   }
			   $this->data['heading'] = 'Same Day Delivery in '.$this->input->get('q');
	           $this->load->view('site/same_day_delivery/list_samedaydly',$this->data); 
			
		}
		   }
	}
	public function more_countries(){
		if ($this->checkLogin('U') == ''){
			redirect(base_url());
		}else{
			$Query = "select s.*,c.name from ".SAME_D_DLY." s 
			   JOIN ".COUNTRY_LIST." c on c.country_code = s.country_name
			   where s.status='Active' group by s.country_name";
			   $country_list = $this->samedaydelivery_model->ExecuteQuery($Query);
			   if($country_list->num_rows()>0){
					$i=1;
				  foreach($country_list->result() as $countries){
					$get_cities[$countries->name] = $this->samedaydelivery_model->get_all_details(SAME_D_DLY,array('country_name'=>$countries->country_name));
					$html .='<dt><a href="javascript:vold(0);" id="'.$i.'" onclick="get_cities(this.id);">'.$countries->name.'
							<small>';if($get_cities[$countries->name]->num_rows()>0){ 
								$html .= $get_cities[$countries->name]->num_rows();
							}else{
								$html .= "0";
							}'</small></a></dt>';
					$html .='<dd class="sdly-city-name" id="city-'.$i.'">';	
					if($get_cities[$countries->name]!='' && $get_cities[$countries->name]->num_rows()>0){
						foreach($get_cities[$countries->name]->result() as $rows){
							$html .='<em><a href="#">'.$rows->city_name.'</a><i>·</i></em>';
						}
					}$i++;					
				  }
			   }
			   echo $html;
		}
	}
	public function search_city(){
		    if(is_numeric($this->input->post('search'))){
			   $search = $this->input->post('search');
			   $Query = "SELECT * FROM ".PRODUCT." WHERE zip_code LIKE '%$search%'";
			   $result = $this->product_model->ExecuteQuery($Query);
			   if($result->num_rows()>0){
			      foreach($result->result() as $_result){
				     if(strstr($_result->zip_code, ',')){
					    $splt_value = explode(',',$_result->zip_code);
					    $input = preg_quote($search, '~');
					    $result_val = preg_grep('~' . $input . '~', $splt_value);
					    foreach($result_val as $result_value){
						   $Query = "SELECT * FROM ".SAME_D_DLY." WHERE (start_zip_code<= '$result_value' AND end_zip_code>= '$result_value')";
						   $result = $this->product_model->ExecuteQuery($Query);
						   if($result->num_rows()>0){
                              foreach($result->result() as $_result){
		                        echo '<div class="show" align="left">';
                                echo '<span class="name"><b>'.$search.'</b>-'.$_result->city_name.'</span>&nbsp;<br/></div>';
				              }
                           }
						}
					 }else{
					    $zipcode = $_result->zip_code;
						$Query = "SELECT * FROM ".SAME_D_DLY." WHERE (start_zip_code<= '$zipcode' AND end_zip_code>= '$zipcode')";
						$result = $this->product_model->ExecuteQuery($Query);
						   if($result->num_rows()>0){
                              foreach($result->result() as $_result){
		                        echo '<div class="show" align="left">';
                                echo '<span class="name"><b>'.$search.'</b>-'.$_result->city_name.'</span>&nbsp;<br/></div>';
				              }
                           }
					 } 
				  }
			   }
			   else {
			      echo '<div class="show" align="left">';
                  echo '<span class="name">No results found</span>&nbsp;<br/></div>';
			   }
			}else{
	           $search = $this->input->post('search');
			   $input = preg_quote($search, '~');
		       $result = preg_grep('~' . $input . '~', $this->config->item('cities'));
               foreach($result as $_result){
			   
			      if(count($_result)==''){
				     echo '<div class="show" align="left">';
                     echo '<span class="name">No results found</span>&nbsp;<br/></div>';
			      }else{
			          echo '<div class="show" align="left">';
                      echo '<span class="name">'.$_result.'</span>&nbsp;<br/></div>';
			      } 
		      }
			
			
		}
	}
	public function check_zipcode(){
	 
	    $zip_code = $this->input->post('zip_code');
		$country = $this->input->post('country');
		$Query = "SELECT * FROM ".PRODUCT." WHERE zip_code LIKE '%$zip_code%'";
		$result = $this->product_model->ExecuteQuery($Query);
		//echo $result->num_rows(); die;
		if($result->num_rows()==1){
		   if(strstr($result->row()->zip_code, ',')){
		       $splt_value = explode(',',$result->row()->zip_code);
			   $input = preg_quote($zip_code, '~');
			   $result_val = preg_grep('~' . $input . '~', $splt_value);
			   foreach($result_val as $result_value){
				  $Query = "SELECT * FROM ".SAME_D_DLY." WHERE (start_zip_code<= '$result_value' AND end_zip_code>= '$result_value') AND country_name= '$country'";
			      $result = $this->product_model->ExecuteQuery($Query);
			      if($result->num_rows() ==1){
		              echo "Yes, your address is eligible for Same-day Delivery.";
                  }   
			   }
		   }else{
		      $zipcode= $result->row()->zip_code;
			  $Query = "SELECT * FROM ".SAME_D_DLY." WHERE (start_zip_code<= '$zipcode' AND end_zip_code>= '$zipcode') AND country_name= '$country'";
			  $result = $this->product_model->ExecuteQuery($Query);
			  if($result->num_rows() ==1){
		        echo "Yes, your address is eligible for Same-day Delivery.";
              }
		   }
		}
		else{
		  echo "Hang tight, we're almost there! Unfortunately same-day delivery is not currently available in your area, but we're expanding rapidly. Please check back soon!";
        }		
			
	  
	}
}