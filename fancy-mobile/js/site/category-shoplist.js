jQuery(function($) {
	var code = null;
	
	var QueryString = function () {
		  // This function is anonymous, is executed immediately and 
		  // the return value is assigned to QueryString!
		  var query_string = {};
		  var query = window.location.search.substring(1);
		  var vars = query.split("&");
		  for (var i=0;i<vars.length;i++) {
		    var pair = vars[i].split("=");
		    	// If first entry with this name
		    if (typeof query_string[pair[0]] === "undefined") {
		      query_string[pair[0]] = pair[1];
		    	// If second entry with this name
		    } else if (typeof query_string[pair[0]] === "string") {
		      var arr = [ query_string[pair[0]], pair[1] ];
		      query_string[pair[0]] = arr;
		    	// If third or later entry with this name
		    } else {
		      query_string[pair[0]].push(pair[1]);
		    }
		  } 
		    return query_string;
	} ();
		
	
	$('.search .sub-category').change(function(){
		var $this = $(this), url = this.value, text = $this.find('>option').eq(this.selectedIndex).text(),issibling = $this.find('>option').eq(this.selectedIndex).hasClass('sibling');
		if(url) window.location = url;
		
        if (issibling)
		    $('ul.breadcrumbs').find('.last').remove();
		$('ul.breadcrumbs').append('<li class="last">/ <a href="'+url+'">'+text+'</a></li>');
	});
	
     $('.search .sub-categorydly').change(function(){
	   
		var cat = this.value, url = location.pathname, args = QueryString, query;

		if(cat){
			args.cy = cat;
		} else {
			delete args.cy;
		}
           
		if(query = $.param(args)) url += '?'+query;

		window.location = url;
	});
     
	$('.search .price-range').change(function(){
									
		var range = this.value, url = location.pathname, args = QueryString, query;

		if(range != '-1'){
			args.p = range;
		} else {
			delete args.p;
		}

		if(query = $.param(args)) url += '?'+query;
		window.location = url;
	});
	
	$('.search .color-filter').change(function(){
		var color = this.value, url = location.pathname, args = QueryString, query;

		if(color){
			args.c = color;
		} else {
			delete args.c;
		}

		if(query = $.param(args)) url += '?'+query;
		window.location = url;
	});
	
	
	  $('.search .sort-by-price').change(function(){

		var sort_by_price = this.value, url = location.pathname, args = QueryString, query;

		if(sort_by_price){
			args.sort_by_price = sort_by_price;
		} else {
			delete args.sort_by_price;
		}

		if(query = $.param(args)) url += '?'+query;

		window.location = url;
	});
	  
	  
	  
	$('.search-frm .del-val').click(function(event){
	    event.preventDefault();
	    $(this).next('.search-string').val('').keyup();
	    var $age = $(this).next('.filter-age');
	    if ($age.length > 0) {
		    $age.val('').keyup();
	    }
		
	});
	
	
	
	$('.search .search-string')
		.bind('keydown', function(event){
			if(event.keyCode == 13){
				var q = $.trim(this.value), url = location.pathname, args = QueryString, query;

				event.preventDefault();

				if(q) {
					args.q = q;
				} else {
					delete args.q;
				}

				if(query = $.param(args)) url += '?'+query;

				window.location = url;
			}
		});
		
	
	
	/*****************************
	 * Same day delivery search
	 * ***************************
	 * 
	 */
     $('.sameday-delivary .sub-listing').change(function(){
	   
		var cat = this.value, url = location.pathname, args = QueryString, query;

		if(cat){
			args.cy = cat;
		} else {
			delete args.cy;
		}
           
		if(query = $.param(args)) url += '?'+query;

		window.location = url;
	});
     
	$('.sameday-delivary .price-range').change(function(){
											
		var range = this.value, url = location.pathname, args = QueryString, query;

		if(range != '-1'){
			args.p = range;
		} else {
			delete args.p;
		}

		if(query = $.param(args)) url += '?'+query;
		window.location = url;
	});
	
	$('.sameday-delivary .color-filter').change(function(){
		var color = this.value, url = location.pathname, args = QueryString, query;

		if(color){
			args.c = color;
		} else {
			delete args.c;
		}

		if(query = $.param(args)) url += '?'+query;
		window.location = url;
	});
	
	
	  $('.sameday-delivary .sort-by-price').change(function(){
		var sort_by_price = this.value, url = location.pathname, args = QueryString, query;

		if(sort_by_price){
			args.sort_by_price = sort_by_price;
		} else {
			delete args.sort_by_price;
		}

		if(query = $.param(args)) url += '?'+query;

		window.location = url;
	});
	  
	  
	
});
