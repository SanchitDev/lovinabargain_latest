<?php $message .= '<table bgcolor=\"#7da2c1\" width=\"640\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">
<tbody>
<tr>
<td style=\"padding: 40px;\">
<table width=\"610\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border: #1d4567 1px solid; font-family: Arial, Helvetica, sans-serif;\">
<tbody>
<tr>
<td><a href=\"'.base_url().'\"><img alt=\"'.$meta_title.'\" src=\"'.base_url().'images/logo/'.$logo.'\" style=\"margin: 15px 5px 0; padding: 0px; border: none;\" /></a></td>
</tr>
<tr>
<td valign=\"top\">
<table bgcolor=\"#FFFFFF\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">
<tbody>
<tr>
<td colspan=\"2\">
<h3 style=\"padding: 10px 15px; margin: 0px; color: #0d487a;\">Hi there,</h3>
<p style=\"padding: 0px 15px 10px 15px; font-size: 12px; margin: 0px;\">&nbsp;</p>
</td>
</tr>
<tr>
<td valign=\"top\" width=\"50%\" style=\"font-size: 12px; padding: 10px 15px;\">
<p>'.$full_name.' invited you on '.$siteTitle.'. To join '.$siteTitle.'&nbsp;<a href=\"'.base_url().'?ref='.$user_name.'\">click here</a>.</p>
</td>
<td valign=\"top\" width=\"50%\" style=\"font-size: 12px; padding: 10px 15px;\">
<p>&nbsp;</p>
<p><strong>- '.$email_title.' Team</strong></p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>';  ?>