<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * This controller contains the common functions
 * @author Teamtweaks
 *
 */
class MY_Controller extends CI_Controller {
	public $privStatus;
	public $data = array();
	function __construct()
	{
		parent::__construct();

		ob_start();
		////error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		////error_reporting(E_ALL ^ (E_NOTICE));
		//error_reporting(0);
        $this->load->helper('cookie');
		date_default_timezone_set("Asia/Calcutta");
		$this->load->helper(array('url','language','lg'));
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		$this->load->library('session');
		$this->load->model('user_model');

		/***Detect Device Start***/
		/*$this->load->library('find_mobile');
		$detect = new Find_mobile;

		$currentController = $this->router->class;
		// echo $currentController;die;
		if($currentController == "mobile" || $currentController == "mobileCheckout" || $currentController == "mobileOrder" || $currentController == "ios" || $currentController == "ios_store"){
			$result = "Yes";
		}else{
			$result = "no";
		}
		if($result == "no"){
			if($detect->isMobile()){
				redirect(base_url().'fancy-mobile');
				exit();
			}
		}*/
		/***Detect Device End***/

		if ($this->config->item('common_prefix') && $this->config->item('common_prefix')!='' && $this->config->item('common_prefix')!='-'){
			define('SITE_COMMON_DEFINE', $this->config->item('common_prefix'));
		}else {
			define('SITE_COMMON_DEFINE', 'fancyy-');
		}

		if($result == "no"){
				if ($this->config->item('https_enabled') && $this->config->item('https_enabled')!=''){
					define('HTTTPS_ENABLED', $this->config->item('https_enabled'));
				}else {
					define('HTTTPS_ENABLED', 'no');
				}
		}


		if($result == "no"){
			if (HTTTPS_ENABLED == 'yes'){
				if ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != 'on'){
					$this->session->keep_flashdata('sErrMSGType');
					$this->session->keep_flashdata('sErrMSG');
					redirect(base_url().$this->uri->uri_string());
				}
			}else if (HTTTPS_ENABLED == 'no'){
				if ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != 'off'){
					$this->session->keep_flashdata('sErrMSGType');
					$this->session->keep_flashdata('sErrMSG');
					redirect(base_url().$this->uri->uri_string());
				}
			}
		}
		/*
		 * Connecting Database
		 */
		$this->load->database();
		/*
		if($_FILES['category']['name'] !=''){

				$config['overwrite'] = FALSE;
				$config['upload_path']   = './uploaded/test/';
				$config['allowed_types'] = 'csv|xlxs|xlsx';
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				
				if($this->upload->do_upload('category')){
					$categoryResult = $this->upload->data(); 
					// echo '<pre>';print_r($_FILES);die;
					require_once 'spreadsheet-reader-master/php-excel-reader/excel_reader2.php';
					require_once 'spreadsheet-reader-master/SpreadsheetReader.php';

					$target_dir = "uploaded/test/";
					$target_file = $target_dir.basename($categoryResult['file_name']);
					//echo "<pre>";print_r($target_file);die;
							
						$Spreadsheet = new SpreadsheetReader($target_file);

						$columarray = array();
						foreach ($Spreadsheet as $key=>$Row)
						{
							if($key==0){
								$columarray = $Row;
							}
						}
						
						$finalarray = array();
						
						foreach ($Spreadsheet as $key=>$Row){
							if($key!=0){

								$errors = array_filter($Row);

								if (!empty($errors)) {
							
									$a = array_combine($columarray, $Row);
									
									foreach($a as $key2=>$vals){
										if($key2 == ''){
											unset($a[$key2]);
										}
									}
									$finalarray[] = $a;
									$catb = $a;
									echo '<pre>';print_r($catb).'<br><br><br><br>';
									
									echo $arrayCount = 	count(array_filter($catb)).'<br>';

									if($arrayCount == 1){
										$rootName1 = $catb['1'];
										$CheckName1	= $this->user_model->get_all_details(CATEGORY,array('cat_name'=>$rootName1));
										if($CheckName1->num_rows() == 0){
											$this->user_model->simple_insert(CATEGORY,array('cat_name'=>$rootName1,
																							'rootID'=>'0',
																							'status'=>'Active',
																							'seourl'=>url_title($rootName1, '-', TRUE),
																							'seo_title' => $rootName1,
																							'seo_keyword' => $rootName1,
																							'seo_description' => $rootName1
																			));
											echo '1-New Cat'.$this->db->last_query().'<br><br><br><br>';
										}else{
											echo '1-Existed'.$rootName1.'<br><br><br><br>';
										}

									}else if($arrayCount == 2){

										$rootIdName2 	= $catb['1'];
										$rootName2 		= $catb['2'];
										

											$rootCategory2	= $this->user_model->get_all_details(CATEGORY,array('cat_name'=>$rootIdName2));
											if($rootCategory2->num_rows() == 1){
												$this->user_model->simple_insert(CATEGORY,array('cat_name'=>$rootName2,
																								'status'=>'Active',
																								'seourl'=>url_title($rootName2, '-', TRUE),
																								'seo_title' => $rootName2,
																								'rootID'=>$rootCategory2->row()->id,
																								'seo_keyword' => $rootName2,
																								'seo_description' => $rootName2
																				));
												echo '2-New Cat'.$this->db->last_query().'<br><br>';
											}else{
												$this->user_model->simple_insert(CATEGORY,array('cat_name'=>$rootIdName2,
																								'rootID'=>'0',
																								'status'=>'Active',
																								'seourl'=>url_title($rootIdName2, '-', TRUE),
																								'seo_title' => $rootIdName2,
																								'seo_keyword' => $rootIdName2,
																								'seo_description' => $rootIdName2
																				));
												echo '2-New Cat Not Existed-Inserted_new'.$this->db->last_query().'<br><br>';
												$last_RootId2 = $this->user_model->get_last_insert_id();

												$this->user_model->simple_insert(CATEGORY,array('cat_name'=>$rootName2,
																								'status'=>'Active',
																								'seourl'=>url_title($rootName2, '-', TRUE),
																								'seo_title' => $rootName2,
																								'rootID'=>$last_RootId2,
																								'seo_keyword' => $rootName2,
																								'seo_description' => $rootName2
																				));
												echo '2-New Sub Cat Inserted '.$this->db->last_query().' <br><br><br><br>';
											}

										
									}else if($arrayCount == 3){

										$rootIdName3 	= $catb['2'];
										$rootName3 		= $catb['3'];
										

											$rootCategory3	= $this->user_model->get_all_details(CATEGORY,array('cat_name'=>$rootIdName3));
											if($rootCategory3->num_rows() == 1){
												$this->user_model->simple_insert(CATEGORY,array('cat_name'=>$rootName3,
																								'status'=>'Active',
																								'seourl'=>url_title($rootName3, '-', TRUE),
																								'seo_title' => $rootName3,
																								'rootID'=>$rootCategory3->row()->id,
																								'seo_keyword' => $rootName3,
																								'seo_description' => $rootName3
																				));
												echo '3-New Cat'.$this->db->last_query().'<br><br><br><br>';
											}else{

												$rootIdName2 	= $catb['1'];
												$rootCategory2	= $this->user_model->get_all_details(CATEGORY,array('cat_name'=>$rootIdName2));
												if($rootCategory2->num_rows() == 1){	
													$this->user_model->simple_insert(CATEGORY,array('cat_name'=>$rootIdName3,
																									'rootID'=>$rootCategory2->row()->id,
																									'status'=>'Active',
																									'seourl'=>url_title($rootIdName3, '-', TRUE),
																									'seo_title' => $rootIdName3,
																									'seo_keyword' => $rootIdName3,
																									'seo_description' => $rootIdName3
																					));
													echo '3-New Cat Not Existed-Inserted_new'.$this->db->last_query().' <br><br><br><br>';

													$last_RootId3 = $this->user_model->get_last_insert_id();

													$this->user_model->simple_insert(CATEGORY,array('cat_name'=>$rootName3,
																									'status'=>'Active',
																									'seourl'=>url_title($rootName3, '-', TRUE),
																									'seo_title' => $rootName3,
																									'rootID'=>$last_RootId3,
																									'seo_keyword' => $rootName3,
																									'seo_description' => $rootName3
																					));
													echo '3-New Sub Cat Inserted '.$this->db->last_query().' <br><br><br><br>';
												}
										}
										
									}else if($arrayCount == 4){

										$rootIdName4 	= $catb['3'];
										$rootName4 		= $catb['4'];
									

											$rootCategory4	= $this->user_model->get_all_details(CATEGORY,array('cat_name'=>$rootIdName4));
											if($rootCategory4->num_rows() == 1){
												$this->user_model->simple_insert(CATEGORY,array('cat_name'=>$rootName4,
																								'status'=>'Active',
																								'seourl'=>url_title($rootName4, '-', TRUE),
																								'seo_title' => $rootName4,
																								'rootID'=>$rootCategory4->row()->id,
																								'seo_keyword' => $rootName4,
																								'seo_description' => $rootName4
																				));
												echo '4-New Sub Cat Inserted '.$this->db->last_query().' <br><br><br><br>';
											}else{

												$rootIdName3 	= $catb['2'];
												$rootCategory3	= $this->user_model->get_all_details(CATEGORY,array('cat_name'=>$rootIdName3));
												if($rootCategory3->num_rows() == 1){
													$this->user_model->simple_insert(CATEGORY,array('cat_name'=>$rootIdName4,
																									'rootID'=>$rootCategory3->row()->id,
																									'status'=>'Active',
																									'seourl'=>url_title($rootIdName4, '-', TRUE),
																									'seo_title' => $rootIdName4,
																									'seo_keyword' => $rootIdName4,
																									'seo_description' => $rootIdName4
																					));
													echo '4-New Cat Not Existed-Inserted_new'.$this->db->last_query().' <br><br><br><br>';
													$last_RootId4 = $this->user_model->get_last_insert_id();

													$this->user_model->simple_insert(CATEGORY,array('cat_name'=>$rootName4,
																									'status'=>'Active',
																									'seourl'=>url_title($rootName4, '-', TRUE),
																									'seo_title' => $rootName4,
																									'rootID'=>$last_RootId4,
																									'seo_keyword' => $rootName4,
																									'seo_description' => $rootName4
																					));
													echo '4-New Sub Cat Inserted '.$this->db->last_query().' <br><br><br><br>';
												}
											}

										
									}else if($arrayCount == 5){
										
										$rootIdName5 	= $catb['4'];
										$rootName5 		= $catb['5'];
										

											$rootCategory5	= $this->user_model->get_all_details(CATEGORY,array('cat_name'=>$rootIdName5));
											if($rootCategory5->num_rows() == 1){
												$this->user_model->simple_insert(CATEGORY,array('cat_name'=>$rootName5,
																								'status'=>'Active',
																								'seourl'=>url_title($rootName5, '-', TRUE),
																								'seo_title' => $rootName5,
																								'rootID'=>$rootCategory5->row()->id,
																								'seo_keyword' => $rootName5,
																								'seo_description' => $rootName5
																				));
												echo '5-New Sub Cat'.$this->db->last_query().'<br><br>';
											}else{
												
												$rootIdName4 	= $catb['3'];
												$rootCategory4	= $this->user_model->get_all_details(CATEGORY,array('cat_name'=>$rootIdName4));
												if($rootCategory4->num_rows() == 1){
													$this->user_model->simple_insert(CATEGORY,array('cat_name'=>$rootIdName5,
																									'rootID'=>$rootCategory4->row()->id,
																									'status'=>'Active',
																									'seourl'=>url_title($rootIdName5, '-', TRUE),
																									'seo_title' => $rootIdName5,
																									'seo_keyword' => $rootIdName5,
																									'seo_description' => $rootIdName5
																					));
													echo '5-New Cat Not Existed-Inserted_new'.$this->db->last_query().'<br><br>';
													$last_RootId5 = $this->user_model->get_last_insert_id();

													$this->user_model->simple_insert(CATEGORY,array('cat_name'=>$rootName5,
																									'status'=>'Active',
																									'seourl'=>url_title($rootName5, '-', TRUE),
																									'seo_title' => $rootName5,
																									'rootID'=>$last_RootId5,
																									'seo_keyword' => $rootName5,
																									'seo_description' => $rootName5
																					));
													
													echo '5-New Sub Cat Inserted '.$this->db->last_query().' <br><br><br><br>';
												}
											}
										
									}else if($arrayCount == 6){

										$rootIdName6 	= $catb['5'];
										$rootName6 		= $catb['6'];
										

											$rootCategory6	= $this->user_model->get_all_details(CATEGORY,array('cat_name'=>$rootIdName6));
											if($rootCategory6->num_rows() == 1){
												$this->user_model->simple_insert(CATEGORY,array('cat_name'=>$rootName6,
																								'status'=>'Active',
																								'seourl'=>url_title($rootName6, '-', TRUE),
																								'seo_title' => $rootName6,
																								'rootID'=>$rootCategory6->row()->id,
																								'seo_keyword' => $rootName6,
																								'seo_description' => $rootName6
																				));
												echo '6-New Cat'.$this->db->last_query().'<br><br>';
											}else{

												$rootIdName5 	= $catb['4'];
												$rootCategory5	= $this->user_model->get_all_details(CATEGORY,array('cat_name'=>$rootIdName5));
												if($rootCategory5->num_rows() == 1){
													$this->user_model->simple_insert(CATEGORY,array('cat_name'	=>$rootIdName6,
																									'rootID'	=>$rootCategory5->row()->id,
																									'status'	=>'Active',
																									'seourl'	=>url_title($rootIdName6, '-', TRUE),
																									'seo_title' => $rootIdName6,
																									'seo_keyword' => $rootIdName6,
																									'seo_description' => $rootIdName6
																					));
													echo '6-New Cat Not Existed-Inserted_new'.$this->db->last_query().'<br><br>';
													$last_RootId6 = $this->user_model->get_last_insert_id();

													$this->user_model->simple_insert(CATEGORY,array('cat_name'=>$rootName6,
																									'status'=>'Active',
																									'seourl'=>url_title($rootName6, '-', TRUE),
																									'seo_title' => $rootName6,
																									'rootID'=>$last_RootId6,
																									'seo_keyword' => $rootName6,
																									'seo_description' => $rootName6
																					));
													echo '6-New Sub Cat Inserted '.$this->db->last_query().' <br><br><br><br>';
												}
											}

										
									}

								}
							}
						}

						$result = $finalarray;		
						die;
				}else{
					echo 'error';die;
				}
		}	*/
		$this->data['demoserverChk'] = $demoserverChk = strpos($this->input->server('DOCUMENT_ROOT'),'kaviraj/');

		/*
		 * Loading CMS Pages
		 */
		if ($_SESSION['cmsPages'] == ''){
			$cmsPages = $this->db->query('select * from '.CMS.' where `status`="Publish" and `hidden_page`="No" order by priority');
			$_SESSION['cmsPages'] = $cmsPages->result_array();
		}
		$this->data['cmsPages'] = $_SESSION['cmsPages'];

		/*
		 * Loading Categories
		 */
		if ($_SESSION['all_categories'] == ''){
			$_SESSION['all_categories'] = $this->db->query('select * from '.CATEGORY.' where `status`="Active" order by cat_position asc limit 5');
		}
		$this->data['all_categories'] = $_SESSION['all_categories'];
		// echo "<pre>";print_r($this->data['all_categories'] ->result());die;
		$root_id_arr = array();
		if ($this->data['all_categories']->num_rows()>0){
			foreach ($this->data['all_categories']->result() as $all_cat_row){
				if (!in_array($all_cat_row->rootID, $root_id_arr)){
					$root_id_arr[] = $all_cat_row->rootID;
				}
			}
		}
		
		$this->data['root_id_arr'] = array_filter($root_id_arr);

		$this->load->model('product_model');
		if($_SESSION['sCountryLists'] == ''){
			$_SESSION['sCountryLists'] = $this->product_model->get_all_details(COUNTRY_LIST,array(),array(array('field'=>'name','type'=>'asc')));
		}
		$this->data['mainCountryLists'] = $_SESSION['sCountryLists'];

		/*
		 * Loading Footer Widgets
		 */
		if ($_SESSION['footerWidget'] == ''){
			$footerWidget = $this->db->query('select * from '.FOOTER.' where `status`="Active"');
			$_SESSION['footerWidget'] = $footerWidget->result_array();
		}
		$this->data['footerWidget'] = $_SESSION['footerWidget'];

		/*
		 * Loading Theme Layouts
		 */
		if ($_SESSION['themeLayout'] == ''){
			$themeLayout = $this->db->query('select * from '.THEME_LAYOUT.'');
			$_SESSION['themeLayout'] = $themeLayout->result_array();
		}
		$this->data['themeLayout'] = $_SESSION['themeLayout'];

		/*
		 * Loading Theme Controls
		 */
		if ($_SESSION['layoutList'] == ''){
			$layoutList = $this->db->query('select * from '.CONTROLMGMT.'');
			$_SESSION['layoutList'] = $layoutList;
		}
		$this->data['layoutList'] = $_SESSION['layoutList'];

		/*
		 * Getting fancybox count
		 */
		if ($_SESSION['fancyBoxCount'] == ''){
			$fancyBoxList = $this->db->query('select * from '.FANCYYBOX.' where `status`="Publish"');
			$_SESSION['fancyBoxCount'] = $fancyBoxList->num_rows();
		}
		$this->data['fancyBoxCount'] = $_SESSION['fancyBoxCount'];

		/*
		 * Loading active languages
		 */
		if ($_SESSION['activeLgs'] == ''){
			$activeLgsList = $this->db->query('select * from '.LANGUAGES.' where `status`="Active"');
			$_SESSION['activeLgs'] = $activeLgsList->result_array();
		}
		$this->data['activeLgs'] = $_SESSION['activeLgs'];

		/*
		 * Loading default languages
		 */
		if ($_SESSION['defaultLg'] == ''){
			$defaultLgList = $this->db->query('select * from '.LANGUAGES.' where `default`="yes"');
			$_SESSION['defaultLg'] = $defaultLgList->result_array();
		}
		$this->data['defaultLg'] = $_SESSION['defaultLg'];

		/*
		 * Loading User Country
		 */
			//echo($this->session->userdata('user_country'));die;
                        /*
                         * Uncomment before moving to live
                         */
		if ($this->session->userdata('user_country') == ''){
			//$user_country_json = file_get_contents('https://freegeoip.net/json/'.$this->input->ip_address(),false,stream_context_create(array('http' => array('header'=>'Connection: close\r\n'))));
                        //print_r($user_country_json);die;
                        //$user_country_json = '{"ip":"192.168.1.159","country_code":"","country_name":"","region_code":"","region_name":"","city":"","zip_code":"","time_zone":"","latitude":0,"longitude":0,"metro_code":0}';
			$user_country_arr = json_decode($user_country_json);

			//print_r($user_country_arr);die;
			$user_country = $user_country_arr->country_code;

			$country_name = $user_country_arr->country_name;
			if($country_name == 'United Kingdom' || $country_name == 'Gibraltar' || $country_name == 'Channel Islands') {
				$this->session->set_userdata('user_country','GB');
			} else {
				$this->session->set_userdata('user_country',$user_country);
			}
		}

		$user_country = $this->session->userdata('user_country');
		if ($user_country=='') $user_country='US';
		$this->data['user_country'] = $user_country;

		/*
		 * Checking user language and loading user details
		 */
		if($this->checkLogin('U')!=''){
			$this->data['userDetails'] = $this->db->query('select * from '.USERS.' where `id`="'.$this->checkLogin('U').'"');
			$this->data['CurrUserImg']=$this->data['userDetails']->row()->thumbnail;
			$selectedLangCode = $this->session->userdata('language_code');
			if ($this->data['userDetails']->row()->language != $selectedLangCode){
				$this->session->set_userdata('language_code',$this->data['userDetails']->row()->language);
				$this->session->keep_flashdata('sErrMSGType');
				$this->session->keep_flashdata('sErrMSG');
				redirect($this->uri->uri_string());
			}
		}

		$uriMethod = $this->uri->segment(3,0);
		if (substr($uriMethod, 0,7) == 'display' || substr($uriMethod, 0,4) == 'view' || $uriMethod == '0'){
			$this->privStatus = '0';
		}else if (substr($uriMethod, 0,3) == 'add'){
			$this->privStatus = '1';
		}else if (substr($uriMethod, 0,4) == 'edit' || substr($uriMethod, 0,6) == 'insert' || substr($uriMethod, 0,6) == 'change'){
			$this->privStatus = '2';
		}else if (substr($uriMethod, 0,6) == 'delete'){
			$this->privStatus = '3';
		}else {
			$this->privStatus = '0';
		}

		$this->load->model('pricing_model');
		$condition_price_full = '';
		$this->data['pricefulllist'] = $this->pricing_model->get_all_details(PRICING,array());
		$this->load->model('layout_model');
		$this->data['layoutfulllist'] = $this->layout_model->get_all_details(LAYOUT,array());
		$this->data['login_succ_msg'] = 'Login Success';
		if ($this->data['layoutfulllist']->num_rows()>0){
			foreach ($this->data['layoutfulllist']->result() as $layout_row){
				if ($layout_row->place == 'login success msg'){
					$this->data['login_succ_msg'] = $layout_row->text;
				}
			}
		}
		$condition = array();
		$this->data['countryList'] = $this->layout_model->get_all_details(COUNTRY_LIST,$condition,array(array('field'=>'name','type'=>'asc')));
		$this->data['countrieslists'] = $this->layout_model->get_all_details(COUNTRIES,array());
		$this->data['state_lists']    = $this->layout_model->get_all_details(STATES,array());
		$this->data['title'] = $this->config->item('meta_title');;
		$this->data['heading'] = '';
		$this->data['flash_data'] = $this->session->flashdata('sErrMSG');
		$this->data['flash_data_type'] = $this->session->flashdata('sErrMSGType');
		$this->data['adminPrevArr'] = $this->config->item('adminPrev');
		$this->data['adminEmail'] = $this->config->item('email');
		$this->data['privileges'] = $this->session->userdata('fc_session_admin_privileges');
		$this->data['subAdminMail'] = $this->session->userdata('fc_session_admin_email');
		$this->data['loginID'] = $this->session->userdata('fc_session_user_id');
		$this->data['allPrev'] = '0';
		$this->data['logo'] = $this->config->item('logo_image');
		$this->data['fevicon'] = $this->config->item('fevicon_image');
		$this->data['footer'] = $this->config->item('footer_content');
		$this->data['siteContactMail'] = $this->config->item('site_contact_mail');
		$this->data['WebsiteTitle'] = $this->config->item('email_title');
		$this->data['siteTitle'] = $this->config->item('email_title');
		$this->data['meta_title'] = $this->config->item('meta_title');
		$this->data['meta_keyword'] = $this->config->item('meta_keyword');
		$this->data['meta_description'] = $this->config->item('meta_description');
		$this->data['giftcard_status'] = $this->config->item('giftcard_status');
		$this->data['sidebar_id'] = $this->session->userdata('session_sidebar_id');
		if ($this->session->userdata('fc_session_admin_name') == $this->config->item('admin_name')){
			$this->data['allPrev'] = '1';
		}

		$this->data['paypal_ipn_settings'] = unserialize($this->config->item('payment_0'));
		$this->data['paypal_credit_card_settings'] = unserialize($this->config->item('payment_1'));
		$this->data['authorize_net_settings'] = unserialize($this->config->item('payment_2'));
		$this->data['paypal_adaptive_settings'] = unserialize($this->config->item('payment_3'));
		$this->data['paypal_stripe_settings'] = unserialize($this->config->item('payment_4'));
		$this->data['cod_settings'] = unserialize($this->config->item('payment_6'));

		//echo("<pre>");print_r(unserialize($this->data['paypal_stripe_settings']['settings']));die;

		$this->data['currencySymbol'] = $this->config->item('currency_currency_symbol');
		//		$this->data['currencySymbol'] = html_entity_decode($this->config->item('currency_currency_symbol'));
		$this->data['currencyType'] = $this->config->item('currency_currency_type');
		$this->data['datestring'] = "%Y-%m-%d %H:%i:%s";
		if($this->checkLogin('U')!=''){
			$this->data['common_user_id'] = $this->checkLogin('U');
		}elseif($this->checkLogin('T')!=''){
			$this->data['common_user_id'] = $this->checkLogin('T');
		}else{
			$temp_id = substr(number_format(time() * rand(),0,'',''),0,6);
			$this->session->set_userdata('fc_session_temp_id',$temp_id);
			$this->data['common_user_id'] = $temp_id;
		}
		$this->data['emailArr'] = $this->config->item('emailArr');
		$this->data['notyArr'] = $this->config->item('notyArr');
		$this->load->model('minicart_model');
		$this->load->model('product_model');

		/*
		 * Like button texts
		 */
		define(LIKE_BUTTON, $this->config->item('like_text'));
		define(LIKED_BUTTON, $this->config->item('liked_text'));
		define(UNLIKE_BUTTON, $this->config->item('unlike_text'));

		if($_SESSION['authUrl'] == ''){
			//header( 'Location:http://192.168.1.253/fancyclone/');
		}


		/*Refereral Start */

		if($this->input->get('ref') != '')
		{
			//echo $this->input->get('ref');
			$referenceName = $this->input->get('ref');
			$this->session->set_userdata('referenceName',$referenceName);
		}

		/*Refereral End */

		/* Multilanguage start*/
		if($this->uri->segment('1') != 'admin')
		{
			$check_country_lg  = $this->product_model->get_all_details(LANGUAGES,array('country_code'=>$user_country,'status'=>'Active'));
			if ($check_country_lg->num_rows()>0){
				$defaultLanguage = $check_country_lg->row()->lang_code;
			} else {
				$defaultLanguage = $this->data['defaultLg'][0]['lang_code'];
			}
			$selectedLanguage = $this->session->userdata('language_code');
			if ($defaultLanguage==''){
				$defaultLanguage = 'en';
			}
			$filePath = APPPATH."language/".$selectedLanguage."/".$selectedLanguage."_lang.php";
			if($selectedLanguage != '')
			{

				if(!(is_file($filePath)))
				{

					$this->lang->load($defaultLanguage, $defaultLanguage);
					$this->data['selectedLanguage']=$defaultLanguage;
				}
				else
				{
					$this->lang->load($selectedLanguage, $selectedLanguage);
					$this->data['selectedLanguage']=$selectedLanguage;
				}

			}
			else
			{
// 				echo $defaultLanguage;die;
				$this->lang->load($defaultLanguage, $defaultLanguage);
				$this->data['selectedLanguage']=$defaultLanguage;
			}
		}
		/* Multilanguage end*/

		/***Mini cart Lg****/

		$mini_cart_lg = array();

		if($this->lang->line('items') != '')
		$mini_cart_lg['lg_items'] =  stripslashes($this->lang->line('items'));
		else
		$mini_cart_lg['lg_items'] =  "items";

		if($this->lang->line('header_description') != '')
		$mini_cart_lg['lg_description'] =  stripslashes($this->lang->line('header_description'));
		else
		$mini_cart_lg['lg_description'] =  "Description";

		if($this->lang->line('qty') != '')
		$mini_cart_lg['lg_qty'] =  stripslashes($this->lang->line('qty'));
		else
		$mini_cart_lg['lg_qty'] =  "Qty";

		if($this->lang->line('giftcard_price') != '')
		$mini_cart_lg['lg_price'] =  stripslashes($this->lang->line('giftcard_price'));
		else
		$mini_cart_lg['lg_price'] =  "Price";

		if($this->lang->line('order_sub_total') != '')
		$mini_cart_lg['lg_sub_tot'] =  stripslashes($this->lang->line('order_sub_total'));
		else
		$mini_cart_lg['lg_sub_tot'] =  "Order Sub Total";

		if($this->lang->line('proceed_to_checkout') != '')
		$mini_cart_lg['lg_proceed'] =  stripslashes($this->lang->line('proceed_to_checkout'));
		else
		$mini_cart_lg['lg_proceed'] =  "Proceed to Checkout";
	
		if($this->lang->line('lg_proceed_auth') != '')
		$mini_cart_lg['lg_proceed_auth'] =  stripslashes($this->lang->line('proceed_to_auth'));
		else
		$mini_cart_lg['lg_proceed_auth'] =  "Proceed to Authorization";

		/***Mini cart Lg****/

		$this->data['MiniCartViewSet']        = $this->minicart_model->mini_cart_view($this->data['common_user_id'],$mini_cart_lg);
		$this->data['AuctionMiniCartViewSet'] = $this->minicart_model->auctionmini_cart_view($this->data['common_user_id'],$mini_cart_lg);

                /******Remove free sellers updated by admin*****/
                $now = date("Y-m-d h:i:s");
                $Query = "SELECT user_id FROM ".STORE_FRONT." WHERE expiresOn < '".$now."' AND expiresOn != ''";
                $expiredUsers = $this->pricing_model->ExecuteQuery($Query);
                if($expiredUsers->num_rows > 0){
                    foreach($expiredUsers->result() as $expiredId){
                        $expiredList[] = $expiredId->user_id;
                    }
                    $Query = "DELETE FROM ".STORE_FRONT." WHERE user_id IN (".implode(',',$expiredList).") ";
                    $this->pricing_model->ExecuteQuery($Query);
                    $Query = "DELETE FROM ".STORE_ARB." WHERE user_id IN (".implode(',',$expiredList).") AND payment_type = 'Free' ";
                    $this->pricing_model->ExecuteQuery($Query);
                    $Query = "UPDATE ".USERS." SET store_payment = 'Cancelled' WHERE id IN (".implode(',',$expiredList).") ";
                    $this->pricing_model->ExecuteQuery($Query);
                }
                /******Remove free sellers updated by admin*****/

                /*-Unread messages start-*/
		if($this->checkLogin('U') != ''){ 
			$this->load->model('user_model');
			if($this->uri->segment(1) == 'conversation'){
				$BidID = $this->uri->segment ( 2, 0 );
				$this->user_model->update_details(BID_MESSAGE,array( 'msg_read' => 'Yes','auction_read'=>'Yes' ), array( 'receiverId'=>$this->checkLogin( 'U' ),'BidId'=> $BidID ) );
				$this->user_model->update_details(BID_MESSAGE,array('rspn_read' => 'YES'),array('receiverId'=>$this->checkLogin( 'U'),'BidId'=> $BidId,'type'=>'auction' ) );
            }
			if($this->uri->segment(1) == 'auction' && $this->uri->segment(2) != ''){
				$auctionid = $this->uri->segment(2);
				$this->user_model->update_details(PAYMENT,array('read_status'=>'read'),array('id'=>$auctionid,'type'=>'auction'));			
			}
			$this->data['unread_messages_count'] = $this->user_model->get_unread_messages_count($this->checkLogin('U'));
			 // echo $this->db->last_query();die;
			$this->data['unread_auctions_count']   = $this->user_model->get_all_details(PAYMENT,array('type'=>'auction', 'read_status'=>'unread', 'user_id'=>$this->checkLogin('U')))->num_rows();
			
		}

		if($this->config->item('storefront_fees_month') == '0' && $this->config->item('storefront_fees_year') == '0'){
			$this->data['sellers_list'] = $this->user_model->get_all_details(USERS,array('group'=>'Seller','status'=>'Active'));
		}else{
			$condition = 'select u.*,s.store_name,s.logo_image, Count(p.id) as ProductCount from '.USERS.' as u left join '.STORE_FRONT.' as s on s.user_id = u.id left join '.PRODUCT.' as p on u.id = p.user_id where u.group ="Seller" and u.store_payment ="Paid" and p.status="Publish" and u.status = "Active" and NOT FIND_IN_SET(u.id ,"'.implode(",", $featuredId).'") group by u.id';
			$this->data['sellers_list'] = $sellers_list = $this->user_model->ExecuteQuery($condition);
		}

		/*** Brands  ***/
			$this->data['brandslist'] = $this->minicart_model->get_all_details(BRAND,array('status'=>'Active'),array(array('field'=>'brand_name','type'=>'asc')));
		/*** Brands  ***/
		if( $this->uri->segment(1) == 'things'){ 
			$this->data ['conditionsList'] = $this->product_model->get_all_details(CONDITION,array('status'=>'Active'));
		}
	}
	public function search_brands(){
		$search_keyword = $this->input->get('search_keyword');
		if( $search_keyword != ''){
			$brandslist = $this->data['brandslist']= $this->minicart_model->get_all_details(BRAND,array('status'=>'Active'),array(array('field'=>'brand_name','type'=>'asc')),array('brand_name'=>$search_keyword));
			//echo $this->db->last_query();
			//echo '<pre>';print_r($brandslist->result());die;
				//$brands .='<li><span><a href="shopby/all?b='.$brand->brand_seourl.'" >'.$brand->brand_name.'</a></span></li>';
				$this->load->view('site/templates/brands',$this->data);
		}else if($search_keyword == ''){
			$this->datap['brandslist'] = $this->minicart_model->get_all_details(BRAND,array('status'=>'Active'),array(array('field'=>'brand_name','type'=>'asc')));
			$this->load->view('site/templates/brands',$this->data);
		}
		//echo $brands;		 
	}
	/**
	 *
	 * This function return the session value based on param
	 * @param $type
	 */
	public function checkLogin($type=''){
		if ($type == 'A'){
			return $this->session->userdata('fc_session_admin_id');
		}else if ($type == 'N'){
			return $this->session->userdata('fc_session_admin_name');
		}else if ($type == 'M'){
			return $this->session->userdata('fc_session_admin_email');
		}else if ($type == 'P'){
			return $this->session->userdata('fc_session_admin_privileges');
		}else if ($type == 'U'){
			return $this->session->userdata('fc_session_user_id');
		}else if ($type == 'T'){
			return $this->session->userdata('fc_session_temp_id');

		}
	}

	/**
	 *
	 * This function set the error message and type in session
	 * @param string $type
	 * @param string $msg
	 */
	public function setErrorMessage($type='',$msg=''){
		($type == 'success') ? $msgVal = 'message-green' : $msgVal = 'message-red';
		$this->session->set_flashdata('sErrMSGType', $msgVal);
		$this->session->set_flashdata('sErrMSG', $msg);
	}
	/**
	 *
	 * This function check the admin privileges
	 * @param String $name	->	Management Name
	 * @param Integer $right	->	0 for view, 1 for add, 2 for edit, 3 delete
	 */
	public function checkPrivileges($name='',$right=''){
		$prev = '0';
		$privileges = $this->session->userdata('fc_session_admin_privileges');
		extract($privileges);
		$userName =  $this->session->userdata('fc_session_admin_name');
		$adminName = $this->config->item('admin_name');
		if ($userName == $adminName){
			$prev = '1';
		}
		if (isset(${$name}) && is_array(${$name}) && in_array($right, ${$name})){
			$prev = '1';
		}
		if ($prev == '1'){
			return TRUE;
		}else {
			return FALSE;
		}
	}

	/**
	 *
	 * Generate random string
	 * @param Integer $length
	 */
	public function get_rand_str($length='6'){
		return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
	}

	public function get_rand_num($length='6'){
		return substr(str_shuffle("01234567897418963285274123741237418963284163574189632741236909876543210"), 0, $length);
	}

	/**
	 *
	 * Unsetting array element
	 * @param Array $productImage
	 * @param Integer $position
	 */
	public function setPictureProducts($productImage,$position){
		unset($productImage[$position]);
		return $productImage;
	}

	/**
	 *
	 * Resize the image
	 * @param int target_width
	 * @param int target_height
	 * @param string image_name
	 * @param string target_path
	 */
	public function imageResizeWithSpace($box_w,$box_h,$userImage,$savepath){

		$thumb_file = $savepath.$userImage;

		list($w, $h, $type, $attr) = getimagesize($thumb_file);

		$size=getimagesize($thumb_file);
		switch($size["mime"]){
			case "image/jpeg":
				$img = imagecreatefromjpeg($thumb_file); //jpeg file
				break;
			case "image/gif":
				$img = imagecreatefromgif($thumb_file); //gif file
				break;
			case "image/png":
				$img = imagecreatefrompng($thumb_file); //png file
				break;

			default:
				$im=false;
				break;
		}

		$new = imagecreatetruecolor($box_w, $box_h);
		if($new === false) {
			//creation failed -- probably not enough memory
			return null;
		}


		$fill = imagecolorallocate($new, 255, 255, 255);
		imagefill($new, 0, 0, $fill);

		//compute resize ratio
		$hratio = $box_h / imagesy($img);
		$wratio = $box_w / imagesx($img);
		$ratio = min($hratio, $wratio);

		if($ratio > 1.0)
		$ratio = 1.0;

		//compute sizes
		$sy = floor(imagesy($img) * $ratio);
		$sx = floor(imagesx($img) * $ratio);

		$m_y = floor(($box_h - $sy) / 2);
		$m_x = floor(($box_w - $sx) / 2);

		if(!imagecopyresampled($new, $img,
		$m_x, $m_y, //dest x, y (margins)
		0, 0, //src x, y (0,0 means top left)
		$sx, $sy,//dest w, h (resample to this size (computed above)
		imagesx($img), imagesy($img)) //src w, h (the full size of the original)
		) {
			//copy failed
			imagedestroy($new);
			return null;

		}
		imagedestroy($i);
		imagejpeg($new, $thumb_file, 99);

	}

	public function crop_and_resize_image($new_width,$new_height,$file_path,$file_name,$des_path){

		define ('MAX_WIDTH', 1500);//max image width
		define ('MAX_HEIGHT', 1500);//max image height
		define ('MAX_FILE_SIZE', 10485760);
		$file = $file_path.$file_name;
		//iamge save path
		$path = $des_path;

		//size of the resize image
		//    $new_width = 128;
		//   $new_height = 128;

		//name of the new image
		$nameOfFile = $file_name;

		$image_type = $file['type'];
		$image_size = $file['size'];
		$image_error = $file['error'];
		$image_file = $file['tmp_name'];
		$image_name = $file['name'];

		$image_info = getimagesize($file);

		//check image type
		if ($image_info['mime'] == 'image/jpeg' or $image_info['mime'] == 'image/jpg'){
		}
		else if ($image_info['mime'] == 'image/png'){
		}
		else if ($image_info['mime'] == 'image/gif'){
		}
		else{
			//set error invalid file type
		}

		if ($image_error){
			//set error image upload error
		}

		if ( $image_size > MAX_FILE_SIZE ){
			//set error image size invalid
		}

		switch ($image_info['mime']) {
			case 'image/jpg': //This isn't a valid mime type so we should probably remove it
			case 'image/jpeg':
				$image = imagecreatefromjpeg ($file);
				break;
			case 'image/png':
				$image = imagecreatefrompng ($file);
				break;
			case 'image/gif':
				$image = imagecreatefromgif ($file);
				break;
		}

		if ($new_width == 0 && $new_height == 0) {
			$new_width = 100;
			$new_height = 100;
		}

		// ensure size limits can not be abused
		$new_width = min ($new_width, MAX_WIDTH);
		$new_height = min ($new_height, MAX_HEIGHT);

		//get original image h/w
		$width = imagesx ($image);
		$height = imagesy ($image);

		//$align = 'b';
		$zoom_crop = 1;
		$origin_x = 0;
		$origin_y = 0;
		//TODO setting Memory

		// generate new w/h if not provided
		if ($new_width && !$new_height) {
			$new_height = floor ($height * ($new_width / $width));
		} else if ($new_height && !$new_width) {
			$new_width = floor ($width * ($new_height / $height));
		}

		// scale down and add borders
		if ($zoom_crop == 3) {

			$final_height = $height * ($new_width / $width);

			if ($final_height > $new_height) {
				$new_width = $width * ($new_height / $height);
			} else {
				$new_height = $final_height;
			}

		}

		// create a new true color image
		$canvas = imagecreatetruecolor ($new_width, $new_height);
		imagealphablending ($canvas, false);


		//        if (strlen ($canvas_color) < 6) {
		$canvas_color = 'ffffff';
		//       }

		$canvas_color_R = hexdec (substr ($canvas_color, 0, 2));
		$canvas_color_G = hexdec (substr ($canvas_color, 2, 2));
		$canvas_color_B = hexdec (substr ($canvas_color, 2, 2));

		// Create a new transparent color for image
		$color = imagecolorallocatealpha ($canvas, $canvas_color_R, $canvas_color_G, $canvas_color_B, 127);

		// Completely fill the background of the new image with allocated color.
		imagefill ($canvas, 0, 0, $color);

		// scale down and add borders
		if ($zoom_crop == 2) {

			$final_height = $height * ($new_width / $width);

			if ($final_height > $new_height) {
				$origin_x = $new_width / 2;
				$new_width = $width * ($new_height / $height);
				$origin_x = round ($origin_x - ($new_width / 2));
			} else {

				$origin_y = $new_height / 2;
				$new_height = $final_height;
				$origin_y = round ($origin_y - ($new_height / 2));

			}

		}

		// Restore transparency blending
		imagesavealpha ($canvas, true);

		if ($zoom_crop > 0) {

			$src_x = $src_y = 0;
			$src_w = $width;
			$src_h = $height;

			$cmp_x = $width / $new_width;
			$cmp_y = $height / $new_height;

			// calculate x or y coordinate and width or height of source
			if ($cmp_x > $cmp_y) {
				$src_w = round ($width / $cmp_x * $cmp_y);
				$src_x = round (($width - ($width / $cmp_x * $cmp_y)) / 2);
			} else if ($cmp_y > $cmp_x) {
				$src_h = round ($height / $cmp_y * $cmp_x);
				$src_y = round (($height - ($height / $cmp_y * $cmp_x)) / 2);
			}

			// positional cropping!
			$align = false;
			if ($align) {
				if (strpos ($align, 't') !== false) {
					$src_y = 0;
				}
				if (strpos ($align, 'b') !== false) {
					$src_y = $height - $src_h;
				}
				if (strpos ($align, 'l') !== false) {
					$src_x = 0;
				}
				if (strpos ($align, 'r') !== false) {
					$src_x = $width - $src_w;
				}
			}

			// positional cropping!
			imagecopyresampled ($canvas, $image, $origin_x, $origin_y, $src_x, $src_y, $new_width, $new_height, $src_w, $src_h);

		} else {
			imagecopyresampled ($canvas, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
		}
		//Straight from Wordpress core code. Reduces filesize by up to 70% for PNG's
		if ( (IMAGETYPE_PNG == $image_info[2] || IMAGETYPE_GIF == $image_info[2]) && function_exists('imageistruecolor') && !imageistruecolor( $image ) && imagecolortransparent( $image ) > 0 ){
			imagetruecolortopalette( $canvas, false, imagecolorstotal( $image ) );
		}
		$quality = 100;
		$nameOfFile = $file_name;

		if (preg_match('/^image\/(?:jpg|jpeg)$/i', $image_info['mime'])){
			imagejpeg($canvas, $path.$nameOfFile, $quality);

		} else if (preg_match('/^image\/png$/i', $image_info['mime'])){
			imagepng($canvas, $path.$nameOfFile, floor($quality * 0.09));

		} else if (preg_match('/^image\/gif$/i', $image_info['mime'])){
			imagegif($canvas, $path.$nameOfFile);

		}
	}

	// /*Push notification process starts*/
// /**
//    * This function send the notification for mobile app
//    * @param Array $regIds
//    * @param String $message
//    * @param string $action
//    * @param string $type (ANDROID/IOS)
//    * @param Array $urlval
//    * */
public function sendPushNotification($regIds, $message = '', $action = '', $type = '', $urlval = array()) {
	if ($message != '') {
		$msg = array();
		$msg ['message'] = $message;
		$msg ['action'] = $action;
		$i = 1;
		foreach ($urlval as $vals) {
		   if ($vals != '') {
				$msg['key' . $i] = (string) $vals;
				$i++;
		   }
		}
		if (is_array($regIds)) {
			$regIds = $regIds;
		} else {
			$regIds = array($regIds);
		}
		if (!empty($regIds) && $type == 'ANDROID') {
			$this->sendPushNotificationToGCM($regIds, $msg);
		}
		if (!empty($regIds) && $type == 'IOS') {
			//Function for APNS
		}
	}
}
//
// /**
//  * This function send the notification for Anriod app
//  * @param string $registration_ids
//  * @param string $message
//  * */
public function sendPushNotificationToGCM($registration_ids, $message) {

	//Google cloud messaging GCM-API url
	$url = 'https://android.googleapis.com/gcm/send';
	if (!is_array($registration_ids)) {
		$registration_ids = array($registration_ids);
	}
	if (!is_array($message)) {
		$message = array("message" => $message);
	}

	$fields = array(
		'registration_ids' => $registration_ids,
		'data' => $message,
	);
	// Google Cloud Messaging GCM API Key
//$google_key = "AIzaSyAPfXC_t6rN4T8UieD9cidYoJKAlFIaK-4";
	$google_key = "AIzaSyDsraHlprdKyeSa8n-JtFaFCGOJGDFfF-Y";
	define("GOOGLE_API_KEY", $google_key);
	$headers = array(
		'Authorization: key=' . GOOGLE_API_KEY,
		'Content-Type: application/json'
	);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
	$result = curl_exec($ch);
	if ($result === FALSE) {
		die('Curl failed: ' . curl_error($ch));
	}
	curl_close($ch);
  //  var_dump($result);
	return $result;
}




}
