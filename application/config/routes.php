<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	http://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There area two reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router what URI segments to use if those provided
  | in the URL cannot be matched to a valid route.
  |
 */
$route['default_controller'] = "site/shop";
$route['404_override'] = '';
$route['shop'] = "site/landing";
$route['admin'] = "admin/adminlogin";
$route['signup'] = "site/user/signup_form";
$route['login'] = "site/user/login_form";
$route['a-to-z-of-brands'] = "site/product/display_brands";
$route['logout'] = "site/user/logout_user";
$route['forgot-password'] = "site/user/forgot_password_form";
$route['send-confirm-mail'] = "site/user/send_quick_register_mail";
$route['onboarding'] = "site/product/onboarding";
$route['gift-cards'] = "site/giftcard";
$route['cart'] = "site/cart";
$route['auctioncart'] = "site/cart/auctioncart";
$route['checkout/(:any)'] = "site/checkout";
$route['order/(:any)'] = "site/order";
$route['settings'] = "site/user_settings";
$route['settings/password'] = "site/user_settings/password_settings";
$route['settings/preferences'] = "site/user_settings/preferences_settings";
$route['settings/notifications'] = "site/user_settings/notifications_settings";
$route['purchases'] = "site/user_settings/user_purchases";
//$route['gifts/list'] = "site/user_settings/user_group_gifts";
$route['orders'] = "site/user_settings/user_orders";
$route['bulk'] = "site/user_settings/bulk_upload";
$route['fancyybox/manage'] = "site/user_settings/manage_fancyybox";
$route['settings/shipping'] = "site/user_settings/shipping_settings";
$route['credits'] = "site/user_settings/user_credits";
$route['referrals/(:any)'] = "site/user_settings/user_referrals/$1";
$route['referrals'] = "site/user_settings/user_referrals";
$route['settings/giftcards'] = "site/user_settings/user_giftcards";
$route['things/(:any)/change_status/(:any)'] = "site/product/change_status/$1";
$route['things/(:any)/edit'] = "site/product/edit_product_detail/$1";
$route['things/(:any)/edit/(:any)'] = "site/product/edit_product_detail/$1";
$route['things/(:any)/delete'] = "site/product/delete_product/$1";
$route['things/shuffle'] = "site/product/display_product_shuffle";
$route['things/(:any)'] = "site/product/display_product_detail/$1";
$route['user/(:any)/added'] = "site/user/display_user_added";
$route['user/(:any)/lists'] = "site/user/display_user_lists";
$route['user/(:any)/lists/(:num)'] = "site/user/display_user_lists_home";
$route['user/(:any)/lists/(:num)/followers'] = "site/user/display_user_lists_followers";
$route['user/(:any)/lists/(:num)/settings'] = "site/user/edit_user_lists";
$route['user/(:any)/wants'] = "site/user/display_user_wants";
$route['user/(:any)/owns'] = "site/user/display_user_owns";
$route['user/(:any)/follows'] = "site/user/display_user_follow";
$route['user/(:any)/following'] = "site/user/display_user_following/$1";
$route['user/(:any)/followers'] = "site/user/display_user_followers/$1";
$route['user/(:any)/things/(:any)'] = "site/product/display_user_thing/$1";
$route['user/seller-timeline/(:any)'] = "site/user/seller_timeline/$1";
$route['user/(:any)'] = "site/user/display_user_profile/$1";
$route['shopby/(:any)'] = "site/searchShop/search_shopby/$1";
$route['stores'] = "site/seller";
$route['featured'] = "site/user_settings/seller_featured";
$route['featured_plan'] = "site/user_settings/view_featured_plan";
$route['featured_plan_pay'] = "site/user_settings/seller_plan_pay";
$route['giftguide/list/(:any)/followers'] = "site/searchShop/search_priceby_followers/$1";
$route['colorsby/list/(:any)/followers'] = "site/searchShop/search_colorby_followers/$1";
$route['giftguide/list/(:any)'] = "site/searchShop/search_priceby/$1";
$route['colorsby/list/(:any)'] = "site/searchShop/search_colorby/$1";
$route['fancybox'] = "site/fancybox";
$route['fancybox/(:any)/(:any)'] = "site/fancybox/display_fancybox/$1";
$route['sales/create'] = "site/product/sales_create";
$route['seller-signup'] = "site/user/seller_signup_form";
$route['pages/(:num)/(:any)'] = "site/cms/page_by_id";
$route['pages/(:any)'] = "site/cms";
$route['create-brand'] = "site/user/create_brand_form";
$route['view-purchase/(:any)'] = "site/user/view_purchase";
$route['view-order/(:any)'] = "site/user/view_order";
$route['order-review/(:num)/(:num)/(:any)'] = "site/user/order_review";
$route['order-review/(:num)'] = "admin/order/order_review";
$route['update-status/(:any)'] = "site/user_settings/update_buyer_status/$1";
$route['image-crop/(:any)'] = "site/user/image_crop";
$route['lang/(:any)'] = "site/fancybox/language_change/$1";
$route['notifications'] = "site/notify/display_notifications";
$route['payment-success'] = "admin/commission/display_payment_success";
$route['payment-failed'] = "admin/commission/display_payment_failed";
$route['cod-payment'] = "site/user_settings/user_cod_payment";
$route['bookmarklet'] = "site/bookmarklet";
$route['bookmarklets'] = "site/bookmarklet/display_bookmarklet";
$route['invite-friends'] = "site/user/invite_friends";
$route['feedback/(:num)'] = "site/user/display_user_product_feedback/$1";
$route['t/(:any)'] = "site/product/load_short_url";
$route['check-extension'] = "site/landing/check_extension";

/**** Auction && Bids ***/
$route['bids'] = "site/user/bids";
$route['auctions'] = "site/user/auction_prodcut_list";
$route['auction/(:any)'] = "site/user/auction_detail";
$route['conversation/(:any)'] = "site/user/conversation/$1";
$route['send_message'] = "site/user/send_message";
$route['send_bid'] = "site/user/send_offer";
$route['update_bid'] = "site/user/update_offer";
$route['accept_bid'] = "site/user/accept_offer";
$route['decline_bid'] = "site/user/decline_offer";
$route['reject_bid'] = "site/user/reject_offer";
/**** Auction && Bids ***/

/* * ************GROUP GIFTS*********** */
$route['group-gifts'] = "site/groupgift";
$route['refund/(:any)'] = "site/groupgift/RefundPaymentCredit";
$route['gifts/contribute/(:any)'] = "site/groupgift/payment_contribute_details/$1";
$route['gifts/(:any)'] = "site/groupgift/display_groupProduct_detail/$1";
$route['groupgift/order/(:any)'] = "site/groupgift/order_payment/$1";
$route['groupgift/payseller/(:any)'] = "site/groupgift/seller_payment/$1";
/* * ********GROUP GIFTS END********** */
/* * ************Store routing ******** */
$route['open-store'] = "site/store/open_store";
$route['store/(:any)'] = "site/store/view_store";
$route['seller/dashboard'] = "site/store/settings";
$route['seller/dashboard/about'] = "site/store/about_settings";
$route['seller/dashboard/profile'] = "site/store/profile_settings";
$route['seller/dashboard/privacy'] = "site/store/privacy_settings";
$route['seller/dashboard/products'] = "site/store/product_settings";
$route['seller/dashboard/orders'] = "site/store/order_settings";
$route['seller/dashboard/shop-settings'] = "site/store/shop_settings";
$route['seller-product'] = "site/product/seller_product_add";





/*
* !! >>>>>>>>>>> Added by shayam <<<<<<<<<<< !!
*/

/*  || <<<<<<< Homepage Details >>>>>>> || */
$route['json/homepage'] = "site/mobile/homepage";       // Completed
/*  || <<<<<<< Homepage Details >>>>>>> || */

/*  || <<<<<<< User Login and Forgot Password >>>>>>> || */
$route['json/user/register'] = "site/mobile/user_register";      // Completed !!
$route['json/user/login'] = "site/mobile/user_login";        // Completed
$route['json/forgot_password'] = "site/mobile/forgot_password_user";        // Completed
$route['json/googlelogin'] = "site/mobile/google_login";        // Completed
$route['json/twitterlogin'] = "site/mobile/twitter_login";          // Completed
$route['json/facebooklogin'] = "site/mobile/facebookLogin";        // Completed
/*  || <<<<<<< User Login and Forgot Password >>>>>>> || */


/*  || <<<<<<< Product Details >>>>>>> || */
$route['json/product'] = "site/mobile/product";         // Completed
$route['json/featuredproduct'] = "site/mobile/featured_product";        // Completed
$route['json/product/productComment'] = "site/mobile/insert_product_comment";   // Completed
$route['json/product/deleteProductComment'] = "site/mobile/deleteProductComment";   // Completed
$route['json/product/ApproveProductComment'] = "site/mobile/ApproveProductComment";   // Completed
$route['json/product/productLike'] = "site/mobile/add_fancy_item"; // Completed
$route['json/product/productUnLike'] = "site/mobile/remove_fancy_item"; // Completed
$route['json/product/AddProductToList'] = "site/mobile/add_list_when_fancyy"; // Completed
$route['json/product/AddedProductToList'] = "site/mobile/add_item_to_lists"; // Completed
$route['json/product/RemoveProductFromList'] = "site/mobile/remove_item_from_lists"; // Completed
$route['json/product/CreateProductList'] = "site/mobile/create_list"; // Completed
$route['json/product/AddProductToWantList'] = "site/mobile/add_want_tag"; // Completed
$route['json/product/RemoveProductFromWantList'] = "site/mobile/delete_want_tag"; // Completed
$route['json/product/AddProductToOwnList'] = "site/mobile/add_have_tag"; // Completed
$route['json/product/RemoveProductOwnList'] = "site/mobile/delete_have_tag"; // Completed
$route['json/AddSellerProductForm'] = "site/mobile/AddSellerProductForm";   // Completed
$route['json/product/addProductsToCart'] = "site/mobile/addProductsToCart"; // Completed
$route['json/product/productCartAttributreCountCheck'] = "site/mobile/productCartAttributreCountCheck"; // Completed

/*  || <<<<<<< Product Details >>>>>>> || */


/*  || <<<<<<< User Product Details >>>>>>> || */
$route['json/userproduct'] = "site/mobile/userproduct";
/*  || <<<<<<< User Product Details >>>>>>> || */

/*  || <<<<<<< User Product Details >>>>>>> || */
$route['json/user/notification'] = "site/mobileNotify";
/*  || <<<<<<< User Product Details >>>>>>> || */



/*  || <<<<<<< User List and Timeline Details >>>>>>> || */
$route['json/user/user_list'] = "site/mobile/display_user_lists";       // Completed
$route['json/user/lists'] = "site/mobile/display_user_lists_home";   // Completed
$route['json/user/seller-timeline']="site/mobile/seller_timeline";      // Completed
/*  || <<<<<<< User List and Timeline Details >>>>>>> || */


/*  || <<<<<<< Open Store Routes >>>>>>> || */
$route['json/getStoreFees'] = "site/mobile/getStoreFees"; // Completed
$route['json/openStore'] = "site/mobile/openStore";   // Completed
$route['json/store'] = "site/mobile/store";   // Completed
$route['json/storeDetails'] = "site/mobile/storeDetails";   // Completed
$route['json/InsertEditstore'] = "site/mobile/InsertEditstore";   // Completed
$route['json/deleteStoreImage'] = "site/mobile/deleteStoreImage";   // Completed
$route['json/storePrivacySettings'] = "site/mobile/storePrivacySettings";   // Completed
$route['json/storePrivacyUpdate'] = "site/mobile/storePrivacyUpdate";   // Completed
$route['json/storeAboutSettings'] = "site/mobile/storeAboutSettings";   // Completed
$route['json/storeAboutUpdate'] = "site/mobile/storeAboutUpdate";   // Completed
$route['json/productSettings'] = "site/mobile/productSettings";   // Completed
$route['json/ProductChangeStatus'] = "site/mobile/ProductChangeStatus";   // Completed
/*  || <<<<<<< Open Store Routes >>>>>>> || */


/*  || <<<<<<< View Store >>>>>>> || */
$route['json/viewstore'] = "site/mobile/view_store";        // Completed
/*  || <<<<<<< View Store >>>>>>> || */


/*  || <<<<<<< List Routes >>>>>>> || */
$route['json/user/lists/settings'] = "site/mobile/edit_user_lists";     // Completed
$route['json/user/lists/SaveListSettings'] = "site/mobile/edit_user_list_details";     // Completed
$route['json/user/lists/DeleteListSettings'] = "site/mobile/delete_user_list";     // Completed
/*  || <<<<<<< List Routes >>>>>>> || */

/*  || <<<<<<< Affiliate Products Routes >>>>>>> || */
$route['json/CatForAffProduct'] = "site/mobile/CatForAffProduct";  // Completed
$route['json/UploadAffProductImage'] = "site/mobile/UploadAffProductImage";  // Completed
$route['json/UploadAffProduct'] = "site/mobile/UploadAffProduct";  // Completed
/*  || <<<<<<< Affiliate Products Routes >>>>>>> || */

/*  || <<<<<<< Seller Signup Routes >>>>>>> || */
$route['json/sellerSignupForm'] = "site/mobile/sellerSignupForm"; // Completed
$route['json/sellerSignup'] = "site/mobile/seller_signup"; // Completed
/*  || <<<<<<< Seller Signup Routes >>>>>>> || */

/*  || <<<<<<< Follow & Unfollow Routes >>>>>>> || */
$route['json/addFollow'] = "site/mobile/addFollow"; // Completed
$route['json/deleteFollow'] = "site/mobile/deleteFollow"; // Completed
/*  || <<<<<<< Follow & Unfollow Routes >>>>>>> || */

/*  || <<<<<<< Search & Shopby Routes >>>>>>> || */
$route['json/searchSuggestions'] = "site/mobile/searchSuggestions"; // Completed
$route['json/shopby/(:any)'] = "site/mobile/search_shopby";   // Completed
$route['json/shop'] = "site/mobile/shopPage";       // Completed
$route['json/countryCategory'] = "site/mobile/countryCategory";       // Completed
$route['json/countryList'] = "site/mobile/coutryList";       // Completed
/*  || <<<<<<< Search & Shopby Routes >>>>>>> || */

/*  || <<<<<<< Cart Routes >>>>>>> || */
$route['json/cart'] = "site/mobile/cart";  // Completed
$route['json/updateCartQuantity'] = "site/mobile/updateCartQuantity";  // Completed
$route['json/removeCartProduct'] = "site/mobile/removeCartProduct";  // Completed
$route['json/cart/ShippingDeleteAddress'] = "site/mobile/ShippingDeleteAddress";   // Completed
$route['json/insertEditShippingAddress'] = "site/mobile/insertEditShippingAddress";   // Completed
$route['json/CheckCoupenCode'] = "site/mobile/CheckCoupenCode"; // Completed
$route['json/removeCoupenCode'] = "site/mobile/removeCoupenCode"; // Completed
$route['json/cart/ChangeShippingAddress'] = "site/mobile/ChangeShippingAddress"; // Completed
$route['json/cart/digitalFileDownload'] = "site/mobile/digitalDownload";
/*  || <<<<<<< Cart Routes >>>>>>> || */


/*  || <<<<<<< Checkout Routes >>>>>>> || */
$route['json/checkout/(:any)'] = "site/mobileCheckout";   // Completed
$route['mobileOrder/(:any)'] = "site/mobileOrder";    // Completed
$route['json/cartCheckout'] = "site/mobile/cartCheckout";  // Completed
/*  || <<<<<<< Checkout Routes >>>>>>> || */


/*  || <<<<<<< User Settings Routes >>>>>>> || */
$route['json/userPurchases'] = "site/mobile/userPurchases";    // Completed
$route['json/userProfileSettings'] = "site/mobile/userProfileSettings";    // Completed
$route['json/UploadUserImage'] = "site/mobile/UploadUserImage";  // Completed
$route['json/updateUserProfile'] = "site/mobile/updateUserProfile";  // Completed
$route['json/userPreferencesSettings'] = "site/mobile/userPreferencesSettings";  // Completed
$route['json/updateUserPreferences'] = "site/mobile/updateUserPreferences";  // Completed
$route['json/changeUserPassword'] = "site/mobile/changeUserPassword";  // Completed
$route['json/UserNotifications'] = "site/mobile/UserNotifications";  // Completed
$route['json/updateUserNotifications'] = "site/mobile/updateUserNotifications";  // Completed
$route['json/fancyybox/manage'] = "site/mobile/manageFancyybox";       // Completed
$route['json/settings/shipping'] = "site/mobile/shippingSettings";     // Completed
$route['json/settings/giftcards'] = "site/mobile/userGiftcards";     // Completed
$route['json/userOrders'] = "site/mobile/userOrders";  // Completed
$route['json/changeShippingStatus'] = "site/mobile/changeShippingStatus";  // Completed
$route['json/updateBuyerStatus'] = "site/mobile/updateBuyerStatus";  // Completed
$route['json/settings/GroupGifts'] = "site/mobile/GroupGifts";     // Completed
$route['json/settings/sellerFeaturedPlan'] = "site/mobile/sellerFeaturedPlan";     // Completed
$route['json/settings/checkFeaturedSeller'] = "site/mobile/checkFeaturedSeller";     // Completed
$route['json/settings/viewPurchaseInvoice'] = "site/mobile/viewPurchaseInvoice";     // Completed
$route['json/settings/productReview'] = "site/mobile/productReview";     // Completed
$route['json/productComment'] = "site/mobile/post_order_comment";
$route['mobilefeedback/(:any)'] = "site/mobile/userProductFeedback";
$route['json/settings/sellerFeatured'] = "site/mobile/sellerFeatured";
$route['json/settings/payFeaturedPlan'] = "site/mobile/payFeaturedPlan";
$route['json/settings/addSellerFeaturedProducts'] = "site/mobile/addSellerFeaturedProducts";
$route['json/getGiftCards'] = "site/mobile/getGiftCards";
/*  || <<<<<<< User Settings Routes >>>>>>> || */


/*  || <<<<<<< Selling Product Details >>>>>>> || */
$route['json/product/addSellerProductDetail'] = "site/mobile/addSellerProductDetail";  // Completed
$route['json/product/updateSellingProduct'] = "site/mobile/updateSellingProduct";   // Completed
$route['json/product/deleteSellerProduct'] = "site/mobile/deleteSellerProduct";   // Completed
$route['json/product/categoryDetails'] = "site/mobile/sellerProductCategoryDetails";   // Completed
$route['json/product/listDetails'] = "site/mobile/sellerProductListDetails";   // Completed
$route['json/product/SellingProductImages'] = "site/mobile/SellingProductImages";   // Completed
$route['json/product/sellerProductAttributesDetails'] = "site/mobile/sellerProductAttributesDetails";   // Completed
$route['json/product/sellerProductRemoveAttributesDetails'] = "site/mobile/sellerProductRemoveAttributesDetails"; // Completed
$route['json/product/sellerProductSeoDetails'] = "site/mobile/sellerProductSeoDetails"; // Completed
$route['json/product/sellerProductShippingDetails'] = "site/mobile/sellerProductShippingDetails"; // Completed
$route['json/product/sellingProductDocDetails'] = "site/mobile/sellingProductDocDetails"; // Completed
$route['json/getProductType'] = "site/mobile/getProductType"; // Completed
$route['json/deleteProductImage'] = "site/mobile/deleteProductImage";
$route['json/product/ContactForm'] = "site/mobile/contactform";
/*  || <<<<<<< Selling Product Details >>>>>>> || */

$route['json/buyGiftCard'] = "site/mobile/buyGiftCard";
$route['json/giftCardPayment'] = "site/mobile/giftCardPayment";
$route['json/cmsPages'] = "site/mobile/cmsPages";
$route['json/resendConfirmationMail'] = "site/mobile/resendConfirmationMail";
/*
* !! >>>>>>>>>>> Added by shayam <<<<<<<<<<< !!
*/






/*
* !! >>>>>>>>>>>> Json Ios Routes <<<<<<<<<<<< !!
*/
// $route['ios/product/CreateProductList'] = "site/ios/create_list"; // Completed

$route['ios/user/lists/settings'] = "site/ios/edit_user_lists";     // Completed
$route['ios/user/lists/SaveListSettings'] = "site/ios/edit_user_list_details";     // Completed
$route['ios/user/lists/DeleteListSettings'] = "site/ios/delete_user_list";     // Completed
$route['ios/shop'] = "site/ios/shopPage";       // Completed
$route['ios/CatForAffProduct'] = "site/ios/CatForAffProduct";  // Completed
$route['ios/UploadAffProductImage'] = "site/ios/UploadAffProductImage";  // Completed
$route['ios/UploadAffProduct'] = "site/ios/UploadAffProduct";  // Completed
//$route['ios/shopby/(:any)'] = "site/ios/search_shopby";   // Completed
$route['ios/AddSellerProductForm'] = "site/ios/AddSellerProductForm";   // Completed
//$route['ios/checkout'] = "site/mobileCheckout";   // Completed
$route['mobileOrder/(:any)'] = "site/mobileOrder";    // Completed
// $route['ios/insertEditShippingAddress'] = "site/ios/insertEditShippingAddress";   // Completed
//$route['ios/product/ajaxProductDetailAttributeUpdate'] = "site/ios/ajaxProductDetailAttributeUpdate"; // Completed
$route['ios/product/addProductsToCart'] = "site/ios/addProductsToCart"; // Completed
//$route['ios/userPurchases'] = "site/ios/userPurchases";    // Completed
$route['ios/userProfileSettings'] = "site/ios/userProfileSettings";    // Completed
$route['ios/updateUserProfile'] = "site/ios/updateUserProfile";  // Completed
$route['ios/changeUserPassword'] = "site/ios/changeUserPassword";  // Completed
$route['ios/fancyybox/manage'] = "site/ios/manageFancyybox";       // Completed
$route['ios/settings/shipping'] = "site/ios/shippingSettings";     // Completed
$route['ios/settings/giftcards'] = "site/ios/userGiftcards";     // Completed
//$route['ios/changeShippingStatus'] = "site/ios/changeShippingStatus";  // Completed
$route['ios/updateBuyerStatus'] = "site/ios/updateBuyerStatus";  // Completed
$route['ios/settings/GroupGifts'] = "site/ios/GroupGifts";     // Completed
$route['ios/user/UploadUserImage'] = "site/ios/UploadUserImage";  // Completed
$route['ios/product/getShipping'] = "site/ios/getsubshipping";
$route['ios/product/inserEditShipping'] = "site/ios/inserEditSubshipping";



/*
* !! >>>>>>>>>>>> Json Ios Routes <<<<<<<<<<<< !!
*/




/*
    |||||| Fine Working Urls||||||
*/

$route['ios/user/login'] = "site/ios/userLogin";
$route['ios/user/register'] = "site/ios/userRegister";
$route['ios/facebookLogin'] = "site/ios/facebookLogin";
$route['ios/googleLogin'] = "site/ios/googleLogin";
$route['ios/twitterLogin'] = "site/ios/twitterLogin";
$route['ios/homepage'] = "site/ios/homepage";
$route['ios/product'] = "site/ios/product";
$route['ios/userproduct'] = "site/ios/userproduct";
$route['ios/product/productComments'] = "site/ios/productComments";
$route['ios/product/productComment'] = "site/ios/insert_product_comment";
$route['ios/product/deleteProductComment'] = "site/ios/deleteProductComment";
$route['ios/product/ApproveProductComment'] = "site/ios/ApproveProductComment";
$route['ios/searchSuggestions'] = "site/ios/searchSuggestions";
$route['ios/store'] = "site/ios/store";
$route['ios/viewstore'] = "site/ios/view_store";
$route['ios/user/user_list'] = "site/ios/display_user_lists";
$route['ios/user/lists'] = "site/ios/getUsersList";
$route['ios/user/changePrimaryAddress'] = "site/ios/changePrimaryAddress";
$route['ios/addFollow'] = "site/ios/addFollow";
$route['ios/deleteFollow'] = "site/ios/deleteFollow";
$route['ios/product/productLike'] = "site/ios/productLike";
$route['ios/product/productUnLike'] = "site/ios/productUnlike";
$route['ios/product/createProductList'] = "site/ios/createNewProductList";
$route['ios/product/productLitsDetails'] = "site/ios/ProductLitsDetails";
$route['ios/product/addProductToList'] = "site/ios/addProductToList";
$route['ios/product/removeProductFromList'] = "site/ios/removeProductFromList";
$route['ios/product/addProductToWantList'] = "site/ios/addProductToWantList";
$route['ios/product/removeProductFromWantList'] = "site/ios/removeProductFromWantList";
$route['ios/product/addProductToOwnList'] = "site/ios/addProductToOwnList";
$route['ios/product/removeProductOwnList'] = "site/ios/removeProductOwnList";
$route['ios/product/categoryDetails'] = "site/ios/sellerProductCategoryDetails";
$route['ios/user/seller-timeline']="site/ios/seller_timeline";
$route['ios/shop'] = "site/ios/shopPage";
$route['ios/shopby'] = "site/ios/searchShop";
$route['ios/product/sellingProductDetails'] = "site/ios/sellingProductDetails";
$route['ios/cart'] = "site/ios/cart";
$route['ios/updateCartQuantity'] = "site/ios/updateCartQuantity";
$route['ios/removeCartProduct'] = "site/ios/removeCartProduct";
$route['ios/deleteShippingAddress'] = "site/ios/deleteShippingAddress";
$route['ios/insertEditShippingAddress'] = "site/ios/insertEditShippingAddress";
$route['ios/checkCoupenCode'] = "site/ios/checkCoupenCode";
$route['ios/removeCoupenCode'] = "site/ios/removeCoupenCode";
$route['ios/product/insertEditSellingProduct'] = "site/ios/updateSellingProductDetails";
$route['ios/product/insertEditSellingProductCategory'] = "site/ios/insertEditSellingProductCategory";
$route['ios/product/sellerProductAttributesDetails'] = "site/ios/sellerProductAttributesDetails";
$route['ios/product/insertEditSellingProductAttribute'] = "site/ios/insertEditSellingProductAttribute";
$route['ios/product/sellerProductRemoveAttributesDetails'] = "site/ios/sellerProductRemoveAttributesDetails";
$route['ios/product/insertEditSellingProductList'] = "site/ios/insertEditSellingProductList";
$route['ios/product/SellingProductImages'] = "site/ios/SellingProductImages";
$route['ios/product/sellerProductSeoDetails'] = "site/ios/sellerProductSeoDetails";
$route['ios/product/updateProductSeoDetails'] = "site/ios/updateProductSeoDetails";
$route['ios/product/listDetails'] = "site/ios/sellerProductListDetails";
$route['ios/user/userProfile'] = "site/ios/user_profile";
$route['ios/user/userFollowersList'] = "site/ios/userFollowersList";
$route['ios/user/userFollowingList'] = "site/ios/userFollowingList";
$route['ios/user/sellerFeatured'] = "site/ios/sellerFeatured";
$route['ios/user/payFeaturedPlan'] = "site/ios/payFeaturedPlan";
$route['ios/user/addSellerFeaturedProducts'] = "site/ios/addSellerFeaturedProducts";
$route['ios/userShippingDetails'] = "site/ios/userShippingDetails";
$route['ios/settings/viewPurchaseInvoice'] = "site/ios/viewPurchaseInvoice";
$route['ios/userPurchases'] = "site/ios/userPurchases";
$route['ios/sellerSignup'] = "site/ios/sellerSignUp";
$route['ios/sellerSignupForm'] = "site/ios/sellerSignupForm";
$route['ios/userOrders'] = "site/ios/userOrders";
$route['ios/changeShippingStatus'] = "site/ios/changeShippingStatus";
$route['ios/userPreferencesSettings'] = "site/ios/userPreferencesSettings";
$route['ios/updateUserPreferences'] = "site/ios/updateUserPreferences";
$route['ios/UserNotifications'] = "site/ios/UserNotifications";
$route['ios/updateUserNotifications'] = "site/ios/updateUserNotifications";
$route['ios/userNotifications'] = "site/iosNotify";
$route['ios/contactSeller'] = "site/ios/contactSeller";
$route['ios/storeDetails'] = "site/ios/storeDetails";
$route['ios/updateStore'] = "site/ios/updateStore";
$route['ios/deleteStoreImage'] = "site/ios/deleteStoreImage";
$route['ios/getStorePrivacyDetails'] = "site/ios/getStorePrivacyDetails";
$route['ios/storePrivacyUpdate'] = "site/ios/storePrivacyUpdate";
$route['ios/getStoreAboutDetails'] = "site/ios/getStoreAboutDetails";
$route['ios/storeAboutUpdate'] = "site/ios/storeAboutUpdate";
$route['ios/storeProducts'] = "site/ios/storeProducts";
$route['ios/ProductChangeStatus'] = "site/ios/ProductChangeStatus";
$route['ios/storeCOD'] = "site/ios/storeCOD";
$route['ios/updateCODStatus'] = "site/ios/updateCODStatus";
$route['ios/digitalProductDownload'] = "site/ios/digitalProductDownload";
$route['ios/buyGiftCard'] = "site/ios/buyGiftCard";
$route['ios/giftCardPayment'] = "site/ios/giftCardPayment";
$route['ios/getGiftCards'] = "site/ios/getGiftCards";
$route['ios/cmsPages'] = "site/ios/cmsPages";
$route['ios/forgotPassword'] = "site/ios/forgotPasswordUser";
$route['ios/resendConfirmationMail'] = "site/ios/resendConfirmationMail";
$route['ios/storeFeedback'] = "site/ios/storeFeedback";
$route['ios/productReview'] = "site/ios/productReview";
$route['ios/cartCheckout'] = "site/ios/cartCheckout";



/* have to check $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$  */
$route['ios/product/updateListDetails'] = "site/ios/updateListDetails";
$route['ios/product/updateListValueDetails'] = "site/ios/updateListValues";
$route['ios/product/UploadSellerProductImg'] = "site/ios/UploadsellerProductImage";
$route['ios/open-store'] = "site/ios_store/open_store";
$route['ios/openstoreForm'] = "site/ios_store/open_storeForm";
/* have to check $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$  */


/*
    |||||| Fine Working Urls||||||
*/
