<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This controller contains the functions related to condition mngmnt
 * @author Teamtweaks
 *
 */ 

class Condition extends MY_Controller {
 
	function __construct(){
        parent::__construct();
		$this->load->helper(array('cookie','date','form'));
		$this->load->library(array('encrypt','form_validation'));		
		$this->load->model('condition_model');
		if ($this->checkPrivileges('condition',$this->privStatus) == FALSE){
			redirect('admin');
		}
    }
    
    /**
     * 
     * This function loads the condition list page
     */
   	public function index(){	
	
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			redirect('admin/condition/display_condition_list');
		}
	}
	
	/**
	 * 
	 * This function loads the condition list page
	 */
	public function display_condition_list(){
		//echo "df";die;
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Condition Details';
			$this->data['ConditionList'] = $this->condition_model->get_all_details(CONDITION,array());
			$this->load->view('admin/condition/display_condition_list',$this->data);
		}
	}

	
	/**
	 * 
	 * This function loads the add new condition form
	 */
	public function add_condition_form(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Add Condition';
			$this->data['condition_id'] = $this->uri->segment(4,0);
			$this->load->view('admin/condition/add_condition',$this->data);
		}
	}
	
	
	
	/**
	 * 
	 * This function insert condition
	 */
	public function insertCondition(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
		
			$condition_name 		= $this->input->post('condition_name');
			$condition_description 	= $this->input->post('condition_description');
			$condition 			= array('condition_name' => $condition_name);
			$duplicate_name 	= $this->condition_model->get_all_details(CONDITION,$condition);
			if ($duplicate_name->num_rows() > 1){
				$this->setErrorMessage('error','Condition name already exists');
				redirect('admin/condition/add_condition_form');
			}
			$seourl 			= url_title($condition_name,'',TRUE);
			
			if ($this->input->post('status') != ''){
				$condition_status = 'Active';
			}else {
				$condition_status = 'Inactive';
			}
			if( $_FILES['condition_logo']['name'] !=''){
					
				$config['encrypt_name'] = TRUE;
				$config['overwrite'] = FALSE;
				$config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';
				$config['max_size'] = 2000;
				$config['upload_path'] = './images/brand';
				$this->load->library('upload', $config);
				if ( $this->upload->do_upload('condition_logo')){
					$logoDetails = $this->upload->data();
					$ImageName = $logoDetails['file_name'];
				}else{
					$logoDetails = $this->upload->display_errors();
					$this->setErrorMessage('error',$logoDetails);
				redirect('admin/condition/add_condition_form');
				}
				$condition_logo  =  $ImageName;
			}else{
				$condition_logo = '';
			}
			
			$dataArr = array( 'condition_name' => $condition_name,'condition_logo' => $condition_logo,'condition_description'=> $condition_description, 'status' => $condition_status,'condition_seourl'=>$seourl );
			
			$this->condition_model->simple_insert(CONDITION,$dataArr);
			$this->setErrorMessage('success','Condition added successfully');
			redirect('admin/condition/display_condition_list');
		}
	}
	
	/**
	 * 
	 * This function Edit condition
	 */
	public function EditCondition(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
		

			$condition_id			= $this->input->post('condition_id');			
			$condition_name			= $this->input->post('condition_name');
			$condition_description 	= $this->input->post('condition_description');
			$condition 			= array('condition_name' => $condition_name,'id !='=>$condition_id);
			$duplicate_name = $this->condition_model->get_all_details(CONDITION,$condition);
			if ($duplicate_name->num_rows() > 0){
				$this->setErrorMessage('error','Condition name already exists');
				redirect('admin/condition/edit_condition_form/'.$condition_id);
			}
			$condition = array('id' => $condition_id);
			
			//$seourl = url_title($condition_name,'',TRUE);
			
			if( $_FILES['condition_logo']['name'] !=''){
					
				$config['encrypt_name'] = TRUE;
				$config['overwrite'] = FALSE;
				$config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';
				$config['max_size'] = 2000;
				$config['upload_path'] = './images/brand';
				$this->load->library('upload', $config);
				if ( $this->upload->do_upload('condition_logo')){
					$logoDetails = $this->upload->data();
					$ImageName = $logoDetails['file_name'];
				}else{
					$logoDetails = $this->upload->display_errors();
					$this->setErrorMessage('error',$logoDetails);
				redirect('admin/condition/add_condition_form');
				}
				$condition_logo  =  $ImageName;
			}else{
				$condition_logo = '';
			}
			
			$dataArr 	= array( 'condition_name' => $condition_name,'condition_description'=> $condition_description,'status' => 'Active' );
			if($condition_logo != ''){
				$imgArr	= array('condition_logo' => $condition_logo);
				$dataArr	 	= array_merge($dataArr, $imgArr);
			}
			
			$this->condition_model->update_details(CONDITION,$dataArr,$condition);
			$this->setErrorMessage('success','Condition updated successfully');
			redirect('admin/condition/display_condition_list');
		}
	}
	
	/**
	 * 
	 * This function loads the edit condition
	 */
	public function edit_condition_form(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Edit Condition';
			$condition_id = $this->uri->segment(4,0);
			$condition = array('id' => $condition_id);
			$this->data['condition_details'] = $this->condition_model->get_all_details(CONDITION,$condition);
			if ($this->data['condition_details']->num_rows() == 1){
				$this->load->view('admin/condition/edit_condition',$this->data);
			}else {
				redirect('admin');
			}
		}
	}

	
	/**
	 * 
	 * This function delete the condition record from db
	 */
	public function delete_condition(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$attribute_id = $this->uri->segment(4,0);
			$condition = array('id' => $attribute_id);
			$this->condition_model->commonDelete(CONDITION,$condition);
			$this->setErrorMessage('success','Condition deleted successfully');
			redirect('admin/condition/display_condition_list');
		}
	}
	
	/**
	 * 
	 * This function change the condition status
	 */
	public function change_condition_status(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$mode = $this->uri->segment(4,0);
			$condition_id = $this->uri->segment(5,0);
			$status = ($mode == '0')?'Inactive':'Active';
			$newdata = array('status' => $status);
			$condition = array('id' => $condition_id);
			$this->condition_model->update_details(CONDITION,$newdata,$condition);
			$this->setErrorMessage('success','Condition Status Changed Successfully');
			redirect('admin/condition/display_condition_list');
		}
	}

	

	
}

/* End of file condition.php */
/* Location: ./application/controllers/admin/condition.php */