<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Groupgift extends MY_Controller {
	function __construct(){
        parent::__construct();
		$this->load->helper(array('cookie','date','form'));
		$this->load->library(array('encrypt','form_validation'));		
		$this->load->model('groupgift_model');
		if ($this->checkPrivileges('groupgift',$this->privStatus) == FALSE){
			redirect('admin');
		}
    }
   	public function index(){	
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			redirect('admin/groupgift/display_groupgift');
		}
	}
	public function display_groupgift_dashboard(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Group Gifts Dashboard';
			$condition = 'order by `created` desc';
			$this->data['groupGiftsList'] = $this->groupgift_model->get_giftcard_details($condition);
			$this->load->view('admin/groupgifts/display_groupgift_dashboard',$this->data);
		}
	}
	public function display_groupgift(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Group Gifts List';
			$condition = array();
			$this->data['GroupgiftList'] = $this->groupgift_model->get_all_details(GROUP_GIFTS,$condition);

			if($this->data['GroupgiftList']->num_rows() >0){
		   foreach($this->data['GroupgiftList']->result() as $_groupgiftlist){
               $this->data['contributeDetails'][$_groupgiftlist->id] = $this->groupgift_model->get_all_details(GROUPGIFT_PAYMENT,array('groupgift_id'=>$_groupgiftlist->id));
		   }
		}
			$this->load->view('admin/groupgifts/display_groupgift',$this->data);
		}
	}
	public function contributors_paid(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Paid Orders';
			$Query = "select gf.* from ".GROUP_GIFTS." as gf left join ".GROUPGIFT_PAYMENT." as gp on (gp.groupgift_id = gf.gift_seller_id) where gp.status ='Paid' group by gp.groupgift_id";
			$groupgiftDetails = $this->groupgift_model->ExecuteQuery($Query);
			if($groupgiftDetails->num_rows()>0){
				$this->data['groupgiftDetails'] = $groupgiftDetails;
				foreach($groupgiftDetails->result() as $_groupgiftDetails){
					$query = "select count(id) as contributors, SUM(amount) as totalAMT from ".GROUPGIFT_PAYMENT." where status='Paid' and groupgift_id = '".$_groupgiftDetails->gift_seller_id."'";
					$contributorsCount = $this->groupgift_model->ExecuteQuery($query);
					if($contributorsCount->row()->contributors >0){
						$this->data['contributors_count'][$_groupgiftDetails->id] = $contributorsCount->row()->contributors;
						$this->data['contributors_amt'][$_groupgiftDetails->id] = $contributorsCount->row()->totalAMT;
					}else{
						$this->data['contributors_count'][$_groupgiftDetails->id] = "0";
					}
				}
			}else{
				$this->data['groupgiftDetails'] ='';
			}
			$this->load->view('admin/groupgifts/display_paid_payments',$this->data);
		}
	}
	public function view_contributors(){
	    if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'View Contributors';
			$view_contributors = $this->groupgift_model->get_all_details(GROUPGIFT_PAYMENT,array('groupgift_id'=>$this->uri->segment(4)));
			if($view_contributors->num_rows()>0){
				$this->data['view_contributors'] = $view_contributors;
			}else{
				$this->data['view_contributors'] ='';
			}
			$this->load->view('admin/groupgifts/view_contributors',$this->data);
		}
	}
	public function contributors_failed(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$view_contributors = $this->groupgift_model->get_all_details(GROUPGIFT_PAYMENT,array('status'=>'Refunded'));
			if($view_contributors->num_rows()>0){
				$this->data['view_contributors'] = $view_contributors;
			}else{
				$this->data['view_contributors'] ='';
			}
			$this->load->view('admin/groupgifts/failed_contributors',$this->data);
		}
	}
	public function refund(){
		if($this->checkLogin('A') == ''){
			redirect('admin');
		}else{
			redirect("site/checkout/contributors_refund/".$this->uri->segment(4));
		}
	}
	public function pay_to_seller(){
		if($this->checkLogin('A') == ''){
			redirect('admin');
		}else{
			redirect("site/checkout/admin_Pay_to_Seller/".$this->uri->segment(4));
		}
	}
	public function insertEditGroupgiftcard(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$dataArr = array();
			$condition = array('id' => '1');
			($this->config->item('id') == '') ? $modeVal = 'insert' : $modeVal = 'update';
			$this->groupgift_model->commonInsertUpdate(GROUPGIFT_SETTINGS,$modeVal,$excludeArr,$dataArr,$condition);
			$cond = "select * from ".GROUPGIFT_SETTINGS." where id = '1'";
			$expiry_date = $this->groupgift_model->ExecuteQuery($cond);
            $config = '<?php ';
		       foreach($expiry_date->row() as $key => $val){
			       $value = addslashes($val);
			       $config .= "\n\$config['$key'] = '$value'; ";
		      }
		    $config .= ' ?>';
		    $file = 'commonsettings/fc_groupgift_settings.php';
		    file_put_contents($file, $config);
			$this->setErrorMessage('success','Groupgifts settings updated successfully');
			redirect('admin/groupgift/edit_groupgift_settings');
		}
	}
	public function edit_groupgift_settings(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Groupgifts Settings';
			$this->data['groupgift_settings'] = $this->groupgift_model->get_all_details(GROUPGIFT_SETTINGS,array());
			$this->load->view('admin/groupgifts/edit_groupgift_settings',$this->data);
		}
	}
	public function view_groupgift(){
	    if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'View Groupgifts';
			$this->data['view_groupgift'] = $this->groupgift_model->get_all_details(GROUP_GIFTS,array('id'=>$this->uri->segment(4)));
			$this->load->view('admin/groupgifts/view_groupgift',$this->data);
		}
	}
	public function delete_groupgifts(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$gift_id = $this->uri->segment(4,0);
			$condition = array('id' => $gift_id);
			$this->groupgift_model->commonDelete(GROUP_GIFTS,$condition);
			$this->setErrorMessage('success','Groupgift deleted successfully');
			redirect('admin/groupgift/display_groupgift');
		}
	}
	public function change_status(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$mode = $this->uri->segment(4,0);
			$status = ($mode == '1')?'Active':'Cancelled Admin';
			$condition = array('id' => $this->uri->segment(5,0));
			$data = array('status'=>$status);
			$this->groupgift_model->update_details(GROUP_GIFTS,$data,$condition);
			//$this->groupgift_model->saveGroupgiftSettings();
			$this->setErrorMessage('success','GroupGift '.$status.'d Successfully');
			redirect('admin/groupgift/display_groupgift');
		}
	}
	public function change_groupgift_status_global(){
		if(count($_POST['checkbox_id']) > 0 &&  $_POST['statusMode'] != ''){
			$this->groupgift_model->activeInactiveCommon(GROUP_GIFTS,'id');
			$this->setErrorMessage('success','Groupgist deleted successfully');
			redirect('admin/groupgifts/display_groupgift');
		}
	}
    public function view_product_comments(){
	
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Product Comments List';
			$condition = array();
			$Query = "select gf.*,u.full_name,u.user_name,u.thumbnail,c.comments ,u.email,c.id,c.status,c.user_id as CUID,p.product_name from ".GROUPGIFT_COMMENTS." c LEFT JOIN ".USERS." u on u.id=c.user_id LEFT JOIN ".GROUP_GIFTS." gf on gf.gift_seller_id=c.group_product_id  LEFT JOIN ".PRODUCT." p on p.id = gf.product_id order by c.created desc";
			$this->data['commentsList'] = $this->groupgift_model->ExecuteQuery($Query);
			$this->load->view('admin/groupgifts/display_product_comments_list',$this->data);
		}
	}
	public function change_product_comment_status(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$mode = $this->uri->segment(4,0);
			$id = $this->uri->segment(5,0);
			$user_id = $this->uri->segment(6,0);
			$status = ($mode == '0')?'InActive':'Active';
			$newdata = array('status' => $status);
			$condition = array('id' => $id,'user_id'=>$user_id);
			$this->groupgift_model->update_details(GROUPGIFT_COMMENTS,$newdata,$condition);
			$this->setErrorMessage('success','Comment Status Changed Successfully');
			redirect('admin/groupgift/view_product_comments');
		}
	}
	public function view_product_comment(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'View Comments';
			$id = $this->uri->segment(4,0);
			$condition = "select gf.comments,u.user_name,p.product_name from ".GROUPGIFT_COMMENTS." gf left join ".USERS." u on (u.id = gf.user_id) left join ".GROUP_GIFTS." c on (c.gift_seller_id = gf.group_product_id) left join ".PRODUCT." p on (p.id = c.product_id) where gf.id = ".$id."";
			//$condition = array('id' => $id);
			$this->data['view_details'] = $this->groupgift_model->ExecuteQuery($condition);
			if ($this->data['view_details']->num_rows() == 1){
				$this->load->view('admin/groupgifts/view_comments',$this->data);
			}else {
				redirect('admin');
			}
		}
	}
	public function delete_product_comment(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$id = $this->uri->segment(4,0);
			$condition = array('id' => $id);
			$this->groupgift_model->commonDelete(GROUPGIFT_COMMENTS,$condition);
			$this->setErrorMessage('success','Comment deleted successfully');
			redirect('admin/groupgift/view_product_comments');
		}
	}
	public function seller_payments(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Seller Payments List';
			$seller_payment = $this->groupgift_model->get_all_details(GROUPGIFT_PAYTO_SELLER,array());
			
				$this->data['seller_payment'] = $seller_payment;
			
			$this->load->view('admin/groupgifts/seller_payments',$this->data);
		}
	}
}

/* End of file giftcards.php */
/* Location: ./application/controllers/admin/giftcards.php */