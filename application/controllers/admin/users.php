<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This controller contains the functions related to user management 
 * @author Teamtweaks
 *
 */

class Users extends MY_Controller {

	function __construct(){
        parent::__construct();
		$this->load->helper(array('cookie','date','form'));
		$this->load->library(array('encrypt','form_validation'));		
		$this->load->model('user_model');
		if ($this->checkPrivileges('user',$this->privStatus) == FALSE){
			redirect('admin');
		}
    }
    
    /**
     * 
     * This function loads the users list page
     */
   	public function index(){	
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			redirect('admin/users/display_user_list');
		}
	}
	
	/**
	 * 
	 * This function loads the users list page
	 */
	public function display_user_list(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Users List';
			$condition = array('group'=>'User');
			$this->data['usersList'] = $this->user_model->get_all_details(USERS,$condition);
			$this->load->view('admin/users/display_userlist',$this->data);
		}
	}
	
	/**
	 * 
	 * This function loads the users dashboard
	 */
	public function display_user_dashboard(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Users Dashboard';
			$condition = 'order by `created` desc';
			$this->data['usersList'] = $this->user_model->get_users_details($condition);
			$condition = "where status = 'Active' and DATE_ADD(birthday, INTERVAL YEAR(CURDATE())-YEAR(birthday) YEAR) BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL 0 DAY)";
			$this->data['BrithdayusersList'] = $this->user_model->getTodayBrithdayList($condition);	
			$this->load->view('admin/users/display_user_dashboard',$this->data);
		}
	}
	
	/**
	 * 
	 * This function loads the add new user form
	 */
	public function add_user_form(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Add New User';
			$this->load->view('admin/users/add_user',$this->data);
		}
	}
	
	/**
	 * 
	 * This function insert and edit a user
	 */
	public function insertEditUser(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$user_id = $this->input->post('user_id');
			$user_name = $this->input->post('user_name');
			$password = md5($this->input->post('new_password'));
			$email = $this->input->post('email');
			if ($user_id == ''){
				$unameArr = $this->config->item('unameArr');
				if (!preg_match('/^\w{1,}$/', trim($user_name))){
					$this->setErrorMessage('error','User name not valid. Only alphanumeric allowed');
					echo "<script>window.history.go(-1);</script>";exit;
				}
				if (in_array($user_name, $unameArr)){
					$this->setErrorMessage('error','User name already exists');
					echo "<script>window.history.go(-1);</script>";exit;
				}
				$condition = array('user_name' => $user_name);
				$duplicate_name = $this->user_model->get_all_details(USERS,$condition);
				if ($duplicate_name->num_rows() > 0){
					$this->setErrorMessage('error','User name already exists');
					redirect('admin/users/add_user_form');
				}else {
					$condition = array('email' => $email);
					$duplicate_mail = $this->user_model->get_all_details(USERS,$condition);
					if ($duplicate_mail->num_rows() > 0){
						$this->setErrorMessage('error','User email already exists');
						redirect('admin/users/add_user_form');
					}
				}
			}
			$excludeArr = array("user_id","thumbnail","new_password","confirm_password","group","status");
			if ($this->input->post('group') != ''){
				$user_group = 'User';
			}else {
				$user_group = 'Seller';
			}
			if ($this->input->post('status') != ''){
				$user_status = 'Active';
			}else {
				$user_status = 'Inactive';
			}
			$inputArr = array('group' => $user_group, 'status' => $user_status);
			if ($user_group == 'Seller'){
				$inputArr['request_status'] = 'Approved';
			}
			$datestring = "%Y-%m-%d %H:%i:%s";
			$time = time();
			//$config['encrypt_name'] = TRUE;
			$config['overwrite'] = FALSE;
	    	$config['allowed_types'] = 'jpg|jpeg|gif|png';
		    $config['max_size'] = 2000;
		    $config['upload_path'] = './images/users';
		    $this->load->library('upload', $config);
			if ( $this->upload->do_upload('thumbnail')){
		    	$imgDetails = $this->upload->data();
		    	$inputArr['thumbnail'] = $imgDetails['file_name'];
			}
			if ($user_id == ''){
				$user_data = array(
					'password'	=>	$password,
					'is_verified'	=>	'Yes',
					'created'	=>	mdate($datestring,$time),
					'modified'	=>	mdate($datestring,$time),
				);
			}else {
				$user_data = array('modified' =>	mdate($datestring,$time));
			}
			$dataArr = array_merge($inputArr,$user_data);
			$condition = array('id' => $user_id);
			if ($user_id == ''){
				$this->user_model->commonInsertUpdate(USERS,'insert',$excludeArr,$dataArr,$condition);
				$this->setErrorMessage('success','User added successfully');
			}else {
				$this->user_model->commonInsertUpdate(USERS,'update',$excludeArr,$dataArr,$condition);
				$this->setErrorMessage('success','User updated successfully');
			}
			redirect('admin/users/display_user_list');
		}
	}
	
	/**
	 * 
	 * This function loads the edit user form
	 */
	public function edit_user_form(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Edit User';
			$user_id = $this->uri->segment(4,0);
			$condition = array('id' => $user_id);
			$this->data['user_details'] = $this->user_model->get_all_details(USERS,$condition);
			if ($this->data['user_details']->num_rows() == 1){
				$this->load->view('admin/users/edit_user',$this->data);
			}else {
				redirect('admin');
			}
		}
	}
	
	/**
	 * 
	 * This function change the user status
	 */
	public function change_user_status(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$mode = $this->uri->segment(4,0);
			$user_id = $this->uri->segment(5,0);
			$status = ($mode == '0')?'Inactive':'Active';
			$newdata = array('status' => $status);
			$condition = array('id' => $user_id);
			$this->user_model->update_details(USERS,$newdata,$condition);
			$this->setErrorMessage('success','User Status Changed Successfully');
			redirect('admin/users/display_user_list');
		}
	}
	
	/**
	 * 
	 * This function loads the user view page
	 */
	public function view_user(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'View User';
			$user_id = $this->uri->segment(4,0);
			$condition = array('id' => $user_id);
			$this->data['user_details'] = $this->user_model->get_all_details(USERS,$condition);
			if ($this->data['user_details']->num_rows() == 1){
				$this->load->view('admin/users/view_user',$this->data);
			}else {
				redirect('admin');
			}
		}
	}
	
	/**
	 * 
	 * This function delete the user record from db
	 */
	public function delete_user(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$user_id = $this->uri->segment(4,0);
			
			$user_details = $this->user_model->get_all_details(USERS,array('id'=>$user_id));
			if ($user_details->num_rows()>0){
				
				//Update following count
				$following_users_qry = "select * from ".USERS." where FIND_IN_SET('".$user_id."',followers)";
				$following_users = $this->user_model->ExecuteQuery($following_users_qry);
				if ($following_users->num_rows()>0){
					foreach ($following_users->result() as $fl_row){
						$fl_ids = array_unique(array_filter(explode(',', $fl_row->followers)));
						if (count($fl_ids)>0){
							if (($key=array_search($user_id, $fl_ids)) !== false){
								unset($fl_ids[$key]);
								$this->user_model->update_details(USERS,array('followers'=>implode(',', $fl_ids),'followers_count'=>count($fl_ids)),array('id'=>$fl_row->id));
							}
						}
					}
				}
				//----------------------
				
				//Update followers count
				$follower_users_qry = "select * from ".USERS." where FIND_IN_SET('".$user_id."',following)";
				$follower_users = $this->user_model->ExecuteQuery($follower_users_qry);
				if ($follower_users->num_rows()>0){
					foreach ($follower_users->result() as $fl_row){
						$fl_ids = array_unique(array_filter(explode(',', $fl_row->following)));
						if (count($fl_ids)>0){
							if (($key=array_search($user_id, $fl_ids)) !== false){
								unset($fl_ids[$key]);
								$this->user_model->update_details(USERS,array('following'=>implode(',', $fl_ids),'following_count'=>count($fl_ids)),array('id'=>$fl_row->id));
							}
						}
					}
				}
				//----------------------
				
				$condition = array('id' => $user_id);
				$this->user_model->commonDelete(USERS,$condition);
				$this->setErrorMessage('success','User deleted successfully');
			}
			redirect('admin/users/display_user_list');
		}
	}
	
	/**
	 * 
	 * This function change the user status, delete the user record
	 */
	public function change_user_status_global(){
		if(count($this->input->post('checkbox_id')) > 0 &&  $this->input->post('statusMode') != ''){
			$data =  $_POST['checkbox_id'];
			if (strtolower($this->input->post('statusMode')) == 'delete'){
				for ($i=0;$i<count($data);$i++){
					if($data[$i] == 'on'){
						unset($data[$i]);
					}
				}
				foreach ($data as $user_id){
					if ($user_id!=''){
						$user_details = $this->user_model->get_all_details(USERS,array('id'=>$user_id));
						if ($user_details->num_rows()>0){
						
							//Update following count
							$following_users_qry = "select * from ".USERS." where FIND_IN_SET('".$user_id."',followers)";
							$following_users = $this->user_model->ExecuteQuery($following_users_qry);
							if ($following_users->num_rows()>0){
								foreach ($following_users->result() as $fl_row){
									$fl_ids = array_unique(array_filter(explode(',', $fl_row->followers)));
									if (count($fl_ids)>0){
										if (($key=array_search($user_id, $fl_ids)) !== false){
											unset($fl_ids[$key]);
											$this->user_model->update_details(USERS,array('followers'=>implode(',', $fl_ids),'followers_count'=>count($fl_ids)),array('id'=>$fl_row->id));
										}
									}
								}
							}
							//----------------------
							
							//Update followers count
							$follower_users_qry = "select * from ".USERS." where FIND_IN_SET('".$user_id."',following)";
							$follower_users = $this->user_model->ExecuteQuery($follower_users_qry);
							if ($follower_users->num_rows()>0){
								foreach ($follower_users->result() as $fl_row){
									$fl_ids = array_unique(array_filter(explode(',', $fl_row->following)));
									if (count($fl_ids)>0){
										if (($key=array_search($user_id, $fl_ids)) !== false){
											unset($fl_ids[$key]);
											$this->user_model->update_details(USERS,array('following'=>implode(',', $fl_ids),'following_count'=>count($fl_ids)),array('id'=>$fl_row->id));
										}
									}
								}
							}
							//----------------------
						}
					}
				}
			}
			$this->user_model->activeInactiveCommon(USERS,'id');
			if (strtolower($this->input->post('statusMode')) == 'delete'){
				$this->setErrorMessage('success','User records deleted successfully');
			}else {
				$this->setErrorMessage('success','User records status changed successfully');
			}
			redirect('admin/users/display_user_list');
		}
	}
	
	
		public function send_birthday_mail($user_id=''){
		$user_id = $this->input->post('user_id');
		if($user_id !=''){
		$user_details = $this->user_model->get_all_details(USERS,array('id'=>$user_id));
		if ($user_details->num_rows()>0){
		$newsid='32';
		$email = $user_details->row()->email;
		$uname = $user_details->row()->user_name;
		$template_values=$this->user_model->get_newsletter_template_details($newsid);
		$subject = 'From: '.$this->config->item('email_title').' - '.$template_values['news_subject'];
		$adminnewstemplateArr=array('email_title'=> $this->config->item('email_title'),'logo'=> $this->data['logo']);
		extract($adminnewstemplateArr);
		//$ddd =htmlentities($template_values['news_descrip'],null,'UTF-8');
		$header .="Content-Type: text/plain; charset=ISO-8859-1\r\n";

		$message .= '<!DOCTYPE HTML>
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
			<meta name="viewport" content="width=device-width"/><body>';
		include('./newsletter/registeration'.$newsid.'.php');

		$message .= '</body>
			</html>';

		if($template_values['sender_name']=='' && $template_values['sender_email']==''){
			$sender_email=$this->data['siteContactMail'];
			$sender_name=$this->data['siteTitle'];
		}else{
			$sender_name=$template_values['sender_name'];
			$sender_email=$template_values['sender_email'];
		}

		$email_values = array('mail_type'=>'html',
							'from_mail_id'=>$sender_email,
							'mail_name'=>$sender_name,
							'to_mail_id'=>$email,
							'subject_message'=>$template_values['news_subject'],
							'body_messages'=>$message,
							'mail_id'=>'Birthday mail',
							'uname'=>$uname
							);
					$email_send_to_common = $this->user_model->common_email_send($email_values);
	$json_encode = json_encode(array("status"=>'success'));	
	}else{
	$json_encode = json_encode(array("status"=>'failure'));	
	}
	}else{
	$json_encode = json_encode(array("status"=>'failure'));	
	}
	echo $json_encode;
	}
}

/* End of file users.php */
/* Location: ./application/controllers/admin/users.php */