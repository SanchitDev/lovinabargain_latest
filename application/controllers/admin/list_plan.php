<?php
class Multilanguage extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('file');
		$this->load->helper('language');
		$this->load->helper(array('cookie','date','form'));
		$this->load->library(array('encrypt','form_validation'));
		$this->load->model('admin_model');
		$this->load->model('multilanguage_model');

		if ($this->checkPrivileges('multilang',$this->privStatus) == FALSE){
			redirect('admin');
		}

	}

	function index()
	{
		$this->display_language_list();
	}
	
	function display_language_list()
	{

		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Multilanguage Manage';
			$this->data['admin_settings'] = $result = $this->admin_model->getAdminSettings();
			$this->data['language_list'] = $result = $this->multilanguage_model->get_language_list();
			$this->load->view('admin/multilanguage/language_list',$this->data);
		}
	}
?>