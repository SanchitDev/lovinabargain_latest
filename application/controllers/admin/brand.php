<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This controller contains the functions related to brand mngmnt
 * @author Teamtweaks
 *
 */ 

class Brand extends MY_Controller {
 
	function __construct(){
        parent::__construct();
		$this->load->helper(array('cookie','date','form'));
		$this->load->library(array('encrypt','form_validation'));		
		$this->load->model('brand_model');
		if ($this->checkPrivileges('brand',$this->privStatus) == FALSE){
			redirect('admin');
		}
    }
    
    /**
     * 
     * This function loads the brand list page
     */
   	public function index(){	
	
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			redirect('admin/brand/display_brand_list');
		}
	}
	
	/**
	 * 
	 * This function loads the brand list page
	 */
	public function display_brand_list(){
		//echo "df";die;
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Brand Details';
			$this->data['brandList'] = $this->brand_model->get_all_details(BRAND,array());
			$this->load->view('admin/brand/display_brand_list',$this->data);
		}
	}

	
	/**
	 * 
	 * This function loads the add new brand form
	 */
	public function add_brand_form(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Add Brand';
			$this->data['brand_id'] = $this->uri->segment(4,0);
			$this->load->view('admin/brand/add_brand',$this->data);
		}
	}
	
	
	
	/**
	 * 
	 * This function insert brand
	 */
	public function insertBrand(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
		
			$brand_name 		= $this->input->post('brand_name');
			$brand_description 	= $this->input->post('brand_description');
			$condition 			= array('brand_name' => $brand_name);
			$duplicate_name 	= $this->brand_model->get_all_details(BRAND,$condition);
			if ($duplicate_name->num_rows() > 1){
				$this->setErrorMessage('error','Brand name already exists');
				redirect('admin/brand/add_brand_form');
			}
			$seourl 			= url_title($brand_name,'',TRUE);
			
			if ($this->input->post('status') != ''){
				$brand_status = 'Active';
			}else {
				$brand_status = 'Inactive';
			}
			if( $_FILES['brand_logo']['name'] !=''){
					
				$config['encrypt_name'] = TRUE;
				$config['overwrite'] = FALSE;
				$config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';
				$config['max_size'] = 2000;
				$config['upload_path'] = './images/brand';
				$this->load->library('upload', $config);
				if ( $this->upload->do_upload('brand_logo')){
					$logoDetails = $this->upload->data();
					$ImageName = $logoDetails['file_name'];
				}else{
					$logoDetails = $this->upload->display_errors();
					$this->setErrorMessage('error',$logoDetails);
				redirect('admin/brand/add_brand_form');
				}
				$brand_logo  =  $ImageName;
			}else{
				$brand_logo = '';
			}
			
			$dataArr = array( 'brand_name' => $brand_name,'brand_logo' => $brand_logo,'brand_description'=> $brand_description, 'status' => $brand_status,'brand_seourl'=>$seourl );
			
			$this->brand_model->simple_insert(BRAND,$dataArr);
			$this->setErrorMessage('success','Brand added successfully');
			redirect('admin/brand/display_brand_list');
		}
	}
	
	/**
	 * 
	 * This function Edit brand
	 */
	public function EditBrand(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
		

			$brand_id			= $this->input->post('brand_id');			
			$brand_name			= $this->input->post('brand_name');
			$brand_description 	= $this->input->post('brand_description');
			$condition 			= array('brand_name' => $brand_name,'id !='=>$brand_id);
			$duplicate_name = $this->brand_model->get_all_details(BRAND,$condition);
			if ($duplicate_name->num_rows() > 0){
				$this->setErrorMessage('error','Brand name already exists');
				redirect('admin/brand/edit_brand_form/'.$brand_id);
			}
			$condition = array('id' => $brand_id);
			
			//$seourl = url_title($brand_name,'',TRUE);
			
			if( $_FILES['brand_logo']['name'] !=''){
					
				$config['encrypt_name'] = TRUE;
				$config['overwrite'] = FALSE;
				$config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';
				$config['max_size'] = 2000;
				$config['upload_path'] = './images/brand';
				$this->load->library('upload', $config);
				if ( $this->upload->do_upload('brand_logo')){
					$logoDetails = $this->upload->data();
					$ImageName = $logoDetails['file_name'];
				}else{
					$logoDetails = $this->upload->display_errors();
					$this->setErrorMessage('error',$logoDetails);
				redirect('admin/brand/add_brand_form');
				}
				$brand_logo  =  $ImageName;
			}else{
				$brand_logo = '';
			}
			
			$dataArr 	= array( 'brand_name' => $brand_name,'brand_description'=> $brand_description,'status' => 'Active' );
			if($brand_logo != ''){
				$imgArr	= array('brand_logo' => $brand_logo);
				$dataArr	 	= array_merge($dataArr, $imgArr);
			}
			
			$this->brand_model->update_details(BRAND,$dataArr,$condition);
			$this->setErrorMessage('success','Brand updated successfully');
			redirect('admin/brand/display_brand_list');
		}
	}
	
	/**
	 * 
	 * This function loads the edit brand
	 */
	public function edit_brand_form(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Edit Brand';
			$brand_id = $this->uri->segment(4,0);
			$condition = array('id' => $brand_id);
			$this->data['brand_details'] = $this->brand_model->get_all_details(BRAND,$condition);
			if ($this->data['brand_details']->num_rows() == 1){
				$this->load->view('admin/brand/edit_brand',$this->data);
			}else {
				redirect('admin');
			}
		}
	}

	/**
	 * 
	 * This function change the brand status
	 */
	public function change_brand_status(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$mode = $this->uri->segment(4,0);
			$brand_id = $this->uri->segment(5,0);
			$status = ($mode == '0')?'Inactive':'Active';
			$newdata = array('status' => $status);
			$condition = array('id' => $brand_id);
			$this->brand_model->update_details(BRAND,$newdata,$condition);
			$this->setErrorMessage('success','Brand Status Changed Successfully');
			redirect('admin/brand/display_brand_list');
		}
	}
	
	/**
	 * 
	 * This function loads the brand view page
	 */
	public function view_brand(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'View Attribute';
			$attribute_id = $this->uri->segment(4,0);
			$condition = array('id' => $attribute_id);
			$this->data['brand_details'] = $this->brand_model->get_all_details(BRAND,$condition);
			if ($this->data['brand_details']->num_rows() == 1){
				$this->load->view('admin/brand/view_brand',$this->data);
			}else {
				redirect('admin');
			}
		}
	}
	
	/**
	 * 
	 * This function delete the brand record from db
	 */
	public function delete_brand(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$attribute_id = $this->uri->segment(4,0);
			$condition = array('id' => $attribute_id);
			$this->brand_model->commonDelete(BRAND,$condition);
			$this->setErrorMessage('success','Brand deleted successfully');
			redirect('admin/brand/display_brand_list');
		}
	}

	
	/**
	 * 
	 * This function change the Brand status, delete the Brand record
	 */
	public function change_brand_status_global(){
	
		if($this->input->post('checkboxID')!=''){
		
			if($this->input->post('checkboxID')=='0'){
				redirect('admin/brand/add_brand_form/0');
			}else{
				redirect('admin/brand/add_brand_form/'.$this->input->post('checkboxID'));			
			}
	
		}else{
			if(count($this->input->post('checkbox_id')) > 0 &&  $this->input->post('statusMode') != ''){
				$this->brand_model->activeInactiveCommon(PRODUCT_ATTRIBUTE,'id');
				if (strtolower($this->input->post('statusMode')) == 'delete'){
					$this->setErrorMessage('success','Brand records deleted successfully');
				}else {
					$this->setErrorMessage('success','Brand records status changed successfully');
				}
				redirect('admin/brand/display_brand_list');
			}
		}
	}


	
}

/* End of file brand.php */
/* Location: ./application/controllers/admin/brand.php */