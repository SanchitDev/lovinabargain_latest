<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This controller contains the functions related to seller management
 * @author Teamtweaks
 *
 */

class Seller extends MY_Controller {

	function __construct(){
        parent::__construct();
		$this->load->helper(array('cookie','date','form'));
		$this->load->library(array('encrypt','form_validation'));		
		$this->load->model('seller_model');
		$this->load->model('seller_plan_model');
                $this->load->model('seller_plan_model','seller_plan');
		if ($this->checkPrivileges('seller',$this->privStatus) == FALSE){
			redirect('admin');
		}
    }
    
    /**
     * 
     * This function loads the seller requests page
     */
   	public function index(){	
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			redirect('admin/seller/display_seller_dashboard');
		}
	}
	
	/**
	 * 
	 * This function loads the sellers dashboard
	 */
	public function display_seller_dashboard(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Sellers Dashboard';
			$condition = array('group'=>'Seller');
			$this->data['sellerList'] = $this->seller_model->get_all_details(USERS,$condition);
			$Query = "select p.sell_id,MAX(p.total) as topAmt,sum(p.quantity) as totQty, u.user_name,u.full_name from ".PAYMENT." p 
						LEFT JOIN ".USERS." u on u.id=p.sell_id
						Where p.status='Paid'
						group by p.dealCodeNumber
						order by p.total+0 desc";
			$this->data['topSellDetails'] = $this->seller_model->ExecuteQuery($Query);
			$condition = array('request_status'=>'Pending','group'=>'User');
			$this->data['pendingList'] = $this->seller_model->get_all_details(USERS,$condition);
			$this->load->view('admin/seller/display_seller_dashboard',$this->data);
		}
	}
	
	/**
	 * 
	 * This function loads the seller requests page
	 */
	public function display_seller_requests(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Seller Requests';
			$condition = array('request_status' => 'Pending','group' => 'User');
			$this->data['sellerRequests'] = $this->seller_model->get_all_details(USERS,$condition);
			$this->load->view('admin/seller/display_seller_requests',$this->data);
		}
	}
	
	/**
	 * 
	 * This function loads the sellers list page
	 */
	public function display_seller_list(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Seller List';
			$condition = array('group' => 'Seller');
			$this->data['sellersList'] = $this->seller_model->get_all_details(USERS,$condition);
			$this->data['featuredList'] = $this->seller_plan_model->get_featured_sellers();
			$featured_ids = array();
			if($this->data['featuredList']->num_rows > 0){
				foreach($this->data['featuredList']->result() as $id){
					$featured_ids[] = $id->id;
				}
			}
			$this->data['featuredIds'] = $featured_ids;
			
			$this->load->view('admin/seller/display_sellerlist',$this->data);
		}
	}
	
	/**
	 * 
	 * This function insert and edit a seller
	 */
	public function insertEditSeller(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$seller_id = $this->input->post('seller_id');
			$excludeArr = array("seller_id");
			$dataArr = array();
			$condition = array('id' => $seller_id);
			if ($seller_id == ''){
				$this->setErrorMessage('success','Seller added successfully');
			}else {
				$this->seller_model->commonInsertUpdate(USERS,'update',$excludeArr,$dataArr,$condition);
				$this->setErrorMessage('success','Seller updated successfully');
			}
			redirect('admin/seller/display_seller_list');
		}
	}
	
	/**
	 * 
	 * This function change the seller request status
	 */
	public function change_seller_request(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$mode = $this->uri->segment(4,0);
			$user_id = $this->uri->segment(5,0);
			$status = ($mode == '0')?'Rejected':'Approved';
			$newdata = array('request_status' => $status);
			if ($status == 'Rejected'){
				$newdata['group'] = 'User';
			}else if ($status == 'Approved'){
				$newdata['group'] = 'Seller';
			}
			$condition = array('id' => $user_id);
			$this->seller_model->update_details(USERS,$newdata,$condition);
			$this->setErrorMessage('success','Seller Request '.$status.' Successfully');
			redirect('admin/seller/display_seller_requests');
		}
	}
	
	/**
	 * 
	 * This function change the seller status
	 */
	public function change_seller_status(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$mode = $this->uri->segment(4,0);
			$user_id = $this->uri->segment(5,0);
			$status = ($mode == '0')?'Rejected':'Approved';
			$newdata = array('request_status' => $status);
			if ($status == 'Rejected'){
				$newdata['group'] = 'User';
			}else if ($status == 'Approved'){
				$newdata['group'] = 'Seller';
			}
			$condition = array('id' => $user_id);
			$this->seller_model->update_details(USERS,$newdata,$condition);
			$this->setErrorMessage('success','Seller Status Changed Successfully');
			redirect('admin/seller/display_seller_list');
		}
	}
	
	/**
	 * 
	 * This function loads the edit seller form
	 */
	public function edit_seller_form(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Edit Seller';
			$user_id = $this->uri->segment(4,0);
			$condition = array('id' => $user_id);
			$this->data['seller_details'] = $this->seller_model->get_all_details(USERS,$condition);
			if ($this->data['seller_details']->num_rows() == 1 && $this->data['seller_details']->row()->group == 'Seller'){
				$this->load->view('admin/seller/edit_seller',$this->data);
			}else {
				redirect('admin');
			}
		}
	}
	public function edit_sellerPlan_form(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Edit Seller Plan';
			$user_id = $this->uri->segment(4,0);
			$condition = array('id' => $user_id);
			$this->data['sellerPlan'] = $this->seller_model->get_all_details(SELLER_PLAN,$condition);
			if ($this->data['sellerPlan']->num_rows() == 1){
				$this->load->view('admin/seller/add_plan_form',$this->data);
			}else {
				redirect('admin');
			}
		}
	}
	
	/**
	 * 
	 * This function loads the seller view page
	 */
	public function view_seller(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'View Seller';
			$seller_id = $this->uri->segment(4,0);
			$condition = array('id' => $seller_id);
			$this->data['seller_details'] = $this->seller_model->get_all_details(USERS,$condition);
			if ($this->data['seller_details']->num_rows() == 1){
				$this->load->view('admin/seller/view_seller',$this->data);
			}else {
				redirect('admin');
			}
		}
	}
	
	/**
	 * 
	 * This function delete the seller record from db
	 */
	public function delete_seller(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$seller_id = $this->uri->segment(4,0);
			$condition = array('id' => $seller_id);
			$this->seller_model->commonDelete(USERS,$condition);
			$this->setErrorMessage('success','Seller deleted successfully');
			redirect('admin/seller/display_seller_list');
		}
	}
	public function delete_sellerPlan(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$seller_id = $this->uri->segment(4,0);
			$condition = array('id' => $seller_id);
			$this->seller_model->commonDelete(SELLER_PLAN,$condition);
			$this->setErrorMessage('success','Seller Plan deleted successfully');
			redirect('admin/seller/featured_plan');
		}
	}
	
	/**
	 * 
	 * This function delete the seller request records
	 */
	public function change_seller_status_global(){
		if(count($_POST['checkbox_id']) > 0 &&  $_POST['statusMode'] != ''){
			$this->seller_model->activeInactiveCommon(USERS,'id');
			if (strtolower($_POST['statusMode']) == 'delete'){
				$this->setErrorMessage('success','Seller records deleted successfully');
			}else {
				$this->setErrorMessage('success','Seller records status changed successfully');
			}
			redirect('admin/seller/display_seller_list');
		}
	}
	public function change_feature_status_global(){
		if(count($_POST['checkbox_id']) > 0 &&  $_POST['statusMode'] != ''){
			$this->seller_model->activeInactiveCommon(SELLER_PLAN,'id');
			if (strtolower($_POST['statusMode']) == 'delete'){
				$this->setErrorMessage('success','Seller  plan deleted successfully');
			}else {
				$this->setErrorMessage('success','Seller plan status changed successfully');
			}
			redirect('admin/seller/featured_plan');
		}
	}
	
	/**
	 * 
	 * This function change the user status
	 */
	public function change_user_status(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$mode = $this->uri->segment(4,0);
			$user_id = $this->uri->segment(5,0);
			$status = ($mode == '0')?'Inactive':'Active';
			$newdata = array('status' => $status);
			$condition = array('id' => $user_id);
			$this->seller_model->update_details(USERS,$newdata,$condition);
			$this->setErrorMessage('success','Seller Status Changed Successfully');
			redirect('admin/seller/display_seller_list');
		}
	}
	public function change_sellerP_status(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$mode = $this->uri->segment(4,0);
			$user_id = $this->uri->segment(5,0);
			$status = ($mode == '0')?'Inactive':'Active';
			$newdata = array('status' => $status);
			$condition = array('id' => $user_id);
			$this->seller_model->update_details(SELLER_PLAN,$newdata,$condition);
			$this->setErrorMessage('success','Seller Plan Status Changed Successfully');
			redirect('admin/seller/featured_plan');
		}
	}
	
	public function update_refund(){
		if ($this->checkLogin('A') != ''){
			$uid = $this->input->post('uid');
			$refund_amount = $this->input->post('amt');
			if ($uid != ''){
				$this->seller_model->update_details(USERS,array('refund_amount'=>$refund_amount),array('id'=>$uid));
			}
		}
	}

	public function featured_plan(){
		if ($this->checkLogin('A') != ''){
					$this->data['heading']= "Seller Plan List";
                    $this->data['sellerPlan'] = $this->seller_plan->get_all_details(SELLER_PLAN,array());
                    $this->load->view('admin/seller/display_feature_plans',$this->data);
		}
	}
	public function add_feature_plan(){
		if ($this->checkLogin('A') != ''){
                   $this->data['heading']= "Add new plan";
                    $this->load->view('admin/seller/add_plan_form',$this->data);
		}
	}
	public function updateEditplan(){
		//$data = $this->input->post();
				if ($this->input->post('status') != ''){
					$data['status'] = 'Active';
				}else {
					$data['status'] = 'Inactive';
				}
                $datestring = "%Y-%m-%d %h:%i:%s";
                $time = time();                
                $data['created_on'] = mdate($datestring,$time);
				if($this->input->post('plan_id') == ""){
					$this->seller_plan->commonInsertUpdate(SELLER_PLAN,'insert',array('plan_id'),$data);
				}else{
					$this->seller_plan->commonInsertUpdate(SELLER_PLAN,'update',array('plan_id','created_on'),$data,array('id'=>$this->input->post('plan_id')));
					
				}
                $this->setErrorMessage('success','Seller Plan saved successfully');
                redirect(base_url().'admin/seller/featured_plan');
	}
        public function feature_paid_success(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Order List';
			$this->data['orderList'] = $this->seller_plan->get_success_seller_with_users();
			$this->load->view('admin/seller/feature_paid_success',$this->data);
		}            
        }
        public function feature_paid_fail(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Order List';
			$this->data['orderList'] = $this->seller_plan->get_fail_seller_with_users();
			$this->load->view('admin/seller/feature_paid_fail',$this->data);
		} 
        }
	public function cancel_subscription($sid=''){
		if ($this->checkLogin('A') != '' && $sid != ''){
			$this->seller_model->update_details(USERS,array('store_payment'=>'Cancelled'),array('id'=>$sid));
			$this->seller_model->simple_insert(STORE_ARB,array('user_id'=>$sid,'status'=>'cancel'));
                        $this->seller_model->commonDelete(STORE_FRONT,array('user_id'=>$sid));
			$this->setErrorMessage('success','Subscription cancelled successfully');
			redirect('admin/seller/display_seller_list');
		}else {
			redirect('admin');
		}
	} 

	public function make_subscription($sid=''){
		if ($this->checkLogin('A') != '' && $sid != ''){
                        $userDetails = $this->seller_model->get_all_details(USERS,array('id'=>$sid));
                        $datestring = "%Y-%m-%d %h:%i:%s";
                        $time = time();
                        $createdTime = mdate($datestring,$time);  
                        $expiresOn = date("Y-m-d h:i:s", strtotime($createdTime.'+ '.$this->input->post('days').' days'));
                        if($userDetails->num_rows == 1){
			$this->seller_model->update_details(USERS,array('store_payment'=>'Paid'),array('id'=>$sid));
			$this->seller_model->simple_insert(STORE_ARB,array('user_id'=>$sid,'status'=>'Paid','created_at'=>$createdTime,'amount'=>'0.00','transaction_id'=>'Free','invoice_no'=>'Free','payment_type'=>'Free'));
                        $this->seller_model->simple_insert(STORE_FRONT,array('user_id'=>$sid,'dateAdded'=>$createdTime,'expiresOn'=>$expiresOn,'store_name'=>$userDetails->row()->brand_name));
                        $newsid = '33';
                        $template_values = $this->seller_model->get_newsletter_template_details($newsid);
			$adminnewstemplateArr=array('logo'=> $this->data['logo'],'meta_title'=>$this->config->item('meta_title'));
			extract($adminnewstemplateArr);                        
                        $subject = $template_values['news_subject'];
                        $days = $this->input->post('days');
			$message .= '<!DOCTYPE HTML>
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
			<meta name="viewport" content="width=device-width"/>
			<title>'.$template_values['news_subject'].'</title><body>';
			include('./newsletter/registeration'.$newsid.'.php');

			$message .= '</body>
			</html>'; 
			if($template_values['sender_name']=='' && $template_values['sender_email']==''){
				$sender_email=$this->data['siteContactMail'];
				$sender_name=$this->data['siteTitle'];
			}else{
				$sender_name=$template_values['sender_name'];
				$sender_email=$template_values['sender_email'];
			}                        
			$email_values = array('mail_type'=>'html',
                                    'from_mail_id'=>$sender_email,
                                    'mail_name'=>$sender_name,
                                    'to_mail_id'=>$userDetails->row()->email,
                                    'subject_message'=>$subject,
                                    'body_messages'=>$message
			);
			$email_send_to_common = $this->seller_model->common_email_send($email_values);                        
                        
			$this->setErrorMessage('success','Seller upgraded successfully');
                        }
			//redirect('admin/seller/display_seller_list');
		}else {
			//redirect('admin');
		}
	}         
	public function edit_store($sid=''){
		if ($this->checkLogin('A') != '' && $sid != ''){
			$this->data['store_details'] = $this->seller_model->get_all_details(STORE_FRONT,array('user_id'=>$sid));
			$this->data['heading'] = 'Edit Store Details';
			$this->data['user_id'] = $sid;
			$this->load->view('admin/seller/edit_store_form',$this->data);
		}else {
			redirect('admin');
		}
	}
	
	public function update_store_details(){
		if ($this->checkLogin('A') != ''){
			$excludeArr = array('logo_image','cover_image','id','user_id');
			$inputArr = array();
			
			//upload logo
			$config['overwrite'] = FALSE;
			$config['allowed_types'] = 'jpg|jpeg|gif|png';
			$config['upload_path'] = './images/store';
			$this->load->library('upload', $config);
			if ( $this->upload->do_upload('logo_image')){
				$logoDetails = $this->upload->data();
				$inputArr['logo_image'] = $logoDetails['file_name'];
			}
			
			//upload banner
			if ( $this->upload->do_upload('cover_image')){
				$coverDetails = $this->upload->data();
				$inputArr['cover_image'] = $coverDetails['file_name'];
			}
			
			if ($this->input->post('id')!=''){
				$condition = array('user_id'=>$this->input->post('user_id'),'id'=>$this->input->post('id'));
				$this->seller_model->commonInsertUpdate(STORE_FRONT,'update',$excludeArr,$inputArr,$condition);
			}else {
				$inputArr['user_id'] = $this->input->post('user_id');				
				$this->seller_model->commonInsertUpdate(STORE_FRONT,'insert',$excludeArr,$inputArr,array());
			}
			$this->setErrorMessage('success','Store details updated successfully');
			redirect('admin/seller/display_seller_list');
		}
	}        

}

/* End of file seller.php */
/* Location: ./application/controllers/admin/seller.php */