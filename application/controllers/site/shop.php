<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * Shop related functions
 * @author Teamtweaks
 *
 */
class Shop extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('cookie', 'date', 'form', 'email'));
        $this->load->library(array('encrypt', 'form_validation'));
        $this->load->model('product_model', 'shop');
        $this->load->model('user_model', 'user');
        if ($_SESSION['sMainCategories'] == '') {
            $sortArr1 = array('field' => 'cat_position', 'type' => 'asc');
            $sortArr = array($sortArr1);
            $_SESSION['sMainCategories'] = $this->shop->get_all_details(CATEGORY, array('rootID' => '0', 'status' => 'Active'), $sortArr);
        }
        $this->data['mainCategories'] = $_SESSION['sMainCategories'];

        if ($_SESSION['sColorLists'] == '') {
            $_SESSION['sColorLists'] = $this->shop->get_all_details(LIST_VALUES, array('list_id' => '1'));
        }
        $this->data['mainColorLists'] = $_SESSION['sColorLists'];

        $this->data['loginCheck'] = $this->checkLogin('U');
        $this->data['likedProducts'] = array();
        if ($this->data['loginCheck'] != '') {
            $this->data['likedProducts'] = $this->shop->get_all_details(PRODUCT_LIKES, array('user_id' => $this->checkLogin('U')));
        }
    }

//    public function index() {
//        $this->data['heading'] = 'Shop';
//        $this->data['bannerList'] = $this->shop->get_all_details(BANNER_CATEGORY, array('status' => 'Publish'));
//        $this->data['recentProducts'] = $this->shop->view_product_details(" where p.status='Publish' and p.quantity > 0 and u.group='Seller' and u.status='Active' or p.status='Publish' and p.quantity > 0 and p.user_id=0 order by p.created desc limit 4");
//        if ($this->config->item('storefront_fees_month') == '0' && $this->config->item('storefront_fees_year') == '0') {
//            $this->data['sellers_list'] = $this->user->get_all_details(USERS, array('group' => 'Seller', 'status' => 'Active'));
//        } else {
//            $condition = 'select u.*,s.store_name,s.logo_image, Count(p.id) as ProductCount from ' . USERS . ' as u left join ' . STORE_FRONT . ' as s on s.user_id = u.id left join ' . PRODUCT . ' as p on u.id = p.user_id where u.group ="Seller" and u.store_payment ="Paid" and p.status="Publish" and u.status = "Active" and NOT FIND_IN_SET(u.id ,"' . implode(",", $featuredId) . '") group by u.id';
//            $this->data['sellers_list'] = $sellers_list = $this->user->ExecuteQuery($condition);
//        }
//        $this->load->view('site/shop/display_shop_list', $this->data);
//    }

    public function index() {
        if ($this->input->get('pg') == '') {
            $this->load->model('seller_plan_model', 'seller_plan');
            $this->data['featured_products'] = $this->seller_plan->get_featured_products();
        }
        $cat = $this->input->get('c');
        $whereCond = $qry_str = '';
        if ($cat != '') {
            $catDetails = $this->product_model->get_all_details(CATEGORY, array('seourl' => $cat));
            if ($catDetails->num_rows() == 1) {
                $catID = $catDetails->row()->id;
                if ($catID != '') {
                    $whereCond = ' and FIND_IN_SET("' . $catID . '",p.category_id)';
                }
            }
        }
        if ($this->input->get('pg') != '') {
            $paginationVal = $this->input->get('pg') * 6;
            $limitPaging = $paginationVal . ',6 ';
        } else {
            $limitPaging = ' 6';
        }
        $newPage = $this->input->get('pg') + 1;
        if ($cat != '') {
            $qry_str = '?c=' . $cat . '&pg=' . $newPage;
        } else {
            $qry_str = '?pg=' . $newPage;
        }


        $this->data['layoutList'] = $layoutList = $this->product_model->view_controller_details();
        //    echo "<pre>";print_r($this->data['layoutList']->result());die;
        $totalSellingProducts = $this->product_model->get_total_records(PRODUCT);
        $totalAffilProducts = $this->product_model->get_total_records(USER_PRODUCTS);
        $this->data['totalProducts'] = $totalAffilProducts->row()->total + $totalSellingProducts->row()->total;

        if ($layoutList->row()->product_control == 'affiliates') {
            $sellingProductDetails = array();
        } else {
            $sellingProductDetails = $this->product_model->view_product_details(" where p.status='Publish' and ((p.quantity > 0 and p.product_type='physical') or (p.product_type='digital')) and u.group='Seller' and u.status='Active' " . $whereCond . " or p.status='Publish' and ((p.quantity > 0 and p.product_type='physical') or (p.product_type='digital')) and p.user_id=0 " . $whereCond . " order by p.created desc limit " . $limitPaging);
        }
        if ($layoutList->row()->product_control == 'selling') {
            $affiliateProductDetails = array();
        } else {
            $affiliateProductDetails = $this->product_model->view_notsell_product_details(" where p.status='Publish' and u.status='Active' " . $whereCond . " or p.status='Publish' and p.user_id=0 " . $whereCond . " order by p.created desc limit " . $limitPaging);
        }

        $this->data['productDetails'] = $this->product_model->get_sorted_array($sellingProductDetails, $affiliateProductDetails, 'created', 'desc');
        foreach ($this->data['productDetails'] as &$value) {
            $value->users_list = $this->product_model->get_liking_users($value->seller_product_id);
        }
//        echo "<pre>";
//        print_r($this->data['productDetails']);
//        exit;
        $paginationDisplay = '<a title="' . $newPage . '" class="btn-more" href="' . base_url() . $qry_str . '" style="display: none;">See More Products</a>';

        $this->data['paginationDisplay'] = $paginationDisplay;
        //echo "<pre>";
        //print_r($this->data['productDetails']);
        //exit;
        //$this->load->view('site/landing/landing', $this->data);
        $this->load->view('site/shop/display_shop_list', $this->data);
    }

}

/*End of file cms.php */
/* Location: ./application/controllers/site/product.php */