<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * User related functions
 * @author Teamtweaks
 *
 */
class Cart extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('cookie', 'date', 'form', 'email'));
        $this->load->library(array('encrypt', 'form_validation'));
        $this->load->model('cart_model');
        $this->load->model('checkout_model');
        if ($_SESSION['sMainCategories'] == '') {
            $sortArr1 = array('field' => 'cat_position', 'type' => 'asc');
            $sortArr = array($sortArr1);
            $_SESSION['sMainCategories'] = $this->cart_model->get_all_details(CATEGORY, array('rootID' => '0', 'status' => 'Active'), $sortArr);
        }
        $this->data['mainCategories'] = $_SESSION['sMainCategories'];

        if ($_SESSION['sColorLists'] == '') {
            $_SESSION['sColorLists'] = $this->cart_model->get_all_details(LIST_VALUES, array('list_id' => '1'));
        }
        $this->data['mainColorLists'] = $_SESSION['sColorLists'];

        $this->data['loginCheck'] = $this->checkLogin('U');
        //$this->data['MiniCartViewSet'] = $this->cart_model->mini_cart_view($this->data['common_user_id']);
    }

    /**
     *
     * Loading Cart Page
     */
    public function index() {
        if ($this->data['loginCheck'] != '') {
            $this->data['heading'] = 'Cart';
            //---------------------Delete unwanted records from Cart------------------------//
            $this->cart_model->delete_unwanted_records();
            //-----------------------------------------------------------------------------//
            $this->data['cartViewResults'] = $this->cart_model->mani_cart_view($this->data['common_user_id']);
            $this->data['countryList'] = $this->cart_model->get_all_details(COUNTRY_LIST, array(), array(array('field' => 'name', 'type' => 'asc')));

            $this->load->view('site/cart/cart.php', $this->data);
        } else {
            redirect('login');
        }
    } 


    /**
     *
     * Loading Auction Cart Page
     */
    public function auctioncart() {
        if ($this->data['loginCheck'] != '') {
            $this->data['heading'] = 'Auction Cart';
            //---------------------Delete unwanted records from Cart------------------------//
            $this->cart_model->delete_unwanted_records();
            //-----------------------------------------------------------------------------//
            $this->data['cartViewResults'] = $this->cart_model->auction_mani_cart_view($this->data['common_user_id']);
            $this->data['countryList'] = $this->cart_model->get_all_details(COUNTRY_LIST, array(), array(array('field' => 'name', 'type' => 'asc')));
		
            $this->load->view('site/cart/auctioncart.php', $this->data);
        } else {
            redirect('login');
        }
    }


    public function auction(){
        if($this->checkLogin('U') != ''){
            $cart_id              = $this->input->post('id1');
            $user_id              = $this->input->post('id2');
            if($cart_id != '' && $user_id != ''){
                if( $user_id == $this->checkLogin('U') ){
                    $cart_details = $this->cart_model->get_all_details(SHOPPING_CART,array('id'=>$cart_id, 'type'=>'auction','user_id'=>$this->checkLogin('U')));
                    if($cart_details->num_rows() == 1){
                        $shipto = $this->input->post('shipto');
						if($shipto != '' && $cart_details->row()->product_type == 'physical'){
							$shipping_detail =  $this->cart_model->get_all_details(SHIPPING_ADDRESS,array('id'=>$shipto));
							$shipping_cost 	 =  $this->cart_model->get_all_details(SUB_SHIPPING,array('product_id'=>$cart_details->row()->product_id,'ship_code'=>$shipping_detail->row()->country));
							$ship_cost 		 =  $shipping_cost->row()->ship_cost;
							$Shipping_tax 	 =  $this->cart_model->get_all_details(COUNTRY_LIST,array( 'status'=>'Active','country_code' => $shipping_detail->row()->country ));
							$Ship_tax 		 =  $Shipping_tax->row()->shipping_tax;
												
								$this->cart_model->update_details(SHOPPING_CART,array('product_shipping_cost'=>$ship_cost,'tax'=>$Ship_tax,'ship_to'=>$shipping_detail->row()->country ),array('id'=>$cart_details->row()->id));
						}
                        $auction_price            = $cart_details->row()->price;
                        $auction_product_id       = $cart_details->row()->product_id ;
                        $auction_cateory_id       = $cart_details->row()->cate_id;
                        $auction_attribute_values = $cart_details->row()->attribute_values;
                        
                            if($auction_price != '' && $auction_product_id != ''){
                                $auction_data = array(  'auction_cart_id'           => $cart_id,
                                                        'auction_price'             => $auction_price,
                                                        'auction_product_id'        => $auction_product_id,
                                                        'auction_cateory_id'        => $auction_cateory_id,
                                                        'auction_attribute_values'  => $auction_attribute_values
                                            );
                                $this->session->set_userdata($auction_data);
                                echo 'success';die;
                            }
                        
                    }else{
                        echo 'auctioncart';die;
                    }
                }
            }

        }else{
            echo 'login';
        }
    }

    public function auctionauthorize(){
        if($this->checkLogin('U') != ''){
            $this->data['cart_id']                  = $this->session->userdata('auction_cart_id');
            $this->data['price']                    = $this->session->userdata('auction_price');
            $this->data['product_id']               = $this->session->userdata('auction_product_id');
            $this->data['auction_cateory_id']       = $this->session->userdata('auction_cateory_id');
            $this->data['auction_attribute_values'] = $this->session->userdata('auction_attribute_values');

            if($this->data['cart_id'] != '' && $this->data['price'] != '' && $this->data['product_id'] != '' && $this->data['auction_cateory_id'] != ''){
                $this->data['cart_detail'] = $cart_details = $this->cart_model->get_all_details(SHOPPING_CART,array('id'=>$this->data['cart_id'],'product_id'=>$this->data['product_id'], 'type'=>'auction','user_id'=>$this->checkLogin('U')));

                    if($cart_details->num_rows() == 1){
                        $userid = $this->checkLogin('U');
                        $this->data['shipping_address'] = $this->cart_model->get_all_details(SHIPPING_ADDRESS,array('user_id'=>$userid));
                        $this->data['paymentGateway']   = $this->cart_model->get_all_details(PAYMENT_GATEWAY,array('status'=>'Enable'));
                        $this->data['product_detail']   = $this->cart_model->get_all_details(PRODUCT,array('id'=>$this->data['product_id'],'status'=>'Publish'));
                       
						$this->data['checkoutViewResults'] = $this->checkout_model->auction_mani_checkout_total($this->data['common_user_id'],$cart_details->row()->id);
						$this->load->view('site/checkout/auction_checkout.php',$this->data);die;
                    }
                    else{
                        $this->setErrorMessage('error','Oops..Something went wrong.Please try again');
                        redirect('auctioncart');
                    }
            }else{
                $this->setErrorMessage('error','Oops..Something went wrong.Please try again');
                redirect('auctioncart');
            }
        }else{
            redirect('login');
        }
    }
    /*     * **************** Insert the cart to user******************* */

    public function insertEditCart() {

        $excludeArr = array('addtocart');
        $dataArrVal = array();
        foreach ($this->input->post() as $key => $val) {
            if (!(in_array($key, $excludeArr))) {
                $dataArrVal[$key] = trim(addslashes($val));
            }
        }

        $datestring = "%Y-%m-%d 23:59:59";
        $code = $this->get_rand_str('10');
        $exp_days = $this->config->item('cart_expiry_days');

        $dataArry_data = array('expiry_date' => mdate($datestring, strtotime($exp_days . ' days')), 'code' => $code, 'user_id' => $this->data['common_user_id']);
        $dataArr = array_merge($dataArrVal, $dataArry_data);

        $condition = '';

        $this->cart_model->commonInsertUpdate(GIFTCARDS_TEMP, 'insert', $excludeArr, $dataArr, $condition);

        if ($this->checkLogin('U') != '') {
            if ($this->lang->line('gift_add_success') != '')
                $lg_err_msg = $this->lang->line('gift_add_success');
            else
                $lg_err_msg = 'Giftcard Added You Cart successfully';
            $this->setErrorMessage('success', $lg_err_msg);
            redirect('gift-cards');
        }else {
            redirect('login');
        }
    }

    public function auctioncartadd() {
        $excludeArr = array('addtocart', 'attr_color', 'mqty');
        $dataArrVal = array();
        $mqty = $this->input->post('mqty');
        foreach ($this->input->post() as $key => $val) {
            if (!(in_array($key, $excludeArr))) {
                $dataArrVal[$key] = trim(addslashes($val));
            }
        }
        $datestring = date('Y-m-d H:i:s', now());
        $indTotal = ( $this->input->post('price') + $this->input->post('product_shipping_cost') + ($this->input->post('price') * 0.01 * $this->input->post('product_tax_cost')) ) * $this->input->post('quantity');

        $dataArry_data = array('type'=>'auction','created' => $datestring, 'user_id' => $this->data['common_user_id'], 'indtotal' => $indTotal, 'total' => $indTotal);
        $dataArr = array_merge($dataArrVal, $dataArry_data);

        $condition = '';

        $this->data['productVal'] = $this->cart_model->get_all_details(SHOPPING_CART, array('type'=>'auction','user_id' => $this->data['common_user_id'], 'product_id' => $this->input->post('product_id'), 'attribute_values' => $this->input->post('attribute_values')));
        $for_qty_check = $this->cart_model->get_all_details(SHOPPING_CART, array('type'=>'auction','user_id' => $this->data['common_user_id'], 'product_id' => $this->input->post('product_id')));

        if ($for_qty_check->num_rows > 0) { 
		
			if($this->lang->line('cart_added_already') != '')
				$lg_err_msg = $this->lang->line('cart_added_already');
			else 
				$lg_err_msg = 'Product already added  in your cart.';
			$this->setErrorMessage('error',$lg_err_msg);
			/* 
            $new_tot_qty = 0;
            foreach ($for_qty_check->result() as $for_qty_check_row) {
                $new_tot_qty += $for_qty_check_row->quantity;
            }
            $new_tot_qty += $this->input->post('quantity');
            if ($new_tot_qty <= $mqty) { 
                if ($this->data['productVal']->num_rows > 0) {
                    $newQty = $this->data['productVal']->row()->quantity + $this->input->post('quantity');
                    $indTotal = ( $this->input->post('price') + $this->input->post('product_shipping_cost') + ($this->input->post('price') * 0.01 * $this->input->post('product_tax_cost')) ) * $newQty;
                    $dataArr = array('quantity' => $newQty, 'indtotal' => $indTotal, 'total' => $indTotal);
                    $condition = array('id' => $this->data['productVal']->row()->id);
                    $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
                } else {
                    $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'insert', $excludeArr, $dataArr, $condition);
                }
            } else {
                $cart_qty = $new_tot_qty - $this->input->post('quantity');
                echo 'Error|' . $cart_qty;
                die;
            } */
        } else {
            $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'insert', $excludeArr, $dataArr, $condition);
        }


        if ($this->checkLogin('U') != '') {
            /*             * *Mini cart Lg*** */

            $mini_cart_lg = array();

            if ($this->lang->line('items') != '')
                $mini_cart_lg['lg_items'] = stripslashes($this->lang->line('items'));
            else
                $mini_cart_lg['lg_items'] = "items";

            if ($this->lang->line('header_description') != '')
                $mini_cart_lg['lg_description'] = stripslashes($this->lang->line('header_description'));
            else
                $mini_cart_lg['lg_description'] = "Description";

            if ($this->lang->line('qty') != '')
                $mini_cart_lg['lg_qty'] = stripslashes($this->lang->line('qty'));
            else
                $mini_cart_lg['lg_qty'] = "Qty";

            if ($this->lang->line('giftcard_price') != '')
                $mini_cart_lg['lg_price'] = stripslashes($this->lang->line('giftcard_price'));
            else
                $mini_cart_lg['lg_price'] = "Price";

            if ($this->lang->line('order_sub_total') != '')
                $mini_cart_lg['lg_sub_tot'] = stripslashes($this->lang->line('order_sub_total'));
            else
                $mini_cart_lg['lg_sub_tot'] = "Order Sub Total";

            if ($this->lang->line('proceed_to_auth') != '')
                $mini_cart_lg['lg_proceed_auth'] = stripslashes($this->lang->line('proceed_to_auth'));
            else
                $mini_cart_lg['lg_proceed_auth'] = "Proceed to Authorization";

            /*             * *Mini cart Lg*** */

            echo 'Success|' . $this->cart_model->auctionmini_cart_view($this->data['common_user_id'], $mini_cart_lg);
        }else {
            echo 'login|lgoin';
        }
    }

    public function cartadd() {
        $excludeArr = array('addtocart', 'attr_color', 'mqty');
        $dataArrVal = array();
        $mqty = $this->input->post('mqty');
        foreach ($this->input->post() as $key => $val) {
            if (!(in_array($key, $excludeArr))) {
                $dataArrVal[$key] = trim(addslashes($val));
            }
        }
        $datestring = date('Y-m-d H:i:s', now());
        $indTotal = ( $this->input->post('price') + $this->input->post('product_shipping_cost') + ($this->input->post('price') * 0.01 * $this->input->post('product_tax_cost')) ) * $this->input->post('quantity');

        $dataArry_data = array('type'=>'cart','created' => $datestring, 'user_id' => $this->data['common_user_id'], 'indtotal' => $indTotal, 'total' => $indTotal);
        $dataArr = array_merge($dataArrVal, $dataArry_data);

        $condition = '';

        $this->data['productVal'] = $this->cart_model->get_all_details(SHOPPING_CART, array('type'=>'cart','user_id' => $this->data['common_user_id'], 'product_id' => $this->input->post('product_id'), 'attribute_values' => $this->input->post('attribute_values')));
        $for_qty_check = $this->cart_model->get_all_details(SHOPPING_CART, array('type'=>'cart','user_id' => $this->data['common_user_id'], 'product_id' => $this->input->post('product_id')));

        if ($for_qty_check->num_rows > 0) {
            $new_tot_qty = 0;
            foreach ($for_qty_check->result() as $for_qty_check_row) {
                $new_tot_qty += $for_qty_check_row->quantity;
            }
            $new_tot_qty += $this->input->post('quantity');
            if ($new_tot_qty <= $mqty) {
                if ($this->data['productVal']->num_rows > 0) {
                    $newQty = $this->data['productVal']->row()->quantity + $this->input->post('quantity');
                    $indTotal = ( $this->input->post('price') + $this->input->post('product_shipping_cost') + ($this->input->post('price') * 0.01 * $this->input->post('product_tax_cost')) ) * $newQty;
                    $dataArr = array('quantity' => $newQty, 'indtotal' => $indTotal, 'total' => $indTotal);
                    $condition = array('id' => $this->data['productVal']->row()->id);
                    $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
                } else {
                    $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'insert', $excludeArr, $dataArr, $condition);
                }
            } else {
                $cart_qty = $new_tot_qty - $this->input->post('quantity');
                echo 'Error|' . $cart_qty;
                die;
            }
        } else {
            $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'insert', $excludeArr, $dataArr, $condition);
        }


        if ($this->checkLogin('U') != '') {
            /*             * *Mini cart Lg*** */

            $mini_cart_lg = array();

            if ($this->lang->line('items') != '')
                $mini_cart_lg['lg_items'] = stripslashes($this->lang->line('items'));
            else
                $mini_cart_lg['lg_items'] = "items";

            if ($this->lang->line('header_description') != '')
                $mini_cart_lg['lg_description'] = stripslashes($this->lang->line('header_description'));
            else
                $mini_cart_lg['lg_description'] = "Description";

            if ($this->lang->line('qty') != '')
                $mini_cart_lg['lg_qty'] = stripslashes($this->lang->line('qty'));
            else
                $mini_cart_lg['lg_qty'] = "Qty";

            if ($this->lang->line('giftcard_price') != '')
                $mini_cart_lg['lg_price'] = stripslashes($this->lang->line('giftcard_price'));
            else
                $mini_cart_lg['lg_price'] = "Price";

            if ($this->lang->line('order_sub_total') != '')
                $mini_cart_lg['lg_sub_tot'] = stripslashes($this->lang->line('order_sub_total'));
            else
                $mini_cart_lg['lg_sub_tot'] = "Order Sub Total";

            if ($this->lang->line('proceed_to_checkout') != '')
                $mini_cart_lg['lg_proceed'] = stripslashes($this->lang->line('proceed_to_checkout'));
            else
                $mini_cart_lg['lg_proceed'] = "Proceed to Checkout";

            /*             * *Mini cart Lg*** */

            echo 'Success|' . $this->cart_model->mini_cart_view($this->data['common_user_id'], $mini_cart_lg);
        }else {
            echo 'login|lgoin';
        }
    }

    public function cartremove() {
        if($this->input->post('type') != ''){
			$this->cart_model->commonDelete(SHOPPING_CART, array('type'=>'auction','user_id' => $this->input->post('user_id'), 'product_id' => $this->input->post('product_id')));
		}else{
			$this->cart_model->commonDelete(SHOPPING_CART, array('user_id' => $this->input->post('user_id'), 'product_id' => $this->input->post('product_id')));
		}
		
        if ($this->checkLogin('U') != '') {
            /*             * *Mini cart Lg*** */

            $mini_cart_lg = array();

            if ($this->lang->line('items') != '')
                $mini_cart_lg['lg_items'] = stripslashes($this->lang->line('items'));
            else
                $mini_cart_lg['lg_items'] = "items";

            if ($this->lang->line('header_description') != '')
                $mini_cart_lg['lg_description'] = stripslashes($this->lang->line('header_description'));
            else
                $mini_cart_lg['lg_description'] = "Description";

            if ($this->lang->line('qty') != '')
                $mini_cart_lg['lg_qty'] = stripslashes($this->lang->line('qty'));
            else
                $mini_cart_lg['lg_qty'] = "Qty";

            if ($this->lang->line('giftcard_price') != '')
                $mini_cart_lg['lg_price'] = stripslashes($this->lang->line('giftcard_price'));
            else
                $mini_cart_lg['lg_price'] = "Price";

            if ($this->lang->line('order_sub_total') != '')
                $mini_cart_lg['lg_sub_tot'] = stripslashes($this->lang->line('order_sub_total'));
            else
                $mini_cart_lg['lg_sub_tot'] = "Order Sub Total";

            if ($this->lang->line('proceed_to_checkout') != '')
                $mini_cart_lg['lg_proceed'] = stripslashes($this->lang->line('proceed_to_checkout'));
            else
                $mini_cart_lg['lg_proceed'] = "Proceed to Checkout";

            /*             * *Mini cart Lg*** */

            echo 'Success|' . $this->cart_model->auction_mini_cart_view($this->data['common_user_id'], $mini_cart_lg);
        }else {
            echo 'login|lgoin';
        }
    }

    public function ajaxUpdate() {

        $excludeArr = array('id', 'qty', 'updval');
        $productVal = $this->cart_model->get_all_details(SHOPPING_CART, array('user_id' => $this->data['common_user_id'], 'id' => $this->input->post('updval')));
        $newQty = $this->input->post('qty');
        $indTotal = ($productVal->row()->price + $productVal->row()->product_shipping_cost + ($productVal->row()->price * 0.01 * $productVal->row()->product_tax_cost)) * $newQty;
        $dataArr = array('quantity' => $newQty, 'indtotal' => $indTotal, 'total' => $indTotal);
        $condition = array('id' => $productVal->row()->id);
        $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
        echo number_format($indTotal, 2, '.', '') . '|' . $this->data['CartVal'] = $this->cart_model->mani_cart_total($this->data['common_user_id']);
        return;
    }

    public function ajaxDelete() {

        $delt_id = $this->input->post('curval');
        $CondID = $this->input->post('cart');
        if ($CondID == 'gift') {
            $this->db->delete(GIFTCARDS_TEMP, array('id' => $delt_id));
            echo $this->data['GiftVal'] = $this->cart_model->mani_gift_total($this->data['common_user_id']);
        } elseif ($CondID == 'subscribe') {
            $this->db->delete(FANCYYBOX_TEMP, array('id' => $delt_id));
            echo $this->data['SubscribeVal'] = $this->cart_model->mani_subcribe_total($this->data['common_user_id']);
        } elseif ($CondID == 'cart') {
            $this->db->delete(SHOPPING_CART, array('id' => $delt_id));
			echo $this->data['CartVal'] = $this->cart_model->mani_cart_total($this->data['common_user_id']);
        }
        return;
    }

    public function checkCode() {

        $Code = $this->input->post('code');
        $amount = $this->input->post('amount');
        $shipamount = $this->input->post('shipamount');

        echo $this->cart_model->Check_Code_Val($Code, $amount, $shipamount, $this->data['common_user_id']);

        return;
    }

    public function checkCodeRemove() {

        $this->cart_model->Check_Code_Val_Remove($this->data['common_user_id']);
        echo $this->data['CartVal'] = $this->cart_model->mani_cart_coupon_sucess($this->data['common_user_id']);
        return;
    }

    public function checkCodeSuccess() {
        echo $this->data['CartVal'] = $this->cart_model->mani_cart_coupon_sucess($this->data['common_user_id']);
    }

    public function ajaxChangeAddress() {
        if($this->input->post('add_id') != '') {
			if($this->session->userdata('currentShipId')!=''){
				$this->session->unset_userdata('currentShipId');
			}
            $ChangeAdds = $this->cart_model->get_all_details(SHIPPING_ADDRESS, array('user_id' => $this->data['common_user_id'], 'id' => $this->input->post('add_id')));
            $ShipCostVal = $this->cart_model->get_all_details(COUNTRY_LIST, array('country_code' => $ChangeAdds->row()->country));
			$this->session->set_userdata('currentShipId', $ChangeAdds->row()->id);
            $MainShipCost = number_format($ShipCostVal->row()->shipping_cost, 2, '.', '');
            $MainTaxCost = number_format(($this->input->post('amt') * 0.01 * $ShipCostVal->row()->shipping_tax), 2, '.', '');
            $TotalAmts = number_format(( ($this->input->post('amt') + $MainShipCost + $MainTaxCost) - $this->input->post('disamt')), 2, '.', '');

            $condition = array('user_id' => $this->data['common_user_id']);
            $dataArr2 = array('shipping_cost' => $MainShipCost, 'tax' => $ShipCostVal->row()->shipping_tax);
            $this->cart_model->update_details(SHOPPING_CART, $dataArr2, $condition);
            echo $Chg_Adds = $MainShipCost . '|' . $MainTaxCost . '|' . $ShipCostVal->row()->shipping_tax . '|' . $TotalAmts . '|' . $ChangeAdds->row()->full_name . '<br>' . $ChangeAdds->row()->address1 . '<br>' . $ChangeAdds->row()->city . ' ' . $ChangeAdds->row()->state . ' ' . $ChangeAdds->row()->postal_code . '<br>' . $ChangeAdds->row()->country . '<br>' . $ChangeAdds->row()->phone;
        } else {
            echo '0';
        }
    }

    public function ajaxSubscribeAddress() {

        if ($this->input->post('add_id') != '') {
            $ChangeAdds = $this->cart_model->get_all_details(SHIPPING_ADDRESS, array('user_id' => $this->data['common_user_id'], 'id' => $this->input->post('add_id')));

            $ShipCostVal = $this->cart_model->get_all_details(COUNTRY_LIST, array('country_code' => $ChangeAdds->row()->country));
            $MainShipCost = number_format($ShipCostVal->row()->shipping_cost, 2, '.', '');
            $MainTaxCost = number_format(($this->input->post('amt') * 0.01 * $ShipCostVal->row()->shipping_tax), 2, '.', '');
            $TotalAmts = number_format(($this->input->post('amt') + $MainShipCost + $MainTaxCost), 2, '.', '');


            echo $Chg_Adds = $MainShipCost . '|' . $MainTaxCost . '|' . $ShipCostVal->row()->shipping_tax . '|' . $TotalAmts . '|' . $ChangeAdds->row()->full_name . '<br>' . $ChangeAdds->row()->address1 . '<br>' . $ChangeAdds->row()->city . ' ' . $ChangeAdds->row()->state . ' ' . $ChangeAdds->row()->postal_code . '<br>' . $ChangeAdds->row()->country . '<br>' . $ChangeAdds->row()->phone;
        } else {
            echo '0';
        }
    }

    public function ajaxDeleteAddress() {
        if ($this->checkLogin('U') == '') {
            redirect('login');
        } else {
            $delID = $this->input->post('del_ID');
            $checkAddrCount = $this->cart_model->get_all_details(SHIPPING_ADDRESS, array('id' => $delID, 'primary' => 'Yes'));

            if ($checkAddrCount->num_rows == 0) {
                $this->cart_model->commonDelete(SHIPPING_ADDRESS, array('id' => $delID));
                echo '0';
            } else {
                echo '1';
            }
        }
    }

    public function insert_shipping_address() {
        if ($this->checkLogin('U') == '') {
            redirect('login');
        } else {
            $is_default = $this->input->post('set_default');
            if ($is_default == '') {
                $primary = 'No';
            } else {
                $primary = 'Yes';
            }
            $checkAddrCount = $this->cart_model->get_all_details(SHIPPING_ADDRESS, array('user_id' => $this->checkLogin('U')));
            if ($checkAddrCount->num_rows == 0) {
                $primary = 'Yes';
            }
            $excludeArr = array('ship_id', 'set_default');
            $dataArr = array('primary' => $primary);


            $this->cart_model->commonInsertUpdate(SHIPPING_ADDRESS, 'insert', $excludeArr, $dataArr, $condition);
            $shipID = $this->cart_model->get_last_insert_id();
            if ($this->lang->line('ship_add_success') != '')
                $lg_err_msg = $this->lang->line('ship_add_success');
            else
                $lg_err_msg = 'Shipping address added successfully';
            $this->setErrorMessage('success', $lg_err_msg);

            if ($primary == 'Yes') {
                $condition = array('id !=' => $shipID, 'user_id' => $this->checkLogin('U'));
                $dataArr = array('primary' => 'No');
                $this->cart_model->update_details(SHIPPING_ADDRESS, $dataArr, $condition);
            }
            redirect('cart');
        }
    }

    public function cartcheckout() {

        $checkout_type = $this->input->post('checkout_type');
        $userid = $this->checkLogin('U');

        if (isset($checkout_type) && $checkout_type == 'giftpurchase') {
            $paymentType = $this->input->post('giftpayment_method');
            /*             * **********Payment Methods************ */
            $paymentGateway = $this->cart_model->get_all_details(PAYMENT_GATEWAY, array('status' => 'Enable', 'id' => $paymentType));
            /*             * **********End************ */
            redirect("checkout/gift/" . $paymentType);
        } else {

            if ($this->input->post('ship_need') > 0 && $this->input->post('Ship_address_val') == '') {
                if ($this->lang->line('add_ship_addr_msg') != '')
                    $lg_err_msg = $this->lang->line('add_ship_addr_msg');
                else
                    $lg_err_msg = 'Please Add the Shipping address';
                $this->setErrorMessage('error', $lg_err_msg);
                redirect("cart");
            }else {

                if ($this->input->post('ship_need') > 0) {

                    $shipping_address = $this->cart_model->get_all_details(SHIPPING_ADDRESS, array('id' => $this->input->post('Ship_address_val')));
                    $userid = $this->checkLogin('U');
                    if ($this->data['userDetails']->row()->postal_code == '' || $this->data['userDetails']->row()->postal_code == 0) {
                        $this->cart_model->update_details(USERS, array('postal_code' => $shipping_address->row()->postal_code), array('id' => $userid));
                    }
                    if ($this->data['userDetails']->row()->address == '') {
                        $this->cart_model->update_details(USERS, array('address' => $shipping_address->row()->address1), array('id' => $userid));
                    }
                    if ($this->data['userDetails']->row()->address2 == '') {
                        $this->cart_model->update_details(USERS, array('address2' => $shipping_address->row()->address2), array('id' => $userid));
                    }
                    if ($this->data['userDetails']->row()->city == '') {
                        $this->cart_model->update_details(USERS, array('city' => $shipping_address->row()->city), array('id' => $userid));
                    }
                    if ($this->data['userDetails']->row()->state == '') {
                        $this->cart_model->update_details(USERS, array('state' => $shipping_address->row()->state), array('id' => $userid));
                    }
                    if ($this->data['userDetails']->row()->country == '') {
                        $this->cart_model->update_details(USERS, array('country' => $shipping_address->row()->country), array('id' => $userid));
                    }
                    if ($this->data['userDetails']->row()->phone_no == '') {
                        $this->cart_model->update_details(USERS, array('phone_no' => $shipping_address->row()->phone), array('id' => $userid));
                    }
                }

                $paymentType = $this->input->post('payment_method');
                /*                 * **********Payment Methods************ */
                $paymentGateway = $this->cart_model->get_all_details(PAYMENT_GATEWAY, array('status' => 'Enable', 'id' => $paymentType));
                /*                 * **********End************ */
                $this->cart_model->addPaymentCart($userid, $paymentGateway->row()->gateway_name, $this->input->post('ship_products'),'cart');
                redirect("checkout/cart/" . $this->input->post('payment_method'));
            }
        }
    }

    public function Subscribecheckout() {
        if ($this->input->post('SubShip_address_val') != '') {
            $shipping_address = $this->cart_model->get_all_details(SHIPPING_ADDRESS, array('id' => $this->input->post('SubShip_address_val')));
            $userid = $this->checkLogin('U');
            if ($this->data['userDetails']->row()->postal_code == '' || $this->data['userDetails']->row()->postal_code == 0) {
                $this->cart_model->update_details(USERS, array('postal_code' => $shipping_address->row()->postal_code), array('id' => $userid));
            }
            if ($this->data['userDetails']->row()->address == '') {
                $this->cart_model->update_details(USERS, array('address' => $shipping_address->row()->address1), array('id' => $userid));
            }
            if ($this->data['userDetails']->row()->address2 == '') {
                $this->cart_model->update_details(USERS, array('address2' => $shipping_address->row()->address2), array('id' => $userid));
            }
            if ($this->data['userDetails']->row()->city == '') {
                $this->cart_model->update_details(USERS, array('city' => $shipping_address->row()->city), array('id' => $userid));
            }
            if ($this->data['userDetails']->row()->state == '') {
                $this->cart_model->update_details(USERS, array('state' => $shipping_address->row()->state), array('id' => $userid));
            }
            if ($this->data['userDetails']->row()->country == '') {
                $this->cart_model->update_details(USERS, array('country' => $shipping_address->row()->country), array('id' => $userid));
            }
            if ($this->data['userDetails']->row()->phone_no == '') {
                $this->cart_model->update_details(USERS, array('phone_no' => $shipping_address->row()->phone), array('id' => $userid));
            }

            $this->cart_model->addPaymentSubscribe($userid);
            redirect("checkout/subscribe/" . $this->input->post('subscrip_id'));
        } else {
            if ($this->lang->line('add_ship_addr_msg') != '')
                $lg_err_msg = $this->lang->line('add_ship_addr_msg');
            else
                $lg_err_msg = 'Please Add the Shipping address';
            $this->setErrorMessage('error', $lg_err_msg);

            redirect("cart");
        }
    }

    public function getQty($pid, $val) {
        if ($pid > 0 && $this->checkLogin('U') != '' && $val > 0) {
            $Query = "select sum(quantity) as QTY from " . SHOPPING_CART . " where product_id='" . $pid . "' and user_id='" . $this->checkLogin('U') . "' and id!='" . $val . "'";
            $resultArr = $this->cart_model->ExecuteQuery($Query);
            if ($resultArr->num_rows() > 0) {
                $qty = $resultArr->row()->QTY;
                if ($qty == '') {
                    $qty = 0;
                }
            } else {
                $qty = 0;
            }
        } else {
            $qty = 0;
        }
        echo $qty;
    }
	public function checkProduct(){
		$sid = $this->input->post('sell_id');
		$uid = $this->checkLogin('U');
		if($sid == $uid){
			$returnArr['success'] = 2;
		}else{
			
			$pid = $this->input->post('pid');
			$cartDetails = $this->cart_model->get_all_details(SHOPPING_CART, array('type' =>'cart', 'user_id'=> $this->data['common_user_id']));
			$product = $this->cart_model->get_all_details(PRODUCT, array('id'=>$pid));
			if($cartDetails->num_rows()>0){
				foreach($cartDetails->result() as $_cartDetails){
					$productType[] = $_cartDetails->product_type;
				}
				if(in_array($product->row()->product_type, $productType)){
					$returnArr['success'] = 1;
				}else{
					$returnArr['success'] = 0;
				}
			}else{
				$returnArr['success'] = 1;
			}
		}
		echo json_encode($returnArr);
	}
	public function checkProductauct(){
		$sid = $this->input->post('sell_id');
		$uid = $this->checkLogin('U');
		if($sid == $uid){
			$returnArr['success'] = 2;
		}else{
			
			$pid = $this->input->post('pid');
			$cartDetails = $this->cart_model->get_all_details(SHOPPING_CART, array('type' =>'auction','user_id'=> $this->data['common_user_id']));
			$product = $this->cart_model->get_all_details(PRODUCT, array('id'=>$pid));
			if($cartDetails->num_rows()>0){
				foreach($cartDetails->result() as $_cartDetails){
					$productType[] = $_cartDetails->product_type;
				}
				if(in_array($product->row()->product_type, $productType)){
					$returnArr['success'] = 1;
				}else{
					$returnArr['success'] = 0;
				}
			}else{
				$returnArr['success'] = 1;
			}
		}
		echo json_encode($returnArr);
	}
}

/* End of file user.php */
/* Location: ./application/controllers/site/user.php */
