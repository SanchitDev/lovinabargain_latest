<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ios_store extends MY_Controller {

	function __construct(){
        parent::__construct();
		$this->load->helper(array('cookie','date','form','email'));
		$this->load->library(array('encrypt','form_validation'));
		$this->load->model(array('order_model','mobile_model','product_model','user_model','cart_model'));
		if($_SESSION['sMainCategories'] == ''){
               $sortArr1 = array('field'=>'cat_position','type'=>'asc');
               $sortArr = array($sortArr1);
               $_SESSION['sMainCategories'] = $this->product_model->get_all_details(CATEGORY,array('rootID'=>'0','status'=>'Active'),$sortArr);
          }
		$commonId=intval($_GET['commonId']);
		$cartCount=0;
		if($commonId>0){
			$cartCount=$this->mobile_model->mini_cart_view($commonId);
		}
		$this->data["cartCount"]= $cartCount;
		$this->data["commonId"]= $commonId;
		$temp_id = substr(number_format(time() * rand(),0,'',''),0,8);
		$this->data['likedProducts'] = $this->mobile_model->get_all_details(PRODUCT_LIKES, array('user_id' => $commonId));
        $this->data['orderpurchaseProducts'] = $this->mobile_model->get_orderpurchase_details($commonId);
		$GlobalUserName='';
    }

	////////Seller Subscription///////////
	public function open_store(){
		$returnArr['status'] = '0';
		$returnArr['response'] = '';
		$userId = trim($_POST['userId']);
		if($userId!=''){
			$userDetails = $this->mobile_model->get_all_details(USERS, array('id'=> $userId));
			if($userDetails->num_rows() >0){
				$userType = $userDetails->row()->group;
				if($userType == 'Seller'){
					if($userDetails->row()->store_payment != 'Paid'){
						if($this->config->item('storefront_fees_month')!= 0){
							if($this->lang->line('product_need_pay') != '') {
								$monthPay = $stripslashes($this->lang->line('product_need_pay'));
							}else{
								$monthPay = "You need to pay";
							}
							if($this->lang->line('store_up_fees') != '') {
								$monthfees =  stripslashes($this->lang->line('store_up_fees'));
							}else{
								$monthfees =  "for store fees per month";
							}
							$fees[]= array(
								'plan' => 'month',
								'amount'=> $this->config->item('storefront_fees_month'),
								'text' => $monthPay.' '.$this->data['currencySymbol'].$this->config->item('storefront_fees_month').' '.$monthfees,
							);
						}
						if($this->config->item('storefront_fees_year')!= 0){
							if($this->lang->line('product_need_pay') != '') {
								$yearPay = $stripslashes($this->lang->line('product_need_pay'));
							}else{
								$yearPay = "You need to pay";
							}
							if($this->lang->line('store_up_fees') != '') {
								$yearfees =  stripslashes($this->lang->line('store_up_fees'));
							}else{
								$yearfees =  "for store fees per year";
							}
							$fees[]= array(
								'plan' => 'year',
								'amount'=> $this->config->item('storefront_fees_year'),
								'text' => $yearPay.' '.$this->data['currencySymbol'].$this->config->item('storefront_fees_year').' '.$yearfees,
							);
						}
						$returnArr['status'] = '1';
						$userProfileDetails = array(
								'id'=> $userDetails->row()->id,
								'userName'=> $userDetails->row()->user_name,
								'fullName'=> $userDetails->row()->full_name,
								'email'=> $userDetails->row()->email,
						);
						$payment[] = array('gatewayId' => 1, 'gatewayName' => 'Paypal');
						$returnArr['userProfileDetails'] = $userProfileDetails;
						$returnArr['fees'] = $fees;
						$returnArr['payment'] = $payment;
					}else{
						$returnArr['response'] = 'Store Already subscribed';
					}
				}else{
					$returnArr['response'] = 'Invalid Seller';
				}
			}else{
				$returnArr['response'] = 'User details not available';
			}
		}else{
			$returnArr['response'] = 'UserId Null';
		}
		echo json_encode($returnArr);
	}

	
	public function open_storeForm(){
		$userId = trim($_GET['userId']);
		$planValue = trim($_GET['amount']);
		$planType = trim($_GET['plan']);
		$paymentId = trim($_GET['paymentId']);
		if($userId!=''){
			$userDetails = $this->mobile_model->get_all_details(USERS, array('id'=> $userId));
			if($userDetails->num_rows() >0){
				$this->data['userDetails'] = $userDetails;
				$userType = $userDetails->row()->group;
				if($userType == 'Seller'){
					if($userDetails->row()->store_payment != 'Paid'){
						$InvoiceNo = mt_rand();
						$checkId = $this->mobile_model->get_all_details(STORE_ARB, array('invoice_no' => $InvoiceNo));
						while($checkId->num_rows() > 0){
							$InvoiceNo = mt_rand();
							$checkId = $this->mobile_model->get_all_details(STORE_ARB, array('invoice_no' => $InvoiceNo));
						}
						$this->data['countryList'] = $this->mobile_model->get_all_details(COUNTRY_LIST, array());
						$this->data['InvoiceNo'] = $InvoiceNo;
						$this->data['plan_value'] = $planValue;
						$this->data['appWeb'] = "1";
						$this->load->view('mobile/checkout/store', $this->data);
					}else{
						$returnArr['status'] = '0';
						$returnArr['response'] = 'Store Already subscribed';
						echo json_encode($returnArr);
					}
				}else{
					$returnArr['status'] = '0';
					$returnArr['response'] = 'Invalid Seller';
					echo json_encode($returnArr);
				}
			}else{
				$returnArr['status'] = '0';
				$returnArr['response'] = 'User details not available';
				echo json_encode($returnArr);
			}
		}else{
			$returnArr['status'] = '0';
			$returnArr['response'] = 'UserId Null';
			echo json_encode($returnArr);
		}
	}
}
