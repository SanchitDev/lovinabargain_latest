<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * User related functions
 * @author Teamtweaks
 *
 */

class Ios extends MY_Controller {
	function __construct(){
        parent::__construct();
		$this->load->helper(array('cookie','date','form','email'));
		$this->load->library(array('encrypt','form_validation'));
		$this->load->model(array('order_model','mobile_model','product_model','user_model','cart_model','store_model','giftcards_model'));
		if($_SESSION['sMainCategories'] == ''){
               $sortArr1 = array('field'=>'cat_position','type'=>'asc');
               $sortArr = array($sortArr1);
               $_SESSION['sMainCategories'] = $this->product_model->get_all_details(CATEGORY,array('rootID'=>'0','status'=>'Active'),$sortArr);
          }
		/* $ua = strtolower($_SERVER['HTTP_USER_AGENT']);
		if(stripos($ua,'shopsymobileapp') === false) {
			show_404();
		} */
		$commonId=intval($_GET['commonId']);
		$cartCount=0;
		if($commonId>0){
			$cartCount=$this->mobile_model->mini_cart_view($commonId);
		}
		$this->data["cartCount"]=$cartCount;
		$this->data["commonId"]=$commonId;
		$temp_id = substr(number_format(time() * rand(),0,'',''),0,8);
		$this->data['likedProducts'] = $this->mobile_model->get_all_details(PRODUCT_LIKES, array('user_id' => $commonId));
        $this->data['orderpurchaseProducts'] = $this->mobile_model->get_orderpurchase_details($commonId);
		$GlobalUserName='';
		/*Currency Settings*/
		/*if($commonId>0){
			$checkUserPreference=$this->mobile_model->get_all_details(USER,array('id' => $commonId));
			if($checkUserPreference->num_rows()>=1){
				$condition = array('currency_code'=> $checkUserPreference->row()->currency);
				$GlobalUserName=$checkUserPreference->row()->user_name;
			}else{
				$condition = array('currency_code'=>'USD');
			}
		} else {
			$condition = array('currency_code'=>'USD');
		}
		$CurrencyVal=$this->product_model->get_all_details(CURRENCY,$condition);
		$this->data["currencySymbol"]=$CurrencyVal->row()->currency_symbol;
		$this->data["currencyCode"]=$CurrencyVal->row()->currency_code;
		$this->data["currencyValue"]=$CurrencyVal->row()->currency_value;
		*/
		#echo $this->data["currencySymbol"].'|'.$this->data["currencyCode"].'|'.$this->data["currencyValue"];
    }


	public function index(){
	}

    /*
    !! >>>>>>>>>> User Login <<<<<<<<<< !!
    */
    public function userLogin(){
        $returnArr['status'] = '0';
        $returnArr['message']='';
        $UserNamestr = $this->input->post('username');
        $PassWordstr = $this->input->post('password');
        if($UserNamestr !="" && $PassWordstr !=""){
	        $condition = '(email = \''.$UserNamestr.'\' OR user_name = \''.$UserNamestr.'\') AND password=\''.md5($PassWordstr).'\'';
	        $userInfoDetails = $this->mobile_model->get_all_details(USERS,$condition);
	        if($userInfoDetails->num_rows()==1){
	                $userInfo = 'Success';
	                if($userInfoDetails->row()->thumbnail!=""){
	                    $userImage=base_url().'images/users/thumb/'.$userInfoDetails->row()->thumbnail;
	                }else{
	                    $userImage=base_url().'images/users/thumb/profile_pic.png';
	                }
	                /*push update*/
	                    $UDID='';
	                 	if($_POST['deviceToken']!=""){
	                        $device_type='ios';
	                        $UDID = $_POST['deviceToken'];
	                        $deviceID = $_POST['deviceID'];
	                    }
	                    if($UDID!=""){
	                        $this->mobile_model->insertupdatePushKey($userInfoDetails->row()->id,$UDID,'user',$device_type);
	                    }
	                /*End*/
	                $this->mobile_model->update_details(SHOPPING_CART,array("user_id"=>$userInfoDetails->row()->id),array("user_id"=>$this->data["commonId"]));
	                $returnArr['status'] = '1';
	                $returnArr['message'] = 'Success';
	                if($userInfoDetails->row()->thumbnail!=""){
	                    $user_image=base_url().'images/users/'.$userInfoDetails->row()->thumbnail;
	                }else{
	                    $user_image=base_url().'images/users/user-thumb1.png';
	                }
	                $returnArr['user_image'] = $user_image;
	                $returnArr['user_id'] = (string)$userInfoDetails->row()->id;
	                $returnArr['user_name'] = $userInfoDetails->row()->user_name;
	                $returnArr['email'] = $userInfoDetails->row()->email;
	                $returnArr['user_fullname'] = $userInfoDetails->row()->full_name;
					$returnArr['userGroup'] = $userInfoDetails->row()->group;
					$returnArr['userType'] = "Normal";
					if($userInfoDetails->row()->store_payment == "Paid"){
						$returnArr['userType'] = "Upgraded";
					}
	        }else{
	                $returnArr['message'] = 'Invalid User';
	        }
        }else{
            $returnArr['message'] = 'Some Parameters are missing';
        }
        $json_encode = json_encode($returnArr);
        echo $json_encode;
    }

    /*
	* !! >>>>>>>>>>>>> Forgot Password  <<<<<<<<<<<< !!
	*/
	public function forgotPasswordUser() {
		$returnArr['status']  =  '0';
		$returnArr['response']  =  '';
		$email = $_POST['email'];
		if (valid_email($email)) {
			$condition = array('email' => $email);
			$checkUser = $this->mobile_model->get_all_details(USERS, $condition);
			if ($checkUser->num_rows() == '1') {
				$pwd = $this->get_rand_str('6');
				$newdata = array('password' => md5($pwd));
				$condition = array('email' => $email);
				$this->mobile_model->update_details(USERS, $newdata, $condition);
				$this->send_user_password($pwd, $checkUser);
				if ($this->lang->line('pwd_sen_mail') != ''){
					$lg_err_msg = $this->lang->line('pwd_sen_mail');
				}else{
					$lg_err_msg = 'New password sent to your mail';
				}
				$returnArr['response']  =  $lg_err_msg;
				$returnArr['status']  =  '1';
			}else {
				if ($this->lang->line('mail_not_record') != ''){
					$lg_err_msg = $this->lang->line('mail_not_record');
				}else{
					$lg_err_msg = 'Your email id not matched in our records';
				}
				$returnArr['response']  =  $lg_err_msg;
			}
		}else {
			if ($this->lang->line('mail_not_valid') != ''){
				$lg_err_msg = $this->lang->line('mail_not_valid');
			}else{
				$lg_err_msg = 'Email id not valid';
			}
			$returnArr['response']  =  $lg_err_msg;
		}
		echo json_encode($returnArr);
	}

    /*
	* !! >>>>>>>>>>>>> Send User Password  <<<<<<<<<<<< !!
	*/

    public function send_user_password($pwd='',$query,$code,$id){

		$newsid='5';
		$template_values=$this->mobile_model->get_newsletter_template_details($newsid);
		$adminnewstemplateArr=array('email_title'=> $this->config->item('email_title'),'logo'=> $this->data['logo']);
		extract($adminnewstemplateArr);
		$pwdlnk=base_url().'resetPassword/'.$code.'/'.$id.'';
		//$cfmurl = base_url().'site/user/confirm_change_email/'.$uid."/".$encode_email."/".$randStr."/confirmation";
		$subject = 'From: '.$this->config->item('email_title').' - '.$template_values['news_subject'];
  		$message .= '<!DOCTYPE HTML>
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
			<meta name="viewport" content="width=device-width"/>
			<title>'.$template_values['news_subject'].'</title>
			<body>';
		include('./newsletter/registeration'.$newsid.'.php');
		$message .= '</body>
			</html>';
		if($template_values['sender_name']=='' && $template_values['sender_email']==''){
			$sender_email=$this->config->item('site_contact_mail');

			$sender_name=$this->config->item('email_title');
		}else{
			$sender_name=$template_values['sender_name'];
			$sender_email=$template_values['sender_email'];
		}
		$email_values = array('mail_type'=>'html',
							'from_mail_id'=>$sender_email,
							'mail_name'=>$sender_name,
							'to_mail_id'=>$query->row()->email,
							'subject_message'=>'Password Reset',
							'body_messages'=>$message
							);
		$email_send_to_common = $this->mobile_model->common_email_send($email_values);
	}


    /*
    * !! >>>>>>>>>>>>> Facebook Login <<<<<<<<<<<< !!
    */

    public function facebookLogin(){
		$returnArr['status'] = '0';
		$returnArr['response']='';
		$email = $_POST['email'];
		$first_name = $_POST['firstName'];
		$first_name = utf8_decode($first_name);
		$profile_image = $_POST['profile_image'];
		$condition = array('email' => $email, "status" => "Active");
		$userInfoDetails = $this->mobile_model->get_all_details(USERS,$condition);
		if($userInfoDetails->num_rows()==1){
				$newdata = array('last_login_date' => date("Y-m-d h:i:s"));
				$condition = array('id' => $userInfoDetails->row()->id);
				$this->mobile_model->update_details(USERS,$newdata,$condition);
					$UDID='';
					if($_POST['deviceToken']!=""){
						$device_type='ios';
						$UDID = $_POST['deviceToken'];
					}
					if($UDID!=""){
						$this->mobile_model->insertupdatePushKey($userInfoDetails->row()->id,$UDID,'user',$device_type);
					}
				if($userInfoDetails->row()->thumbnail!=""){
					$user_image=base_url().'images/users/'.$userInfoDetails->row()->thumbnail;
				}else{
					$user_image=base_url().'images/users/user-thumb1.png';
				}
				$returnArr['status'] = '1';
				$returnArr['response'] = 'Success';
				$returnArr['registeredUser'] = "1";
				$returnArr['userImage'] = $user_image;
				$returnArr['userId'] = (string)$userInfoDetails->row()->id;
				$returnArr['userName'] = $userInfoDetails->row()->user_name;
				$returnArr['email'] = $userInfoDetails->row()->email;
				$returnArr['userFullname'] = utf8_encode($userInfoDetails->row()->full_name);
				$returnArr['userGroup'] = $userInfoDetails->row()->group;
				$returnArr['userType'] = "Normal";
				if($userInfoDetails->row()->store_payment == "Paid"){
					$returnArr['userType'] = "Upgraded";
				}
		}else{
			if($email!=" "){
				$passwordstr=$this->get_rand_str(8);
				$this->db->select('id');
				$this->db->from(USERS);
				$this->db->where('email',$email);
				$userNameDetails = $this->db->get();
				$chckUserName = strtok($email,'@');
				$this->db->select('id');
				$this->db->from(USERS);
				$this->db->where('user_name',$chckUserName);
				$userNamechecking = $this->db->get();
				if($userNamechecking->num_rows() > 0){
						$count = $userNamechecking->num_rows();
						$count = $count+1;
						$chckUserName = $chckUserName.$count;
				}
				if($userNameDetails->num_rows()>0){
					$returnArr['response'] ='User Already Exist';
				}else{
					if($profile_image!=""){
						$imgPath ='images/users/temp/';
						$img =$profile_image;
						$imgName = time(). ".jpg";
						$imageFormat = array('data:image/jpeg;base64', 'data:image/png;base64', 'data:image/jpg;base64', 'data:image/gif;base64');
						$img = str_replace($imageFormat, '', $img);
						$data = base64_decode($img);
						$image = @imagecreatefromstring($data);
						if ($image !== false) {
							$uploadPath = $imgPath . $imgName;
							imagejpeg($image, $uploadPath, 100);
							imagedestroy($image);
						} else {
							$responseArr['response'] ='An error occurred2';
						}
						if (isset($uploadPath)) {
							$filename = base_url($uploadPath);
							list($width, $height) = getimagesize($filename);
							$newwidth = 100;
							$newheight = 100;
							$sourceImage = @imagecreatefromjpeg($filename);
							imagecopyresized($thumbImage, $sourceImage, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
						}else {
							$responseArr['response'] ='Invalid File';
						}
						$new_name = $imgName;
						@copy("./images/users/temp/".$imgName, './images/users/'.$new_name);
						@copy("./images/users/temp/".$imgName, './images/users/thumb/'.$new_name);
						$this->imageResizeWithSpace(210, 210, $new_name, './images/users/thumb/');
						$this->imageResizeWithSpace(600, 600, $new_name, './images/users/');
						$dir =getcwd().DIRECTORY_SEPARATOR ."images".DIRECTORY_SEPARATOR ."users".DIRECTORY_SEPARATOR."temp".DIRECTORY_SEPARATOR ;
						$interval = strtotime('-24 hours');
						foreach (glob($dir."*.*") as $file){
							if (filemtime($file) <= $interval ){
								if(!is_dir($file) ){
									unlink($file);
								}
							}
						}
						$thumbnail = $imgName;
						}else{
							$thumbnail='';
						}
						$dataArr = array(
													'loginUserType'=>'facebook',
													'full_name'=>$first_name,
													'user_name'=>$chckUserName,
													'thumbnail'=>$thumbnail,
													'email'=>$email,
													'password'=>md5($passwordstr),
													'status'=>'Active',
													'is_verified'=>'No',
										);
						$this->db->insert(USERS,$dataArr);
						$this->db->select('*');
						$this->db->from(USERS);
						$this->db->where('email',$email);
						$checkUser = $this->db->get();
						$UDID='';
						 if($_POST['deviceToken']!=""){
							$device_type='ios';
							$UDID = $_POST['deviceToken'];
						}
						if($UDID!=""){
							$this->mobile_model->insertupdatePushKey($checkUser->row()->id,$UDID,'user',$device_type);
						}
						$userDetails=$checkUser;
						$this->send_confirm_mail($userDetails);
						if($checkUser->row()->thumbnail!=""){
							$userImage=base_url().'images/users/'.$checkUser->row()->thumbnail;
						}else{
							$userImage=base_url().'images/users/user-thumb1.png';
						}
						$userInfoDetails = $this->mobile_model->get_all_details(USERS,array('email'=>$email));
						if($userInfoDetails->row()->thumbnail!=""){
							$user_image=base_url().'images/users/'.$userInfoDetails->row()->thumbnail;
						}else{
							$user_image=base_url().'images/users/user-thumb1.png';
						}
						$returnArr['status'] = '1';
						$returnArr['response'] = 'Success';
						$returnArr['userImage'] = $user_image;
						$returnArr['userId'] = (string)$userInfoDetails->row()->id;
						$returnArr['userName'] = $userInfoDetails->row()->user_name;
						$returnArr['email'] = $userInfoDetails->row()->email;
						$returnArr['userFullname'] = utf8_encode($userInfoDetails->row()->full_name);
						$returnArr['userGroup'] = $userInfoDetails->row()->group;
						$returnArr['userType'] = "Normal";
						if($userInfoDetails->row()->store_payment == "Paid"){
							$returnArr['userType'] = "Upgraded";
						}
					}
			}else{
				$returnArr['response'] = 'Some Parameters are Missing';
			}
		}
		echo json_encode($returnArr);
	}


    /*
    * !! >>>>>>>>>>>>> Send Confirmation Mail <<<<<<<<<<<< !!
    */

    public function send_confirm_mail($userDetails=''){
        $uid = $userDetails->row()->id;
        $email = $userDetails->row()->email;
        $name = $userDetails->row()->full_name;
        $randStr = $this->get_rand_str('10');
        $condition = array('id'=>$uid);
        $dataArr = array('verify_code'=>$randStr);
        $this->db->where('id', $uid);
        $this->db->update(USERS, $dataArr);
        $newsid='3';
        $this->db->select('*');
        $this->db->from(NEWSLETTER);
        $this->db->where('id',$newsid);
        $this->db->where('status','Active');
        $template_values = $this->db->get()->result_array();
        $cfmurl = base_url().'site/user/confirm_register/'.$uid."/".$randStr."/confirmation";
        $subject = 'From: '.$this->config->item('email_title').' - '.$template_values[0]['news_subject'];
        $adminnewstemplateArr=array('email_title'=> $this->config->item('email_title'),'logo'=> $this->data['logo'],'footer_content'=> $this->config->item('footer_content'));
        extract($adminnewstemplateArr);
        //$ddd =htmlentities($template_values['news_descrip'],null,'UTF-8');
        $header .="Content-Type: text/plain; charset=ISO-8859-1\r\n";
        $message .= '<!DOCTYPE HTML>
            <html>
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <meta name="viewport" content="width=device-width"/><body>';
        include('./newsletter/registeration'.$newsid.'.php');
        $message .= '</body>
            </html>';
        if($template_values[0]['sender_name']=='' && $template_values[0]['sender_email']==''){
            $sender_email=$this->data['siteContactMail'];
            $sender_name=$this->data['siteTitle'];
        }else{
            $sender_name=$template_values[0]['sender_name'];
            $sender_email=$template_values[0]['sender_email'];
        }
        $email_values = array('mail_type'=>'html',
                            'from_mail_id'=>$sender_email,
                            'mail_name'=>$sender_name,
                            'to_mail_id'=>$email,
                            'subject_message'=>$template_values[0]['news_subject'],
                            'body_messages'=>$message
                            );
        $email_send_to_common = $this->mobile_model->common_email_send($email_values);
    }


    /*
    * !! >>>>>>>>>>>>> Google Login <<<<<<<<<<<< !!
    */

    public function googleLogin(){
    	$returnArr['status'] = '0';
		$email =$_POST['email'];
		$first_name =$_POST['firstName'];
		$first_name = utf8_decode($first_name);
		$profile_image =$_POST['profile_image'];
		$condition = array("email" => $email, "status" => "Active");
		$userInfoDetails = $this->mobile_model->get_all_details(USERS,$condition);
		if($userInfoDetails->num_rows()==1){
				$newdata = array(
	               'last_login_date' => date("Y-m-d h:i:s"),
	    	        );
				$condition = array('id' => $userInfoDetails->row()->id);
				$this->mobile_model->update_details(USERS,$newdata,$condition);
				/*push update*/
					$UDID='';
					if($_POST['deviceToken']!=""){
						$device_type='ios';
						$UDID = $_POST['deviceToken'];
					}
					if($UDID!=""){
						$this->mobile_model->insertupdatePushKey($userInfoDetails->row()->id,$UDID,'user',$device_type);
					}

					if($userInfoDetails->row()->thumbnail!=""){
						$user_image=base_url().'images/users/'.$userInfoDetails->row()->thumbnail;
					}else{
						$user_image=base_url().'images/users/user-thumb1.png';
					}
				$returnArr['status'] = '1';
				$returnArr['message'] = 'Success';
				$returnArr['user_image'] = $user_image;
				$returnArr['user_id'] = (string)$userInfoDetails->row()->id;
				$returnArr['user_name'] = $userInfoDetails->row()->user_name;
				$returnArr['email'] = $userInfoDetails->row()->email;
				$returnArr['user_fullname'] = $userInfoDetails->row()->full_name;
				$returnArr['userGroup'] = $userInfoDetails->row()->group;
				$returnArr['userType'] = "Normal";
				if($userInfoDetails->row()->store_payment == "Paid"){
					$returnArr['userType'] = "Upgraded";
				}
				/*End*/
		}else{
			if($email!=''){
				$passwordstr=$this->get_rand_str(8);
				$this->db->select('id');
				$this->db->from(USERS);
				$this->db->where('email',$email);
				$userNameDetails = $this->db->get();
				$chckUserName = strtok($email,'@');
				$this->db->select('id');
				$this->db->from(USERS);
				$this->db->where('user_name',$chckUserName);
				$userNamechecking = $this->db->get();
				if($userNamechecking->num_rows() > 0){
						$count = $userNamechecking->num_rows();
						$count = $count+1;
						$chckUserName = $chckUserName.$count;
				}
				if($userNameDetails->num_rows()>0){
					$returnArr['response'] ='User Already Exist';
				}else{
					if($profile_image!=""){
						$imgPath ='images/users/temp/';
						$img =$profile_image;
						$imgName = time(). ".jpg";
						$imageFormat = array('data:image/jpeg;base64', 'data:image/png;base64', 'data:image/jpg;base64', 'data:image/gif;base64');
						$img = str_replace($imageFormat, '', $img);
						$data = base64_decode($img);
						$image = @imagecreatefromstring($data);
						if ($image !== false) {
							$uploadPath = $imgPath . $imgName;
							imagejpeg($image, $uploadPath, 100);
							imagedestroy($image);
						} else {
							$responseArr['response'] ='An error occurred2';
						}
						if (isset($uploadPath)) {
							$filename = base_url($uploadPath);
							list($width, $height) = getimagesize($filename);
							$newwidth = 100;
							$newheight = 100;
							$sourceImage = @imagecreatefromjpeg($filename);
							imagecopyresized($thumbImage, $sourceImage, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
						}else {
							$responseArr['response'] ='Invalid File';
						}
						$new_name = $imgName;
						@copy("./images/users/temp/".$imgName, './images/users/'.$new_name);
						@copy("./images/users/temp/".$imgName, './images/users/thumb/'.$new_name);
						$this->imageResizeWithSpace(210, 210, $new_name, './images/users/thumb/');
						$this->imageResizeWithSpace(600, 600, $new_name, './images/users/');
						$dir =getcwd().DIRECTORY_SEPARATOR ."images".DIRECTORY_SEPARATOR ."users".DIRECTORY_SEPARATOR."temp".DIRECTORY_SEPARATOR ;
						$interval = strtotime('-24 hours');
						foreach (glob($dir."*.*") as $file){
							if (filemtime($file) <= $interval ){
								if(!is_dir($file) ){
									unlink($file);
								}
							}
						}
						$thumbnail = $imgName;
						}else{
							$thumbnail='';
						}
						$dataArr = array(
													'loginUserType'=>'google',
													'full_name'=>$first_name,
													'user_name'=>$chckUserName,
													'thumbnail'=>$thumbnail,
													'email'=>$email,
													'password'=>md5($passwordstr),
													'status'=>'Active',
													'is_verified'=>'No',
										);
						$this->db->insert(USERS,$dataArr);
						$this->db->select('*');
						$this->db->from(USERS);
						$this->db->where('email',$email);
						$checkUser = $this->db->get();
						$UDID='';
						 if($_POST['deviceToken']!=""){
							$device_type='ios';
							$UDID = $_POST['deviceToken'];
						}
						if($UDID!=""){
							$this->mobile_model->insertupdatePushKey($checkUser->row()->id,$UDID,'user',$device_type);
						}
						$userDetails=$checkUser;
						$this->send_confirm_mail($userDetails);
						$userInfoDetails = $this->mobile_model->get_all_details(USERS,array('email'=>$email));
						if($userInfoDetails->row()->thumbnail!=""){
							$user_image=base_url().'images/users/'.$userInfoDetails->row()->thumbnail;
						}else{
							$user_image=base_url().'images/users/user-thumb1.png';
						}
						$returnArr['status'] = '1';
						$returnArr['response'] = 'Success';
						$returnArr['userImage'] = $user_image;
						$returnArr['userId'] = (string)$userInfoDetails->row()->id;
						$returnArr['userName'] = $userInfoDetails->row()->user_name;
						$returnArr['email'] = $userInfoDetails->row()->email;
						$returnArr['userFullname'] = utf8_encode($userInfoDetails->row()->full_name);
						$returnArr['userGroup'] = $userInfoDetails->row()->group;
						$returnArr['userType'] = "Normal";
						if($userInfoDetails->row()->store_payment == "Paid"){
							$returnArr['userType'] = "Upgraded";
						}
					}
			}else{
				$returnArr['message'] = 'Invalid Input';
			}
		}
		$json_encode = json_encode($returnArr);
		echo $json_encode;
	}

    /*
    !! >>>>>>>>>>>>>> Twitter Login <<<<<<<<<<<<< !!
    */

    public function twitterLogin(){
    	$returnArr['status'] = '0';
    	$returnArr['message']='';
		$email =$_POST['email'];
		$first_name =$_POST['firstName'];
		$first_name = utf8_decode($first_name);
		$profile_image =$_POST['profile_image'];
		$condition = array("email" => $email, "status" => "Active");
		$userInfoDetails = $this->mobile_model->get_all_details(USERS,$condition);
		if($userInfoDetails->num_rows()==1){
				$this->mobile_model->update_details(SHOPPING_CART,array("user_id"=>$userInfoDetails->row()->id),array("user_id"=>$this->data["commonId"]));
				$newdata = array(
	               'last_login_date' => date("Y-m-d h:i:s"),
	    	        );
				$condition = array('id' => $userInfoDetails->row()->id);
				$this->mobile_model->update_details(USERS,$newdata,$condition);
				/*push update*/
				$UDID='';
				 if($_POST['deviceToken']!=""){
					$device_type='ios';
					$UDID = $_POST['deviceToken'];
					$deviceID = $_POST['deviceID'];
				}
				if($UDID!=""){
					$this->mobile_model->insertupdatePushKey($userInfoDetails->row()->id,$UDID,'user',$device_type);
				}
				if($userInfoDetails->row()->thumbnail!=""){
					$user_image=base_url().'images/users/'.$userInfoDetails->row()->thumbnail;
				}else{
					$user_image=base_url().'images/users/user-thumb1.png';
				}
				$returnArr['status'] = '1';
				$returnArr['message'] = 'Success';
				$returnArr['user_image'] = $user_image;
				$returnArr['user_id'] = (string)$userInfoDetails->row()->id;
				$returnArr['user_name'] = $userInfoDetails->row()->user_name;
				$returnArr['email'] = $userInfoDetails->row()->email;
				$returnArr['user_fullname'] = utf8_encode($userInfoDetails->row()->full_name);
				$returnArr['userGroup'] = $userInfoDetails->row()->group;
				$returnArr['userType'] = "Normal";
				if($userInfoDetails->row()->store_payment == "Paid"){
					$returnArr['userType'] = "Upgraded";
				}
				/*End*/
		}else{
			if($email!=''){
				$passwordstr=$this->get_rand_str(8);
				$this->db->select('id');
				$this->db->from(USERS);
				$this->db->where('email',$email);
				$userNameDetails = $this->db->get();
				$chckUserName = strtok($email,'@');
				$this->db->select('id');
				$this->db->from(USERS);
				$this->db->where('user_name',$chckUserName);
				$userNamechecking = $this->db->get();
				if($userNamechecking->num_rows() > 0){
						$count = $userNamechecking->num_rows();
						$count = $count+1;
						$chckUserName = $chckUserName.$count;
				}
				if($userNameDetails->num_rows()>0){
					$returnArr['response'] ='User Already Exist';
				}else{
					if($profile_image!=""){
						$imgPath ='images/users/temp/';
						$img =$profile_image;
						$imgName = time(). ".jpg";
						$imageFormat = array('data:image/jpeg;base64', 'data:image/png;base64', 'data:image/jpg;base64', 'data:image/gif;base64');
						$img = str_replace($imageFormat, '', $img);
						$data = base64_decode($img);
						$image = @imagecreatefromstring($data);
						if ($image !== false) {
							$uploadPath = $imgPath . $imgName;
							imagejpeg($image, $uploadPath, 100);
							imagedestroy($image);
						} else {
							$responseArr['response'] ='An error occurred2';
						}
						if (isset($uploadPath)) {
							$filename = base_url($uploadPath);
							list($width, $height) = getimagesize($filename);
							$newwidth = 100;
							$newheight = 100;
							$sourceImage = @imagecreatefromjpeg($filename);
							imagecopyresized($thumbImage, $sourceImage, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
						}else {
							$responseArr['response'] ='Invalid File';
						}
						$new_name = $imgName;
						@copy("./images/users/temp/".$imgName, './images/users/'.$new_name);
						@copy("./images/users/temp/".$imgName, './images/users/thumb/'.$new_name);
						$this->imageResizeWithSpace(210, 210, $new_name, './images/users/thumb/');
						$this->imageResizeWithSpace(600, 600, $new_name, './images/users/');
						$dir =getcwd().DIRECTORY_SEPARATOR ."images".DIRECTORY_SEPARATOR ."users".DIRECTORY_SEPARATOR."temp".DIRECTORY_SEPARATOR ;
						$interval = strtotime('-24 hours');
						foreach (glob($dir."*.*") as $file){
							if (filemtime($file) <= $interval ){
								if(!is_dir($file) ){
									unlink($file);
								}
							}
						}
						$thumbnail = $imgName;
						}else{
							$thumbnail='';
						}
						$dataArr = array(
													'loginUserType'=>'twitter',
													'full_name'=>$first_name,
													'user_name'=>$chckUserName,
													'thumbnail'=>$thumbnail,
													'email'=>$email,
													'password'=>md5($passwordstr),
													'status'=>'Active',
													'is_verified'=>'No',
										);
						$this->db->insert(USERS,$dataArr);
						$this->db->select('*');
						$this->db->from(USERS);
						$this->db->where('email',$email);
						$checkUser = $this->db->get();
						$UDID='';
						 if($_POST['deviceToken']!=""){
							$device_type='ios';
							$UDID = $_POST['deviceToken'];
						}
						if($UDID!=""){
							$this->mobile_model->insertupdatePushKey($checkUser->row()->id,$UDID,'user',$device_type);
						}
						$userDetails=$checkUser;
						$this->send_confirm_mail($userDetails);
						$userInfoDetails = $this->mobile_model->get_all_details(USERS,array('email'=>$email));
						if($userInfoDetails->row()->thumbnail!=""){
							$user_image=base_url().'images/users/'.$userInfoDetails->row()->thumbnail;
						}else{
							$user_image=base_url().'images/users/user-thumb1.png';
						}
						$returnArr['status'] = '1';
						$returnArr['response'] = 'Success';
						$returnArr['userImage'] = $user_image;
						$returnArr['userId'] = (string)$userInfoDetails->row()->id;
						$returnArr['userName'] = $userInfoDetails->row()->user_name;
						$returnArr['email'] = $userInfoDetails->row()->email;
						$returnArr['userFullname'] = utf8_encode($userInfoDetails->row()->full_name);
						$returnArr['userGroup'] = $userInfoDetails->row()->group;
						$returnArr['userType'] = "Normal";
						if($userInfoDetails->row()->store_payment == "Paid"){
							$returnArr['userType'] = "Upgraded";
						}
					}
			}else{
				$returnArr['response'] = 'Invalid Input';
			}
		}
		$json_encode = json_encode($returnArr);
		echo $json_encode;
	}

    /*
    !! >>>>>>>>>>>> User Registration  <<<<<<<<<<<<  !!
    */
    public function userRegister(){
		$returnArr['status'] = '0';
		$returnArr['message'] = 'failure';
		$fullnamestr = $_POST['fullname'];
		//$lastnamestr = $_POST['lastname'];
		$emailstr = $_POST['email'];
		$passwordstr = $_POST['password'];
		$usernamestr1 = $_POST['username'];
		$usernamestr = stripslashes($usernamestr1);

		$this->db->select('*');
		$this->db->from(USERS);
		$this->db->where('user_name',$usernamestr);
		$userNameDetails = $this->db->get();

		$this->db->select('*');
		$this->db->from(USERS);
		$this->db->where('email',$emailstr);
		$userEmailDetails = $this->db->get();

		if($fullnamestr=='' ||  $emailstr=='' || $passwordstr=='' || $usernamestr==''){
			$returnArr['message'] = 'Failure';
		}else if($userNameDetails->num_rows()==1){
			$returnArr['message'] = 'Username Already Exist';
		}else if($userEmailDetails->num_rows()==1){
			$returnArr['message'] = 'Email Address Already Exist';
		}else{
			$dataArr = array('loginUserType'=>'mobile','full_name'=>$fullnamestr,'user_name'=>$usernamestr,'email'=>$emailstr,'password'=>md5($passwordstr),'status'=>'Active','is_verified'=>'No','commision'=>$this->config->item('product_commission'));

			$this->db->insert(USERS,$dataArr);

			$this->db->select('*');
			$this->db->from(USERS);
			$this->db->where('email',$emailstr);
			$checkUser = $this->db->get();


			/*push update*/
			$UDID='';
			if($_POST['uu_id']!=""){
				$device_type='android';
				$UDID = $_POST['uu_id'];
			}else if($_POST['deviceToken']!=""){
				$device_type='ios';
				$UDID = $_POST['deviceToken'];
				$deviceID = $_POST['deviceID'];
			}
			if($UDID!=""){
				$this->mobile_model->insertupdatePushKey($checkUser->row()->id,$UDID,'user',$device_type);
			}
			/*End*/

		//	$this->mobile_model->update_details(SHOPPING_CART,array("user_id"=>$checkUser->row()->id),array("user_id"=>$this->data["commonId"]));
			$userDetails=$checkUser;
			$this->send_confirm_mail($userDetails);

			if($checkUser->row()->thumbnail!=""){
				$user_image=base_url().'images/users/'.$checkUser->row()->thumbnail;
			}else{
				$user_image=base_url().'images/users/user-thumb1.png';
			}
			/*Blog registration Starts*/
			// $this->load->library('curl');
			// $url = base_url().'wp_change_user_role.php';
			// $post_data = array (
			// 		"un" => $checkUser->row()->user_name,
			// 		"pd" => $passwordstr,
			// 		"em" => $emailstr
			// );
			// $output = $this->curl->simple_get($url, $post_data);
			/*Blog registration Ends*/

			$returnArr['status'] = '1';
			$returnArr['message'] = 'Success';
			$returnArr['user_image'] = $user_image;
			$returnArr['user_id'] = (string)$checkUser->row()->id;
			$returnArr['user_name'] = $checkUser->row()->user_name;
			$returnArr['email'] = $checkUser->row()->email;
			$returnArr['user_fullname'] = $checkUser->row()->full_name;
		}
		$json_encode = json_encode($returnArr);
		echo $json_encode;
	}

	/*
	 	!! >>>>>>>>>>>> Homepage <<<<<<<<<<<< !!
	*/
	public function homepage(){
		$returnArr['status_code'] = 0;
		$returnArr['response'] = '';
		$pg = intval($_POST['pg']);
		$UserId = "";
		$cartCount = "";
		if(!empty($_POST['UserId'])){
			$UserId = $_POST['UserId'];
			$UserProfileDetails = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
			$returnArr['UserGroup'] = $UserProfileDetails->row()->group;

			$this->db->select('a.*,b.product_name,b.seourl,b.image,b.id as prdid,b.price as orgprice');
			$this->db->from(SHOPPING_CART . ' as a');
			$this->db->join(PRODUCT . ' as b', 'b.id = a.product_id');
			$this->db->where('a.user_id = ' . $UserId);
			$cartMiniVal = $this->db->get();
			if ($cartMiniVal->num_rows() > 0) {
				foreach ($cartMiniVal->result() as $CartRow) {
					$cartMiniQty = $cartMiniQty + $CartRow->quantity;
				}
			}
			$giftMiniRes = $this->minicart_model->get_all_details(GIFTCARDS_TEMP, array('user_id' => $UserId));
			$SubcribeMiniRes = $this->minicart_model->get_all_details(FANCYYBOX_TEMP, array('user_id' => $UserId));
			$countMiniVal = $giftMiniRes->num_rows() + $cartMiniQty + $SubcribeMiniRes->num_rows();
			$cartCount = $countMiniVal;
		}
		$returnArr['cartCount'] = $cartCount;
		if ($pg!='') {
			$paginationVal = $pg * 10;
			$limitPaging = $paginationVal . ',10 ';
		} else {
		$limitPaging = '10';
		}
		$newPage = $pg + 1;
		$qry_str = '?pg=' . $newPage;
        $sellingProductDetails = $this->mobile_model->view_product_details(" where p.status='Publish' and ((p.quantity > 0 and p.product_type='physical') or p.product_type='digital' ) order by p.created desc limit ". $limitPaging);

        $affiliateProductDetails = $this->mobile_model->view_notsell_product_details(" where p.status='Publish' and u.status='Active' order by p.created desc limit ". $limitPaging);
		$productDetails =  $this->data['productDetails'] = $this->mobile_model->get_sorted_array_mobile($sellingProductDetails, $affiliateProductDetails, 'created', 'desc');
		$prodArr=array();
		if(count($productDetails) > 0){
		//	print_r($productDetails);die;
			foreach($productDetails as $prodRow){
				$likedStatus = "No";
				if($UserId !=""){
					$cond = "select user_id from ".PRODUCT_LIKES." where product_id=".$prodRow['seller_product_id']." and user_id=".$UserId;
					$likedUsersList = $this->mobile_model->ExecuteQuery($cond);
					if($likedUsersList->num_rows() > 0){
						$likedStatus = "Yes";
					}
				}
				$imgArr=@explode(',',$prodRow['image']);
				$productImage = ($imgArr[0]=='') ? '' : base_url().'images/product/'.$imgArr[0];
				$salePrice = "";
				if($prodRow['sale_price'] != ""){
					$salePrice = $prodRow['sale_price'];
				}
				if(!empty($prodRow['id'])){
				   $userproducturl =base_url().'things/'.$prodRow['id'].'/'.$prodRow['product_name'];

				}else{
					$userproducturl ="";
				}
				$prodArr[]=array(
										'id'=>$prodRow['id'],
										'seller_product_id'=>$prodRow['seller_product_id'],
										'product_name'=>$prodRow['product_name'],
										'productUserId' => $prodRow['user_id'],
										'image'=>$productImage,
										'sale_price'=>$salePrice,
										'currencySymbol' => $this->config->item('currency_country_code'),
										'user_name'=>$prodRow['user_name'],
										'likes'=>$prodRow['likes'],
										'liked' => $likedStatus,
										'userproducturl'=> $userproducturl,
										'type' => $prodRow['type']
									);
				}
				$featured_products = $this->mobile_model->get_featured_products();
				if($featured_products->num_rows > 0){
						foreach($featured_products->result() as $featuredProResult){
							$likedStatus = "No";
							if($UserId !=""){
								$cond = "select user_id from ".PRODUCT_LIKES." where product_id=".$prodRow['seller_product_id']." and user_id=".$UserId;
								$likedUsersList = $this->mobile_model->ExecuteQuery($cond);
								if($likedUsersList->num_rows() > 0){
									$likedStatus = "Yes";
								}
							}
							$imgArr=@explode(',',$featuredProResult->image);
							$productImage = ($imgArr[0]=='') ? '' : base_url().'images/product/'.$imgArr[0];
							$featured_productsArr[] = array(
																				'id'=>$featuredProResult->id,
																				'seller_product_id'=>$featuredProResult->seller_product_id,
																				'image'=>$productImage,
																				'currencySymbol' => $this->config->item('currency_country_code'),
																				'product_name'=>$featuredProResult->product_name,
																				'productUserId'=>$featuredProResult->user_id,
																				'user_name'=>$featuredProResult->user_name,
																				'sale_price'=>$featuredProResult->sale_price,
																				'likes'=>$featuredProResult->likes,
																				'liked' => $likedStatus,
																				'type' => 'SellerProduct'
																		);
						}
					$returnArr['featuredProductsList']=$featured_productsArr;
				}else {
					$returnArr['featuredProductsList']=array();
				}
				$returnArr['status_code'] = 1;
				$returnArr['response'] = 'Success';
				$returnArr['productDetails']=$prodArr;
		}else{
			$returnArr['status_code'] = 0;
			$returnArr['response'] = 'No More Products Avaliable !!';
		}
		echo json_encode($returnArr);
	}

	/*
	 	!! >>>>>>>>>>>> Product Detail <<<<<<<<<<<< !!
	*/
	public function product() {
		$returnArr['status_code'] = 0;
		$returnArr['response'] = '';
		$thisUserId = "";
		$thisUserId = $_POST['UserId'];
		if ($thisUserId != "") {
			$thisUserProfileDetails = $this->mobile_model->get_all_details(USERS, array('id' => $thisUserId));
			$followingListArr = explode(',', $thisUserProfileDetails->row()->following);
			$ownProductsArr = explode(',', $thisUserProfileDetails->row()->own_products);
		}
		$pid=intval($_POST['pid']);
		if($pid == ''){
			$returnArr['response'] = 'Product Id Null !!';
		}else{
				$condition = "  where p.status='Publish' and u.group='Seller' and u.status='Active' and p.id='" . $pid . "' or p.status='Publish' and p.user_id=0 and p.id='" . $pid . "'";
				$this->data['productDetails'] = $this->mobile_model->view_product_details($condition);
				$thisProdUserId = $this->data['productDetails']->row()->user_id;
				$ThisUseraddedProd = $this->mobile_model->GetThisUserAddedProduct($thisProdUserId);
				$PrdAttrVal = $this->mobile_model->view_subproduct_details_join($pid);
				$attributesCountValue = $PrdAttrVal->num_rows();

				$productAttributes = array();
				foreach ($PrdAttrVal->result() as $attributes) {
					$productAttributes[$attributes->attr_type][]= array(
															'id' => $attributes->pid,
															'value' => $attributes->attr_name,
															'price' => $attributes->attr_price
										);
					if(!in_array($attributes->attr_type,$productAttributesList)){
						$productAttributesList[]=$attributes->attr_type;
					}
				}
				$productAttributesListarray=array();
				foreach($productAttributesList as $row){
				   $productAttributesListarray[]=array('values'=>$row);
				}
				$userType = "NormalUser";
				$storeBanner = base_url().'images/store/banner-dummy.jpg';
				$storeLogo = base_url().'images/store/dummy-logo.png';
				$storeTagline = "";
				$storeDescription = "";
				$storeName ="";
				if($this->data['productDetails']->row()->store_payment == "Paid"){
					$store_details = $this->mobile_model->get_store_details($thisProdUserId);
					$storeName = $store_details->row()->store_name;
					$userType = "UpgradedUser";
					if($store_details->row()->cover_image !=""){
						$storeBanner = base_url().'images/store/'.$store_details->row()->cover_image;
					}
					if($store_details->row()->logo_image !=""){
						$storeLogo =  base_url().'images/store/'.$store_details->row()->logo_image;
					}
					$storeTagline = $store_details->row()->tagline;
					$storeDescription = $store_details->row()->description;
				}
				$imgArr=@explode(',',$this->data['productDetails']->row()->image);
				$images = array();
				// if($imgArr[0] != ""){
				// 	$images = base_url().'images/product/'.$imgArr[0];
				// }
				foreach($imgArr as $img){
					if($img!="") $images[]=base_url().'images/product/'.$img;
				}
				$userImage = base_url().'images/users/user-thumb1.png';
				if($this->data['productDetails']->row()->thumbnail != ""){
					$userImage = base_url().'images/users/'.$this->data['productDetails']->row()->thumbnail;
				}
				$normalUserUrl ='';
				if($userType == 'NormalUser' ){
					$normalUserUrl = 'http://192.168.1.251:8081/product-working/fancyclone-v3/json/user/seller-timeline?username='.$this->data['productDetails']->row()->user_name;
				}
				$attributeMust = 'No';
				if($this->data['productDetails']->row()->attribute_must !='' && $this->data['productDetails']->row()->attribute_must != '0'){
					$attributeMust = 'Must';
				}
				$following = "";
				if ($thisUserId != "") {
					$following = 'No';
					if(in_array($this->data['productDetails']->row()->user_id,$followingListArr)){$following = 'Yes';}
					$owned = 0;
					if(in_array($this->data['productDetails']->row()->seller_product_id,$ownProductsArr)){$owned=1;}
				}
				$userName = $this->data['productDetails']->row()->user_name;
				if($this->data['productDetails']->row()->user_id == 0){
					$userName = "administrator";
				}

				$likedStatus = 0;
				if($thisUserId !=""){
					$cond = "select user_id from ".PRODUCT_LIKES." where product_id=".$this->data['productDetails']->row()->seller_product_id." and user_id=".$thisUserId;
					$likedUsersList = $this->mobile_model->ExecuteQuery($cond);
					if($likedUsersList->num_rows() > 0){
						$likedStatus = 1;
					}
				}

				$productUserProfileDetails = $this->mobile_model->get_all_details(USERS, array('id' => $this->data['productDetails']->row()->user_id));
				if($productUserProfileDetails->num_rows() > 0){
				   $userproducturl =base_url().'user/'.$productUserProfileDetails->row()->user_name.'/things/'.$this->data['productDetails']->row()->seller_product_id.'/'.$this->data['productDetails']->row()->product_name;

				}else{
					$userproducturl ="";
				}

				$productDetailsArr[] = array(
					'id'=>$this->data['productDetails']->row()->id,
					'seller_product_id'=>$this->data['productDetails']->row()->seller_product_id,
					'UserId' => $this->data['productDetails']->row()->user_id,
					'image'=>$images,
					'product_name'=>$this->data['productDetails']->row()->product_name,
					'description'=>strip_tags($this->data['productDetails']->row()->description),
					'shippingPolicies' =>strip_tags($this->data['productDetails']->row()->shipping_policies),
					'user_name'=> $userName,
					'price'=>$this->data['productDetails']->row()->price,
					'sale_price'=>$this->data['productDetails']->row()->sale_price,
					'shippingCost' =>  $this->data['productDetails']->row()->shipping_cost,
					'taxCost' =>  $this->data['productDetails']->row()->tax_cost,
					'likes'=>$this->data['productDetails']->row()->likes,
					'liked' => $likedStatus,
					'category_id'=>$this->data['productDetails']->row()->category_id,
					'attributesCountValue'=> $attributesCountValue,
					'AttributeMust' => $attributeMust,
					'productQuantity' => $this->data['productDetails']->row()->quantity,
					'productType' => $this->data['productDetails']->row()->product_type,
					'UserImage'=>$userImage,
					'userType' => $userType,
					'normalUserUrl' => $normalUserUrl,
					'storeBanner' => $storeBanner,
					'storeLogo' => $storeLogo,
					'storeTagline' => $storeTagline,
					'storeDescription' => $storeDescription,
					'storeName' => $storeName,
					'storeFollowing' => $following,
					'userproducturl' =>$userproducturl,
					'owned' => $owned
				);

                $relProductDetailsArr = array();
		        if ($this->data['productDetails']->num_rows() == 1) {

					if($thisUserId == $this->data['productDetails']->row()->user_id){
						$this->data['productComment'] = $this->product_model->view_product_comments_details('where c.product_id=' . $this->data['productDetails']->row()->seller_product_id . ' order by c.dateAdded desc');
						$this->data['productComment'] = $this->data['productComment']->result();
					}else{
						$productComment1 = $this->product_model->view_product_comments_details('where c.status="Active" and c.product_id=' . $this->data['productDetails']->row()->seller_product_id . ' order by c.dateAdded desc');
						$productComment2 = "";
						if ($thisUserId != "") {
							$productComment2 = $this->product_model->view_product_comments_details('where c.user_id='.$thisUserId.' and c.product_id=' . $this->data['productDetails']->row()->seller_product_id . ' order by c.dateAdded desc');
						}
						if($productComment2 != ""){
							$this->data['productComment'] = array_merge($productComment1->result(),$productComment2->result());
						}else{
							$this->data['productComment'] =$productComment1->result();
						}

					}
		            $catArr = explode(',', $this->data['productDetails']->row()->category_id);
		            if (count($catArr) > 0) {
		                foreach ($catArr as $cat) {
		                    if ($limit > 2)
		                        break;
		                    if ($cat != '') {
		                        $condition = ' where FIND_IN_SET("' . $cat . '",p.category_id) and p.quantity>0 and p.status="Publish" and u.group="Seller" and u.status="Active" and p.id != "' . $pid . '" or p.status="Publish" and p.quantity > 0 and p.user_id=0 and FIND_IN_SET("' . $cat . '",p.category_id) and p.id != "' . $pid . '"';
		                        $relatedProductDetails = $this->product_model->view_product_details($condition);
		                        if ($relatedProductDetails->num_rows() > 0) {
		                            foreach ($relatedProductDetails->result() as $relatedProduct) {
		                                if (!in_array($relatedProduct->id, $relatedArr)) {
		                                    array_push($relatedArr, $relatedProduct->id);
		                                    $relatedProdArr[] = $relatedProduct;
		                                    $limit++;
		                                }
		                            }
		                        }
		                    }
		                }
		            }
		        }
				 $this->data['relatedProductsArr'] = $relatedProdArr;

				if(count($relatedProdArr) > 0){
					 foreach($this->data['relatedProductsArr'] as $relatedProductRow){
					 	$imgArr=@explode(',',$relatedProductRow->image);
			 			$productImage = ($imgArr[0]=='') ? '' : base_url().'images/product/'.$imgArr[0];

						$userName = $relatedProductRow->user_name;
						if($relatedProductRow->user_id == "0"){
							$userName = "administrator";
						}

						$likedStatus = 0;
						if($thisUserId !=""){
							$cond = "select user_id from ".PRODUCT_LIKES." where product_id=".$relatedProductRow->seller_product_id." and user_id=".$thisUserId;
							$likedUsersList = $this->mobile_model->ExecuteQuery($cond);
							if($likedUsersList->num_rows() > 0){
								$likedStatus = 1;
							}
						}

						$relProductDetailsArr[] = array('id'=>$relatedProductRow->id,
											'seller_product_id'=>$relatedProductRow->seller_product_id,
											'image'=>$productImage,
											'description'=>strip_tags($relatedProductRow->description),
											'shippingPolicies'=>strip_tags($relatedProductRow->shipping_policies),
											'product_name'=>$relatedProductRow->product_name,
											'user_name'=> $userName,
											'price'=>$relatedProductRow->price,
											'sale_price'=>$relatedProductRow->sale_price,
											'likes' =>$relatedProductRow->likes,
											'liked' => $likedStatus,
											'category_id'=>$relatedProductRow->category_id,
											'url'=>base_url().'json/product?pid='.$relatedProductRow->id
											);
					}
				}
					$UserAddedProductsArr = array();
					foreach($ThisUseraddedProd->result() as $UserAddedProd){

							$userName = $UserAddedProd->user_name;
							if($UserAddedProd->user_id == "0"){
								$userName = "administrator";
							}

							$likedStatus = 0;
							if($thisUserId !=""){
								$cond = "select user_id from ".PRODUCT_LIKES." where product_id=".$UserAddedProd->seller_product_id." and user_id=".$thisUserId;
								$likedUsersList = $this->mobile_model->ExecuteQuery($cond);
								if($likedUsersList->num_rows() > 0){
									$likedStatus = 1;
								}
							}

						   $imgArr=@explode(',',$UserAddedProd->image);
   			 			   $productImage = ($imgArr[0]=='') ? '' : base_url().'images/product/'.$imgArr[0];
						   $UserAddedProductsArr[] = array('id'=>$UserAddedProd->id,
											   'seller_product_id'=>$UserAddedProd->seller_product_id,
											   'image'=>$productImage,
											   'product_name'=>$UserAddedProd->product_name,
											   'description'=>strip_tags($UserAddedProd->description),
											   'shippingPolicies'=>strip_tags($UserAddedProd->shipping_policies),
											   'user_name'=>$userName,
											   'price'=>$UserAddedProd->price,
											   'sale_price'=>$UserAddedProd->sale_price,
											   'likes'=>$UserAddedProd->likes,
											   'liked' => $likedStatus,
											   'category_id'=>$UserAddedProd->category_id,
											   'url'=>base_url().'json/product?pid='.$UserAddedProd->id
											   );
					   }
				$productComment = $this->data['productComment'];
				$ProductsCommentsCount = count($productComment);
		        $this->data['seller_product_details'] = $this->product_model->get_all_details(PRODUCT, array('user_id' => $this->data['productDetails']->row()->user_id, 'id !=' => $pid, 'status' => 'Publish'));
		        $this->data['seller_affiliate_products'] = $this->product_model->get_all_details(USER_PRODUCTS, array('user_id' => $this->data['productDetails']->row()->user_id));
				$this->data['product_feedback']=$product_feedback = $this->mobile_model->product_feedback_view($this->data['productDetails']->row()->user_id);
				$json_encode = array("productDetails" => $productDetailsArr,
									'commentsCount' => $ProductsCommentsCount,
									"ThisUseraddedProd"=>$UserAddedProductsArr,
									"productAttributesList" => $productAttributesListarray,
									"ProductAttribute"=>$productAttributes,
									"relatedProduct" => $relProductDetailsArr
								);
				$returnArr['status_code'] = 1;
				$returnArr['response'] = 'Success';
				$returnArr['productPage']=$json_encode;
		}
		echo json_encode($returnArr);
	}

	/*
	 	!! >>>>>>>>>>>>  Product Comments <<<<<<<<<<<< !!
	*/
	public function productComments() {
		$returnArr['status_code'] = 0;
		$returnArr['response'] = '';
		$thisUserId = $_POST['UserId'];
		$pid=intval($_POST['pid']);
		if($pid == ''){
			$returnArr['response'] = 'Product Id Null !!';
		}else{
			$condition = "  where p.status='Publish' and u.group='Seller' and u.status='Active' and p.id='" . $pid . "' or p.status='Publish' and p.user_id=0 and p.id='" . $pid . "'";
			$this->data['productDetails'] = $this->mobile_model->view_product_details($condition);
			if ($this->data['productDetails']->num_rows() == 1) {
				if($thisUserId == $this->data['productDetails']->row()->user_id){
					$this->data['productComment'] = $this->product_model->view_product_comments_details('where c.product_id=' . $this->data['productDetails']->row()->seller_product_id . ' order by c.dateAdded desc');
					$this->data['productComment'] = $this->data['productComment']->result();
				}else{
					$productComment1 = $this->product_model->view_product_comments_details('where c.status="Active" and c.product_id=' . $this->data['productDetails']->row()->seller_product_id . ' order by c.dateAdded desc');
					if($thisUserId != ""){
						$productComment2 = $this->product_model->view_product_comments_details('where c.user_id='.$thisUserId.' and c.product_id=' . $this->data['productDetails']->row()->seller_product_id . ' order by c.dateAdded desc');
						$this->data['productComment'] = array_merge($productComment1->result(),$productComment2->result());
					}else{
						$this->data['productComment'] = $productComment1->result();
					}
				}
			}
			$productComment = $this->data['productComment'];
			$ProductsComments = array();
			$ProductsCommentsCount = count($productComment);
			foreach($productComment as $comments){
			 $defaultuserImg = 'user-thumb1.png';
			 $userImage =base_url().'images/users/'.$defaultuserImg;
			 if(!empty($comments->thumbnail)){
				 $userImage =base_url().'images/users/'.$comments->thumbnail;
			 }
				$ProductsComments[]=array(
														'CommenterFullName'	  => $comments->full_name,
														'CommenterUserName'	=> $comments->user_name,
														'CommenterImage' => $userImage,
														'comments'	=> $comments->comments,
														'CommenterEmail' =>	$comments->email,
														'commentId'	=> $comments->id,
														'status' =>	$comments->status,
														'CommenterId'=>	$comments->CUID,
														'date' => isset($comments->dateAdded)? $comments->dateAdded : ""
													);
		   }
		  $returnArr['status_code'] = 1;
	      $returnArr['response'] = 'Success';
		  $returnArr['ProductsComments'] = array_reverse($ProductsComments);;
	   }
	   echo json_encode($returnArr);
	}

	/*
		!! >>>>>>>>>>>>>> RECENT ACTIVITES OF USER <<<<<<<<<<<<<< !!
	*/

	public function seller_timeline() {
		$returnArr['status'] = "0";
		$returnArr['response'] = "";
		$thisUserId = $_POST['UserId'];
		$username = $_POST['username'];
		$username = trim($username);
		if($username == ""){
			$returnArr['response'] = "Some parameters are missing !!";
		}else{
			if($thisUserId != ""){
				$thisUserProfileDetails = $this->mobile_model->get_all_details(USERS, array('id' => $thisUserId));
				$followingListArr = explode(',', $thisUserProfileDetails->row()->following);
			}
			$this->data['userProfileDetails'] = $this->mobile_model->get_all_details(USERS, array('user_name' => $username, 'status' => 'Active'));

			/* !! >>>>>>>>>>>>>>>> USER  WANTS PRODUCTS<<<<<<<<<<<<<<  !! */
				$wantList = $this->mobile_model->get_all_details(WANTS_DETAILS, array('user_id' => $this->data['userProfileDetails']->row()->id));
				$WantsProductDetail =array();
				if($wantList->num_rows() > 0 && $wantList->row()->product_id != ""){
					$wantProductDetails = $this->mobile_model->get_wants_product($wantList);
					$notSellProducts    = $this->mobile_model->get_notsell_wants_product($wantList);
					$WantProductDetail= $this->mobile_model->get_sorted_array_mobile($wantProductDetails, $notSellProducts, 'created', 'desc');
					foreach($WantProductDetail as $wantsProduct){
						$likedStatus = 0;
						if($thisUserId != ""){
							$cond = "select user_id from ".PRODUCT_LIKES." where product_id=".$wantsProduct['seller_product_id']." and user_id=".$thisUserId;
							$likedUsersList = $this->mobile_model->ExecuteQuery($cond);
							if($likedUsersList->num_rows() > 0){
								$likedStatus = 1;
							}
						}
						$saleprice = "";
						if($wantsProduct['sale_price'] != ""){
							$saleprice =$wantsProduct['sale_price'];
						}
								$WantsProductDetail[]=array(
									'id'=>$wantsProduct['id'],
									'sellerProductId'=>$wantsProduct['seller_product_id'],
									'productName'=>$wantsProduct['product_name'],
									'salePrice'=>$saleprice,
									'userName'=>$wantsProduct['user_name'],
									'UserId'=>$wantsProduct['user_id'],
									'likes'=>$wantsProduct['likes'],
									'liked' => $likedStatus,
									'image'=>base_url().'images/product/'.str_replace(',','',$wantsProduct['image']),
									'type' => $wantsProduct['type'],
									//'price'=>$wantsProduct['price'],
									//'full_name'=>$wantsProduct['full_name'],
									//'description'=>strip_tags($wantsProduct['description']),
									//'shippingPolicies'=>strip_tags($wantsProduct['shipping_policies']),
									//'productUrl'=>$wantsProduct['url'].'&UserId='.$thisUserId,
									//'shareUrl' => $wantsProduct['shareUrl'].'?ref='.$thisUserProfileDetails->row()->user_name,
									//'seourl'=>$wantsProduct['seourl'],
								);
					}
				}
				//print_r($WantsProductDetail);die;
				/* !! >>>>>>>>>>>>>>>> USER  WANTS PRODUCTS<<<<<<<<<<<<<<  !! */
				if($this->data['userProfileDetails']->num_rows()>0){
					$follow = $this->mobile_model->view_follow_list($this->data['userProfileDetails']->row()->id);
					$this->data['recentActivityDetails'] = $this->mobile_model->get_activity_details($this->data['userProfileDetails']->row()->id);
				}else{
					$follow = array();
					$this->data['recentActivityDetails'] = array();
				}
				$userProfileDetails = array();
				if($this->data['userProfileDetails']->num_rows()>0){
				//echo "<pre>";print_r($this->data['userProfileDetails']->result());die;
				foreach($this->data['userProfileDetails']->result() as $user){
					$fb_link = $user->facebook;
					if (substr($fb_link, 0,4)!='http') $fb_link='http://'.$fb_link;
					$tw_link = $user->twitter;
					if (substr($tw_link, 0,4)!='http') $tw_link='http://'.$tw_link;
					$go_link = $user->google;
					if (substr($go_link, 0,4)!='http') $go_link='http://'.$go_link;
					$userThumbnail = base_url().'images/users/user-thumb1.png';
					if($user->thumbnail !='') $userThumbnail = base_url().'images/users/'.$user->thumbnail;
						$following = 'No';
					if($thisUserId != ""){
						if(in_array($user->id,$followingListArr)){
							$following = 'Yes';
						}
					}
					$userProfileDetails = array(
													'id'=> $user->id,
													'userName'=> $user->user_name,
													'fullName'=> $user->full_name,
													'userGroup'=> $user->group,
													'likesCount'=> $user->likes,
													'productsCount'=> $user->products,
													'listsCount'=> $user->lists,
													'email'=> $user->email,
													'wantCount'=> strval(count($WantProductDetail)),
													'ownCount'=> $user->own_count,
													'userThumbnail'=> $userThumbnail,
													'webUrl'=> $user->web_url,
													'location'=> $user->location,
													'about'=> $user->about,
													'facebook'=> $fb_link,
													'twitter'=> $tw_link,
													'google'=> $go_link,
													//'verifiedUser'=> $user->is_verified,
													'group'=> $user->group,
													'followersCount'=> $user->followers_count,
													'followingCount'=> $user->following_count,
													'followCount' => strval($follow->num_rows()),
													'following' => $following,
													'storePayment'=> $user->store_payment,
													'storeStatus' => $user->request_status
												);
				}
				}

				/* !! >>>>>>>>>>>>>>>> USER RECENT ACTIVITY<<<<<<<<<<<<<<  !! */

				$userReacentActitvity = array();
				if($this->lang->line('date_year') != ''){
				   $date_year = $this->lang->line('date_year');
			   }else{
				   $date_year = "Year";
			   }
				if($this->lang->line('date_years') != ''){
				   $date_years = $this->lang->line('date_years');
			   }else{
				   $date_years = "Years";
			   }
				if($this->lang->line('date_month') != ''){
				   $date_month = $this->lang->line('date_month');
			   }else{
				   $date_month = "Month";
			   }
				if($this->lang->line('date_months') != ''){
				   $date_months = $this->lang->line('date_months');
			   }else{
				   $date_months = "Months";
			   }
				if($this->lang->line('date_week') != ''){
				   $date_week = $this->lang->line('date_week');
			   }else{
				   $date_week = "Week";
			   }
				if($this->lang->line('date_weeks') != ''){
				   $date_weeks = $this->lang->line('date_weeks');
			   }else{
				   $date_weeks = "Weeks";
			   }
				if($this->lang->line('date_day') != ''){
				   $date_day = $this->lang->line('date_day');
			   }else{
				   $date_day = "Day";
			   }
				if($this->lang->line('date_days') != ''){
					$date_days = $this->lang->line('date_days');
				}else{
					$date_days = "Days";
				}
				if($this->lang->line('date_hour') != ''){
				   $date_hour = $this->lang->line('date_hour');
			   }else{
				   $date_hour = "Hour";
			   }
				if($this->lang->line('date_hours') != ''){
				   $date_hours = $this->lang->line('date_hours');
			   }else{
				   $date_hours = "Hours";
			   }
				if($this->lang->line('date_minute') != ''){
				   $date_minute = $this->lang->line('date_minute');
			   }else{
				   $date_minute = "Minute";
			   }
				if($this->lang->line('date_minutes') != ''){
				   $date_minutes = $this->lang->line('date_minutes');
			   }else{
				   $date_minutes = "Minutes";
			   }
				if($this->lang->line('date_second') != ''){
				   $date_second = $this->lang->line('date_second');
			   }else{
				   $date_second = "Second";
			   }
				if($this->lang->line('date_seconds') != ''){
				   $date_seconds = $this->lang->line('date_seconds');
			   }else{
				   $date_seconds = "Seconds";
			   }
				if($this->lang->line('ago') != ''){
				   $date_ago = $this->lang->line('ago');
			   }else{
				   $date_ago = "ago";
			   }

				$date_lg_arr = array($date_years, $date_year,$date_months,$date_month,$date_weeks,$date_week,$date_days,$date_day,$date_hours,$date_hour,$date_minutes,$date_minute,$date_seconds,$date_second,$date_ago);
				$date_txt_arr = array( "Years", "Year", "Months", "Month","Weeks", "Week","Days","Day","Hours", "Hour","Minutes", "Minute", "Seconds", "Second", "ago");
				$userReacentActitvity = array();
				if(count($this->data['recentActivityDetails'])>0 && $this->data['recentActivityDetails']->num_rows()>0){
					foreach($this->data['recentActivityDetails']->result() as $userActivities){
						$activityTime = strtotime($userActivities->activity_time);
						$actTime = timespan($activityTime).' ago';
						$actTime = str_replace($date_txt_arr, $date_lg_arr, $actTime);
						$thumbnail = '';
						if($userActivities->thumbnail !=''){
							$thumbnail = base_url().'images/users/'.$userActivities->thumbnail;
						}
						$productID = "";
						$productName = "";
						$fullName = "";
						$userName = "";
						$productImage = "";
						if($userActivities->full_name != ""){$fullName = $userActivities->full_name;}
						if($userActivities->user_name != ""){
							$userName = $userActivities->user_name;
							$thumbnail = base_url().'images/users/user-thumb1.png';
							if($userActivities->thumbnail !=''){
								$thumbnail = base_url().'images/users/'.$userActivities->thumbnail;

							}
						}
						if($userActivities->productID != ""){
							$productID = $userActivities->productID;
							if($userActivities->product_name !=""){
								$productName= $userActivities->product_name;
								$productImage =base_url().'images/product/dummyProductImage.jpg';
								if($userActivities->image !='') $productImage = base_url().'images/product/'.$userActivities->image;
							}else{
								$productName= $userActivities->user_product_name;
								$productImage =base_url().'images/product/dummyProductImage.jpg';
								if($userActivities->image !='') $productImage = base_url().'images/product/'.$userActivities->user_product_image;
							}
						}
						$userReacentActitvity[] = array(
																'activityName'=>  $userActivities->activity_name,
																'activityId' => $userActivities->activity_id,
																'productID' => $productID,
																'productName' => $productName,
																'productImage' => $productImage,
																'time' => $actTime,
																'fullName' => $fullName,
																'userName' => $userName,
																'thumnail' => $thumbnail
															);
					}
				}
				/* !! >>>>>>>>>>>>>>>> USER ADDED PRODUCTS<<<<<<<<<<<<<<  !! */
				if($this->data['userProfileDetails']->num_rows()>0){
					$this->data['addedProductDetails']=$addedProductDetails = $this->product_model->view_product_details(' where p.user_id=' .$this->data['userProfileDetails']->row()->id . ' and p.status="Publish"');
					$this->data['notSellProducts'] = $affiliateProductDetails =  $this->product_model->view_notsell_product_details(' where p.user_id=' . $this->data['userProfileDetails']->row()->id . ' and p.status="Publish"');
				}else{
					$this->data['notSellProducts'] = $affiliateProductDetails =  array();
					$this->data['addedProductDetails']=$addedProductDetails = array();

				}
				$productDetails =  $this->data['productDetails'] = $this->mobile_model->get_sorted_array_mobile($addedProductDetails, $affiliateProductDetails, 'created', 'desc');
				$userAddedProducts = array();
				if(count($productDetails)>0){
				foreach($productDetails as $addedProduct){
					$likedStatus = "0";
					if($thisUserId != ""){
						$cond = "select user_id from ".PRODUCT_LIKES." where product_id=".$addedProduct['seller_product_id']." and user_id=".$thisUserId;
						$likedUsersList = $this->mobile_model->ExecuteQuery($cond);
						if($likedUsersList->num_rows() > 0){
							$likedStatus = "1";
						}
					}
					$imgArr=@explode(',',$addedProduct['image']);
					$images='';
					if($imgArr[0] != ""){
						$images=base_url().'images/product/'.$imgArr[0];
					}
					$saleprice = "";
					if($addedProduct['sale_price'] != ""){
						$saleprice =$addedProduct['sale_price'];
					}
					$userAddedProducts[]=array(

															'id'=>$addedProduct['id'],
															'sellerProductId'=>$addedProduct['seller_product_id'],
															'productName'=>$addedProduct['product_name'],
															'image'=>$images,
															'userId' =>$addedProduct['user_id'],
															'salePrice'=> $saleprice,
															'liked' => $likedStatus,
															'userName'=>$addedProduct['user_name'],
															'userId'=>$addedProduct['user_id'],
															'likes'=>$addedProduct['likes'],
															'type' => $addedProduct['type'],
															//'category_id'=>$addedProduct['category_id'],
															//'seourl'=>$addedProduct['seourl'],
															//'price'=>$addedProduct['price'],
															//'productUrl'=>$addedProduct['url'].'&UserId='.$thisUserId,
															//'shareUrl' => $addedProduct['shareUrl'].'?ref='.$thisUserProfileDetails->row()->user_name,
															//'description'=>strip_tags($addedProduct['description']),
															//'shippingPolicies'=>strip_tags($addedProduct['shipping_policies']),
															//'full_name'=>$addedProduct['full_name'],
															//'UserTimeline' => 'http://192.168.1.251:8081/product-working/fancyclone-v3/json/user/seller-timeline?username='.$addedProduct['user_name']
															);
					}
					}

				/* !! >>>>>>>>>>>>>>>> USER LIKED PRODUCTS<<<<<<<<<<<<<<  !! */
				$this->data['listDetails'] = $this->mobile_model->get_all_details(LISTS_DETAILS,array('user_id'=>$this->data['userProfileDetails']->row()->id));
				$listDetailsArr = array();
				if($this->data['listDetails']->num_rows()>0){
				foreach($this->data['listDetails']->result() as $resultArr){
					if ($resultArr->product_id != ''){
						$pidArr = array_filter(explode(',', $resultArr->product_id));
						$productDetails = '';
						if (count($pidArr)>0){
							foreach ($pidArr as $pidRow){
								if ($pidRow!=''){
									$productDetails = $this->product_model->get_all_details(PRODUCT,array('seller_product_id'=>$pidRow,'status'=>'Publish'));
									if ($productDetails->num_rows()==0){
										$productDetails = $this->product_model->get_all_details(USER_PRODUCTS,array('seller_product_id'=>$pidRow,'status'=>'Publish'));
									}
									if ($productDetails->num_rows()==1)break;
								}
							}
						}
					$listUrl = base_url().'json/user/lists?lid='.$resultArr->id.'&uname='.$username.'&UserId='.$thisUserId;
					$LImage = str_replace(","," ",$productDetails->row()->image);
					$listImage = base_url().'images/product/'.$LImage;
					$listDetailsArr[] = array('id'=>$resultArr->id,
													'name'=>$resultArr->name,
													'product_id'=>$resultArr->product_id,
													'user_id'=>$resultArr->user_id,
													'product_count'=>$resultArr->product_count,
													'followers_count'=>$resultArr->followers_count,
													'TotalProductsCount' => strval(count(array_filter(explode(',',$resultArr->product_id)))),
													'listImage' => $listImage,
													'listUrl' => $listUrl
												);
					}
				}
				}


			/* !! >>>>>>>>>>>>>>>> USER  FOLLOWS<<<<<<<<<<<<<<  !! */
					$userFollows = array();
					if(count($follow)>0){
					foreach($follow->result() as $userFollow){
							$userFollows[] = array(
																'id' => $userFollow->id,
																'name' => $userFollow->name,
																'UserId' =>$userFollow->user_id,
																'productId' => $userFollow->product_id,
																'followers' => $userFollow->followers,
																'banner' => $userFollow->banner,
																'categoryId' => $userFollow->category_id,
																'contributors' => $userFollow->contributors,
																'contributorsInvited' => $userFollow->contributors_invited,
																'productCount' => $userFollow->product_count,
																'followersCount' => $userFollow->followers_count,
																'pname' => $userFollow->pname,
															);
					}
					}


				/* !! >>>>>>>>>>>>>>>> USER  OWN PRODUCTS<<<<<<<<<<<<<<  !! */
						$productIdsArr = array_filter(explode(',', $this->data['userProfileDetails']->row()->own_products));
		                $productIds = '';
		                if (count($productIdsArr) > 0) {
		                    foreach ($productIdsArr as $pidRow) {
		                        if ($pidRow != '') {
		                            $productIds .= $pidRow . ',';
		                        }
		                    }
		                    $productIds = substr($productIds, 0, -1);
		                }
		                if ($productIds != '') {
		                    $this->data['ownsProductDetails']=$ownsProductDetails= $this->product_model->view_product_details(' where p.seller_product_id in (' . $productIds . ') and p.status="Publish"');
		                    $this->data['notSellProducts'] =$notSellProducts= $this->product_model->view_notsell_product_details(' where p.seller_product_id in (' . $productIds . ') and p.status="Publish"');
		                } else {
		                    $this->data['addedProductDetails'] = '';
		                    $this->data['notSellProducts'] = '';
		                }
						$ownsProductDetail= $this->mobile_model->get_sorted_array_mobile($ownsProductDetails, $notSellProducts, 'created', 'desc');
						$ownsProducts = array();
						if(count($ownsProductDetail)>0){
						foreach($ownsProductDetail as $ownsProduct){
							$likedStatus = "0";
							if($thisUserId != ""){
								$cond = "select user_id from ".PRODUCT_LIKES." where product_id=".$ownsProduct['seller_product_id']." and user_id=".$thisUserId;
								$likedUsersList = $this->mobile_model->ExecuteQuery($cond);
								if($likedUsersList->num_rows() > 0){
									$likedStatus = "1";
								}
							}
							$saleprice = "";
							if($ownsProduct['sale_price'] != ""){
								$saleprice =$ownsProduct['sale_price'];
							}
									$ownsProducts[]=array(
										'id'=>$ownsProduct['id'],
										'sellerProductId'=>$ownsProduct['seller_product_id'],
										'productName'=>$ownsProduct['product_name'],
										'image'=>base_url().'images/product/'.str_replace(',','',$ownsProduct['image']),
										'UserId' =>$ownsProduct['user_id'],
										'userName'=>$ownsProduct['user_name'],
										'salePrice'=>$saleprice,
										'userId'=>$ownsProduct['user_id'],
										'likes'=>$ownsProduct['likes'],
										'liked' => $likedStatus,
										'type' => $ownsProduct['type'],
										//'seourl'=>$ownsProduct['seourl'],
										//'price'=>$ownsProduct['price'],
										//'productUrl1'=> $ownsProduct['url'].'&UserId='.$thisUserId,
										//'shareUrl' => $ownsProduct['shareUrl'].'?ref='.$thisUserProfileDetails->row()->user_name,
										//'full_name'=>$ownsProduct['full_name'],
										//'description'=>strip_tags($ownsProduct['description']),
										//'shippingPolicies'=>strip_tags($ownsProduct['shipping_policies']),
										//'productUrl'=>$ownsProduct['url'],
										//'UserTimeline' => 'http://192.168.1.251:8081/product-working/fancyclone-v3/json/user/seller-timeline?username='.$ownsProduct['user_name']
										);
						}
					}

					if($this->data['userProfileDetails']->num_rows()>0){
					$productLikeDetails= $this->mobile_model->get_like_details_fully($this->data['userProfileDetails']->row()->id);
					$userProductLikeDetails= $this->mobile_model->get_like_details_fully_user_products($this->data['userProfileDetails']->row()->id);
					}else{
						$productLikeDetails=array();
						$userProductLikeDetails=array();
					}
					$userLikeProduct= $this->mobile_model->get_sorted_array_mobile($productLikeDetails, $userProductLikeDetails, 'created', 'desc');
					$userLikedProducts = array();
					if(count($userLikeProduct)>0){
					foreach($userLikeProduct as $userLike){
						$likedStatus = "0";
						if($thisUserId != ""){
							$cond = "select user_id from ".PRODUCT_LIKES." where product_id=".$userLike['seller_product_id']." and user_id=".$thisUserId;
							$likedUsersList = $this->mobile_model->ExecuteQuery($cond);
							if($likedUsersList->num_rows() > 0){
								$likedStatus = "1";
							}
						}
						$saleprice = "";
						if($userLike['sale_price'] != ""){
							$saleprice =$userLike['sale_price'];
						}
						$userLikedProducts[]=array(
							'id'=>$userLike['id'],
							'sellerProductId'=>$userLike['seller_product_id'],
							'productName'=>$userLike['product_name'],
							'image'=>base_url().'images/product/'.str_replace(',','',$userLike['image']),
							'salePrice'=>$saleprice,
							'UserId' =>$userLike['user_id'],
							'userName'=>$userLike['user_name'],
							'liked' => $likedStatus,
							'likes'=>$userLike['likes'],
							'type' => $userLike['type'],
							//'seourl'=>$userLike['seourl'],
							//'price'=>$userLike['price'],
							//'productUrl2'=>$userLike['url'].'&UserId='.$thisUserId,
							//'shareUrl' => $userLike['shareUrl'].'?ref='.$thisUserProfileDetails->row()->user_name,
							//'full_name'=>$userLike['full_name'],
							//'description'=>strip_tags($userLike['description']),
							//'shippingPolicies'=>strip_tags($userLike['shipping_policies']),
							//'productUrl'=>$userLike['url'],
							//'UserTimeline' => 'http://192.168.1.251:8081/product-working/fancyclone-v3/json/user/seller-timeline?username='.$userLike['user_name']
							);
					}
				}
			$returnArr['status'] = "1";
			$returnArr['response'] = "success";
			$returnArr['userProfileDetails'] = $userProfileDetails;  /* !! >>>>>>>>>>>>>>>> USER PROFILE<<<<<<<<<<<<<<  !! */
			$returnArr['userReacentActitvity'] = $userReacentActitvity; /* !! >>>>>>>>>>>>>>>> USER RECENT ACTIVITY <<<<<<<<<<<<<<  !! */
			$returnArr['userAddedProducts'] = $userAddedProducts;  /* !! >>>>>>>>>>>>>>>> USER ADDED PRODUCTS<<<<<<<<<<<<<<  !! */
			$returnArr['listDetails']=$listDetailsArr;  /* !! >>>>>>>>>>>>>>>> USER LIKED PRODUCTS<<<<<<<<<<<<<<  !! */
			$returnArr['userFollows']=$userFollows;  /* !! >>>>>>>>>>>>>>>> USER  FOLLOWS<<<<<<<<<<<<<<  !! */
			$returnArr['ownsProduct'] = $ownsProducts; 	/* !! >>>>>>>>>>>>>>>> USER  OWN PRODUCTS<<<<<<<<<<<<<<  !! */
			$returnArr['WantsProductDetail'] = $WantsProductDetail; /* !! >>>>>>>>>>>>>>>> USER  WANTS <<<<<<<<<<<<<<  !! */
			$returnArr['userLikedProducts'] = $userLikedProducts; /* !! >>>>>>>>>>>>>>>> USER  WANTS <<<<<<<<<<<<<<  !! */
		}
		echo json_encode($returnArr);
	}

	/*
	* !! >>>>>>>>>>>>> Seller Signup Form  <<<<<<<<<<<< !!
	*/
	public function sellerSignupForm() {
		$returnArr['status'] = '0';
        $returnArr['response'] = '';
		$returnArr['CountryLists']='';
		$UserId  =  $_POST['UserId'];
		if($UserId != ""){
			$CountryLists = array();
			$this->data['userProfileDetails'] = $this->mobile_model->get_all_details(USERS, array('id' => $UserId, 'status' => 'Active'));
	        if ($this->data['userProfileDetails']->row()->is_verified == 'No') {
	            if ($this->lang->line('cfm_mail_fst') != ''){
	                $lg_err_msg = $this->lang->line('cfm_mail_fst');
				}else{
	                $lg_err_msg = 'Please confirm your email first';
				}
				$returnArr['status'] = '0';
			    $returnArr['response'] = $lg_err_msg;
	        }else {
				    // if ($this->data['userProfileDetails']->row()->request_status == 'Not Requested'){
					// 	$countryList= $this->mobile_model->get_all_details(COUNTRY_LIST, array(), array(array('field' => 'name', 'type' => 'asc')));
					// 	$countryLists = array();
					// 	foreach($countryList->result() as $country){
					// 			$countryLists[] = array(
					// 												'categoryName' => $country->name,
					// 												'countryCode' => $country->country_code
					// 										);
					// 	}
					// 	$returnArr['status'] = '1';
					// 	$message = "Success";
					// 	if($this->lang->line('json_success') != ""){
					// 		$message = stripslashes($this->lang->line('json_success'));
					// 	}
					// 	$returnArr['response'] = $message;
					// 	$returnArr['CountryLists'] = $countryLists;
					// }

				if($this->data['userProfileDetails']->row()->request_status == 'Pending'){
					$lg_err_msg = 'Our team is evaluating your request. We will contact you shortly';
					$returnArr['status'] = '0';
					$returnArr['response'] = $lg_err_msg;
				}else if($this->data['userProfileDetails']->row()->request_status == 'Approved'){
					$lg_err_msg = 'Approved';
					$returnArr['status'] = '1';
					$returnArr['response'] = $lg_err_msg;
				}else if($this->data['userProfileDetails']->row()->request_status == 'Rejected'){
					$lg_err_msg = 'Your request has been rejected, Please contact Admin';
					$returnArr['status'] = '0';
					$returnArr['response'] = $lg_err_msg;
				}
	        }
		}else{
			$returnArr['status'] = '0';
	        $returnArr['response'] = 'Please Login';
		}
		echo json_encode($returnArr);
    }

	/*
	 	!! >>>>>>>>>>>> User Products <<<<<<<<<<<< !!
	*/
	public function userproduct(){
		$returnArr['status_code'] = 0;
		$returnArr['response'] = '';
		$uname = $_POST['uname'];
        $pid = $_POST['pid'];
		$loggedUserId = $_POST['UserId'];
		if($uname== "" && $pid ==""){
			$returnArr['response'] = 'Some Parameters are Missing !!';
		}else{
			$ownProductsArr = array();
			if($loggedUserId != ""){
				$thisUserProfileDetails = $this->mobile_model->get_all_details(USERS, array('id' => $loggedUserId));
				$ownProductsArr = explode(',', $thisUserProfileDetails->row()->own_products);
			}
			$productUserDetails = $this->mobile_model->get_all_details(USERS, array('user_name' => $uname));

	        $this->data['productDetails'] = $this->mobile_model->view_notsell_product_details(' where p.seller_product_id="' . $pid . '" and p.status="Publish"');
			$thisProdUserId = $this->data['productDetails']->row()->user_id;
			$ThisUseraddedProd = $this->mobile_model->GetThisUserAddedProduct($thisProdUserId);
				$imgArr=@explode(',',$this->data['productDetails']->row()->image);
				$images=array();
				foreach($imgArr as $img){
					if($img!="")$images[]=base_url().'images/product/'.$img;
				}
				$likedStatus = 0;
				$owned = 0;
				if($loggedUserId != ""){
					$cond = "select user_id from ".PRODUCT_LIKES." where product_id=".$this->data['productDetails']->row()->seller_product_id." and user_id=".$loggedUserId;
					$likedUsersList = $this->mobile_model->ExecuteQuery($cond);
					if($likedUsersList->num_rows() > 0){
						$likedStatus = 1;
					}
					if(in_array($this->data['productDetails']->row()->seller_product_id,$ownProductsArr)){$owned=1;}
				}
				$emailVerified = 0;
				if($productUserDetails->row()->is_verified == "Yes"){
					$emailVerified = 1;
				}
				$storeStatus = 0;
				$userRequestStatus = "";
				$storeMessage = "";
				if($productUserDetails->row()->request_status == "Approved"){
					$storeStatus =1;
					$userRequestStatus = "approved";
					$storeMessage = "Approved";
				}else if($productUserDetails->row()->request_status == "Not Requested"){
					$userRequestStatus = "notRequested";
					$storeMessage = "Please subscribe as seller.";
				}else if($productUserDetails->row()->request_status == "Pending"){
					$userRequestStatus = "pending";
					$storeMessage = "Our team is evaluating your request. We will contact you shortly";
				}else if($productUserDetails->row()->request_status == "Rejected"){
					$userRequestStatus = "rejected";
					$storeMessage = "Your request has been rejected, Please contact Admin";
				}
				$sellerSubscriptionDetails = array(
																'emailVerified' => $emailVerified,
																'storeStatus' => $storeStatus,
																'userRequestStatus' => $userRequestStatus,
																'storeMessage' => $storeMessage
															);

				if(!empty($this->data['productDetails']->row()->id)){
				   $userproducturl =base_url().'user/'.$this->data['productDetails']->row()->user_name.'/things/'.$this->data['productDetails']->row()->seller_product_id.'/'.$this->data['productDetails']->row()->product_name;

				}else{
					$userproducturl ="";
				}
				$productDetailsArr[] = array('id'=>$this->data['productDetails']->row()->id,
																'seller_product_id'=>$this->data['productDetails']->row()->seller_product_id,
																'image'=>$images,
																'product_name'=>$this->data['productDetails']->row()->product_name,
																'user_name'=>$this->data['productDetails']->row()->user_name,
																'webLink' => $this->data['productDetails']->row()->web_link,
																'likes'=>$this->data['productDetails']->row()->likes,
																'category_id'=>$this->data['productDetails']->row()->category_id,
																'liked' => $likedStatus,
																'user_id'=>$this->data['productDetails']->row()->userid,
																'userproducturl'=>$userproducturl,
																'owned' => $owned,
																);

	        if ($this->data['productDetails']->num_rows() == 1) {
	            $categoryArr = explode(',', $this->data['productDetails']->row()->category_id);
	            $catID = 0;
	            if (count($categoryArr) > 0) {
	                foreach ($categoryArr as $catRow) {
	                    if ($catRow != '') {
	                        $catID = $catRow;
	                        break;
	                    }
	                }
	            }
	            $this->data['relatedProductsArr'] = $this->mobile_model->get_products_by_category($catID);
					$relProductDetailsArr = array();
					if($this->data['relatedProductsArr']->num_rows() > 0){
						foreach($this->data['relatedProductsArr']->result() as $relatedProductRow){
							$likedStatus = 0;
							if($loggedUserId !=""){
								$cond = "select user_id from ".PRODUCT_LIKES." where product_id=".$relatedProductRow->seller_product_id." and user_id=".$loggedUserId;
								$likedUsersList = $this->mobile_model->ExecuteQuery($cond);
								if($likedUsersList->num_rows() > 0){
									$likedStatus = 1;
								}
							}
							$imgArr=@explode(',',$relatedProductRow->image);
							$productImage = ($imgArr[0]=='') ? '' : base_url().'images/product/'.$imgArr[0];
							$relProductDetailsArr[] = array(
																		'id'=>$relatedProductRow->id,
																		'seller_product_id'=>$relatedProductRow->seller_product_id,
																		'image'=>$productImage,
																		'product_name'=>$relatedProductRow->product_name,
																		'user_name'=>$relatedProductRow->user_name,
																		'sale_price'=>$relatedProductRow->sale_price,
																		'likes'=>$relatedProductRow->likes,
																		'liked'=>$likedStatus,
																		'productType' => 'Selling Product',
																);
					}
				}
				$UserAddedProductsArr = array();
				if($ThisUseraddedProd->num_rows() > 0){
					foreach($ThisUseraddedProd->result() as $UserAddedProd){
					   $imgArr=@explode(',',$UserAddedProd->image);
					   $productImage = ($imgArr[0]=='') ? '' : base_url().'images/product/'.$imgArr[0];
					   $likedStatus = 0;
	   					if($loggedUserId !=""){
	   						$cond = "select user_id from ".PRODUCT_LIKES." where product_id=".$UserAddedProd->seller_product_id." and user_id=".$loggedUserId;
	   						$likedUsersList = $this->mobile_model->ExecuteQuery($cond);
	   						if($likedUsersList->num_rows() > 0){
	   							$likedStatus = 1;
	   						}
	   					}
					   $UserAddedProductsArr[] = array('id'=>$UserAddedProd->id,
										   'seller_product_id'=>$UserAddedProd->seller_product_id,
										   'image'=>$productImage,
										   'product_name'=>$UserAddedProd->product_name,
										   'user_name'=>$UserAddedProd->user_name,
										   'sale_price'=>$UserAddedProd->sale_price,
										   'likes'=>$UserAddedProd->likes,
										   'liked'=>$likedStatus,
										   'productType' => 'Selling Product'
										   );
				   }
			   }
				$json_encode = array("productDetails" => $productDetailsArr,"ThisUseraddedProd"=>$UserAddedProductsArr,"relatedProduct" => $relProductDetailsArr, "sellerSubscriptionDetails" => $sellerSubscriptionDetails);
				$returnArr['status_code'] = 1;
				$returnArr['response'] = 'Success';
				$returnArr['productPage']=$json_encode;
	        } else {
					$json_encode = array("productDetails" => array(),"relatedProduct" => array());
					$returnArr['productPage']=$json_encode;
	    	}
		}
		echo json_encode($returnArr);
	}

	/*
	 	!! >>>>>>>>>>>> Product Comments <<<<<<<<<<<< !!
	*/
	public function insert_product_comment() {
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$uid = $this->input->post('user_id');
		if($uid == ''){
			$returnArr['response'] = 'UserId Null !!';
		}else{
			$comments = $this->input->post('comments');
			$product_id = $this->input->post('seller_product_id');
			if($comments == "" || $product_id == ""){
				$returnArr['response'] = 'Some Parameters Are Missing !!';
			}else{
				$datestring = "%Y-%m-%d %H:%i:%s";
				$time = time();
				$conditionArr = array(
													'comments' => $comments,
													'user_id' => $uid,
													'product_id' => $product_id,
													'status' => 'InActive',
													'dateAdded' => mdate($datestring, $time)
												);
				$this->order_model->simple_insert(PRODUCT_COMMENTS, $conditionArr);
				$cmtID = $this->order_model->get_last_insert_id();
				$datestring = "%Y-%m-%d %H:%i:%s";
				$time = time();
				$createdTime = mdate($datestring, $time);
				$actArr = array(
											'activity' => 'own-product-comment',
											'activity_id' => $product_id,
											'user_id' => $uid,
											//'activity_ip' => $this->input->ip_address(),
											'created' => $createdTime,
											'comment_id' => $cmtID
										);
				$this->order_model->simple_insert(NOTIFICATIONS, $actArr);
				$this->send_comment_noty_mail($cmtID, $product_id);
				$this->send_comment_noty_mail_to_admin($cmtID, $product_id);
				$returnArr['status'] = 1;
				$returnArr['response'] = 'Your Comment is Waiting For Approvel';
			}
		}
		echo json_encode($returnArr);
	}


	/*
	!! >>>>>>>>>>>> Add Product To Cart <<<<<<<<<<<< !!
	*/
	public function addProductsToCart() {
		$returnArr['status_code'] = 0;
		$returnArr['response'] = '';
		$excludeArr = array('addtocart', 'attr_color', 'totalQuantity','UserId');
		$dataArrVal = array();
		$UserId = $_POST['UserId'];
		$mqty = $this->input->post('totalQuantity');
		foreach ($this->input->post() as $key => $val) {
			if (!(in_array($key, $excludeArr))) {
				$dataArrVal[$key] = trim(addslashes($val));
			}
		}
		$datestring = date('Y-m-d H:i:s', now());
		$indTotal = ( $this->input->post('price') + $this->input->post('product_shipping_cost') + ($this->input->post('price') * 0.01 * $this->input->post('product_tax_cost')) ) * $this->input->post('quantity');
		$dataArry_data = array('created' => $datestring, 'user_id' => $UserId, 'indtotal' => $indTotal, 'total' => $indTotal);
		$dataArr = array_merge($dataArrVal, $dataArry_data);
		$condition = '';
		$this->data['productVal'] = $this->cart_model->get_all_details(SHOPPING_CART, array('user_id' => $UserId, 'product_id' => $this->input->post('product_id'), 'attribute_values' => $this->input->post('attribute_values')));
		$for_qty_check = $this->cart_model->get_all_details(SHOPPING_CART, array('user_id' => $UserId, 'product_id' => $this->input->post('product_id')));

		if ($for_qty_check->num_rows > 0) {
			$new_tot_qty = 0;
			foreach ($for_qty_check->result() as $for_qty_check_row) {
				$new_tot_qty += $for_qty_check_row->quantity;
			}
			$new_tot_qty += $this->input->post('quantity');
			if ($new_tot_qty <= $mqty) {
				if ($this->data['productVal']->num_rows > 0) {
					$newQty = $this->data['productVal']->row()->quantity + $this->input->post('quantity');
					$indTotal = ( $this->input->post('price') + $this->input->post('product_shipping_cost') + ($this->input->post('price') * 0.01 * $this->input->post('product_tax_cost')) ) * $newQty;
					$dataArr = array('quantity' => $newQty, 'indtotal' => $indTotal, 'total' => $indTotal);
					$condition = array('id' => $this->data['productVal']->row()->id);
					$this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
				} else {
					$this->cart_model->commonInsertUpdate(SHOPPING_CART, 'insert', $excludeArr, $dataArr, $condition);
				}
			} else {
				$cart_qty = $new_tot_qty - $this->input->post('quantity');
				//echo 'Error|' . $cart_qty;
				$returnArr['response'] = 'Error, Inavlid Quantity'.$cart_qty;
				die;
			}
		} else {
			$this->cart_model->commonInsertUpdate(SHOPPING_CART, 'insert', $excludeArr, $dataArr, $condition);
		}
		if ($this->input->post('UserId') != '') {
			$mini_cart_lg = array();
			if ($this->lang->line('items') != '')
				$mini_cart_lg['lg_items'] = stripslashes($this->lang->line('items'));
			else
				$mini_cart_lg['lg_items'] = "items";
			if ($this->lang->line('header_description') != '')
				$mini_cart_lg['lg_description'] = stripslashes($this->lang->line('header_description'));
			else
				$mini_cart_lg['lg_description'] = "Description";
			if ($this->lang->line('qty') != '')
				$mini_cart_lg['lg_qty'] = stripslashes($this->lang->line('qty'));
			else
				$mini_cart_lg['lg_qty'] = "Qty";
			if ($this->lang->line('giftcard_price') != '')
				$mini_cart_lg['lg_price'] = stripslashes($this->lang->line('giftcard_price'));
			else
				$mini_cart_lg['lg_price'] = "Price";
			if ($this->lang->line('order_sub_total') != '')
				$mini_cart_lg['lg_sub_tot'] = stripslashes($this->lang->line('order_sub_total'));
			else
				$mini_cart_lg['lg_sub_tot'] = "Order Sub Total";
			if ($this->lang->line('proceed_to_checkout') != '')
				$mini_cart_lg['lg_proceed'] = stripslashes($this->lang->line('proceed_to_checkout'));
			else
				$mini_cart_lg['lg_proceed'] = "Proceed to Checkout";

			/**  Mini cart Lg  **/
			//echo 'Success|' . $this->cart_model->mini_cart_view($UserId, $mini_cart_lg);
			$returnArr['status_code'] = 1;
			$returnArr['response'] = 'Successfully Added To Cart';
		}else {
			$returnArr['response'] = 'UserId Null';
		}
		echo json_encode($returnArr);
	}

	/*
	* !! >>>>>>>>>>>>> Product Liking Function  <<<<<<<<<<<< !!
	*/
	public function productLike() {
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$userId = trim($_POST['UserId']);
		$tid = trim($_POST['sellerProductId']);
		if($userId != ""){
			if($tid != ""){
				$this->data['userProfileDetails'] = $this->mobile_model->get_all_details(USERS, array('id' => $userId, 'status' => 'Active'));
				$checkProductLike = $this->mobile_model->get_all_details(PRODUCT_LIKES, array('product_id' => $tid, 'user_id' =>$userId));
				if ($checkProductLike->num_rows() == 0) {
					$productDetails = $this->mobile_model->get_all_details(PRODUCT, array('seller_product_id' => $tid));
					if ($productDetails->num_rows() == 0) {
						$productDetails = $this->mobile_model->get_all_details(USER_PRODUCTS, array('seller_product_id' => $tid));
						$productTable = USER_PRODUCTS;
					} else {
						$productTable = PRODUCT;
					}
					if ($productDetails->num_rows() == 1) {
						$likes = $productDetails->row()->likes;
						$dataArr = array('product_id' => $tid, 'user_id' => $userId);
						$this->mobile_model->simple_insert(PRODUCT_LIKES, $dataArr);
						$actArr = array(
							'activity_name' => 'fancy',
							'activity_id' => $tid,
							'user_id' => $userId,
						);
						$this->mobile_model->simple_insert(USER_ACTIVITY, $actArr);
						$datestring = "%Y-%m-%d %H:%i:%s";
						$time = time();
						$createdTime = mdate($datestring, $time);
						$actArr = array(
							'activity' => 'like',
							'activity_id' => $tid,
							'user_id' => $userId,
							'created' => $createdTime
						);
						$this->mobile_model->simple_insert(NOTIFICATIONS, $actArr);
						$likes++;
						$dataArr = array('likes' => $likes);
						$condition = array('seller_product_id' => $tid);
						$this->mobile_model->update_details($productTable, $dataArr, $condition);
						$totalUserLikes = $this->data['userProfileDetails']->row()->likes;
						$totalUserLikes++;
						$this->mobile_model->update_details(USERS, array('likes' => $totalUserLikes), array('id' =>$userId));
						$returnArr['status'] = 1;
						$message = "Success";
						if($this->lang->line('json_success') != ""){
							$message = stripslashes($this->lang->line('json_success'));
						}
						$returnArr['response'] = $message;
					} else {
						if ($this->lang->line('prod_not_avail') != ''){
							$returnArr['response'] = $this->lang->line('prod_not_avail');
						}else{
							$returnArr['response'] = 'Product not available';
						}
					}
				}else{
					$returnArr['response'] = 'You have already liked this product';
				}
			}else{
				$returnArr['response'] = 'SellerProductId Null';
			}
		}else{
			$returnArr['response'] = 'UserId Null';
		}
		echo json_encode($returnArr);
	}

	/*
	* !! >>>>>>>>>>>>> Product Unliking Function  <<<<<<<<<<<< !!
	*/
	public function productUnlike() {
	    $returnArr['status'] = 0;
		$returnArr['response'] = '';

		$userId = trim($_POST['UserId']);
		$tid = trim($_POST['sellerProductId']);

		if($userId != ""){
			if($tid != ""){
				$this->data['userProfileDetails'] = $this->mobile_model->get_all_details(USERS, array('id' => $userId, 'status' => 'Active'));
		        $checkProductLike = $this->mobile_model->get_all_details(PRODUCT_LIKES, array('product_id' => $tid, 'user_id' => $userId));
		        if ($checkProductLike->num_rows() == 1) {
		            $productDetails = $this->mobile_model->get_all_details(PRODUCT, array('seller_product_id' => $tid));
		            if ($productDetails->num_rows() == 0) {
		                $productDetails = $this->mobile_model->get_all_details(USER_PRODUCTS, array('seller_product_id' => $tid));
		                $productTable = USER_PRODUCTS;
		            } else {
		                $productTable = PRODUCT;
		            }
		            if ($productDetails->num_rows() == 1) {
		                $likes = $productDetails->row()->likes;
		                $conditionArr = array('product_id' => $tid, 'user_id' => $userId);
		                $this->mobile_model->commonDelete(PRODUCT_LIKES, $conditionArr);
		                $actArr = array(
		                    'activity_name' => 'unfancy',
		                    'activity_id' => $tid,
		                    'user_id' => $userId,
		                );
		                $this->mobile_model->simple_insert(USER_ACTIVITY, $actArr);
		                $likes--;
		                $dataArr = array('likes' => $likes);
		                $condition = array('seller_product_id' => $tid);
		                $this->mobile_model->update_details($productTable, $dataArr, $condition);
		                $totalUserLikes = $this->data['userProfileDetails']->row()->likes;
		                $totalUserLikes--;
		                $this->mobile_model->update_details(USERS, array('likes' => $totalUserLikes), array('id' => $userId));
		                $returnArr['status'] = 1;
						$message = "Success";
						if($this->lang->line('json_success') != ""){
							$message = stripslashes($this->lang->line('json_success'));
						}
						$returnArr['response'] = $message;
		            } else {
		                if ($this->lang->line('prod_not_avail') != ''){
		                    $returnArr['response'] = $this->lang->line('prod_not_avail');
						}else{
		                    $returnArr['response'] = 'Product not available';
						}
		            }
		        }else{
					$returnArr['response'] = 'You can\'t unlike a product without liking it';
				}
			}else{
				$returnArr['response'] = 'SellerProductId Null';
			}
		}else{
			$returnArr['response'] = 'UserId Null';
		}
    	echo json_encode($returnArr);
	}


	/*
	* !! >>>>>>>>>>>>> User Added Lists Details  <<<<<<<<<<<< !!
	*/
	public function ProductLitsDetails() {
	        $returnArr['status'] = 0;
			$returnArr['response'] = '';
	        $returnArr['listDetails'] = array();
	        $returnArr['wanted'] = 0;
	        $uniqueListNames = array();
            $tid= $_POST['sellerProductId'];
            $userId = $_POST['UserId'];
            $firstCatName = '';
            $firstCatDetails = '';
            $count = 1;
            $ListCount  =  array();
            $productDetails = $this->mobile_model->get_all_details(PRODUCT, array('seller_product_id' => $tid));
            if ($productDetails->num_rows() == 0) {
                $productDetails = $this->mobile_model->get_all_details(USER_PRODUCTS, array('seller_product_id' => $tid));
            }
            if ($productDetails->num_rows() == 1) {
                $productCatArr = explode(',', $productDetails->row()->category_id);
                if (count($productCatArr) > 0) {
                    $productCatNameArr = array();
                    foreach ($productCatArr as $productCatID) {
                        if ($productCatID != '') {
                            $productCatDetails = $this->mobile_model->get_all_details(CATEGORY, array('id' => $productCatID));
                            if ($productCatDetails->num_rows() == 1) {
                                if ($count == 1) {
                                    $firstCatName = $productCatDetails->row()->cat_name;
                                }
                                $listConditionArr = array('name' => $productCatDetails->row()->cat_name, 'user_id' => $userId);
                                $listCheck = $this->mobile_model->get_all_details(LISTS_DETAILS, $listConditionArr);
                                if ($count == 1) {
                                    $firstCatDetails = $listCheck;
                                }
                                if ($listCheck->num_rows() == 0) {
                                    $this->mobile_model->simple_insert(LISTS_DETAILS, $listConditionArr);
                                    $userDetails = $this->mobile_model->get_all_details(USERS, array('id' => $userId));
                                    $listCount = $userDetails->row()->lists;
                                    if ($listCount < 0 || $listCount == '') {
                                        $listCount = 0;
                                    }
                                    $listCount++;
                                    $this->mobile_model->update_details(USERS, array('lists' => $listCount), array('id' => $userId));
                                }
                                $count++;
                            }
                        }
                    }
					$checkListsArr = $this->mobile_model->get_list_details($tid, $userId);
		            if ($checkListsArr->num_rows() == 0) {
		                if ($firstCatName != '') {
		                    $listConditionArr = array('name' => $firstCatName, 'user_id' => $userId);
		                    if ($firstCatDetails == '' || $firstCatDetails->num_rows() == 0) {
		                        $dataArr = array('product_id' => $tid);
		                    } else {
		                        $productRowArr = explode(',', $firstCatDetails->row()->product_id);
		                        $productRowArr[] = $tid;
		                        $newProductRowArr = implode(',', $productRowArr);
		                        $dataArr = array('product_id' => $newProductRowArr);
		                    }
		                    $this->mobile_model->update_details(LISTS_DETAILS, $dataArr, $listConditionArr);
		                    $listCntDetails = $this->mobile_model->get_all_details(LISTS_DETAILS, $listConditionArr);
		                    if ($listCntDetails->num_rows() == 1) {
		                        array_push($uniqueListNames, $listCntDetails->row()->id);
		                        $ListCount[] = array(
		                        								'ListId'  =>  $listCntDetails->row()->id,
		                        								'Checked'  =>  1,
		                        								'ListName'  =>  $listCntDetails->row()->name
		                        								);
		                        $returnArr['listDetails'] =  $ListCount ;
		                    }
		                }
		            } else {
		                foreach ($checkListsArr->result() as $checkListsRow) {
		                    array_push($uniqueListNames, $checkListsRow->id);
	                        $ListCount[] = array(
	                        								'ListId'  =>  $checkListsRow->id,
	                        								'Checked'  =>  1,
	                        								'ListName'  =>  $checkListsRow->name
	                        								);
		                    $returnArr['listDetails'] =  $ListCount ;
		                }
		            }
		            $all_lists = $this->mobile_model->get_all_details(LISTS_DETAILS, array('user_id' => $userId));
		            if ($all_lists->num_rows() > 0) {
		                foreach ($all_lists->result() as $all_lists_row) {
		                    if (!in_array($all_lists_row->id, $uniqueListNames)) {
		                        $ListCount[] = array(
		                        								'ListId'  =>  $all_lists_row->id,
		                        								'Checked'  =>  0,
		                        								'ListName'  =>  $all_lists_row->name
		                        								);
		                        $returnArr['listDetails'] =  $ListCount ;
		                    }
		                }
		            }
		            $wantedProducts = $this->mobile_model->get_all_details(WANTS_DETAILS, array('user_id' => $userId));
		            if ($wantedProducts->num_rows() == 1) {
		                $wantedProductsArr = explode(',', $wantedProducts->row()->product_id);
		                if (in_array($tid, $wantedProductsArr)) {
		                    $returnArr['wanted'] = 1;
		                }
		            }
		            $returnArr['status'] = 1;
					$message = "Success";
					if($this->lang->line('json_success') != ""){
						$message = stripslashes($this->lang->line('json_success'));
					}
					$returnArr['response'] = $message;
                }else{
					$returnArr['response'] = 'Something Went Wrong. Try Again.';
				}
            }else{
				$returnArr['response'] = 'Product Not Found';
			}
	        echo json_encode($returnArr);
	}

	/*
	* !! >>>>>>>>>>>>> Product Adding To List Function  <<<<<<<<<<<< !!
	*/
	public function addProductToList() {
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$tid  =  $_POST['sellerProductId'];
		$lid  =  $_POST['listId'];
		$UserId  =  $_POST['UserId'];
		if($tid != "" && $lid != "" && $UserId != ""){
			$listDetails = $this->mobile_model->get_all_details(LISTS_DETAILS, array('id' => $lid));
			if ($listDetails->num_rows() == 1){
				$product_ids = explode(',', $listDetails->row()->product_id);
				if (!in_array($tid, $product_ids)) {
					array_push($product_ids, $tid);
					$new_product_ids = implode(',', $product_ids);
					$this->mobile_model->update_details(LISTS_DETAILS, array('product_id' => $new_product_ids), array('id' => $lid));
					$returnArr['status'] = 1;
					$message = "Success";
					if($this->lang->line('json_success') != ""){
						$message = stripslashes($this->lang->line('json_success'));
					}
					$returnArr['response'] = $message;
				}else{
					$returnArr['response'] = 'You have already added this product to this list';
				}
			}else{
				$returnArr['response'] = 'No List Found';
			}
		}else{
			$returnArr['response'] = 'Some Parameters are missing';
		}
		echo json_encode($returnArr);
	}

	/*
	* !! >>>>>>>>>>>>> Product Removing From List Function  <<<<<<<<<<<< !!
	*/
	public function removeProductFromList() {

		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$tid  =  $_POST['sellerProductId'];
		$lid  =  $_POST['listId'];
		$UserId  =  $_POST['UserId'];
		if($tid != "" && $lid != "" && $UserId != ""){
			$listDetails = $this->mobile_model->get_all_details(LISTS_DETAILS, array('id' => $lid));
			if ($listDetails->num_rows() == 1){
				$product_ids = explode(',', $listDetails->row()->product_id);
				if (in_array($tid, $product_ids)) {
					if (($key = array_search($tid, $product_ids)) !== false) {
						unset($product_ids[$key]);
					}
					$new_product_ids = implode(',', $product_ids);
					$this->mobile_model->update_details(LISTS_DETAILS, array('product_id' => $new_product_ids), array('id' => $lid));
					$returnArr['status'] = 1;
					$message = "Success";
					if($this->lang->line('json_success') != ""){
						$message = stripslashes($this->lang->line('json_success'));
					}
					$returnArr['response'] = $message;
				}else{
					$returnArr['response'] = 'You have already removed this product from this list';
				}
			}else{
				$returnArr['response'] = 'No List Found';
			}
		}else{
			$returnArr['response'] = 'Some Parameters are missing';
		}
		echo json_encode($returnArr);
	}

	/*
	* !! >>>>>>>>>>>>> Add Product To Want List <<<<<<<<<<<< !!
	*/
	public function addProductToWantList() {
        $returnArr['status'] = 0;
		$returnArr['response'] = '';
		$tid  =  $_POST['sellerProductId'];
		$userId  =  $_POST['UserId'];
		if($tid != "" && $userId){
			$this->data['userProfileDetails'] = $this->mobile_model->get_all_details(USERS, array('id' => $userId, 'status' => 'Active'));
			if($this->data['userProfileDetails']->num_rows() == 1){
				$wantDetails = $this->mobile_model->get_all_details(WANTS_DETAILS, array('user_id' => $userId));
				if ($wantDetails->num_rows() == 1) {
		            $product_ids = explode(',', $wantDetails->row()->product_id);
		            if (!in_array($tid, $product_ids)) {
		                array_push($product_ids, $tid);
						$new_product_ids = implode(',', $product_ids);
			            $this->mobile_model->update_details(WANTS_DETAILS, array('product_id' => $new_product_ids), array('user_id' => $userId));
		            }else{
						$returnArr['response'] = 'You have already added this product to want list';
					}
		        }else {
		            $dataArr = array('user_id' => $userId, 'product_id' => $tid);
		            $this->mobile_model->simple_insert(WANTS_DETAILS, $dataArr);
		        }
				$wantCount = $this->data['userProfileDetails']->row()->want_count;
				if ($wantCount <= 0 || $wantCount == '') {
					$wantCount = 0;
				}
				$wantCount++;
				$dataArr = array('want_count' => $wantCount);
				$ownProducts = explode(',', $this->data['userProfileDetails']->row()->own_products);
		        if (in_array($tid, $ownProducts)) {
		            if (($key = array_search($tid, $ownProducts)) !== false) {
		                unset($ownProducts[$key]);
		            }
		            $ownCount = $this->data['userProfileDetails']->row()->own_count;
		            $ownCount--;
		            $dataArr['own_count'] = $ownCount;
		            $dataArr['own_products'] = implode(',', $ownProducts);
		        }
		        $this->mobile_model->update_details(USERS, $dataArr, array('id' => $userId));
		        $returnArr['status'] = 1;
				$message = "Success";
				if($this->lang->line('json_success') != ""){
					$message = stripslashes($this->lang->line('json_success'));
				}
				$returnArr['response'] = $message;
			}else{
				$returnArr['response'] = 'Something Went Wrong';
			}
		}else{
			$returnArr['response'] = 'Some Parameters are missing';
		}
        echo json_encode($returnArr);
    }


	/*
	* !! >>>>>>>>>>>>> Create New Product List <<<<<<<<<<<< !!
	*/
	public function createNewProductList() {
        $returnArr['status'] = 0;
		$returnArr['response'] = "";

		$userId  =  $_POST['UserId'];
		$tid = $_POST['sellerProductId'];
        $list_name = $_POST['listName'];

		if($userId != "" && $tid != "" && $list_name != ""){
			$checkList = $this->mobile_model->get_all_details(LISTS_DETAILS, array('name' => $list_name, 'user_id' => $userId));
	        if ($checkList->num_rows() == 0) {
	            $dataArr = array('user_id' => $userId, 'name' => $list_name, 'product_id' => $tid);
	            $this->mobile_model->simple_insert(LISTS_DETAILS, $dataArr);
				$lastInsertId = $this->mobile_model->get_last_insert_id();
	            $userDetails = $this->mobile_model->get_all_details(USERS, array('id' => $userId));
	            $listCount = $userDetails->row()->lists;
	            if ($listCount < 0 || $listCount == '') {
	                $listCount = 0;
	            }
	            $listCount++;
	            $this->mobile_model->update_details(USERS, array('lists' => $listCount), array('id' => $userId));
				$ListDetails = $this->mobile_model->get_all_details(LISTS_DETAILS, array('id' => $lastInsertId, 'user_id' => $userId));
				$ListCount = array(
												'ListId'  =>  $ListDetails->row()->id,
												'Checked'  =>  'Checked',
												'ListName'  =>  $ListDetails->row()->name
												);
				$returnArr['ListDetails'] =  $ListCount ;
				$returnArr['status'] = 1;
				$message = "Success";
				if($this->lang->line('json_success') != ""){
					$message = stripslashes($this->lang->line('json_success'));
				}
				$returnArr['response'] = $message;
	        } else {
	            $productArr = explode(',', $checkList->row()->product_id);
	            if (!in_array($tid, $productArr)) {
	                array_push($productArr, $tid);
					$product_id = implode(',', $productArr);
		            $dataArr = array('product_id' => $product_id);
		            $this->mobile_model->update_details(LISTS_DETAILS, $dataArr, array('user_id' =>$userId, 'name' => $list_name));
					$ListDetails = $this->mobile_model->get_all_details(LISTS_DETAILS, array('id' => $checkList->row()->id, 'user_id' => $userId));
					$ListCount = array(
													'ListId'  =>  $ListDetails->row()->id,
													'Checked'  =>  'Checked',
													'ListName'  =>  $ListDetails->row()->name
													);
					$returnArr['ListDetails'] =  $ListCount ;
					$returnArr['status'] = 1;
					$message = "Success";
					if($this->lang->line('json_success') != ""){
						$message = stripslashes($this->lang->line('json_success'));
					}
					$returnArr['response'] = $message;
	            }else{
					$returnArr['response'] = "This product is already in this list";
				}
	        }
		}else{
			$returnArr['response'] = "Some Parameters are missing";
		}
        echo json_encode($returnArr);
    }


	/*
	* !! >>>>>>>>>>>>> Remove Product From Want List <<<<<<<<<<<< !!
	*/
	public function removeProductFromWantList() {
		$returnArr['status'] = 0;
		$returnArr['response'] = "";
		$tid  =  $_POST['sellerProductId'];
		$userId  =  $_POST['UserId'];
		if($tid != "" && $userId != ""){
			$this->data['userProfileDetails'] = $this->mobile_model->get_all_details(USERS, array('id' => $userId, 'status' => 'Active'));
			if($this->data['userProfileDetails']->num_rows() == 1){
				$wantDetails = $this->mobile_model->get_all_details(WANTS_DETAILS, array('user_id' => $userId));
				if ($wantDetails->num_rows() == 1) {
					$product_ids = explode(',', $wantDetails->row()->product_id);
					if (in_array($tid, $product_ids)) {
						if (($key = array_search($tid, $product_ids)) !== false) {
							unset($product_ids[$key]);
						}
						$new_product_ids = implode(',', $product_ids);
						$this->mobile_model->update_details(WANTS_DETAILS, array('product_id' => $new_product_ids), array('user_id' => $userId));
						$wantCount = $this->data['userProfileDetails']->row()->want_count;
						if ($wantCount <= 0 || $wantCount == '') {
							$wantCount = 1;
						}
						$wantCount--;
						$this->mobile_model->update_details(USERS, array('want_count' => $wantCount), array('id' => $userId));
						$returnArr['status'] = 1;
						$message = "Success";
						if($this->lang->line('json_success') != ""){
							$message = stripslashes($this->lang->line('json_success'));
						}
						$returnArr['response'] = $message;
					}else{
						$returnArr['response'] = 'You already removed this product from want list';
					}
				}else{
					$returnArr['response'] = 'Something Went Wrong';
				}
			}else{
				$returnArr['response'] = 'Something Went Wrong';
			}
		}else{
			$returnArr['response'] = "Some Parameters are missing";
		}
		echo json_encode($returnArr);
	}


	/*
	* !! >>>>>>>>>>>>> Add Product to Own List <<<<<<<<<<<< !!
	*/
	public function addProductToOwnList() {
		$returnArr['status'] = 0;
		$returnArr['response'] = "";
		$tid  =  $_POST['sellerProductId'];
		$uid  =  $_POST['UserId'];
		if($tid != "" && $uid != ""){
			$this->data['userProfileDetails'] = $this->mobile_model->get_all_details(USERS, array('id' => $uid, 'status' => 'Active'));
			if($this->data['userProfileDetails']->num_rows() == 1){
				$ownArr = explode(',', $this->data['userProfileDetails']->row()->own_products);
				$ownCount = $this->data['userProfileDetails']->row()->own_count;
				if (!in_array($tid, $ownArr)){
					array_push($ownArr, $tid);
					$ownCount++;
					$dataArr = array('own_products' => implode(',', $ownArr), 'own_count' => $ownCount);
					$wantProducts = $this->mobile_model->get_all_details(WANTS_DETAILS, array('user_id' => $uid));
					if ($wantProducts->num_rows() == 1) {
						$wantProductsArr = explode(',', $wantProducts->row()->product_id);
						if (in_array($tid, $wantProductsArr)) {
							if (($key = array_search($tid, $wantProductsArr)) !== false) {
								unset($wantProductsArr[$key]);
							}
							$wantsCount = $this->data['userProfileDetails']->row()->want_count;
							$wantsCount--;
							$dataArr['want_count'] = $wantsCount;
							$this->mobile_model->update_details(WANTS_DETAILS, array('product_id' => implode(',', $wantProductsArr)), array('user_id' => $uid));
						}
					}
					$this->mobile_model->update_details(USERS, $dataArr, array('id' => $uid));
					$returnArr['status'] = 1;
					$message = "Success";
					if($this->lang->line('json_success') != ""){
						$message = stripslashes($this->lang->line('json_success'));
					}
					$returnArr['response'] = $message;
				}else{
					$returnArr['response'] = "You already own this product";
				}
			}else{
				$returnArr['response'] = "Something went wrong";
			}
		}else{
			$returnArr['response'] = "Some Parameters are missing";
		}
		echo json_encode($returnArr);
	}

	/*
	* !! >>>>>>>>>>>>> Remove Product From Want List <<<<<<<<<<<< !!
	*/
	public function removeProductOwnList() {
		$returnArr['status'] = 0;
		$tid  =  $_POST['sellerProductId'];
		$uid  =  $_POST['UserId'];
		if($tid != "" && $uid != ""){
			$this->data['userProfileDetails'] = $this->mobile_model->get_all_details(USERS, array('id' => $uid, 'status' => 'Active'));
			if($this->data['userProfileDetails']->num_rows() == 1){
				$ownArr = explode(',', $this->data['userProfileDetails']->row()->own_products);
				$ownCount = $this->data['userProfileDetails']->row()->own_count;
				if (in_array($tid, $ownArr)) {
					if ($key = array_search($tid, $ownArr) !== false) {
						unset($ownArr[$key]);
						$ownCount--;
					}
					$this->mobile_model->update_details(USERS, array('own_products' => implode(',', $ownArr), 'own_count' => $ownCount), array('id' => $uid));
					$returnArr['status'] = 1;
					$message = "Success";
					if($this->lang->line('json_success') != ""){
						$message = stripslashes($this->lang->line('json_success'));
					}
					$returnArr['response'] = $message;
				}else{
					$returnArr['response'] = "You don't own this product";
				}
			}else{
				$returnArr['response'] = "Something went wrong";
			}
		}else{
			$returnArr['response'] = "Some Parameters are missing";
		}
		echo json_encode($returnArr);
	}


	/*
	* !! >>>>>>>>>>>>> List Details For Editing <<<<<<<<<<<< !!
	*/
	public function edit_user_lists() {
		$lid  =  $_POST['lid'];
		$uname  =  $_POST['uname'];
		$returnArr['status'] = 0;
		$listDetails =array();
		if($lid !='' && $lid !=''){
        $user  = $this->mobile_model->get_all_details(USERS, array('user_name' => $uname,'status'=>'Active'));
        if($user->num_rows() > 0){

		$userThumbnail = base_url().'images/users/user-thumb1.png';
		if($user->row()->thumbnail !='') $userThumbnail = base_url().'images/users/'.$user->row()->thumbnail;
		$userProfileDetails[] = array(
													'id'=> $user->row()->id,
													'userName'=> $user->row()->user_name,
													'fullName'=> $user->row()->full_name,
													'email'=> $user->row()->email,
													'userThumbnail'=> $userThumbnail,
													'group'=> $user->row()->group
												);
		$list_details = $this->mobile_model->get_all_details(LISTS_DETAILS, array('id' => $lid, 'user_id' => $user->row()->id));
		if($list_details->num_rows >0 ){
		 $listDetails[] =array(
									'id'  =>  $list_details->row()->id,
									'name'  =>  $list_details->row()->name

								);
	     }

		$list_category_details  = $this->mobile_model->get_all_details(CATEGORY, array('id' => $list_details->row()->category_id));
		$listCategoryDetails = array();
		if($list_category_details->num_rows() >0){
			$listCategoryDetails[] =array(
										'id'  =>  $list_category_details->row()->id,
										'categoryName'  =>  $list_category_details->row()->cat_name,
										'rootID'  =>  $list_category_details->row()->rootID,
										'seourl'  =>  $list_category_details->row()->seourl,
										'status'  =>  $list_category_details->row()->status

									);
		}
		foreach($_SESSION['sMainCategories']->result() as $MainCategories){
			if($list_category_details->row()->id == $MainCategories->id){
				$select = 1;
			}else{
				$select= 0;
			}

			$categoryList[]  =  array(
													'id'  =>  $MainCategories->id,
													'categoryName'  =>  $MainCategories->cat_name,
													'rootID'  =>  $MainCategories->rootID,
													'seourl'  =>  $MainCategories->seourl,
													'status'  =>  $MainCategories->status,
													'select_status'  =>  $select
													//'image'  =>  $_SESSION['sMainCategories']->row()->image,
													//'catPosition'  =>  $list_category_details->row()->cat_position,
													//	'seo_title'  =>  $list_category_details->row()->seo_title,
													//	'seo_keyword'  =>  $list_category_details->row()->seo_keyword,
													//	'seo_description'  =>  $list_category_details->row()->seo_description,
													//	'dateAdded'  =>  $list_category_details->row()->dateAdded
										);

		}
		$returnArr['userProfileDetails'] = $userProfileDetails;
		$returnArr['listDetails'] = $listDetails;
		//$returnArr['selectlistCategoryDetails'] = $listCategoryDetails;
		$returnArr['categoryList'] = $categoryList;
		$returnArr['status'] = 1;
	}else{
		$returnArr['response'] = 'Invalid User ';
	}
		}else{
		$returnArr['response'] = 'Parameters missing ';
	   }
		echo json_encode($returnArr);
    }

	/*
	* !! >>>>>>>>>>>>> Edited User List Save Function  <<<<<<<<<<<< !!
	*/

	public function edit_user_list_details() {

		$returnArr['status'] = 0;
		$lid = $_POST['listId'];
		$uid = $_POST['userId'];
		$list_title = $_POST['titleName'];
		$catID = $_POST['categoryId'];
		if($lid !='' && $uid !='' ) {

			 $duplicateCheck = $this->mobile_model->get_all_details(LISTS_DETAILS, array('user_id' => $uid, 'id' => $lid));

			if ($duplicateCheck->num_rows() > 0) {
				if ($catID == '') {
					$catID = 0;
				}
				$this->mobile_model->update_details(LISTS_DETAILS, array('name' => $list_title, 'category_id' => $catID), array('id' => $lid, 'user_id' => $uid));
				$returnArr['status'] = 1;
				$returnArr['response'] = "  List update successfully ";
			}else {
				$returnArr['response'] = " No list ";
			}
	   }else{
	   		$returnArr['response'] = " Parameters Missing ";
	   }
		echo json_encode($returnArr);
	}

	/*
	* !! >>>>>>>>>>>>> Delete User List  <<<<<<<<<<<< !!
	*/
	public function delete_user_list(){
		$returnArr['status'] = 0;
		$lid = $_POST['listId'];
		$uid = $_POST['userId'];
		$this->data['userProfileDetails'] = $this->mobile_model->get_all_details(USERS, array('id' => $uid, 'status' => 'Active'));
		$list_details = $this->mobile_model->get_all_details(LISTS_DETAILS, array('id' => $lid, 'user_id' => $uid));
		if ($list_details->num_rows() == 1) {
			$followers_id = $list_details->row()->followers;
			if ($followers_id != '') {
				$searchArr = array_filter(explode(',', $followers_id));
				$fieldsArr = array('following_user_lists', 'id');
				$followersArr = $this->mobile_model->get_fields_from_many(USERS, $fieldsArr, 'id', $searchArr);
				if ($followersArr->num_rows() > 0) {
					foreach ($followersArr->result() as $followersRow) {
						$listArr = array_filter(explode(',', $followersRow->following_user_lists));
						if (in_array($lid, $listArr)) {
							if (($key = array_search($lid, $listArr)) != false) {
								unset($listArr[$key]);
								$this->mobile_model->update_details(USERS, array('following_user_lists' => implode(',', $listArr)), array('id' => $followersRow->id));
							}
						}
					}
				}
			}
			$this->mobile_model->commonDelete(LISTS_DETAILS, array('id' => $lid, 'user_id' => $uid));
			$listCount = $this->data['userProfileDetails']->row()->lists;
			$listCount--;
			if ($listCount == '' || $listCount < 0) {
				$listCount = 0;
			}
			$this->mobile_model->update_details(USERS, array('lists' => $listCount), array('id' => $uid));
		//	$returnArr['url'] = base_url() . 'user/' . $this->data['userProfileDetails']->row()->user_name . '/lists';
			$returnArr['status'] = 1;
			$returnArr['response'] = "List deleted successfully";
		}else {
			if ($this->lang->line('lst_not_avail') != '')
				$returnArr['response'] = $this->lang->line('lst_not_avail');
			else
				$returnArr['response'] = 'List not available';
		}
		echo json_encode($returnArr);
	}

	/*
	* !! >>>>>>>>>>>>> Shop Page  <<<<<<<<<<<< !!
	*/
	public function shopPage(){
		$UserId = $_POST['UserId'];
		$this->data['bannerList'] = $this->mobile_model->get_all_details(BANNER_CATEGORY,array('status'=>'Publish'));
		$this->data['recentProducts'] = $this->mobile_model->view_product_details(" where p.status='Publish' and p.quantity > 0 and u.group='Seller' and u.status='Active' or p.status='Publish' and p.quantity > 0 and p.user_id=0 order by p.created desc limit 20");
		$bannerlistArr = array();
		if($this->data['bannerList']->num_Rows()){
			foreach($this->data['bannerList']->result() as $bannerlist){
				$bannerlistArr[]=array(
											'id'=>$bannerlist->id,
											'name'=>$bannerlist->name,
											'image'=>base_url().'images/category/banner/'.$bannerlist->image,
											'link'=>$bannerlist->link
										);
			}
		}
		$recentProductsArr =array();
		if($this->data['recentProducts']->result()){
			foreach($this->data['recentProducts']->result() as $recentprodList){
				$imgArr=@explode(',',$recentprodList->image);
				$images='';
				if($imgArr[0] != ""){
					$images=base_url().'images/product/'.$imgArr[0];
				}
				$liked = "0";
				if($UserId != ""){
					$cond = "select user_id from ".PRODUCT_LIKES." where product_id=".$recentprodList->seller_product_id." and user_id=".$UserId;
					$likedUsersList = $this->mobile_model->ExecuteQuery($cond);
					if($likedUsersList->num_rows() > 0){
						$liked = "1";
					}
				}
				$recentProductsArr[]=array(
													'id'=>$recentprodList->id,
													'sellerProductId'=>$recentprodList->seller_product_id,
													'productName'=>$recentprodList->product_name,
													'productImage'=>$images,
													'salePrice'=>$recentprodList->sale_price,
													'userName'=>$recentprodList->user_name,
													'userId'=>$recentprodList->user_id,
													'likes'=>$recentprodList->likes,
													'liked' => $liked
													//'seourl'=>$recentprodList->seourl,
													//'description'=>$recentprodList->description,
													//'shippingPolices'=>$recentprodList->shipping_policies,
													//'price'=>$recentprodList->price,
													//'full_name'=>$recentprodList->full_name,
													//'url'  =>   base_url().'json/product?pid='.$recentprodList->id
											);
			}
		}
		$mainCatList = $this->product_model->get_all_details(CATEGORY,array('rootID'=>'0','status'=>'Active'),$sortArr);
		$categoryList = array();
		if($mainCatList->num_rows() > 0){
			foreach($mainCatList->result() as $MainCategories){

				$cond = "select count(id) as productCount from ".PRODUCT." where FIND_IN_SET('".$MainCategories->id."',category_id) and status='Publish' ";
				$productCount = $this->mobile_model->ExecuteQuery($cond);
				$categoryImage = base_url().'images/category/dummy.jpg';
				if($MainCategories->image != ""){
					$categoryImage = base_url().'images/category/'.$MainCategories->image;
				}
				$categoryList[]  =  array(
												'id'  =>  $MainCategories->id,
												'categoryName'  =>  $MainCategories->cat_name,
												//'rootID'  =>  $MainCategories->rootID,
												'seourl'  =>  $MainCategories->seourl,
												'image'  =>  $categoryImage,
												'productCount' => $productCount->row()->productCount
												//'url'      =>   base_url().'json/shopby/'.$MainCategories->cat_name
											);

			}
		}
		$returnArr['categoryList'] = $categoryList;
		$returnArr['bannerList']=$bannerlistArr;
		$returnArr['recentProducts']=$recentProductsArr;
		echo json_encode($returnArr);
	}

	/*
	* !! >>>>>>>>>>>>> This Function to Upload User Product Picture    <<<<<<<<<<<< !!
	*/
	public function UploadAffProductImage(){
		$responseArr['status'] = 0;
		$responseArr['response'] = '';
		if (!empty($_POST['AffProductImage'])) {
		//if (true) {
			$imgPath ='images/temp/';
			$img = $_POST['AffProductImage'];
			
			$imgName = time(). ".jpg";
			$imageFormat = array('data:image/jpeg;base64', 'data:image/png;base64', 'data:image/jpg;base64', 'data:image/gif;base64');
			$img = str_replace($imageFormat, '', $img);
			$data = base64_decode($img);		
			
			$image = @imagecreatefromstring($data);

			if ($image !== false) {
				$uploadPath = $imgPath . $imgName;
				imagejpeg($image, $uploadPath, 100);
				imagedestroy($image);
			} else {
				$responseArr['response'] ='An error occurred2';
			}
			if (isset($uploadPath)) {
				/* Creating Thumbnail image with the size of 100 X 100 */
				$filename = base_url($uploadPath);
				list($width, $height) = getimagesize($filename);
				$newwidth = 100;
				$newheight = 100;
				//$thumbImage = @imagecreatetruecolor($newwidth, $newheight);
				$sourceImage = @imagecreatefromjpeg($filename);
				imagecopyresized($thumbImage, $sourceImage, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
				// if ($thumbImage !== false) {
				// 	/* Not uploading to thumb folder */
				// 	$thumbImgPath = 'images/users/thumb/';
				// 	$thumpUploadPath = $thumbImgPath . $imgName;
				// 	imagejpeg($thumbImage, $thumpUploadPath, 100);
				// }
			} else {
				$responseArr['response'] ='Invalid File';
			}
		$responseArr['status'] = 1;
		$responseArr['response'] = $imgName;
		}else{
			$responseArr['response'] = "Please Upload Image";
		}
		$json_encode = json_encode($responseArr, JSON_PRETTY_PRINT);
		echo $json_encode;
	}

	/*
	* !! >>>>>>>>>>>>> This Function to Upload User Products    <<<<<<<<<<<< !!
	*/
	public function UploadsellerProductImage() {
		$responseArr['status'] = 0;
		$responseArr['response'] = '';
		$user_Id = $_POST['UserId'];
		$sellerproduct_Id = $_POST['sellerProductId'];
		$images = $this->input->post('images');
		$imgArr = array();
		if($images !=''){
			$imgArr=array_filter(@explode(',',$images));
		}
		if($user_Id !="" && $sellerproduct_Id !=""){
			$user_details = $this->product_model->get_all_details(USERS, array('id' => $user_Id, 'status' => 'Active'));
			if($user_details->num_rows > 0){
				$ProductDetails = $this->product_model->get_all_details(PRODUCT, array('seller_product_id' => $sellerproduct_Id,'user_id'=>$user_Id));
				if($ProductDetails->num_rows > 0){
					if(count($imgArr)>0){
						$imagesVal=array();
						$imgsav="temp/";
						$imgsavr="./temp/";
						foreach($imgArr as $img){
							$imgName = basename($img);
							$new_name = $imgName;
							$filename = './images/product/'.$img;
							if(file_exists($filename)){
								$imagesVal[]=$img;
							}else{
								/* $timeImg=time();
								@copy($imgsavr.$img, './images/product/temp_img/'.$timeImg.'-'.$img);

								@copy($imgsavr.$img, './images/product/'.$timeImg.'-'.$img);
								$this->ImageResizeWithCrop(600, 600, $timeImg.'-'.$img, './images/product/');

								@copy('./images/product/'.$timeImg.'-'.$img, './images/product/thumb/'.$timeImg.'-'.$img);
								$this->ImageResizeWithCrop(200, 200, $timeImg.'-'.$img, './images/product/thumb/');

								$imagesVal[]=$timeImg.'-'.$img;*/

								@copy("./images/temp/".$imgName, './images/product/'.$new_name);
								@copy("./images/temp/".$imgName, './images/product/thumb/'.$new_name);

								$this->imageResizeWithSpace(200, 200, $new_name, './images/product/thumb/');
								$this->imageResizeWithSpace(600, 600, $new_name, './images/product/');
								$imagesVal[]=$new_name;
							}
						}
						$finalImageVal=@implode(',',$imagesVal);
						$dataArr = array('image'=>$finalImageVal);

						$this->product_model->edit_product($dataArr,array('seller_product_id' => $sellerproduct_Id,'user_id'=>$user_Id));
						$json_encode =  json_encode(array("status_code"=>"1","response"=>"Product updated successfully"));
					}else{
						$json_encode =  json_encode(array("status_code"=>"0","response"=>"Minimum one photo is required"));
					}
				}else{
					$json_encode =  json_encode(array("status_code"=>"0","response"=>"Invalid Product"));
				}
			}else{
				$json_encode =  json_encode(array("status_code"=>"0","response"=>"Invalid User"));
			}

		}else{
			$json_encode =  json_encode(array("status_code"=>"0","response"=>"Some Parameters are Missing"));
		}
        echo $json_encode;;
   }

	/*
	* !! >>>>>>>>>>>>> This Function to Upload User Product Picture    <<<<<<<<<<<< !!
	*/
	public function UploadsellerProductImagetest(){
		$responseArr['status'] = 0;
		$responseArr['response'] = '';
		$user_Id = $_POST['UserId'];
		$sellerproduct_Id = $_POST['sellerProductId'];
		if($user_Id !="" && $sellerproduct_Id !=""){
		// if (!empty($_POST['ProductImage'])) {
		if (true) {
			$product_details  = $this->mobile_model->get_all_details(PRODUCT,array('status'=>'Publish','seller_product_id'=>$sellerproduct_Id,'user_id'=>$user_Id));
		    $arrproduct_details = $product_details->result();
		    $eximagename = array_filter(explode(',', $arrproduct_details[0]->image));
		   if($product_details->num_rows() > 0){
			$imgPath ='images/product/';
			$img = $_POST['ProductImage'];
			$imgName = time(). ".jpg";
			$imageFormat = array('data:image/jpeg;base64', 'data:image/png;base64', 'data:image/jpg;base64', 'data:image/gif;base64');
			$img = str_replace($imageFormat, '', $img);
			$data = base64_decode($img);
			$image = @imagecreatefromstring($data);
			if ($image !== false) {
				$uploadPath = $imgPath . $imgName;
				imagejpeg($image, $uploadPath, 100);
				imagedestroy($image);
			} else {
				$responseArr['response'] ='An error occurred2';
			}
			if (isset($uploadPath)) {
				$filename = base_url($uploadPath);
				list($width, $height) = getimagesize($filename);
				$newwidth = 100;
				$newheight = 100;
				$sourceImage = @imagecreatefromjpeg($filename);
				imagecopyresized($thumbImage, $sourceImage, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
			} else {
				$responseArr['response'] ='Invalid File';
			}
			$newimga = explode("", $imgName);
		    $dataArr = array_merge($eximagename, $newimga);
		    // print_r($dataArr);
		    // die;
			$responseArr['status'] = 1;
			$responseArr['response'] = $imgName;
	}else{
			$responseArr['response'] = "Product Not Found ";
	}
		}else{
			$responseArr['response'] = "Please Upload Image";
		}
	}else{
			$responseArr['response'] = "Some parameter missing ";
	}
		$json_encode = json_encode($responseArr, JSON_PRETTY_PRINT);
		echo $json_encode;
	}

	/*
	* !! >>>>>>>>>>>>> This Function to Upload User Products    <<<<<<<<<<<< !!
	*/
	public function UploadAffProduct() {
        $returnArr['status_code'] = 0;
        $returnArr['response'] = '';
		$UserId = $_POST['UserId'];
        if ($UserId != '') {
		$this->data['userProfileDetails'] = $this->mobile_model->get_all_details(USERS, array('id' => $UserId, 'status' => 'Active'));
		$short_url = $this->get_rand_str('6');
		$checkId = $this->mobile_model->get_all_details(SHORTURL, array('short_url' => $short_url));
		while ($checkId->num_rows() > 0) {
			$short_url = $this->get_rand_str('6');
			$checkId = $this->mobile_model->get_all_details(SHORTURL, array('short_url' => $short_url));
		}
		$result = $this->mobile_model->add_user_product($UserId, $short_url);
		$product_id = $this->db->insert_id();
		$user_details = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
		if ($user_details->row()->group == "Seller") {
			$user_product = $this->mobile_model->get_all_details(USER_PRODUCTS, array('id' => $product_id));
			$dataArr = array('activity_name' => "add_product",
						'activity_id' => $user_product->row()->seller_product_id,
						'user_id' => $UserId
						);
			$this->mobile_model->simple_insert(USER_ACTIVITY, $dataArr);
		}
		if ($result['image'] != ''){
			$new_name = $result['image'];
			@copy("./images/temp/".$result['image'], './images/product/'.$new_name);
			@copy("./images/temp/".$result['image'], './images/product/thumb/'.$nw_name);
		//	$this->imageResizeWithSpace(210, 210, $new_name, './images/product/tehumb/');
		//	$this->imageResizeWithSpace(600, 600, $new_name, './images/product/');
		}
		$dir = getcwd()."images/temp/";
		$interval = strtotime('-24 hours');
		foreach (glob($dir."*.*") as $file){
			if (filemtime($file) <= $interval ){
				if(!is_dir($file) ){
					unlink($file);
				}
			}
		}
		$returnArr['status_code'] = 1;
		$userDetails = $this->data['userProfileDetails'];
		$total_added = $userDetails->row()->products;
		$total_added++;
		$this->mobile_model->update_details(USERS, array('products' => $total_added), array('id' => $UserId));
		$returnArr['ProductUrl'] = base_url() . 'json/userproduct?pid='.$result['pid'].'&uname='.$userDetails->row()->user_name;
		$returnArr['status_code'] = 1;
		$returnArr['response'] = 'Successfully Uploaded';
        }
        echo json_encode($returnArr);
    }

	/*
	* !! >>>>>>>>>>>>> List Of Categories  <<<<<<<<<<<< !!
	*/
	public function CatForAffProduct(){
		foreach($_SESSION['sMainCategories']->result() as $MainCategories){
			// $categoryImage = base_url().'images/category/dummy.jpg';
			// if($MainCategories->image != ""){
			// 	$categoryImage = base_url().'images/category/'.$MainCategories->image;
			// }
			$categoryList[]  =  array(
							'id'  =>  $MainCategories->id,
							'categoryName'  =>  $MainCategories->cat_name,
							'rootID'  =>  $MainCategories->rootID,
							'seourl'  =>  $MainCategories->seourl,
							'status'  =>  $MainCategories->status
							//'image'  =>  $categoryImage
							//'catPosition'  =>  $list_category_details->row()->cat_position,
							//'seo_title'  =>  $list_category_details->row()->seo_title,
							//'seo_keyword'  =>  $list_category_details->row()->seo_keyword,
							//'seo_description'  =>  $list_category_details->row()->seo_description,
							//'dateAdded'  =>  $list_category_details->row()->dateAdded
							);

		}
		$returnArr['categoryList'] = $categoryList;
		echo json_encode($returnArr);
	}

	/*
	* !! >>>>>>>>>>>>> Follow User <<<<<<<<<<<< !!
	*/
	public function addFollow() {
        $returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId  =  $_POST['UserId'];
        if ($UserId != ''){
            $follow_id = $_POST['followingUserId'];
			if($follow_id != ""){
				$this->data['userProfileDetails'] = $this->mobile_model->get_all_details(USERS, array('id' => $UserId, 'status' => 'Active'));
	            $followingListArr = explode(',', $this->data['userProfileDetails']->row()->following);
	            if (!in_array($follow_id, $followingListArr)) {
	                $followingListArr[] = $follow_id;
	                $newFollowingList = implode(',', $followingListArr);
	                $followingCount = $this->data['userProfileDetails']->row()->following_count;
	                $followingCount++;
	                $dataArr = array('following' => $newFollowingList, 'following_count' => $followingCount);
	                $condition = array('id' => $UserId);
	                $this->mobile_model->update_details(USERS, $dataArr, $condition);
	                $followUserDetails = $this->mobile_model->get_all_details(USERS, array('id' => $follow_id));
	                if ($followUserDetails->num_rows() == 1) {
	                    $followersListArr = explode(',', $followUserDetails->row()->followers);
	                    if (!in_array($UserId, $followersListArr)) {
	                        $followersListArr[] = $UserId;
	                        $newFollowersList = implode(',', $followersListArr);
	                        $followersCount = $followUserDetails->row()->followers_count;
	                        $followersCount++;
	                        $dataArr = array('followers' => $newFollowersList, 'followers_count' => $followersCount);
	                        $condition = array('id' => $follow_id);
	                        $this->mobile_model->update_details(USERS, $dataArr, $condition);
	                    }
	                }
	                $actArr = array(
	                    'activity_name' => 'follow',
	                    'activity_id' => $follow_id,
	                    'user_id' => $UserId,
	                    'activity_ip' => $this->input->ip_address()
	                );
	                $this->mobile_model->simple_insert(USER_ACTIVITY, $actArr);
	                $datestring = "%Y-%m-%d %H:%i:%s";
	                $time = time();
	                $createdTime = mdate($datestring, $time);
	                $actArr = array(
	                    'activity' => 'follow',
	                    'activity_id' => $follow_id,
	                    'user_id' => $UserId,
	                    'activity_ip' => $this->input->ip_address(),
	                    'created' => $createdTime
	                );
	                $this->mobile_model->simple_insert(NOTIFICATIONS, $actArr);
	                $returnArr['status'] = 1;
					$returnArr['response'] = 'Success';
	            } else {
					$returnArr['response'] = 'This user is in ur following List';
	            }
			}else{
				$returnArr['response'] = 'Following UserId Null';
			}
        }else{
	        $returnArr['response'] = 'UserId Null';
		}
        echo json_encode($returnArr);
    }


	/*
	* !! >>>>>>>>>>>>> UnFollow User <<<<<<<<<<<< !!
	*/
	public function deleteFollow() {
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId  =  $_POST['UserId'];
        if ($UserId != '') {
            $follow_id = $_POST['followingUserId'];
			if($follow_id != ""){
				$this->data['userProfileDetails'] = $this->mobile_model->get_all_details(USERS, array('id' => $UserId, 'status' => 'Active'));
				$followingListArr = explode(',', $this->data['userProfileDetails']->row()->following);
				if (in_array($follow_id, $followingListArr)) {
					if (($key = array_search($follow_id, $followingListArr)) !== false) {
						unset($followingListArr[$key]);
					}
					$newFollowingList = implode(',', $followingListArr);
					$followingCount = $this->data['userProfileDetails']->row()->following_count;
					$followingCount--;
					$dataArr = array('following' => $newFollowingList, 'following_count' => $followingCount);
					$condition = array('id' => $UserId);
					$this->mobile_model->update_details(USERS, $dataArr, $condition);
					$followUserDetails = $this->mobile_model->get_all_details(USERS, array('id' => $follow_id));
					if ($followUserDetails->num_rows() == 1) {
						$followersListArr = explode(',', $followUserDetails->row()->followers);
						if (in_array($UserId, $followersListArr)) {
							if (($key = array_search($UserId, $followersListArr)) !== false) {
								unset($followersListArr[$key]);
							}
							$newFollowersList = implode(',', $followersListArr);
							$followersCount = $followUserDetails->row()->followers_count;
							$followersCount--;
							$dataArr = array('followers' => $newFollowersList, 'followers_count' => $followersCount);
							$condition = array('id' => $follow_id);
							$this->mobile_model->update_details(USERS, $dataArr, $condition);
						}
					}
					$actArr = array(
						'activity_name' => 'unfollow',
						'activity_id' => $follow_id,
						'user_id' => $UserId,
						'activity_ip' => $this->input->ip_address()
					);
					$this->mobile_model->simple_insert(USER_ACTIVITY, $actArr);
					$returnArr['status'] = 1;
					$returnArr['response'] = 'Success';
				} else {
					$returnArr['response'] = 'This user is not in ur following list';
				}
			}else{
				$returnArr['response'] = 'Following UserId Null';
			}
		}else{
			$returnArr['response'] = 'UserId Null';
		}
		echo json_encode($returnArr);
	}

	/*
	* !! >>>>>>>>>>>>> Search Suggestions <<<<<<<<<<<< !!
	*/
	public function searchSuggestions(){
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$search_key = $_POST['q'];
		if($search_key == ""){
			$returnArr['response'] = 'Search Key Null !!';
		}else{
			$type = $_POST['type'];
			if($type == '0'){
				$type= '';
			}
			$count = $_POST['pg'];
			if ($search_key != '' && $type==''){
				$limit=10;
				$offset=0;
				$sellingProductDetails = $this->mobile_model->get_products_search_results($search_key,$limit,$offset);
				$affiliateProductDetails = $this->mobile_model->get_user_products_search_results($search_key,$limit,$offset);
				$productDetails =  $this->data['productDetails'] = $this->mobile_model->get_sorted_array_mobile($sellingProductDetails, $affiliateProductDetails, 'created', 'desc');
				$prodArr = array();
				if(count($productDetails) > 0){
					foreach($productDetails as $prodRow){
						// $imgArr=@explode(',',$prodRow['image']);
						// $images=array();
						// foreach($imgArr as $img){
						// if($img!="")
						// 	$images[]=base_url().'images/product/'.$img;
						// }
						$imgArr=@explode(',',$prodRow['image']);
						$productImage = ($imgArr[0]=='') ? '' : base_url().'images/product/'.$imgArr[0];

						$prodArr[]=array(
													'id'=>$prodRow['id'],
													'seller_product_id'=>$prodRow['seller_product_id'],
													'product_name'=>$prodRow['product_name'],
													'seourl'=>$prodRow['seourl'],
													'description'=>$prodRow['description'],
													'shippingPolicies'=>$prodRow['shipping_policies'],
													'image'=>$productImage,
													'price'=>$prodRow['price'],
													'sale_price'=>$prodRow['sale_price'],
													'user_name'=>$prodRow['user_name'],
													'likes'=>$prodRow['likes'],
													'url'=>$prodRow['url'],
													'type' => $prodRow['type'],
													'userUrl' => 	$normalUserUrl = 'http://192.168.1.251:8081/product-working/fancyclone-v3/json/user/seller-timeline?username='.$prodRow['user_name']
													);
					}
				}
				$returnArr['status'] = 1;
				$returnArr['response'] = 'Success';
				$returnArr['productDetails']=$prodArr;
				$usersCount=20;
				$offset=0;
				$userDetails = $this->mobile_model->get_user_search_results($search_key,$usersCount,$offset);
				$userProfileDetails = array();
				if ($userDetails->num_rows()>0){
					foreach($userDetails->result() as $user){
						$userThumbnail = base_url().'images/users/user-thumb1.png';
						if($user->thumbnail !='') $userThumbnail = base_url().'images/users/'.$user->thumbnail;
						$userProfileDetails[] = array(
																	'id'=> $user->id,
																	'userName'=> $user->user_name,
																	'fullName'=> $user->full_name,
																	'email'=> $user->email,
																	'userThumbnail'=> $userThumbnail,
																	'group'=> $user->group
																);
					}
				}
				$returnArr['userProfileDetails']=$userProfileDetails;
				$this->data['featured_seller'] = $this->mobile_model->get_featured_sellers();
				if($this->data['featured_seller']->num_rows > 0){
					$featured = $this->data['featured_seller']->result();
					$featuredId = array();
					foreach ($featured as $seller) {
						$featuredId[] = $seller->id;
					}
				}
				if($this->config->item('storefront_fees_month') == '0' && $this->config->item('storefront_fees_year') == '0'){
						$condition = " Select * from ".USERS." where 'group'='Seller' and 'status'=>'Active' and  user_name like '%".$search_key."%'";
						$sellers_list = $this->mobile_model->get_all_details($condition);
				}else{
					$condition = 'select u.*,s.store_name,s.logo_image,s.cover_image,s.tagline , s.description from '.USERS.' as u left join '.STORE_FRONT.' as s on s.user_id = u.id where u.group ="Seller" and u.store_payment ="Paid" and u.status = "Active" and NOT FIND_IN_SET(u.id ,"'.implode(",", $featuredId).'") and full_name like "%'.$search_key.'%"  or u.group ="Seller" and u.store_payment ="Paid" and u.status = "Active" and user_name like "%'.$search_key.'%" and NOT FIND_IN_SET(u.id ,"'.implode(",", $featuredId).'") or s.store_name like "%'.$search_key.'%"';
					$sellers_list  = $this->mobile_model->ExecuteQuery($condition);

				}
				$storeDetails = array();
				if($sellers_list->num_rows() > 0){
					foreach($sellers_list->result() as $store){
						$storeBanner = base_url().'images/store/banner-dummy.jpg';
						$storeLogo = base_url().'images/store/dummy-logo.png';
						$storeTagline = "";
						$storeDescription = "";
						$storeName ="";
						$storeName = $store->store_name;
						if($store->cover_image !=""){
							$storeBanner = base_url().'images/store/'.$store->cover_image;
						}
						if($store->logo_image !=""){
							$storeLogo =  base_url().'images/store/'.$store->logo_image;
						}
						$storeTagline =$store->tagline;
						$storeDescription =$store->description;
						$userThumbnail = base_url().'images/users/user-thumb1.png';
						if($store->thumbnail !='') $userThumbnail = base_url().'images/users/'.$store->thumbnail;
						$storeDetails[] = array(
													'UserId' => $store->id,
													'fullName' => $store->full_name,
													'userName' => $store->user_name,
													'UserImage' => $userThumbnail,
													'state' => $store->state,
													'country' => $store->country,
													'storeName' => $storeName,
													'storeBanner' => $storeBanner,
													'storeLogo' => $storeLogo,
													'storeDescription' =>$storeDescription,
													'storeDescription' => $storeDescription
													);
					}
				}
				$returnArr['Stores']=$storeDetails;
			}else{
				if($type=='product'){
					$limit=10;
					$offset = 10*$count;
					$sellingProductDetails = $this->mobile_model->get_products_search_results($search_key,$limit,$offset);
					$affiliateProductDetails = $this->mobile_model->get_user_products_search_results($search_key,$limit,$offset);
					$productDetails =  $this->data['productDetails'] = $this->mobile_model->get_sorted_array_mobile($sellingProductDetails, $affiliateProductDetails, 'created', 'desc');
					$prodArr = array();
					if(count($productDetails) > 0){
						foreach($productDetails as $prodRow){
							$imgArr=@explode(',',$prodRow['image']);
							$images=array();
							foreach($imgArr as $img){
							if($img!="")
								$images[]=base_url().'images/product/'.$img;
							}
							$prodArr[]=array(
														'id'=>$prodRow['id'],
														'seller_product_id'=>$prodRow['seller_product_id'],
														'product_name'=>$prodRow['product_name'],
														'seourl'=>$prodRow['seourl'],
														'description'=>$prodRow['description'],
														'shippingPolicies'=>$prodRow['shipping_policies'],
														'image'=>$images,
														'price'=>$prodRow['price'],
														'sale_price'=>$prodRow['sale_price'],
														'user_name'=>$prodRow['user_name'],
														'likes'=>$prodRow['likes'],
														'url'=>$prodRow['url'],
														'type' => $prodRow['type'],
														'userUrl' => 	$normalUserUrl = 'http://192.168.1.251:8081/product-working/fancyclone-v3/json/user/seller-timeline?username='.$prodRow['user_name']
														);
						}
					}
					$returnArr['productDetails']=$prodArr;
				}else if($type=='people'){
					$usersCount=20;
					$offset=10*$count;
					$userDetails = $this->mobile_model->get_user_search_results($search_key,$usersCount,$offset);
					$userProfileDetails = array();
					if ($userDetails->num_rows()>0){
						foreach($userDetails->result() as $user){
							$userThumbnail = base_url().'images/users/user-thumb1.png';
							if($user->thumbnail !='') $userThumbnail = base_url().'images/users/'.$user->thumbnail;
							$userProfileDetails[] = array(
																		'id'=> $user->id,
																		'userName'=> $user->user_name,
																		'fullName'=> $user->full_name,
																		'email'=> $user->email,
																		'userThumbnail'=> $userThumbnail,
																		'group'=> $user->group
																	);
						}
					}
					$returnArr['userProfileDetails']=$userProfileDetails;
				}else if($type=='store'){
					$limit=20;
					$offset=10*$count;
					$this->data['featured_seller'] = $this->mobile_model->get_featured_sellers();
					if($this->data['featured_seller']->num_rows > 0){
						$featured = $this->data['featured_seller']->result();
						$featuredId = array();
						foreach ($featured as $seller) {
							$featuredId[] = $seller->id;
						}
					}
					if($this->config->item('storefront_fees_month') == '0' && $this->config->item('storefront_fees_year') == '0'){
							$condition = " Select * from ".USERS." where 'group'='Seller' and 'status'=>'Active' and  user_name like '%".$search_key."%' limit". $limit." offset ".$offset;
							$sellers_list = $this->mobile_model->get_all_details($condition);
					}else{
						$condition = 'select u.*,s.store_name,s.logo_image,s.cover_image,s.tagline , s.description from '.USERS.' as u left join '.STORE_FRONT.' as s on s.user_id = u.id where u.group ="Seller" and u.store_payment ="Paid" and u.status = "Active" and NOT FIND_IN_SET(u.id ,"'.implode(",", $featuredId).'") and full_name like "%'.$search_key.'%"  or u.group ="Seller" and u.store_payment ="Paid" and u.status = "Active" and user_name like "%'.$search_key.'%" and NOT FIND_IN_SET(u.id ,"'.implode(",", $featuredId).'") or s.store_name like "%'.$search_key.'%" limit '.$limit.' offset '.$offset;
						$sellers_list  = $this->mobile_model->ExecuteQuery($condition);

					}
					$storeDetails = array();
					if($sellers_list->num_rows() > 0){
						foreach($sellers_list->result() as $store){
							$storeBanner = base_url().'images/store/banner-dummy.jpg';
							$storeLogo = base_url().'images/store/dummy-logo.png';
							$storeTagline = "";
							$storeDescription = "";
							$storeName ="";
							$storeName = $store->store_name;
							if($store->cover_image !=""){
								$storeBanner = base_url().'images/store/'.$store->cover_image;
							}
							if($store->logo_image !=""){
								$storeLogo =  base_url().'images/store/'.$store->logo_image;
							}
							$storeTagline =$store->tagline;
							$storeDescription =$store->description;
							$userThumbnail = base_url().'images/users/user-thumb1.png';
							if($store->thumbnail !='') $userThumbnail = base_url().'images/users/'.$store->thumbnail;
							$storeDetails[] = array(
														'UserId' => $store->id,
														'fullName' => $store->full_name,
														'userName' => $store->user_name,
														'UserImage' => $userThumbnail,
														'state' => $store->state,
														'country' => $store->country,
														'storeName' => $storeName,
														'storeBanner' => $storeBanner,
														'storeLogo' => $storeLogo,
														'storeDescription' =>$storeDescription,
														'storeDescription' => $storeDescription
														);
						}
					}
					$returnArr['Stores']=$storeDetails;
				}


			}
		}
		echo json_encode($returnArr);
	}

	/*
	* !! >>>>>>>>>>>>> Add Seller Product Form <<<<<<<<<<<< !!
	*/

	public function AddSellerProductForm() {
		$returnArr['status_code'] = 0;
		$returnArr['response'] = '';
		$UserId = $_GET['UserId'];
		if ($UserId == '') {
			$returnArr['response'] = 'UserId Null !! Please Login';
		} else {
			$this->data['userProfileDetails'] = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
			$productDetails=array();
			$userType = $this->data['userProfileDetails']->row()->group;
			if ($userType == 'Seller'){
				$pid = $_GET['ProductId'];
				$product_Details = $this->mobile_model->get_all_details(USER_PRODUCTS, array('seller_product_id' => $pid));
				if ($product_Details->num_rows() == 1) {
					if ($product_Details->row()->user_id == $this->data['userProfileDetails']->row()->id) {
						$shipping_Cost_Details = $this->mobile_model->get_all_details(SHIPPING_COST, array('product_id' => $pid));
						$productDetails[]=array(
																'id'  => $product_Details->row()->id,
																'sellerProductId' => $product_Details->row()->seller_product_id,
																'productName' => $product_Details->row()->product_name,
																'seourl' => $product_Details->row()->seourl,
																'excerpt' => $product_Details->row()->excerpt,
																'categoryId' => $product_Details->row()->category_id,
																'image' => $product_Details->row()->image,
																'status' => $product_Details->row()->status,
																'userId' => $product_Details->row()->user_id,
																'shortUrlId' => $product_Details->row()->short_url_id,
																'likes' => $product_Details->row()->likes
																);
						$returnArr['productDetails'] = $productDetails;
						$returnArr['status_code'] = 1;
						$returnArr['response'] = 'Success';
					}else {
						$returnArr['status_code'] = 0;
						$returnArr['response'] = 'You cant add other\'s product as seller product';
					}
				} else {
					$returnArr['status_code'] = 0;
					$returnArr['response'] = 'Product Not Found';
				}
			} else {
				$returnArr['status_code'] = 0;
				$returnArr['response'] = 'Subscribe as Seller';
			}
		}
		echo json_encode($returnArr);
	}

	/*
	!! >>>>>>>>>> Insert & Update Shippping Address <<<<<<<<<<< !!
	*/
	public function insertEditShippingAddress() {
		$returnArr['status_code'] = 0;
		$returnArr['response'] = '';
		$UserId  =  $_POST['user_id'];
        if ($UserId == '') {
			$returnArr['response'] = 'UserId Null !!';
        } else {
            $shipID = $_POST['ship_id'];
            $is_default = '';
            $is_default = $_POST['set_default'];
            if ($is_default != '') {
                $primary = 'Yes';
            } else {
                $primary = 'No';
            }
            $checkAddrCount = $this->mobile_model->get_all_details(SHIPPING_ADDRESS, array('user_id' => $UserId));
            if ($checkAddrCount->num_rows == 0) {
                $primary = 'Yes';
            }
            $excludeArr = array('ship_id', 'set_default');
            $dataArr = array('primary' => $primary);
            $condition = array('id' => $shipID);
            if (empty($shipID)) {
                $this->mobile_model->commonInsertUpdate(SHIPPING_ADDRESS, 'insert', $excludeArr, $dataArr, $condition);
                $shipID = $this->mobile_model->get_last_insert_id();
				$condition1 = array('id' => $shipID, 'user_id' => $UserId);
                $insertedShipAddr = $this->mobile_model->get_all_details(SHIPPING_ADDRESS, $condition1);
				$optionsValues = '';
				$shippingAddress[]  =  array(
														 'selectedAddress' =>  $optionsValues,
														 'id'  =>  $insertedShipAddr->row()->id,
														 'FullName'  =>  $insertedShipAddr->row()->full_name,
														 'Address1'  =>  $insertedShipAddr->row()->address1,
														 'city'  =>  $insertedShipAddr->row()->city,
														 'state'  =>  $insertedShipAddr->row()->state,
														 'postalCode'  =>  $insertedShipAddr->row()->postal_code,
														 'country'  =>  $insertedShipAddr->row()->country,
														 'phone'  =>  $insertedShipAddr->row()->phone
													 );

                if ($this->lang->line('ship_add_succ') != ''){
                    $lg_err_msg = $this->lang->line('ship_add_succ');
				}else{
                    $lg_err_msg = 'Your Shipping address is added successfully !';
				}
				$returnArr['status_code'] = 1;
				$returnArr['response'] = $lg_err_msg;
				$returnArr['shippingAddress'] =$shippingAddress;
            }else {
                $this->mobile_model->commonInsertUpdate(SHIPPING_ADDRESS, 'update', $excludeArr, $dataArr, $condition);
                if ($this->lang->line('ship_updat_succ') != ''){
                    $lg_err_msg = $this->lang->line('ship_updat_succ');
				}else{
                    $lg_err_msg = 'Shipping address updated successfully';
				}
				$returnArr['status_code'] = 1;
				$returnArr['response'] = $lg_err_msg;
            }
            if ($primary == 'Yes') {
                $condition = array('id !=' => $shipID, 'user_id' => $UserId);
                $dataArr = array('primary' => 'No');
                $this->mobile_model->update_details(SHIPPING_ADDRESS, $dataArr, $condition);
            } else {
                $condition = array('primary' => 'Yes', 'user_id' => $UserId);
                $checkPrimary = $this->mobile_model->get_all_details(SHIPPING_ADDRESS, $condition);
                if ($checkPrimary->num_rows() == 0) {
                    $condition = array('id' => $shipID, 'user_id' => $UserId);
                    $dataArr = array('primary' => 'Yes');
                    $this->mobile_model->update_details(SHIPPING_ADDRESS, $dataArr, $condition);
                }
            }
        }
		echo json_encode($returnArr);
    }

	public function changePrimaryAddress(){
		$returnArr['status'] = 0;
		$returnArr['response'] = "";
		$UserId = $_POST['UserId'];
		if($UserId != ""){
			$shipId = $_POST['shipId'];
			if($shipId != ""){
	            $condition = array('id' => $shipId, 'user_id' => $UserId);
				$shippingAddrDetails = $this->mobile_model->get_all_details(SHIPPING_ADDRESS, $condition);
				if($shippingAddrDetails->num_rows() == 1){
					$checkCondition = array('user_id' => $UserId, 'primary' => 'Yes');
					$oldDefaultAddress = $this->mobile_model->get_all_details(SHIPPING_ADDRESS, $checkCondition);
					if($oldDefaultAddress->num_rows() == 1){
						if($oldDefaultAddress->row()->id != $shipId){
							$dataArr = array('primary'=> 'No');
							$changeCondition = array('id' => $oldDefaultAddress->row()->id ,'user_id' => $UserId, 'primary' => 'Yes');
							$this->user_model->update_details(SHIPPING_ADDRESS, $dataArr, $changeCondition);
							$excludeArr = array('UserId','shipId');
							$dataArr = array('primary' => 'Yes');
							$this->mobile_model->commonInsertUpdate(SHIPPING_ADDRESS, 'update', $excludeArr, $dataArr, $condition);
							$returnArr['status'] = 1;
							$message = "Success";
							if($this->lang->line('json_success') != ""){
								$message = stripslashes($this->lang->line('json_success'));
							}
							$returnArr['response'] = $message;
						}else{
							$returnArr['response'] = "This is your primary address";
						}
					}else{
						$returnArr['response'] = "Something wrong with Shipping Address";
					}
				}else{
					$returnArr['response'] = "Something wrong with Shipping Address";
				}
			}else{
				$returnArr['response'] = "ShipId Null";
			}
		}else{
			$returnArr['response'] = "UserId Null";
		}
		echo json_encode($returnArr);
	}
	/*
	 	!! >>>>>>>>>>>> Approve Product Comments <<<<<<<<<<<< !!
	*/
	public function ApproveProductComment() {
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$cid = $_POST['commentId'];
		$product_id = $_POST['sellerProductId'];
		$user_id = $_POST['UserId'];
		if ($user_id != '') {
			if($cid != "" && $product_id != ""){
				$this->product_model->update_details(PRODUCT_COMMENTS, array('status' => 'Active'), array('id' => $cid));
				$datestring = "%Y-%m-%d %h:%i:%s";
				$time = time();
				$createdTime = mdate($datestring, $time);
				$this->product_model->commonDelete(NOTIFICATIONS, array('comment_id' => $cid));
				$actArr = array(
					'activity' => 'comment',
					'activity_id' => $product_id,
					'user_id' => $user_id,
				//	'activity_ip' => $this->input->ip_address(),
					'comment_id' => $cid,
					'created' => $createdTime
				);
				$this->product_model->simple_insert(NOTIFICATIONS, $actArr);
				$this->send_comment_noty_mail($product_id, $cid);
				$returnArr['status'] = 1;
				$returnArr['response'] = 'Comment Successfully Approved';
			}else{
				$returnArr['response'] = 'Some Parameters are Missing !!';
			}
		}else{
			$returnArr['response'] = 'UserId Null !!';
		}
		echo json_encode($returnArr);
	}

	/*
	 	!! >>>>>>>>>>>> Delete Product Comments <<<<<<<<<<<< !!
	*/
	public function deleteProductComment() {
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = $_POST['UserId'];
        if ($UserId != '') {
            $cid = $_POST['commentId'];
			if($cid != ""){
				$this->product_model->commonDelete(PRODUCT_COMMENTS, array('id' => $cid));
	            $returnArr['status'] = 1;
				$returnArr['response'] = 'Comment Successfully Deleted';
			}else{
				$returnArr['response'] = 'CommentId Null !!';
			}
        }else{
			$returnArr['response'] = 'UserId Null !!';
		}
        echo json_encode($returnArr);
	}

	public function send_comment_noty_mail($cmtID = '0', $pid = '0') {
		if ($cmtID != '0' && $pid != '0') {
			$productUserDetails = $this->product_model->get_product_full_details($pid);
			if ($productUserDetails->num_rows() == 1) {
				$emailNoty = explode(',', $productUserDetails->row()->email_notifications);
				if (in_array('comments', $emailNoty)) {
					$commentDetails = $this->product_model->view_product_comments_details('where c.id=' . $cmtID);
					if ($commentDetails->num_rows() == 1) {
						if ($productUserDetails->prodmode == 'seller') {
							$prodLink = base_url() . 'things/' . $productUserDetails->row()->id . '/' . url_title($productUserDetails->row()->product_name, '-');
						} else {
							$prodLink = base_url() . 'user/' . $productUserDetails->row()->user_name . '/things/' . $productUserDetails->row()->seller_product_id . '/' . url_title($productUserDetails->row()->product_name, '-');
						}
						$newsid = '8';
						$template_values = $this->order_model->get_newsletter_template_details($newsid);
						$adminnewstemplateArr = array('email_title' => $this->config->item('email_title'), 'logo' => $this->data['logo'], 'full_name' => $commentDetails->row()->full_name, 'product_name' => $productUserDetails->row()->product_name, 'user_name' => $commentDetails->row()->user_name);
						extract($adminnewstemplateArr);
						$subject = $template_values['news_subject'];
						$message .= '<!DOCTYPE HTML>
							<html>
							<head>
							<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
							<meta name="viewport" content="width=device-width"/>
							<title>' . $template_values['news_subject'] . '</title>
							<body>';
						include('./newsletter/registeration' . $newsid . '.php');
						$message .= '</body>
							</html>';
						if ($template_values['sender_name'] == '' && $template_values['sender_email'] == '') {
							$sender_email = $this->data['siteContactMail'];
							$sender_name = $this->data['siteTitle'];
						} else {
							$sender_name = $template_values['sender_name'];
							$sender_email = $template_values['sender_email'];
						}
						$email_values = array('mail_type' => 'html',
							'from_mail_id' => $sender_email,
							'mail_name' => $sender_name,
							'to_mail_id' => $productUserDetails->row()->email,
							'subject_message' => $subject,
							'body_messages' => $message
						);
						$email_send_to_common = $this->product_model->common_email_send($email_values);
					}
				}
			}
		}
	}

	public function send_comment_noty_mail_to_admin($cmtID = '0', $pid = '0') {
		if ($cmtID != '0' && $pid != '0') {
			$productUserDetails = $this->product_model->get_product_full_details($pid);
			if ($productUserDetails->num_rows() == 1) {
				$commentDetails = $this->product_model->view_product_comments_details('where c.id=' . $cmtID);
				if ($commentDetails->num_rows() == 1) {
					if ($productUserDetails->prodmode == 'seller') {
						$prodLink = base_url() . 'things/' . $productUserDetails->row()->id . '/' . url_title($productUserDetails->row()->product_name, '-');
					} else {
						$prodLink = base_url() . 'user/' . $productUserDetails->row()->user_name . '/things/' . $productUserDetails->row()->seller_product_id . '/' . url_title($productUserDetails->row()->product_name, '-');
					}

					$newsid = '20';
					$template_values = $this->order_model->get_newsletter_template_details($newsid);
					$adminnewstemplateArr = array('email_title' => $this->config->item('email_title'), 'logo' => $this->data['logo'], 'full_name' => $commentDetails->row()->full_name, 'product_name' => $productUserDetails->row()->product_name, 'user_name' => $commentDetails->row()->user_name);
					extract($adminnewstemplateArr);
					$subject = $template_values['news_subject'];

					$message .= '<!DOCTYPE HTML>
							<html>
							<head>
							<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
							<meta name="viewport" content="width=device-width"/>
							<title>' . $template_values['news_subject'] . '</title>
							<body>';
					include('./newsletter/registeration' . $newsid . '.php');

					$message .= '</body>
							</html>';

					if ($template_values['sender_name'] == '' && $template_values['sender_email'] == '') {
						$sender_email = $this->data['siteContactMail'];
						$sender_name = $this->data['siteTitle'];
					} else {
						$sender_name = $template_values['sender_name'];
						$sender_email = $template_values['sender_email'];
					}

					$email_values = array('mail_type' => 'html',
						'from_mail_id' => $sender_email,
						'mail_name' => $sender_name,
						'to_mail_id' => $this->data['siteContactMail'],
						'subject_message' => $subject,
						'body_messages' => $message
					);
					$email_send_to_common = $this->product_model->common_email_send($email_values);
				}
			}
		}
	}



	/*
	||  >>>>>>>>>>>> User purchases <<<<<<<<<<<< ||
	*/
	public function userPurchases() {
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = $_POST['UserId'];
		if($UserId != " "){
			$user_Detail = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
			if($user_Detail->num_rows() == 1){
				$purchasesList = $this->mobile_model->get_purchase_details2($UserId);
				$purchaseLists = array();
				if($purchasesList->num_rows() > 0){
					foreach($purchasesList->result() as $purchase){
						if($purchase->image != ""){
							$images = explode(',',$purchase->image);
							if($images[0] != ""){
								$imagePath = base_url().'images/product/'.$images[0];
							}
						}
						$purchaseLists[]=array(
															'id'=> $purchase->id,
															'UserGroup' => $user_Detail->row()->group,
															'product_id'=>$purchase->product_id,
															'productName' => $purchase->product_name,
															'productImage' => $imagePath,
															'sell_id'=>$purchase->sell_id,
															'user_id'=>$purchase->user_id,
															'invoice' => $purchase->dealCodeNumber,
															'orderDate'=>$purchase->modified,
															'paymentType'=>$purchase->payment_type,
															'paymentStatus'=>$purchase->status,
															'total'=>$purchase->total,
															'shippingStatus'=>$purchase->shipping_status,
													);
					}
					$returnArr['status'] = 1;
					$message = "Success";
					if($this->lang->line('json_success') != ""){
						$message = stripslashes($this->lang->line('json_success'));
					}
					$returnArr['response'] = $message;
				}else{
					$returnArr['response'] = "No purchase Details Found !!";
				}
			}else{
				$returnArr['purchaseLists'] = "Such User Not Found !!";
			}
			$returnArr['purchaseLists'] = $purchaseLists;
		}else{
			$returnArr['response'] = 'UserId Null';
		}
		echo json_encode($returnArr);
	}




	/*
	||  >>>>>>>>>>>> User purchases <<<<<<<<<<<< ||
	*/
	public function userProfileSettings() {
		$returnArr['status_code'] = 0;
		$returnArr['response'] = '';
		$UserId = $_POST['UserId'];
		if($UserId != ""){
			$user_Detail = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
			$UserDetails = array();
			if($user_Detail->num_rows() > 0){
				$userImg =  base_url().'images/users/user-thumb1.png';
				if ($user_Detail->row()->thumbnail != ''){
					$userImg = base_url().'images/users/'.$user_Detail->row()->thumbnail;
				}
				$userType = "Normal";
				if($user_Detail->row()->store_payment == "Paid"){
					$userType = "Upgraded";
				}
				$verified = 0;
				if($user_Detail->row()->is_verified == "Yes"){
					$verified = 1;
				}
				$UserDetails = array(
											'id' => $user_Detail->row()->id,
											'full_name' => $user_Detail->row()->full_name,
											'user_name' => $user_Detail->row()->user_name,
											'web_url' => $user_Detail->row()->web_url,
											'location' => $user_Detail->row()->location,
											'twitter' => $user_Detail->row()->twitter,
											'facebook' => $user_Detail->row()->facebook,
											'google' => $user_Detail->row()->google,
											'birthday' => $user_Detail->row()->birthday,
											'about' => $user_Detail->row()->about,
											'email' => $user_Detail->row()->email,
											'age' => $user_Detail->row()->age,
											'gender' => $user_Detail->row()->gender,
											'userImg' => $userImg,
											'userType' => $userType,
											'userGroup' => $user_Detail->row()->group,
											'emailVerified' => $verified
										);
				$returnArr['status_code'] = 1;
				$returnArr['response'] = 'Success';
				$returnArr['UserDetails'] = $UserDetails;
			}else{
				$returnArr['response'] = 'User Not Found !! ';
			}
		}else{
			$returnArr['response'] = 'UserId Null !! ';
		}
		echo json_encode($returnArr);
	}


	/*
	* !! >>>>>>>>>>>>> This Function to Upload User Product Picture    <<<<<<<<<<<< !!
	*/
	public function UploadUserImage(){
		$responseArr['status'] = 0;
		$responseArr['response'] = '';
		$UserId = $_POST['UserId'];
		if($UserId !=''){
			if (!empty($_POST['UserProfImage'])) {
				$imgPath ='images/users/temp/';
				$img = $_POST['UserProfImage'];
				$imgName = time(). ".jpg";
				$imageFormat = array('data:image/jpeg;base64', 'data:image/png;base64', 'data:image/jpg;base64', 'data:image/gif;base64');
				$img = str_replace($imageFormat, '', $img);
				$data = base64_decode($img);
				$image = @imagecreatefromstring($data);
				if ($image !== false) {
					$uploadPath = $imgPath . $imgName;
					imagejpeg($image, $uploadPath, 100);
					imagedestroy($image);
				} else {
					$responseArr['response'] ='An error occurred2';
				}
				if (isset($uploadPath)) {
					/* Creating Thumbnail image with the size of 100 X 100 */
					$filename = base_url($uploadPath);
					list($width, $height) = getimagesize($filename);
					$newwidth = 100;
					$newheight = 100;
					//$thumbImage = @imagecreatetruecolor($newwidth, $newheight);
					$sourceImage = @imagecreatefromjpeg($filename);
					imagecopyresized($thumbImage, $sourceImage, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
					// if ($thumbImage !== false) {
					// 	/* Not uploading to thumb folder */
					// 	$thumbImgPath = 'images/users/thumb/';
					// 	$thumpUploadPath = $thumbImgPath . $imgName;
					// 	imagejpeg($thumbImage, $thumpUploadPath, 100);
					// }
				}else {
					$responseArr['response'] ='Invalid File';
				}
			$new_name = $imgName;
			@copy("./images/users/temp/".$imgName, './images/users/'.$new_name);
			@copy("./images/users/temp/".$imgName, './images/users/thumb/'.$new_name);
			//$this->imageResizeWithSpace(210, 210, $new_name, './images/users/thumb/');
			//$this->imageResizeWithSpace(600, 600, $new_name, './images/users/');

			$dataArr = array('thumbnail'=> $new_name);
			$condition = array('id' => $UserId);
			$this->user_model->update_details(USERS, $dataArr, $condition);

			$dir = getcwd().DIRECTORY_SEPARATOR ."images".DIRECTORY_SEPARATOR ."users".DIRECTORY_SEPARATOR."temp".DIRECTORY_SEPARATOR ;
			$interval = strtotime('-24 hours');
			foreach (glob($dir."*.*") as $file){
				if (filemtime($file) <= $interval ){
					if(!is_dir($file) ){
						unlink($file);
					}
				}
			}
			$responseArr['status'] = 1;
			$responseArr['response'] = $imgName;
			}else{
				$responseArr['response'] = "Please Upload Image";
			}
		}else{
			$responseArr['response'] = 'UserId Null !!';
		}
		$json_encode = json_encode($responseArr, JSON_PRETTY_PRINT);
		echo $json_encode;
	}

	/*
	* !! >>>>>>>>>>>>> This Function to Update User Profile    <<<<<<<<<<<< !!
	*/

	public function updateUserProfile() {
        $inputArr = array();
		$responseArr['status'] = 0;
		$responseArr['response'] = '';
		$UserId = $_POST['UserId'];
		if ($UserId == '') {
            $responseArr['response'] = 'UserId Null !!';
        } else {
                    $condition = array('id' => $UserId, 'status' =>'Active');
                    $duplicateuser = $this->user_model->get_all_details(USERS, $condition);
		            if ($duplicateuser->num_rows() > 0) {

		                $birthday = date('Y-m-d',strtotime($this->input->post('birthdate')));
		                $excludeArr = array(
		                					'birthdate',
		                					'UserId',
		                					'userImg'
		                					);
		                $inputArr = array(
		                	                          'birthday'=>$birthday,
		                	                          'web_url'=>$this->input->post('web_url'),
		                	                          'twitter'=>$this->input->post('twitter'),
		                	                          'facebook'=>$this->input->post('facebook'),
		                	                          'google'=>$this->input->post('google'),
		                	                          'about'=>$this->input->post('about'),
		                	                          'gender'=>$this->input->post('gender'),
		                	                          'thumbnail'=>$this->input->post('userImg'),
		                	                          'full_name'=>$this->input->post('full_name'),
		                	                          'age'=>$this->input->post('age')
		                	                          );
		                $condition = array('id' => $UserId);
		                $this->user_model->commonInsertUpdate(USERS, 'update', $excludeArr, $inputArr, $condition);
		                $this->setErrorMessage('success', $lg_err_msg);
		                $responseArr['status'] = '1';
						$responseArr['response'] = 'Profile Updated Successfully';

                    } else {

                        $responseArr['response'] = 'Invalid User ';
                    }

            if ($update == '1') {

            }
        }
		echo json_encode($responseArr);
    }

	/*
	* !! >>>>>>>>>>>>>  User Preferencs    <<<<<<<<<<<< !!
	*/
	public function userPreferencesSettings() {
		$responseArr['status'] = 0;
		$responseArr['response'] = '';
		$UserId = $_POST['UserId'];
        if ($UserId== '') {
        	$responseArr['response'] = 'UserId Null !!';
        }else {
			$user_Detail = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
			if($user_Detail->num_rows() == 1){
				$activeLangList = array();
				foreach($this->data['activeLgs'] as $actLangs){
						if($actLangs['lang_code'] == $user_Detail->row()->language){
							$languageName = $actLangs['name'];
						}
						$selected = 0;
						if($user_Detail->row()->language == $actLangs['lang_code']){
							$selected = 1;
						}
						$activeLangList[] = array(
															'name' => $actLangs['name'],
															'langCode' => $actLangs['lang_code'],
															'selected' => $selected,
														);
				}
				$responseArr['status'] = 1;
				$responseArr['response'] = 'Success';
				$responseArr['language'] = $activeLangList;
				$responseArr['visibility'] = $user_Detail->row()->visibility;
				$responseArr['displayLists'] = $user_Detail->row()->display_lists;
			}else{
				$responseArr['response'] = 'No Such User Found !!';
			}
        }
		echo json_encode($responseArr);
    }

	/*
	* !! >>>>>>>>>>>>>  Update User Preferencs    <<<<<<<<<<<< !!
	*/
	public function updateUserPreferences() {
		$responseArr['status'] = 0;
		$responseArr['response'] = '';
		$UserId = $_POST['UserId'];
        if ($UserId== '') {
        	$responseArr['response'] = 'UserId Null !!';
        } else {
			$excludeArr = array('UserId');
            $this->user_model->commonInsertUpdate(USERS, 'update', $excludeArr, array(), array('id' => $UserId));
			$responseArr['status'] = 1;
			$responseArr['response'] = 'success';
        }
		echo json_encode($responseArr);
    }

	/*
	* !! >>>>>>>>>>>>>  Update User Password    <<<<<<<<<<<< !!
	*/
	public function changeUserPassword() {
		$responseArr['status'] = 0;
		$responseArr['response'] = '';
		$UserId = $_POST['UserId'];
		 $pwd = $_POST['password'];
		 $oldpassword = $_POST['oldpassword'];
         $cfmpwd = $_POST['confirmpass'];
        if ($UserId == '' && $pwd == '' && $cfmpwd == '' && $oldpassword == '') {
            $responseArr['response'] = ' Parameters Missing';
        } else {
        	$condition = array('id' => $UserId, 'status' =>'Active');
            $duplicateuser = $this->user_model->get_all_details(USERS, $condition);

        	if($duplicateuser->num_rows() > 0 ){
            if ($pwd != '' && $cfmpwd != '') {
            	if(strlen($pwd) > 5){

                if ($pwd == $cfmpwd) {
                	$conditionpas = array('id' => $UserId, 'password' =>md5($oldpassword));
                    $duplicatepass = $this->user_model->get_all_details(USERS, $conditionpas);
                    if($duplicatepass->num_rows() > 0 ){
                    $dataArr = array('password' => md5($pwd));
                    $condition = array('id' => $UserId);
                    $this->user_model->update_details(USERS, $dataArr, $condition);
                    if ($this->lang->line('pwd_cge_succ') != ''){
                        $lg_err_msg = $this->lang->line('pwd_cge_succ');
					}
                    else{
                        $lg_err_msg = 'Password changed successfully';
					}
                    //$this->setErrorMessage('success', $lg_err_msg);
					$responseArr['status'] = 1;
					$responseArr['response'] = $lg_err_msg;

				}else{
					 $responseArr['response']= 'Your Password Wrong ';
				}

                }else {
                    if ($this->lang->line('pwd_donot_match') != ''){
                        $lg_err_msg = $this->lang->line('pwd_donot_match');
					}
                    else{
                        $lg_err_msg = 'Passwords does not match';
					}
                    //$this->setErrorMessage('error', $lg_err_msg);
					$responseArr['response'] = $lg_err_msg;
                }
            }else {
                $lg_err_msg = 'Password Should Have Atleast Six Characters';
                //$this->setErrorMessage('error', $lg_err_msg);
				$responseArr['response'] = $lg_err_msg;
            }
        }else{
        	   $lg_err_msg = 'password or cinfirmpassword is empty';
                //$this->setErrorMessage('error', $lg_err_msg);
				$responseArr['response'] = $lg_err_msg;
        }
         }else{
         	$responseArr['response'] = "Invalid User ";
         }
        }
		echo json_encode($responseArr);
    }
	/*
	* !! >>>>>>>>>>>>>  User Notifications    <<<<<<<<<<<< !!
	*/
	public function UserNotifications() {
		$responseArr['status'] = 0;
		$responseArr['response'] = '';
		$UserId = $_POST['UserId'];
		$UserEmailNotifications  = array();
		$UserNotifications = array();
		$siteUpdates = array();
        if ($UserId== '') {
        	$responseArr['response'] = 'UserId Null !!';
        } else {
			$userDetails = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
			if($userDetails->num_rows() == 1){

				$email_Notifications = explode(',', $userDetails->row()->email_notifications);
				$emailNotfication = array();
				$liked = LIKED_BUTTON;
				if (is_array($email_Notifications)) {
					$emailNotifications = $email_Notifications;
				}
			 	if($this->lang->line('notify_some_follu') != '') {
					$emailFollow = stripslashes($this->lang->line('notify_some_follu'));
				} else{
					$emailFollow = "When someone follows you";
				}
				if ($this->lang->line('notify_comm_things') != '') {
					$emailLiked = stripslashes($this->lang->line('notify_comm_things')).' '.$liked;
				} else{
					$emailLiked = "When someone comments on a thing you ".' '.$liked ;
				}
				if ($this->lang->line('notify_thing_feature') != '') {
					$emailNoti = stripslashes($this->lang->line('notify_thing_feature'));
				} else{
					$emailNoti = "When one of your things is featured";
				}
				if ($this->lang->line('cmt_on_ur_thing') != '') {
					$emailComts =  stripslashes($this->lang->line('cmt_on_ur_thing'));
				} else{
					$emailComts =  "When someone comments on your thing";
				}
				if ($this->lang->line('cmt_on_ur_thing') != '') {
					$emailDash =  stripslashes($this->lang->line('cmt_on_ur_thing_seller_dashboard'));
				} else{
					$emailDash =  "When someone comments on your seller dashboard";
				}
				$UserNotifications['review-comments'] = $emailDash;
				$UserNotifications['comments'] = $emailComts;
				$UserNotifications['following'] = $emailFollow;
				$UserNotifications['comments_on_fancyd'] = $emailLiked;
				$UserNotifications['featured'] = $emailNoti;
				foreach($UserNotifications as $key => $value){
					$checked = 0;
					if (in_array($key, $emailNotifications)) $checked = 1;
						$UserEmailNotifications[]=array(
																			'title' => $UserNotifications[$key],
																			'key' => $key,
																			'status' => $checked
																		);
				}
				$notificationArray = array();
				$noty = explode(',', $userDetails->row()->notifications);
				if (is_array($noty)) {
					$notifications = $noty;
				}
				if ($this->lang->line('notify_some_follu') != '') {
				   $emailFollow = stripslashes($this->lang->line('notify_some_follu'));
			   } else{
				   $emailFollow = "When someone follows you";
			   }
			   if ($this->lang->line('notify_comm_things') != '') {
				   $noti_commts = stripslashes($this->lang->line('notify_comm_things')).' '.LIKED_BUTTON;
			   } else{
				   $noti_commts = "When someone comments on a thing you".' '.LIKED_BUTTON;
			   }
			   if ($this->lang->line('notify_when_some') != '') {
				   $notiFancy = stripslashes($this->lang->line('notify_when_some')).' '.LIKE_BUTTON.' '.stripslashes($this->lang->line('notify_ur_thing'));
			   } else{
				   $notiFancy = "When someone".' '.LIKE_BUTTON.' '. "one of your things";
			   }
			   if ($this->lang->line('notify_thing_feature') != '') {
				   $noitFea = stripslashes($this->lang->line('notify_thing_feature'));
			   } else{
				   $noitFea = "When one of your things is featured";
			   }
			   if ($this->lang->line('cmt_on_ur_thing') != '') {
				   $notiComts = stripslashes($this->lang->line('cmt_on_ur_thing'));
			   } else{
				   $notiComts = "When someone comments on your thing";
			   }
			   if ($this->lang->line('cmt_on_seller_discussion') != '') {
				   $notiRevCmts = stripslashes($this->lang->line('cmt_on_seller_discussion'));
			   } else{
				   $notiRevCmts = "When someone comments on a product's seller discussion";
			   }
			   if ($this->lang->line('cmt_on_seller_purchase') != '') {
				   $notiPurchse = stripslashes($this->lang->line('cmt_on_seller_purchase'));
			   } else{
				   $notiPurchse = "When someone Purchase your product";
			   }
				$notificationArray['wmn-follow'] = $emailFollow;
				$notificationArray['wmn-comments_on_fancyd'] = $noti_commts;
				$notificationArray['wmn-fancyd'] = $notiFancy;
				$notificationArray['wmn-featured'] = $noitFea;
				$notificationArray['wmn-comments'] = $notiComts;
				$notificationArray['wmn-review-comments'] = $notiRevCmts;
				$notificationArray['wmn-purchased'] = $notiPurchse;
				foreach($notificationArray as $key => $value){
					$checked = 0;
					if (in_array($key, $notifications)) $checked = 1;
						$User_Notifications[]=array(
																			'title' => $notificationArray[$key],
																			'key' => $key,
																			'status' => $checked
																		);
				}
				if ($this->lang->line('notify_send_news') != '') {
					$sndUp = stripslashes($this->lang->line('notify_send_news')).' '.$this->data['siteTitle'];
				} else{
					$sndUp = "Send news about".' '.$this->data['siteTitle'];
				}
				$siteUpdatesNews = 0;
				if ($userDetails->row()->updates == '1')$siteUpdatesNews = 1;
					$siteUpdates[] = array(
														'title' => $sndUp,
														'key' => 'updates',
														'status' => $siteUpdatesNews
												);

				if ($this->lang->line('referrals_email') != '') {
 				   $email = stripslashes($this->lang->line('referrals_email'));
 			    }else{
 				   $email = "Email";
 			    }
				if ($this->lang->line('referrals_notification') != '') {
 				   $notification = stripslashes($this->lang->line('referrals_notification'));
 			    }else{
 				   $notification = "Notification ";
 			    }
				if ($this->lang->line('notify_update') != '') {
 				   $update = stripslashes($this->lang->line('notify_update'));
 			    }else{
 				   $update = "Updates";
 			    }
				$title = array($email,$notification,$update);
				for($i=0;$i<count($title);$i++){
					$notificationHeadings[] = array("title" => $title[$i]);
				}
				$responseArr['status'] = 1;
				$responseArr['response'] = 'Success';
				$responseArr['Email'] = $UserEmailNotifications;
				$responseArr['Notifications'] = $User_Notifications;
				$responseArr['notificationHeadings'] = $notificationHeadings;
				$responseArr['Updates'] = $siteUpdates;
			}else{
				$responseArr['response'] = 'No such User Found !!';
			}
		}
		echo json_encode($responseArr);
    }

	/*
	* !! >>>>>>>>>>>>>  Update User Notifications    <<<<<<<<<<<< !!
	*/
	public function updateUserNotifications() {
		$returnArr['status'] = '0';
		$returnArr['response'] = '';
		$UserId = $_POST['UserId'];
        if ($UserId == '') {
            $returnArr['response'] = 'UserId Null !!';
        } else {
			$this->data['userDetails'] = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
			if($this->data['userDetails']->num_rows() == 1){
				$email = $_POST['email'];
				$notification = $_POST['notification'];
				$emailStr = $email;
			 	$notyStr = $notification;
	            $updates = $_POST['updates'];
	            $updates = ($updates == '') ? '0' : '1';
	            $dataArr = array(
	                'email_notifications' => $emailStr,
	                'notifications' => $notyStr,
	                'updates' => $updates
	            );
	            $condition = array('id' => $UserId);
	            $this->user_model->update_details(USERS, $dataArr, $condition);
	            if ($updates == 1) {
	                $checkEmail = $this->user_model->get_all_details(SUBSCRIBERS_LIST, array('subscrip_mail' => $this->data['userDetails']->row()->email));
	                if ($checkEmail->num_rows() == 0) {
	                    $this->user_model->simple_insert(SUBSCRIBERS_LIST, array('subscrip_mail' => $this->data['userDetails']->row()->email, 'active' => 1, 'status' => 'Active'));
	                }
	            } else {
	                $this->user_model->commonDelete(SUBSCRIBERS_LIST, array('subscrip_mail' => $this->data['userDetails']->row()->email));
	            }
	            if ($this->lang->line('noty_sav_succ') != ''){
	                $lg_err_msg = $this->lang->line('noty_sav_succ');
				}else{
	                $lg_err_msg = 'Notifications settings saved successfully';
				}
				$returnArr['status'] = '1';
				$returnArr['response'] = $lg_err_msg;
			}else{
				$returnArr['response'] = "No Such User Found !!";
			}
        }
		echo json_encode($returnArr);
    }

	/*
	* !! >>>>>>>>>>>>>  Manage FancyBox    <<<<<<<<<<<< !!
	*/

	public function manageFancyybox(){
		$responseArr['status'] = 0;
		$responseArr['response'] = '';
		$UserId = $_POST['UserId'];
		if($UserId == ""){
			$responseArr['response'] = 'UserId Null !!';
		}else{
			$userDetails = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
	        $manageFancyybox = $this->mobile_model->get_subscriptions_list($UserId);
			if($manageFancyybox->num_rows() > 0){
					foreach( $manageFancyybox->result() as $subscribeRow){
			 		   $subscribeRowArr[]=array(
			 												   'name'=>$subscribeRow->name,
			 												   'created'=>$subscribeRow->created,
			 												   'total'=>$subscribeRow->total,
			 												   'invoice_no'=>$subscribeRow->invoice_no
			 											   );
			 	   }
				$responseArr['status'] = 1;
		   		$responseArr['response'] = 'Success';
			}else{
				$responseArr['response'] = 'No Subscriptions List Avaloable';
			}
			if($this->lang->line('purchases__invoice') != '') { $value1 =  stripslashes($this->lang->line('purchases__invoice')); } else $value1 =  "Invoice";
			if($this->lang->line('manage_subsname') != '') { $value2 =  stripslashes($this->lang->line('manage_subsname')); } else $value2 =  "Subscription Name";
			if($this->lang->line('purchases_total') != '') { $value3 =  stripslashes($this->lang->line('purchases_total')); } else $value3 =  "Total";
			if($this->lang->line('order_date') != '') { $value4 =  stripslashes($this->lang->line('order_date')); } else $value4 =  "Date";
			$subscribeTabelHead[] = array(
															'value1' => $value1,
															'value2' => $value2,
															'value3' => $value3,
															'value4' => $value4,
														);
			$responseArr['subscribeTabelHead'] = $subscribeTabelHead;
			$responseArr['subscribeList'] = $subscribeRowArr;
		}
		echo json_encode($responseArr);
    }

	/*
	* !! >>>>>>>>>>>>>  Shipping Settings    <<<<<<<<<<<< !!
	*/

	public function shippingSettings() {
		$responseArr['status'] = 0;
		$responseArr['response'] = '';
		$UserId = $_POST['UserId'];
		if($UserId == ""){
			$responseArr['response'] = 'UserId Null !!';
		}else{
            $countryList = $this->mobile_model->get_all_details(COUNTRY_LIST, array(), array(array('field' => 'name', 'type' => 'asc')));
            $shippingList = $this->mobile_model->get_all_details(SHIPPING_ADDRESS, array('user_id' => $UserId));
            foreach( $shippingList->result() as $shipping){
			   $shippinglists[]=array(
											   'full_name'=>$shipping->full_name,
											   'nick_name'=>$shipping->nick_name,
											   'address1'=>$shipping->address1,
											   'address2'=>$shipping->address2,
											   'city'=>$shipping->city,
											   'district'=>$shipping->district,
											   'state'=>$shipping->state,
											   'country'=>$shipping->country,
											   'postal_code'=>$shipping->postal_code,
											   'phone'=>$shipping->phone,
											   'primary'=>$shipping->primary
										   );
		   }
		   if($this->lang->line('shipping_default') != '') { $value1 = stripslashes($this->lang->line('shipping_default')); } else $value1 = "Default";
		   if($this->lang->line('shipping_nickname') != '') { $value2 = stripslashes($this->lang->line('shipping_nickname')); } else $value2 = "Nick Name";
		   if($this->lang->line('shipping_address_comm') != '') { $value3 = stripslashes($this->lang->line('shipping_address_comm')); } else $value3 = "Address";
		   if($this->lang->line('shipping_phone') != '') { $value4 = stripslashes($this->lang->line('shipping_phone')); } else $value4 = "Phone";
		   if($this->lang->line('purchases_option') != '') { $value5 = stripslashes($this->lang->line('purchases_option')); } else $value5 = "Option";
			 $shippinglistsTableHead[]=array(
																 'value1' => $value1,
																 'value2' => $value2,
																 'value3' => $value3,
																 'value4' => $value4,
																 'value5' => $value5,
															 );
		  $responseArr['status'] = 1;
		  $responseArr['response'] = 'Success';
		  $responseArr['shippinglistsTableHead'] = $shippinglistsTableHead;
		  $responseArr['shippinglists'] = $shippinglists;
	   }
		echo json_encode($responseArr);
    }

	/*
	* !! >>>>>>>>>>>>>  Gift Cards <<<<<<<<<<<< !!
	*/
	public function userGiftcards() {
		$responseArr['status'] = 0;
		$responseArr['response'] = '';
		$UserId= $_POST['UserId'];
		$giftcardslists = array();
		if($UserId == ""){
			$responseArr['response'] = 'UserId Null !!';
		}else{
			$userDetails = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
			$giftcardsList =$this->data['giftcardsList'] = $this->user_model->get_gift_cards_list($userDetails->row()->email);
			 foreach( $giftcardsList->result() as $giftcards) {
				$status = $giftcards->card_status;
				if ($status == 'not used') {
					$expDate = strtotime($giftcards->expiry_date);
					if ($expDate < time()) {
						$status = 'expired';
					}
				}
			   $giftcardslists[]=array(
												   'code'=>$giftcards->code,
												   'recipient_name'=>$giftcards->recipient_name,
												   'sender_mail'=>$giftcards->sender_mail,
												   'sender_name'=>$giftcards->sender_name,
												   'price_value'=>$giftcards->price_value,
												   'expiry_date'=>$giftcards->expiry_date,
												   'card_status'=>ucwords($status)
											   );
		   }
			if ($this->lang->line('giftcard_code') != '') {
				$value1 = stripslashes($this->lang->line('giftcard_code'));
				} else
				$value1 = "Code";
		   if ($this->lang->line('giftcard_sendername') != '') {
			   $value2 = stripslashes($this->lang->line('giftcard_sendername'));
		   } else
			   $value2 = "Sender Name";
		   if ($this->lang->line('giftcard_sender_mail') != '') {
			   $value3 = stripslashes($this->lang->line('giftcard_sender_mail'));
		   } else
			   $value3 = "Sender Mail";
		   if ($this->lang->line('giftcard_price') != '') {
			   $value4 = stripslashes($this->lang->line('giftcard_price'));
		   } else
			   $value4 = "Price";
		   if ($this->lang->line('giftcard_expireson') != '') {
			   $value5 = stripslashes($this->lang->line('giftcard_expireson'));
		   } else
			   $value5 = "Expires on";
		   if ($this->lang->line('giftcard_card_stats') != '') {
			   $value6 = stripslashes($this->lang->line('giftcard_card_stats'));
		   } else
			   $value6 = "Card Status";
			$giftcardslistsTableHead[] = array(
																	'value1' => $value1,
																	'value2' => $value2,
																	'value3' => $value3,
																	'value4' => $value4,
																	'value5' => $value5,
																	'value6' => $value6
																);
		   $responseArr['status'] = 1;
		   $responseArr['response'] = 'Success';
		   $responseArr['giftcardslistsTableHead'] = $giftcardslistsTableHead;
		   $responseArr['giftcardslists'] = $giftcardslists;
	   }
		echo json_encode($responseArr);
	}


	/*
	* !! >>>>>>>>>>>>> Purchase Invoice <<<<<<<<<<<< !!
	*/
	// public function viewPurchaseInvoice() {
	// 	$responseArr['status'] = 0;
	// 	$responseArr['response'] = '';
	// 	$UserId = $_POST['UserId'];
	// 	$dealCode = $_POST['dealCode'];
	// 	if($UserId == ""){
	// 		$responseArr['response'] = 'UserId Null !!';
	// 	}else{
	// 		if($dealCode == ""){
	// 			$responseArr['response'] = 'DealCode Number Null !!';
	// 		}else{
	//             $PrdList = $this->mobile_model->getPurchaseList($UserId, $dealCode);
	// 			$shipAddRess = $this->user_model->get_all_details(SHIPPING_ADDRESS, array('id' => $PrdList->row()->shippingid));
	// 			$ShippingBillingAddr= array(
	// 											            //'logo' => $this->data['logo'],
	// 											            //'meta_title' => $this->config->item('meta_title'),
	// 											            'ship_fullname' => stripslashes($shipAddRess->row()->full_name),
	// 											            'ship_address1' => stripslashes($shipAddRess->row()->address1),
	// 											            'ship_address2' => stripslashes($shipAddRess->row()->address2),
	// 											            'ship_city' => stripslashes($shipAddRess->row()->city),
	// 											            'ship_country' => stripslashes($shipAddRess->row()->country),
	// 											            'ship_state' => stripslashes($shipAddRess->row()->state),
	// 											            'ship_postalcode' => stripslashes($shipAddRess->row()->postal_code),
	// 											            'ship_phone' => stripslashes($shipAddRess->row()->phone),
	// 											            'bill_fullname' => stripslashes($PrdList->row()->full_name),
	// 											            'bill_address1' => stripslashes($PrdList->row()->address),
	// 											            'bill_address2' => stripslashes($PrdList->row()->address2),
	// 											            'bill_city' => stripslashes($PrdList->row()->city),
	// 											            'bill_country' => stripslashes($PrdList->row()->country),
	// 											            'bill_state' => stripslashes($PrdList->row()->state),
	// 											            'bill_postalcode' => stripslashes($PrdList->row()->postal_code),
	// 											            'bill_phone' => stripslashes($PrdList->row()->phone_no),
	// 											            'invoice_number' => $PrdList->row()->dealCodeNumber,
	// 											            'payment_type' => $PrdList->row()->payment_type,
	// 											            'invoice_date' => date("F j, Y g:i a", strtotime($PrdList->row()->created))
	// 											        );
	// 			$disTotal = 0;
	// 	        $grantTotal = 0;
	// 	        foreach ($PrdList->result() as $cartRow) {
	// 				if ($cartRow->image != '') {
	// 	                $InvImg = @explode(',', $cartRow->image);
	// 	            } else {
	// 	                $InvImg = @explode(',', $cartRow->old_image);
	// 	            }
	// 				$product_name = $cartRow->product_name;
	// 	            if ($product_name == '') {
	// 	                $product_name = $cartRow->old_product_name;
	// 	            }
	// 				if ($cartRow->attr_name != '' || $cartRow->attr_type) {
	// 	                $atr = '<br>' . $cartRow->attr_type . ' / ' . $cartRow->attr_name;
	// 	            } else if ($cartRow->old_attr_name != '') {
	// 	                $atr = '<br>' . $cartRow->old_attr_name;
	// 	            } else {
	// 	                $atr = '';
	// 	            }
	// 				if ($cartRow->product_type == 'physical') {
	// 	                $dwnldcode = 'Not applicable';
	// 	            } else {
	// 	                $dwnldcode = $cartRow->product_download_code;
	// 	            }
	// 				$unitPrice = ($cartRow->price * (0.01 * $cartRow->product_tax_cost)) + $cartRow->product_shipping_cost + $cartRow->price;
	// 				$uTot = $unitPrice * $cartRow->quantity;
	// 				$purchaseDetails[] = array(
	// 													'productImage' => base_url() . PRODUCTPATH . $InvImg[0],
	// 													'productName' => $product_name,
	// 													'productAttributes' => $atr,
	// 													'code' => $dwnldcode,
	// 													'quantity' => strtoupper($cartRow->quantity),
	// 													'unitPrice' => $this->data['currencySymbol'] . number_format($unitPrice, 2, '.', ''),
	// 													'subTotal' => $this->data['currencySymbol'] . number_format($uTot, 2, '.', '')
	// 												);
	// 				$grantTotal = $grantTotal + $uTot;
	// 			}
	// 			$private_total = $grantTotal - $PrdList->row()->discountAmount;
	// 	        $private_total = $private_total + $PrdList->row()->tax + $PrdList->row()->shippingcost;
	// 			$priceDetails[] = array(
	// 						'Total' =>  $this->data['currencySymbol'] . number_format($grantTotal, '2', '.', ''),
	// 						'discountAmount' => $this->data['currencySymbol'] . number_format($PrdList->row()->discountAmount, '2', '.', ''),
	// 						'shippingCost' => $this->data['currencySymbol'] . number_format($PrdList->row()->shippingcost, 2, '.', ''),
	// 						'shippingTax' => $this->data['currencySymbol'] . number_format($PrdList->row()->tax, 2, '.', ''),
	// 						'grandTotal' => $this->data['currencySymbol'] . number_format($private_total, '2', '.', '')
	// 						);
	// 		}
	// 		$responseArr['status'] = 1;
 // 		    $responseArr['response'] = 'Success';
 // 		    $responseArr['ShippingBillingAddress'] = $ShippingBillingAddr;
	// 		$responseArr['purchaseDetails'] = $purchaseDetails;
	// 		$responseArr['priceDetails'] = $priceDetails;
    //     }
	// 	 echo json_encode($responseArr);
    // }


	/*
	* !! >>>>>>>>>>>>> Update Buyer Status <<<<<<<<<<<< !!
	*/
	public function updateBuyerStatus() {
		$responseArr['status'] = 0;
		$responseArr['response'] = '';
		$user_id = $_POST['UserId'];
        if ($user_id == '') {
            $responseArr['response'] = 'UserId Null !!';
        } else {
            $seller_id = $_POST['SellId'];
            $invoice_id = $_POST['Invoice'];
			if($seller_id == "" || $invoice_id == ""){
				$responseArr['response'] = 'Some Paremters are Missing !!';
			}else{
				$Query = "select p.*,u.commision,u.cash_on_delivery from " . PAYMENT . " as p left join " . USERS . " as u on u.id=p.sell_id where p.dealCodeNumber = '" . $invoice_id . "'";
	            $seller_list = $this->user_model->ExecuteQuery($Query);
	            if ($seller_list->num_rows() > 0) {
	                $admin_commision = ($seller_list->row()->sumtotal * ($seller_list->row()->commision / 100));
	                $cod = $seller_list->row()->cash_on_delivery + $admin_commision;
	                $this->user_model->update_details(USERS, array('cash_on_delivery' => $cod), array('id' => $seller_id));
					$this->user_model->update_details(PAYMENT, array('status' => 'Paid'), array('user_id' => $user_id, 'sell_id' => $seller_id, 'dealCodeNumber' => $invoice_id));
					$responseArr['status'] = 1;
		            $responseArr['response'] = 'Success, Payment Confirmed';
	            }else{
					$responseArr['response'] = 'Invoice Number Not Found';
				}
			}
        }
		echo json_encode($responseArr);
    }

	/*
	* !! >>>>>>>>>>>>> Group Gifts <<<<<<<<<<<< !!
	*/
	public function GroupGifts() {
		$responseArr['status'] = 0;
		$responseArr['response'] = '';
		$user_id = $_POST['UserId'];
		$groupGiftList = array();
		$groupGiftPayment = array();
		if ($user_id ==''){
			$responseArr['response'] = 'UserId Null !!';
		}else {
			$groupgiftList = $this->groupgift_model->get_groupgift_list($user_id);
			if($groupgiftList->num_rows() >0){
			   foreach($groupgiftList->result() as $_groupgiftlist){
					$current_date = date('m-d-Y');
					$date = $_groupgiftlist->created;
					$date = strtotime($date);
					$date = strtotime("+".$this->config->item('expiry_days')." day", $date);
					$expired_date = date('m-d-Y', $date);
					if($expired_date < $current_date && $_groupgiftlist->status=='Active'){
						$this->groupgift_model->update_details(GROUP_GIFTS,array('status'=>'Unsuccessful'),array('id'=>$_groupgiftlist->id));
					}
				   $this->data['contributeDetails'][$_groupgiftlist->id] = $this->groupgift_model->get_all_details(GROUPGIFT_PAYMENT,array('groupgift_id'=>$_groupgiftlist->id));
					if($_groupgiftlist->status == "Cancelled") {
					   $Status =  "Canceled(by creator)";
					} elseif($_groupgiftlist->status == "Cancelled Admin"){
						   $Status =  "Canceled(by admin)";
					}elseif($_groupgiftlist->status == "Unsuccessful"){
						  $Status =  $_groupgiftlist->status;
					}else{
						$Status =  $_groupgiftlist->status;
					}
					$date = $_groupgiftlist->created;
					$date = strtotime($date);
					$date = strtotime("+".$this->config->item('expiry_days')." day", $date);
					$endDate =  date('M d, Y, h:i', $date);
					 if($_groupgiftlist->status == "Cancelled" || $_groupgiftlist->status == "Cancelled Admin") {
						$Contribution = "Cancelled";
					} elseif($_groupgiftlist->status == "Successful") {
						$Contribution = $_groupgiftlist->status ;
					 } elseif($_groupgiftlist->status == "Unsuccessful"){
						$Contribution = "Expired" ;
					 }else{
						$Contribution = "Pending";
					}
				   $groupGiftList[] = array(
					   										'SllerID'=> $_groupgiftlist->gift_seller_id,
														   'GiftName' => $_groupgiftlist->gift_name,
									   		   			   'Status' => $Status,
														   'EndDate' => $endDate,
														   'Contribution' => $Contribution
													   );
			   }
			}
			$groupgiftPayment = $this->groupgift_model->get_all_details(GROUPGIFT_PAYMENT,array('user_id'=>$user_id));
			if($groupgiftPayment->num_rows() > 0){
				foreach($groupgiftPayment->result() as $row){
					$groupGiftPayment[] = array(
																'GiftId' => $row->groupgift_id,
																'ContributorName' => $row->contributor_name,
																'PaymentType' => 'Paypal',
																'Amount' => $row->amount,
																'Transaction Id' => $row->transaction_id,
																'Status' =>  $row->status,
																'Created Date' => $row->created,
															);
				}
			}

			if($this->lang->line('groupgift_name') != ''){ $value1 = stripslashes($this->lang->line('groupgift_name')); } else $value1 = "Gift Name";
			if($this->lang->line('groupgift_status') != ''){ $value2 = stripslashes($this->lang->line('groupgift_status')); } else $value2 = "Status";
			if($this->lang->line('groupgift_end_date') != ''){ $value3 = stripslashes($this->lang->line('groupgift_end_date')); } else $value3 = "End Date";
			if($this->lang->line('groupgift_contribution') != ''){ $value4 = stripslashes($this->lang->line('groupgift_contribution')); } else $value4 = "Contribution";
			$groupGiftListTableHead[] = array(
																	'value1' => $value1,
																	'value2' => $value2,
																	'value3' => $value3,
																	'value4' => $value4,
																);
			 if($this->lang->line('groupgift_id') != '') { $value1 = stripslashes($this->lang->line('groupgift_id')); } else $value1 = "Gift Id";
			 if($this->lang->line('contributor_name') != '') { $value2 = stripslashes($this->lang->line('contributor_name')); } else $value2 = "Contributor Name";
			 if($this->lang->line('groupgift_payment_type') != '') { $value3 = stripslashes($this->lang->line('groupgift_payment_type')); } else $value3 = "Payment Type";
			 if($this->lang->line('groupgift_payment_amt') != '') { $value4 = stripslashes($this->lang->line('groupgift_payment_amt')); } else $value4 = "Amount";
			 if($this->lang->line('groupgift_payment_txid') != '') { $value5 = stripslashes($this->lang->line('groupgift_payment_txid')); } else $value5 = "Transaction Id";
			 if($this->lang->line('groupgift_status') != '') { $value6 = stripslashes($this->lang->line('groupgift_status')); } else $value6 = "Status";
			 if($this->lang->line('groupgift_payment_created') != '') { $value7 = stripslashes($this->lang->line('groupgift_payment_created')); } else $value7 = "Created Date";
			 $groupGiftPaymentTableHead[] = array(
																	'value1' => $value1,
																	'value2' => $value2,
																	'value3' => $value3,
																	'value4' => $value4,
																	'value5' => $value5,
																	'value6' => $value6,
																	'value7' => $value7,
																);

			$responseArr['status'] = 1;
			$responseArr['response'] = 'Success';
			$responseArr['groupGiftListTableHead'] = $groupGiftListTableHead;
			$responseArr['groupGiftList'] = $groupGiftList;
			$responseArr['groupGiftPaymentTableHead'] = $groupGiftPaymentTableHead;
			$responseArr['groupGiftPayment'] = $groupGiftPayment;
		}
	echo json_encode($responseArr);
	}

	/*
	* !! >>>>>>>>>>>>> Search List <<<<<<<<<<<< !!
	*/

	public function store(){
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = $_POST['UserId'];
		$storeDetails = array();
		if($UserId != ""){
			$userProfileDetails = $this->mobile_model->get_all_details(USERS, array('id' => $UserId, 'status' => 'Active'));
			$followingListArr = explode(',', $userProfileDetails->row()->following);
		}
		$this->load->model('seller_plan_model','seller_plan');
        /******Remove free sellers updated by admin*****/
        $now = date("Y-m-d h:i:s");
        $Query = "SELECT user_id FROM ".STORE_FRONT." WHERE expiresOn < '".$now."' AND expiresOn != ''";
        $expiredUsers = $this->seller_plan->ExecuteQuery($Query);
        if($expiredUsers->num_rows > 0){
            foreach($expiredUsers->result() as $expiredId){
                $expiredList[] = $expiredId->user_id;
            }
            $Query = "DELETE FROM ".STORE_FRONT." WHERE user_id IN (".implode(',',$expiredList).") ";
            $this->seller_plan->ExecuteQuery($Query);
            $Query = "DELETE FROM ".STORE_ARB." WHERE user_id IN (".implode(',',$expiredList).") AND payment_type = 'Free' ";
            $this->seller_plan->ExecuteQuery($Query);
            $Query = "UPDATE ".USERS." SET store_payment = 'Cancelled' WHERE id IN (".implode(',',$expiredList).") ";
            $this->seller_plan->ExecuteQuery($Query);
        }
        /******Remove free sellers updated by admin*****/

    	$featured_seller = $this->seller_plan->get_featured_sellers();
    	if($featured_seller->num_rows > 0){
    		$featured = $featured_seller->result();
    		$featuredId = array();
    		foreach ($featured as $seller) {
    			$featuredId[] = $seller->id;
    		}
    	}

		$featuredStoreDetails = array();
		if($featured_seller->num_rows() > 0){
			$condition = 'select u.*,s.store_name,s.logo_image,s.cover_image from '.USERS.' as u left join '.STORE_FRONT.' as s on s.user_id = u.id where u.group ="Seller" and u.store_payment ="Paid" and u.status = "Active" and FIND_IN_SET(u.id ,"'.implode(",", $featuredId).'")';
			$featuredStoreLIst = $this->user_model->ExecuteQuery($condition);
			foreach($featuredStoreLIst->result() as $featuredStore){
				$storeBanner = base_url().'images/store/banner-dummy.jpg';
				$storeLogo = base_url().'images/store/dummy-logo.png';
				$storeTagline = "";
				$storeDescription = "";
				$storeName ="";
				$storeName = $featuredStore->store_name;
				if($featuredStore->cover_image !=""){
					$storeBanner = base_url().'images/store/'.$featuredStore->cover_image;
				}
				if($featuredStore->logo_image !=""){
					$storeLogo =  base_url().'images/store/'.$featuredStore->logo_image;
				}
				$storeTagline =$featuredStore->tagline;
				$storeDescription =$featuredStore->description;

				$userThumbnail = base_url().'images/users/user-thumb1.png';
				if($featuredStore->thumbnail !='') $userThumbnail = base_url().'images/users/'.$featuredStore->thumbnail;
				$following = 'No';
				if(in_array($featuredStore->id,$followingListArr)){$following = 'Yes';}
				if($storeName != ""){
					$cond = "select count(id) as productCount from ".PRODUCT." where user_id=".$featuredStore->id." and status='Publish'";
					$result = $this->mobile_model->ExecuteQuery($cond);
					$featuredStoreDetails[] = array(
												'UserId' => $featuredStore->id,
												'fullName' => $featuredStore->full_name,
												'userName' => $featuredStore->user_name,
												'UserImage' => $userThumbnail,
												'state' => $featuredStore->state,
												'country' => $featuredStore->country,
												'storeName' => trim($storeName),
												'storeBanner' => trim($storeBanner),
												'storeLogo' => $storeLogo,
												'storeDescription' =>strip_tags($storeDescription),
												'storeFollowing' => $following,
												'productCount' => $result->row()->productCount
												);
				}
			}
		}

        if($this->config->item('storefront_fees_month') == '0' && $this->config->item('storefront_fees_year') == '0'){
                $sellers_list = $this->user_model->get_all_details(USERS,array('group'=>'Seller','status'=>'Active'));
        }else{
            $condition = 'select u.*,s.store_name,s.logo_image,s.cover_image,count(p.id) as productCount from '.USERS.' as u left join '.STORE_FRONT.' as s on s.user_id = u.id left join '.PRODUCT.' as p on u.id=p.user_id where u.group ="Seller" and u.store_payment ="Paid" and u.status = "Active" and p.status="Publish" and NOT FIND_IN_SET(u.id ,"'.implode(",", $featuredId).'") group by u.id';
            $sellers_list = $sellers_list = $this->user_model->ExecuteQuery($condition);
        }

		if($sellers_list->num_rows() > 0){
			foreach($sellers_list->result() as $store){
				$storeBanner = base_url().'images/store/banner-dummy.jpg';
				$storeLogo = base_url().'images/store/dummy-logo.png';
				$storeTagline = "";
				$storeDescription = "";
				$storeName ="";
				$storeName = $store->store_name;
				if($store->cover_image !=""){
					$storeBanner = base_url().'images/store/'.$store->cover_image;
				}
				if($store->logo_image !=""){
					$storeLogo =  base_url().'images/store/'.$store->logo_image;
				}
				$storeTagline =$store->tagline;
				$storeDescription =$store->description;
				$userThumbnail = base_url().'images/users/user-thumb1.png';
				if($store->thumbnail !='') $userThumbnail = base_url().'images/users/'.$store->thumbnail;
				$following = 0;
				if($UserId != ""){
					if(in_array($store->id,$followingListArr)){$following = 1;}
				}
				if($storeName != ""){
					$storeDetails[] = array(
												'UserId' => $store->id,
												'fullName' => $store->full_name,
												'userName' => $store->user_name,
												'UserImage' => $userThumbnail,
												// 'state' => $store->state,
												// 'country' => $store->country,
												'storeName' => trim($storeName),
												'storeBanner' => trim($storeBanner),
												'storeLogo' => $storeLogo,
												// 'storeDescription' =>strip_tags($storeDescription),
												'storeFollowing' => $following,
												'productCount' => $store->productCount
												);
				}
			}
		}
		$returnArr['status'] = 1;
		$message = "Success";
		if($this->lang->line('json_success') != ""){
			$message = stripslashes($this->lang->line('json_success'));
		}
		$returnArr['response'] = $message;
		$returnArr['featuredStoreDetails'] = $featuredStoreDetails;
		$returnArr['storeList'] = $storeDetails;
		echo json_encode($returnArr);
	}


	/*
	 	!! >>>>>>>>>>>> View Store Details <<<<<<<<<<<< !!
	*/
	public function view_store() {
		$thisUserId = $_POST['UserId'];
		if($thisUserId != ""){
			$thisUserProfileDetails = $this->mobile_model->get_all_details(USERS, array('id' => $thisUserId));
			$followingListArr = explode(',', $thisUserProfileDetails->row()->following);
		}

		$condition = 'select s.id as sid,s.store_name,s.description,s.cover_image,s.logo_image,s.privacy,s.tagline,
			u.* from ' . STORE_FRONT . ' as s left join ' . USERS . ' as u on u.id = s.user_id where u.user_name= "' . $_POST['userName'] . '"';
		$userDetails = $this->mobile_model->ExecuteQuery($condition);

		if ($userDetails->num_rows() >= 1) {
            	$this->data['user_Details'] = $userDetails;
				$coverImage = base_url().'images/store/banner-dummy.jpg';
				$logoImage = base_url().'images/store/dummy-logo.png';
				if($userDetails->row()->cover_image !=''){
					$coverImage = base_url().'images/store/'.$userDetails->row()->cover_image;
				}
				if($userDetails->row()->logo_image !=''){
					$logoImage = base_url().'images/store/'.$userDetails->row()->logo_image;
				}
				$following = 0;
				if($thisUserId != ""){
					if(in_array($userDetails->row()->id,$followingListArr)){$following = 1;}
				}
				$product_feedback = $this->mobile_model->product_feedback_view($userDetails->row()->id);
				$ratingCount = count($product_feedback);
				if(empty($product_feedback)){
					$rating=0;
				}
				foreach($product_feedback as $feedback) {
					$rating=$feedback['rating'];
				}
				//echo "<pre>";print_r($userDetails->result());die;
				if($userDetails->row()->district != ""){
					$district = trim($userDetails->row()->district).',';
				}
				if($userDetails->row()->state != ""){
					$state = trim($userDetails->row()->state).',';
				}
				if($userDetails->row()->country != ""){
					$countryDetails = trim($userDetails->row()->country);
				}

				$user_Details = array(
					'store_id'=>$userDetails->row()->sid,
					'store_name'=>$userDetails->row()->store_name,
					'userId'=>$userDetails->row()->id,
					'user_name'=>$userDetails->row()->user_name,
					'full_name'=>$userDetails->row()->full_name,
					'followers'=>$userDetails->row()->followers,
					'followers_count'=>$userDetails->row()->followers_count,
					'description'=>strip_tags($userDetails->row()->description),
					'cover_image'=>$coverImage,
					'logo_image'=>$logoImage,
					'privacy'=>$userDetails->row()->privacy,
					'tagline'=>$userDetails->row()->tagline,
					'following' => $following,
					'country' => $district.$state.$countryDetails,
					'ratingCount' => $ratingCount,
					'storeRating' => $rating
				);
		} else {
			$userDetails = $this->mobile_model->get_all_details(USERS, array('user_name' => $_POST['userName']));
			$this->data['user_Details'] = $userDetails;
			$user_Details[]=array('user_name'=>$userDetails->row()->user_name);
		}

        if ($_POST['sort_by_price']) {
            if ($_POST['sort_by_price'] == 'desc') {
                $orderBy = ' order by p.sale_price desc';
            } else {
                $orderBy = ' order by p.sale_price asc';
            }
        } else {
            $orderBy = ' order by p.created desc ';
        }
        $this->data['product_count'] = $productDetails = $this->mobile_model->get_all_details(PRODUCT, array('user_id' => $userDetails->row()->id, 'status' => "Publish"));
        $cat_ids = array();
        $immediate_shipping = false;
        $list_ids = array();
        if ($productDetails->num_rows() > 0) {
            foreach ($productDetails->result() as $product_row) {
                if ($product_row->ship_immediate == 'true')
                    $immediate_shipping = true;
                $product_cat = $product_row->category_id;
                if ($product_cat != '') {
                    $product_cat_arr = array_filter(explode(',', $product_cat));
                    $cat_ids = array_unique(array_merge($cat_ids, $product_cat_arr));
                }
                $product_lists = $product_row->list_value;
                if ($product_lists != '') {
                    $product_lists_arr = array_filter(explode(',', $product_lists));
                    $list_ids = array_unique(array_merge($list_ids, $product_lists_arr));
                }
            }
        }
        $this->data['store_category'] = $cat_ids;
		$store_category[]=array('store_category'=>$cat_ids);
        $this->data['immediate_shipping'] = $immediate_shipping;
		$immediate_shipping[]=array('immediate_shipping'=>$immediate_shipping);
		$returnArr['user_details']=$user_Details;
		//$returnArr['store_category']=$store_category;
		//$returnArr['immediate_shipping']=$immediate_shipping;
        $this->data['list_ids'] = $list_ids;
		if(!empty( $this->data['list_ids'])){
			$list_ids=array('list_ids'=>$this->data['list_ids']);
		//	$returnArr['list_ids']=$list_ids;
		}
        $this->data['liked_count'] = $this->mobile_model->get_all_details(PRODUCT_LIKES, array('user_id' => $userDetails->row()->id));
		$like_count[] = array('count'=>$this->data['liked_count']->num_rows());
		//$returnArr['like_count']=$like_count;


        if ($_POST['is']) {
            $whereCond .= ' and p.ship_immediate = "' . $_POST['is'] . '"';
        }
        if ($_POST['p']) {
            $price = explode('-', $_POST['p']);
            $whereCond .= ' and p.sale_price >= "' . $price[0] . '" and p.sale_price <= "' . $price[1] . '"';
        }
        if ($_POST['q']) {
            $whereCond .= ' and p.product_name LIKE "%' . $_POST['q'] . '%"';
        }
        if ($_POST['c']) {
            $condition = " where list_value_seourl = '" . $_POST['c'] . "'";
            $listID = $this->mobile_model->getAttrubteValues($condition);
            $whereCond .= ' and FIND_IN_SET("' . $listID->row()->id . '",p.list_value)';
        }
        if ($_POST['products'] == 'products' && $_POST['category'] == ''|| $_POST['products'] == '' ) {
            $whereCond = ' where u.id=' . $userDetails->row()->id . '' . $whereCond . ' and p.status="Publish"';
            $searchProd = $whereCond . ' ' . $orderBy . ' ';
            $productList = $this->mobile_model->searchShopyByCategory($searchProd);
            $this->data['productDetails'] = $productList;
            $productDetailsArr = array();
            foreach($this->data['productDetails']->result() as $productDetailLists) {
				$likedStatus = 0;
				if($thisUserId != ""){
					$cond = "select user_id from ".PRODUCT_LIKES." where product_id=".$productDetailLists->seller_product_id." and user_id=".$thisUserId;
					$likedUsersList = $this->mobile_model->ExecuteQuery($cond);
					if($likedUsersList->num_rows() > 0){
						$likedStatus = 1;
					}
				}
				$imgArr=explode(',', $productDetailLists->image);
				$prodImage = '';
				if($imgArr[0] != ""){
					$prodImage = base_url()."images/product/".$imgArr[0];
				}
				$prodUrl = base_url().'json/product?pid='.$productDetailLists->id.'&UserId='.$thisUserId;
				
				$productDetailsArr[]=array(
											'id'=>$productDetailLists->id,
											'seller_product_id'=>$productDetailLists->seller_product_id,
											'product_name'=>$productDetailLists->product_name,
										//	'description'=>strip_tags($productDetailLists->description),
										//	'shippingPolicies'=>strip_tags($productDetailLists->shipping_policies),
											'image'=>$prodImage,
											'sale_price'=>$productDetailLists->sale_price,
										//	'url' => $prodUrl,
											'UserId' =>$productDetailLists->user_id,
										//	'seourl'=>$productDetailLists->seourl,
										//	'price'=>$productDetailLists->price,
											'user_name'=>$productDetailLists->user_name,
											'likes'=>$productDetailLists->likes,
											'liked' => $likedStatus,
											//'shareUrl' => base_url().'things/'.$productDetailLists->id.'/'.$productDetailLists->user_name.'?ref='.$thisUserProfileDetails->row()->user_name,
											'type' => 'SellingProduct',
										//	'userUrl' => 'http://192.168.1.251:8081/product-working/fancyclone-v3/json/user/seller-timeline?username='.$productDetailLists->user_name
										);
				}
			$returnArr['productDetails']=$productDetailsArr;
			echo json_encode($returnArr);
        }  elseif ($_POST['category'] != '') {
            $cate_id = $this->mobile_model->get_all_details(CATEGORY, array('cat_name' => urldecode($_POST['category'])));
            $whereCond = ' where FIND_IN_SET(' . $cate_id->row()->id . ',p.category_id) and user_id = ' . $userDetails->row()->id . '' . $whereCond . ' and p.status="Publish"';
            $searchProd = $whereCond . ' ' . $orderBy . ' ';
            $productList = $this->mobile_model->searchShopyByCategory($searchProd);
            $this->data['productDetails'] = $productList;
			            foreach($this->data['productDetails']->result() as $productDetailLists) {
									$productDetailsArr[]=array(
																'id'=>$productDetailLists->id,
																'seller_product_id'=>$productDetailLists->seller_product_id,
																'product_name'=>$productDetailLists->product_name,
																'image'=>base_url().'images/product/'.$productDetailLists->image,
																'sale_price'=>$productDetailLists->sale_price);
						}
			$returnArr['productDetails']=$productDetailsArr;
			echo json_encode($returnArr);
        }
    }

	/*
		!! >>>>>>>>>>>>>> User List <<<<<<<<<<<<<< !!
	*/
	public function display_user_lists(){
		$returnArr['status'] = "0";
		$returnArr['response'] = "";
		$username = $_POST['userName'];
		if($username != ""){
			$userProfileDetails = $this->mobile_model->get_all_details(USERS,array('user_name'=>$username));
			if ($userProfileDetails->num_rows()==1){
				// $this->data['userProfileDetails'] = $userProfileDetails;
				$userProfileArr[]=array(
											'user_name'=>$userProfileDetails->row()->user_name,
										);
				$this->data['listDetails'] = $this->mobile_model->get_all_details(LISTS_DETAILS,array('user_id'=>$userProfileDetails->row()->id));
				foreach($this->data['listDetails']->result() as $resultArr){
					if ($resultArr->product_id != ''){
						$pidArr = array_filter(explode(',', $resultArr->product_id));
						$productDetails = '';
						if (count($pidArr)>0){
							foreach ($pidArr as $pidRow){
								if ($pidRow!=''){
									$productDetails = $this->product_model->get_all_details(PRODUCT,array('seller_product_id'=>$pidRow,'status'=>'Publish'));
									if ($productDetails->num_rows()==0){
										$productDetails = $this->product_model->get_all_details(USER_PRODUCTS,array('seller_product_id'=>$pidRow,'status'=>'Publish'));
									}
									if ($productDetails->num_rows()==1)break;
								}
							}
						}
					//	if ($productDetails != '' && $productDetails->num_rows()==1){
							// $this->data['listImg'][$listDetailsRow->id] = $productDetails->row()->image;
						//	$returnArr['listImg'][$listDetailsRow->id] = $productDetails->row()->image;
					//	}
					$listUrl = base_url().'ios/user/lists?lid='.$resultArr->id.'&uname='.$username.'&UserId='.$userProfileDetails->row()->id;
					$LImage = str_replace(","," ",$productDetails->row()->image);
					$listImage = base_url().'images/product/'.$LImage;
					$listDetailsArr[] = array(
													'listId'=>$resultArr->id,
													'listName'=>$resultArr->name,
													//'product_id'=>$resultArr->product_id,
													//'user_id'=>$resultArr->user_id,
													//'product_count'=>$resultArr->product_count,
													//'followers_count'=>$resultArr->followers_count,
													'product_count' => strval(count(array_filter(explode(',',$resultArr->product_id)))),
													'listImage' => $listImage,
												//	'listUrl' => $listUrl
													);
					}

				}
				$returnArr['status'] = "1";
				$message = "Success";
				if($this->lang->line('json_success') != ""){
					$message = stripslashes($this->lang->line('json_success'));
				}
				$returnArr['response'] = $message;
				$returnArr['userProfileDetails']=$userProfileArr;
				$returnArr['listDetails']=$listDetailsArr;
				//$this->data['follow'] = $this->mobile_model->view_follow_list($userProfileDetails->row()->id);
				//$returnArr['total_follows']=$this->data['follow']->num_rows();

			}else {
				$message = "No data found";
				if($this->lang->line('json_no_data_found') != ""){
				    $message = stripslashes($this->lang->line('json_no_data_found'));
				}
				$returnArr['response'] = $message;
			}
		}else{
			$returnArr['response'] =  'UserName Null !!';
		}
		echo json_encode($returnArr);
	}

	/*
		!! >>>>>>>>>>>>>> RECENT ACTIVITES OF USER <<<<<<<<<<<<<< !!
	*/
	public function getUsersList() {
		$returnArr['status'] = "0";
		$returnArr['response'] = "";
		$lid = intval($_POST['listId']);
		$uname = $_POST['userName'];
		$thisUserId = $_POST['UserId'];
		if($lid == "" && $uname == ""){
			$returnArr['response'] = "Some Parameters are missing !!";
		}else{
			$this->data['user_profile_details'] = $userProfileDetails = $this->mobile_model->get_all_details(USERS, array('user_name' => $uname));
			$this->data['list_details'] = $list_details = $this->mobile_model->get_all_details(LISTS_DETAILS, array('id' => $lid, 'user_id' =>$this->data['user_profile_details']->row()->id));
			if ($this->data['list_details']->num_rows() == 0) {
				$message = "No lists avaliable";
				if($this->lang->line('json_no_list_found') != ""){
				    $message = stripslashes($this->lang->line('json_no_list_found'));
				}
				$returnArr['response'] = $message;
			} else {
				$searchArr = array_filter(explode(',', $list_details->row()->product_id));
				if (count($searchArr) > 0) {
					$fieldsArr = array(PRODUCT . '.*', USERS . '.user_name', USERS . '.full_name');
					$condition = array(PRODUCT . '.status' => 'Publish');
					$joinArr1 = array('table' => USERS, 'on' => USERS . '.id=' . PRODUCT . '.user_id', 'type' => '');
					$joinArr = array($joinArr1);
					$product_details = $this->product_model->get_fields_from_many(PRODUCT, $fieldsArr, PRODUCT . '.seller_product_id', $searchArr, $joinArr, '', '', $condition);
					$this->data['totalProducts'] = strval(count($searchArr));
					$fieldsArr = array(USER_PRODUCTS . '.*', USERS . '.user_name', USERS . '.full_name');
					$condition = array(USER_PRODUCTS . '.status' => 'Publish');
					$joinArr1 = array('table' => USERS, 'on' => USERS . '.id=' . USER_PRODUCTS . '.user_id', 'type' => '');
					$joinArr = array($joinArr1);
					$notsell_product_details= $this->product_model->get_fields_from_many(USER_PRODUCTS, $fieldsArr, USER_PRODUCTS . '.seller_product_id', $searchArr, $joinArr, '', '', $condition);
				}
				$FinalproductDetails = $this->mobile_model->get_sorted_array_mobile($product_details, $notsell_product_details, 'created', 'desc');
				foreach($FinalproductDetails as $prodRow){

					$likedStatus = "No";
					if($thisUserId != ""){
						$cond = "select user_id from ".PRODUCT_LIKES." where product_id=".$prodRow['seller_product_id']." and user_id=".$thisUserId;
						$likedUsersList = $this->mobile_model->ExecuteQuery($cond);
						if($likedUsersList->num_rows() > 0){
							$likedStatus = "Yes";
						}
					}

					$imgArr=@explode(',',$prodRow['image']);
					if($imgArr[0] != ""){
						$images=base_url().'images/product/'.$imgArr[0];
					}
					// foreach($imgArr as $img){
					// 	if($img!="")$images[]=base_url().'images/product/'.$img;
					// }
					$prodArr[]=array(
									'id'=>$prodRow['id'],
									'seller_product_id'=>$prodRow['seller_product_id'],
									'product_name'=>$prodRow['product_name'],
									'seourl'=>$prodRow['seourl'],
								//	'description'=>strip_tags($prodRow['description']),
								//	'shippingPolicies'=>strip_tags($prodRow['shipping_policies']),
									'image'=>$images,
								//	'price'=>$prodRow['price'],
									'UserId' =>$prodRow['user_id'],
									'sale_price'=>$prodRow['sale_price'],
									'user_name'=>$prodRow['user_name'],
									'likes'=>$prodRow['likes'],
									'liked' => $likedStatus,
								//	'url'=>$prodRow['url'].'&UserId='.$thisUserId,
									'type' => $prodRow['type'],
								//	'shareUrl' => base_url().'things/'.$prodRow['id'].'/'.$prodRow['user_name'].'?ref='.$thisUserProfileDetails->row()->user_name,
								//	'userUrl' => 'http://192.168.1.251:8081/product-working/fancyclone-v3/json/user/seller-timeline?username='.$prodRow['user_name']
									);
				}
				foreach($list_details->result() as $listProduct){
					$listsProducts[]=array(
										'name'=>$listProduct->name,
										'user_id'=>$listProduct->user_id,
										'product_id'=>$listProduct->product_id,
										'followers'=>$listProduct->followers,
										'id'=>$listProduct->id,
										'user_id'=>$listProduct->user_id,
										'category_id'=>$listProduct->category_id,
										'followers_count'=>$listProduct->followers_count
									);
				}
				$returnArr['status'] = "1";
				$message = "Success";
				if($this->lang->line('json_success') != ""){
					$message = stripslashes($this->lang->line('json_success'));
				}
				$returnArr['response'] = $message;
				$returnArr['totalProducts'] = $this->data['totalProducts'];
				$returnArr['productDetails'] = $prodArr;
				$returnArr['listsProducts'] = $listsProducts;
			}
		}
		echo json_encode($returnArr);
    }

	public function searchShop(){
			$countryList= $this->mobile_model->get_all_details(COUNTRY_LIST, array(), array(array('field' => 'name', 'type' => 'asc')));
			$countryLists = array();
			if($countryList->num_rows() > 0){
				foreach($countryList->result() as $country){
						$countryLists[] = array(
															'countryName' => $country->name,
															'countryCode' => $country->country_code
													);
				}
			}
			if($this->input->post('page') != ''){
					$paginationVal = $this->input->post('page')*20;
					$limitPaging = ' limit '.$paginationVal.',20 ';
			} else {
					$limitPaging = ' limit 0,20';
			}
			if($this->input->post('price') != ""){
				$price = explode('-',$this->input->post('price'));
				$whereCond .= ' and p.sale_price >= "'.$price[0].'" and p.sale_price <= "'.$price[1].'"';
			}
			if($this->input->post('immediateShipping')){
				$whereCond .= ' and p.ship_immediate = "'.$this->input->post('immediateShipping').'"';
			}
			// if($this->input->post('q')){
			// 	$whereCond .= ' and p.product_name LIKE "%'.$this->input->post('q').'%"';
			// }
			if($this->input->post('countryCode')){
				$countryCode = $this->input->post('countryCode');
				$whereCond .= ' and u.country = "'.$countryCode.'"';
			}
			if($this->input->post('sortByPrice')){
				($this->input->post('sortByPrice') == 'desc') ? $orderbyVal = $this->input->post('sortByPrice') : $orderbyVal ='';
				$orderBy = ' order by p.sale_price '.$orderbyVal.'';
			}else {
				$orderBy = ' order by p.created desc ';
			}
			$searchCriteria = $this->input->post('category');
			if($searchCriteria != '') {
				$condition = " where c.seourl = '".$searchCriteria."'";
				$catID = $this->mobile_model->getCategoryValues(' c.*,sbc.id as subcat_id,sbc.seourl as subcat_seourl,sbc.cat_name as subcat_sub_cat_name ',$condition);
				$whereCond = ' where FIND_IN_SET("'.$catID->row()->id.'",p.category_id) '.$whereCond.' and p.quantity>0 and p.status="Publish" and u.group="Seller" and u.status="Active" or FIND_IN_SET("'.$catID->row()->id.'",p.category_id) '.$whereCond.' and p.status="Publish" and p.quantity > 0 and p.user_id=0';
				$searchProd = $whereCond.' '.$orderBy.' '.$limitPaging.' ';
			} else {
				$whereCond = ' where p.id != "" '.$whereCond.' and p.quantity>0 and p.status="Publish" and u.group="Seller" and u.status="Active" or p.id != "" '.$whereCond.' and p.status="Publish" and p.quantity > 0 and p.user_id=0';
				$searchProd = $whereCond.' '.$orderBy.' '.$limitPaging.' ';
			}
			$productList = $this->mobile_model->searchShopyByCategory($searchProd);
			//echo "<pre>";print_r($productList->result());die;
			$productListArr = array();
			if($productList->num_rows() > 0){
				foreach($productList->result() as $productListRow){
					if($productListRow->user_id != "" && $productListRow->user_name != ""){
						if($productListRow->image != ""){
							$images = explode(',', $productListRow->image);
							if($images[0] != ""){
								$productImage = base_url().'images/product/'.$images[0];
							}
						}
						$liked = 0;
						if($this->input->post('UserId') != ""){
							$cond = "select user_id from ".PRODUCT_LIKES." where product_id=".$productListRow->seller_product_id." and user_id=".$this->input->post('UserId');
							$likedUsersList = $this->mobile_model->ExecuteQuery($cond);
							if($likedUsersList->num_rows() > 0){
								$liked = 1;
							}
						}
						$productListArr[]=array(
														'id' => $productListRow->id,
														'sellerProdcutId' => $productListRow->seller_product_id,
														'productName' => $productListRow->product_name,
														'productImage' => $productImage,
														'salePrice' => $productListRow->sale_price,
														'userId' => $productListRow->user_id,
														'userName' => $productListRow->user_name,
														'likes' => $productListRow->likes,
														'liked' => $liked
													);
					}
				}
			}
		$returnArr['productDetails']=$productListArr;
		$returnArr['CountryLists'] = $countryLists;
		echo json_encode($returnArr);
	}


	/*
	|| <<<<<<<<<<<<< Seller Product Details >>>>>>>>>>> ||
	*/
	public function sellingProductDetails(){
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = intval($_POST['UserId']);
		$productDetails = array();
		if($UserId == ""){
            $returnArr['response'] = 'UserId Null !!';
        }else {
			$userDetails = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
            $userType = $userDetails->row()->group;
            if ($userType == 'Seller') {
                $pid = $_POST['sellerProductId'];
				if($pid == ""){
					$returnArr['response'] = 'Seller Product Id Null !!';
				}else{
					$productDetails = $this->product_model->get_all_details(PRODUCT, array('seller_product_id' => $pid));
					if($productDetails->num_rows() == 0){
						$productDetails = $this->product_model->get_all_details(USER_PRODUCTS, array('seller_product_id' => $pid));
					}
					$productDetail = array();
	                if ($productDetails->num_rows() == 1) {
	                    if ($productDetails->row()->user_id == $userDetails->row()->id) {
							$shippingImmediate =0;
							if($productDetails->row()->ship_immediate == "true"){
								$shippingImmediate = 1;
							}
							$productDetail = array(
								'productName' => (string)$productDetails->row()->product_name,
								'sellerProductId' => (string)$productDetails->row()->seller_product_id,
								'description' => strip_tags($productDetails->row()->description),
								'shipping_policies' => strip_tags($productDetails->row()->shipping_policies),
								'excerpt' => (string)$productDetails->row()->excerpt,
								'product_type' => (string)$productDetails->row()->product_type,
								'quantity' => (string)$productDetails->row()->quantity,
								'shipImmediate' => $shippingImmediate,
								'sku' => (string)$productDetails->row()->sku,
								'weight' => (string)$productDetails->row()->weight,
								'attribute_must' => (string)$productDetails->row()->attribute_must,
								'youtube' => (string)$productDetails->row()->youtube,
								'price' => (string)$productDetails->row()->price,
								'sale_price' => (string)$productDetails->row()->sale_price,
							);
							$returnArr['status'] = '1';
							$message = "Success";
							if($this->lang->line('json_success') != ""){
								$message = stripslashes($this->lang->line('json_success'));
							}
							$returnArr['response'] = $message;
							$returnArr['productDetails'] = $productDetail;
	                    } else {
	                        $returnArr['response'] = 'Product UserId and UserId Mismatch !!';
	                    }
	                } else {
	                    $returnArr['response'] = 'Product Not Found !!';
	                }
				}
            } else {
                $returnArr['response'] = 'You are not a Seller. Please Sign up as Sller !!';
            }
        }
		echo json_encode($returnArr);
	}

	/*
	|| <<<<<<<<<<<<< Update Seller Product Details >>>>>>>>>>> ||
	*/
	public function  updateSellingProductDetails(){
		$returnArr['status'] = (string)0;
		$returnArr['response'] = '';
		$UserId = intval($_POST['UserId']);
		if( $UserId == ""){
			$returnArr['response'] = 'UserId Null !!';
		}else{
			$sellingProductId = intval($_POST['sellingProductId']);
			if($sellingProductId != ""){
				$table = PRODUCT;
				$productDetails = $this->product_model->get_all_details($table, array('seller_product_id' => $sellingProductId));
				if($productDetails->num_rows() == 0){
					$table = USER_PRODUCTS;
					$productDetails = $this->product_model->get_all_details($table, array('seller_product_id' => $sellingProductId));

				}
				$delte_user_product = $this->product_model->get_all_details(USER_PRODUCTS, array('seller_product_id' => $sellingProductId));
				if($productDetails->num_rows() == 1){
					$productName = $this->input->post('productName');
					$productDescription = $this->input->post('productDescription');
					$productExcerpt = $this->input->post('productExcerpt');
					$productType = $this->input->post('productType');
					$productSKU = $this->input->post('productSKU');
					$youtube = $this->input->post('youtube');
					$productPrice = $this->input->post('productPrice');
					$productSalePrice = $this->input->post('productSalePrice');
					if($productName != "" && $productDescription != "" && $productType != "" && $productPrice != "" && $productSalePrice != ""){
						if($productSalePrice < $productPrice){
							$price_range = 0;
				            if ($productSalePrice > 0 && $productSalePrice < 21) {
				                $price_range = '1-20';
				            } else if ($productSalePrice > 20 && $productSalePrice < 101) {
				                $price_range = '21-100';
				            } else if ($productSalePrice > 100 && $productSalePrice < 201) {
				                $price_range = '101-200';
				            } else if ($productSalePrice > 200 && $productSalePrice < 501) {
				                $price_range = '201-500';
				            } else if ($productSalePrice > 500) {
				                $price_range = '501+';
				            }
							$seourl = url_title($productName, '-');
							$priceRange = $price_range;
							parse_str(parse_url($youtube, PHP_URL_QUERY), $youtube_id);
							$shippingType = "Not Shippable";
							$productShipping = "";
							$productQuantity = "";
							$productWeight = "";
							$must = "";
							$shipImmediate = "";
							$attributeMust = "";
							if($productType == "physical"){
								$productQuantity = $this->input->post('productQuantity');
								$productWeight = $this->input->post('productWeight');
								$attributeMust = $this->input->post('attributeMust');
								$productShippingImmediate = $this->input->post('productShippingImmediate');
								if($attributeMust != "" && $productQuantity != "" && $productShippingImmediate != ""){
									$shippingType = "Shippable";
									$productShipping = $this->input->post('productShipping');
									if($attributeMust == 1){
										$must = "yes";
									}
									$shipImmediate = 'false';
									if($productShippingImmediate == 1){
										$shipImmediate = "true";
									}
								}else{
									$returnArr['response'] = 'Some Parameters are missing which are related to Physical Type !!';
									echo json_encode($returnArr);
									die;
								}
							}
							$modified = "";
							if($table == PRODUCT){
								$datestring = "%Y-%m-%d %H:%i:%s";
								$time = time();
								$modified = mdate($datestring, $time);
							}
							$categoryId = $productDetails->row()->category_id;
							$productImage = $productDetails->row()->image;
							$dataArray = array(
													'product_name' => trim($productName),
													'seller_product_id' => $productDetails->row()->seller_product_id,
													'created' => $productDetails->row()->created,
													'modified' => $modified,
													'description' => trim($productDescription),
													'shipping_policies' => trim($productShipping),
													'excerpt' => trim($productExcerpt),
													'user_id' => $productDetails->row()->user_id,
													'image' => $productImage,
													'category_id' => $categoryId,
													'seourl' => $seourl,
													'weight' => $productWeight,
													'quantity' => $productQuantity,
													'attribute_must' =>$must,
													'ship_immediate' => $shipImmediate,
													'price_range' => $priceRange,
													'shipping_type' => $shippingType,
													'product_type' => $productType,
													'youtube' => $youtube_id['v'],
													'sku' => $productSKU,
													'price' => $productPrice,
													'sale_price' => $productSalePrice,
												);
							if($table == PRODUCT){

								$condition = array('seller_product_id' => $sellingProductId, 'user_id' => $UserId);
					            $this->user_model->update_details(PRODUCT, $dataArray, $condition);
								$returnArr['status'] = 1;
								$returnArr['sellerProductId'] = $productDetails->row()->seller_product_id;
								$returnArr['response'] = 'Product Updated Successfully';
							}else{

								$this->product_model->simple_insert(PRODUCT,$dataArray);
								$returnArr['status'] = 1;
								$returnArr['sellerProductId'] = $productDetails->row()->seller_product_id;


								$returnArr['response'] = 'Product Added Successfully';
							}
							if($delte_user_product->num_rows() > 0){
								if($productDetails->row()->seller_product_id == $delte_user_product->row()->seller_product_id){
									$this->product_model->commonDelete(USER_PRODUCTS, array('seller_product_id' => $delte_user_product->row()->seller_product_id));
								}
							}

						}else{
							$returnArr['response'] = 'Product\'s Sale Price should be less than Product\'s Price !!';
						}
					}else{
						$returnArr['response'] = 'Some Parameters are missing !!';
					}
				}else{
					$returnArr['response'] = 'No Such Product Found !!';
				}
			}else{
				$productName = $this->input->post('productName');
				$productDescription = $this->input->post('productDescription');
				$productExcerpt = $this->input->post('productExcerpt');
				$productType = $this->input->post('productType');
				$productSKU = $this->input->post('productSKU');
				$youtube = $this->input->post('youtube');
				$productPrice = $this->input->post('productPrice');
				$productSalePrice = $this->input->post('productSalePrice');
				if($productName != "" && $productDescription != "" && $productType != "" && $productPrice != "" && $productSalePrice != ""){
					if($productSalePrice < $productPrice){
						$price_range = 0;
						if ($productSalePrice > 0 && $productSalePrice < 21) {
							$price_range = '1-20';
						} else if ($productSalePrice > 20 && $productSalePrice < 101) {
							$price_range = '21-100';
						} else if ($productSalePrice > 100 && $productSalePrice < 201) {
							$price_range = '101-200';
						} else if ($productSalePrice > 200 && $productSalePrice < 501) {
							$price_range = '201-500';
						} else if ($productSalePrice > 500) {
							$price_range = '501+';
						}
						$seourl = url_title($productName, '-');
						$priceRange = $price_range;
						parse_str(parse_url($youtube, PHP_URL_QUERY), $youtube_id);
						$shippingType = "Not Shippable";
						$productShipping = "";
						$productQuantity = "";
						$productWeight = "";
						$must = "";
						$shipImmediate = "";
						$attributeMust = "";
						if($productType == "physical"){
							$productQuantity = $this->input->post('productQuantity');
							$productWeight = $this->input->post('productWeight');
							$attributeMust = $this->input->post('attributeMust');
							$productShippingImmediate = $this->input->post('productShippingImmediate');
							if($attributeMust != "" && $productQuantity != "" && $productShippingImmediate != ""){
								$shippingType = "Shippable";
								$productShipping = $this->input->post('productShipping');
								if($attributeMust == 1){
									$must = "yes";
								}
								$shipImmediate = 'false';
								if($productShippingImmediate == 1){
									$shipImmediate = "true";
								}
							}else{
								$returnArr['response'] = 'Some Parameters are missing which are related to Physical Type !!';
								echo json_encode($returnArr);
								die;
							}
						}
						$datestring = "%Y-%m-%d %H:%i:%s";
						$time = time();
						$created = mdate($datestring, $time);
						$dataArray = array(
												'product_name' => trim($productName),
												'seller_product_id' => time(),
												'created' => $created,
												'description' => trim($productDescription),
												'shipping_policies' => trim($productShipping),
												'excerpt' => trim($productExcerpt),
												'user_id' => $UserId,
												'seourl' => $seourl,
												'weight' => $productWeight,
												'quantity' => $productQuantity,
												'attribute_must' =>$must,
												'ship_immediate' => $shipImmediate,
												'price_range' => $priceRange,
												'shipping_type' => $shippingType,
												'product_type' => $productType,
												'youtube' => $youtube_id['v'],
												'sku' => $productSKU,
												'price' => $productPrice,
												'sale_price' => $productSalePrice,
											);
						$this->product_model->simple_insert(PRODUCT,$dataArray);
						$productID = $this->user_model->get_last_insert_id();
						$productDetails = $this->product_model->get_all_details(PRODUCT, array('id' => $productID, 'user_id' => $UserId));
						$returnArr['status'] = 1;
						$returnArr['sellerProductId'] = $productDetails->row()->seller_product_id;
						$returnArr['response'] = 'Product Added Successfully';
					}else{
						$returnArr['response'] = 'Product\'s Sale Price should be less than Product\'s Price !!';
					}
				}else{
					$returnArr['response'] = 'Some Parameters are missing !!';
				}
			}
	    }
		echo json_encode($returnArr);
    }

	/*
		||| SELLING PRODUCT CATEGORY |||
	*/
	public function insertEditSellingProductCategory(){
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = $_POST['UserId'];
		if($UserId != ""){
			$sellingProductId = intval($_POST['sellingProductId']);
			if($sellingProductId != ""){
				if ($_POST['categoryId'] != '') {
					$category_id = $_POST['categoryId'];
					$dataArr = array('category_id' => $category_id);
					$this->product_model->update_details(PRODUCT, $dataArr, array('seller_product_id' => $sellingProductId, 'user_id' => $UserId));
					$returnArr['status'] = '1';
					$returnArr['response'] = 'Product Category Updated Successfully';
				} else {
					$returnArr['response'] = 'CategoryId Null !!';
				}
			}else{
				$returnArr['response'] = 'SellerProductId Null !!';
			}
		}else{
			$returnArr['response'] = 'UserId Null !!';
		}
		echo json_encode($returnArr);
	}


	// public function insertEditSellingProductList('){
	// 	$returnArr['status'] = 0;
	// 	$returnArr['response'] = '';
	// 	$UserId = $_POST['UserId'];
	// 	if($UserId != ""){
	// 		$sellingProductId = intval($_POST['sellingProductId']);
	// 		if($sellingProductId != ""){
	// 		}else{
	//
	// 		}
	// 	}else{
	//
	// 	}
	// }


	/*
	|| <<<<<<<<<<<<< Attributes Details >>>>>>>>>>> ||
	*/
	public function  sellerProductAttributesDetails(){
		$returnArr['status'] = '0';
		$returnArr['response'] = '';
		$UserId = $_POST['UserId'];
		$attributes  = "";
		if($UserId != ""){
			$pid = $_POST['sellerProductId'];
			if($pid == ""){
				$returnArr['response'] = 'SellerProduct Id Null !!';
			}else{
				$productDetails = $this->product_model->get_all_details(PRODUCT, array('seller_product_id' => $pid));
				$PrdattrVal = $this->product_model->view_product_atrribute_details();
				$attributesList = array();
				$selectedArrtibuteList = array();
				if($PrdattrVal->num_rows() > 0){
					foreach($PrdattrVal->result() as $row){
						$attributesList[] = array(
													'attributeId' =>$row->id,
													'attributeType' => $row->attr_name
												);
					}

					$SubPrdVal = $this->product_model->view_subproduct_details($productDetails->row()->id);
					$attributes  =$productDetails->row()->attribute_must;
					foreach ($SubPrdVal->result_array() as $SubPrdValS){
						foreach($PrdattrVal->result() as $row){
							if( $row->id == $SubPrdValS['attr_id'] ){
								$selectedArrtibuteList[] = array(
																'id' => $SubPrdValS['pid'],
																'attributeId' =>$row->id,
																'attributeType' => $row->attr_name,
																'attributeName' => $SubPrdValS['attr_name'],
																'attributePrice' => $SubPrdValS['attr_price'],
																'attributeQuantity' => $SubPrdValS['attr_qty']
															);
							}
						}
					}
					$returnArr['status'] = '1';
					$message = "Success";
					if($this->lang->line('json_success') != ""){
						$message = stripslashes($this->lang->line('json_success'));
					}
					$returnArr['response'] = $message;
				}
				$returnArr['attributesTypesList'] = $attributesList;
				$returnArr['attribute_must'] = $attributes;
				$returnArr['selectedArrtibuteList'] = $selectedArrtibuteList;
			}
		}
		echo json_encode($returnArr);
	}

	/*
		||| CART PAGE |||
	*/
	public function cart(){
		$returnArr['status_code'] = 0;
		$returnArr['response'] = '';
		//$returnArr['shipping'] = 1;
		$returnArr['shipping'] = 1;
		$UserId = $_POST['UserId'];
		$ShipId = $_POST['shipId'];
		if ($UserId != '') {
			$MainShipCost = 0;
			$MainTaxCost = 0;
			$cartQty = 0;
			$shipVal = $this->mobile_model->get_all_details(SHIPPING_ADDRESS, array('user_id' => $UserId));

			if ($shipVal->num_rows() > 0) {
				$shipValID = $this->mobile_model->get_all_details(SHIPPING_ADDRESS, array('user_id' => $UserId, 'primary' => 'Yes'));
				$dataArr = array('shipping_id' => $shipValID->row()->id);
				$condition = array('user_id' => $UserId);
				$this->mobile_model->update_details(FANCYYBOX_TEMP, $dataArr, $condition);
				$ShipCostVal = $this->mobile_model->get_all_details(COUNTRY_LIST, array('country_code' => $shipValID->row()->country));
				if($ShipCostVal->row()->shipping_cost != ""){
					$MainShipCost = $ShipCostVal->row()->shipping_cost;
				};
				if($ShipCostVal->row()->shipping_tax != ""){
					$MainTaxCost = $ShipCostVal->row()->shipping_tax;
				}
				$dataArr2 = array('shipping_cost' => $MainShipCost, 'tax' => $MainTaxCost);
				$this->mobile_model->update_details(SHOPPING_CART, $dataArr2, $condition);
			}
			/* >>>>>>>>>>>> Discount Amount <<<<<<<<<<< */

			$this->db->select('discountAmount,couponID,couponCode,coupontype');
			$this->db->from(SHOPPING_CART);
			$this->db->where('user_id = ' . $UserId);
			$discountQuery = $this->db->get();
			$disAmt = $discountQuery->row()->discountAmount;
			$couponID = '';
			$coupontype = '';
			$couponCode = '';
			if ($disAmt > 0) {
				$couponID =$discountQuery->row()->couponID;
				$coupontype =$discountQuery->row()->coupontype;
				$couponCode =  $discountQuery->row()->couponCode;
			}
			/* >>>>>>>>>>>> Cart Details <<<<<<<<<<< */
			$cart_Details = $this->mobile_model->getProductsInCart($UserId);
			$cartAmt = 0; $cartShippingAmt = 0;$cartTaxAmt = 0;$s = 0;$ship_need = 0;
			$cartDetails = array();

			if($cart_Details->num_rows() > 0){
				foreach ($cart_Details->result() as $CartRow) {
					$subShipping = $this->cart_model->get_all_details(SUB_SHIPPING, array('product_id'=> $CartRow->product_id));
					$countryCode = [];
					$everyelse =[];
					if($subShipping->num_rows() >0){
						foreach($subShipping->result() as $_subShipping){
							$countryCode[] = $_subShipping->ship_code;
							$everyelse[] = $_subShipping->ship_name;
						}
					}
					if($ShipId == ""){
						$shippingAddressOfUser = $this->cart_model->get_all_details(SHIPPING_ADDRESS, array('user_id' => $UserId,'primary'=>'Yes'));
						$ShipId = $shippingAddressOfUser->row()->id;
					}else{
						$shippingAddressOfUser = $this->cart_model->get_all_details(SHIPPING_ADDRESS, array('user_id' => $UserId,'id'=>$ShipId));
					}
					if(in_array($shippingAddressOfUser->row()->country, $countryCode)){
						$prodIds[] = $CartRow->product_id;
					}elseif(in_array('Everywhere Else', $everyelse)){
						$prodIds[] = $CartRow->product_id;
					}else{
						$UnprodIds[] = $CartRow->product_id;
					}
				}
				$s=0;
				foreach ($cart_Details->result() as $CartRow) {
					if($CartRow->product_type == 'physical'){
						$ship_need++;
						if($ShipId ==''){
							$shipping = $this->cart_model->get_all_details(SHIPPING_ADDRESS, array('user_id' => $userid,'primary'=>'Yes'));
							if($shipping->num_rows() >0){
								$ShipId = $shipping->row()->id;
							}
						}
					}

					$attri = array_filter(explode(',', $CartRow->attribute_values));
					$curdate = date('Y-m-d');
					$newImg = @explode(',', $CartRow->image);
					$productUrl =  base_url().'things/' . $CartRow->prdid . '/' . $CartRow->seourl;
					$productImage =  base_url().'images/product/' . $newImg[0];
					$productName  = $CartRow->product_name;
					$productId = $CartRow->prdid;
					$productAttributes = array();
					foreach ($attri as $_attributes) {
						$Query = "select b.attr_name as attr_type,a.attr_name from " . SUBPRODUCT . " as a left join " . PRODUCT_ATTRIBUTE . " as b on a.attr_id = b.id where a.pid = '" . $_attributes . "'";
						$attributes = $this->mobile_model->ExecuteQuery($Query);
						if ($attributes->num_rows() > 0) {
							$productAttributes[]= array(
																	'attributeName'=>$attributes->row()->attr_type,
																	'attributeValue' => $attributes->row()->attr_name
																	);
						}
					}
					$productPrice = $this->data['currencySymbol'] . $CartRow->price;
					if ($CartRow->product_type == 'physical') {
						$productQuantity  =  $CartRow->quantity;
						$totalProductQuantity  =  $CartRow->mqty;
						$QuantityUpdatable = 1;
					} else {
						$productQuantity  =  $CartRow->quantity;
						$totalProductQuantity  = '';
						$QuantityUpdatable = 0;
					}
					$totalProductPrice = $this->data['currencySymbol']. $CartRow->indtotal;

					if(in_array($CartRow->product_id, $prodIds)){
						$shippingPossible = 1;
						if($ShipId!=''){
							$Query = "SELECT * FROM (SELECT MAX(`sub`.`ship_cost`) AS maxCost, MAX(`sub`.`ship_other_cost`) AS maxOtherCost, `c`.`name` FROM (`fc_sub_shipping` as sub) JOIN `fc_shipping_address` as ship ON `ship`.`country` = `sub`.`ship_code` JOIN `fc_country` as c ON `c`.`country_code` = `ship`.`country` WHERE `ship`.`id` = ".$ShipId." AND `sub`.`status` = 'Active' AND `sub`.`product_id` IN (".implode(',', $prodIds).")) AS A WHERE A.maxCost IS NOT NULL";
							$shipCostDetails = $this->cart_model->ExecuteQuery($Query);
							if($shipCostDetails->num_rows() > 0){
								$shipCost = $shipCostDetails->row()->maxCost;
								$shipOtherCost = $shipCostDetails->row()->maxOtherCost;
							}else{
								$Query = "SELECT * FROM (SELECT MAX(`ship_cost`) AS maxCost, MAX(`ship_other_cost`) AS maxOtherCost FROM (`fc_sub_shipping`) WHERE `status` = 'Active' AND `ship_id` = 232 AND `product_id` IN (".implode(',', $prodIds).")) AS A WHERE A.maxCost IS NOT NULL";
								$shipCostDetails = $this->cart_model->ExecuteQuery($Query);
								if($shipCostDetails->num_rows() > 0){
									$shipCost = $shipCostDetails->row()->maxCost;
									$shipOtherCost = $shipCostDetails->row()->maxOtherCost;
								}
							}
						}else{
							$Query = "SELECT * FROM (SELECT MAX(`ship_cost`) AS maxCost, MAX(`ship_other_cost`) AS maxOtherCost FROM (`fc_sub_shipping`) WHERE `status` = 'Active' AND `ship_id` = 232 AND `product_id` IN (".implode(',', $prodIds).")) AS A WHERE A.maxCost IS NOT NULL";
							$shipCostDetails = $this->cart_model->ExecuteQuery($Query);
							if($shipCostDetails->num_rows() > 0){
								$shipCost = $shipCostDetails->row()->maxCost;
								$shipOtherCost = $shipCostDetails->row()->maxOtherCost;
							}
						}
						$condition = "select * from ".SHOPPING_CART." where `product_id` IN(".implode(',',$prodIds).") and user_id=".$UserId;
						$cartProduct = $this->cart_model->ExecuteQuery($condition);
						if($shipCostDetails->num_rows() > 0){
							if($cartProduct->num_rows() <= 1){
								$shippingValue =0;
								if($CartRow->quantity >1){
									$Quantity = $CartRow->quantity - 1;
									$shippingValue = $shipCost + ($shipOtherCost * $Quantity);
								}else{
									$shippingValue = $shipCost;
								}
							}else{
								if($s == 0){
									$shippingValue =0;
									if($CartRow->quantity > 1){
										$Quantity = $CartRow->quantity - 1;
										$shippingValue = $shipCost + ($shipOtherCost * $Quantity);
									}else{
										$shippingValue = $shipCost;
									}
								}else{
									$shippingValue =0;
									$shippingValue = $shipOtherCost * $CartRow->quantity;
								}
							}
							if ($CartRow->ship_immediate == 'true') {
								$productRefShipping =$shippingValue. '(Immediate)';
								$productRefShipping = (string)$productRefShipping;
							} else {
								if($CartRow->ship_duration != ""){
									$productRefShipping = number_format($shippingValue,2). '('.$CartRow->ship_duration.')';
								}else{
									$productRefShipping = number_format($shippingValue,2);
								}
								$productRefShipping = (string)$productRefShipping;
							}
							$productCost =  (($CartRow->product_shipping_cost + $CartRow->price + ($CartRow->price * 0.01 * $CartRow->product_tax_cost)));
							$productTotalCost = (($CartRow->product_shipping_cost + $CartRow->price + ($CartRow->price * 0.01 * $CartRow->product_tax_cost)) * $CartRow->quantity);
							$cartAmt = $cartAmt + (($CartRow->product_shipping_cost + $CartRow->price + ($CartRow->price * 0.01 * $CartRow->product_tax_cost)) * $CartRow->quantity);
							$cartTaxAmt = $cartTaxAmt + ($CartRow->product_tax_cost * $CartRow->quantity);
							$TotalShipCost = $TotalShipCost + $shippingValue;
							$cartQty = $cartQty + $CartRow->quantity;
							$sellerName = "";
							if($CartRow->sellerName != ""){
								$sellerName = $CartRow->sellerName;
							}
							$shippingPossible = 1; 
							$cartDetails[]  =  array(
																's' => (string)$s,
																'cartId' => $CartRow->id,
																'productUrl' => $productUrl,
																'productImage' => $productImage,
																'productName' => $productName,
																'productId' => $productId,
																'productType' => $CartRow->product_type,
																'sellerName' => $sellerName,
															//	'productAttributesList' => $productAttributesList,
																'productAttributes' => $productAttributes,
																'productQuantity' => $productQuantity,
																'totalProductQuantity' => $totalProductQuantity,
																'QuantityUpdatable' => $QuantityUpdatable,
																'productRefShipping' => $productRefShipping,
																'cartShippingAmt' => number_format($shippingValue,2,'.',''),
																'cartTaxAmt' => number_format($cartTaxAmt, 2, '.', ''),
																'cartQty' => (string)$cartQty,
																'productCost' => number_format($productCost, 2, '.', ''),
																'Amount'  =>  number_format($productTotalCost, 2, '.', ''),
																'shippingPossible' => $shippingPossible,
														);
						}else{
							$shippingPossible = 0;
							//$returnArr['shipping'] = 0;
							//$shippingPossible = 1;
							$returnArr['shipping'] = 1;
							$sellerName = "";
							if($CartRow->sellerName != ""){
								$sellerName = $CartRow->sellerName;
							}
							$cartDetails[]  =  array(
																's' => (string)$s,
																'cartId' => $CartRow->id,
																'productUrl' => $productUrl,
																'productImage' => $productImage,
																'productName' => $productName,
																'productId' => $productId,
																'productType' => $CartRow->product_type,
																'sellerName' => $sellerName,
															//	'productAttributesList' => $productAttributesList,
																'productAttributes' => $productAttributes,
																'productQuantity' => $productQuantity,
																'totalProductQuantity' => $totalProductQuantity,
																'QuantityUpdatable' => $QuantityUpdatable,
																'productRefShipping' => "This product cant be shipped to your address !!",
																'cartShippingAmt' => number_format($shippingValue,2,'.',''),
																'cartTaxAmt' => number_format($cartTaxAmt, 2, '.', ''),
																'cartQty' => (string)$cartQty,
																'productCost' => number_format($productCost, 2, '.', ''),
																'Amount'  =>  number_format($productTotalCost, 2, '.', ''),
																'shippingPossible' => $shippingPossible,
														);
						}
					}else{
						$shippingPossible = 0;
						//$returnArr['shipping'] = 0;
						//$shippingPossible = 1;
						$returnArr['shipping'] = 1;
						$sellerName = "";
						if($CartRow->sellerName != ""){
							$sellerName = $CartRow->sellerName;
						}
						$cartDetails[]  =  array(
															's' => (string)$s,
															'cartId' => $CartRow->id,
															'productUrl' => $productUrl,
															'productImage' => $productImage,
															'productName' => $productName,
															'productId' => $productId,
															'productType' => $CartRow->product_type,
															'sellerName' => $sellerName,
														//	'productAttributesList' => $productAttributesList,
															'productAttributes' => $productAttributes,
															'productQuantity' => $productQuantity,
															'totalProductQuantity' => $totalProductQuantity,
															'QuantityUpdatable' => $QuantityUpdatable,
															'productRefShipping' => "This product cant be shipped to your address !!",
															'cartShippingAmt' => number_format($shippingValue,2,'.',''),
															'cartTaxAmt' => number_format($cartTaxAmt, 2, '.', ''),
															'cartQty' => (string)$cartQty,
															'productCost' => number_format($productCost, 2, '.', ''),
															'Amount'  =>  number_format($productTotalCost, 2, '.', ''),
															'shippingPossible' => $shippingPossible,
													);
					}
				$s++;
			}
			$returnArr['cartDetails'] = $cartDetails;
		/* >>>>>>>>>>>> Total Amount  <<<<<<<<<<< */
			$cartSAmt = $TotalShipCost;
			$cartTAmt = ($cartAmt * 0.01 * $MainTaxCost);
			$grantAmt = $cartAmt + $cartSAmt + $cartTAmt;
			if($disAmt > 0){
				$grantAmt = $grantAmt-$disAmt;
			}
			$totalAmount=array(
										'CartItemTotal'  =>  number_format($cartAmt, 2, '.', ''),
										'MainTaxCost' => $MainTaxCost,
										'discountAmt' => $disAmt,
										'couponCode' => $couponCode,
										'couponID' => $couponID,
										'coupontype' => $coupontype,
										'CartShippingAmount'  =>  number_format($cartSAmt, 2, '.', ''),
										'CartTaxAmount'  =>  number_format($cartTAmt, 2, '.', ''),
										'GrantAmount'  =>  number_format($grantAmt, 2, '.', '')
									);
			$returnArr['totalAmount'] = $totalAmount;
		/* >>>>>>>>>>>> Shipping Address <<<<<<<<<<< */
			$shippingAddress  =  array();
			$PrimaryAddress  =  array();
			foreach ($shipVal->result() as $Shiprow) {
				if ($Shiprow->primary == 'Yes') {
					$PrimaryAddress = array(
														'id'  =>  $Shiprow->id,
														'FullName'  =>  $Shiprow->full_name,
														'NickName'  =>  $Shiprow->nick_name,
														'Address1'  =>  $Shiprow->address1,
														'Address2'  =>  $Shiprow->address2,
														'city'  =>  $Shiprow->city,
														'state'  =>  $Shiprow->state,
														'postalCode'  =>  $Shiprow->postal_code,
														'country'  =>  $Shiprow->country,
														'phone'  =>  $Shiprow->phone
													);
					if($ShipId != ""){
						if($ShipId == $Shiprow->id){
							$optionsValues = 1;
						}else{
							$optionsValues = 0;
						}
					}else{
						$optionsValues = 1;
					}
				}else {
					if($ShipId != ""){
						if($ShipId == $Shiprow->id){
							$optionsValues = 1;
						}else{
							$optionsValues = 0;
						}
					}else{
						$optionsValues = 0;
					}
				}
				$shippingAddress[]  =  array(
													'selectedAddress' =>  $optionsValues,
													'id'  =>  $Shiprow->id,
													'FullName'  =>  $Shiprow->full_name,
													'NickName'  =>  $Shiprow->nick_name,
													'Address1'  =>  $Shiprow->address1,
													'Address2'  =>  $Shiprow->address2,
													'city'  =>  $Shiprow->city,
													'state'  =>  $Shiprow->state,
													'postalCode'  =>  $Shiprow->postal_code,
													'country'  =>  $Shiprow->country,
													'phone'  =>  $Shiprow->phone
												);
				}



				$countryList= $this->mobile_model->get_all_details(COUNTRY_LIST, array(), array(array('field' => 'name', 'type' => 'asc')));
			/* >>>>>>> Payment Methods <<<<<<<< */
				$condition = 'select * from '.PAYMENT_GATEWAY.' where status="Enable"';
				$payment_Gateway = $this->mobile_model->ExecuteQuery($condition);
				$paymentGateway = array();
				foreach ($payment_Gateway->result() as $_payment) {
					$paymentGateway[]=array(
														'gatewayId' => $_payment->id,
														'gatewayName' => $_payment->gateway_name
													);
				}
				$returnArr['PrimaryAddress'] = $PrimaryAddress;
				$returnArr['shippingAddress'] = $shippingAddress;
				$returnArr['paymentGateway'] = $paymentGateway;
				$returnArr['status_code'] = (string)1;
				$message = "Success";
				if($this->lang->line('json_success') != ""){
					$message = stripslashes($this->lang->line('json_success'));
				}
				$returnArr['response'] = $message;
			}else{
				$returnArr['response'] = 'No Items in Cart';
			}
		}else{
			$returnArr['response'] = 'User Id Null, Please Log in';
		}
		echo json_encode($returnArr);
	}

	/*
		||| UPDATE CART QUANTITY |||
	*/
	public function updateCartQuantity(){
		$returnArr['status'] = '0';
		$returnArr['response'] = '';
		$UserId = $_POST['UserId'];
		$cartId = $_POST['cartId'];
		$productId = $_POST['productId'];
		$cartQuantity = $_POST['cartQuantity'];
		$totalQuantity = $_POST['totalQuantity'];
		$shipId = $_POST['shipId'];
		if ($productId != '' && $UserId != '' && $cartId  != '' && $cartQuantity  != '' && $shipId  != '' && $totalQuantity != '') {
			$chckCartQuantity = $cartQuantity;
			if($chckCartQuantity > $totalQuantity){
				$errorTxt = 'Maximum stock available for this product is '.$totalQuantity;
				$returnArr['response'] = $errorTxt;
			}else{
				$excludeArr = array('cartQuantity', 'totalQuantity','UserId','cartId','productId','shipId');
		        $productVal = $this->mobile_model->get_all_details(SHOPPING_CART, array('user_id' => $UserId, 'id' => $cartId));
		        $newQty = $cartQuantity;
		        $indTotal = ($productVal->row()->price + ($productVal->row()->price * 0.01 * $productVal->row()->product_tax_cost) ) * $newQty;
		        $dataArr = array('quantity' => $newQty, 'indtotal' => $indTotal, 'total' => $indTotal);
		        $condition = array('id' => $productVal->row()->id);
		        $this->mobile_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
				$returnArr['status'] = '1';
				$message = "Product Quantity Updated Successfully";
				$returnArr['response'] = $message;
				}
        } else {
			$returnArr['response'] = 'Some Parameters Missing !!';
        }
		echo json_encode($returnArr);
	}

	/*
		||| REMOVE PRODUCT FROM CART |||
	*/
	public function removeCartProduct(){
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = $_POST['UserId'];
		$delt_id = $_POST['cartId'];
		$shipId = $_POST['shipId'];
		$CondID =  $_POST['cart'];
		if($UserId !=" " && $delt_id !=" "){
			if ($CondID == 'gift') {
				$this->db->delete(GIFTCARDS_TEMP, array('id' => $delt_id));
				$GiftVal= $this->cart_model->mani_gift_total($UserId);
			} elseif ($CondID == 'subscribe') {
				$this->db->delete(FANCYYBOX_TEMP, array('id' => $delt_id));
				$SubscribeVal = $this->cart_model->mani_subcribe_total($UserId);
			} elseif ($CondID == 'cart') {
				$this->db->delete(SHOPPING_CART, array('id' => $delt_id));
			$returnArr['status'] = '1';
			$message = "Product Removed Successfully from Cart";
			$returnArr['response'] = $message;
			}
		}else{
			$returnArr['response'] = 'Some Parameters are Misssing';
		}
		echo json_encode($returnArr);
	}

	/*
		||| CHANGE SHIPPING ADDRESS |||
	*/
	public function ChangeShippingAddress() {
		$returnArr['status'] = (string)0;
		$returnArr['response'] = '';
		$UserId = intval($_POST['UserId']);
		$add_id  = $_POST['shipId'];
		$amt  = $_POST['cartAmount'];
		$disamt  = $_POST['discountAmount'];
        if($add_id != '' && $UserId != "" && $amt !="" && $disamt != "") {
            $ChangeAdds = $this->cart_model->get_all_details(SHIPPING_ADDRESS, array('user_id' => $UserId, 'id' => $add_id));
            $ShipCostVal = $this->cart_model->get_all_details(COUNTRY_LIST, array('country_code' => $ChangeAdds->row()->country));
            $MainShipCost = number_format($ShipCostVal->row()->shipping_cost, 2, '.', '');
            $MainTaxCost = number_format(($amt * 0.01 * $ShipCostVal->row()->shipping_tax), 2, '.', '');
            $TotalAmts = number_format((($amt + $MainShipCost + $MainTaxCost) - $disamt), 2, '.', '');
            $condition = array('user_id' => $UserId);
            $dataArr2 = array('shipping_cost' => $MainShipCost, 'tax' => $ShipCostVal->row()->shipping_tax);
            $this->cart_model->update_details(SHOPPING_CART, $dataArr2, $condition);
			$returnArr['status'] = (string)1;
			$message = "Shipping Address Changed Successfully";
        } else {
            $returnArr['response'] = 'Some parameters are missing !!';
        }
		echo json_encode($returnArr);
    }

	/*
	!! >>>>>>>>>> Delete Shippping Address <<<<<<<<<<< !!
	*/
	public function deleteShippingAddress() {
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId  =  $_POST['UserId'];
        if ($UserId == '') {
			$returnArr['response'] = 'UserId Null !!';
        }else {
				$shippingAddrId = $_POST['shippingAddrId'];
				if($shippingAddrId !=""){
					$checkAddrCount = $this->cart_model->get_all_details(SHIPPING_ADDRESS, array('id' => $shippingAddrId, 'primary' => 'Yes'));
					if ($checkAddrCount->num_rows == 0) {
							$this->cart_model->commonDelete(SHIPPING_ADDRESS, array('id' => $shippingAddrId));
							$returnArr['status'] = 1;
							$returnArr['response'] = 'Shipping Address Successfully Deleted';
					}else {
							$returnArr['response'] = 'Primary Address Cannot be Deleted';
					}
				}else{
					$returnArr['response'] = 'Shipping Address Id is Empty';
				}
        }
		echo json_encode($returnArr);
    }

	/*
	||  >>>>>>>>>>>> Check Coupen Code <<<<<<<<<<<< ||
	*/

	public function checkCoupenCode() {
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = $this->input->post('UserId');
        $Code = $this->input->post('couponCode');
		$cartAmount = $this->input->post('cartAmount');
        $amount = $this->input->post('totalAmount');
        $shipamount = $this->input->post('shipAmount');
		if($cartAmount > 0){
			if($Code != ""){
					if($amount != "" && $shipamount != "" && $UserId !=""){
						$response = $this->mobile_model->Check_Code_Val($Code, $amount, $shipamount, $UserId);
						$returnArr1 = explode('|',$response);
						if ($response == 1) {
							$returnArr['response'] = 'Entered code is invalid';
						} else if ($response == 2) {
							$returnArr['response'] = 'Code is already used';
						} else if ($response == 3) {
							$returnArr['response'] = 'Please add more items in the cart and enter the coupon code';
						} else if ($response == 4) {
							$returnArr['response'] = 'Entered Coupon code is not valid for this product';
						} else if ($response == 5) {
							$returnArr['response'] = 'Entered Coupon code is expired';
						} else if ($response == 6) {
							$returnArr['response'] = 'Entered code is Not Valid';
						} else if ($response == 7) {
							$returnArr['response'] = 'Please add more items quantity in the particular category or product, for using this coupon code';
						} else if ($response == 8) {
							$returnArr['response'] = 'Entered Gift code is expired';
						} else if ($returnArr1[0] == 'Success') {
							$returnArr['status'] = 1;
							$message = "Coupan Code Applied Successfully";
							$returnArr['response'] = $message;
						}
				}else{
					$returnArr['response'] = 'Some Parameters are missing !!';
				}
			}else{
				$returnArr['response'] = 'Please Enter Code';
			}
		}else{
			$returnArr['response'] = 'Please add items in cart and enter the coupon code';
		}
		echo json_encode($returnArr);
    }

	/*
	||  >>>>>>>>>>>> Remove Coupen Code <<<<<<<<<<<< ||
	*/

	public function removeCoupenCode() {
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = $this->input->post('UserId');
		if($UserId != ""){
			$Code = $this->input->post('couponCode');
			if($UserId !="" && $Code != ""){
				$condition = array('user_id' => $UserId,"couponCode" => $Code);
				$couponCodeDetails = $this->mobile_model->get_all_details(SHOPPING_CART,$condition);
				if($couponCodeDetails->num_rows() > 0){
					$this->mobile_model->Check_Code_Val_Remove($UserId);
					$returnArr['status'] = 1;
					$message = "Coupon Code Removed Successfully";
					if($this->lang->line('json_success') != ""){
						$message = stripslashes($this->lang->line('json_success'));
					}
					$returnArr['response'] = $message;
				}else{
					$returnArr['response'] = 'Something Went Wrong!!';
				}
			}else{
				$returnArr['response'] = 'Some Parameters are missing !!';
			}
		}else{
			$returnArr['response'] = 'UserId Null !!';
		}
		echo json_encode($returnArr);
	}

	/*
	* !! >>>>>>>>>>>>> Seller Signup <<<<<<<<<<<< !!
	*/
	public function sellerSignUp(){
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId  =  $_POST['UserId'];
		if(!empty($UserId)){
			$this->data['userProfileDetails'] = $this->mobile_model->get_all_details(USERS, array('id' => $UserId, 'status' => 'Active'));
	        if ($this->data['userProfileDetails']->row()->is_verified == 'Yes') {
				$excludeArr = array('UserId');
				$dataArr = array('request_status' => 'Pending');
				$this->mobile_model->commonInsertUpdate(USERS, 'update', $excludeArr, $dataArr, array('id' => $UserId));
				$lg_err_msg = 'Welcome onboard ! Our team is evaluating your request. We will contact you shortly';
				$returnArr['status'] = 1;
				$returnArr['response'] = $lg_err_msg;
			}else{
				if ($this->lang->line('cfm_mail_fst') != ''){
	                $lg_err_msg = $this->lang->line('cfm_mail_fst');
				}else{
	                $lg_err_msg = 'Please confirm your email first';
				}
				$returnArr['status'] = 0;
			    $returnArr['response'] = $lg_err_msg;
			}
		}else{
			$returnArr['status'] = 0;
	        $returnArr['response'] = 'Please Login';
		}
		echo json_encode($returnArr);
	}

	/*
	|| <<<<<<<<<<<<< Category Details >>>>>>>>>>> ||
	*/
	public function  sellerProductCategoryDetails(){
		$returnArr['status'] = '0';
		$returnArr['response'] = '';
		$pid = $_POST['sellerProductId'];
		if($pid == ""){
			$returnArr['response'] = 'SellerProduct Id Null !!';
		}else{
			$productDetails = $this->product_model->get_all_details(PRODUCT, array('seller_product_id' => $pid));
			if($productDetails->num_rows == 0){
				$returnArr['response'] = 'Product Not Found !!';
			}else{
				$catListArr = explode(',', $productDetails->row()->category_id);
				$CategoryList = array();
				$nullValue= array();
				$select_qry = "select * from ".CATEGORY." where rootID=0 and status='Active'";
				$categoryList = $this->mobile_model->ExecuteQuery($select_qry);
				if($categoryList == ""){
					$returnArr['response'] = 'No categories Found For This Product !!';
				}else{
					foreach($categoryList->result() as $rowMain){
						$checked1 = 0;
						if(in_array($rowMain->id,$catListArr))$checked1 = 1;
						$select_qry2 = "select * from ".CATEGORY." where rootID=".$rowMain->id." and status='Active'";
						$categoryList2 = $this->mobile_model->ExecuteQuery($select_qry2);
						if($categoryList2->num_rows() > 0){
							foreach($categoryList2->result() as $rowsubcat1){
								$checked2 = 0;
								if(in_array($rowsubcat1->id,$catListArr))$checked2 = 1;
								$select_qry3 = "select * from ".CATEGORY." where rootID=".$rowsubcat1->id." and status='Active'";
								$categoryList3 = $this->mobile_model->ExecuteQuery($select_qry3);
								if($categoryList3->num_rows() > 0){
									foreach($categoryList3->result() as $rowsubcat2){
										$checked3 = 0;
										if(in_array($rowsubcat2->id,$catListArr))$checked3 = 1;
										$thirdCategory[] = array(
																					'catId' => $rowsubcat2->id,
																					'catName' => $rowsubcat2->cat_name,
																					'checked' => $checked3,
																					'seourl' => $rowsubcat2->seourl,
																					'level' => 2,
																					$rowsubcat2->seourl => $nullValue
																				);
									}
								}
								$secondCategory[] = array(
																			'catId' => $rowsubcat1->id,
																			'catName' => $rowsubcat1->cat_name,
																			'checked' => $checked2,
																			'seourl' => $rowsubcat1->seourl,
																			'level' => 1,
																			$rowsubcat1->seourl => $thirdCategory
																		);
								$thirdCategory =array();
							}
						}
						$CategoryList[] = array(
																	'catId' => $rowMain->id,
																	'catName' => $rowMain->cat_name,
																	'checked' => $checked1,
																	'seourl' => $rowMain->seourl,
																	'level' => 0,
																	$rowMain->seourl => $secondCategory
																);
						$secondCategory = array();
					}
				}
				$returnArr['status'] = '1';
				$message = "Success";
				if($this->lang->line('json_success') != ""){
					$message = stripslashes($this->lang->line('json_success'));
				}
				$returnArr['response'] = $message;
				$returnArr['categoryList'] = $CategoryList;
			}
		}
		echo json_encode($returnArr);
	}

	/*
	|| <<<<<<<<<<<<< Remove Attributes Details >>>>>>>>>>> ||
	*/
	public function  sellerProductRemoveAttributesDetails(){
		$returnArr['status'] = '0';
		$returnArr['response'] = '';
		$attributeId = $_POST['attributeId'];
		if($attributeId == ""){
			$returnArr['response'] = 'Attribute Id Null !!';
		}else{
			$this->product_model->commonDelete(SUBPRODUCT, array('pid' => $attributeId));
			$returnArr['status'] = '1';
			$message = "Successfully Removed";
			if($this->lang->line('json_successfully_removed') != ""){
				$message = stripslashes($this->lang->line('json_successfully_removed'));
			}
			$returnArr['response'] = $message;
		}
		echo json_encode($returnArr);
	}

	/*
	||| PRODUCT ATTRIBUTES EDIT|||
	*/
	public function insertEditSellingProductAttribute(){
		$pid = $_POST['sellingProductId'];
		if($pid != ""){
			$dataArr = array('seller_product_id' => $pid);
			$checkProduct = $this->product_model->get_all_details(PRODUCT, $dataArr);
			if ($checkProduct->num_rows() == 1) {
				$prodId = $checkProduct->row()->id;
				$Attr_name_str = $Attr_val_str = '';
				$Attr_type_arr = explode(',', $this->input->post('product_attribute_type'));
				$Attr_name_arr = explode(',', $this->input->post('product_attribute_name'));
				$Attr_val_arr = explode(',', $this->input->post('product_attribute_val'));
				$Attr_qty_arr = explode(',',$this->input->post('product_attribute_qty'));
				if (is_array($Attr_type_arr) && count($Attr_type_arr) > 0) {
					for ($k = 0; $k < sizeof($Attr_type_arr); $k++) {
						$dataSubArr = '';
						$dataSubArr = array('product_id' => $prodId, 'attr_id' => $Attr_type_arr[$k], 'attr_name' => $Attr_name_arr[$k], 'attr_price' => $Attr_val_arr[$k],'attr_qty'=>$Attr_qty_arr[$k]);
						$this->product_model->add_subproduct_insert($dataSubArr);
					}
					/*
					|| ===|| New Changes ||=== ||
					*/
					$existQty = $this->db->select_sum('attr_qty','Qty')->from(SUBPRODUCT)->where('product_id',$prodId)->get();
					if($this->input->post('attribute_must') =='yes'){
						$newQty = $existQty->row()->Qty;
						$this->product_model->update_details(PRODUCT,array('quantity'=>$newQty),array('id'=>$prodId));
					}else{
						$_product = $this->product_model->get_all_details(PRODUCT,array('id'=>$prodId));
						$sumQty = $existQty->row()->Qty;
						if($sumQty > $_product->row()->quantity){
							$newQty = $sumQty;
							$this->product_model->update_details(PRODUCT,array('quantity'=>$newQty),array('id'=>$prodId));
						}
					}
					/*
					|| ===|| New Changes ||=== ||
					*/
				}

				$returnArr['status'] = '1';
				if ($this->lang->line('change_saved') != ''){
					$lg_err_msg = $this->lang->line('change_saved');
				}else{
					$lg_err_msg = 'Yeah ! changes have been saved';
				}
				$returnArr['response'] = $lg_err_msg;
			}else {
				if ($this->lang->line('prod_not_found_db') != ''){
					$lg_err_msg = $this->lang->line('prod_not_found_db');
				}else{
					$lg_err_msg = 'Product not found in database';
				}
				$returnArr['response'] = $lg_err_msg;
			}
		}else{
			$returnArr['response'] = "SellingProductId Null !!";
		}
		echo json_encode($returnArr);
	}

	/*
	|| <<<<<<<<<<<<< Selling Product Images List and Remove Images >>>>>>>>>>> ||
	*/
	public function  SellingProductImages(){
		$returnArr['status'] = '0';
		$returnArr['response'] = '';
		$pid = $_POST['sellingProductId'];
		$mode = $_POST['mode'];
		if($pid != ""){
			$productDetails = $this->product_model->get_all_details(PRODUCT, array('seller_product_id' => $pid));
			if($mode == 'remove'){
		        $id = $_POST['imageId'];
				if($id == ""){
					$returnArr['response'] = 'ImageId Null !!';
				}else{
					$imgArr = explode(',', $productDetails->row()->image);
					$newImageArray = array_filter($imgArr);
					if(count($newImageArray) > 1){
						$image = $newImageArray[$id];
						unset($newImageArray[$id]);
						$dir = getcwd().DIRECTORY_SEPARATOR ."images".DIRECTORY_SEPARATOR ."product".DIRECTORY_SEPARATOR ;
						foreach (glob($dir."*.*") as $file){
							if ($file == $dir.$image ){
								if(!is_dir($file) ){
									unlink($file);
								}
							}
						}
						$dir1 = getcwd().DIRECTORY_SEPARATOR ."images".DIRECTORY_SEPARATOR ."product".DIRECTORY_SEPARATOR."thumb".DIRECTORY_SEPARATOR ;
						foreach (glob($dir1."*.*") as $file1){
							if ($file1 == $dir1.$image ){
								if(!is_dir($file1) ){
									unlink($file1);
								}
							}
						}
						$newImageString = implode(',',$newImageArray);
						$dataArr = array('image' => $newImageString);
			            $condition = array('seller_product_id' => $pid);
			            $this->product_model->update_details(PRODUCT, $dataArr, $condition);
						$returnArr['status'] = '1';
						$returnArr['response'] = 'Image removed Successfully ';
					}else{
						$returnArr['response'] = 'You cant delete all the images !!';
					}
				}
			}else{
				$imgArr = explode(',', $productDetails->row()->image);
				$i = 0;
				foreach($imgArr as $imgRow){
					if(!empty($imgRow)){
						$imageDetails = base_url().'images/product/'.$imgRow;
						$images[] = array(
													'imageId' => (string)$i,
													'ImageName' => trim($imgRow),
													'images' => $imageDetails
												);
					}
				$i++;
				}
				$returnArr['status'] = '1';
				$message = "Success";
				if($this->lang->line('json_success') != ""){
					$message = stripslashes($this->lang->line('json_success'));
				}
				$returnArr['response'] = $message;
				$returnArr['productId'] = $productDetails->row()->id;
				$returnArr['images'] = $images;
			}
		}else{
			$returnArr['response'] = 'Seller Product Id Null !!';
		}
		echo json_encode($returnArr);
	}


	/*
	|| <<<<<<<<<<<<< Seo Details >>>>>>>>>>> ||
	*/
	public function  sellerProductSeoDetails(){
		$returnArr['status'] = '0';
		$returnArr['response'] = '';
		$pid = $_POST['sellingProductId'];
		$seoDetails = array();
		if($pid == ""){
			$returnArr['response'] = 'SellerProduct Id Null !!';
		}else{
			$productDetails = $this->product_model->get_all_details(PRODUCT, array('seller_product_id' => $pid));
			$seoDetails = array(
				'metaTitle' => $productDetails->row()->meta_title,
				'metaKeyword' => $productDetails->row()->meta_keyword,
				'metaDescription' => strip_tags($productDetails->row()->meta_description)
			);
			$returnArr['status'] = '1';
			$message = "Success";
			if($this->lang->line('json_success') != ""){
				$message = stripslashes($this->lang->line('json_success'));
			}
			$returnArr['response'] = $message;
			$returnArr['seoDetails'] = $seoDetails;
		}
		echo json_encode($returnArr);
	}

	/*
	|| This Function is to Update seo details of Product ||
	*/
	public function updateProductSeoDetails(){
		$returnArr['status'] = '0';
		$returnArr['response'] = '';
		$pid = $_POST['sellingProductId'];
		if($pid != ""){
			$excludeArr = array('sellingProductId');
			$this->product_model->commonInsertUpdate(PRODUCT, 'update', $excludeArr, array(), array('seller_product_id' => $pid));
			if ($this->lang->line('change_saved') != ''){
				$lg_err_msg = $this->lang->line('change_saved');
			}
			else{
				$lg_err_msg = 'Yeah ! changes have been saved';
			}
			$returnArr['status'] = '1';
			$returnArr['response'] = $lg_err_msg;
		}else{
			$returnArr['response'] = "SellerProductId Null !!";
		}
		echo json_encode($returnArr);
	}

	/*
	|| <<<<<<<<<<<<< List Details >>>>>>>>>>> ||
	*/
	public function  sellerProductListDetails(){
		$returnArr['status'] = '0';
		$returnArr['response'] = '';
		$pid = $_POST['sellingProductId'];
		if($pid == ""){
			$returnArr['response'] = 'SellerProduct Id Null !!';
		}else{
			$listNames = array();
			$listValues = array();
			$selectlistValues = array();
			$elsectlist_Value = array();
			$selectedListDetails = array();
			$productDetails = $this->product_model->get_all_details(PRODUCT, array('seller_product_id' => $pid));
			if($productDetails->num_rows() > 0){
				$list_names_arr = explode(',', $list_names);
				$list_values = $productDetails->row()->list_value;
				$list_values_arr = array_filter(explode(',', $list_values));
				$listValuesString = implode(',',$list_values_arr);
				if(!empty($listValuesString)){
					$elsectlist_Value = $this->mobile_model->sellerProductListValue($listValuesString);
				}
				$list_Value = $this->product_model->get_all_details(LIST_VALUES, array('list_id' => '1'));
				if ($list_Value->num_rows() > 0) {
					foreach ($list_Value->result() as $listRow) {
							$listValues[] = array(
															'listId' => $listRow->id,
															'listValue' => $listRow->list_value
														);
					}
	            }
	            $newListArray = $listValues;
	            $value = '';
				$listName = "";
				if ($list_Value->num_rows()>0){
					$value="1";
					if($this->lang->line('color') != ''){
						$listName = stripslashes($this->lang->line('color')); } else $listName = "Color";
				}
				if(!empty($listValuesString)){
					if ($elsectlist_Value->num_rows() > 0) {
						foreach ($elsectlist_Value->result() as $listRow) {
							$selectlistValues[] = array(
								'listId' => $value,
								'listName' => $listName,
								'listValueId' => $listRow->id,
								'listValueName' => $listRow->list_value
							);
						}
					}
					$selectlistValues = $selectlistValues;
				}


				/*	if (count($list_names_arr)>0){*/
						/*$listValueId = "";
						$listValueName = "";
						for($i=0;$i<count($list_names_arr);$i++){
							foreach ($elsectlist_Value->result() as $listRows) {
								if ($listRows->id == $list_values_arr[$i]){
									$listValueId = $listRows->id;
									$listValueName = $listRows->list_value;
								}
							}
							$selectedListDetails[] = array(
													'listId' => $value,
													'listName' => $listName,
													'listValueId' => $listValueId,
													'listValueName' =>$listValueName
												);
						}*/
				$returnArr['status'] = '1';
				$message = "Success";
				if($this->lang->line('json_success') != ""){
					$message = stripslashes($this->lang->line('json_success'));
				}
				$returnArr['response'] = $message;
				$returnArr['listValues'] = $newListArray;
				$returnArr['selectedListDetails'] = $selectlistValues;
				//}
		    }else{
		    	$returnArr['response'] = " Invalid seller id ";
		    }
		}
		echo json_encode($returnArr);
	}

	/*
	|| <<<<<<<<<<<<< List Details >>>>>>>>>>> ||
	*/
	public function  updateListDetails(){
		$returnArr['status'] = '0';
		$returnArr['response'] = '';
		$pid = $_POST['sellingProductId'];
		if($pid != ""){
				$list_name_str = $this->input->post('attribute_name');
				$list_val_str = $this->input->post('attribute_val');
				$dataArr = array('list_name' => $list_name_str, 'list_value' => $list_val_str);
				$this->product_model->update_details(PRODUCT, $dataArr, array('seller_product_id' => $pid));
				if (is_array($list_val_arr)) {
					foreach ($list_val_arr as $list_val_row) {
						$list_val_details = $this->product_model->get_all_details(LIST_VALUES, array('id' => $list_val_row));
						if ($list_val_details->num_rows() == 1) {
							$product_count = $list_val_details->row()->product_count;
							$products_in_this_list = $list_val_details->row()->products;
							$products_in_this_list_arr = explode(',', $products_in_this_list);
							if (!in_array($pid, $products_in_this_list_arr)) {
								array_push($products_in_this_list_arr, $pid);
								$product_count++;
								$list_update_values = array(
										'products' => implode(',', $products_in_this_list_arr),
										'product_count' => $product_count
								);
								$list_update_condition = array('id' => $list_val_row);
								$this->product_model->update_details(LIST_VALUES, $list_update_values, $list_update_condition);
							}
						}
					}
				}
				$returnArr['status'] = '1';
				if ($this->lang->line('change_saved') != ''){
					$lg_err_msg = $this->lang->line('change_saved');
				}else{
					$lg_err_msg = 'Yeah ! changes have been saved';
				}
				$returnArr['response'] = $lg_err_msg;
		}else{
			$returnArr['response'] = 'SellerProductId Missing !!';
		}
		echo json_encode($returnArr);
	}
/*
  this function for getsubshipping

*/
	 public function getsubshipping() {

	 	$returnArr['status'] = '0';
		$returnArr['response'] = '';
        $product_id = $this->input->post('product_id');
        $userId = $this->input->post('UserId');
        $sellerProductId = $this->input->post('sellerProductId');
       if($userId != "" && $sellerProductId != "" ){
         $userdetails = $this->product_model->get_all_details(USERS, array('id' =>$userId));
         if($userdetails->num_rows() > 0){
             $product_details = $this->product_model->get_all_details(PRODUCT, array('user_id' =>$userId,'seller_product_id'=>$sellerProductId));
              if($product_details->num_rows() > 0){
	              $product_row	= $product_details->result();
	              $subshipping = $this->product_model->view_subshipping_details_join($product_row[0]->id);
			        if($subshipping->num_rows() > 0){
			        	foreach ($subshipping->result() as  $shippingrow) {
				         $dataArrVal[] = array(
				         					'product_id'=>$shippingrow->product_id,
				         					'ship_id'=>$shippingrow->ship_id,
				         					'ship_name'=>$shippingrow->ship_name,
				         					'ship_code'=>$shippingrow->ship_code,
				         					'ship_cost'=>$shippingrow->ship_cost,
				         					'ship_seourl'=>$shippingrow->ship_seourl,
				         					'ship_other_cost'=>$shippingrow->ship_other_cost
				         					);
				     }
				        $returnArr['status'] = '1';
				        $returnArr['shipping_Details'] = $dataArrVal;
				 }else{
			    	$returnArr['response'] = " No Shipping address   ";
			    }
		    }else{
		    	$returnArr['response'] = " Invalid Product  ";
		    }
		}else{
		    	$returnArr['response'] = " Invalid User  ";
		}
    }else{
			$returnArr['response'] = " Some parameters missing  ";
    }
    echo json_encode($returnArr);
   }

	/*
		this function for getsubshipping
	*/
	 public function inserEditSubshipping() {

	 	$returnArr['status'] = '0';
		$returnArr['response'] = '';
        $userId = $this->input->post('UserId');
        $sellerProductId = $this->input->post('sellerProductId');

       if($userId != "" && $sellerProductId != "" ){

         $userdetails = $this->product_model->get_all_details(USERS, array('id' =>$userId));
         if($userdetails->num_rows() > 0){
             $product_details = $this->product_model->get_all_details(PRODUCT, array('user_id' =>$userId,'seller_product_id'=>$sellerProductId));
              if($product_details->num_rows() > 0){
              $product_row	= $product_details->result();
              $subshipping = $this->product_model->view_subshipping_details_join($product_row[0]->id);
                $prodId = $product_row[0]->id;
				$product_ship_id_arr = explode(',', $this->input->post('product_ship_id'));
				$product_ship_name_arr = explode(',', $this->input->post('product_ship_name'));
				$product_ship_code_arr = explode(',', $this->input->post('product_ship_code'));
				$product_ship_cost_arr = explode(',',$this->input->post('product_ship_cost'));
				$product_ship_seourl_arr = explode(',', $this->input->post('product_ship_seourl'));
				$product_ship_other_cost_arr = explode(',',$this->input->post('product_ship_other_cost'));

	        if($subshipping->num_rows() > 0){
	        	
	        	$this->product_model->commonDelete(SUB_SHIPPING, array('product_id' => $prodId));

				if (is_array($product_ship_code_arr) && count($product_ship_code_arr) > 0) {
					for ($k = 0; $k < sizeof($product_ship_code_arr); $k++) {
					$country_details = $this->product_model->get_all_details(COUNTRY_LIST, array('country_code' =>$product_ship_code_arr[$k]));
					//$ship_name = @explode('|', $ship_to[$i]);
						$country_detailsrow = $country_details->result();
					if($product_ship_name_arr[$k] == 'Everywhere Else'){
						$shipName = 'Everywhere Else';
						$shipCode = "";
						$shipId = 232;
					} else {

						$shipName = $country_detailsrow[0]->name;

						$shipCode = $country_detailsrow[0]->country_code;
						$shipId = $country_detailsrow[0]->id;
					}
					$seourlBase = $seourl = url_title($shipName, '-', TRUE);

					$seourl_check = '0';
					$duplicate_url = $this->product_model->get_all_details(SUB_SHIPPING,array('ship_seourl'=>$seourl));
					if($duplicate_url->num_rows()>0){
						$seourl = $seourlBase.'-'.$duplicate_url->num_rows();
					}else{
						$seourl_check = '1';
					}
					$urlCount = $duplicate_url->num_rows();
					while($seourl_check == '0'){
						$urlCount++;
						$duplicate_url = $this->product_model->get_all_details(SUB_SHIPPING,array('ship_seourl'=>$seourl));
						if ($duplicate_url->num_rows()>0){
							$seourl = $seourlBase.'-'.$urlCount;
						}else {
							$seourl_check = '1';
						}
					}
					
						$dataSubArr = '';
						$dataSubArr = array(
							   'product_id' => $prodId,
							   'ship_id' => $shipId,
							   'ship_name' => $shipName,
							   'ship_code' => $shipCode,
							   'ship_cost'=>$product_ship_cost_arr[$k],
							   'ship_seourl'=>$seourl,
							   'ship_other_cost'=>$product_ship_other_cost_arr[$k]
							   );
						$this->product_model->add_subproduct_ship_insert($dataSubArr);
					}

				}

		        $returnArr['status'] = '1';
		        $returnArr['response'] = 'Shipping Update Successfully';
			 }else{

		 	if (is_array($product_ship_code_arr) && count($product_ship_code_arr) > 0) {
           
					for ($k = 0; $k < sizeof($product_ship_code_arr); $k++) {

						$country_details = $this->product_model->get_all_details(COUNTRY_LIST, array('country_code' =>$product_ship_code_arr[$k]));
					//$ship_name = @explode('|', $ship_to[$i]);
						$country_detailsrow = $country_details->result();

					if($product_ship_name_arr[$k] == 'Everywhere Else'){
						$shipName = 'Everywhere Else';
						$shipId = 232;
						$shipCode = "";
					} else {

						$shipName = $country_detailsrow[0]->name;
						$shipCode = $country_detailsrow[0]->country_code;
						$shipId = $country_detailsrow[0]->id;

					}

					$seourlBase = $seourl = url_title($shipName, '-', TRUE);
					$seourl_check = '0';
					$duplicate_url = $this->product_model->get_all_details(SUB_SHIPPING,array('ship_seourl'=>$seourl));
					if($duplicate_url->num_rows()>0){
						$seourl = $seourlBase.'-'.$duplicate_url->num_rows();
					}else{
						$seourl_check = '1';
					}
					$urlCount = $duplicate_url->num_rows();
					while($seourl_check == '0'){
						$urlCount++;
						$duplicate_url = $this->product_model->get_all_details(SUB_SHIPPING,array('ship_seourl'=>$seourl));
						if ($duplicate_url->num_rows()>0){
							$seourl = $seourlBase.'-'.$urlCount;
						}else {
							$seourl_check = '1';
						}
					}
						$dataSubArr = '';
						if($product_ship_code_arr[$k]!= NULL){
							 $shicode = $product_ship_code_arr[$k];
						}else{
							$shicode = "";
							
						}
						

						$dataSubArr = array(
							   'product_id' => $prodId,
							   'ship_id' => $product_ship_id_arr[$k],
							   'ship_name' => $product_ship_name_arr[$k],
							   'ship_code' => $shicode,
							   'ship_cost'=>$product_ship_cost_arr[$k],
							   'ship_seourl'=>$product_ship_seourl_arr[$k],
							   'ship_other_cost'=>$product_ship_other_cost_arr[$k]
							   );
						

						$this->product_model->add_subproduct_ship_insert($dataSubArr);
					}
					
				}

		        $returnArr['status'] = '1';
		        $returnArr['response'] = 'Shipping Added Successfully';
	    }
	    }else{
	    	$returnArr['response'] = " Invalid Product  ";
	    }
	}else{
	    	$returnArr['response'] = " Invalid User  ";

	}
    }else{

			$returnArr['response'] = " Some parameters missing  ";

    }
    echo json_encode($returnArr);
   }


/*  this function for get user profile  detials   */

   public function user_profile(){
		////error_reporting(0);
   	   $user_name = $this->input->post('user_name');
		$userProfile=array();
		$userDetails=$this->mobile_model->get_all_details(USERS,array( 'user_name' => $user_name,'status' =>'Active'));
		if($userDetails->num_rows()>0){
			$user_id=$userDetails->row()->id;
			if($userDetails->row()->thumbnail!=""){
				$userImage=base_url().'images/users/thumb/'.$userDetails->row()->thumbnail;
			}else{
				$userImage=base_url().'images/users/thumb/profile_pic.png';
			}
			if($userDetails->row()->followers_count !='' && $userDetails->row()->followers_count !=NULL){
			$followerscount=$userDetails->row()->followers_count;
			}else{
			$followerscount =0;
			}
			if($userDetails->row()->following_count !='' && $userDetails->row()->following_count !=NULL){
			$followingcount=$userDetails->row()->following_count;
			}else{
			$followingcount =0;
			}
			if($userDetails->row()->city !=''){
			$user_city= $userDetails->row()->city;
			}else{
			$user_city= '';
			}
			$userInfo[]=array('user_name'=>$userDetails->row()->user_name,
										'userFullName'=>$userDetails->row()->full_name.' '.$userDetails->row()->last_name,
										'userImage'=>$userImage,
										'userLocation'=>$user_city,
										'userGender'=>(string)$userDetails->row()->gender,
										'about'=>(string)$userDetails->row()->about,
										'userJoined'=>date("F d Y",strtotime($userDetails->row()->created)),
										'followersTotal'=>$followerscount,
										'followingTotal'=>$followingcount,
										'gcm_id'=>$userDetails->row()->gcm_buyer_id,
										'ios_id'=>$userDetails->row()->ios_device_id

										);

			#User Favorite Item List
			$userfavDetails=$this->mobile_model->getFavoriteListProduct($user_id,5,0)->result_array();

			//print_r($this->mobile_model->db->last_query());die;
			$userfavDetailsTotal=$this->mobile_model->getFavoriteListProduct_count($user_id);
				$favItems=array();
				$params = array();
				if(count($userfavDetails)>0){
					$favItems=array();
					$favProductList=array();
					$prdcount=4;
					for($i=0;$i<$prdcount;$i++){
						$val=$i+1;
						if($i<count($userfavDetails)){
							$img=explode(',',$userfavDetails[$i]['product_image']);
							$image=base_url().'images/product/mb/thumb/'.$img[0];
							$params[] = $img[0];
						}else{
							$image="";
						}
					}
						$image = "";
						$image1="";
						$image2 = "";
						$image3="";
						$image4="";
						if(count($params) > 0){
					//$image = $this->newimagemerger($params,'favProduct');
					for($i=0;$i<count($params);$i++){
							if($i==0){
							$image1=base_url().'images/product/mb/thumb/'.$params[$i];
							}
							if($i==1){
							$image2=base_url().'images/product/mb/thumb/'.$params[$i];

							}
							if($i==2){
							$image3=base_url().'images/product/mb/thumb/'.$params[$i];

							}
							if($i==3){
							$image4=base_url().'images/product/mb/thumb/'.$params[$i];
							}
							}
					}
						$favItems[]=array('listName'=>'Items I Love',
													'Link'=>'json/'.$userDetails->row()->user_name.'/favorite',
													'image1'=>$image1,
													'image2'=>$image2,
													'image3'=>$image3,
													'image4'=>$image4,
													'favCount'=>(string)$userfavDetailsTotal->num_rows());
				}
			#User Favorite Shop List

			$userfavShops=$this->mobile_model->getFavoriteListShop($user_id);
			$favShops=array();
			if($userfavShops->num_rows()>0){
				$shopCount=0;
				foreach($userfavShops->result() as $favShop){
					$shopCount++;
					if($shopCount<=3){
						$favShopsPrds=array();
						$shopPrds=$this->mobile_model->get_shopProducts($favShop->shopId,5,0)->result_array();
						$shopPrdsCount=$this->mobile_model->get_shopProducts_count($favShop->shopId);
						$params = array();
						$url ="http://192.168.1.251:8081//shopsy-v2////images//product//list-image//1439204441-81iovdzwofl._UL1500_";
						//print_r(basename($url));die;
						$path_parts = pathinfo($url);
						//print_r($path_parts['extension']);die;
						if(count($shopPrds)>0){
							$prdcount=4;
							for($i=0;$i<$prdcount;$i++){
								if($i<count($shopPrds)){
									$img=explode(',',$shopPrds[$i]['productImage']);
									$image=base_url().'images/product/mb/thumb/'.$img[0];
									$params[] = $img[0];
								}else{
									$image="";
								}
								$favShopsPrds[]=array('image'=>$image);
							}
						}
						$image = "";
						$image1="";
						$image2 = "";
						$image3="";
						$image4="";
						if(count($params) > 0){
							//$image = $this->newimagemerger($params,'favshop');
							for($i=0;$i<count($params);$i++){
								if($i==0){
									$image1=base_url().'images/product/mb/thumb/'.$params[$i];
								}
								if($i==1){
									$image2=base_url().'images/product/mb/thumb/'.$params[$i];
								}
								if($i==2){
									$image3=base_url().'images/product/mb/thumb/'.$params[$i];
								}
								if($i==3){
									$image4=base_url().'images/product/mb/thumb/'.$params[$i];
								}
							}
						}

						if($favShop->sellerImage!=""){
							$sellerImage=base_url().'images/users/thumb/'.$favShop->sellerImage;
						}else{
							$sellerImage=base_url().'images/users/thumb/profile_pic.png';
						}
						if($favShop->seller_banner!=''){
							$seller_banner=base_url().'images/banner/'.$favShop->seller_banner; }
						else {
							$seller_banner=base_url().'images/dummyProductImage.jpg';
						}

						$favShops[]=array('shopName'=>$favShop->shop_name,
										'image1'=>$image1,
										'image2'=>$image2,
										'image3'=>$image3,
										'image4'=>$image4,
										'sellerImage'=>$sellerImage,
										"seller_banner"=>$seller_banner,
										'prdCount'=>(string)$shopPrdsCount->row()->total,
										"shopname"=>$favShop->shop_name,
										"seller_username"=>$favShop->seller_username,"seller_firstname"=>$favShop->seller_firstname,
										"seller_lastname"=>$favShop->seller_lastname,
										"shopseourl"=>$favShop->shop_url);
					}
				}
			}
			$userProfile[]=array('userInfo'=>$userInfo,'favProduct'=>$favItems,'favShop'=>$favShops,'favShopCount'=>(string)$userfavShops->num_rows(),'favPrdCount'=>(string)$userfavDetailsTotal->num_rows());
		}
		//$shopArr[]=array('favShopCount'=>(string)$userfavShops->num_rows(),'favShop'=>$favShops);
		$json_encode = json_encode(array("userProfile" => $userProfile,"cartCount"=>(string)$this->data["cartCount"]));
		echo $json_encode;
	}

	/*
	|| <<<<<<<<<<<<< List Details >>>>>>>>>>> ||
	*/
	public function  updateListValues(){
		$returnArr['status'] = '0';
		$returnArr['response'] = '';
		$pid = $_POST['sellingProductId'];
		if($pid != ""){
				$list_name_str = $this->input->post('attribute_name');
				$list_val_str = $this->input->post('attribute_val');
				$dataArr = array('list_name' => $list_name_str, 'list_value' => $list_val_str);
				$this->product_model->update_details(PRODUCT, $dataArr, array('seller_product_id' => $pid));
				$returnArr['status'] = '1';
				if ($this->lang->line('change_saved') != ''){
					$lg_err_msg = $this->lang->line('change_saved');
				}else{
					$lg_err_msg = 'Yeah ! changes have been saved';
				}
				$returnArr['response'] = $lg_err_msg;
		}else{
			$returnArr['response'] = 'SellerProductId Missing !!';
		}
		echo json_encode($returnArr);
	}

	/*
	|| <<<<<<<<<<<<< USER FOLLOWERS >>>>>>>>>>> ||
	*/
	public function userFollowersList() {
		$returnArr['status'] = '0';
		$returnArr['response'] = '';
		$userId = $_POST['UserId'];
		if($userId != ""){
			$userProfileDetails = $this->user_model->get_all_details(USERS, array('id' => $userId));
			if ($userProfileDetails->num_rows() == 1) {
	            $fieldsArr = array('*');
	            $searchName = 'id';
	            $searchArr = explode(',', $userProfileDetails->row()->followers);
	            $joinArr = array();
	            $sortArr = array();
	            $limit = '';
	            $followingUserDetails = $this->product_model->get_fields_from_many(USERS, $fieldsArr, $searchName, $searchArr, $joinArr, $sortArr, $limit);
				$followersDetail = array();
				//echo "<pre>";print_r($followingUserDetails->result());die;
				if($followingUserDetails->num_rows() > 0){
					foreach ($followingUserDetails->result() as $userDetails) {
						$userImage = base_url()."images/users/user-thumb1.png";
						if($userDetails->thumbnail != ""){
							$userImage = base_url()."images/users/".$userDetails->thumbnail;
						}
						$followingList = $userDetails->following;
						$followingList = explode(',',$followingList);
						$followingListArr = array_filter($followingList);
						$following = 0;
						if(in_array($userDetails->id,$followingListArr)){
							$following = 1;
						}
						$followersDetail[] = array(
							"userId" => $userDetails->id,
							"fullName" => $userDetails->full_name,
							"userName" => $userDetails->user_name,
							"userImage" => $userImage,
							"following" => $following
						);
					}
					$returnArr['status'] = '1';
					$returnArr['response'] = 'Success';
				}
				$returnArr['followersDetails'] = $followersDetail;
	        } else {
	            $returnArr['response'] = 'No Such User Found !!';
	        }
		}else{
			$returnArr['response'] = 'UserId Null !!';
		}
		echo json_encode($returnArr);
    }

	/*
	|| <<<<<<<<<<<<< USER FOLLOWING >>>>>>>>>>> ||
	*/
	public function userFollowingList() {
		$returnArr['status'] = '0';
		$returnArr['response'] = '';
		$userId = $_POST['UserId'];
		if($userId != ""){
			$userProfileDetails = $this->user_model->get_all_details(USERS, array('id' => $userId));
	        if ($userProfileDetails->num_rows() == 1){
	            $fieldsArr = array('*');
	            $searchName = 'id';
	            $searchArr = explode(',', $userProfileDetails->row()->following);
	            $joinArr = array();
	            $sortArr = array();
	            $limit = '';
				$followingUserDetail = array();
	            $followingUserDetails = $this->product_model->get_fields_from_many(USERS, $fieldsArr, $searchName, $searchArr, $joinArr, $sortArr, $limit);
				if($followingUserDetails->num_rows() > 0){
					foreach ($followingUserDetails->result() as $userDetails) {
						$userImage = base_url()."images/users/user-thumb1.png";
						if($userDetails->thumbnail != ""){
							$userImage = base_url()."images/users/".$userDetails->thumbnail;
						}
						$following = 1;
						$followingUserDetail[] = array(
							"userId" => $userDetails->id,
							"fullName" => $userDetails->full_name,
							"userName" => $userDetails->user_name,
							"userImage" => $userImage,
							"following" => $following
						);
					}
					$returnArr['status'] = '1';
					$returnArr['response'] = 'Success';
				}
				$returnArr['followingUserDetail'] = $followingUserDetail;
	        } else {
	            $returnArr['response'] = 'No Such User Found !!';
	        }
		}else{
			$returnArr['response'] = 'UserId Null !!';
		}
		echo json_encode($returnArr);
    }

	/*
	|| <<<<<<<<<<<<< SELLER FEATURE PLAN >>>>>>>>>>> ||
	*/
	public function sellerFeatured() {
		$returnArr['status'] = '0';
		$returnArr['response'] = '';
		$userId = $_POST['UserId'];
		if($userId != ""){
			$selectedProdIds = array();
			$featuredProductDetails = array();
			$sellingProductList = array();
			$selectedPlan = array();
			$paymentTypes = array();
			$this->load->model('seller_plan_model', 'seller_plan');
			$featuredStorePlan = $this->seller_plan->get_all_details(SELLER_PLAN, array('status'=> 'Active'));
			$planDetails = array();
			$featured = 0;
			$checkFeatured = $this->seller_plan->get_all_details(FEATURED_SELLER, array('userId' => $userId, 'isExpired' => '0'));
			if ($checkFeatured->num_rows > 0){
				$selectedPlan = array(
											"featuredFrom" => $checkFeatured->row()->featuredOn,
											"featuredTo" => $checkFeatured->row()->expiresOn,
											"planPrice" => $checkFeatured->row()->planPrice,
											"planPeriod" => $checkFeatured->row()->planPeriod,
											"maxProductAllowed" => $checkFeatured->row()->productCount,
										);
				$featured = 1;
				$featuredProducts = $this->seller_plan->get_featured_product($userId, $checkFeatured->row()->id);
				if(count($featuredProducts) > 0){
					$i = 0;
					foreach ($featuredProducts as $deatils){
						$productImage =base_url().'images/product/dummyProductImage.jpg';
						if($deatils->image != ""){
							$img = explode(',',$deatils->image);
							$imgArr = array_filter($img);
							$productImage = base_url()."images/product/".$imgArr[0];
						}
						$selectedProdIds[$i] = $deatils->id;
						$featuredProductDetails[] = array(
																		"productID" => $deatils->id,
																		"sellerProductId" => $deatils->seller_product_id,
																		"productName" => $deatils->product_name,
																		"productImage" => $productImage
																	);
					    $i++;
					}
				}
				$sellerProducts = $this->product_model->view_product_details(' where p.user_id=' . $userId . ' and p.status="Publish"');
				if($sellerProducts->num_rows() > 0){
					foreach($sellerProducts->result() as $prodDetails){
						$productImage =base_url().'images/product/dummyProductImage.jpg';
						if($prodDetails->image != ""){
							$img = explode(',',$prodDetails->image);
							$imgArr = array_filter($img);
							$productImage = base_url()."images/product/".$imgArr[0];
						}
						$selected = 0;
						if(in_array($prodDetails->id, $selectedProdIds)){
							$selected = 1;
						}
						$sellingProductList[] = array(
																"productID" => $prodDetails->id,
																"sellerProductId" => $prodDetails->seller_product_id,
																"productName" => $prodDetails->product_name,
																"productImage" => $productImage,
																"selected" => $selected
															);
					}
				}
			}else{
				if($featuredStorePlan->num_rows() > 0){
				//	echo "<pre>";print_r($featuredStorePlan->result());die;
					foreach ($featuredStorePlan->result() as $ftStrePlan) {
						$planDetails[] = array(
													"planId" => $ftStrePlan->id,
													"planName" => $ftStrePlan->plan_name,
													"planPrice" => $ftStrePlan->plan_price,
													"planPeriod" => $ftStrePlan->plan_period,
													"maxProductAllowed" => $ftStrePlan->plan_product_count,
												);
					}
					$paymentGateways = $this->mobile_model->get_all_details(PAYMENT_GATEWAY, array('status' => "Enable"));
					$paymentTypes = array();
					if($paymentGateways->num_rows() > 0){
						foreach ($paymentGateways->result() as $gatewayData) {
							if($gatewayData->id != 4 && $gatewayData->id != 7){
								$paymentTypes[] = array(
														"paymentId" => $gatewayData->id,
														"paymentName" => $gatewayData->gateway_name,
													);
							}
						}
					}
				}
			}
			$returnArr['status'] = 1;
			$returnArr['response'] = 'Success';
			$returnArr['featured'] = $featured;
			$returnArr['planDetails'] = $planDetails;
			$returnArr['paymentTypes'] = $paymentTypes;
			$returnArr['featuredPlanDetails'] = $selectedPlan;
			$returnArr['featuredProductDetails'] = $featuredProductDetails;
			$returnArr['sellingProductList'] = $sellingProductList;
		}else{
			$returnArr['response'] = 'UserId Null !!';
		}
		echo json_encode($returnArr);
	}

	/*
	|| <<<<<<<<<<<<< SELLER FEATURE PLAN >>>>>>>>>>> ||
	*/
	public function payFeaturedPlan() {
		$userId = $_GET['UserId'];
		if($userId != ""){
			$planId = $_GET['planId'];
			if($planId != ""){
				$paymentId = $_GET['paymentId'];
				if($paymentId != ""){
					if($paymentId == "1"){
						$redirect = 'paypalPay';
					}else if($paymentId == "2"){
						$redirect = 'paypalCreditPayment';
					}else if($paymentId == "3"){
						$redirect = 'authorizePayment';
					}else if($paymentId == "4"){
						$redirect = 'stripePayment';
					}else if($paymentId == "5"){
						$redirect = 'featureTwoCheckout';
					}
					redirect(base_url() . 'site/ios/' . $redirect.'/'.$planId.'/'.$userId);
				}else{
					$returnArr['response'] = 'PaymentId Null !!';
				}
			}else{
				$returnArr['response'] = 'PlanId Null !!';
			}
		}else{
			$returnArr['response'] = 'UserId Null !!';
		}
		echo json_encode($returnArr);
	}

	/*
	|| <<<<<<<<<<<<< SELLER FEATURE PLAN PAYPAL PAY >>>>>>>>>>> ||
	*/
	public function paypalPay($planId = "", $userId = "") {
		if($userId != ""){
			if($planId != ""){
				$condition = array('id' => $userId, "status" => "Active");
				$userInfoDetails = $this->mobile_model->get_all_details(USERS,$condition);
				if($userInfoDetails->num_rows() == 1){
					$this->data['userDetails'] = $userInfoDetails;
					$this->load->model('seller_plan_model', 'seller_plan');
			        $this->data['plan'] = $this->seller_plan->get_all_details(SELLER_PLAN, array('id'=>$planId));
					$this->data['countryList'] = $this->mobile_model->get_all_details(COUNTRY_LIST, array(), array(array('field' => 'name', 'type' => 'asc')));
			        $this->load->view('mobile/featured/featuredPayPaypal', $this->data);
				}else{
					$this->data['errorMessage'] = "No Such User Found";
					$this->load->view("mobile/featured/failed",$this->data);
				}
			}else{
				$this->data['errorMessage'] = "PlanId Missing";
				$this->load->view("mobile/featured/failed",$this->data);
			}
		}else{
			$this->data['errorMessage'] = "UserId Missing";
			$this->load->view("mobile/featured/failed",$this->data);
		}
    }

	/*
	|| <<<<<<<<<<<<< SELLER FEATURE PLAN PAYPAL PAYMENT >>>>>>>>>>> ||
	*/
	public function paypal_payment() {
		//print_r($_POST);die;
		$UserId = $_POST['user_id'];
		if($UserId != ""){
			$this->load->model('seller_plan_model', 'seller_plan');
	        $listCheck = $this->seller_plan->get_all_details(FEATURED_SELLER, array('userId' => $UserId, 'isExpired' => '0'));
	        if ($listCheck->num_rows == 0) {
	            $plan = $this->seller_plan->get_all_details(SELLER_PLAN, array('id'=>$this->input->post('plan_id')));
	            $registeredSellers = $this->seller_plan->get_all_details(FEATURED_SELLER, array('isExpired' => '0'));
	            if ($registeredSellers->num_rows < $plan->row()->plan_promoted_count){
	                $time = time();
	                $datestring = "%Y-%m-%d %h:%i:%s";
	                $dataArr = array('userId' => $UserId, 'planPrice' => $plan->row()->plan_price, 'planPeriod' => $plan->row()->plan_period, 'productCount' => $plan->row()->plan_product_count, 'paymentType' => 'Paypal');
	                $dataArr['createdOn'] = mdate($datestring, $time);
	                $this->seller_plan->simple_insert('fc_featured_seller_temp', $dataArr);
	                $temp_id = $this->db->insert_id();
	                $this->load->library('paypal_class');
	                $item_name = $this->config->item('email_title') . ' Products';
	                $totalAmount = $this->input->post('total_price');
	                $loginUserId = $UserId;
	                //DealCodeNumber
	                //$lastFeatureInsertId = $this->session->userdata('randomNo');
	                $quantity = 1;
	                if ($this->input->post('paypalmode') == 'sandbox') {
	                    $this->paypal_class->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';   // testing paypal url
	                } else {
	                    $this->paypal_class->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';     // paypal url
	                }
	                $this->paypal_class->add_field('currency_code', $this->data['currencyType']);

	                $this->paypal_class->add_field('business', $this->input->post('paypalEmail')); // Business Email

	                $this->paypal_class->add_field('return', base_url() . 'site/ios/paypal_return/' . $temp_id.'?AppWebView=1'); // Return URL

	                $this->paypal_class->add_field('cancel_return', base_url() . 'site/ios/paypal_cancel/' . $temp_id.'?AppWebView=1'); // Cancel URL

	                $this->paypal_class->add_field('notify_url', base_url() . 'site/ios/paypal_notify/' . $temp_id.'?AppWebView=1'); // Notify url

	                $this->paypal_class->add_field('custom', $loginUserId . '|' . $temp_id . '|Product'); // Custom Values

	                $this->paypal_class->add_field('item_name', $item_name); // Product Name

	                $this->paypal_class->add_field('user_id', $loginUserId);

	                $this->paypal_class->add_field('quantity', $quantity); // Quantity
	                //echo $totalAmount;die;
	                $this->paypal_class->add_field('amount', $totalAmount); // Price
	                //$this->paypal_class->add_field('amount', 1); // Price
	                //			echo base_url().'order/success/'.$loginUserId.'/'.$lastFeatureInsertId; die;
	                $this->paypal_class->submit_paypal_post();
	            } else {
					$this->data['errorMessage'] = "'Maximum Number of featured Sellers reached";
		            $this->load->view("mobile/featured/failed",$this->data);
	            }
	        } else {
				$this->data['errorMessage'] = "You have Already Subscribed this Plan";
	            $this->load->view("mobile/featured/failed",$this->data);
	        }
		}else{
			$this->data['errorMessage'] = "User Id Missing";
			$this->load->view("mobile/featured/failed",$this->data);
		}
    }

	/*
	|| <<<<<<<<<<<<< SELLER FEATURE PLAN PAYPAL PAYMENT RETURN >>>>>>>>>>> ||
	*/
	public function paypal_return($id) {
	//	print_r($_POST);die;
        if (strtolower($this->input->post('payment_status')) == 'completed') {
            $this->load->model('seller_plan_model', 'seller_plan');
            $temp_store = $this->seller_plan->get_all_details('fc_featured_seller_temp', array('id' => $id));
            if ($temp_store->num_rows > 0) {
                $this->seller_plan->commonDelete('fc_featured_seller_temp', array('id' => $id));
                $userExists = $this->seller_plan->get_all_details(FEATURED_SELLER, array('userId' => $temp_store->row()->userId, 'isExpired' => '0'));
                if ($userExists->num_rows == 0) {
                    $dataArr = array('userId' => $temp_store->row()->userId, 'planPrice' => $temp_store->row()->planPrice, 'planPeriod' => $temp_store->row()->planPeriod, 'productCount' => $temp_store->row()->productCount, 'paymentType' => $temp_store->row()->paymentType);
                    $time = time();
                    $datestring = "%Y-%m-%d %h:%i:%s";
                    $dataArr['featuredOn'] = mdate($datestring, $time);
                    $dataArr['expiresOn'] = date("Y-m-d h:i:s", strtotime($dataArr['featuredOn'] . '+ ' . $temp_store->row()->planPeriod . ' days'));
                    $dataArr['createdOn'] = mdate($datestring, $time);
                    $this->seller_plan->simple_insert(FEATURED_SELLER, $dataArr);
                    $this->seller_plan->commonDelete('fc_featured_seller_temp', array('id' => $id));
					$this->data['errorMessage'] = "Payment Successfull";
					$this->load->view("mobile/featured/success",$this->data);
                }
            }
        } else {
			$this->data['errorMessage'] = "Payment Unsuccessfull";
			$this->load->view("mobile/featured/failed",$this->data);
        }
    }

	/*
	|| <<<<<<<<<<<<< SELLER FEATURE PLAN PAYPAL PAYMENT CANCEL >>>>>>>>>>> ||
	*/
	public function paypal_cancel($id) {
        $this->load->model('seller_plan_model', 'seller_plan');
        $temp_store = $this->seller_plan->get_all_details('fc_featured_seller_temp', array('id' => $id));
        if ($temp_store->num_rows > 0) {
            $this->seller_plan->commonDelete('fc_featured_seller_temp', array('id' => $id));
        };
		$this->data['errorMessage'] = "Payment Cancelled";
		$this->load->view("mobile/featured/failed",$this->data);
    }

	/*
	|| <<<<<<<<<<<<< SELLER FEATURE PLAN PAYPAL PAYMENT NOTIFY >>>>>>>>>>> ||
	*/
	public function paypal_notify($id) {
        $this->load->model('seller_plan_model', 'seller_plan');
        $temp_store = $this->seller_plan->get_all_details('fc_featured_seller_temp', array('id' => $id));
        if ($temp_store->num_rows > 0) {
            $this->seller_plan->commonDelete('fc_featured_seller_temp', array('id' => $id));
            $userExists = $this->seller_plan->get_all_details(FEATURED_SELLER, array('userId' => $temp_store->row()->userId, 'isExpired' => '0'));
            if ($userExists->num_rows == 0) {
                $dataArr = array('userId' => $temp_store->row()->userId, 'planPrice' => $temp_store->row()->planPrice, 'planPeriod' => $temp_store->row()->planPeriod, 'productCount' => $temp_store->row()->productCount);
                $time = time();
                $datestring = "%Y-%m-%d %h:%i:%s";
                $dataArr['featuredOn'] = mdate($datestring, $time);
                $dataArr['expiresOn'] = date("Y-m-d h:i:s", strtotime($dataArr['featuredOn'] . '+ ' . $temp_store->row()->planPeriod . ' days'));
                $dataArr['createdOn'] = mdate($datestring, $time);
                $this->seller_plan->simple_insert(FEATURED_SELLER, $dataArr);
                $this->seller_plan->commonDelete('fc_featured_seller_temp', array('id' => $id));
				$this->data['errorMessage'] = "Payment Successfull";
            }
        }
		$this->load->view("mobile/featured/success",$this->data);
    }

	/*
	|| <<<<<<<<<<<<< SELLER FEATURE PLAN PAYPAL CREDICT PAY >>>>>>>>>>> ||
	*/
	public function paypalCreditPayment($plan_id= "", $userId = "") {
		if($userId != ""){
			if($plan_id != ""){
				$condition = array('id' => $userId, "status" => "Active");
				$userInfoDetails = $this->mobile_model->get_all_details(USERS,$condition);
				if($userInfoDetails->num_rows() == 1){
					$this->load->model('seller_plan_model', 'seller_plan');
					$this->data['plan'] = $this->seller_plan->get_all_details(SELLER_PLAN, array('id'=>$plan_id));
					$this->data['userDetails'] = $userInfoDetails;
					$this->data['countryList'] = $this->mobile_model->get_all_details(COUNTRY_LIST, array(), array(array('field' => 'name', 'type' => 'asc')));
					$this->load->view('mobile/featured/featuredPayPaypalCredict', $this->data);
				}else{
					$this->data['errorMessage'] = "No Such User Found";
					$this->load->view("mobile/featured/failed",$this->data);
				}
			}else{
				$this->data['errorMessage'] = "PlanId Missing";
				$this->load->view("mobile/featured/failed",$this->data);
			}
		}else{
			$this->data['errorMessage'] = "UserId Missing";
			$this->load->view("mobile/featured/failed",$this->data);
		}
    }


	public function paypal_credit_payment() {

		$UserId = $_POST['user_id'];
		if($UserId !=""){
			$this->load->model('seller_plan_model', 'seller_plan');
	        $listCheck = $this->seller_plan->get_all_details(FEATURED_SELLER, array('userId' => $UserId, 'isExpired' => '0'));
	        if ($listCheck->num_rows == 0) {
	            $plan = $this->seller_plan->get_all_details(SELLER_PLAN, array('id'=>$this->input->post('plan_id')));
	            $registeredSellers = $this->seller_plan->get_all_details(FEATURED_SELLER, array('isExpired' => '0'));
	            if ($registeredSellers->num_rows < $plan->row()->plan_promoted_count) {
	                $time = time();
	                $datestring = "%Y-%m-%d %h:%i:%s";
	                $dataArr = array('userId' => $UserId, 'planPrice' => $plan->row()->plan_price, 'planPeriod' => $plan->row()->plan_period, 'productCount' => $plan->row()->plan_product_count, 'paymentType' => 'Credit Card');
	                $dataArr['createdOn'] = mdate($datestring, $time);
	                $this->seller_plan->simple_insert('fc_featured_seller_temp', $dataArr);
	                $temp_id = $this->db->insert_id();
	                $PaypalDodirect = unserialize($this->data['paypal_credit_card_settings']['settings']);
	                $dodirects = array(
	                    'Sandbox' => $PaypalDodirect['mode'], // Sandbox / testing mode option.
	                    'APIUsername' => $PaypalDodirect['Paypal_API_Username'], // PayPal API username of the API caller
	                    'APIPassword' => $PaypalDodirect['paypal_api_password'], // PayPal API password of the API caller
	                    'APISignature' => $PaypalDodirect['paypal_api_Signature'], // PayPal API signature of the API caller
	                    'APISubject' => '', // PayPal API subject (email address of 3rd party user that has granted API permission for your app)
	                    'APIVersion' => '85.0'  // API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
	                );
	                // Show Errors
	                if ($dodirects['Sandbox']) {
	                     
	                    ini_set('display_errors', '1');
	                }
	                $this->load->library('paypal/Paypal_pro', $dodirects);
	                $DPFields = array(
	                    'paymentaction' => '', // How you want to obtain payment.  Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
	                    'ipaddress' => $this->input->ip_address(), // Required.  IP address of the payer's browser.
	                    'returnfmfdetails' => '1'    // Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
	                );
	                $CCDetails = array(
	                    'creditcardtype' => $this->input->post('cardType'), // Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
	                    'acct' => $this->input->post('cardNumber'), // Required.  Credit card number.  No spaces or punctuation.
	                    'expdate' => $this->input->post('CCExpDay') . $this->input->post('CCExpMnth'), // Required.  Credit card expiration date.  Format is MMYYYY
	                    'cvv2' => $this->input->post('creditCardIdentifier'), // Requirements determined by your PayPal account settings.  Security digits for credit card.
	                    'startdate' => '', // Month and year that Maestro or Solo card was issued.  MMYYYY
	                    'issuenumber' => ''       // Issue number of Maestro or Solo card.  Two numeric digits max.
	                );
	                $PayerInfo = array(
	                    'email' => $this->input->post('email'), // Email address of payer.
	                    'payerid' => '', // Unique PayPal customer ID for payer.
	                    'payerstatus' => '', // Status of payer.  Values are verified or unverified
	                    'business' => ''
	                        // Payer's business name.
	                );
	                $PayerName = array(
	                    'salutation' => 'Mr.', // Payer's salutation.  20 char max.
	                    'firstname' => $this->input->post('full_name'), // Payer's first name.  25 char max.
	                    'middlename' => '', // Payer's middle name.  25 char max.
	                    'lastname' => '', // Payer's last name.  25 char max.
	                    'suffix' => ''        // Payer's suffix.  12 char max.
	                );
	                //'x_amount'				=> ,
	                //			'x_email'				=> $this->input->post('email'),

	                $BillingAddress = array(
	                    'street' => $this->input->post('address'), // Required.  First street address.
	                    'street2' => '', // Second street address.
	                    'city' => $this->input->post('city'), // Required.  Name of City.
	                    'state' => $this->input->post('state'), // Required. Name of State or Province.
	                    'countrycode' => $this->input->post('country'), // Required.  Country code.
	                    'zip' => $this->input->post('postal_code'), // Required.  Postal code of payer.
	                    'phonenum' => $this->input->post('phone_no')       // Phone Number of payer.  20 char max.
	                );
	                $ShippingAddress = array(
	                    'shiptoname' => $this->input->post('full_name'), // Required if shipping is included.  Person's name associated with this address.  32 char max.
	                    'shiptostreet' => $this->input->post('address'), // Required if shipping is included.  First street address.  100 char max.
	                    'shiptostreet2' => '', // Second street address.  100 char max.
	                    'shiptocity' => $this->input->post('city'), // Required if shipping is included.  Name of city.  40 char max.
	                    'shiptostate' => $this->input->post('state'), // Required if shipping is included.  Name of state or province.  40 char max.
	                    'shiptozip' => $this->input->post('postal_code'), // Required if shipping is included.  Postal code of shipping address.  20 char max.
	                    'shiptocountry' => $this->input->post('country'), // Required if shipping is included.  Country code of shipping address.  2 char max.
	                    'shiptophonenum' => $this->input->post('phone_no')  // Phone number for shipping address.  20 char max.
	                );

	                $PaymentDetails = array(
	                    'amt' => $this->input->post('total_price'), // Required.  Total amount of order, including shipping, handling, and tax.
	                    'currencycode' => $this->data['currencyType'], // Required.  Three-letter currency code.  Default is USD.
	                    'itemamt' => '', // Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
	                    'shippingamt' => '', // Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
	                    'insuranceamt' => '', // Total shipping insurance costs for this order.
	                    'shipdiscamt' => '', // Shipping discount for the order, specified as a negative number.
	                    'handlingamt' => '', // Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
	                    'taxamt' => '', // Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax.
	                    'desc' => '', // Description of the order the customer is purchasing.  127 char max.
	                    'custom' => '', // Free-form field for your own use.  256 char max.
	                    'invnum' => '', // Your own invoice or tracking number
	                    'buttonsource' => '', // An ID code for use by 3rd party apps to identify transactions.
	                    'notifyurl' => '', // URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
	                    'recurring' => ''      // Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
	                );
	                // For order items you populate a nested array with multiple $Item arrays.
	                // Normally you'll be looping through cart items to populate the $Item array
	                // Then push it into the $OrderItems array at the end of each loop for an entire
	                // collection of all items in $OrderItems.

	                $OrderItems = array();
	                $Item = array(
	                    'l_name' => '', // Item Name.  127 char max.
	                    'l_desc' => '', // Item description.  127 char max.
	                    'l_amt' => '', // Cost of individual item.
	                    'l_number' => '', // Item Number.  127 char max.
	                    'l_qty' => '', // Item quantity.  Must be any positive integer.
	                    'l_taxamt' => '', // Item's sales tax amount.
	                    'l_ebayitemnumber' => '', // eBay auction number of item.
	                    'l_ebayitemauctiontxnid' => '', // eBay transaction ID of purchased item.
	                    'l_ebayitemorderid' => ''     // eBay order ID for the item.
	                );
	                array_push($OrderItems, $Item);
	                $Secure3D = array(
	                    'authstatus3d' => '',
	                    'mpivendor3ds' => '',
	                    'cavv' => '',
	                    'eci3ds' => '',
	                    'xid' => ''
	                );
	                $PayPalRequestData = array(
	                    'DPFields' => $DPFields,
	                    'CCDetails' => $CCDetails,
	                    'PayerInfo' => $PayerInfo,
	                    'PayerName' => $PayerName,
	                    'BillingAddress' => $BillingAddress,
	                    'ShippingAddress' => $ShippingAddress,
	                    //'ShippingAddress' => array(),
	                    'PaymentDetails' => $PaymentDetails,
	                    'OrderItems' => $OrderItems,
	                    'Secure3D' => $Secure3D
	                );
	                $PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);
	                if (!$this->paypal_pro->APICallSuccessful($PayPalResult['ACK'])) {
	                    $this->seller_plan->commonDelete('fc_featured_seller_temp', array('id' => $temp_id));
	                    $errors = array('Errors' => $PayPalResult['ERRORS']);
	                    //$this->load->view('paypal_error',$errors);
	                    $newerrors = $errors['Errors'][0]['L_LONGMESSAGE'];
	                    // $this->setErrorMessage('danger', $newerrors);
	                    // redirect(base_url() . 'featured');
						$this->data['errorMessage'] = $newerrors;
						$this->load->view("mobile/featured/failed",$this->data);
	                } else {
	                    // Successful call.  Load view or whatever you need to do here.
	                    $temp_store = $this->seller_plan->get_all_details('fc_featured_seller_temp', array('id' => $temp_id));
	                    $this->seller_plan->commonDelete('fc_featured_seller_temp', array('id' => $temp_id));
	                    $time = time();
	                    $datestring = "%Y-%m-%d %h:%i:%s";
	                    $dataArr_1['userId'] = $UserId;
	                    $dataArr_1['featuredOn'] = mdate($datestring, $time);
	                    $dataArr_1['expiresOn'] = date("Y-m-d h:i:s", strtotime($dataArr_1['featuredOn'] . '+ ' . $temp_store->row()->planPeriod . ' days'));
	                    $dataArr_1['planPrice'] = $temp_store->row()->planPrice;
	                    $dataArr_1['planPeriod'] = $temp_store->row()->planPeriod;
	                    $dataArr_1['productCount'] = $temp_store->row()->productCount;
	                    $dataArr_1['paymentType'] = $temp_store->row()->paymentType;
	                    $dataArr_1['createdOn'] = mdate($datestring, $time);
	                    $this->seller_plan->simple_insert(FEATURED_SELLER, $dataArr_1);
						$this->data['errorMessage'] = "Payment Successful";
						$this->load->view("mobile/featured/success",$this->data);
	                }
	            } else {
					$this->data['errorMessage'] = "Maximum Number of featured Sellers reached";
					$this->load->view("mobile/featured/failed",$this->data);
	            }
	        } else {
				$this->data['errorMessage'] = "Already plan subscribed";
				$this->load->view("mobile/featured/failed",$this->data);
	        }
		}else{
			$this->data['errorMessage'] = "UserId Missing";
			$this->load->view("mobile/featured/failed",$this->data);
		}
    }

	public function authorizePayment($plan_id='', $userId="") {
		if($userId != ""){
			if($plan_id != ""){
				$condition = array('id' => $userId, "status" => "Active");
				$userInfoDetails = $this->mobile_model->get_all_details(USERS,$condition);
				if($userInfoDetails->num_rows() == 1){
					$this->data['userDetails'] = $userInfoDetails;
					$this->data['countryList'] = $this->mobile_model->get_all_details(COUNTRY_LIST, array(), array(array('field' => 'name', 'type' => 'asc')));
					$this->load->model('seller_plan_model', 'seller_plan');
					$this->data['plan'] = $this->seller_plan->get_all_details(SELLER_PLAN, array('id'=>$plan_id));
					$this->load->view('mobile/featured/featuredPayAuthorizeCredict', $this->data);
				}else{
					$this->data['errorMessage'] = "No Such User Found";
					$this->load->view("mobile/featured/failed",$this->data);
				}
			}else{
				$this->data['errorMessage'] = "PlanId Missing";
				$this->load->view("mobile/featured/failed",$this->data);
			}
		}else{
			$this->data['errorMessage'] = "UserId Missing";
			$this->load->view("mobile/featured/failed",$this->data);
		}
    }


	public function authorize_payment() {
		$userId = $_POST['user_id'];
		if($userId != ""){
			$this->load->model('seller_plan_model', 'seller_plan');
	        $listCheck = $this->seller_plan->get_all_details(FEATURED_SELLER, array('userId' => $userId, 'isExpired' => '0'));
	        if ($listCheck->num_rows == 0) {
	            $plan = $this->seller_plan->get_all_details(SELLER_PLAN, array('id'=>$this->input->post('plan_id')));
	            $registeredSellers = $this->seller_plan->get_all_details(FEATURED_SELLER, array('isExpired' => '0'));
	            if ($registeredSellers->num_rows < $plan->row()->plan_promoted_count) {
	                $time = time();
	                $datestring = "%Y-%m-%d %h:%i:%s";
	                $dataArr = array('userId' => $userId, 'planPrice' => $plan->row()->plan_price, 'planPeriod' => $plan->row()->plan_period, 'productCount' => $plan->row()->plan_product_count, 'paymentType' => 'authorize');
	                $dataArr['createdOn'] = mdate($datestring, $time);
	                $this->seller_plan->simple_insert('fc_featured_seller_temp', $dataArr);
	                $temp_id = $this->db->insert_id();

	                $Auth_Details = unserialize(API_LOGINID);
	                $Auth_Setting_Details = unserialize($Auth_Details['settings']);

	                 
	                define("AUTHORIZENET_API_LOGIN_ID", $Auth_Setting_Details['Login_ID']);    // Add your API LOGIN ID
	                define("AUTHORIZENET_TRANSACTION_KEY", $Auth_Setting_Details['Transaction_Key']); // Add your API transaction key
	                define("API_MODE", $Auth_Setting_Details['mode']);

	                if (API_MODE == 'sandbox') {
	                    define("AUTHORIZENET_SANDBOX", true); // Set to false to test against production
	                } else {
	                    define("AUTHORIZENET_SANDBOX", false);
	                }
	                define("TEST_REQUEST", "FALSE");
	                require_once './authorize/AuthorizeNet.php';

	                $transaction = new AuthorizeNetAIM;
	                $transaction->setSandbox(AUTHORIZENET_SANDBOX);
	                $transaction->setFields(
	                        array(
	                            'amount' => $this->input->post('total_price'),
	                            'card_num' => $this->input->post('cardNumber'),
	                            'exp_date' => $this->input->post('CCExpDay') . '/' . $this->input->post('CCExpMnth'),
	                            'first_name' => $this->input->post('full_name'),
	                            'last_name' => '',
	                            'address' => $this->input->post('address'),
	                            'city' => $this->input->post('city'),
	                            'state' => $this->input->post('state'),
	                            'country' => $this->input->post('country'),
	                            'phone' => $this->input->post('phone_no'),
	                            'email' => $this->input->post('email'),
	                            'card_code' => $this->input->post('creditCardIdentifier'),
	                        )
	                );
	                $response = $transaction->authorizeAndCapture();
	                if ($response->approved) {
	                    $temp_store = $this->seller_plan->get_all_details('fc_featured_seller_temp', array('id' => $temp_id));
	                    $this->seller_plan->commonDelete('fc_featured_seller_temp', array('id' => $temp_id));
	                    $time = time();
	                    $datestring = "%Y-%m-%d %h:%i:%s";
	                    $dataArr_1['userId'] = $userId;
	                    $dataArr_1['featuredOn'] = mdate($datestring, $time);
	                    $dataArr_1['expiresOn'] = date("Y-m-d h:i:s", strtotime($dataArr_1['featuredOn'] . '+ ' . $temp_store->row()->planPeriod . ' days'));
	                    $dataArr_1['planPrice'] = $temp_store->row()->planPrice;
	                    $dataArr_1['planPeriod'] = $temp_store->row()->planPeriod;
	                    $dataArr_1['productCount'] = $temp_store->row()->productCount;
	                    $dataArr_1['paymentType'] = $temp_store->row()->paymentType;
	                    $dataArr_1['createdOn'] = mdate($datestring, $time);
	                    $this->seller_plan->simple_insert(FEATURED_SELLER, $dataArr_1);
						$this->data['errorMessage'] = "Payment Successful";
						$this->load->view("mobile/featured/success",$this->data);
	                } else {
	                    //redirect('site/shopcart/cancel?failmsg='.$response->response_reason_text);
	                    //redirect('order/failure/'.$response->response_reason_text);
	                    $this->seller_plan->commonDelete('fc_featured_seller_temp', array('id' => $temp_id));
						$this->data['errorMessage'] = $response->response_reason_text;
						$this->load->view("mobile/featured/failed",$this->data);
	                }
	            } else {
					$this->data['errorMessage'] = "Maximum Number of featured Sellers reached";
					$this->load->view("mobile/featured/failed",$this->data);
	            }
	        } else {
				$this->data['errorMessage'] = "Already plan subscribed!!";
				$this->load->view("mobile/featured/failed",$this->data);
	        }
		}else{
			$this->data['errorMessage'] = "UserId Missing";
			$this->load->view("mobile/featured/failed",$this->data);
		}
    }

	public function stripePayment($plan_id='', $userId="") {
		if($userId != ""){
			if($plan_id != ""){
				$condition = array('id' => $userId, "status" => "Active");
				$userInfoDetails = $this->mobile_model->get_all_details(USERS,$condition);
				if($userInfoDetails->num_rows() == 1){
					$this->data['userDetails'] = $userInfoDetails;
					$this->data['countryList'] = $this->mobile_model->get_all_details(COUNTRY_LIST, array(), array(array('field' => 'name', 'type' => 'asc')));
					$this->load->model('seller_plan_model', 'seller_plan');
					$this->data['plan'] = $this->seller_plan->get_all_details(SELLER_PLAN, array('id'=>$plan_id));
					$this->load->view('mobile/featured/featuredStripePayment', $this->data);
				}else{
					$this->data['errorMessage'] = "No Such User Found";
					$this->load->view("mobile/featured/failed",$this->data);
				}
			}else{
				$this->data['errorMessage'] = "PlanId Missing";
				$this->load->view("mobile/featured/failed",$this->data);
			}
		}else{
			$this->data['errorMessage'] = "UserId Missing";
			$this->load->view("mobile/featured/failed",$this->data);
		}
    }

	public function stripe_payment(){
		$userId = $_POST['user_id'];
		if($userId != ""){
			$this->load->model('seller_plan_model', 'seller_plan');
	        $listCheck = $this->seller_plan->get_all_details(FEATURED_SELLER, array('userId' => $userId, 'isExpired' => '0'));
	        if ($listCheck->num_rows == 0) {
	            $plan = $this->seller_plan->get_all_details(SELLER_PLAN, array('id'=>$this->input->post('plan_id')));
	            $registeredSellers = $this->seller_plan->get_all_details(FEATURED_SELLER, array('isExpired' => '0'));
	            if ($registeredSellers->num_rows < $plan->row()->plan_promoted_count) {
	                $time = time();
	                $datestring = "%Y-%m-%d %h:%i:%s";
	                $dataArr = array('userId' => $userId, 'planPrice' => $plan->row()->plan_price, 'planPeriod' => $plan->row()->plan_period, 'productCount' => $plan->row()->plan_product_count, 'paymentType' => 'stripe');
	                $dataArr['createdOn'] = mdate($datestring, $time);
	                $this->seller_plan->simple_insert('fc_featured_seller_temp', $dataArr);
	                $temp_id = $this->db->insert_id();

	                $this->load->library('Stripe');
	                $success = "";
	                $error = "";
	                $totalAmount = $this->input->post('total_price');
	                $crdno = $this->input->post('cardNumber');
	                $crdname = $this->input->post('cardType');
	                $year = $this->input->post('CCExpMnth');
	                $mnth = $this->input->post('CCExpDay');
	                $cvv = $this->input->post('creditCardIdentifier');
	                $crd = array('number' => $crdno, 'exp_month' => $mnth, 'exp_year' => $year, 'cvc' => $cvv, 'name' => $crdname);
	                $item_name = $this->config->item('email_title') . ' Products';
	                if ($_POST) {
	                    //print_r($crd);die;
	                    $success = Stripe::charge_card($totalAmount, $crd);
	                    //echo "<pre>";print_r($result);die;
	                    if (isset($success)) {
	                        $result_success = (array) json_decode($success);
	                        //print_r($result_success);die;

	                        if ($result_success['status'] == "succeeded") {
	                            $temp_store = $this->seller_plan->get_all_details('fc_featured_seller_temp', array('id' => $temp_id));
	                            $this->seller_plan->commonDelete('fc_featured_seller_temp', array('id' => $temp_id));
	                            $time = time();
	                            $datestring = "%Y-%m-%d %h:%i:%s";
	                            $dataArr_1['userId'] = $userId;
	                            $dataArr_1['featuredOn'] = mdate($datestring, $time);
	                            $dataArr_1['expiresOn'] = date("Y-m-d h:i:s", strtotime($dataArr_1['featuredOn'] . '+ ' . $temp_store->row()->planPeriod . ' days'));
	                            $dataArr_1['planPrice'] = $temp_store->row()->planPrice;
	                            $dataArr_1['planPeriod'] = $temp_store->row()->planPeriod;
	                            $dataArr_1['productCount'] = $temp_store->row()->productCount;
	                            $dataArr_1['paymentType'] = $temp_store->row()->paymentType;
	                            $dataArr_1['createdOn'] = mdate($datestring, $time);
	                            $this->seller_plan->simple_insert(FEATURED_SELLER, $dataArr_1);
								$this->data['errorMessage'] = "Payment Successful";
								$this->load->view("mobile/featured/success",$this->data);
	                        } else {
	                            $err = (array) $result_success['error'];
	                            $this->seller_plan->commonDelete('fc_featured_seller_temp', array('id' => $temp_id));
	                            $statustrans['msg'] = $err['message'];
								$this->data['errorMessage'] = $err['message'];
								$this->load->view("mobile/featured/failed",$this->data);
	                        }
	                    }
	                }
	            } else {
					$this->data['errorMessage'] = "Maximum Number of featured Sellers reached";
					$this->load->view("mobile/featured/failed",$this->data);
	            }
	        } else {
				$this->data['errorMessage'] = "Already plan subscribed!!";
				$this->load->view("mobile/featured/failed",$this->data);
	        }
		}else{
			$this->data['errorMessage'] = "UserId Missing";
			$this->load->view("mobile/featured/failed",$this->data);
		}
    }

	public function featureTwoCheckout($plan_id='', $userId="") {
		if($userId != ""){
			if($plan_id != ""){
				$condition = array('id' => $userId, "status" => "Active");
				$userInfoDetails = $this->mobile_model->get_all_details(USERS,$condition);
				if($userInfoDetails->num_rows() == 1){
					$this->data['userDetails'] = $userInfoDetails;
					$this->data['countryList'] = $this->mobile_model->get_all_details(COUNTRY_LIST, array(), array(array('field' => 'name', 'type' => 'asc')));
					$this->load->model('seller_plan_model', 'seller_plan');
					$this->data['plan'] = $this->seller_plan->get_all_details(SELLER_PLAN, array('id'=>$plan_id));
					$this->load->view('mobile/featured/featuredTwoCheckout', $this->data);
				}else{
					$this->data['errorMessage'] = "No Such User Found";
					$this->load->view("mobile/featured/failed",$this->data);
				}
			}else{
				$this->data['errorMessage'] = "PlanId Missing";
				$this->load->view("mobile/featured/failed",$this->data);
			}
		}else{
			$this->data['errorMessage'] = "UserId Missing";
			$this->load->view("mobile/featured/failed",$this->data);
		}
    }


	public function twocheckout_payment() {
		$userId = $_POST['user_id'];
		if($userId != ""){
			$this->load->model('seller_plan_model', 'seller_plan');
	        $listCheck = $this->seller_plan->get_all_details(FEATURED_SELLER, array('userId' => $userId, 'isExpired' => '0'));
	        if ($listCheck->num_rows == 0) {
	            $plan = $this->seller_plan->get_all_details(SELLER_PLAN, array('id'=>$this->input->post('plan_id')));
	            $registeredSellers = $this->seller_plan->get_all_details(FEATURED_SELLER, array('isExpired' => '0'));
	            if ($registeredSellers->num_rows < $plan->row()->plan_promoted_count) {
	                $time = time();
	                $datestring = "%Y-%m-%d %h:%i:%s";
	                $dataArr = array('userId' => $userId, 'planPrice' => $plan->row()->plan_price, 'planPeriod' => $plan->row()->plan_period, 'productCount' => $plan->row()->plan_product_count, 'paymentType' => '2CO');
	                $dataArr['createdOn'] = mdate($datestring, $time);
					//echo "<pre>";print_r($dataArr);die;
	                $this->seller_plan->simple_insert('fc_featured_seller_temp', $dataArr);
	                $temp_id = $this->db->insert_id();
	                $this->load->library('Twocheckout');
	                $paypal_settings = unserialize($this->config->item('payment_5'));
	                $settings = unserialize($paypal_settings['settings']);
	                Twocheckout::privateKey($settings['privateKey']);
	                Twocheckout::sellerId($settings['sellerId']);
	                Twocheckout::sandbox(true);  #Uncomment to use Sandbox
	                $tokenid = $this->input->post('token');
	                //echo $tokenid;die;
	                $loginUserId = $userId;
	                $totalAmount = $this->input->post('total_price');
	                $quantity = 1;
	                try {
	                    $charge = Twocheckout_Charge::auth(array(
	                                "merchantOrderId" => "123",
	                                "token" => $tokenid,
	                                "currency" => 'USD',
	                                "total" => $totalAmount,
	                                "billingAddr" => array(
	                                    "name" => $this->input->post('full_name'),
	                                    "addrLine1" => $this->input->post('address'),
	                                    "city" => $this->input->post('city'),
	                                    "state" => $this->input->post('state'),
	                                    "zipCode" => $this->input->post('postal_code'),
	                                    "country" => $this->input->post('country'),
	                                    "email" => $this->input->post('email'),
	                                    "phoneNumber" => $this->input->post('phone_no')
	                                ),
	                                "shippingAddr" => array(
	                                    "name" => $this->input->post('full_name'),
	                                    "addrLine1" => $this->input->post('address'),
	                                    "city" => $this->input->post('city'),
	                                    "state" => $this->input->post('state'),
	                                    "zipCode" => $this->input->post('postal_code'),
	                                    "country" => $this->input->post('country'),
	                                    "email" => $this->input->post('email'),
	                                    "phoneNumber" => $this->input->post('phone_no')
	                                )
	                                    ), 'array');

	                    $result = (array) json_decode($charge);
	                    $rescod = $result['response'];
	                    if ($rescod->responseCode == 'APPROVED') {
	                        $temp_store = $this->seller_plan->get_all_details('fc_featured_seller_temp', array('id' => $temp_id));
	                        $this->seller_plan->commonDelete('fc_featured_seller_temp', array('id' => $temp_id));
	                        $time = time();
	                        $datestring = "%Y-%m-%d %h:%i:%s";
	                        $dataArr_1['userId'] = $userId;
	                        $dataArr_1['featuredOn'] = mdate($datestring, $time);
	                        $dataArr_1['expiresOn'] = date("Y-m-d h:i:s", strtotime($dataArr_1['featuredOn'] . '+ ' . $temp_store->row()->planPeriod . ' days'));
	                        $dataArr_1['planPrice'] = $temp_store->row()->planPrice;
	                        $dataArr_1['planPeriod'] = $temp_store->row()->planPeriod;
	                        $dataArr_1['productCount'] = $temp_store->row()->productCount;
	                        $dataArr_1['paymentType'] = $temp_store->row()->paymentType;
	                        $dataArr_1['createdOn'] = mdate($datestring, $time);
	                        $this->seller_plan->simple_insert(FEATURED_SELLER, $dataArr_1);
							$this->data['errorMessage'] = "Payment Successful";
							$this->load->view("mobile/featured/success",$this->data);
	                    } else {
	                        $this->seller_plan->commonDelete('fc_featured_seller_temp', array('id' => $temp_id));
							$this->data['errorMessage'] = $result['exception']->errorMsg;
							$this->load->view("mobile/featured/failed",$this->data);
	                    }
	                } catch (Twocheckout_Error $e) {
	                    $e->getMessage();
	                }
	            } else {
					$this->data['errorMessage'] = "Maximum Number of featured Sellers reached";
					$this->load->view("mobile/featured/failed",$this->data);
	            }
	        } else {
				$this->data['errorMessage'] = "Already plan subscribed!!";
				$this->load->view("mobile/featured/failed",$this->data);
	        }
		}else{
			$this->data['errorMessage'] = "UserId Missing";
			$this->load->view("mobile/featured/failed",$this->data);
		}
    }

	public function userShippingDetails(){
		$returnArr['status'] = '0';
		$returnArr['response'] = '';
		$UserId = $_POST['UserId'];
		if($UserId != ""){
			$shipVal = $this->mobile_model->get_all_details(SHIPPING_ADDRESS, array('user_id' => $UserId));
			$shippingAddress  =  array();
			$PrimaryAddress  =  array();
			if($shipVal->num_rows() > 0){
				foreach ($shipVal->result() as $Shiprow) {
					if ($Shiprow->primary == 'Yes') {
						$PrimaryAddress = array(
															'id'  =>  $Shiprow->id,
															'FullName'  =>  $Shiprow->full_name,
															'NickName'  =>  $Shiprow->nick_name,
															'Address1'  =>  $Shiprow->address1,
															'Address2'  =>  $Shiprow->address2,
															'city'  =>  $Shiprow->city,
															'state'  =>  $Shiprow->state,
															'postalCode'  =>  $Shiprow->postal_code,
															'country'  =>  $Shiprow->country,
															'phone'  =>  $Shiprow->phone
														);
						if($ShipId != ""){
							if($ShipId == $Shiprow->id){
								$optionsValues = 1;
							}else{
								$optionsValues = 0;
							}
						}else{
							$optionsValues = 1;
						}
					}else {
						if($ShipId != ""){
							if($ShipId == $Shiprow->id){
								$optionsValues = 1;
							}else{
								$optionsValues = 0;
							}
						}else{
							$optionsValues = 0;
						}
					}
					$shippingAddress[]  =  array(
														'selectedAddress' =>  $optionsValues,
														'id'  =>  $Shiprow->id,
														'FullName'  =>  $Shiprow->full_name,
														'NickName'  =>  $Shiprow->nick_name,
														'Address1'  =>  $Shiprow->address1,
														'Address2'  =>  $Shiprow->address2,
														'city'  =>  $Shiprow->city,
														'state'  =>  $Shiprow->state,
														'postalCode'  =>  $Shiprow->postal_code,
														'country'  =>  $Shiprow->country,
														'phone'  =>  $Shiprow->phone
													);
					}
			}
			$returnArr['status'] = 1;
			$returnArr['response'] = 'Success';
			$returnArr['PrimaryAddress'] = $PrimaryAddress;
			$returnArr['shippingAddress'] = $shippingAddress;
		}else{
			$returnArr['response'] = "UserId Null !!";
		}
		echo json_encode($returnArr);
	}


	/*
	|| >>>>>>>>>>>>>>>>> Web View Invoice - Purchase <<<<<<<<<<<<<<<<  ||
	*/
	public function viewPurchaseInvoice() {
		$returnArr['status'] = (string)0;
		$returnArr['response'] = '';
		$UserId = intval($_GET['UserId']);
		$mode = $_GET['mode'];
        if($UserId == ''){
            $returnArr['response'] = ' UserId Null !!';
			echo json_encode($returnArr);
        }else{
			$InvoiceNumber = $_GET['InvoiceNumber'];
		    if($InvoiceNumber != ''){
				if($mode == ""){
					$returnArr['response'] = ' Mode Null !!';
					echo json_encode($returnArr);
				}else{
					if($mode == "purchase"){
						$purchaseList = $this->user_model->get_purchase_list($UserId, $InvoiceNumber);
					}else{
						$purchaseList = $this->user_model->get_order_list($UserId, $InvoiceNumber);
					}
					$shipAddRess = $this->user_model->get_all_details(SHIPPING_ADDRESS, array('id' => $purchaseList->row()->shippingid));
					$newsid = '19';
					$template_values = $this->product_model->get_newsletter_template_details($newsid);
					$this->data['title'] = $template_values['news_subject'];
		            $this->data['logo'] = $this->data['logo'];
		            $this->data['meta_title'] = $this->config->item('meta_title');
		            $this->data['ship_fullname'] = stripslashes($shipAddRess->row()->full_name);
		            $this->data['ship_address1'] = stripslashes($shipAddRess->row()->address1);
		            $this->data['ship_address2'] = stripslashes($shipAddRess->row()->address2);
		            $this->data['ship_city'] = stripslashes($shipAddRess->row()->city);
		            $this->data['ship_country'] = stripslashes($shipAddRess->row()->country);
		            $this->data['ship_state'] = stripslashes($shipAddRess->row()->state);
		            $this->data['ship_postalcode'] = stripslashes($shipAddRess->row()->postal_code);
		            $this->data['ship_phone'] = stripslashes($shipAddRess->row()->phone);
		            $this->data['bill_fullname'] = stripslashes($purchaseList->row()->full_name);
		            $this->data['bill_address1'] = stripslashes($purchaseList->row()->address);
		            $this->data['bill_address2'] = stripslashes($purchaseList->row()->address2);
		            $this->data['bill_city'] = stripslashes($purchaseList->row()->city);
		            $this->data['bill_country'] = stripslashes($purchaseList->row()->country);
		            $this->data['bill_state'] = stripslashes($purchaseList->row()->state);
		            $this->data['bill_postalcode'] = stripslashes($purchaseList->row()->postal_code);
		            $this->data['bill_phone'] = stripslashes($purchaseList->row()->phone_no);
		            $this->data['invoice_number'] = $purchaseList->row()->dealCodeNumber;
		            $this->data['payment_type'] = $purchaseList->row()->payment_type;
		            $this->data['invoice_date'] = date("F j, Y g:i a", strtotime($purchaseList->row()->created));
					$items = "";
					foreach ($purchaseList->result() as $cartRow) {
			            if ($cartRow->image != '') {
			                $InvImg = @explode(',', $cartRow->image);
			            } else {
			                $InvImg = @explode(',', $cartRow->old_image);
			            }
			            $unitPrice = ($cartRow->price * (0.01 * $cartRow->product_tax_cost)) + $cartRow->product_shipping_cost + $cartRow->price;
			            $uTot = $unitPrice * $cartRow->quantity;
			            if ($cartRow->attr_name != '' || $cartRow->attr_type) {
			                $atr = '<br>' . $cartRow->attr_type . ' / ' . $cartRow->attr_name;
			            } else if ($cartRow->old_attr_name != '') {
			                $atr = '<br>' . $cartRow->old_attr_name;
			            } else {
			                $atr = '';
			            }
			            $product_name = $cartRow->product_name;
			            if ($product_name == '') {
			                $product_name = $cartRow->old_product_name;
			            }
			            if ($cartRow->product_type == 'physical') {
			                $dwnldcode = 'Not applicable';
			            } else {
			                $dwnldcode = 'Copy & paste this code to download product: ' . $cartRow->product_download_code;
			            }
					$items .= '<div class="product-list"><div class="container"><div class="col-md-2 no-padding"><p>Bag Items</p><p class="clearfix"><img src="'. base_url() . PRODUCTPATH . $InvImg[0] .'" title="" alt="' . stripslashes($cartRow->product_name) . '" /></p></div><div class="col-md-2 no-padding"><p>Product Name</p> <p>' . $product_name . $atr . '</p></div><div class="col-md-2 no-padding"><p>Code</p><p>' . $dwnldcode . '</p></div><div class="col-md-2 no-padding"><p>Qty</p><p>' . strtoupper($cartRow->quantity) . '</p></div><div class="col-md-2 no-padding"><p>Unit Price</p>
					<p>' . $this->data['currencySymbol'] . number_format($unitPrice, 2, '.', '') . '</p></div><div class="col-md-2 no-padding"><p>Sub Total</p><p>' . $this->data['currencySymbol'] . number_format($uTot, 2, '.', '') . '</p></div></div></div>';
			            $grantTotal = $grantTotal + $uTot;
			        }
					$private_total = $grantTotal - $purchaseList->row()->discountAmount;
			        $private_total = $private_total + $purchaseList->row()->tax + $purchaseList->row()->shippingcost;
					$items .= '<div class="product-price">
						<div class="container">
							<div class="pull-right">
								<div class="total-row clearfix ">
									<p>Sub Total</p>
									<p>' . $this->data['currencySymbol'] . number_format($grantTotal, '2', '.', '') . '</p>
								</div>
								<div class="total-row clearfix">
									<p>Discount Amount</p>
									<p>' . $this->data['currencySymbol'] . number_format($purchaseList->row()->discountAmount, '2', '.', '') . '</p>
								</div>
								<div class="total-row clearfix">
									<p>Shipping Cost</p>
									<p>' . $this->data['currencySymbol'] . number_format($purchaseList->row()->shippingcost, 2, '.', '') . '</p>
								</div>
								<div class="total-row clearfix">
									<p>Shipping Tax</p>
									<p>' . $this->data['currencySymbol'] . number_format($purchaseList->row()->tax, 2, '.', '') . '</p>
								</div>
								<div class="total-row clearfix">
									<p>Grand Total</p>
									<p>' . $this->data['currencySymbol'] . number_format($private_total, '2', '.', '') . '</p>
								</div>
							</div>
						</div>
					</div>';
					$this->data['items'] = $items;
					$this->data['appWeb'] = "1";
					$this->load->view('mobile/userSettings/invoice', $this->data);
				}
			}else{
				$returnArr['response'] = ' Invoice Number Null !!';
				echo json_encode($returnArr);
			}
        }
    }

	public function addSellerFeaturedProducts() {
		$returnArr['status'] = (string)0;
		$returnArr['response'] = '';
		$UserId = $_POST['UserId'];
		if($UserId != ""){
			$this->load->model('seller_plan_model', 'seller_plan');
			$return['message'] = '0';
			$listCheck = $this->seller_plan->get_all_details(FEATURED_SELLER, array('userId' => $UserId, 'isExpired' => '0'));
			if ($listCheck->num_rows > 0) {
				$sellerProductID = $this->input->post('sellerProductID');
				if($sellerProductID != ""){
					$sellerProductIDArr = explode(',',$sellerProductID);
					$sellerProductIDArrFinal = array_filter($sellerProductIDArr);
					$featuredProducts = serialize($sellerProductIDArrFinal);
					$dataArr['featuredProducts'] = $featuredProducts;
					$productExists = $this->seller_plan->get_all_details(FEATURED_PRODUCTS, array('userId' => $UserId, 'sellerPlan_id' => $listCheck->row()->id));
					if ($productExists->num_rows > 0) {
						$this->seller_plan->update_details(FEATURED_PRODUCTS, $dataArr, array('userId' => $UserId, 'sellerPlan_id' => $listCheck->row()->id));
					} else {
						$dataArr['userId'] = $UserId;
						$dataArr['sellerPlan_id'] = $listCheck->row()->id;
						$this->seller_plan->simple_insert(FEATURED_PRODUCTS, $dataArr);
					}
					$returnArr['status'] = 1;
					$returnArr['response'] = 'Products Successfully Saved as Featured Products';
				}else{
					$returnArr['response'] = 'SellerProductId Null !!';
				}
			}else{
				$returnArr['response'] = 'You are not a featured seller !!';
			}
		}else{
			$returnArr['response'] = 'UserId Null !!';
		}
		echo json_encode($returnArr);
    }

	/*
	* !! >>>>>>>>>>>>> Change Shipping Status <<<<<<<<<<<< !!
	*/
	public function changeShippingStatus() {
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = $_POST['UserId'];
		if ($UserId== '') {
            $returnArr['response'] = 'UserId Null !!';
        } else {
            $sellId = $_POST['sellerId'];
			if($sellId == ""){
				$returnArr['response'] = 'SellId Null !!';
			}else{
				if ($sellId != $UserId) {
	                $returnArr['response'] = 'UserId & SellId Not Same !!';
	            } else {
	                $dealCode =$_POST['invoiceNumber'];
	                $status = $_POST['shippingStatus'];
					if($dealCode != "" && $status != ""){
						$dataArr = array('shipping_status' => $status);
		                $conditionArr = array('dealCodeNumber' => $dealCode, 'sell_id' => $sellId);
		                $this->user_model->update_details(PAYMENT, $dataArr, $conditionArr);
		                $returnArr['status'] = 1;
						$returnArr['response'] = 'Successfully Updated ';
					}else{
						$returnArr['response'] = 'Some Parameters are Missing !!';
					}
	            }
			}
        }
		echo json_encode($returnArr);
	}

	/*
	* !! >>>>>>>>>>>>> Users Orders Lists <<<<<<<<<<<< !!
	*/
	public function userOrders() {
		$responseArr['status'] = 0;
		$responseArr['response'] = '';
		$UserId = $_POST['UserId'];
		if($UserId != ""){
			$userDetails = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
			if($userDetails->row()->group != "User"){
				$usersOrderList = $this->user_model->get_user_orders_list($UserId);
				if($usersOrderList->num_rows() > 0){
					$usersOrders = array();
					$shippingStatusList= array();
					if ($this->lang->line('order_pending') != '') {
						$status1 =  stripslashes($this->lang->line('order_pending'));
					} else{
						$status1 =  "Pending";
					}
					if ($this->lang->line('order_processed') != '') {
						$status2 =  stripslashes($this->lang->line('order_processed'));
					} else{
						$status2 =  "Processed";
					}
					if ($this->lang->line('order_delivered') != '') {
						$status3 =  stripslashes($this->lang->line('order_delivered'));
					} else{
						$status3 =  "Delivered";
					}
					if ($this->lang->line('order_returnred') != '') {
						$status4 =  stripslashes($this->lang->line('order_returnred'));
					} else{
						$status4 =  "Returned";
					}
					if ($this->lang->line('order_reshipp') != '') {
						$status5 =  stripslashes($this->lang->line('order_reshipp'));
					} else{
						$status5 =  "Re-Shipped";
					}
					if ($this->lang->line('order_cancelled') != '') {
						$status6 =  stripslashes($this->lang->line('order_cancelled'));
					} else{
						$status6 =  "Cancelled";
					}
					$statusarr1['status'] =	$status1;
					$statusarr2['status'] =	$status2;
					$statusarr3['status'] =	$status3;
					$statusarr4['status'] =	$status4;
					$statusarr5['status'] =	$status5;
					$statusarr6['status'] =	$status6;
					$shippingStatusList[]= $statusarr1;
					$shippingStatusList[]= $statusarr2;
					$shippingStatusList[]= $statusarr3;
					$shippingStatusList[]= $statusarr4;
					$shippingStatusList[]= $statusarr5;
					$shippingStatusList[]= $statusarr6;
					foreach($usersOrderList->result() as $ordersList){
						if ($this->lang->line('view_invoice') != '') {
							$Option1 = stripslashes($this->lang->line('view_invoice'));
						} else{
							$Option1 =  "View Invoice";
						}
						 if ($ordersList->status == 'Paid') {
								if ($this->lang->line('buyer_discuss') != '') {
									$Option2 = stripslashes($this->lang->line('buyer_discuss'));
								} else
									$Option2 = "Buyer Discussion";
						}else if($ordersList->status == 'Pending' && $ordersList->payment_type == 'Cash on Delivery' && $ordersList->sell_id == $UserId){
							if ($this->lang->line('buyer_pending') != '') {
								$Option2 = stripslashes($this->lang->line('buyer_pending'));
							} else{
								$Option2 = "Confirm payment";
							}
						}
					$product_Img =  "";
					if ($ordersList->image != ''){
						$arrimage = array_filter(explode(",", $ordersList->image));
						$arrimagestr = implode('', $arrimage);
						$product_Img = base_url().'images/product/'.$arrimagestr;					}
						$usersOrders[] = array(
															'SellId' => $ordersList->sell_id,
															'Invoice' => $ordersList->dealCodeNumber,
															'PaymentStatus' => $ordersList->status,
															'ShippingStatus' => $ordersList->shipping_status,
															'TotalPrice' => $ordersList->TotalPrice,
															'Created' => $ordersList->created,
															'Option1' => $Option1,
															'Option2' => $Option2,
															'product_name' =>$ordersList->product_name,
															'product_image'=>$product_Img
														);
					}
					if ($this->lang->line('purchases__invoice') != '') {
						$value1 = stripslashes($this->lang->line('purchases__invoice'));
					} else{
						$value1 = "Invoice";
					}
					if ($this->lang->line('purchases__paystatus') != '') {
						$value2 = stripslashes($this->lang->line('purchases__paystatus'));
					} else{
						$value2 = "Payment Status";
					}
					if ($this->lang->line('purchases_shipstatus') != '') {
						$value3 = stripslashes($this->lang->line('purchases_shipstatus'));
					} else{
						$value3 = "Shipping Status";
					}
					if ($this->lang->line('purchases_orddate') != '') {
						$value4 = stripslashes($this->lang->line('order_date'));
					} else{
						$value4 = "Date";
					}
					if ($this->lang->line('purchases_option') != '') {
						$value5 = stripslashes($this->lang->line('purchases_option'));
					} else{
						$value5 = "Option";
					}
					$usersOrdersTableHead[] = array(
																		'value1' => $value1,
																		'value2' => $value2,
																		'value3' => $value3,
																		'value4' => $value4,
																		'value5' => $value5
																);
					$responseArr['status'] = 1;
					$responseArr['response'] = 'Success !!';
					$responseArr['shippingStatusList'] = $shippingStatusList;
					$responseArr['usersOrdersTableHead'] =$usersOrdersTableHead;
					$responseArr['usersOrders'] =$usersOrders;
				}else{
					$responseArr['response'] = 'No Order Details Avaliable !!';
				}
			}else{
				$responseArr['response'] = 'This User is not a Seller';
			}
		}else{
			$responseArr['response'] = 'UserId Null !!';
		}
		echo json_encode($responseArr);
    }

	/*
	Contact Seller >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	*/
	public function contactSeller() {
		$responseArr['status'] = 0;
		$responseArr['response'] = '';
		$UserId = $_POST['UserId'];
		if($UserId != ""){
			$question = $_POST['question'];
			$phone = $_POST['phone'];
			$sellerid = $_POST['sellerid'];
			$product_id = $_POST['product_id'];
			if($question != "" && $phone != "" && $sellerid != "" && $product_id != ""){
				$userDetails = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
				if($userDetails->num_rows() == 1){
					$name = $userDetails->row()->full_name;
					$email = $userDetails->row()->email;
					$sellerDetails = $this->mobile_model->get_all_details(USERS, array('id' => $sellerid));
					if($sellerDetails->num_rows() == 1){
						$selleremail = $sellerDetails->row()->email;
						$dataArrVal = array(
													"question" => $question,
													"name" => $name,
													"email" => $email,
													"phone" => $phone,
													"selleremail" => $selleremail,
													"sellerid" => $sellerid,
													"product_id" => $product_id,
												);
				        $this->product_model->simple_insert(CONTACTSELLER, $dataArrVal);
				        $this->data['productVal'] = $this->product_model->get_all_details(PRODUCT, array('id' => $this->input->post('product_id')));
				        $newimages = array_filter(@explode(',', $this->data['productVal']->row()->image));
				        $newsid = '20';
				        $template_values = $this->product_model->get_newsletter_template_details($newsid);
				        $subject = 'From: ' . $this->config->item('email_title') . ' - ' . $template_values['news_subject'];
				        $adminnewstemplateArr = array('email_title' => $this->config->item('email_title'), 'logo' => $this->data['logo'],
				            'name' => $this->input->post('name'),
				            'question' => $this->input->post('question'),
				            'email' => $this->input->post('email'),
				            'phone' => $this->input->post('phone'),
				            'productId' => $this->data['productVal']->row()->id,
				            'productName' => $this->data['productVal']->row()->product_name,
				            'productSeourl' => $this->data['productVal']->row()->seourl,
				            'productImage' => $newimages[0],
				        );
				        extract($adminnewstemplateArr);
				        //$ddd =htmlentities($template_values['news_descrip'],null,'UTF-8');
				        $header .="Content-Type: text/plain; charset=ISO-8859-1\r\n";
				        $message .= '<!DOCTYPE HTML>
							<html>
							<head>
							<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
							<meta name="viewport" content="width=device-width"/><body>';
				        include('./newsletter/registeration' . $newsid . '.php');
				        $message .= '</body>
							</html>';
				        if ($template_values['sender_name'] == '' && $template_values['sender_email'] == '') {
				            $sender_email = $this->data['siteContactMail'];
				            $sender_name = $this->data['siteTitle'];
				        } else {
				            $sender_name = $template_values['sender_name'];
				            $sender_email = $template_values['sender_email'];
				        }
				        $email_values = array('mail_type' => 'html',
				            'from_mail_id' => $sender_email,
				            'mail_name' => $sender_name,
				            'to_mail_id' => $this->input->post('selleremail'),
				            'cc_mail_id' => $this->config->item('site_contact_mail'),
				            'subject_message' => $template_values['news_subject'],
				            'body_messages' => $message
				        );
				        $email_send_to_common = $this->product_model->common_email_send($email_values);
						$responseArr['status'] = 1;
				        $responseArr['response'] = 'Message Sent Successfully. Seller Will Contact You Shortly.';
					}else{
						$responseArr['response'] = 'No such seller data found !!';
					}
				}else{
					$responseArr['response'] = 'No such logged in user data found !!';
				}
			}else{
				$responseArr['response'] = 'Someparameters are Missing !!';
			}
		}else{
			$responseArr['response'] = 'UserId Null !!';
		}
		echo json_encode($responseArr);
    }

	/*
	|| >>>>>>>>>> Store Settings <<<<<<<<<<< !!
	*/
	public function storeDetails() {
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = $_POST['UserId'];
        if ($UserId == '') {
            $returnArr['response'] = 'UserId Null !!';
        }else {
            $store_Details = $this->store_model->get_all_details(STORE_FRONT, array('user_id' => $UserId));
			$storeDetails = array();
			if($store_Details->num_rows() >0){
				$storeLogo = base_url().'images/store/dummy-logo.png';
				if($store_Details->row()->logo_image != ""){
					$storeLogo = base_url().'images/store/'.$store_Details->row()->logo_image;
				}
				$storeCover = base_url().'images/store/banner-dummy.jpg';
				if($store_Details->row()->cover_image != ""){
					$storeCover = base_url().'images/store/'.$store_Details->row()->cover_image;
				}
				$storeDetails = array(
														'storeId' => $store_Details->row()->id,
														'UserId' => $store_Details->row()->user_id,
														'store_name' => $store_Details->row()->store_name,
														'storeLogo' => $storeLogo,
														'storeCover' => $storeCover,
														'tagline' => $store_Details->row()->tagline
													);
				$returnArr['status'] = 1;
				$message = "Success";
				if($this->lang->line('json_success') != ""){
					$message = stripslashes($this->lang->line('json_success'));
				}
				$returnArr['response'] = $message;
				$returnArr['storeDetails'] = $storeDetails;
			}else{
				$returnArr['response'] = "No such Store Found. Please subscribe to open a store";
			}
        }
		echo json_encode($returnArr);
    }

	/*
	|| >>>>>>>>>> Upadte Store Settings <<<<<<<<<<< !!
	*/

	public function updateStore() {
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = intval($_POST['UserId']);
        if($UserId == ''){
            $returnArr['response'] = 'UserId Null !!';
        }else{
			if ($this->input->post('storeId') != '') {
				if (!empty($_POST['logoImage'])) {
					$imgPath ='images/store/temp/';
					$img = $_POST['logoImage'];
					$imgName = time()."l.jpg";
					$imageFormat = array('data:image/jpeg;base64', 'data:image/png;base64', 'data:image/jpg;base64', 'data:image/gif;base64');
					$img = str_replace($imageFormat, '', $img);
					$data = base64_decode($img);
					$image = @imagecreatefromstring($data);
					if ($image !== false) {
						$uploadPath = $imgPath . $imgName;
						imagejpeg($image, $uploadPath, 100);
						imagedestroy($image);
						if (isset($uploadPath)) {
							/* Creating Thumbnail image with the size of 100 X 100 */
							$filename = base_url($uploadPath);
							list($width, $height) = getimagesize($filename);
							$newwidth = 100;
							$newheight = 100;
							//$thumbImage = @imagecreatetruecolor($newwidth, $newheight);
							$sourceImage = @imagecreatefromjpeg($filename);
							imagecopyresized($thumbImage, $sourceImage, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
							// if ($thumbImage !== false) {
							// 	/* Not uploading to thumb folder */
							// 	$thumbImgPath = 'images/users/thumb/';
							// 	$thumpUploadPath = $thumbImgPath . $imgName;
							// 	imagejpeg($thumbImage, $thumpUploadPath, 100);
							// }
							$new_name = $imgName;
							@copy("./images/store/temp/".$imgName, './images/store/'.$new_name);
							$dir = getcwd().DIRECTORY_SEPARATOR ."images".DIRECTORY_SEPARATOR ."store".DIRECTORY_SEPARATOR."temp".DIRECTORY_SEPARATOR ;
							$interval = strtotime('-24 hours');
							foreach (glob($dir."*.*") as $file){
								if (filemtime($file) <= $interval ){
									if(!is_dir($file) ){
										unlink($file);
									}
								}
							}
							$logo_image = $imgName;
						}else{
							$returnArr['response'] = 'There is something wrong in "Uploading Path" !!';
						}
					}else{
						$returnArr['response'] = 'There is something wrong with "Logo Image" !!';
					}
				}

				if (!empty($_POST['bannerImage'])) {
					$imgPath ='images/store/temp/';
					$img = $_POST['bannerImage'];
					$imgName = time()."b.jpg";
					$imageFormat = array('data:image/jpeg;base64', 'data:image/png;base64', 'data:image/jpg;base64', 'data:image/gif;base64');
					$img = str_replace($imageFormat, '', $img);
					$data = base64_decode($img);
					$image = @imagecreatefromstring($data);
					if ($image !== false) {
						$uploadPath = $imgPath . $imgName;
						imagejpeg($image, $uploadPath, 100);
						imagedestroy($image);
						if (isset($uploadPath)) {
							/* Creating Thumbnail image with the size of 100 X 100 */
							$filename = base_url($uploadPath);
							list($width, $height) = getimagesize($filename);
							$newwidth = 100;
							$newheight = 100;
							//$thumbImage = @imagecreatetruecolor($newwidth, $newheight);
							$sourceImage = @imagecreatefromjpeg($filename);
							imagecopyresized($thumbImage, $sourceImage, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
							// if ($thumbImage !== false) {
							// 	/* Not uploading to thumb folder */
							// 	$thumbImgPath = 'images/users/thumb/';
							// 	$thumpUploadPath = $thumbImgPath . $imgName;
							// 	imagejpeg($thumbImage, $thumpUploadPath, 100);
							// }
							$new_name = $imgName;
							@copy("./images/store/temp/".$imgName, './images/store/'.$new_name);
							$dir = getcwd().DIRECTORY_SEPARATOR ."images".DIRECTORY_SEPARATOR ."store".DIRECTORY_SEPARATOR."temp".DIRECTORY_SEPARATOR ;
							$interval = strtotime('-24 hours');
							foreach (glob($dir."*.*") as $file){
								if (filemtime($file) <= $interval ){
									if(!is_dir($file) ){
										unlink($file);
									}
								}
							}
							$banner_image = $imgName;
						}else{
							$returnArr['response'] = 'There is something wrong in "Uploading Path" !!';
						}
					}else{
						$returnArr['response'] = 'There is something wrong with "Banner Image" !!';
					}
				}

	            $excludeArr = array('logoImage', 'bannerImage', 'storeId','UserId');
	            if ($logo_image == '' && $banner_image == '') {
	                $dataArr = array();
	            } elseif ($logo_image == '') {
	                $dataArr = array('cover_image' => $banner_image);
	            } elseif ($banner_image == '') {
	                $dataArr = array('logo_image' => $logo_image);
	            } else {
	                $dataArr = array('logo_image' => $logo_image, 'cover_image' => $banner_image);
	            }
				$condition = array('id' => $this->input->post('storeId'));
                $this->store_model->commonInsertUpdate(STORE_FRONT, 'update', $excludeArr, $dataArr, $condition);
				$returnArr['status'] = 1;
				$message = "Store details updated successfully";
				if($this->lang->line('json_store_updated_success') != ""){
				    $message = stripslashes($this->lang->line('json_store_updated_success'));
				}
				$returnArr['response'] = $message;
            } else {
                $returnArr['response'] = "StoreId Null !!";
            }
        }
		echo json_encode($returnArr);
    }

	/*
	|| >>>>>>>>>> Store Images Delete function <<<<<<<<<<< !!
	*/
	public function deleteStoreImage() {
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = intval($_POST['UserId']);
        if ($UserId == '') {
            $returnArr['response'] = 'UserId Null !!';
        } else {
			$store_Details = $this->store_model->get_all_details(STORE_FRONT, array('user_id' => $UserId));
			if($store_Details->num_rows() == 1){
				$condition = array('user_id' => $UserId);
				if($this->input->post('delete') != ""){
					if ($this->input->post('delete') == 'banner') {
		                $dataArr = array('cover_image' => '');
		            } else {
		                $dataArr = array('logo_image' => '');
		            }
		            $this->store_model->update_details(STORE_FRONT, $dataArr, $condition);
		            if ($this->input->post('delete') == 'banner') {
		                $lg_err_msg = 'Store banner deleted successfully';
		            } else {
		                $lg_err_msg = 'store logo deleted successfully';
		            }
					$returnArr['status'] = '1';
					$returnArr['response'] = $lg_err_msg;
				}else{
					$returnArr['response'] = "No Value for 'Delete' parameter !!";
				}
			}else{
				$returnArr['response'] = "No Such Store Found";
			}
        }
        echo json_encode($returnArr);
    }

	/*
	|| >>>>>>>>>> Store Privacy Settings <<<<<<<<<<< !!
	*/
	public function getStorePrivacyDetails() {
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = intval($_POST['UserId']);
		if ($UserId == '') {
			$returnArr['response'] = 'UserId Null !!';
        } else {
            $store_Details = $this->store_model->get_all_details(STORE_FRONT, array('user_id' => $UserId));
			if($store_Details->num_rows() >0 ){
				$storeDetails = array(
													'storeId' => $store_Details->row()->id,
													'privacy' => $store_Details->row()->privacy
												);
				$returnArr['status'] = 1;
				$message = "Success";
				if($this->lang->line('json_success') != ""){
					$message = stripslashes($this->lang->line('json_success'));
				}
				$returnArr['response'] = $message;
				$returnArr['storeDetails'] = $storeDetails;
			}else{
				$returnArr['response'] = "No Such Store Found";
			}
        }
		echo json_encode($returnArr);
    }

	/*
	|| >>>>>>>>>> Store Privacy update <<<<<<<<<<< !!
	*/
	public function storePrivacyUpdate() {
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = intval($_POST['UserId']);
		if ($UserId == '') {
			$returnArr['response'] = 'UserId Null !!';
		} else {
			$excludeArr = array('storeId','UserId');
			$dataArr = array('privacy' => $this->input->post('privacy'), 'user_id' => $this->input->post('UserId'));
			if ($_POST['storeId'] == '') {
				$returnArr['response'] = "StoreId Null !!";
			} else {
				$store_Details = $this->store_model->get_all_details(STORE_FRONT, array('user_id' => $UserId));
				if($store_Details->num_rows() == 1 ){
					$condition = array('id' => $this->input->post('storeId'));
					$this->store_model->commonInsertUpdate(STORE_FRONT, 'update', $excludeArr, $dataArr, $condition);
					$returnArr['status'] = (string)1;
					$message = "Terms & Conditions updated successfully";
					if($this->lang->line('json_store_updated_success') != ""){
						$message = stripslashes($this->lang->line('json_store_updated_success'));
					}
					$returnArr['response'] = $message;
				}else{
					$returnArr['response'] = "No Such Store Found";
				}
			}
		}
		echo json_encode($returnArr);
	}

	/*
	|| >>>>>>>>>> Store About Settings <<<<<<<<<<< !!
	*/
	public function getStoreAboutDetails() {
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = intval($_POST['UserId']);
		if ($UserId == '') {
			$returnArr['response'] = 'UserId Null !!';
        } else {
            $store_Details = $this->store_model->get_all_details(STORE_FRONT, array('user_id' => $UserId));
			if($store_Details->num_rows() >0 ){
				$storeDetails = array(
													'storeId' => $store_Details->row()->id,
													'description' => strip_tags($store_Details->row()->description)
												);
				$returnArr['status'] = 1;
				$message = "Success";
				if($this->lang->line('json_success') != ""){
					$message = stripslashes($this->lang->line('json_success'));
				}
				$returnArr['response'] = $message;
				$returnArr['storeDetails'] = $storeDetails;
			}else{
				$returnArr['response'] = "No Such Store Found";
			}
        }
		echo json_encode($returnArr);
    }

	/*
	|| >>>>>>>>>> Store About update <<<<<<<<<<< !!
	*/
	public function storeAboutUpdate() {
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = intval($_POST['UserId']);
		if ($UserId == '') {
			$returnArr['response'] = 'UserId Null !!';
        } else {
			$store_Details = $this->store_model->get_all_details(STORE_FRONT, array('user_id' => $UserId));
			if($store_Details->num_rows() == 1 ){
				$excludeArr = array('storeId','UserId');
	            $dataArr = array('description' => $this->input->post('description'), 'user_id' => $this->input->post('UserId'));
	            if ($this->input->post('storeId') == '') {
					$returnArr['response'] = "StoreId Null !!";
	            } else {
	                $condition = array('id' => $this->input->post('storeId'));
	                $this->store_model->commonInsertUpdate(STORE_FRONT, 'update', $excludeArr, $dataArr, $condition);
					$returnArr['status'] = (string)1;
					$message = "Store details updated successfully";
					if($this->lang->line('json_store_updated_success') != ""){
					    $message = stripslashes($this->lang->line('json_store_updated_success'));
					}
					$returnArr['response'] = $message;
	            }
			}else{
				$returnArr['response'] = "No Such Store Found";
			}
        }
		echo json_encode($returnArr);
    }

	/*
	|| >>>>>>>>>> Store Product <<<<<<<<<<< !!
	*/
	public function storeProducts() {
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$WantsProductDetail = array();
		$UserId = intval($_POST['UserId']);
		if ($UserId == '') {
			$returnArr['response'] = 'UserId Null !!';
		}else{
			$WantProductDetail = $this->store_model->get_all_details(PRODUCT, array('user_id' => $UserId));
			if($WantProductDetail->num_rows() > 0){
				foreach($WantProductDetail->result() as $wantsProduct){
					$imgArr=@explode(',',$wantsProduct->image);
					$productImage = ($imgArr[0]=='') ? '' : base_url().'images/product/'.$imgArr[0];
					$WantsProductDetail[]=array(
						'id'=>$wantsProduct->id,
						'sellerProductId'=>$wantsProduct->seller_product_id,
						'product_name'=>$wantsProduct->product_name,
						'UserId' =>$wantsProduct->user_id,
						'image'=>$productImage,
						'status'=> $wantsProduct->status,
						'productType'=>$wantsProduct->product_type,
						'purchased'=>$wantsProduct->purchasedCount,
						'sale_price'=>$wantsProduct->sale_price,
						'remain'=>$wantsProduct->quantity,
					);
				}
				$returnArr['status'] = 1;
				$message = "Success";
				if($this->lang->line('json_success') != ""){
					$message = stripslashes($this->lang->line('json_success'));
				}
				$returnArr['response'] = $message;
			}else{
				$returnArr['response'] = "You didn't add products till now !!";
			}
			$returnArr['ProductDetails'] = $WantsProductDetail;
		}
		echo json_encode($returnArr);
    }

	/*
	|| >>>>>>>>>> Store Product Status Settings <<<<<<<<<<< !!
	*/
	public function ProductChangeStatus() {
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = intval($_POST['UserId']);
        if ($UserId != '') {
			$pid = $this->input->post('sellerProductId');
            $status = $this->input->post('status');
            if ($pid != '' && $status != "") {
                $product_details = $this->product_model->get_all_details(PRODUCT, array('seller_product_id' => $pid));
                if ($product_details->num_rows() > 0) {
                    $dataArr = array('status' => $status);
                    $product_details = $this->product_model->update_details(PRODUCT, $dataArr, array('seller_product_id' => $pid));
					$returnArr['status'] = (string)1;
					$message = "Status changed successfully";
					if($this->lang->line('json_status_success') != ""){
					    $message = stripslashes($this->lang->line('json_status_success'));
					}
					$returnArr['response'] = $message;
                }
            }else{
				$returnArr['response'] = 'Some parameters are missing !!';
			}
        } else {
            $returnArr['response'] = 'UserId null !!';
        }
		echo json_encode($returnArr);
    }

	/*
	|| >>>>>>>>>> Store Product <<<<<<<<<<< !!
	*/
	public function storeCOD() {
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = intval($_POST['UserId']);
		if ($UserId != '') {
			$userDetails = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
			if($userDetails->num_rows() == 1){
				$cod = 0;
				if($userDetails->row()->cod_status == "yes"){$cod = 1;}
				$returnArr['status'] = 1;
				$returnArr['CODStatus'] = $cod;
				$returnArr['response'] = 'success';
			}else{
				$returnArr['response'] = 'No Such User Found !!';
			}
		}else{
		    $returnArr['response'] = 'UserId null !!';
		}
		echo json_encode($returnArr);
    }

	/*
	|| >>>>>>>>>> Store Product <<<<<<<<<<< !!
	*/
	public function updateCODStatus() {
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = intval($_POST['UserId']);
		if ($UserId != '') {
			$userDetails = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
			if($userDetails->num_rows() == 1){
				$mode = $_POST['mode'];
				if($mode != ""){
					$cod = "no";
					if($mode == 1){
						$cod = "yes";
					}
					$newdata = array('cod_status' => $cod);
					$condition = array('id' => $UserId);
					$this->mobile_model->update_details(USERS, $newdata, $condition);
					$returnArr['status'] = 1;
					$returnArr['response'] = 'success';
				}else{
					$returnArr['response'] = 'Mode Null !!';
				}
			}else{
				$returnArr['response'] = 'No Such User Found !!';
			}
		}else{
		    $returnArr['response'] = 'UserId null !!';
		}
		echo json_encode($returnArr);
    }

	public function digitalProductDownload() {
		$returnArr['status'] = (string)0;
		$returnArr['response'] = '';
		$user_id = $_POST['UserId'];
		$product_download_code = $_POST['code'];
		if($user_id !="" && $product_download_code !=""){
			$codeDetails = $this->product_model->get_all_details(PAYMENT, array('product_download_code' => $product_download_code, 'user_id' => $user_id, 'product_code_status' => "Pending"));
			/*echo $this->db->last_query();
			die;*/
			if ($codeDetails->num_rows() == 1){
				$id = $codeDetails->row()->id;
				$pid = $codeDetails->row()->product_id;
				$prodetails = $this->product_model->get_all_details(PRODUCT, array('id' => $pid));
				if ($prodetails->num_rows() > 0) {
					$digital = $prodetails->row()->product_doc;
				if(!empty($digital)){
					$imagesdigital = base_url().'pdf-doc/pdf/'.$digital;
				}else{
					$imagesdigital = " No file ";
				}
				$returnArr['response'] = " success ";
				$returnArr['digital_file'] = $imagesdigital;
				$returnArr['digital_file_name'] = $digital;
				$returnArr['status'] = 1;
				}else{
				$returnArr['response'] = " No product  ";
				}
			} else {
				$returnArr['response'] = " No digital file   ";
			}
		}else{
			$returnArr['response'] = " Some parameters missing  ";
		}
		echo json_encode($returnArr);
	}

	public function buyGiftCard(){
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = $_POST['UserId'];
		if($UserId != ""){
			$priceValue = $_POST['priceValue'];
			$recipientName = $_POST['recipientName'];
			$recipientEmail = $_POST['recipientEmail'];
			$message = $_POST['message'];
			if($priceValue != "" && $recipientName != "" && $recipientEmail != "" && $message != ""){
				$userDetails = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
				if($userDetails->num_rows() == 1){
					$senderEmail = $userDetails->row()->email;
					$senderName = $userDetails->row()->full_name;
					$dataArrVal = array(
												"user_id" => $UserId,
												"price_value" => $priceValue,
												"recipient_name" => $recipientName,
												"recipient_mail" => $recipientEmail,
												"sender_name" => $senderName,
												"sender_mail" => $senderEmail,
												"description" => $message,
											);
					$datestring = "%Y-%m-%d 23:59:59";
					$code = $this->get_rand_str('10');
					$exp_days = $this->config->item('giftcard_expiry_days');
					$dataArry_data = array(
													'expiry_date' => mdate($datestring, strtotime($exp_days . ' days')),
													'code' => $code,
												);
					$dataArr = array_merge($dataArrVal, $dataArry_data);
					$this->giftcards_model->simple_insert(GIFTCARDS_TEMP, $dataArr);
					$paymentGateways = $this->mobile_model->get_all_details(PAYMENT_GATEWAY, array('status' => "Enable"));
					$paymentTypes = array();
					if($paymentGateways->num_rows() > 0){
						foreach ($paymentGateways->result() as $gatewayData) {
							if($gatewayData->id != 4 && $gatewayData->id != 7){
								$paymentTypes[] = array(
														"paymentId" => $gatewayData->id,
														"paymentName" => $gatewayData->gateway_name,
													);
							}
						}
					}
					$returnArr['status'] = 1;
					$returnArr['response'] = 'Success';
					$returnArr['paymentTypes'] = $paymentTypes;
				}else{
					$returnArr['response'] = " No Such User Found !!";
				}
			}else{
				$returnArr['response'] = " Some Parameters Values are Missing !!";
			}
		}else{
			$returnArr['response'] = " UserId Null !!";
		}
		echo json_encode($returnArr);
	}

	public function giftCardPayment(){
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = $_GET['UserId'];
		if($UserId != ""){
			$paymentType = $_GET['paymentId'];
			if($paymentType != ""){
				redirect('json/checkout/cart?UserId='.$UserId.'&paymentId='.$paymentType.'&checkoutType=gift&AppWebView=1');
			}else{
				$returnArr['response'] = " paymentId Null !!";
			}
		}else{
			$returnArr['response'] = " UserId Null !!";
		}
		echo json_encode($returnArr);
	}


	public function getGiftCards() {
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = $_POST['UserId'];
		if($UserId != ""){
			$userDetails = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
			if($userDetails->num_rows() == 1){

				$giftcardDetails = $this->user_model->get_gift_cards_list($userDetails->row()->email);
				$giftList = array();
				if($giftcardDetails->num_rows() > 0){
					foreach($giftcardDetails->result() as $data){
						$giftList[] = array(
												"giftCode" => $data->code,
												"senderName" => $data->sender_name,
												"senderEmail" => $data->sender_mail,
												"priceValue" => $data->price_value,
												"expiresOn" => $data->expiry_date,
												"status" => $data->card_status,
											);
					}
				}
				$returnArr['status'] = 1;
				$returnArr['response'] = 'Success';
				$returnArr['giftCardList'] = $giftList;
			}else{
				$returnArr['response'] = " No Such User Found !!";
			}
		}else{
			$returnArr['response'] = " UserId Null !!";
		}
		echo json_encode($returnArr);
    }


	public function cmsPages(){
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = $_GET['UserId'];
		if($UserId != ""){
			$seourl = $_GET['seourl'];
			if($seourl != ""){
				$pageDetails = $this->product_model->get_all_details(CMS,array('seourl'=>$seourl,'status'=>'Publish'));
				if ($pageDetails->num_rows() == 0){
					$returnArr['response'] = "Something Went Wrong !!";
				}else {
					$this->data['heading'] = $pageDetails->row()->meta_title;
					$this->data['pageDetails'] = $pageDetails;
					$this->load->view('mobile/userSettings/cms',$this->data);
				}
			}else{
				$returnArr['response'] = " Seourl Null !!";
				echo json_encode($returnArr);
			}
		}else{
			$returnArr['response'] = " UserId Null !!";
			echo json_encode($returnArr);
		}
	}

	public function resendConfirmationMail(){
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = $_POST['UserId'];
		if($UserId != ""){
			$mail = $_POST['email'];
			if($mail != ""){
				if (valid_email($email)){
					$condition = array('email' => $mail);
		            $userDetails = $this->user_model->get_all_details(USERS, $condition);
					if($userDetails->num_rows() == 1){
						$this->send_confirm_mail($userDetails);
						$returnArr['status'] = 1;
						$returnArr['response'] = 'Verification Mail Sent Successfully !!';
					}else{
						$returnArr['response'] = " No Such User Found !!";
					}
				}else{
					$returnArr['response'] = " Enter Valid Email !!";
				}
			}else{
				$returnArr['response'] = " Email Null !!";
			}
		}else{
			$returnArr['response'] = " UserId Null !!";
			echo json_encode($returnArr);
		}
	}

	public function storeFeedback(){
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = $_POST['UserId'];
		if($UserId != ""){
			$productFeedback = $this->mobile_model->productFeedback($UserId);
			$storeFeedback = array();
			if($productFeedback->num_rows() > 0){
				foreach ($productFeedback->result() as $data) {
					$userImage = base_url()."images/users/user-thumb1.png";
					if($data->thumbnail != ""){
						$userImage = base_url()."images/users/".$data->thumbnail;
					}
					$storeFeedback[] = array(
													"userName" => $data->user_name,
													"userImage" => $userImage,
													"title" => $data->title,
													"description" => $data->description,
													"rating" => $data->rating,
													"date" => $data->dateAdded,
												);
				}
			}
			$returnArr['status'] = 1;
			$returnArr['response'] = 'Success';
			$returnArr['storeFeedback'] = $storeFeedback;
		}else{
			$returnArr['response'] = " UserId Null !!";
		}
		echo json_encode($returnArr);
	}

	/*
	|| >>>>>>>>>>>>>>>>> Web View Review - Purchase <<<<<<<<<<<<<<<<  ||
	*/
	public function productReview() {
		$returnArr['status'] = (string)0;
		$returnArr['response'] = '';
		$UserId = intval($_GET['UserId']);
		$this->data['UserId'] = $UserId;
        if ($UserId == '') {
            $returnArr['response'] = 'UserId Null !!';
			echo json_encode($returnArr);
        } else {
			$sid = intval($_GET['sellerId']);
			if($sid == ""){
				$returnArr['response'] = 'SellerId Null !!';
				echo json_encode($returnArr);
			}else{
				$dealCode = intval($_GET['InvoiceNumber']);
	            //$dealCode = $this->uri->segment(4, 0);
				if($dealCode == ""){
					$returnArr['response'] = 'Invoice Number Null !!';
					echo json_encode($returnArr);
				}else{
		            $view_mode = 'user';
		         	if ($sid == $UserId) {
		                $view_mode = 'seller';
		            }
	                if ($view_mode == 'seller') {
	                    $this->db->select('p.*,pAr.attr_name as attr_type,sp.attr_name');
	                    $this->db->from(PAYMENT . ' as p');
	                    $this->db->join(SUBPRODUCT . ' as sp', 'sp.pid = p.attribute_values', 'left');
	                    $this->db->join(PRODUCT_ATTRIBUTE . ' as pAr', 'pAr.id = sp.attr_id', 'left');
	                    $this->db->where('p.sell_id = "' . $sid . '" and p.status = "Paid" and p.dealCodeNumber = "' . $dealCode . '"');
	                    $order_details = $this->db->get();
	                    //$order_details = $this->user_model->get_all_details(PAYMENT,array('dealCodeNumber'=>$dealCode,'status'=>'Paid','sell_id'=>$sid));
	                } else {
	                    //$order_details = $this->user_model->get_all_details(PAYMENT,array('dealCodeNumber'=>$dealCode,'status'=>'Paid'));
	                    $this->db->select('p.*,pAr.attr_name as attr_type,sp.attr_name');
	                    $this->db->from(PAYMENT . ' as p');
	                    $this->db->join(SUBPRODUCT . ' as sp', 'sp.pid = p.attribute_values', 'left');
	                    $this->db->join(PRODUCT_ATTRIBUTE . ' as pAr', 'pAr.id = sp.attr_id', 'left');
	                    $this->db->where("p.status = 'Paid' and p.dealCodeNumber = '" . $dealCode . "'");
	                    $order_details = $this->db->get();
	                }
	                if ($order_details->num_rows() == 0) {
						$message = "Sorry, No Such Order Found";
						if($this->lang->line('json_no_order') != ""){
						    $message = stripslashes($this->lang->line('json_no_order'));
						}
						$returnArr['response'] = $message;
						echo json_encode($returnArr);
	                } else {
	                    if ($view_mode == 'user') {
	                        $this->data['user_details'] = $this->user_model->get_all_details(USERS, array('id' => $UserId));
	                        $this->data['seller_details'] = $this->user_model->get_all_details(USERS, array('id' => $sid));
	                    } elseif ($view_mode == 'seller') {
	                        $this->data['user_details'] = $this->user_model->get_all_details(USERS, array('id' => $UserId));
	                        $this->data['seller_details'] = $this->user_model->get_all_details(USERS, array('id' => $sid));
	                    }
	                    foreach ($order_details->result() as $order_details_row) {
	                        $this->data['prod_details'][$order_details_row->product_id] = $this->user_model->get_all_details(PRODUCT, array('id' => $order_details_row->product_id));
	                    }
	                    $this->data['view_mode'] = $view_mode;
	                    $this->data['order_details'] = $order_details;
	                    $sortArr1 = array('field' => 'date', 'type' => 'desc');
	                    $sortArr = array($sortArr1);
	                    $this->data['order_comments'] = $this->user_model->get_all_details(REVIEW_COMMENTS, array('deal_code' => $dealCode), $sortArr);
						$this->data['appWeb'] = "1";
	                    $this->load->view('mobile/userSettings/OrderReviews', $this->data);
	                }
				}
			}
        }
    }




	// public function cartCheckout() {
	// 	$returnArr['status'] = "0";
	// 	$returnArr['response'] = "";
    // //    $checkout_type = $_GET['checkout_type'];
	// 	$userid = $_GET['UserId'];
	// 	if($userid == ""){
	// 		$returnArr['response'] = "UserId Null !!";
	// 	}else{
	// 		$this->data['userDetails'] = $this->cart_model->get_all_details(USERS, array('id' => $userid));
	// 		if ($_GET['ship_need'] != "" && $_GET['Ship_address_val'] != '') {
	// 			$shipping_address = $this->cart_model->get_all_details(SHIPPING_ADDRESS, array('id' => $_GET['Ship_address_val']));
	// 			if($shipping_address->num_rows() == 1){
	// 				$dataArray = array(
	// 					'postal_code' => $shipping_address->row()->postal_code,
	// 					'address' => $shipping_address->row()->address1,
	// 					'address2' => $shipping_address->row()->address2,
	// 					'city' => $shipping_address->row()->city,
	// 					'state' => $shipping_address->row()->state,
	// 					'country' => $shipping_address->row()->country,
	// 					'phone_no' => $shipping_address->row()->phone_no,
	// 				);
	// 				$this->cart_model->update_details(USERS, $dataArray, array('id' => $userid));
	// 			}
	// 			$paymentType = $_GET['paymentId'];
	// 			$paymentGateway = $this->cart_model->get_all_details(PAYMENT_GATEWAY, array('status' => 'Enable', 'id' => $paymentType));
	// 			$dealcodeNumber = $this->mobile_model->addPaymentCart($userid, $paymentGateway->row()->gateway_name, $this->input->get('ship_products'));
	// 			$returnUrlDate[] = array(
	// 				'UserId' => $userid,
	// 				'paymentId' => $paymentType,
	// 				'invoiceNumber' => $dealcodeNumber,
	// 				'AppWebView' => "1"
	// 			);
	// 			$returnArr['status'] = "1";
	// 			$message = "Success";
	// 			if($this->lang->line('json_success') != ""){
	// 				$message = stripslashes($this->lang->line('json_success'));
	// 			}
	// 			$returnArr['response'] = $message;
	// 			$returnArr['UrlData'] = $returnUrlDate;
	// 			redirect('json/checkout/cart?UserId='.$userid.'&paymentId='.$paymentType.'&invoiceNumber='.$dealcodeNumber.'&AppWebView=1');
	// 		}else {
	// 			if ($this->lang->line('add_ship_addr_msg') != ''){
	// 				$lg_err_msg = $this->lang->line('add_ship_addr_msg');
	// 			}else{
	// 				$lg_err_msg = 'Please Add the Shipping address';
	// 				$returnArr['response'] = $lg_err_msg;
	// 			}
	// 			echo json_encode($returnArr);
	// 		}
	// 	}
    // }
}
