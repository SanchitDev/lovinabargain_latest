<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * User related functions
 * @author Teamtweaks
 *
 */
class MobileCheckout extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('cookie', 'date', 'form', 'email'));
        $this->load->library(array('encrypt', 'form_validation'));
        $this->load->model('checkout_model');
        if ($_SESSION['sMainCategories'] == '') {
            $sortArr1 = array('field' => 'cat_position', 'type' => 'asc');
            $sortArr = array($sortArr1);
            $_SESSION['sMainCategories'] = $this->checkout_model->get_all_details(CATEGORY, array('rootID' => '0', 'status' => 'Active'), $sortArr);
        }
        $this->data['mainCategories'] = $_SESSION['sMainCategories'];

        if ($_SESSION['sColorLists'] == '') {
            $_SESSION['sColorLists'] = $this->checkout_model->get_all_details(LIST_VALUES, array('list_id' => '1'));
        }
        $this->data['mainColorLists'] = $_SESSION['sColorLists'];

        $this->data['loginCheck'] = $this->checkLogin('U');
        $this->data['countryList'] = $this->checkout_model->get_all_details(COUNTRY_LIST, array());
        define("API_LOGINID", $this->config->item('payment_2'));
        /*         * *********************Adaptive Payment configuration************** */
        $this->config->load('paypal_adaptive');
        $config = array(
            'Sandbox' => $this->config->item('Sandbox'), // Sandbox / testing mode option.
            'APIUsername' => $this->config->item('APIUsername'), // PayPal API username of the API caller
            'APIPassword' => $this->config->item('APIPassword'), // PayPal API password of the API caller
            'APISignature' => $this->config->item('APISignature'), // PayPal API signature of the API caller
            'APISubject' => '', // PayPal API subject (email address of 3rd party user that has granted API permission for your app)
            'APIVersion' => $this->config->item('APIVersion'), // API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
            'DeviceID' => $this->config->item('DeviceID'),
            'ApplicationID' => $this->config->item('ApplicationID'),
            'DeveloperEmailAccount' => $this->config->item('DeveloperEmailAccount')
        );
        $this->load->library('paypal/Paypal_adaptive', $config);
        $this->data['appWeb'] = 1;
        /*         * **************************END********************** */
    }

    /**
     *
     * Loading Cart Page
     */
    public function index() {
        $UserId = $_GET['UserId'];
        $this->data['UserId'] = $UserId;
        if ($UserId != '') {
            $invoiceNumber = $_GET['invoiceNumber'];
            $checkout_type = $_GET['checkoutType'];
            if ($checkout_type != "subscribe") {
                $paymentId = $_GET['paymentId'];
            }
            $subscripid = $this->uri->segment(4);
            $this->data['userDetails'] = $this->checkout_model->get_all_details(USERS, array('id' => $UserId));
            $this->data['checkoutViewResults'] = $this->checkout_model->mani_checkout_total_mobile($UserId,$invoiceNumber);
            $this->data['GiftViewTotal'] = $this->checkout_model->mani_gift_total($UserId);
            $this->data['SubCribViewTotal'] = $this->checkout_model->mani_subcribe_total($UserId, $subscripid);
            $this->data['countryList'] = $this->checkout_model->get_all_details(COUNTRY_LIST, array());
            $this->data['invoiceNumber'] = $invoiceNumber;

            if ($paymentId == 7) {
                $this->load->view('mobile/checkout/checkout_cod.php', $this->data);
            } else if ($paymentId != '') {
                if($checkout_type == "gift"){
                    $this->load->view('mobile/gift/checkout_' . $paymentId . '.php', $this->data);
                }else{
                     $this->load->view('mobile/checkout/checkout_' . $paymentId . '.php', $this->data);
                }
            } else {
                $this->load->view('mobile/checkout/checkout.php', $this->data);
            }
        } else {
            $returnStr['status'] = 0;
            $returnStr['response'] = 'UserId Null, Please Login !!';
            echo json_encode($returnStr);
        }
    }

    public function CashonDelivery() {
        $excludeArr = array('total_price', 'CodSubmit', 'shipping_id','UserId','AppWebView','randomNo');
        $UserId = $this->input->post('UserId');
        $dataArr = array();
        $condition = array('id' => $UserId);
        $this->checkout_model->commonInsertUpdate(USERS, 'update', $excludeArr, $dataArr, $condition);
        $loginUserId =$UserId;
        $lastFeatureInsertId = $this->input->post('randomNo');
        //$lastFeatureInsertId = $this->session->userdata('randomNo');
        redirect('mobileOrder/success/' . $loginUserId . '/' . $lastFeatureInsertId . '/0/cod?AppWebView=1');
    }

    public function cod_PaymentProcess() {
        $excludeArr = array('paypalmode', 'paypalEmail', 'total_price', 'PaypalSubmit');
        $dataArr = array();
        $condition = array('id' => $this->checkLogin('U'));
        $this->checkout_model->commonInsertUpdate(USERS, 'update', $excludeArr, $dataArr, $condition);
        //echo '<pre>';print_r($_POST); die;
        /* Paypal integration start */
        $this->load->library('paypal_class');
        $item_name = $this->config->item('email_title') . ' Products';
        $totalAmount = $this->input->post('total_price');
        //User ID
        $loginUserId = $this->checkLogin('U');
        //DealCodeNumber
        $lastFeatureInsertId = $this->session->userdata('invoiceID');
        $quantity = 1;
        if ($this->input->post('paypalmode') == 'sandbox') {
            $this->paypal_class->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';   // testing paypal url
        } else {
            $this->paypal_class->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';     // paypal url
        }
        $this->paypal_class->add_field('currency_code', $this->data['currencyType']);
        $this->paypal_class->add_field('business', $this->input->post('paypalEmail')); // Business Email
        $this->paypal_class->add_field('return', base_url() . 'order/cod/' . $loginUserId . '/' . $lastFeatureInsertId . '/' . $totalAmount); // Return URL
        $this->paypal_class->add_field('cancel_return', base_url() . 'order/failure'); // Cancel URL
        $this->paypal_class->add_field('notify_url', base_url() . 'site/order/ipnpayment'); // Notify url
        $this->paypal_class->add_field('custom', $loginUserId . '|' . $lastFeatureInsertId . '|Product'); // Custom Values
        $this->paypal_class->add_field('item_name', $item_name); // Product Name
        $this->paypal_class->add_field('user_id', $loginUserId);
        $this->paypal_class->add_field('quantity', $quantity); // Quantity
        $this->paypal_class->add_field('amount', $totalAmount); // Price
        $this->paypal_class->submit_paypal_post();
    }

    public function cod_PaymentCredit() {
        $excludeArr = array('creditvalue', 'shipping_id', 'cardType', 'email', 'cardNumber', 'CCExpDay', 'CCExpMnth', 'creditCardIdentifier', 'total_price', 'CreditSubmit','AppWebView');
        $dataArr = array();
        $condition = array('id' => $this->checkLogin('U'));
        $this->checkout_model->commonInsertUpdate(USERS, 'update', $excludeArr, $dataArr, $condition);

        //User ID
        $loginUserId = $this->checkLogin('U');
        //DealCodeNumber
        $lastFeatureInsertId = $this->session->userdata('invoiceID');

        if ($this->input->post('creditvalue') == 'authorize') {
            //Authorize.net Intergration

            $Auth_Details = unserialize(API_LOGINID);
            $Auth_Setting_Details = unserialize($Auth_Details['settings']);

             
            define("AUTHORIZENET_API_LOGIN_ID", $Auth_Setting_Details['Login_ID']);    // Add your API LOGIN ID
            define("AUTHORIZENET_TRANSACTION_KEY", $Auth_Setting_Details['Transaction_Key']); // Add your API transaction key
            define("API_MODE", $Auth_Setting_Details['mode']);

            if (API_MODE == 'sandbox') {
                define("AUTHORIZENET_SANDBOX", true); // Set to false to test against production
            } else {
                define("AUTHORIZENET_SANDBOX", false);
            }
            define("TEST_REQUEST", "FALSE");
            require_once './authorize/AuthorizeNet.php';

            $transaction = new AuthorizeNetAIM;
            $transaction->setSandbox(AUTHORIZENET_SANDBOX);
            $transaction->setFields(
                    array(
                        'amount' => $this->input->post('total_price'),
                        'card_num' => $this->input->post('cardNumber'),
                        'exp_date' => $this->input->post('CCExpDay') . '/' . $this->input->post('CCExpMnth'),
                        'first_name' => $this->input->post('full_name'),
                        'last_name' => '',
                        'address' => $this->input->post('address'),
                        'city' => $this->input->post('city'),
                        'state' => $this->input->post('state'),
                        'country' => $this->input->post('country'),
                        'phone' => $this->input->post('phone_no'),
                        'email' => $this->input->post('email'),
                        'card_code' => $this->input->post('creditCardIdentifier'),
                    )
            );
            $response = $transaction->authorizeAndCapture();

            if ($response->approved) {
                redirect('order/cod/' . $loginUserId . '/' . $lastFeatureInsertId . '/' . $response->transaction_id . '/' . $this->input->post('total_price'));
            } else {
                //redirect('site/shopcart/cancel?failmsg='.$response->response_reason_text);
                redirect('order/failure/' . $response->response_reason_text);
            }
        } else if ($this->input->post('creditvalue') == 'paypaldodirect') {

            $shipValID = $this->checkout_model->get_all_details(SHIPPING_ADDRESS, array('id' => $this->input->post('shipping_id')));
            //echo '<pre>';print_r($shipValID->row()); die;

            $PaypalDodirect = unserialize($this->data['paypal_credit_card_settings']['settings']);
            $dodirects = array(
                'Sandbox' => $PaypalDodirect['mode'], // Sandbox / testing mode option.
                'APIUsername' => $PaypalDodirect['Paypal_API_Username'], // PayPal API username of the API caller
                'APIPassword' => $PaypalDodirect['paypal_api_password'], // PayPal API password of the API caller
                'APISignature' => $PaypalDodirect['paypal_api_Signature'], // PayPal API signature of the API caller
                'APISubject' => '', // PayPal API subject (email address of 3rd party user that has granted API permission for your app)
                'APIVersion' => '85.0'  // API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
            );

            // Show Errors
            if ($dodirects['Sandbox']) {
                 
                ini_set('display_errors', '1');
            }


            $this->load->library('Paypal_pro', $dodirects);

            $DPFields = array(
                'paymentaction' => '', // How you want to obtain payment.  Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
                'ipaddress' => $this->input->ip_address(), // Required.  IP address of the payer's browser.
                'returnfmfdetails' => '1'    // Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
            );


            $CCDetails = array(
                'creditcardtype' => $this->input->post('cardType'), // Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
                'acct' => $this->input->post('cardNumber'), // Required.  Credit card number.  No spaces or punctuation.
                'expdate' => $this->input->post('CCExpDay') . $this->input->post('CCExpMnth'), // Required.  Credit card expiration date.  Format is MMYYYY
                'cvv2' => $this->input->post('creditCardIdentifier'), // Requirements determined by your PayPal account settings.  Security digits for credit card.
                'startdate' => '', // Month and year that Maestro or Solo card was issued.  MMYYYY
                'issuenumber' => ''       // Issue number of Maestro or Solo card.  Two numeric digits max.
            );

            $PayerInfo = array(
                'email' => $this->input->post('email'), // Email address of payer.
                'payerid' => '', // Unique PayPal customer ID for payer.
                'payerstatus' => '', // Status of payer.  Values are verified or unverified
                'business' => ''
                    // Payer's business name.
            );

            $PayerName = array(
                'salutation' => 'Mr.', // Payer's salutation.  20 char max.
                'firstname' => $this->input->post('full_name'), // Payer's first name.  25 char max.
                'middlename' => '', // Payer's middle name.  25 char max.
                'lastname' => '', // Payer's last name.  25 char max.
                'suffix' => ''        // Payer's suffix.  12 char max.
            );

            //'x_amount'				=> ,
            //			'x_email'				=> $this->input->post('email'),

            $BillingAddress = array(
                'street' => $this->input->post('address'), // Required.  First street address.
                'street2' => '', // Second street address.
                'city' => $this->input->post('city'), // Required.  Name of City.
                'state' => $this->input->post('state'), // Required. Name of State or Province.
                'countrycode' => $this->input->post('country'), // Required.  Country code.
                'zip' => $this->input->post('postal_code'), // Required.  Postal code of payer.
                'phonenum' => $this->input->post('phone_no')       // Phone Number of payer.  20 char max.
            );

            $ShippingAddress = array(
                'shiptoname' => $shipValID->row()->full_name, // Required if shipping is included.  Person's name associated with this address.  32 char max.
                'shiptostreet' => $shipValID->row()->address1, // Required if shipping is included.  First street address.  100 char max.
                'shiptostreet2' => $shipValID->row()->address2, // Second street address.  100 char max.
                'shiptocity' => $shipValID->row()->city, // Required if shipping is included.  Name of city.  40 char max.
                'shiptostate' => $shipValID->row()->state, // Required if shipping is included.  Name of state or province.  40 char max.
                'shiptozip' => $shipValID->row()->postal_code, // Required if shipping is included.  Postal code of shipping address.  20 char max.
                'shiptocountry' => $shipValID->row()->country, // Required if shipping is included.  Country code of shipping address.  2 char max.
                'shiptophonenum' => $shipValID->row()->phone  // Phone number for shipping address.  20 char max.
            );

            $PaymentDetails = array(
                'amt' => $this->input->post('total_price'), // Required.  Total amount of order, including shipping, handling, and tax.
                'currencycode' => $this->data['currencyType'], // Required.  Three-letter currency code.  Default is USD.
                'itemamt' => '', // Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
                'shippingamt' => '', // Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
                'insuranceamt' => '', // Total shipping insurance costs for this order.
                'shipdiscamt' => '', // Shipping discount for the order, specified as a negative number.
                'handlingamt' => '', // Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
                'taxamt' => '', // Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax.
                'desc' => '', // Description of the order the customer is purchasing.  127 char max.
                'custom' => '', // Free-form field for your own use.  256 char max.
                'invnum' => '', // Your own invoice or tracking number
                'buttonsource' => '', // An ID code for use by 3rd party apps to identify transactions.
                'notifyurl' => '', // URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
                'recurring' => ''      // Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
            );

            // For order items you populate a nested array with multiple $Item arrays.
            // Normally you'll be looping through cart items to populate the $Item array
            // Then push it into the $OrderItems array at the end of each loop for an entire
            // collection of all items in $OrderItems.

            $OrderItems = array();

            $Item = array(
                'l_name' => '', // Item Name.  127 char max.
                'l_desc' => '', // Item description.  127 char max.
                'l_amt' => '', // Cost of individual item.
                'l_number' => '', // Item Number.  127 char max.
                'l_qty' => '', // Item quantity.  Must be any positive integer.
                'l_taxamt' => '', // Item's sales tax amount.
                'l_ebayitemnumber' => '', // eBay auction number of item.
                'l_ebayitemauctiontxnid' => '', // eBay transaction ID of purchased item.
                'l_ebayitemorderid' => ''     // eBay order ID for the item.
            );

            array_push($OrderItems, $Item);

            $Secure3D = array(
                'authstatus3d' => '',
                'mpivendor3ds' => '',
                'cavv' => '',
                'eci3ds' => '',
                'xid' => ''
            );

            $PayPalRequestData = array(
                'DPFields' => $DPFields,
                'CCDetails' => $CCDetails,
                'PayerInfo' => $PayerInfo,
                'PayerName' => $PayerName,
                'BillingAddress' => $BillingAddress,
                'ShippingAddress' => $ShippingAddress,
                'PaymentDetails' => $PaymentDetails,
                'OrderItems' => $OrderItems,
                'Secure3D' => $Secure3D
            );

            $PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);

            if (!$this->paypal_pro->APICallSuccessful($PayPalResult['ACK'])) {
                $errors = array('Errors' => $PayPalResult['ERRORS']);
                //$this->load->view('paypal_error',$errors);
                $newerrors = $errors['Errors'][0]['L_LONGMESSAGE'];
                redirect('order/failure/' . $newerrors);
            } else {
                // Successful call.  Load view or whatever you need to do here.
                redirect('order/cod/' . $loginUserId . '/' . $lastFeatureInsertId . '/' . $PayPalResult['TRANSACTIONID'] . '/' . $this->input->post('total_price'));
            }
        }
    }

    /*     * **************** Insert the checkout to user******************* */

    public function PaymentProcess() {
    //    echo "<pre>";print_r($_POST);die;
        $excludeArr = array('paypalmode', 'paypalEmail', 'total_price', 'PaypalSubmit','UserId','AppWebView','randomNo');
        $dataArr = array();
        $UserId = $this->input->post('UserId');
        $condition = array('id' =>$UserId);
        $this->checkout_model->commonInsertUpdate(USERS, 'update', $excludeArr, $dataArr, $condition);
        $this->load->library('paypal_class');
        $item_name = $this->config->item('email_title') . ' Products';
        $totalAmount = $this->input->post('total_price');
        $loginUserId = $UserId;        //User ID
    //    $lastFeatureInsertId = $this->session->userdata('randomNo');    //DealCodeNumber
        $lastFeatureInsertId = $this->input->post('randomNo');

        $quantity = 1;
        if ($this->input->post('paypalmode') == 'sandbox') {
            $this->paypal_class->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';   // testing paypal url
        } else {
            $this->paypal_class->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';     // paypal url
        }
        $this->paypal_class->add_field('currency_code', $this->data['currencyType']);
        $this->paypal_class->add_field('business', $this->input->post('paypalEmail')); // Business Email
        $this->paypal_class->add_field('return', base_url() . 'mobileOrder/success/' . $loginUserId . '/' . $lastFeatureInsertId.'?AppWebView=1');
        $this->paypal_class->add_field('cancel_return', base_url() . 'mobileOrder/failure/'.$loginUserId); // Cancel URL
        $this->paypal_class->add_field('notify_url', base_url() . 'site/mobileOrder/ipnpayment'); // Notify url
        $this->paypal_class->add_field('custom', $loginUserId . '|' . $lastFeatureInsertId . '|Product'); // Custom Values
        $this->paypal_class->add_field('item_name', $item_name); // Product Name
        $this->paypal_class->add_field('user_id', $loginUserId);
        $this->paypal_class->add_field('quantity', $quantity); // Quantity
        $this->paypal_class->add_field('amount', $totalAmount); // Price
        //$this->paypal_class->add_field('amount', 1); // Price
        $this->paypal_class->submit_paypal_post();
    }

    public function PaymentCredit() {
        $excludeArr = array('creditvalue', 'shipping_id', 'cardType', 'email', 'cardNumber', 'CCExpDay', 'CCExpMnth', 'creditCardIdentifier', 'total_price', 'CreditSubmit','UserId','AppWebView','randomNo');
        $dataArr = array();
    //    $condition = array('id' => $this->checkLogin('U'));
        $UserId = $this->input->post('UserId');
        $condition = array('id' => $UserId);

        $this->checkout_model->commonInsertUpdate(USERS, 'update', $excludeArr, $dataArr, $condition);

        //User ID
        $loginUserId = $UserId;
        //DealCodeNumber
        //$lastFeatureInsertId = $this->session->userdata('randomNo');
        $lastFeatureInsertId = $this->input->post('randomNo');

        if ($this->input->post('creditvalue') == 'authorize') {
            //Authorize.net Intergration

            $Auth_Details = unserialize(API_LOGINID);
            $Auth_Setting_Details = unserialize($Auth_Details['settings']);

             
            define("AUTHORIZENET_API_LOGIN_ID", $Auth_Setting_Details['Login_ID']);    // Add your API LOGIN ID
            define("AUTHORIZENET_TRANSACTION_KEY", $Auth_Setting_Details['Transaction_Key']); // Add your API transaction key
            define("API_MODE", $Auth_Setting_Details['mode']);

            if (API_MODE == 'sandbox') {
                define("AUTHORIZENET_SANDBOX", true); // Set to false to test against production
            } else {
                define("AUTHORIZENET_SANDBOX", false);
            }
            define("TEST_REQUEST", "FALSE");
            require_once './authorize/AuthorizeNet.php';

            $transaction = new AuthorizeNetAIM;
            $transaction->setSandbox(AUTHORIZENET_SANDBOX);
            $transaction->setFields(
                    array(
                        'amount' => $this->input->post('total_price'),
                        'card_num' => $this->input->post('cardNumber'),
                        'exp_date' => $this->input->post('CCExpDay') . '/' . $this->input->post('CCExpMnth'),
                        'first_name' => $this->input->post('full_name'),
                        'last_name' => '',
                        'address' => $this->input->post('address'),
                        'city' => $this->input->post('city'),
                        'state' => $this->input->post('state'),
                        'country' => $this->input->post('country'),
                        'phone' => $this->input->post('phone_no'),
                        'email' => $this->input->post('email'),
                        'card_code' => $this->input->post('creditCardIdentifier'),
                    )
            );
            $response = $transaction->authorizeAndCapture();

            if ($response->approved) {
                //$moveShoppingDataToPayment = $this->ibrandshopping_model->moveShoppingDataToPayment();
                //redirect('site/shopcart/returnpage/'.$loginUserId.'/'.$lastFeatureInsertId.'/'.$response->transaction_id);
                redirect('mobileOrder/success/' . $loginUserId . '/' . $lastFeatureInsertId . '/' . $response->transaction_id.'?AppWebView=1');
            } else {
                //redirect('site/shopcart/cancel?failmsg='.$response->response_reason_text);
                redirect('mobileOrder/failure/' . $response->response_reason_text.'/'.$loginUserId.'?AppWebView=1');
            }
        } else if ($this->input->post('creditvalue') == 'paypaldodirect') {

            $shipValID = $this->checkout_model->get_all_details(SHIPPING_ADDRESS, array('id' => $this->input->post('shipping_id')));
            //echo '<pre>';print_r($shipValID->row()); die;

            $PaypalDodirect = unserialize($this->data['paypal_credit_card_settings']['settings']);
            $dodirects = array(
                'Sandbox' => $PaypalDodirect['mode'], // Sandbox / testing mode option.
                'APIUsername' => $PaypalDodirect['Paypal_API_Username'], // PayPal API username of the API caller
                'APIPassword' => $PaypalDodirect['paypal_api_password'], // PayPal API password of the API caller
                'APISignature' => $PaypalDodirect['paypal_api_Signature'], // PayPal API signature of the API caller
                'APISubject' => '', // PayPal API subject (email address of 3rd party user that has granted API permission for your app)
                'APIVersion' => '85.0'  // API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
            );

            // Show Errors
            if ($dodirects['Sandbox']) {
                 
                ini_set('display_errors', '1');
            }


            $this->load->library('paypal/Paypal_pro', $dodirects);

            $DPFields = array(
                'paymentaction' => '', // How you want to obtain payment.  Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
                'ipaddress' => $this->input->ip_address(), // Required.  IP address of the payer's browser.
                'returnfmfdetails' => '1'    // Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
            );


            $CCDetails = array(
                'creditcardtype' => $this->input->post('cardType'), // Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
                'acct' => $this->input->post('cardNumber'), // Required.  Credit card number.  No spaces or punctuation.
                'expdate' => $this->input->post('CCExpDay') . $this->input->post('CCExpMnth'), // Required.  Credit card expiration date.  Format is MMYYYY
                'cvv2' => $this->input->post('creditCardIdentifier'), // Requirements determined by your PayPal account settings.  Security digits for credit card.
                'startdate' => '', // Month and year that Maestro or Solo card was issued.  MMYYYY
                'issuenumber' => ''       // Issue number of Maestro or Solo card.  Two numeric digits max.
            );

            $PayerInfo = array(
                'email' => $this->input->post('email'), // Email address of payer.
                'payerid' => '', // Unique PayPal customer ID for payer.
                'payerstatus' => '', // Status of payer.  Values are verified or unverified
                'business' => ''
                    // Payer's business name.
            );

            $PayerName = array(
                'salutation' => 'Mr.', // Payer's salutation.  20 char max.
                'firstname' => $this->input->post('full_name'), // Payer's first name.  25 char max.
                'middlename' => '', // Payer's middle name.  25 char max.
                'lastname' => '', // Payer's last name.  25 char max.
                'suffix' => ''        // Payer's suffix.  12 char max.
            );

//'x_amount'				=> ,
            //			'x_email'				=> $this->input->post('email'),

            $BillingAddress = array(
                'street' => $this->input->post('address'), // Required.  First street address.
                'street2' => '', // Second street address.
                'city' => $this->input->post('city'), // Required.  Name of City.
                'state' => $this->input->post('state'), // Required. Name of State or Province.
                'countrycode' => $this->input->post('country'), // Required.  Country code.
                'zip' => $this->input->post('postal_code'), // Required.  Postal code of payer.
                'phonenum' => $this->input->post('phone_no')       // Phone Number of payer.  20 char max.
            );

            $ShippingAddress = array(
                'shiptoname' => $shipValID->row()->full_name, // Required if shipping is included.  Person's name associated with this address.  32 char max.
                'shiptostreet' => $shipValID->row()->address1, // Required if shipping is included.  First street address.  100 char max.
                'shiptostreet2' => $shipValID->row()->address2, // Second street address.  100 char max.
                'shiptocity' => $shipValID->row()->city, // Required if shipping is included.  Name of city.  40 char max.
                'shiptostate' => $shipValID->row()->state, // Required if shipping is included.  Name of state or province.  40 char max.
                'shiptozip' => $shipValID->row()->postal_code, // Required if shipping is included.  Postal code of shipping address.  20 char max.
                'shiptocountry' => $shipValID->row()->country, // Required if shipping is included.  Country code of shipping address.  2 char max.
                'shiptophonenum' => $shipValID->row()->phone  // Phone number for shipping address.  20 char max.
            );

            $PaymentDetails = array(
                'amt' => $this->input->post('total_price'), // Required.  Total amount of order, including shipping, handling, and tax.
                'currencycode' => $this->data['currencyType'], // Required.  Three-letter currency code.  Default is USD.
                'itemamt' => '', // Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
                'shippingamt' => '', // Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
                'insuranceamt' => '', // Total shipping insurance costs for this order.
                'shipdiscamt' => '', // Shipping discount for the order, specified as a negative number.
                'handlingamt' => '', // Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
                'taxamt' => '', // Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax.
                'desc' => '', // Description of the order the customer is purchasing.  127 char max.
                'custom' => '', // Free-form field for your own use.  256 char max.
                'invnum' => '', // Your own invoice or tracking number
                'buttonsource' => '', // An ID code for use by 3rd party apps to identify transactions.
                'notifyurl' => '', // URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
                'recurring' => ''      // Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
            );

            // For order items you populate a nested array with multiple $Item arrays.
            // Normally you'll be looping through cart items to populate the $Item array
            // Then push it into the $OrderItems array at the end of each loop for an entire
            // collection of all items in $OrderItems.

            $OrderItems = array();

            $Item = array(
                'l_name' => '', // Item Name.  127 char max.
                'l_desc' => '', // Item description.  127 char max.
                'l_amt' => '', // Cost of individual item.
                'l_number' => '', // Item Number.  127 char max.
                'l_qty' => '', // Item quantity.  Must be any positive integer.
                'l_taxamt' => '', // Item's sales tax amount.
                'l_ebayitemnumber' => '', // eBay auction number of item.
                'l_ebayitemauctiontxnid' => '', // eBay transaction ID of purchased item.
                'l_ebayitemorderid' => ''     // eBay order ID for the item.
            );

            array_push($OrderItems, $Item);

            $Secure3D = array(
                'authstatus3d' => '',
                'mpivendor3ds' => '',
                'cavv' => '',
                'eci3ds' => '',
                'xid' => ''
            );

            $PayPalRequestData = array(
                'DPFields' => $DPFields,
                'CCDetails' => $CCDetails,
                'PayerInfo' => $PayerInfo,
                'PayerName' => $PayerName,
                'BillingAddress' => $BillingAddress,
                'ShippingAddress' => $ShippingAddress,
                'PaymentDetails' => $PaymentDetails,
                'OrderItems' => $OrderItems,
                'Secure3D' => $Secure3D
            );

            $PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);



            if (!$this->paypal_pro->APICallSuccessful($PayPalResult['ACK'])) {
                $errors = array('Errors' => $PayPalResult['ERRORS']);
                //$this->load->view('paypal_error',$errors);
                $newerrors = $errors['Errors'][0]['L_LONGMESSAGE'];
                redirect('mobileOrder/failure/' . $newerrors.'/'.$loginUserId.'?AppWebView=1');
            } else {
                // Successful call.  Load view or whatever you need to do here.
                redirect('mobileOrder/success/' . $loginUserId . '/' . $lastFeatureInsertId . '/' . $PayPalResult['TRANSACTIONID'].'?AppWebView=1');
            }
        }
    }

    /*     * ************************ Gift Cart Submit Options  ****************************** */

    public function PaymentProcessGift() {
        $UserId = $_POST['UserId'];
        if($UserId != ""){
            $excludeArr = array('paypalmode', 'paypalEmail', 'total_price', 'UserId','AppWebView');
            $dataArr = array();
            $condition = array('id' => $UserId);
            $this->checkout_model->commonInsertUpdate(USERS, 'update', $excludeArr, $dataArr, $condition);

            /* Paypal integration start */
            $this->load->library('paypal_class');
            $item_name = $this->config->item('email_title') . ' Gifts';
            $totalAmount = $this->input->post('total_price');
            $loginUserId = $UserId;
            $quantity = 1;
            if ($this->input->post('paypalmode') == 'sandbox') {
                $this->paypal_class->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';   // testing paypal url
            } else {
                $this->paypal_class->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';     // paypal url
            }
            $this->paypal_class->add_field('currency_code', $this->data['currencyType']);
            $this->paypal_class->add_field('business', $this->input->post('paypalEmail')); // Business Email
            $this->paypal_class->add_field('return', base_url() . 'mobileOrder/giftsuccess/' . $loginUserId); // Return URL
            $this->paypal_class->add_field('cancel_return', base_url() . 'mobileOrder/failure'); // Cancel URL
            $this->paypal_class->add_field('notify_url', base_url() . 'site/mobileOrder/ipnpayment'); // Notify url
            $this->paypal_class->add_field('custom', $loginUserId . '|Gift'); // Custom Values
            $this->paypal_class->add_field('item_name', $item_name); // Product Name
            $this->paypal_class->add_field('user_id', $loginUserId);
            $this->paypal_class->add_field('quantity', $quantity); // Quantity
            $this->paypal_class->add_field('amount', $totalAmount); // Price
            //$this->paypal_class->add_field('amount', 1); // Price
            $this->paypal_class->submit_paypal_post();
        }
    }





    public function PaymentCreditGift() {
        $UserId = $_POST['UserId'];
        if($UserId != ""){
            $excludeArr = array('creditvalue', 'UserId', 'email', 'cardNumber', 'CCExpDay', 'CCExpMnth', 'creditCardIdentifier', 'total_price');
            $dataArr = array();
            $condition = array('id' => $UserId);
            $this->checkout_model->commonInsertUpdate(USERS, 'update', $excludeArr, $dataArr, $condition);
            $loginUserId = $UserId;
            if ($this->input->post('creditvalue') == 'authorize') {
                $Auth_Details = unserialize(API_LOGINID);
                $Auth_Setting_Details = unserialize($Auth_Details['settings']);
                 
                define("AUTHORIZENET_API_LOGIN_ID", $Auth_Setting_Details['Login_ID']);    // Add your API LOGIN ID
                define("AUTHORIZENET_TRANSACTION_KEY", $Auth_Setting_Details['Transaction_Key']); // Add your API transaction key
                define("API_MODE", $Auth_Setting_Details['mode']);

                if (API_MODE == 'sandbox') {
                    define("AUTHORIZENET_SANDBOX", true); // Set to false to test against production
                } else {
                    define("AUTHORIZENET_SANDBOX", false);
                }
                define("TEST_REQUEST", "FALSE");
                require_once './authorize/AuthorizeNet.php';
                $transaction = new AuthorizeNetAIM;
                $transaction->setSandbox(AUTHORIZENET_SANDBOX);
                $transaction->setFields(
                        array(
                            'amount' => $this->input->post('total_price'),
                            'card_num' => $this->input->post('cardNumber'),
                            'exp_date' => $this->input->post('CCExpDay') . '/' . $this->input->post('CCExpMnth'),
                            'first_name' => $this->input->post('full_name'),
                            'last_name' => '',
                            'address' => $this->input->post('address'),
                            'city' => $this->input->post('city'),
                            'state' => $this->input->post('state'),
                            'country' => $this->input->post('country'),
                            'phone' => $this->input->post('phone_no'),
                            'email' => $this->input->post('email'),
                            'card_code' => $this->input->post('creditCardIdentifier'),
                        )
                );
                $response = $transaction->authorizeAndCapture();
                if ($response->approved) {
                    redirect('mobileOrder/giftsuccess/' . $loginUserId . '/' . $response->transaction_id);
                } else {
                    redirect('mobileOrder/failure/' . $response->response_reason_text);
                }
            }else if ($this->input->post('creditvalue') == 'paypaldodirect') {
                $PaypalDodirect = unserialize($this->data['paypal_credit_card_settings']['settings']);
                $dodirects = array(
                    'Sandbox' => $PaypalDodirect['mode'], // Sandbox / testing mode option.
                    'APIUsername' => $PaypalDodirect['Paypal_API_Username'], // PayPal API username of the API caller
                    'APIPassword' => $PaypalDodirect['paypal_api_password'], // PayPal API password of the API caller
                    'APISignature' => $PaypalDodirect['paypal_api_Signature'], // PayPal API signature of the API caller
                    'APISubject' => '', // PayPal API subject (email address of 3rd party user that has granted API permission for your app)
                    'APIVersion' => '85.0'  // API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
                );
                // Show Errors
                if ($dodirects['Sandbox']) {
                     
                    ini_set('display_errors', '1');
                }
                $this->load->library('paypal/Paypal_pro', $dodirects);
                $DPFields = array(
                    'paymentaction' => '', // How you want to obtain payment.  Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
                    'ipaddress' => $this->input->ip_address(), // Required.  IP address of the payer's browser.
                    'returnfmfdetails' => '1'    // Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
                );
                $CCDetails = array(
                    'creditcardtype' => $this->input->post('cardType'), // Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
                    'acct' => $this->input->post('cardNumber'), // Required.  Credit card number.  No spaces or punctuation.
                    'expdate' => $this->input->post('CCExpDay') . $this->input->post('CCExpMnth'), // Required.  Credit card expiration date.  Format is MMYYYY
                    'cvv2' => $this->input->post('creditCardIdentifier'), // Requirements determined by your PayPal account settings.  Security digits for credit card.
                    'startdate' => '', // Month and year that Maestro or Solo card was issued.  MMYYYY
                    'issuenumber' => ''       // Issue number of Maestro or Solo card.  Two numeric digits max.
                );
                $PayerInfo = array(
                    'email' => $this->input->post('email'), // Email address of payer.
                    'payerid' => '', // Unique PayPal customer ID for payer.
                    'payerstatus' => '', // Status of payer.  Values are verified or unverified
                    'business' => ''
                        // Payer's business name.
                );
                $PayerName = array(
                    'salutation' => 'Mr.', // Payer's salutation.  20 char max.
                    'firstname' => $this->input->post('full_name'), // Payer's first name.  25 char max.
                    'middlename' => '', // Payer's middle name.  25 char max.
                    'lastname' => '', // Payer's last name.  25 char max.
                    'suffix' => ''        // Payer's suffix.  12 char max.
                );
                $BillingAddress = array(
                    'street' => $this->input->post('address'), // Required.  First street address.
                    'street2' => '', // Second street address.
                    'city' => $this->input->post('city'), // Required.  Name of City.
                    'state' => $this->input->post('state'), // Required. Name of State or Province.
                    'countrycode' => $this->input->post('country'), // Required.  Country code.
                    'zip' => $this->input->post('postal_code'), // Required.  Postal code of payer.
                    'phonenum' => $this->input->post('phone_no')       // Phone Number of payer.  20 char max.
                );
                $ShippingAddress = array(
                    'shiptoname' => $this->input->post('full_name'), // Required if shipping is included.  Person's name associated with this address.  32 char max.
                    'shiptostreet' => $this->input->post('address'), // Required if shipping is included.  First street address.  100 char max.
                    'shiptostreet2' => $this->input->post('address2'), // Second street address.  100 char max.
                    'shiptocity' => $this->input->post('city'), // Required if shipping is included.  Name of city.  40 char max.
                    'shiptostate' => $this->input->post('state'), // Required if shipping is included.  Name of state or province.  40 char max.
                    'shiptozip' => $this->input->post('postal_code'), // Required if shipping is included.  Postal code of shipping address.  20 char max.
                    'shiptocountry' => $this->input->post('country'), // Required if shipping is included.  Country code of shipping address.  2 char max.
                    'shiptophonenum' => $this->input->post('phone_no')  // Phone number for shipping address.  20 char max.
                );
                $PaymentDetails = array(
                    'amt' => $this->input->post('total_price'), // Required.  Total amount of order, including shipping, handling, and tax.
                    'currencycode' => $this->data['currencyType'], // Required.  Three-letter currency code.  Default is USD.
                    'itemamt' => '', // Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
                    'shippingamt' => '', // Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
                    'insuranceamt' => '', // Total shipping insurance costs for this order.
                    'shipdiscamt' => '', // Shipping discount for the order, specified as a negative number.
                    'handlingamt' => '', // Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
                    'taxamt' => '', // Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax.
                    'desc' => '', // Description of the order the customer is purchasing.  127 char max.
                    'custom' => '', // Free-form field for your own use.  256 char max.
                    'invnum' => '', // Your own invoice or tracking number
                    'buttonsource' => '', // An ID code for use by 3rd party apps to identify transactions.
                    'notifyurl' => '', // URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
                    'recurring' => ''      // Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
                );
                // For order items you populate a nested array with multiple $Item arrays.
                // Normally you'll be looping through cart items to populate the $Item array
                // Then push it into the $OrderItems array at the end of each loop for an entire
                // collection of all items in $OrderItems.
                $OrderItems = array();
                $Item = array(
                    'l_name' => '', // Item Name.  127 char max.
                    'l_desc' => '', // Item description.  127 char max.
                    'l_amt' => '', // Cost of individual item.
                    'l_number' => '', // Item Number.  127 char max.
                    'l_qty' => '', // Item quantity.  Must be any positive integer.
                    'l_taxamt' => '', // Item's sales tax amount.
                    'l_ebayitemnumber' => '', // eBay auction number of item.
                    'l_ebayitemauctiontxnid' => '', // eBay transaction ID of purchased item.
                    'l_ebayitemorderid' => ''     // eBay order ID for the item.
                );
                array_push($OrderItems, $Item);
                $Secure3D = array(
                    'authstatus3d' => '',
                    'mpivendor3ds' => '',
                    'cavv' => '',
                    'eci3ds' => '',
                    'xid' => ''
                );
                $PayPalRequestData = array(
                    'DPFields' => $DPFields,
                    'CCDetails' => $CCDetails,
                    'PayerInfo' => $PayerInfo,
                    'PayerName' => $PayerName,
                    'BillingAddress' => $BillingAddress,
                    'ShippingAddress' => $ShippingAddress,
                    'PaymentDetails' => $PaymentDetails,
                    'OrderItems' => $OrderItems,
                    'Secure3D' => $Secure3D
                );
                $PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);
                if (!$this->paypal_pro->APICallSuccessful($PayPalResult['ACK'])) {
                    $errors = array('Errors' => $PayPalResult['ERRORS']);
                    $newerrors = $errors['Errors'][0]['L_LONGMESSAGE'];
                    redirect('mobileOrder/failure/' . $newerrors);
                } else {
                    redirect('mobileOrder/success/' . $loginUserId . '/' . $lastFeatureInsertId . '/' . $PayPalResult['TRANSACTIONID']);
                }
            }elseif($this->input->post('creditvalue') == 'twocheckout'){
    			$this->load->library('Twocheckout');
    			$twoCheckoutsettings = unserialize($this->config->item('payment_5'));
    			$settings = unserialize($twoCheckoutsettings['settings']);
    			Twocheckout::privateKey($settings['privateKey']);
    			Twocheckout::sellerId($settings['sellerId']);
    			Twocheckout::sandbox(true);
    			$totalAmount = $this->input->post('total_price');
    			$quantity = 1;
    			$tokenid = $this->input->post('token');
    			try{
    				$charge = Twocheckout_Charge::auth(array(
    					"merchantOrderId" => $lastFeatureInsertId,
    					"token" => $tokenid,
    					"currency" => 'USD',
    					"total" => $totalAmount,
    					"billingAddr" => array(
                                "name" => 'Testing Tester',
                                "addrLine1" => '123 Test St',
                                "city" => 'Columbus',
                                "state" => 'OH',
                                "zipCode" => '43123',
                                "country" => 'USA',
                                "email" => 'testingtester@2co.com',
                                "phoneNumber" => '555-555-5555'
    					),
    					"shippingAddr" => array(
                                "name" => $this->input->post('full_name'),
                                "addrLine1" => $this->input->post('address'),
                                "city" => $this->input->post('city'),
                                "state" => $this->input->post('state'),
                                "zipCode" => $this->input->post('postal_code'),
                                "country" => $this->input->post('country'),
                                "email" => $this->input->post('email'),
                                "phoneNumber" => $this->input->post('phone_no')
    					)
    				), 'array');
    				$result = (array)json_decode($charge);
    				$rescod = $result['response'];
    				if($rescod->responseCode == 'APPROVED'){
    					redirect('mobileOrder/giftsuccess/'.$loginUserId.'/'.$lastFeatureInsertId.'/'.$rescod->transactionId);
    				}else{
    					$rescod = $result[exception]->errorMsg;
    					redirect('mobileOrder/failure/'.str_replace("-", " ", url_title($rescod)));
    				}
    			}catch(Twocheckout_Error $e){
    				$e->getMessage();
    			}
    		}elseif($this->input->post('creditvalue') == 'stripe'){
                $this->load->library('Stripe');
                $success = "";
                $error = "";
                $totalAmount = $this->input->post('total_price');
                $crdno = $this->input->post('cardNumber');
                $crdname = $this->input->post('cardType');
                $year = $this->input->post('CCExpMnth');
                $mnth = $this->input->post('CCExpDay');
                $cvv = $this->input->post('creditCardIdentifier');
                $crd = array('number' => $crdno, 'exp_month' => $mnth, 'exp_year' => $year, 'cvc' => $cvv, 'name' => $crdname);
                $item_name = $this->config->item('email_title') . ' Products';
                if ($_POST) {
                    $success = Stripe::charge_card($totalAmount, $crd);
                    if (isset($success)) {
                        $result_success = (array) json_decode($success);
                        if ($result_success['status'] == "succeeded") {
                            $transaction_idstripe = $result_success['id'];
                            $statustrans['mgs'] = $result_success['status'];
                            redirect('mobileOrder/giftsuccess/' . $loginUserId . '/' . $lastFeatureInsertId . '/' . $transaction_idstripe);
                        } else {
                            $err = (array) $result_success['error'];
                            $statustrans['msg'] = $err['message'];
                            redirect('mobileOrder/failure/' . $statustrans['msg']);
                        }
                    }
                }
    		}
        }
    }

    /*     * ***************************************** Subscribe Form **************************************************************** */

    public function PaymentCreditSubscribe() {
        $excludeArr = array('email', 'cardNumber', 'CCExpDay', 'CCExpMnth', 'creditCardIdentifier', 'total_price', 'CreditSubscribeSubmit', 'invoiceNumber', 'subscription_type', 'subscription_id');
        $dataArr = array();
        $condition = array('id' => $this->checkLogin('U'));
        $this->checkout_model->commonInsertUpdate(USERS, 'update', $excludeArr, $dataArr, $condition);
        $userDetails = $this->checkout_model->get_all_details(USERS, $condition);

        $credit_type = 'recurring';
        $loginUserId = $this->checkLogin('U');
        //Authorize.net Intergration
        $this->load->library('authorize_arb');

        //echo '<pre>'; print_r($_POST);
        // Start with a create object
        $this->authorize_arb->startData('create');

        // Locally-defined reference ID (can't be longer than 20 chars)
        $refId = substr(md5(microtime() . 'ref'), 0, 20);
        $this->authorize_arb->addData('refId', $refId);

        // Data must be in this specific order
        // For full list of possible data, refer to the documentation:
        // http://www.authorize.net/support/ARB_guide.pdf
        //print_r($this->input->post('subscription_type'));die;
        $length = 1;
        if ($this->input->post('subscription_type') == "1 Month") {
            $length = 1;
        } else if ($this->input->post('subscription_type') == "3 Months") {
            $length = 3;
        } else if ($this->input->post('subscription_type') == "6 Months") {
            $length = 6;
        } else if ($this->input->post('subscription_type') == "1 Year") {
            $length = 12;
        }
        $subscription_data = array(
            'name' => $this->config->item('email_title') . ' Subscription',
            'paymentSchedule' => array(
                'interval' => array(
                    'length' => $length,
                    'unit' => 'months',
                ),
                'startDate' => date('Y-m-d'),
                'totalOccurrences' => 9999,
                'trialOccurrences' => 0,
            ),
            'amount' => $this->input->post('total_price'),
            'trialAmount' => 0.00,
            'payment' => array(
                'creditCard' => array(
                    'cardNumber' => $this->input->post('cardNumber'),
                    'expirationDate' => $this->input->post('CCExpMnth') . '-' . $this->input->post('CCExpDay'),
                    'cardCode' => $this->input->post('creditCardIdentifier'),
                ),
            ),
            'order' => array(
                'invoiceNumber' => $this->input->post('invoiceNumber'),
                'description' => $this->config->item('email_title') . ' Subscription',
            ),
            'customer' => array(
                'id' => $this->checkLogin('U'),
                'email' => $userDetails->row()->email,
                'phoneNumber' => $this->input->post('phone_no'),
            ),
            'billTo' => array(
                'firstName' => $this->input->post('full_name'),
                'lastName' => $this->input->post('full_name'),
                'address' => $this->input->post('address'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'zip' => $this->input->post('postal_code'),
                'country' => $this->input->post('country'),
            ),
        );
        //echo '<pre>'; print_r($subscription_data); die;
        $this->authorize_arb->addData('subscription', $subscription_data);

        // Send request
        if ($this->authorize_arb->send()) {
            //echo '<h1>Success! ID: ' . $this->authorize_arb->getId() . '</h1>';
            redirect('order/subscribesuccess/' . $loginUserId . '/' . $this->authorize_arb->getId() . '/' . $this->input->post('subscription_id'));
        } else {
            //echo '<h1>Epic Fail!</h1>';
            //echo '<p>' . $this->authorize_arb->getError() . '</p>';
            redirect('order/subscribesuccess/' . $loginUserId . '/1/' . $this->input->post('subscription_id'));
            //redirect('order/failure/'.$this->authorize_arb->getError());
        }

        // Show debug data
        //$this->authorize_arb->debug();
    }

    /*     * ****************************************Payment Balance Zero Using Gift Card****************************************************** */

    public function PaymentGiftFree() {

        $excludeArr = array('total_price', 'PaypalSubmit');
        $dataArr = array();
        $condition = array('id' => $this->checkLogin('U'));
        $this->checkout_model->commonInsertUpdate(USERS, 'update', $excludeArr, $dataArr, $condition);


        $item_name = $this->config->item('email_title') . ' Products';

        $totalAmount = $this->input->post('total_price');
        //User ID
        $loginUserId = $this->checkLogin('U');
        //DealCodeNumber
        $lastFeatureInsertId = $this->session->userdata('randomNo');

        redirect('order/successgift/' . $loginUserId . '/' . $lastFeatureInsertId); // Return URL
    }

    public function PaymentAdaptive() {
        $excludeArr = array('paypalmode', 'paypalEmail', 'total_price', 'PaypalSubmit','UserId','AppWebView','randomNo');
        $dataArr = array();
        $UserId = $this->input->post('UserId');
        //$condition = array('id' => $this->checkLogin('U'));
        $condition = array('id' => $UserId);
        $this->checkout_model->commonInsertUpdate(USERS, 'update', $excludeArr, $dataArr, $condition);
        $loginUserId = $UserId;
        //$lastFeatureInsertId = $this->session->userdata('randomNo');.
        $lastFeatureInsertId = $this->input->post('randomNo');
        $Query = 'select sc.*,u.id as uid,u.commision as ucom,
		u.paypal_email as uemail from ' . SHOPPING_CART . ' as sc left join ' . USERS . ' as u
		on (u.id = sc.sell_id) where sc.user_id = ' . $UserId . '';
        $get_details = $this->checkout_model->ExecuteQuery($Query);
        //echo("<pre>");print_r($get_details->result());die;
        $Details = array();
        $adminCommision = 0;
        if ($get_details->num_rows() > 0) {
            foreach ($get_details->result() as $_Details) {
                $admin_commision = (($_Details->total - $_Details->discountAmount) * ($_Details->ucom / 100));

                $adminCommision += $admin_commision;

                $ucom = ($_Details->total - $admin_commision - $_Details->discountAmount);
            }
            if (isset($Details[$_Details->uid]['amount'])) {

                $Details[$_Details->uid]['amount'] += $ucom;
            } else {

                $Details[$_Details->uid]['amount'] = $ucom;
                $Details[$_Details->uid]['email'] = $_Details->uemail;
            }
        }
        $SuccessURL = 'mobileOrder/success/' . $loginUserId . '/' . $lastFeatureInsertId . '/adaptive?AppWebView=1';
        $CancelURL = 'mobileOrder/failure/' . $loginUserId . '/' . $lastFeatureInsertId . '/adaptive?AppWebView=1';
        $curreny_type = 'USD';
        $PayRequestFields = array(
            'ActionType' => 'PAY', // Required.  Whether the request pays the receiver or whether the request is set up to create a payment request, but not fulfill the payment until the ExecutePayment is called.  Values are:  PAY, CREATE, PAY_PRIMARY
            'CancelURL' => site_url($CancelURL), // Required.  The URL to which the sender's browser is redirected if the sender cancels the approval for the payment after logging in to paypal.com.  1024 char max.
            'CurrencyCode' => $curreny_type, // Required.  3 character currency code.
            'FeesPayer' => 'EACHRECEIVER', // The payer of the fees.  Values are:  SENDER, PRIMARYRECEIVER, EACHRECEIVER, SECONDARYONLY
            'IPNNotificationURL' => '', // The URL to which you want all IPN messages for this payment to be sent.  1024 char max.
            'Memo' => '', // A note associated with the payment (text, not HTML).  1000 char max
            'Pin' => '', // The sener's personal id number, which was specified when the sender signed up for the preapproval
            'PreapprovalKey' => '', // The key associated with a preapproval for this payment.  The preapproval is required if this is a preapproved payment.
            'ReturnURL' => site_url($SuccessURL), // Required.  The URL to which the sener's browser is redirected after approvaing a payment on paypal.com.  1024 char max.
            'ReverseAllParallelPaymentsOnError' => '', // Whether to reverse paralel payments if an error occurs with a payment.  Values are:  TRUE, FALSE
            'SenderEmail' => '', // Sender's email address.  127 char max.
            'TrackingID' => ''                                          // Unique ID that you specify to track the payment.  127 char max.
        );

        $ClientDetailsFields = array(
            'CustomerID' => '', // Your ID for the sender  127 char max.
            'CustomerType' => '', // Your ID of the type of customer.  127 char max.
            'GeoLocation' => '', // Sender's geographic location
            'Model' => '', // A sub-identification of the application.  127 char max.
            'PartnerName' => ''                                          // Your organization's name or ID
        );

        $FundingTypes = array('ECHECK', 'BALANCE', 'CREDITCARD');
        $Receivers = array();
        $Receiver = array(
            'Amount' => $this->input->post('total_price'), // Required.  Amount to be paid to the receiver.
            'Email' => $this->input->post('paypalEmail'), // Receiver's email address. 127 char max.
            'InvoiceID' => '', // The invoice number for the payment.  127 char max.
            'PaymentType' => 'SERVICE', // $this->input->post('paypalEmail')Transaction type.  Values are:  GOODS, SERVICE, PERSONAL, CASHADVANCE, DIGITALGOODS
            'PaymentSubType' => '', // The transaction subtype for the payment.
            'Phone' => array('CountryCode' => '', 'PhoneNumber' => '', 'Extension' => ''), // Receiver's phone number.   Numbers only.
            'Primary' => 'TRUE'                                             // Whether this receiver is the primary receiver.  Values are boolean:  TRUE, FALSE
        );
        //echo("<pre>");print_r($Receiver);die;
        array_push($Receivers, $Receiver);
        foreach ($Details as $_Details) {
            $Receiver = array(
                'Amount' => $_Details['amount'], // Required.  Amount to be paid to the receiver.
                'Email' => $_Details['email'], // Receiver's email address. 127 char max.
                'InvoiceID' => '', // The invoice number for the payment.  127 char max.
                'PaymentType' => 'SERVICE', // Transaction type.  Values are:  GOODS, SERVICE, PERSONAL, CASHADVANCE, DIGITALGOODS
                'PaymentSubType' => '', // The transaction subtype for the payment.
                'Phone' => array('CountryCode' => '', 'PhoneNumber' => '', 'Extension' => ''), // Receiver's phone number.   Numbers only.
                'Primary' => 'FALSE'                                        // Whether this receiver is the primary receiver.  Values are boolean:  TRUE, FALSE
            );

            array_push($Receivers, $Receiver);
        }
        $SenderIdentifierFields = array(
            'UseCredentials' => ''                          // If TRUE, use credentials to identify the sender.  Default is false.
        );
        $AccountIdentifierFields = array(
            'Email' => '', // Sender's email address.  127 char max.
            'Phone' => array('CountryCode' => '', 'PhoneNumber' => '', 'Extension' => '')        // Sender's phone number.  Numbers only.
        );
        $PayPalRequestData = array(
            'PayRequestFields' => $PayRequestFields,
            'ClientDetailsFields' => $ClientDetailsFields,
// 			'FundingTypes' => $FundingTypes,
            'Receivers' => $Receivers,
            'SenderIdentifierFields' => $SenderIdentifierFields,
            'AccountIdentifierFields' => $AccountIdentifierFields
        );
//echo "<pre>";print_r($Receivers);die;
        $PayPalResult = $this->paypal_adaptive->Pay($PayPalRequestData);
        //echo "<pre>";print_r($PayPalResult);die;

        if (!$this->paypal_adaptive->APICallSuccessful($PayPalResult['Ack'])) {
            //echo "<pre>"; print_r($PayPalResult['Errors']); die;
            redirect('mobileOrder/failure/' . $loginUserId . '/' . $lastFeatureInsertId . '/adaptive/' . urlencode($PayPalResult['Errors'][0]['Message']).'?AppWebView=1');
        } else {
            $excludeArr = array('seller_id', 'product_type', 'paypalmode', 'paypalEmail', 'full_name', 'address', 'address2', 'city', 'state', 'country', 'postal_code', 'phone_no', 'total_price', 'email', 'PaypalSubmit','UserId','AppWebView','randomNo');
            $trans_id = $PayPalResult['CorrelationID'];
            $paykey = $PayPalResult['PayKey'];
            $dataArr = array(
                'transaction_id' => $trans_id,
                'pay_key' => $paykey,
                'user_id' => $loginUserId,
                'dealCodeNumber' => $lastFeatureInsertId,
                'status' => "Pending"
            );
            $condition = array();
            $this->checkout_model->commonInsertUpdate(ADAPTIVE_PAYMENTS, 'insert', $excludeArr, $dataArr, $condition);
            header('Location: ' . $PayPalResult['RedirectURL']);
            exit();
        }
    }

    public function contributor_payment() {
        //echo $this->data['currencyType']; die;
        $loginUserId = $this->checkLogin('U');
        $lastfeatureId = "CAPT" . time();
        $excludeArr = array('total_price', 'paypalEmail', 'sellerEmail', 'groupgift_id');
        $dataArr = array();
        $condition = array('id' => $loginUserId);
        $this->checkout_model->commonInsertUpdate(USERS, 'update', $excludeArr, $dataArr, $condition);
        $SuccessURL = 'groupgift/order/success/' . $loginUserId . '/' . $lastfeatureId;
        $CancelURL = 'groupgift/order/failure/' . $loginUserId . '/' . $lastfeatureId;
        $curreny_type = $this->data['currencyType'];
        //$curreny_type = "USD";
        $PayRequestFields = array(
            'ActionType' => 'PAY', // Required.  Whether the request pays the receiver or whether the request is set up to create a payment request, but not fulfill the payment until the ExecutePayment is called.  Values are:  PAY, CREATE, PAY_PRIMARY
            'CancelURL' => site_url($CancelURL), // Required.  The URL to which the sender's browser is redirected if the sender cancels the approval for the payment after logging in to paypal.com.  1024 char max.
            'CurrencyCode' => $curreny_type, // Required.  3 character currency code.
            'FeesPayer' => 'EACHRECEIVER', // The payer of the fees.  Values are:  SENDER, PRIMARYRECEIVER, EACHRECEIVER, SECONDARYONLY
            'IPNNotificationURL' => '', // The URL to which you want all IPN messages for this payment to be sent.  1024 char max.
            'Memo' => '', // A note associated with the payment (text, not HTML).  1000 char max
            'Pin' => '', // The sener's personal id number, which was specified when the sender signed up for the preapproval
            'PreapprovalKey' => '', // The key associated with a preapproval for this payment.  The preapproval is required if this is a preapproved payment.
            'ReturnURL' => site_url($SuccessURL), // Required.  The URL to which the sener's browser is redirected after approvaing a payment on paypal.com.  1024 char max.
            'ReverseAllParallelPaymentsOnError' => '', // Whether to reverse paralel payments if an error occurs with a payment.  Values are:  TRUE, FALSE
            'SenderEmail' => '', // Sender's email address.  127 char max.
            'TrackingID' => ''                                          // Unique ID that you specify to track the payment.  127 char max.
        );
        $ClientDetailsFields = array(
            'CustomerID' => '', // Your ID for the sender  127 char max.
            'CustomerType' => '', // Your ID of the type of customer.  127 char max.
            'GeoLocation' => '', // Sender's geographic location
            'Model' => '', // A sub-identification of the application.  127 char max.
            'PartnerName' => ''                                          // Your organization's name or ID
        );
        $FundingTypes = array('ECHECK', 'BALANCE', 'CREDITCARD');
        $Receivers = array();
        $Receiver = array(
            'Amount' => $this->input->post('total_price'), // Required.  Amount to be paid to the receiver.
            'Email' => $this->input->post('paypalEmail'), // Receiver's email address. 127 char max.
            'InvoiceID' => '', // The invoice number for the payment.  127 char max.
            'PaymentType' => 'SERVICE', // $this->input->post('paypalEmail')Transaction type.  Values are:  GOODS, SERVICE, PERSONAL, CASHADVANCE, DIGITALGOODS
            'PaymentSubType' => '', // The transaction subtype for the payment.
            'Phone' => array('CountryCode' => '', 'PhoneNumber' => '', 'Extension' => ''), // Receiver's phone number.   Numbers only.
            'Primary' => 'FALSE'                                             // Whether this receiver is the primary receiver.  Values are boolean:  TRUE, FALSE
        );
        array_push($Receivers, $Receiver);
        $SenderIdentifierFields = array(
            'UseCredentials' => ''                          // If TRUE, use credentials to identify the sender.  Default is false.
        );
        $AccountIdentifierFields = array(
            'Email' => '', // Sender's email address.  127 char max.
            'Phone' => array('CountryCode' => '', 'PhoneNumber' => '', 'Extension' => '')        // Sender's phone number.  Numbers only.
        );
        $PayPalRequestData = array(
            'PayRequestFields' => $PayRequestFields,
            'ClientDetailsFields' => $ClientDetailsFields,
            'FundingTypes' => $FundingTypes,
            'Receivers' => $Receivers,
            'SenderIdentifierFields' => $SenderIdentifierFields,
            'AccountIdentifierFields' => $AccountIdentifierFields
        );
        $PayPalResult = $this->paypal_adaptive->Pay($PayPalRequestData);
        if (!$this->paypal_adaptive->APICallSuccessful($PayPalResult['Ack'])) {
            redirect('groupgift/order/failure/' . $loginUserId . '/' . $lastfeatureId);
        } else {
            $excludeArr = array('total_price', 'full_name', 'address', 'address2', 'city', 'state', 'country', 'postal_code', 'phone_no', 'paypalEmail', 'sellerEmail');
            $trans_id = $PayPalResult['CorrelationID'];
            $paykey = $PayPalResult['PayKey'];
            $dataArr = array(
                'amount' => $this->input->post('total_price'),
                'contributor_name' => $this->input->post('full_name'),
                'groupgift_id' => $this->input->post('groupgift_id'),
                'transaction_id' => $trans_id,
                'DealcodeNo' => $lastfeatureId,
                'pay_key' => $paykey,
                'user_id' => $loginUserId,
                'payment_type' => "Paypal Adaptive",
                'status' => "Pending"
            );
            $condition = array();
            $this->checkout_model->commonInsertUpdate(GROUPGIFT_PAYMENT, 'insert', $excludeArr, $dataArr, $condition);
            header('Location: ' . $PayPalResult['RedirectURL']);
            exit();
        }
    }

    public function contributors_refund() {
        // Prepare request arrays
        $paypal_settings = unserialize($this->config->item('payment_3'));
        $paypalProcess = unserialize($paypal_settings['settings']);
        $pid = $this->uri->segment(4);
        $transactionDetails = $this->checkout_model->get_all_details(GROUPGIFT_PAYMENT, array('groupgift_id' => $pid));
        if ($transactionDetails->num_rows() > 0) {
            foreach ($transactionDetails->result() as $_transactionDetails) {
                $curreny_type = "USD";
                $RefundFields = array(
                    'CurrencyCode' => $curreny_type, // Required.  Must specify code used for original payment.  You do not need to specify if you use a payKey to refund a completed transaction.
                    'PayKey' => $_transactionDetails->pay_key, // Required.  The key used to create the payment that you want to refund.
                    'TransactionID' => '', // Required.  The PayPal transaction ID associated with the payment that you want to refund.
                    'TrackingID' => ''     // Required.  The tracking ID associated with the payment that you want to refund.
                );
                $Receivers = array();
                $Receiver = array(
                    'Email' => $paypalProcess['merchant_email'], // A receiver's email address.
                    'Amount' => $_transactionDetails->amount, // Amount to be debited to the receiver's account.
                    'Primary' => 'FALSE', // Set to true to indicate a chained payment.  Only one receiver can be a primary receiver.  Omit this field, or set to false for simple and parallel payments.
                    'Primary' => 'FALSE', // Set to true to indicate a chained payment.  Only one receiver can be a primary receiver.  Omit this field, or set to false for simple and parallel payments.
                    'InvoiceID' => '', // The invoice number for the payment.  This field is only used in Pay API operation.
                    'PaymentType' => 'SERVICE'   // The transaction subtype for the payment.  Allowable values are: GOODS, SERVICE
                );
                array_push($Receivers, $Receiver);
                $PayPalRequestData = array(
                    'RefundFields' => $RefundFields,
                    'Receivers' => $Receivers
                );
                $PayPalResult = $this->paypal_adaptive->Refund($PayPalRequestData);

                if (!$this->paypal_adaptive->APICallSuccessful($PayPalResult['Ack'])) {
                    $this->setErrorMessage('error', $PayPalResult['Errors'][0]['Message']);
                    redirect('admin/groupgift/contributors_paid');
                } else {
                    $excludeArr = array();
                    $dataArr = array('status' => "Refunded");
                    $condition = array('transaction_id' => $_transactionDetails->transaction_id);
                    $this->checkout_model->commonInsertUpdate(GROUPGIFT_PAYMENT, 'update', $excludeArr, $dataArr, $condition);

                    $dataArr1 = array('status' => "Cancelled Admin");
                    $condition1 = array('gift_seller_id' => $pid);
                    $this->checkout_model->commonInsertUpdate(GROUP_GIFTS, 'update', $excludeArr, $dataArr1, $condition1);
                    $giftDetails = $this->checkout_model->get_all_details(GROUP_GIFTS, array('gift_seller_id' => $pid));

                    $OrderId = $_transactionDetails->DealcodeNo;
                    $AdminEmail = $this->config->item('site_contact_mail');
                    $SellerDetails = $this->checkout_model->get_all_details(USERS, array('id' => $giftDetails->row()->sell_id));
                    $UserDetails = $this->checkout_model->get_all_details(USERS, array('id' => $giftDetails->row()->user_id));
                    $UserEmail = $UserDetails->row()->email;
                    $SellerEmail = $SellerDetails->row()->email;

                    $newsid = '29';
                    $template_values = $this->checkout_model->get_newsletter_template_details($newsid);
                    $subject = 'From: ' . $this->config->item('email_title') . ' - ' . $template_values['news_subject'];
                    $adminnewstemplateArr = array('email_title' => $this->config->item('email_title'), 'logo' => $this->data['logo']);
                    extract($adminnewstemplateArr);
                    //$ddd =htmlentities($template_values['news_descrip'],null,'UTF-8');
                    $header .="Content-Type: text/plain; charset=ISO-8859-1\r\n";
                    $message .= '<!DOCTYPE HTML>
						<html>
						<head>
						<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
						<meta name="viewport" content="width=device-width"/><body>';
                    include('./newsletter/registeration' . $newsid . '.php');
                    $message .= '</body>
						</html>';
                    if ($template_values['sender_name'] == '' && $template_values['sender_email'] == '') {
                        $sender_email = $this->data['siteContactMail'];
                        $sender_name = $this->data['siteTitle'];
                    } else {
                        $sender_name = $template_values['sender_name'];
                        $sender_email = $template_values['sender_email'];
                    }
                    $email_values = array('mail_type' => 'html',
                        'from_mail_id' => $sender_email,
                        'mail_name' => $sender_name,
                        'to_mail_id' => $UserEmail,
                        'cc_mail_id' => $AdminEmail . ',' . $SellerEmail,
                        'subject_message' => $template_values['news_subject'],
                        'body_messages' => $message,
                        'mail_id' => 'register mail'
                    );
                    $email_send_to_common = $this->product_model->common_email_send($email_values);
                }
            }
        }
        $this->setErrorMessage('success', 'Successfully Refunded this Payment');
        redirect('admin/groupgift/contributors_failed');
    }

    public function admin_Pay_to_Seller() {
        $GiftId = $this->uri->segment(4);
        $curreny_type = $this->data['currencyType'];
        $lastFeatureInsertId = "TXPY" . time();
        $Query = "select s.*,gf.gift_total, count(cp.id) as contributors from " . USERS . " as s left join " . GROUP_GIFTS . " as gf on (gf.sell_id = s.id) left join " . GROUPGIFT_PAYMENT . " as cp on (cp.groupgift_id = gf.gift_seller_id) where gf.gift_seller_id = '" . $GiftId . "'";
        $sellerDetails = $this->checkout_model->ExecuteQuery($Query);

        $per = $sellerDetails->row()->commision / 100;
        $admin_comis = $sellerDetails->row()->gift_total * $per;
        $seller_amount = $sellerDetails->row()->gift_total - $admin_comis;
        $this->load->library('paypal_class');
        $item_name = $sellerDetails->row()->paypal_email . ' Products';
        $totalAmount = $seller_amount;
        //User ID
        $loginUserId = $sellerDetails->row()->id;
        $quantity = 1;
        if ($this->input->post('paypalmode') == 'sandbox') {
            $this->paypal_class->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';   // testing paypal url
        } else {
            $this->paypal_class->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';     // paypal url
        }
        $this->paypal_class->add_field('currency_code', $this->data['currencyType']);

        $this->paypal_class->add_field('business', $sellerDetails->row()->paypal_email); // Business Email

        $this->paypal_class->add_field('return', base_url() . 'groupgift/payseller/success/' . $loginUserId . '/' . $lastFeatureInsertId); // Return URL

        $this->paypal_class->add_field('cancel_return', base_url() . 'groupgift/payseller/failure'); // Cancel URL

        $this->paypal_class->add_field('notify_url', base_url() . 'site/order/ipnpayment'); // Notify url

        $this->paypal_class->add_field('custom', $loginUserId . '|' . $lastFeatureInsertId . '|Product'); // Custom Values

        $this->paypal_class->add_field('item_name', $item_name); // Product Name

        $this->paypal_class->add_field('user_id', $loginUserId);

        $this->paypal_class->add_field('quantity', $quantity); // Quantity
        //echo $totalAmount;die;
        $this->paypal_class->add_field('amount', $totalAmount); // Price
        //$this->paypal_class->add_field('amount', 1); // Price
        //			echo base_url().'order/success/'.$loginUserId.'/'.$lastFeatureInsertId; die;

        $this->paypal_class->submit_paypal_post();
        $condition = array();
        $excludeArr = array();
        $dataArr = array(
            'grupgiftId' => $GiftId,
            'total_amt' => $sellerDetails->row()->gift_total,
            'total_contributors' => $sellerDetails->row()->contributors,
            'payment_type' => 'Paypal',
            'paid_amt' => $totalAmount,
            'admin_comm' => $admin_comis,
            'sell_id' => $sellerDetails->row()->id,
            'transactionId' => $lastFeatureInsertId,
            'status' => 'Pending'
        );
        $this->checkout_model->commonInsertUpdate(GROUPGIFT_PAYTO_SELLER, 'insert', $excludeArr, $dataArr, $condition);
    }

    /* Stripe integration */

    public function stripe_payment() {

        //$loginUserId = $this->checkLogin('U');
        $UserId = $this->input->post('UserId');
        $loginUserId = $UserId;
        $lastFeatureInsertId = $this->input->post('randomNo');
    //    $lastFeatureInsertId = $this->session->userdata('randomNo');

        $excludeArr = array('creditvalue', 'card_name', 'shipping_id', 'email', 'cardNumber', 'cardType', 'CCExpDay', 'CCExpMnth', 'creditCardIdentifier', 'exp_year', 'cvc_number1', 'total_price', 'CreditSubmit','UserId','AppWebView','randomNo');
        $dataArr = array();
        $condition = array('id' => $UserId);
        $this->checkout_model->commonInsertUpdate(USERS, 'update', $excludeArr, $dataArr, $condition);
        $this->load->library('Stripe');
        $success = "";
        $error = "";
        $totalAmount = $this->input->post('total_price');
        $crdno = $this->input->post('cardNumber');
        $crdname = $this->input->post('cardType');
        $year = $this->input->post('CCExpMnth');
        $mnth = $this->input->post('CCExpDay');
        $cvv = $this->input->post('creditCardIdentifier');
        $crd = array('number' => $crdno, 'exp_month' => $mnth, 'exp_year' => $year, 'cvc' => $cvv, 'name' => $crdname);
        $item_name = $this->config->item('email_title') . ' Products';
        if ($_POST) {
            //print_r($crd);die;
            $success = Stripe::charge_card($totalAmount, $crd);
            //echo "<pre>";print_r($result);die;
            if (isset($success)) {
                $result_success = (array) json_decode($success);


                if ($result_success['status'] == "succeeded") {
                    $transaction_idstripe = $result_success['id'];
                    $statustrans['mgs'] = $result_success['status'];
                    redirect('mobileOrder/success/' . $loginUserId . '/' . $lastFeatureInsertId . '/' . $transaction_idstripe.'?AppWebView=1');
                } else {
                    $err = (array) $result_success['error'];
                    $statustrans['msg'] = $err['message'];
                    redirect('mobileOrder/failure/' . $statustrans['msg'].'/'.$loginUserId.'?AppWebView=1');
                }
            }
        }
    }

    public function tocheckout_payment() {
        //echo("<pre>");print_r($this->input->post());dsdf;
        $excludeArr = array('creditvalue', 'card_name', 'seller_id', 'publishableKey', 'mode', 'shipping_id', 'email', 'cardNumber', 'cardType', 'CCExpDay', 'CCExpMnth', 'creditCardIdentifier', 'total_price', 'CreditSubmit', 'token','UserId','AppWebView','randomNo');
        $dataArr = array();
        $UserId = $this->input->post('UserId');
        $condition = array('id' => $UserId);
        $this->checkout_model->commonInsertUpdate(USERS, 'update', $excludeArr, $dataArr, $condition);
        $this->load->library('Twocheckout');
        $paypal_settings = unserialize($this->config->item('payment_5'));
        $settings = unserialize($paypal_settings['settings']);
        Twocheckout::privateKey($settings['privateKey']);
        Twocheckout::sellerId($settings['sellerId']);
        Twocheckout::sandbox(true);  #Uncomment to use Sandbox
        $tokenid = $this->input->post('token');
        //echo $tokenid;die;
        $loginUserId = $UserId;
        $lastFeatureInsertId = $this->input->post('randomNo');
    //    $lastFeatureInsertId = $this->session->userdata('randomNo');

        //$item_name = $this->config->item('email_title').' Products';

        $totalAmount = $this->input->post('total_price');

        $quantity = 1;



        try {
            $charge = Twocheckout_Charge::auth(array(
                        "merchantOrderId" => "123",
                        "token" => $tokenid,
                        "currency" => 'USD',
                        "total" => $totalAmount,
                        "billingAddr" => array(
                            "name" => 'Testing Tester',
                            "addrLine1" => '123 Test St',
                            "city" => 'Columbus',
                            "state" => 'OH',
                            "zipCode" => '43123',
                            "country" => 'USA',
                            "email" => 'testingtester@2co.com',
                            "phoneNumber" => '555-555-5555'
                        ),
                        "shippingAddr" => array(
                            "name" => $this->input->post('full_name'),
                            "addrLine1" => $this->input->post('address'),
                            "city" => $this->input->post('city'),
                            "state" => $this->input->post('state'),
                            "zipCode" => $this->input->post('postal_code'),
                            "country" => $this->input->post('country'),
                            "email" => $this->input->post('email'),
                            "phoneNumber" => $this->input->post('phone_no')
                        )
                            ), 'array');

            $result = (array) json_decode($charge);
            //echo "<pre>";print_r($result);die;
            $rescod = $result['response'];
            if ($rescod->responseCode == 'APPROVED') {
                redirect('mobileOrder/success/' . $loginUserId . '/' . $lastFeatureInsertId.'?AppWebView=1');
            } else {
                $rescod = $result[exception]->errorMsg;

                redirect('mobileOrder/failure/' . str_replace("-", " ", url_title($rescod)).'/'.$loginUserId.'?AppWebView=1');
            }
        } catch (Twocheckout_Error $e) {
            $e->getMessage();
        }
    }

    /**
     * Seller subscription methods
     */
    public function PaymentProcessStorefront() {

        //echo "<pre>"; print_r($_POST);die;
        $excludeArr = array('paypalmode', 'paypalEmail', 'total_price', 'PaypalSubmit', 'email', 'invoiceNumber', 'is_coupon', 'discount_Amt', 'CouponCode', 'Coupon_id', 'couponType', 'plan_value','UserId','AppWebView');

        $this->data['UserId'] = $UserId = $this->input->post('UserId');
        $this->data['invoiceNumber'] = $invoiceNumber = $this->input->post('invoiceNumber');


        $dataArr = array();
        $condition = array('id' => $UserId);
        $this->checkout_model->commonInsertUpdate(USERS, 'update', $excludeArr, $dataArr, $condition);

        // $checkUserPreference = $this->checkout_model->get_all_details(USERS,array('id' => $UserId));
        // $countryCurrency=$this->checkout_model->get_all_details(COUNTRY_LIST,array('country_code' => $checkUserPreference->row()->country));
        // $this->data['currencyType'] = $countryCurrency->row()->currency_type;
        // if($countryCurrency->row()->currency_type == ""){
            $this->data['currencyType'] = 'USD';
    //    }

        $this->data['totalAmount'] = $this->input->post('total_price');
        if ($this->data['totalAmount'] == $this->config->item('storefront_fees_year')) {
            $this->data['plan_type'] = 'Y';
        }else {
            $this->data['plan_type'] = 'M';
        }
        $this->data['paypalEmail'] = $this->input->post('paypalEmail');
        //DealCodeNumber
        $this->data['lastFeatureInsertId'] = $this->input->post('invoiceNumber');

        if ($this->input->post('paypalmode') == 'sandbox') {
            $this->data['paypal_url'] = 'https://www.sandbox.paypal.com/cgi-bin/webscr';   // testing paypal url
        } else {
            $this->data['paypal_url'] = 'https://www.paypal.com/cgi-bin/webscr';     // paypal url
        }

        $this->checkout_model->commonDelete(STORE_ARB, array('invoice_no' => $invoiceNumber));
        $dataARB = array(
            'user_id' => $UserId,
            'transaction_id' => $invoiceNumber,
            'invoice_no' => $invoiceNumber,
            'status' => 'Pending',
            'discount' => $this->input->post('discount_Amt'),
            'couponcode' => $this->input->post('CouponCode'),
            'couponid' => $this->input->post('Coupon_id'),
            'coupontype' => $this->input->post('couponType'),
            'amount' => $this->data['totalAmount'],
            'payment_type' => 'Paypal'
        );
        $this->checkout_model->simple_insert(STORE_ARB, $dataARB);

        $key = 'team-fancyy-clone-tweaks';

    //    $this->session->set_userdata('cde', $this->encrypt->encode($invoiceNumber, $key));
    //    $this->session->set_userdata('cde_user', $this->encrypt->encode($UserId, $key));

        $this->data['cde'] = $this->encrypt->encode($invoiceNumber, $key);
        $this->data['cde_user'] = $this->encrypt->encode($UserId, $key);
        $this->data['appWeb'] = "1";
        $this->load->view('mobile/checkout/paypal_subscription', $this->data);
    }


    public function PaymentCreditStorefront() {
        $excludeArr = array('email', 'cardNumber', 'CCExpDay', 'CCExpMnth', 'creditCardIdentifier', 'total_price', 'cardType', 'invoiceNumber', 'last_name');
        $dataArr = array();
        $condition = array('id' => $this->checkLogin('U'));
        $this->checkout_model->commonInsertUpdate(USERS, 'update', $excludeArr, $dataArr, $condition);
        $this->load->library('authorize_arb');
        $this->authorize_arb->startData('create');
        $refId = substr(md5(microtime() . 'ref'), 0, 20);
        $this->authorize_arb->addData('refId', $refId);
        $loginUserId = $this->checkLogin('U');
        $subscription_data = array(
            'name' => $this->config->item('email_title') . ' Subscription',
            'paymentSchedule' => array(
                'interval' => array(
                    'length' => 1,
                    'unit' => 'months',
                ),
                'startDate' => date('Y-m-d'),
                'totalOccurrences' => 9999,
                'trialOccurrences' => 0,
            ),
            'amount' => $this->input->post('total_price'),
            'trialAmount' => 0.00,
            'payment' => array(
                'creditCard' => array(
                    'cardNumber' => $this->input->post('cardNumber'),
                    'expirationDate' => $this->input->post('CCExpMnth') . '-' . $this->input->post('CCExpDay'),
                    'cardCode' => $this->input->post('creditCardIdentifier'),
                ),
            ),
            'order' => array(
                'invoiceNumber' => $this->input->post('invoiceNumber'),
                'description' => $this->config->item('email_title') . ' Subscription',
            ),
            'customer' => array(
                'id' => $this->checkLogin('U'),
                'email' => $this->input->post('email'),
                'phoneNumber' => $this->input->post('phone_no'),
            ),
            'billTo' => array(
                'firstName' => $this->input->post('full_name'),
                'lastName' => $this->input->post('last_name'),
                'address' => $this->input->post('address') . '' . $this->input->post('address2'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'zip' => $this->input->post('postal_code'),
                'country' => $this->input->post('country'),
            ),
        );
        $this->authorize_arb->addData('subscription', $subscription_data);
        if ($this->authorize_arb->send()) {
            $newsid = '30';
            $template_values = $this->checkout_model->get_newsletter_template_details($newsid);
            $email = $this->input->post('email');
            $invoice_no = $this->input->post('invoiceNumber');
            $transaction = $this->authorize_arb->getId();
            $email_message = "Payment Success. Please update your store setting";
            $subject = $template_values['news_subject'];
            $adminnewstemplateArr = array('email_title' => $this->config->item('email_title'), 'logo' => $this->data['logo']);
            extract($adminnewstemplateArr);
            //$ddd =htmlentities($template_values['news_descrip'],null,'UTF-8');
            $header .="Content-Type: text/plain; charset=ISO-8859-1\r\n";
            $message .= '<!DOCTYPE HTML>
				<html>
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
				<meta name="viewport" content="width=device-width"/><body>';
            include('./newsletter/registeration' . $newsid . '.php');
            $message .= '</body>
				</html>';
            if ($template_values['sender_name'] == '' && $template_values['sender_email'] == '') {
                $sender_email = $this->data['siteContactMail'];
                $sender_name = $this->data['siteTitle'];
            } else {
                $sender_name = $template_values['sender_name'];
                $sender_email = $template_values['sender_email'];
            }
            $email_values = array('mail_type' => 'html',
                'from_mail_id' => $sender_email,
                'mail_name' => $sender_name,
                'to_mail_id' => $email,
                'cc_mail_id' => $this->config->item('site_contact_mail'),
                'subject_message' => $template_values['news_subject'],
                'body_messages' => $message,
                'mail_id' => 'register mail'
            );
            $email_send_to_common = $this->product_model->common_email_send($email_values);
            $dataArr = array(
                'store_payment' => 'Paid'
//				'ref_comm_purchaser' => $this->config->item('ref_comm_purchaser_ug'),
//				'ref_comm_seller' => $this->config->item('ref_comm_seller_ug'),
//				'sell_commission' => $this->config->item('sell_commission_ug')
            );
            $condition = array('id' => $this->checkLogin('U'));
            $this->checkout_model->update_details(USERS, $dataArr, $condition);
            $dataARB = array(
                'user_id' => $this->checkLogin('U'),
                'transaction_id' => $this->authorize_arb->getId(),
                'invoice_no' => $this->session->userdata('InvoiceNo'),
                'status' => 'Paid',
                'payment_type' => 'Credit Card');
            $this->checkout_model->simple_insert(STORE_ARB, $dataARB);
            $this->checkout_model->simple_insert(STORE_FRONT, array('user_id' => $this->checkLogin('U'), 'store_name' => $this->data['userDetails']->row()->brand_name));
            $this->session->unset_userdata('InvoiceNo');
            $this->data['Confirmation'] = 'subscription_success';
            $this->data['errors'] = 'Payment Success';
            $this->load->view('site/order/order.php', $this->data);
        } else {
            $this->data['Confirmation'] = 'subscription_fail';
            $this->data['errors'] = 'Payment Failed';
            $this->load->view('site/order/order.php', $this->data);
        }
    }

    public function paymentStatus(){
        //echo $this->data['UserId'] = $this->uri->segment('5');
        //echo $this->uri->segment('4');
        $this->load->view('mobile/order/payment_return');
    }

    public function storeSubcription(){
        //echo $this->data['UserId'] = $this->uri->segment('5');
        //echo $this->uri->segment('4');
        $this->load->view('mobile/order/payment_return');
    }

    public function storeFailed(){
        //echo $this->data['UserId'] = $this->uri->segment('5');
        //echo $this->uri->segment('4');
        $this->load->view('mobile/order/payment_return');
    }

}

/* End of file checkout.php */
/* Location: ./application/controllers/site/checkout.php */
