<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * Notifications related functions
 * @author Teamtweaks
 *
 */
class Notify extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('cookie', 'date', 'email'));
        $this->load->model('notify_model');
        
       
        if ($_SESSION['sMainCategories'] == '') {
            $sortArr1 = array('field' => 'cat_position', 'type' => 'asc');
            $sortArr = array($sortArr1);
            $_SESSION['sMainCategories'] = $this->notify_model->get_all_details(CATEGORY, array('rootID' => '0', 'status' => 'Active'), $sortArr);
        }
        $this->data['mainCategories'] = $_SESSION['sMainCategories'];

        if ($_SESSION['sColorLists'] == '') {
            $_SESSION['sColorLists'] = $this->notify_model->get_all_details(LIST_VALUES, array('list_id' => '1'));
        }
        $this->data['mainColorLists'] = $_SESSION['sColorLists'];

        $this->data['loginCheck'] = $this->checkLogin('U');
        $this->data['likedProducts'] = array();
        if ($this->data['loginCheck'] != '') {
            $this->data['likedProducts'] = $this->notify_model->get_all_details(PRODUCT_LIKES, array('user_id' => $this->checkLogin('U')));
            $this->data['orderpurchaseProducts'] = $this->notify_model->get_orderpurchase_details($this->checkLogin('U'));
        }
    }

    public function getlatest() {

            
        //Language Start

        if ($this->lang->line('login_required') != '')
            $login_required = stripslashes($this->lang->line('login_required'));
        else
            $login_required = "Login required";

        if ($this->lang->line('follows_you') != '')
            $follows_you = stripslashes($this->lang->line('follows_you'));
        else
            $follows_you = "follows you";

        if ($this->lang->line('featured') != '')
            $featured = stripslashes($this->lang->line('featured'));
        else
            $featured = "featured";


        if ($this->lang->line('commented_on') != '')
            $commented_on = stripslashes($this->lang->line('commented_on'));
        else
            $commented_on = "commented on";


        if ($this->lang->line('no_notifications') != '')
            $no_notifications = stripslashes($this->lang->line('no_notifications'));
        else
            $no_notifications = "No Notifications";

        if ($this->lang->line('no_notifications_available') != '')
            $no_notifications_available = stripslashes($this->lang->line('no_notifications_available'));
        else
            $no_notifications_available = "No notifications available";

        if ($this->lang->line('ago') != '')
            $ago = stripslashes($this->lang->line('ago'));
        else
            $ago = "ago";
		if($this->lang->line('purchase') != '')
			$purchase = stripslashes($this->lang->line('purchase'));
		else
			$purchase = 'Purchase';

        //Language End

        $returnStr['status_code'] = 0;
        if ($this->checkLogin('U') == '') {
            $returnStr['message'] = $login_required;
        } else {
            $notifications = array_filter(explode(',', $this->data['userDetails']->row()->notifications));
            $searchArr = array();
            if (count($notifications) > 0) {
                if (in_array('wmn-follow', $notifications)) {
                    array_push($searchArr, 'follow');
                }
				
                if (in_array('wmn-comments_on_fancyd', $notifications)) {
                    array_push($searchArr, 'comment');
                }
                if (in_array('wmn-fancyd', $notifications)) {
                    array_push($searchArr, 'like');
                }
                if (in_array('wmn-featured', $notifications)) {
                    array_push($searchArr, 'featured');
                }
                if (in_array('wmn-comments', $notifications)) {
                    array_push($searchArr, 'own-product-comment');
                }
                if (in_array('wmn-review-comments', $notifications)) {
                	array_push($searchArr, 'review-comments');
                }if (in_array('wmn-purchased', $notifications)) {
                    array_push($searchArr, 'Purchased');
                }if (in_array('wmn-auction-process', $notifications)) {
                    array_push($searchArr, 'New Auction Request');
                }if (in_array('wmn-auction-process', $notifications)) {
                    array_push($searchArr, 'Sent New Offer');
                }if (in_array('wmn-auction-process', $notifications)) {
                    array_push($searchArr, 'Declined-Offer');
                }if (in_array('wmn-auction-process', $notifications)) {
                    array_push($searchArr, 'Reject-Offer');
                }if (in_array('wmn-auction-process', $notifications)) {
                    array_push($searchArr, 'Accept-Offer');
                }
				
                $likedProductsIdArr = array();
                if ($this->data['likedProducts']->num_rows() > 0) {
                    foreach ($this->data['likedProducts']->result() as $likeProdRow) {
                        array_push($likedProductsIdArr, $likeProdRow->product_id);
                    }
                    array_filter($likedProductsIdArr);
                }
                
                
                
                $orderProductsIdArr = array();
                if ($this->data['orderpurchaseProducts']->num_rows() > 0) {
                	foreach ($this->data['orderpurchaseProducts']->result() as $orderProdRow) {
                		array_push($orderProductsIdArr, $orderProdRow->product_id);
                	}
                	array_filter($orderProductsIdArr);
                }
                
                if (count($orderProductsIdArr) > 0) {
                	$fields = " p.product_name,p.id,p.seller_product_id,p.image,u.full_name,u.user_name,u.thumbnail,u.feature_product ";
                	$condition = ' where p.status="Publish" and u.status="Active" and p.id in (' . implode(',', $orderProductsIdArr) . ')
			 						or p.status="Publish" and p.user_id=0 and p.id in (' . implode(',', $orderProductsIdArr) . ') ';
                	$orderProductsDetails = $this->notify_model->get_active_sell_products($condition, $fields);
                } else {
                	$orderProductsDetails = '';
                }
                
                $orderProductsIdArr = array();
                if ($orderProductsDetails!='' && $orderProductsDetails->num_rows() > 0) {
                	foreach ($orderProductsDetails->result() as $orderProductsDetailsRow) {
                		array_push($orderProductsIdArr, $orderProductsDetailsRow->seller_product_id);
                	}
                	array_filter($orderProductsIdArr);
                }
                
                
//		 		$fieldsArr = array('product_name');
//   	 			$likedProductsDetails = $this->notify_model->get_fields_from_many(PRODUCT,$fieldsArr,'seller_product_id',$likedProductsIdArr);
                if (count($likedProductsIdArr) > 0) {
                    $fields = " p.product_name,p.id,p.seller_product_id,p.image,u.full_name,u.user_name,u.thumbnail,u.feature_product ";
                    $condition = ' where p.status="Publish" and u.status="Active" and p.seller_product_id in (' . implode(',', $likedProductsIdArr) . ')
			 						or p.status="Publish" and p.user_id=0 and p.seller_product_id in (' . implode(',', $likedProductsIdArr) . ') ';
                    $likedProductsDetails = $this->notify_model->get_active_sell_products($condition, $fields);
                } else {
                    $likedProductsDetails = '';
                }
                $addedSellProducts = $this->notify_model->get_all_details(PRODUCT, array('user_id' => $this->checkLogin('U'), 'status' => 'Publish'));
                $addedUserProducts = $this->notify_model->get_all_details(USER_PRODUCTS, array('user_id' => $this->checkLogin('U'), 'status' => 'Publish'));
                $addedSellProductsArr = array();
                $addedUserProductsArr = array();
                $addedProductsArr = array();
                if ($addedSellProducts->num_rows() > 0) {
                    foreach ($addedSellProducts->result() as $addedSellProductsRow) {
                        array_push($addedSellProductsArr, $addedSellProductsRow->seller_product_id);
                        array_push($addedProductsArr, $addedSellProductsRow->seller_product_id);
                    }
                }
                if ($addedUserProducts->num_rows() > 0) {
                    foreach ($addedUserProducts->result() as $addedUserProductsRow) {
                        array_push($addedUserProductsArr, $addedUserProductsRow->seller_product_id);
                        array_push($addedProductsArr, $addedUserProductsRow->seller_product_id);
                    }
                }
                
                
                if ($orderProductsDetails!='' && $orderProductsDetails->num_rows() > 0) {
                	foreach ($orderProductsDetails->result() as $orderProductsDetailsRow) {
                		array_push($orderProductsDetailsArr, $orderProductsDetailsRow->seller_product_id);
                		array_push($addedProductsArr, $orderProductsDetailsRow->seller_product_id);
                	}
                }
                
                
                $activityArr = array_merge($likedProductsIdArr, $addedProductsArr);
                array_push($activityArr, $this->checkLogin('U'));
                $allNoty = $this->notify_model->get_latest_notifications($searchArr, $activityArr, $this->checkLogin('U'));
                if ($allNoty->num_rows() > 0) {
                    $notyCount = 0;
                    $notyFinal = array();


                    foreach ($allNoty->result() as $allRow) {
                        //		if ($notyCount>4)break;
                        if ($allRow->activity == 'like') {
                            if (in_array($allRow->activity_id, $addedProductsArr)) {
                                array_push($notyFinal, array('user_id' => $allRow->user_id, 'activity' => 'like', 'activity_id' => $allRow->activity_id));
                                $notyCount++;
                            }
                        } else if ($allRow->activity == 'featured') {
                            if (in_array($allRow->activity_id, $addedProductsArr)) {
                                array_push($notyFinal, array('user_id' => $allRow->user_id, 'activity' => 'featured', 'activity_id' => $allRow->activity_id));
                                $notyCount++;
                            }
                        } else if ($allRow->activity == 'follow') {
                            if ($this->checkLogin('U') == $allRow->activity_id) {
                                array_push($notyFinal, array('user_id' => $allRow->user_id, 'activity' => 'follow', 'activity_id' => $allRow->activity_id));
                                $notyCount++;
                            }
                        } else if ($allRow->activity == 'comment') {
                            if (in_array($allRow->activity_id, $likedProductsIdArr)) {
                                array_push($notyFinal, array('user_id' => $allRow->user_id, 'activity' => 'comment', 'activity_id' => $allRow->activity_id));
                                $notyCount++;
                            }
                        } else if ($allRow->activity == 'own-product-comment') {
                            if (in_array($allRow->activity_id, $addedProductsArr)) {
                                array_push($notyFinal, array('user_id' => $allRow->user_id, 'activity' => 'own-product-comment', 'activity_id' => $allRow->activity_id));
                                $notyCount++;
                            }
                        } else if ($allRow->activity == 'review-comments') {
                        	if (in_array($allRow->activity_id, $orderProductsIdArr)) {
                                array_push($notyFinal, array('user_id' => $allRow->user_id, 'activity' => 'review-comments', 'activity_id' => $allRow->activity_id, 'created' => $allRow->created, 'comment_id' => $allRow->comment_id));
                                $notyCount++;
                            }
                        } else if ($allRow->activity == 'Purchased') {
                        	if (in_array($allRow->activity_id, $orderProductsIdArr)) {
                                array_push($notyFinal, array('user_id' => $allRow->user_id, 'activity' => 'Purchased', 'activity_id' => $allRow->activity_id, 'created' => $allRow->created, 'comment_id' => $allRow->comment_id));
                                $notyCount++;
                            }
                        } else if ($allRow->activity == 'New Auction Request') {
                        	if (in_array($allRow->activity_id, $orderProductsIdArr)) {
                                array_push($notyFinal, array('user_id' => $allRow->user_id, 'activity' => 'New Auction Request', 'activity_id' => $allRow->activity_id, 'created' => $allRow->created, 'comment_id' => $allRow->comment_id));
                                $notyCount++;
                            }
                        } else if ($allRow->activity == 'Sent New Offer') {
                        	if (in_array($allRow->activity_id, $orderProductsIdArr)) {
                                array_push($notyFinal, array('user_id' => $allRow->user_id, 'activity' => 'Sent New Offer', 'activity_id' => $allRow->activity_id, 'created' => $allRow->created, 'comment_id' => $allRow->comment_id));
                                $notyCount++;
                            }
                        } else if ($allRow->activity == 'Declined-Offer') {
                        	if (in_array($allRow->activity_id, $orderProductsIdArr)) {
                                array_push($notyFinal, array('user_id' => $allRow->user_id, 'activity' => 'Declined-Offer', 'activity_id' => $allRow->activity_id, 'created' => $allRow->created, 'comment_id' => $allRow->comment_id));
                                $notyCount++;
                            }
                        } else if ($allRow->activity == 'Reject-Offer') {
                        	if (in_array($allRow->activity_id, $orderProductsIdArr)) {
                                array_push($notyFinal, array('user_id' => $allRow->user_id, 'activity' => 'Reject-Offer', 'activity_id' => $allRow->activity_id, 'created' => $allRow->created, 'comment_id' => $allRow->comment_id));
                                $notyCount++;
                            }
                        } else if ($allRow->activity == 'Accept-Offer') {
                        	if (in_array($allRow->activity_id, $orderProductsIdArr)) {
                                array_push($notyFinal, array('user_id' => $allRow->user_id, 'activity' => 'Accept-Offer', 'activity_id' => $allRow->activity_id, 'created' => $allRow->created, 'comment_id' => $allRow->comment_id));
                                $notyCount++;
                            }
                        }
                    }
					
                    if ($notyCount > 0) {
                        $returnStr['content'] = '<ul>';
                        $total_count = 0;
                        foreach ($notyFinal as $notyFinalRow) {
                            if ($total_count > 4)
                                break;
                            $activityUserDetails = $this->notify_model->get_all_details(USERS, array('id' => $notyFinalRow['user_id']));
                            if ($activityUserDetails->num_rows() > 0) {
                                $userImg = 'user-thumb1.png';
                                if ($activityUserDetails->row()->thumbnail != '') {
                                    if (file_exists('images/users/' . $activityUserDetails->row()->thumbnail)) {
                                        $userImg = $activityUserDetails->row()->thumbnail;
                                    }
                                }
                                $activityUserLink = '<a href="user/' . $activityUserDetails->row()->user_name . '">
			   	 					<img src="images/users/' . $userImg . '" class="photo"/>
									</a>';
                                if ($notyFinalRow['activity'] != 'follow') {
                                    if (in_array($notyFinalRow['activity_id'], $addedSellProductsArr)) {
                                        $prodTbl = PRODUCT;
                                    } else if (in_array($notyFinalRow['activity_id'], $addedUserProductsArr)) {
                                        $prodTbl = USER_PRODUCTS;
                                    } else {
                                        $prodTbl = '';
                                    }
                                    if ($notyFinalRow['activity'] == 'comment') {
                                        $prodTbl = 'comment';
                                    }
                                    if ($notyFinalRow['activity'] == 'review-comments' || $notyFinalRow['activity'] == 'Purchased') {
                                    	$prodTbl = 'order';
                                    }
                                    if ($prodTbl == PRODUCT) {
                                        foreach ($addedSellProducts->result() as $addedSellProductsRow) {
                                            if ($addedSellProductsRow->seller_product_id == $notyFinalRow['activity_id']) {
                                                $imgArr = array_filter(explode(',', $addedSellProductsRow->image));
                                                if (count($imgArr) > 0) {
                                                    if (file_exists('images/product/' . $imgArr[0])) {
                                                        $prodImg = $imgArr[0];
                                                    }
                                                } else {
                                                    $prodImg = 'dummyProductImage.jpg';
                                                }
                                                $activityProdName = $addedSellProductsRow->product_name;
                                                $activityProdLink = '<a href="things/' . $addedSellProductsRow->id . '/' . url_title($addedSellProductsRow->product_name, '-') . '">
													<img src="images/product/' . $prodImg . '" class="thing"/>
													</a>';
                                                break;
                                            }
                                        }
                                    } else if ($prodTbl == USER_PRODUCTS) {
                                        foreach ($addedUserProducts->result() as $addedUserProductsRow) {
                                            if ($addedUserProductsRow->seller_product_id == $notyFinalRow['activity_id']) {
                                                $imgArr = array_filter(explode(',', $addedUserProductsRow->image));
                                                if (count($imgArr) > 0) {
                                                    if (file_exists('images/product/' . $imgArr[0])) {
                                                        $prodImg = $imgArr[0];
                                                    }
                                                } else {
                                                    $prodImg = 'dummyProductImage.jpg';
                                                }
                                                $activityProdName = $addedUserProductsRow->product_name;
                                                $activityProdLink = '<a href="user/' . $this->data['userDetails']->row()->user_name . '/things/' . $addedUserProductsRow->seller_product_id . '/' . url_title($addedUserProductsRow->product_name, '-') . '">
													<img src="images/product/' . $prodImg . '" class="thing"/>
													</a>';
                                                break;
                                            }
                                        }
                                    } else if ($prodTbl == 'comment') {
                                        if ($likedProductsDetails != '' && $likedProductsDetails->num_rows() > 0) {
                                            foreach ($likedProductsDetails->result() as $likedProductsDetailsRow) {
                                                if ($likedProductsDetailsRow->seller_product_id == $notyFinalRow['activity_id']) {
                                                    $imgArr = array_filter(explode(',', $likedProductsDetailsRow->image));
                                                    if (count($imgArr) > 0) {
                                                        if (file_exists('images/product/' . $imgArr[0])) {
                                                            $prodImg = $imgArr[0];
                                                        }
                                                    } else {
                                                        $prodImg = 'dummyProductImage.jpg';
                                                    }
                                                    $activityProdName = $likedProductsDetailsRow->product_name;
                                                    $activityProdLink = '<a href="things/' . $likedProductsDetailsRow->id . '/' . url_title($likedProductsDetailsRow->product_name, '-') . '">
														<img src="images/product/' . $prodImg . '" class="thing"/>
														</a>';
                                                    break;
                                                }
                                            }
                                        }
                                    } else if ($prodTbl == 'order') {
                                        if ($orderProductsDetails != '' && $orderProductsDetails->num_rows() > 0) {
                                            foreach ($orderProductsDetails->result() as $orderProductsDetailsRow) {
                                                if ($orderProductsDetailsRow->seller_product_id == $notyFinalRow['activity_id']) {
                                                    $imgArr = array_filter(explode(',', $orderProductsDetailsRow->image));
                                                    if (count($imgArr) > 0) {
                                                        if (file_exists('images/product/' . $imgArr[0])) {
                                                            $prodImg = $imgArr[0];
                                                        }
                                                    } else {
                                                        $prodImg = 'dummyProductImage.jpg';
                                                    }
                                                    $activityProdName = $orderProductsDetailsRow->product_name;
                                                    $activityProdLink = '<a href="things/' . $orderProductsDetailsRow->id . '/' . url_title($orderProductsDetailsRow->product_name, '-') . '">
														<img src="images/product/' . $prodImg . '" class="thing"/>
														</a>';
                                                    $activityProdNameLink = '<a href="things/' . $orderProductsDetailsRow->id . '/' . url_title($orderProductsDetailsRow->product_name, '-') . '">' . $activityProdName . '</a>.';
                                                    break;
                                                }
                                            }
                                        }
                                    } else {
                                        $activityProdName = '';
                                        $activityProdLink = '';
                                    }
                                }
                                
                                
                                
                                if ($notyFinalRow['comment_id'] != '0') {

                                		$cmtDetails = $this->notify_model->get_all_details(PRODUCT_COMMENTS, array('id' => $notyFinalRow['comment_id']));
                                		$comment = '';
                                		if ($cmtDetails->num_rows() > 0) {
                                			$comment = $cmtDetails->row()->comments;
                                			$activityTime = strtotime($cmtDetails->row()->dateAdded);
                                			$actTime = timespan($activityTime) . ' ' . $ago . '';
                                		}
                                }
                                
                                $li_count = 0;
                                if ($notyFinalRow['activity'] == 'follow') {
                                    if ($activityUserLink != '') {
                                        $userImg = 'user-thumb1.png';
                                        if ($activityUserDetails->row()->thumbnail != '') {
                                            if (file_exists('images/users/' . $activityUserDetails->row()->thumbnail)) {
                                                $userImg = $activityUserDetails->row()->thumbnail;
                                            }
                                        }
                                        $returnStr['content'] .= '<li style="color:white">' . $activityUserLink . $activityUserDetails->row()->full_name . ' ' . $follows_you . '
				   	 					</li>';
                                        $li_count++;
                                        $total_count++;
                                    }
                                } else if ($notyFinalRow['activity'] == 'like') {
                                    if ($activityUserLink != '' && $activityProdLink != '') {
                                        $returnStr['content'] .= '<li style="color:white">' . $activityUserLink . $activityProdLink . $activityUserDetails->row()->full_name . ' ' . LIKED_BUTTON . ' ' . $activityProdName . '</li>';
                                        $li_count++;
                                        $total_count++;
                                        $total_count++;
                                    }
                                } else if ($notyFinalRow['activity'] == 'featured') {
                                    if ($activityUserLink != '' && $activityProdLink != '') {
                                        $returnStr['content'] .= '<li style="color:white">' . $activityUserLink . $activityProdLink . $activityUserDetails->row()->full_name . ' ' . $featured . ' ' . $activityProdName . '</li>';
                                        $li_count++;
                                        $total_count++;
                                    }
                                } else if ($notyFinalRow['activity'] == 'comment') {
                                    if ($activityUserLink != '' && $activityProdLink != '') {
                                        $returnStr['content'] .= '<li style="color:white">' . $activityUserLink . $activityProdLink . $activityUserDetails->row()->full_name . ' ' . $commented_on . ' ' . $activityProdName . '</li>';
                                        $li_count++;
                                        $total_count++;
                                    }
                                } else if ($notyFinalRow['activity'] == 'own-product-comment') {
                                    if ($activityUserLink != '' && $activityProdLink != '') {
                                        $returnStr['content'] .= '<li style="color:white">' . $activityUserLink . $activityProdLink . $activityUserDetails->row()->full_name . ' ' . $commented_on . ' ' . $activityProdName . '</li>';
                                        $li_count++;
                                        $total_count++;
                                    }
                                } else if ($notyFinalRow['activity'] == 'review-comments') {
                                	$order = $this->notify_model->get_all_details(PAYMENT, array('dealCodeNumber' => $notyFinalRow['comment_id'],'sell_id'=>$this->checkLogin('U')));
                                	if($order->num_rows() > 0){
                                		$discussionLink = '<a href="order-review/'.$order->row()->user_id.'/'.$order->row()->sell_id.'/'.$order->row()->dealCodeNumber.'">#'.$notyFinalRow['comment_id'].'</a>';
                                	}else{
                                		$order = $this->notify_model->get_all_details(PAYMENT, array('dealCodeNumber' => $notyFinalRow['comment_id'],'user_id'=>$this->checkLogin('U')));
                                		$discussionLink = '<a href="order-review/'.$order->row()->user_id.'/'.$order->row()->sell_id.'/'.$order->row()->dealCodeNumber.'">#'.$notyFinalRow['comment_id'].'</a>';
                                	}
                                	 
                                	 
                                	if ($activityUserLink != '' && $activityProdLink != '') {
                                        $returnStr['content'] .= '<li style="color:white">' . $activityUserLink . $activityProdLink . $activityUserDetails->row()->full_name . ' ' . $commented_on . ' discussion '. $discussionLink .'</li>';
                                        $li_count++;
                                        $total_count++;
                                    }
                                }else if ($notyFinalRow['activity'] == 'Purchased') {
                                	$discussionLink = '<a href="order-review/'.$notyFinalRow['user_id'].'/'.$this->checkLogin('U').'/'.$notyFinalRow['comment_id'].'">#'.$notyFinalRow['comment_id'].'</a>';
                                	if ($activityUserLink != '' && $activityProdLink != '') {
                                        $returnStr['content'] .= '<li style="color:white">' . $activityUserLink . $activityProdLink . $activityUserDetails->row()->full_name . ' Purchased ' . $activityProdName .' '. $discussionLink.'</li>';
                                        $li_count++;
                                        $total_count++;
                                    }
                                }
								else if ($notyFinalRow['activity'] == 'New Auction Request') {
                                	$discussionLink = '<a href="bids">Inbox</a>';
                                	if ($activityUserLink != '' && $activityProdLink != '') {
                                        $returnStr['content'] .= '<li style="color:white">' . $activityUserLink . $activityProdLink . $activityUserDetails->row()->full_name . ' Sent Auction Request ' . $activityProdName .' '. $discussionLink.'</li>';
                                        $li_count++;
                                        $total_count++;
                                    }
                                }else if ($notyFinalRow['activity'] == 'Sent New Offer') {
                                	$discussionLink = '<a href="auctions">View</a>';
                                	if ($activityUserLink != '' ) {
                                        $returnStr['content'] .= '<li style="color:white">' . $activityUserLink . $activityProdLink . $activityUserDetails->row()->full_name . ' Sent New Offer ' . $activityProdName .' '. $discussionLink.'</li>';
                                        $li_count++;
                                        $total_count++;
                                    }
                                }else if ($notyFinalRow['activity'] == 'Declined-Offer') {
                                	$discussionLink = '<a href="bids">Inbox</a>';
                                	if ($activityUserLink != '' ) {
                                        $returnStr['content'] .= '<li style="color:white">' . $activityUserLink . $activityProdLink . $activityUserDetails->row()->full_name . ' Declined your offer ' . $activityProdName .' '. $discussionLink.'</li>';
                                        $li_count++;
                                        $total_count++;
                                    }
                                }else if ($notyFinalRow['activity'] == 'Reject-Offer') {
                                	$discussionLink = '<a href="bids">Inbox</a>';
                                	if ($activityUserLink != '' ) {
                                        $returnStr['content'] .= '<li style="color:white">' . $activityUserLink . $activityProdLink . $activityUserDetails->row()->full_name . ' Rejected your offer ' . $activityProdName .' '. $discussionLink.'</li>';
                                        $li_count++;
                                        $total_count++;
                                    }
                                }else if ($notyFinalRow['activity'] == 'Accept-Offer') {
                                	$discussionLink = '<a href="bids">Inbox</a>';
                                	if ($activityUserLink != '' ) {
                                        $returnStr['content'] .= '<li style="color:white">' . $activityUserLink . $activityProdLink . $activityUserDetails->row()->full_name . ' Accepted your offer ' . $activityProdName .' '. $discussionLink.'</li>';
                                        $li_count++;
                                        $total_count++;
                                    }
                                }
                                
                            }
                        }
                          if ($li_count == 0) {
                            $returnStr['status_code'] = 2;

                            $returnStr['content'] .= '<li style="padding-right:0px;padding-left:100px;width:180px;color:white">' . $no_notifications . '</li>';
                        } else { 
                            $returnStr['status_code'] = 1;
                        } 
						$returnStr['status_code'] = 1;
                        $returnStr['content'] .= '</ul>';
                    } else {
                        $returnStr['status_code'] = 2;

                        $returnStr['content'] = '<ul><li style="padding-right:0px;padding-left:100px;width:180px;color:white">' . $no_notifications . '</li></ul>';
                    }
                } else {
                    $returnStr['status_code'] = 2;
                    $returnStr['content'] = '<ul><li style="padding-right:0px;padding-left:100px;width:180px;color:white">' . $no_notifications . '</li></ul>';
                }
            } else {
                $returnStr['status_code'] = 2;

                $returnStr['content'] = '<ul><li style="padding-right:0px;padding-left:100px;width:180px;color:white">' . $no_notifications . '</li></ul>';
            }
            
        }
 
            $now = date("Y-m-d h:i:s");
            $UserAuctions   = $this->notify_model->get_all_details(PAYMENT,array('type'=>'auction', 'do_capture'=>'Pending', 'end_date >='=>$now, 'user_id'=>$this->checkLogin('U')));
            $SellerAuctions = $this->notify_model->get_all_details(BID_MESSAGE,array('type'=>'auction', 'status'=>'Pending', 'expire_time >='=>$now, 'receiverId'=>$this->checkLogin('U')));
            $totalNumRows   = $UserAuctions->num_rows() + $SellerAuctions->num_rows();
            if( $totalNumRows > 0 ){
                
               
                    if( $UserAuctions->num_rows > 0 ){
                        $auctheader      .= '<div id="auct-list1" class="auct-list1"><h4 class="aucthdr" >Sent Auctions</h4>';
                        foreach($UserAuctions->result() as $auction){
                            $end = $auction->end_date;
                            $pdimg = explode(',',$auction->old_image )[0];
                            $auctionlist = '<ul><li style="color:white">
                                                
                                                Auction Expires in 
                                                <div data-countdown="'.$end.'"></div>
                                                
                                                <a href="auction/'.$auction->id.'">
                                                    <img src="images/product/'.$pdimg.'" class="thing">
                                                </a>

                                            </li></ul>';
                        }
                        $auctheader      .='</div>';
                    }

                    if( $SellerAuctions->num_rows > 0 ){
                        $auctheader      .= '<div id="auct-list2" class="auct-list1"><h4 class="aucthdr" >Received Auctions</h4>';
                        foreach($SellerAuctions->result() as $auction){
                            $end = $auction->expire_time;
                            $pdctDetail = $this->notify_model->get_all_details(PRODUCT,array('id'=>$auction->productId));
                            $pdimg = explode(',',$pdctDetail->row()->image )[0];
                            $auctionlist = '<ul><li style="color:white">
                                                
                                                Auction Expires in 
                                                <div data-countdown="'.$end.'"></div>
                                                
                                                <a href="auction/'.$auction->id.'">
                                                    <img src="images/product/'.$pdimg.'" class="thing">
                                                </a>

                                            </li></ul>';
                        }
                        $auctheader      .='</div>';
                    }
            
               
                $returnStr['content'] .= $auctheader;
                $returnStr['content'] .= $auctionlist;
                
                $returnStr['content'] .=   '
                    <script type="text/javascript" src="'.base_url().'js/assets/lib/countdown/jquery.plugin.js"></script>
                    <script type="text/javascript" src="'.base_url().'js/site/jquery.countdown.js"></script>

                    <script type="text/javascript">
                        $(document).ready(function(e) {
                            $("[data-countdown]").each(function() {
                                
                                var day  = "Day";
                                var days = "Days";
                                var $this = $(this), finalDate = $(this).data("countdown");
                                $this.countdown(finalDate, function(event) {
                                    if( event.strftime("%D") != 00 && event.strftime("%D") == 01 ){
                                        $this.html(event.strftime("%D "+day+" %H:%M:%S"));
                                    }
                                    else if( event.strftime("%D") > 01 ){
                                        $this.html(event.strftime("%D "+days+" %H:%M:%S"));
                                    }
                                    else{
                                        $this.html(event.strftime("%H:%M:%S"));
                                    }
                               });
                            });
                           
                        });
                    </script>';

            }    
        echo json_encode($returnStr);
    }

    public function display_notifications() {
		
		if ($this->checkLogin('U') == '') {
            redirect(base_url() . 'login');die;
        }
        //Language Start
        if ($this->lang->line('login_required') != '')
            $login_required = stripslashes($this->lang->line('login_required'));
        else
            $login_required = "Login required";

        if ($this->lang->line('follows_you') != '')
            $follows_you = stripslashes($this->lang->line('follows_you'));
        else
            $follows_you = "follows you";

        if ($this->lang->line('featured') != '')
            $featured = stripslashes($this->lang->line('featured'));
        else
            $featured = "featured";


        if ($this->lang->line('commented_on') != '')
            $commented_on = stripslashes($this->lang->line('commented_on'));
        else
            $commented_on = "commented on";


        if ($this->lang->line('no_notifications') != '')
            $no_notifications = stripslashes($this->lang->line('no_notifications'));
        else
            $no_notifications = "No Notifications";

        if ($this->lang->line('no_notifications_available') != '')
            $no_notifications_available = stripslashes($this->lang->line('no_notifications_available'));
        else
            $no_notifications_available = "No notifications available";

        if ($this->lang->line('ago') != '')
            $ago = stripslashes($this->lang->line('ago'));
        else
            $ago = "ago";

        if ($this->lang->line('date_year') != '')
            $date_year = $this->lang->line('date_year');
        else
            $date_year = "Year";

        if ($this->lang->line('date_years') != '')
            $date_years = $this->lang->line('date_years');
        else
            $date_years = "Years";

        if ($this->lang->line('date_month') != '')
            $date_month = $this->lang->line('date_month');
        else
            $date_month = "Month";

        if ($this->lang->line('date_months') != '')
            $date_months = $this->lang->line('date_months');
        else
            $date_months = "Months";

        if ($this->lang->line('date_week') != '')
            $date_week = $this->lang->line('date_week');
        else
            $date_week = "Week";

        if ($this->lang->line('date_weeks') != '')
            $date_weeks = $this->lang->line('date_weeks');
        else
            $date_weeks = "Weeks";

        if ($this->lang->line('date_day') != '')
            $date_day = $this->lang->line('date_day');
        else
            $date_day = "Day";

        if ($this->lang->line('date_days') != '')
            $date_days = $this->lang->line('date_days');
        else
            $date_days = "Days";

        if ($this->lang->line('date_hour') != '')
            $date_hour = $this->lang->line('date_hour');
        else
            $date_hour = "Hour";

        if ($this->lang->line('date_hours') != '')
            $date_hours = $this->lang->line('date_hours');
        else
            $date_hours = "Hours";

        if ($this->lang->line('date_minute') != '')
            $date_minute = $this->lang->line('date_minute');
        else
            $date_minute = "Minute";

        if ($this->lang->line('date_minutes') != '')
            $date_minutes = $this->lang->line('date_minutes');
        else
            $date_minutes = "Minutes";

        if ($this->lang->line('date_second') != '')
            $date_second = $this->lang->line('date_second');
        else
            $date_second = "Second";

        if ($this->lang->line('date_seconds') != '')
            $date_seconds = $this->lang->line('date_seconds');
        else
            $date_seconds = "Seconds";

        if ($this->lang->line('ago') != '')
            $date_ago = $this->lang->line('ago');
        else
            $date_ago = "ago";

        //Language End
        $date_txt_arr = array(
            "Years",
            "Year",
            "Months",
            "Month",
            "Weeks",
            "Week",
            "Days",
            "Day",
            "Hours",
            "Hour",
            "Minutes",
            "Minute",
            "Seconds",
            "Second",
            "ago"
        );
        $date_lg_arr = array(
            $date_years,
            $date_year,
            $date_months,
            $date_month,
            $date_weeks,
            $date_week,
            $date_days,
            $date_day,
            $date_hours,
            $date_hour,
            $date_minutes,
            $date_minute,
            $date_seconds,
            $date_second,
            $date_ago
        );

        if ($this->checkLogin('U') == '') {
            show_404();
        } else {

            $notifications = array_filter(explode(',', $this->data['userDetails']->row()->notifications));
			//echo '<pre>';print_r($notifications);
		   $searchArr = array();
            if (count($notifications) > 0) {
                if (in_array('wmn-follow', $notifications)) {
                    array_push($searchArr, 'follow');
                }
                if (in_array('wmn-comments_on_fancyd', $notifications)) {
                    array_push($searchArr, 'comment');
                }
                if (in_array('wmn-fancyd', $notifications)) {
                    array_push($searchArr, 'like');
                }
                if (in_array('wmn-featured', $notifications)) {
                    array_push($searchArr, 'featured');
                }
                if (in_array('wmn-comments', $notifications)) {
                    array_push($searchArr, 'own-product-comment');
                }
                if (in_array('wmn-review-comments', $notifications)) {
                	array_push($searchArr, 'review-comments');
                }if (in_array('wmn-purchased', $notifications)) {
                    array_push($searchArr, 'Purchased');
                }if (in_array('wmn-auction-process', $notifications)) {
                    array_push($searchArr, 'New Auction Request');
                }if (in_array('wmn-auction-process', $notifications)) {
                    array_push($searchArr, 'Sent New Offer');
                }if (in_array('wmn-auction-process', $notifications)) {
                    array_push($searchArr, 'Declined-Offer');
                }if (in_array('wmn-auction-process', $notifications)) {
                    array_push($searchArr, 'Reject-Offer');
                }if (in_array('wmn-auction-process', $notifications)) {
                    array_push($searchArr, 'Accept-Offer');
                }
                $likedProductsIdArr = array();
				
				if ($this->data['likedProducts']->num_rows() > 0) {
                    foreach ($this->data['likedProducts']->result() as $likeProdRow) {
                        array_push($likedProductsIdArr, $likeProdRow->product_id);
                    }
                    array_filter($likedProductsIdArr);
                }
                
                
                $orderProductsIdArr = array();
                if ($this->data['orderpurchaseProducts']->num_rows() > 0) {
                	foreach ($this->data['orderpurchaseProducts']->result() as $orderProdRow) {
                		array_push($orderProductsIdArr, $orderProdRow->product_id);
                	}
                	array_filter($orderProductsIdArr);
                }
                
                if (count($orderProductsIdArr) > 0) {
                	$fields = " p.product_name,p.id,p.seller_product_id,p.image,u.full_name,u.user_name,u.thumbnail,u.feature_product ";
                	$condition = ' where p.status="Publish" and u.status="Active" and p.id in (' . implode(',', $orderProductsIdArr) . ')
			 						or p.status="Publish" and p.user_id=0 and p.id in (' . implode(',', $orderProductsIdArr) . ') ';
                	$orderProductsDetails = $this->notify_model->get_active_sell_products($condition, $fields);
                } else {
                	$orderProductsDetails = '';
                }
               
                $orderProductsIdArr = array();
                if ($orderProductsDetails!='' && $orderProductsDetails->num_rows() > 0) {
	                foreach ($orderProductsDetails->result() as $orderProductsDetailsRow) {
	                	array_push($orderProductsIdArr, $orderProductsDetailsRow->seller_product_id);
	                }
	                array_filter($orderProductsIdArr);
                }
                
				if (count($likedProductsIdArr) > 0) { 
                    $fields = " p.product_name,p.id,p.seller_product_id,p.image,u.full_name,u.user_name,u.thumbnail,u.feature_product ";
                    $condition = ' where p.status="Publish" and u.status="Active" and p.seller_product_id in (' . implode(',', $likedProductsIdArr) . ')
			 						or p.status="Publish" and p.user_id=0 and p.seller_product_id in (' . implode(',', $likedProductsIdArr) . ') ';
                    $likedProductsDetails = $this->notify_model->get_active_sell_products($condition, $fields);
                } else {
                    $likedProductsDetails = '';
                }
                $addedSellProducts = $this->notify_model->get_all_details(PRODUCT, array('user_id' => $this->checkLogin('U'), 'status' => 'Publish'));
                $addedUserProducts = $this->notify_model->get_all_details(USER_PRODUCTS, array('user_id' => $this->checkLogin('U'), 'status' => 'Publish'));
                $addedSellProductsArr = array();
                $addedUserProductsArr = array();
                $addedProductsArr = array();
                
                if ($addedSellProducts->num_rows() > 0) {
                    foreach ($addedSellProducts->result() as $addedSellProductsRow) {
                        array_push($addedSellProductsArr, $addedSellProductsRow->seller_product_id);
                        array_push($addedProductsArr, $addedSellProductsRow->seller_product_id);
                    }
                }
                
                if ($addedUserProducts->num_rows() > 0) {
                    foreach ($addedUserProducts->result() as $addedUserProductsRow) {
                        array_push($addedUserProductsArr, $addedUserProductsRow->seller_product_id);
                        array_push($addedProductsArr, $addedUserProductsRow->seller_product_id);
                    }
                }
                
                if ($orderProductsDetails != '' && $orderProductsDetails->num_rows() > 0) {
                	foreach ($orderProductsDetails->result() as $orderProductsDetailsRow) {
                		array_push($orderProductsDetailsArr, $orderProductsDetailsRow->seller_product_id);
                		array_push($addedProductsArr, $orderProductsDetailsRow->seller_product_id);
                	}
                }
                
                $activityArr = array_merge($likedProductsIdArr, $addedProductsArr);
                array_push($activityArr, $this->checkLogin('U'));
                $allNoty = $this->notify_model->get_latest_notifications($searchArr, $activityArr, $this->checkLogin('U'));
                /* echo $this->db->last_query();
				echo '<pre>';print_r($allNoty->result());die; */
				
				if ($allNoty->num_rows() > 0) {
                	
                    $notyCount = 0;
                    $notyFinal = array();
                    foreach ($allNoty->result() as $allRow) {
                        if ($allRow->activity == 'like') {
                            if (in_array($allRow->activity_id, $addedProductsArr)) {
                                array_push($notyFinal, array('user_id' => $allRow->user_id, 'activity' => 'like', 'activity_id' => $allRow->activity_id, 'created' => $allRow->created));
                                $notyCount++;
                            }
                        } else if ($allRow->activity == 'featured') {
                            if (in_array($allRow->activity_id, $addedProductsArr)) {
                                array_push($notyFinal, array('user_id' => $allRow->user_id, 'activity' => 'featured', 'activity_id' => $allRow->activity_id, 'created' => $allRow->created));
                                $notyCount++;
                            }
                        } else if ($allRow->activity == 'follow') {
                            if ($this->checkLogin('U') == $allRow->activity_id) {
                                array_push($notyFinal, array('user_id' => $allRow->user_id, 'activity' => 'follow', 'activity_id' => $allRow->activity_id, 'created' => $allRow->created));
                                $notyCount++;
                            }
                        } else if ($allRow->activity == 'comment') {
                            if (in_array($allRow->activity_id, $likedProductsIdArr)) {
                                array_push($notyFinal, array('user_id' => $allRow->user_id, 'activity' => 'comment', 'activity_id' => $allRow->activity_id, 'created' => $allRow->created, 'comment_id' => $allRow->comment_id));
                                $notyCount++;
                            }
                        } else if ($allRow->activity == 'own-product-comment') {
                            if (in_array($allRow->activity_id, $addedProductsArr)) {
                                array_push($notyFinal, array('user_id' => $allRow->user_id, 'activity' => 'own-product-comment', 'activity_id' => $allRow->activity_id, 'created' => $allRow->created, 'comment_id' => $allRow->comment_id));
                                $notyCount++;
                            }
                        } else if ($allRow->activity == 'review-comments') {
                            if (in_array($allRow->activity_id, $orderProductsIdArr)) {
                                array_push($notyFinal, array('user_id' => $allRow->user_id, 'activity' => 'review-comments', 'activity_id' => $allRow->activity_id, 'created' => $allRow->created, 'comment_id' => $allRow->comment_id));
                                $notyCount++;
                            }
                        }else if ($allRow->activity == 'Purchased') {
                        	if (in_array($allRow->activity_id, $orderProductsIdArr)) {
							    array_push($notyFinal, array('user_id' => $allRow->user_id, 'activity' => 'Purchased', 'activity_id' => $allRow->activity_id, 'created' => $allRow->created, 'comment_id' => $allRow->comment_id));
                                $notyCount++;
                            }
                        }else if ($allRow->activity == 'New Auction Request') {
                        	if (in_array($allRow->activity_id, $orderProductsIdArr)) {
							    array_push($notyFinal, array('user_id' => $allRow->user_id, 'activity' => 'New Auction Request', 'activity_id' => $allRow->activity_id, 'created' => $allRow->created));
                                $notyCount++;
                            }
                        }else if ($allRow->activity == 'Sent New Offer') {
                        	if (in_array($allRow->activity_id, $orderProductsIdArr)) {
							    array_push($notyFinal, array('user_id' => $allRow->user_id, 'activity' => 'Sent New Offer', 'activity_id' => $allRow->activity_id, 'created' => $allRow->created));
                                $notyCount++;
                            }
                        }else if ($allRow->activity == 'Declined-Offer') {
                        	if (in_array($allRow->activity_id, $orderProductsIdArr)) {
							    array_push($notyFinal, array('user_id' => $allRow->user_id, 'activity' => 'Declined-Offer', 'activity_id' => $allRow->activity_id, 'created' => $allRow->created));
                                $notyCount++;
                            }
                        }else if ($allRow->activity == 'Reject-Offer') {
                        	if (in_array($allRow->activity_id, $orderProductsIdArr)) {
							    array_push($notyFinal, array('user_id' => $allRow->user_id, 'activity' => 'Reject-Offer', 'activity_id' => $allRow->activity_id, 'created' => $allRow->created));
                                $notyCount++;
                            }
                        }else if ($allRow->activity == 'Accept-Offer') {
                        	if (in_array($allRow->activity_id, $orderProductsIdArr)) {
							    array_push($notyFinal, array('user_id' => $allRow->user_id, 'activity' => 'Accept-Offer', 'activity_id' => $allRow->activity_id, 'created' => $allRow->created));
                                $notyCount++;
                            }
                        }
                    }
                    
                   	// echo "<pre>"; print_r($notyFinal); die;
                    
                    if ($notyCount > 0) {
                        $returnStr['content'] = '<ul class="notify-list">';
                        foreach ($notyFinal as $notyFinalRow) {
                            $activityUserDetails = $this->notify_model->get_all_details(USERS, array('id' => $notyFinalRow['user_id']));
                            if ($activityUserDetails->num_rows() > 0) {
                                $userImg = 'user-thumb1.png';
                                if ($activityUserDetails->row()->thumbnail != '') {
                                    if (file_exists('images/users/' . $activityUserDetails->row()->thumbnail)) {
                                        $userImg = $activityUserDetails->row()->thumbnail;
                                    }
                                }
                                $activityUserLink = '<a href="user/' . $activityUserDetails->row()->user_name . '">
			   	 					<img src="images/users/' . $userImg . '" class="avartar" style="float:left;position:static;"/>
									</a>';
                                $activityUserNameLink = '<a href="user/' . $activityUserDetails->row()->user_name . '" class="user">' . $activityUserDetails->row()->full_name . '</a>';
                                $activityTime = strtotime($notyFinalRow['created']);
                                $actTime = timespan($activityTime) . ' ' . $ago . '';
                                if ($notyFinalRow['activity'] != 'follow') {
                                    if (in_array($notyFinalRow['activity_id'], $addedSellProductsArr)) {
                                        $prodTbl = PRODUCT;
                                    } else if (in_array($notyFinalRow['activity_id'], $addedUserProductsArr)) {
                                        $prodTbl = USER_PRODUCTS;
                                    } else {
                                        $prodTbl = '';
                                    }
                                    if ($notyFinalRow['activity'] == 'comment') {
                                        $prodTbl = 'comment';
                                    }
                                    if ($notyFinalRow['activity'] == 'review-comments') {
                                    	$prodTbl = 'order';
                                    }if ($notyFinalRow['activity'] == 'Purchased') {
                                    	$prodTbl = 'order';
                                    }
                                    if ($prodTbl == PRODUCT) {
                                        foreach ($addedSellProducts->result() as $addedSellProductsRow) {
                                            if ($addedSellProductsRow->seller_product_id == $notyFinalRow['activity_id']) {
                                                $imgArr = array_filter(explode(',', $addedSellProductsRow->image));
                                                if (count($imgArr) > 0) {
                                                    if (file_exists('images/product/' . $imgArr[0])) {
                                                        $prodImg = $imgArr[0];
                                                    }
                                                } else {
                                                    $prodImg = 'dummyProductImage.jpg';
                                                }
                                                $activityProdName = $addedSellProductsRow->product_name;
                                                $activityProdLink = '<a href="things/' . $addedSellProductsRow->id . '/' . url_title($addedSellProductsRow->product_name, '-') . '">
													<img src="images/site/blank.gif" style="background-image:url(\'images/product/' . $prodImg . '\');float: right;background-position: 50% 50%;  background-size: cover;" class="u"/>
													</a>';
                                                $activityProdNameLink = '<a href="things/' . $addedSellProductsRow->id . '/' . url_title($addedSellProductsRow->product_name, '-') . '">' . $activityProdName . '</a>.';
                                                break;
                                            }
                                        }
                                    } else if ($prodTbl == USER_PRODUCTS) {
                                        foreach ($addedUserProducts->result() as $addedUserProductsRow) {
                                            if ($addedUserProductsRow->seller_product_id == $notyFinalRow['activity_id']) {
                                                $imgArr = array_filter(explode(',', $addedUserProductsRow->image));
                                                if (count($imgArr) > 0) {
                                                    if (file_exists('images/product/' . $imgArr[0])) {
                                                        $prodImg = $imgArr[0];
                                                    }
                                                } else {
                                                    $prodImg = 'dummyProductImage.jpg';
                                                }
                                                $activityProdName = $addedUserProductsRow->product_name;
                                                $activityProdLink = '<a href="user/' . $this->data['userDetails']->row()->user_name . '/things/' . $addedUserProductsRow->seller_product_id . '/' . url_title($addedUserProductsRow->product_name, '-') . '">
													<img src="images/site/blank.gif" style="background-image:url(\'images/product/' . $prodImg . '\');float: right;background-position: 50% 50%;  background-size: cover;" class="u"/>
													</a>';
                                                $activityProdNameLink = '<a href="user/' . $this->data['userDetails']->row()->user_name . '/things/' . $addedUserProductsRow->seller_product_id . '/' . url_title($addedUserProductsRow->product_name, '-') . '">' . $activityProdName . '</a>.';
                                                break;
                                            }
                                        }
                                    } else if ($prodTbl == 'comment') {
                                        if ($likedProductsDetails != '' && $likedProductsDetails->num_rows() > 0) {
                                            foreach ($likedProductsDetails->result() as $likedProductsDetailsRow) {
                                                if ($likedProductsDetailsRow->seller_product_id == $notyFinalRow['activity_id']) {
                                                    $imgArr = array_filter(explode(',', $likedProductsDetailsRow->image));
                                                    if (count($imgArr) > 0) {
                                                        if (file_exists('images/product/' . $imgArr[0])) {
                                                            $prodImg = $imgArr[0];
                                                        }
                                                    } else {
                                                        $prodImg = 'dummyProductImage.jpg';
                                                    }
                                                    $activityProdName = $likedProductsDetailsRow->product_name;
                                                    $activityProdLink = '<a href="things/' . $likedProductsDetailsRow->id . '/' . url_title($likedProductsDetailsRow->product_name, '-') . '">
														<img src="images/site/blank.gif" style="background-image:url(\'images/product/' . $prodImg . '\');float: right;background-position: 50% 50%;  background-size: cover;" class="u"/>
														</a>';
                                                    $activityProdNameLink = '<a href="things/' . $likedProductsDetailsRow->id . '/' . url_title($likedProductsDetailsRow->product_name, '-') . '">' . $activityProdName . '</a>.';
                                                    break;
                                                }
                                            }
                                        }
                                    }else if ($prodTbl == 'order') {
                                        if ($orderProductsDetails != '' && $orderProductsDetails->num_rows() > 0) {
                                            foreach ($orderProductsDetails->result() as $orderProductsDetailsRow) {
                                                if ($orderProductsDetailsRow->seller_product_id == $notyFinalRow['activity_id']) {
                                                    $imgArr = array_filter(explode(',', $orderProductsDetailsRow->image));
                                                    if (count($imgArr) > 0) {
                                                        if (file_exists('images/product/' . $imgArr[0])) {
                                                            $prodImg = $imgArr[0];
                                                        }
                                                    } else {
                                                        $prodImg = 'dummyProductImage.jpg';
                                                    }
                                                    $activityProdName = $orderProductsDetailsRow->product_name;
                                                    $activityProdLink = '<a href="things/' . $orderProductsDetailsRow->id . '/' . url_title($orderProductsDetailsRow->product_name, '-') . '">
														<img src="images/site/blank.gif" style="background-image:url(\'images/product/' . $prodImg . '\');float: right;background-position: 50% 50%;  background-size: cover;" class="u"/>
														</a>';
                                                    $activityProdNameLink = '<a href="things/' . $orderProductsDetailsRow->id . '/' . url_title($orderProductsDetailsRow->product_name, '-') . '">' . $activityProdName . '</a>.';
                                                    break;
                                                }
                                            }
                                        }
                                    } else {
                                        $activityProdName = '';
                                        $activityProdLink = '';
                                    }
                                    
                                }
                                
                                if ($notyFinalRow['comment_id'] != '0') {
                                	
                                	$cmtDetails = $this->notify_model->get_all_details(PRODUCT_COMMENTS, array('id' => $notyFinalRow['comment_id']));
	                                    $comment = '';
	                                    if ($cmtDetails->num_rows() > 0) {
	                                        $comment = $cmtDetails->row()->comments;
	                                        $activityTime = strtotime($cmtDetails->row()->dateAdded);
	                                        $actTime = timespan($activityTime) . ' ' . $ago . '';
	                                    }

                                }
                                
                                
                                $li_count = 0;
                                $actTime = str_replace($date_txt_arr, $date_lg_arr, $actTime);
                                if ($notyFinalRow['activity'] == 'follow') {
                                    if ($activityUserLink != '') {
                                        $userImg = 'user-thumb1.png';
                                        if ($activityUserDetails->row()->thumbnail != '') {
                                            if (file_exists('images/users/' . $activityUserDetails->row()->thumbnail)) {
                                                $userImg = $activityUserDetails->row()->thumbnail;
                                            }
                                        }
                                        $returnStr['content'] .= '<li class="notification-item" style="width:850px;">' . $activityUserLink . '
			   	 						<p class="right" style="width:790px;"><span class="title"> ' . $activityUserNameLink . ' ' . $follows_you . '</span>
			   	 						<span class="activity-reply">' . $actTime . '</span></p>
			   	 						</li>';
                                        $li_count++;
                                    }
                                } else if ($notyFinalRow['activity'] == 'like') {
                                    if ($activityUserLink != '' && $activityProdLink != '') {
                                        $returnStr['content'] .= '<li class="notification-item" style="width:850px;">' . $activityProdLink . $activityUserLink . '
			   	 						<p class="right" style="width:720px;"><span class="title"> ' . $activityUserNameLink . ' ' . LIKED_BUTTON . ' ' . $activityProdNameLink . '</span>
			   	 						<span class="activity-reply">' . $actTime . '</span></p>
			   	 						</li>';
                                        $li_count++;
                                    }
                                } else if ($notyFinalRow['activity'] == 'featured') {
                                    if ($activityUserLink != '' && $activityProdLink != '') {
                                        $returnStr['content'] .= '<li class="notification-item" style="width:850px;">' . $activityProdLink . $activityUserLink . '
			   	 						<p class="right" style="width:720px;"><span class="title"> ' . $activityUserNameLink . ' ' . $featured . ' ' . $activityProdNameLink . '</span>
			   	 						<span class="activity-reply">' . $actTime . '</span></p>
			   	 						</li>';
                                        $li_count++;
                                    }
                                } else if ($notyFinalRow['activity'] == 'comment') {
                                    if ($activityUserLink != '' && $activityProdLink != '') {
                                        $returnStr['content'] .= '<li class="notification-item" style="width:850px;">' . $activityProdLink . $activityUserLink . ' 
			   	 						<p class="right" style="width:720px;"><span class="title"> ' . $activityUserNameLink . ' ' . $commented_on . ' ' . $activityProdNameLink . '</span>
			   	 						<span class="cmt">' . $comment . '</span>
			   	 						<span class="activity-reply">' . $actTime . '</span></p>
			   	 						</li>';
                                        $li_count++;
									
                                    }
                                } else if ($notyFinalRow['activity'] == 'own-product-comment') {
                                    if ($activityUserLink != '' && $activityProdLink != '') {
                                        $returnStr['content'] .= '<li class="notification-item" style="width:850px;">' . $activityProdLink . $activityUserLink . ' 
			   	 						<p class="right" style="width:720px;"><span class="title"> ' . $activityUserNameLink . ' ' . $commented_on . ' ' . $activityProdNameLink . '</span>
			   	 						<span class="cmt">' . $comment . '</span>
			   	 						<span class="activity-reply">' . $actTime . '</span></p>
			   	 						</li>';
                                        $li_count++;
                                    }
                                } else if ($notyFinalRow['activity'] == 'review-comments') {
                                	$order = $this->notify_model->get_all_details(PAYMENT, array('dealCodeNumber' => $notyFinalRow['comment_id'],'sell_id'=>$this->checkLogin('U')));
                                	if($order->num_rows() > 0){
                                		$discussionLink = '<a href="order-review/'.$order->row()->user_id.'/'.$order->row()->sell_id.'/'.$order->row()->dealCodeNumber.'">#'.$notyFinalRow['comment_id'].'</a>';
                                	}else{
                                		$order = $this->notify_model->get_all_details(PAYMENT, array('dealCodeNumber' => $notyFinalRow['comment_id'],'user_id'=>$this->checkLogin('U')));
                                		$discussionLink = '<a href="order-review/'.$order->row()->user_id.'/'.$order->row()->sell_id.'/'.$order->row()->dealCodeNumber.'">#'.$notyFinalRow['comment_id'].'</a>';
                                	}
                                	
                                    if ($activityUserLink != '' && $activityProdLink != '') {
                                        $returnStr['content'] .= '<li class="notification-item" style="width:850px;">' . $activityProdLink . $activityUserLink . ' 
			   	 						<p class="right" style="width:720px;"><span class="title"> ' . $activityUserNameLink . ' ' . $commented_on . ' discussion ' . $discussionLink . '</span>
			   	 						<span class="cmt">' . $comment . '</span>
			   	 						<span class="activity-reply">' . $actTime . '</span></p>
			   	 						</li>';
                                        $li_count++;
                                    }
                                }else if ($notyFinalRow['activity'] == 'Purchased') {
									$discussionLink = '<a href="order-review/'.$notyFinalRow['user_id'].'/'.$this->checkLogin('U').'/'.$notyFinalRow['comment_id'].'">#'.$notyFinalRow['comment_id'].'</a>';
                                	
                                	if ($activityUserLink != '' && $activityProdLink != '') {
                                        $returnStr['content'] .= '<li class="notification-item" style="width:850px;">' . $activityProdLink . $activityUserLink . ' 
			   	 						<p asdfas class="right" style="width:720px;"><span class="title"> ' . $activityUserNameLink . ' purchased ' . $activityProdNameLink . $discussionLink.'</span>
			   	 						<span class="cmt">' . $comment . '</span>
			   	 						<span class="activity-reply">' . $actTime . '</span></p>
			   	 						</li>';
                                        $li_count++;
                                    }
                                }else if ($notyFinalRow['activity'] == 'New Auction Request') {
									$discussionLink = '<a href="bids" style="color:red;" >Inbox</a>';
                                	
                                	if ($activityUserLink != '' && $activityProdLink != '') {
                                        $returnStr['content'] .= '<li class="notification-item" style="width:850px;">' . $activityProdLink . $activityUserLink . ' 
			   	 						<p asdfas class="right" style="width:720px;"><span class="title"> ' . $activityUserNameLink . ' Sent Auction Request ' . $activityProdNameLink . $discussionLink.'</span>
			   	 						<!--<span class="cmt">' . $comment . '</span>-->
			   	 						<span class="activity-reply">' . $actTime . '</span></p>
			   	 						</li>';
                                        $li_count++;
                                    }
                                }else if ($notyFinalRow['activity'] == 'Sent New Offer') {
									$discussionLink = '<a href="auctions" style="color:red;">View</a>';
									
                                	if ($activityUserLink != '' ) {
                                        $returnStr['content'] .= '<li class="notification-item" style="width:850px;">' . $activityProdLink . $activityUserLink . ' 
			   	 						<p asdfas class="right" style="width:720px;"><span class="title"> ' . $activityUserNameLink . ' Sent New Offer ' . $activityProdNameLink . $discussionLink.'</span>
			   	 						<!--<span class="cmt">' . $comment . '</span>-->
			   	 						<span class="activity-reply">' . $actTime . '</span></p>
			   	 						</li>';
                                        $li_count++;
                                    }
									
                                }else if ($notyFinalRow['activity'] == 'Declined-Offer') {
									$discussionLink = '<a href="bids" style="color:red;">Inbox</a>';
									
                                	if ($activityUserLink != '' ) {
                                        $returnStr['content'] .= '<li class="notification-item" style="width:850px;">' . $activityProdLink . $activityUserLink . ' 
			   	 						<p asdfas class="right" style="width:720px;"><span class="title"> ' . $activityUserNameLink . ' Declined your offer ' . $activityProdNameLink . $discussionLink.'</span>
			   	 						<!--<span class="cmt">' . $comment . '</span>-->
			   	 						<span class="activity-reply">' . $actTime . '</span></p>
			   	 						</li>';
                                        $li_count++;
                                    }
									
                                }else if ($notyFinalRow['activity'] == 'Reject-Offer') {
									$discussionLink = '<a href="bids" style="color:red;">Inbox</a>';
									
                                	if ($activityUserLink != '' ) {
                                        $returnStr['content'] .= '<li class="notification-item" style="width:850px;">' . $activityProdLink . $activityUserLink . ' 
			   	 						<p asdfas class="right" style="width:720px;"><span class="title"> ' . $activityUserNameLink . ' Rejected your offer ' . $activityProdNameLink . $discussionLink.'</span>
			   	 						<!--<span class="cmt">' . $comment . '</span>-->
			   	 						<span class="activity-reply">' . $actTime . '</span></p>
			   	 						</li>';
                                        $li_count++;
                                    }
									
                                }else if ($notyFinalRow['activity'] == 'Accept-Offer') {
									$discussionLink = '<a href="bids" style="color:red;">Inbox</a>';
									
                                	if ($activityUserLink != '' ) {
                                        $returnStr['content'] .= '<li class="notification-item" style="width:850px;">' . $activityProdLink . $activityUserLink . ' 
			   	 						<p asdfas class="right" style="width:720px;"><span class="title"> ' . $activityUserNameLink . ' Accepted your offer ' . $activityProdNameLink . $discussionLink.'</span>
			   	 						<!--<span class="cmt">' . $comment . '</span>-->
			   	 						<span class="activity-reply">' . $actTime . '</span></p>
			   	 						</li>';
                                        $li_count++;
                                    }
									
                                }
                            }
                        }
						
                       /*  if ($li_count == 0) {
                            $returnStr['content'] .= '<li class="notification-item" style="width:850px;">' . $no_notifications_available . '</li>';
                            $returnStr['status_code'] = 2;
                        } else { */
                            $returnStr['status_code'] = 1;
                        //} 
                        $returnStr['content'] .= '</ul>';
                    } else {
                        $returnStr['status_code'] = 2;
                        $returnStr['content'] = '<ul class="notify-list"><li class="notification-item" style="width:850px;">' . $no_notifications_available . '</li></ul>';
                    }
                } else {
                    $returnStr['status_code'] = 2;
                    $returnStr['content'] = '<ul class="notify-list"><li class="notification-item" style="width:850px;">' . $no_notifications_available . '</li></ul>';
                }
            } else {
                $returnStr['status_code'] = 2;
                $returnStr['content'] = '<ul class="notify-list"><li class="notification-item" style="width:850px;">' . $no_notifications_available . '</li></ul>';
            }
        }
        $this->data['notyList'] = $returnStr['content'];
        $this->data['heading'] = 'Notifications';
        $this->load->view('site/notification/display_notification', $this->data);
    }
	public function new_noty_request() {
		header ( 'Content-Type: text/event-stream' );
		header ( 'Cache-Control: no-cache' );
		$user_id = $this->checkLogin ('U');
		
		$this->load->model ( 'user_model' );
		$notifications = $this->user_model->get_new_notifications($user_id);
		if ($notifications == 0) {
		} else {
			$notifications_count = $notifications->num_rows ();
			if ($notifications_count > 0) {
				echo "data: " . $notifications_count . "\n\n";
			}
		}
		flush ();
	}

}

/*End of file notify.php */
/* Location: ./application/controllers/site/notify.php */