<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * User related functions
 * @author Teamtweaks
 *
 */
class Store extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('cookie', 'date', 'form', 'email', 'text'));
        $this->load->library(array('encrypt', 'form_validation'));
        $this->load->model(array('store_model', 'user_model', 'category_model', 'product_model'));
// 		if($_SESSION['sMainCategories'] == ''){
        $sortArr1 = array('field' => 'cat_position', 'type' => 'asc');
        $sortArr = array($sortArr1);
        $this->data['mainCategories'] = $this->store_model->get_all_details(CATEGORY, array('rootID' => '0', 'status' => 'Active'), $sortArr);
// 			$_SESSION['sMainCategories'] = $this->store_model->get_all_details(CATEGORY,array('rootID'=>'0','status'=>'Active'),$sortArr);
// 		}
// 		$this->data['mainCategories'] = $_SESSION['sMainCategories'];
// 		if($_SESSION['sColorLists'] == ''){
// 			$_SESSION['sColorLists'] = $this->store_model->get_all_details(LIST_VALUES,array('list_id'=>'1'));
// 		}
// 		$this->data['mainColorLists'] = $_SESSION['sColorLists'];
        $this->data['mainColorLists'] = $this->store_model->get_all_details(LIST_VALUES, array('list_id' => '1'));
        $this->data['loginCheck'] = $this->checkLogin('U');

        $this->data['likedProducts'] = array();
        if ($this->data['loginCheck'] != '') {
            $this->data['likedProducts'] = $this->store_model->get_all_details(PRODUCT_LIKES, array('user_id' => $this->checkLogin('U')));
        }
    }

    public function open_store() {

        if ($this->checkLogin('U') == '') {
            redirect('login');
        } else {
            $userType = $this->data['userDetails']->row()->group;
            if ($userType == 'Seller') {
                if($this->data['userDetails']->row()->store_payment != 'Paid') {
                    if($this->session->userdata('InvoiceNo') != '') {
                        $InvoiceNo = $this->session->userdata('InvoiceNo');
                    } else {
                        $InvoiceNo = mt_rand();
                        $checkId = $this->store_model->get_all_details(STORE_ARB, array('invoice_no' => $InvoiceNo));
                        while ($checkId->num_rows() > 0) {
                            $InvoiceNo = mt_rand();
                            $checkId = $this->store_model->get_all_details(STORE_ARB, array('invoice_no' => $InvoiceNo));
                        }
                    }
                    $this->session->set_userdata('InvoiceNo', $InvoiceNo);
                    $this->data['editmode'] = '0';
                    $this->data['countryList'] = $this->store_model->get_all_details(COUNTRY_LIST, array());
                    $this->data['userDetails'] = $this->store_model->get_all_details(USERS, array('id' => $this->checkLogin('U')));
                    $this->load->view('site/store/open_store', $this->data);
                } else {
                    $this->setErrorMessage('danger', 'Already subscribed');
                    redirect(base_url() . 'user/seller-timeline/' . $this->data['userDetails']->row()->user_name);
                }
            } else {
                redirect('seller-signup');
            }
        }
    }

    public function view_store() {
        if ($this->uri->segment(3) == 'liked') {
            show_404();
        }
        $condition = 'select s.id as sid,s.store_name,s.description,s.cover_image,s.logo_image,s.privacy,s.tagline,
			u.* from ' . STORE_FRONT . ' as s left join ' . USERS . ' as u on u.id = s.user_id where u.user_name= "' . $this->uri->segment(2, 0) . '"';
        $userDetails = $this->store_model->ExecuteQuery($condition);
        // print_r($userDetails->result());die;
        if ($userDetails->num_rows() == 1) {
            $this->data['user_Details'] = $userDetails;
        } else {
            $userDetails = $this->store_model->get_all_details(USERS, array('user_name' => $this->uri->segment(2, 0)));
            $this->data['user_Details'] = $userDetails;
        }
        if ($this->input->get('sort_by_price')) {
            if ($this->input->get('sort_by_price') == 'desc') {
                $orderBy = ' order by p.sale_price desc';
            } else {
                $orderBy = ' order by p.sale_price asc';
            }
        } else {
            $orderBy = ' order by p.created desc ';
        }
        $this->data['product_count'] = $productDetails = $this->store_model->get_all_details(PRODUCT, array('user_id' => $userDetails->row()->id, 'status' => "Publish"));
        //echo "<pre>";print_r($this->data['product_count']->result());die;
        $cat_ids = array();
        $immediate_shipping = false;
        $list_ids = array();
        if ($productDetails->num_rows() > 0) {
            foreach ($productDetails->result() as $product_row) {
                if ($product_row->ship_immediate == 'true')
                    $immediate_shipping = true;
                $product_cat = $product_row->category_id;
                if ($product_cat != '') {
                    $product_cat_arr = array_filter(explode(',', $product_cat));
                    $cat_ids = array_unique(array_merge($cat_ids, $product_cat_arr));
                }
                $product_lists = $product_row->list_value;
                if ($product_lists != '') {
                    $product_lists_arr = array_filter(explode(',', $product_lists));
                    $list_ids = array_unique(array_merge($list_ids, $product_lists_arr));
                }
            }
        }
        $this->data['store_category'] = $cat_ids;
        $this->data['immediate_shipping'] = $immediate_shipping;
        $this->data['list_ids'] = $list_ids;
        $this->data['liked_count'] = $this->store_model->get_all_details(PRODUCT_LIKES, array('user_id' => $userDetails->row()->id));

        $product_feedback = $this->product_model->product_feedback_view($productDetails->row()->user_id);
        $this->data['all_feedback'] = $product_feedback;
        if ($this->input->get('is')) {
            $whereCond .= ' and p.ship_immediate = "' . $this->input->get('is') . '"';
        }
        if ($this->input->get('p')) {
            $price = explode('-', $this->input->get('p'));
            $whereCond .= ' and p.sale_price >= "' . $price[0] . '" and p.sale_price <= "' . $price[1] . '"';
        }
        if ($this->input->get('q')) {
            $whereCond .= ' and p.product_name LIKE "%' . $this->input->get('q') . '%"';
        }
        if ($this->input->get('c')) {
            $condition = " where list_value_seourl = '" . $this->input->get('c') . "'";
            $listID = $this->category_model->getAttrubteValues($condition);
            $whereCond .= ' and FIND_IN_SET("' . $listID->row()->id . '",p.list_value)';
        }
        if ($this->uri->segment(3, 0) == 'products' && $this->uri->segment(4, 0) == '' || $this->uri->segment(3, 0) == '') {
            $whereCond = ' where u.id=' . $userDetails->row()->id . '' . $whereCond . ' and p.status="Publish"';
            $searchProd = $whereCond . ' ' . $orderBy . ' ';
            $productList = $this->store_model->searchShopyByCategory($searchProd);
            $this->data['productDetails'] = $productList;
            $this->load->view('site/store/view_store', $this->data);
        } elseif ($this->uri->segment(3, 0) == 'liked') {
            $this->data['productLikeDetails'] = $this->user_model->get_like_details_fully($userDetails->row()->id);
            $this->data['userProductLikeDetails'] = $this->user_model->get_like_details_fully_user_products($userDetails->row()->id);
            $this->load->view('site/store/store_likes', $this->data);
        } elseif ($this->uri->segment(4, 0) != '') {

            $cate_id = $this->store_model->get_all_details(CATEGORY, array('cat_name' => urldecode($this->uri->segment(4, 0))));
            $whereCond = ' where FIND_IN_SET(' . $cate_id->row()->id . ',p.category_id) and user_id = ' . $userDetails->row()->id . '' . $whereCond . ' and p.status="Publish"';
            $searchProd = $whereCond . ' ' . $orderBy . ' ';
            $productList = $this->store_model->searchShopyByCategory($searchProd);
            $this->data['productDetails'] = $productList;
            $this->load->view('site/store/view_store', $this->data);
        } elseif ($this->uri->segment(3, 0) == 'reviews') {
            $this->data['product_feedback'] = $product_feedback;
            $this->load->view('site/store/store_reviews', $this->data);
        }
    }

    public function settings() {
        if ($this->checkLogin('U') == '') {
            redirect('login');
        } else {
            $this->data['storeDetails'] = $this->store_model->get_all_details(STORE_FRONT, array('user_id' => $this->checkLogin('U')));
            $this->load->view('site/store/settings', $this->data);
        }
    }
	public function shop_settings() {
        if ($this->checkLogin('U') == '') {
            redirect('login');
        } else {
            $this->data['storeDetails'] = $this->store_model->get_all_details(STORE_FRONT, array('user_id' => $this->checkLogin('U')));
            $this->load->view('site/store/shop_settings', $this->data);
        }
    }
    public function profile_settings() {
        if ($this->checkLogin('U') == '') {
            redirect('login');
        } else {
            $this->data['storeDetails'] = $this->store_model->get_all_details(STORE_FRONT, array('user_id' => $this->checkLogin('U')));
            $this->load->view('site/store/settings', $this->data);
        }
    }

    public function InsertEditstore() {
        if ($this->checkLogin('U') == '') {
            redirect('login');
        } else {
            if ($_FILES['logo-file']['name'] != '') {
                $config['overwrite'] = FALSE;
                $config['allowed_types'] = 'jpg|jpeg|gif|png';
                $config['max_size'] = 2000;
                $config['max_width'] = '600';
                $config['max_height'] = '600';
                $config['upload_path'] = './images/store';
                $this->load->library('upload', $config);
                if ($this->upload->do_upload('logo-file')) {
                    $imgDetails = $this->upload->data();
                    $logo_image = $imgDetails['file_name'];
                } else {
                    $this->setErrorMessage('error', strip_tags($this->upload->display_errors() . " In Logo-image"));
                    redirect('site/store/settings');
                }
            }
            if ($_FILES['banner-file']['name'] != '') {
                $config['overwrite'] = FALSE;
                $config['allowed_types'] = 'jpg|jpeg|gif|png';
                $config['max_size'] = 2000;
                $config['max_width'] = '1300';
                $config['max_height'] = '500';
                $config['upload_path'] = './images/store';
                $this->load->library('upload', $config);
                if ($this->upload->do_upload('banner-file')) {
                    $imgDetails = $this->upload->data();
                    $banner_image = $imgDetails['file_name'];
                } else {
                    $this->setErrorMessage('error', strip_tags($this->upload->display_errors() . " In Banner-image"));
                    redirect('site/store/settings');
                }
            }
            $excludeArr = array('logo-file', 'banner-file', 'store_id', 'store_name');

            if ($logo_image == '' && $banner_image == '') {
                $dataArr = array();
            } elseif ($logo_image == '') {
                $dataArr = array('cover_image' => $banner_image);
            } elseif ($banner_image == '') {
                $dataArr = array('logo_image' => $logo_image);
            } else {
                $dataArr = array('logo_image' => $logo_image, 'cover_image' => $banner_image);
            }
            if ($this->input->post('store_id') == '') {
                $condition = array();
                $this->store_model->commonInsertUpdate(STORE_FRONT, 'insert', $excludeArr, $dataArr, $condition);
            } else {
                $condition = array('id' => $this->input->post('store_id'));
                $this->store_model->commonInsertUpdate(STORE_FRONT, 'update', $excludeArr, $dataArr, $condition);
            }
            redirect('site/store/settings');
        }
    }

    public function delete_store_image() {
        $response['success'] = '0';
        if ($this->checkLogin('U') == '') {
            $response['msg'] = 'You must login';
        } else {
            $condition = array('user_id' => $this->checkLogin('U'));
            if ($this->input->post('id') == 'delete_banner_image') {
                $dataArr = array('cover_image' => '');
            } else {
                $dataArr = array('logo_image' => '');
            }
            $this->store_model->update_details(STORE_FRONT, $dataArr, $condition);
            if ($this->lang->line('prof_photo_del') != '')
                $lg_err_msg = $this->lang->line('prof_photo_del');
            else
            if ($this->input->post('id') == 'delete_banner_image') {
                $lg_err_msg = 'Store banner deleted successfully';
            } else {
                $lg_err_msg = 'store logo deleted successfully';
            }
            $this->setErrorMessage('success', $lg_err_msg);
            $response['success'] = '1';
        }
        echo json_encode($response);
    }

    public function product_settings() {
        if ($this->checkLogin('U') == '') {
            redirect('login');
        } else {
            $this->data['productDetails'] = $this->store_model->get_all_details(PRODUCT, array('user_id' => $this->checkLogin('U')));
            $this->load->view('site/store/settings_products', $this->data);
        }
    }

    public function order_settings() {
        if ($this->checkLogin('U') == '') {
            redirect('login');
        } else {
            $this->data['heading'] = 'Orders';
            $this->data['ordersList'] = $this->user_model->get_user_orders_list($this->checkLogin('U'));
            $this->load->view('site/store/settings_orders', $this->data);
        }
    }

    public function privacy_settings() {
        if ($this->checkLogin('U') == '') {
            redirect('login');
        } else {
            $this->data['storeDetails'] = $this->store_model->get_all_details(STORE_FRONT, array('user_id' => $this->checkLogin('U')));
            $this->load->view('site/store/settings_privacy', $this->data);
        }
    }

    public function privacyUpdate() {
        if ($this->checkLogin('U') == '') {
            redirect('login');
        } else {
            $excludeArr = array('store_id');
            $dataArr = array('privacy' => $this->input->post('privacy'), 'user_id' => $this->input->post('user_id'));
            if ($this->input->post('store_id') == '') {
                $condition = array();
                $this->store_model->commonInsertUpdate(STORE_FRONT, 'insert', $excludeArr, $dataArr, $condition);
            } else {
                $condition = array('id' => $this->input->post('store_id'));
                $this->store_model->commonInsertUpdate(STORE_FRONT, 'update', $excludeArr, $dataArr, $condition);
            }
            $this->data['storeDetails'] = $this->store_model->get_all_details(STORE_FRONT, array('id' => $this->input->post('store_id')));
            redirect('site/store/privacy_settings');
        }
    }

    public function about_settings() {
        if ($this->checkLogin('U') == '') {
            redirect('login');
        } else {
            $this->data['storeDetails'] = $this->store_model->get_all_details(STORE_FRONT, array('user_id' => $this->checkLogin('U')));
            $this->load->view('site/store/settings_about', $this->data);
        }
    }

    public function aboutUpdate() {
        if ($this->checkLogin('U') == '') {
            redirect('login');
        } else {
            $excludeArr = array('store_id');
            $dataArr = array('description' => $this->input->post('description'), 'user_id' => $this->input->post('user_id'));
            if ($this->input->post('store_id') == '') {
                $condition = array();
                $this->store_model->commonInsertUpdate(STORE_FRONT, 'insert', $excludeArr, $dataArr, $condition);
            } else {
                $condition = array('id' => $this->input->post('store_id'));
                $this->store_model->commonInsertUpdate(STORE_FRONT, 'update', $excludeArr, $dataArr, $condition);
            }
            //echo $this->db->last_query(); die;
            $this->data['storeDetails'] = $this->store_model->get_all_details(STORE_FRONT, array('id' => $this->input->post('store_id')));
            redirect('site/store/about_settings');
        }
    }

    public function contact_mail() {
        $newsid = '31';
        $template_values = $this->user_model->get_newsletter_template_details($newsid);
        //print_r($this->input->post());

        $email = $this->input->post('to');
        $email_from = $this->input->post('from');
        $email_subject = $this->input->post('subject');
        $email_message = $this->input->post('message');
        $reciever = $this->input->post('reciever');
        $sender = $this->input->post('sender');

        //$cfmurl = base_url().'site/user/confirm_register/'.$uid."/".$randStr."/confirmation";
        $subject = $template_values['news_subject'];
        $adminnewstemplateArr = array('email_title' => $this->config->item('email_title'), 'logo' => $this->data['logo']);
        extract($adminnewstemplateArr);
        //$ddd =htmlentities($template_values['news_descrip'],null,'UTF-8');


        $header .="Content-Type: text/plain; charset=ISO-8859-1\r\n";
        $message .= '<!DOCTYPE HTML>
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
			<meta name="viewport" content="width=device-width"/><body>';
        include('./newsletter/registeration' . $newsid . '.php');
        $message .= '</body>
			</html>';

        if ($template_values['sender_name'] == '' && $template_values['sender_email'] == '') {
            $sender_email = $this->data['siteContactMail'];
            $sender_name = $this->data['siteTitle'];
        } else {
            $sender_name = $template_values['sender_name'];
            $sender_email = $template_values['sender_email'];
        }

        $email_values = array('mail_type' => 'html',
            'from_mail_id' => $sender_email,
            'mail_name' => $sender_name,
            'to_mail_id' => $email,
            'subject_message' => $template_values['news_subject'],
            'body_messages' => $message,
            'mail_id' => 'register mail'
        );

        //print_r(stripslashes($message)); die;
        $email_send_to_common = $this->product_model->common_email_send($email_values);
        if ($email_send_to_common) {
            echo "send";
        }
    }

    public function paypalsuccess() {
        if ($this->checkLogin('U') != '') {
            $cde = $this->session->userdata('cde');
            $cde_user = $this->session->userdata('cde_user');
            $invoice = $this->session->userdata('InvoiceNo');
            $key = 'team-fancyy-clone-tweaks';
            if (($this->encrypt->decode($cde, $key) == $invoice) && ($this->encrypt->decode($cde_user, $key) == $this->checkLogin('U'))) {

                $this->session->unset_userdata('cde');
                $this->session->unset_userdata('cde_user');
                $this->session->unset_userdata('InvoiceNo');
                $status_chk = $this->store_model->get_all_details(STORE_ARB, array('invoice_no' => $invoice, 'status' => 'Paid'));

                if ($status_chk->num_rows() == 0) {
                    $pay_details = $this->store_model->get_all_details(STORE_ARB, array('invoice_no' => $invoice, 'status' => 'Pending'));
                    if ($pay_details->num_rows() > 0) {
                        $couponID = $pay_details->row()->couponid;
                        $couponAmont = $pay_details->row()->discount;
                        $couponType = $pay_details->row()->coupontype;

                        // Update Coupon
                        if ($couponID != 0) {
                            if ($couponType == 'Gift') {
                                $SelGift = $this->store_model->get_all_details(GIFTCARDS, array('id' => $couponID));
                                $GiftCountValue = $SelGift->row()->used_amount + $couponAmont;
                                $condition = array('id' => $couponID);
                                $dataArr = array('used_amount' => $GiftCountValue);
                                $this->store_model->update_details(GIFTCARDS, $dataArr, $condition);
                                if ($SelGift->row()->price_value <= $GiftCountValue) {

                                    $condition1 = array('id' => $couponID);
                                    $dataArr1 = array('card_status' => 'redeemed');
                                    $this->store_model->update_details(GIFTCARDS, $dataArr1, $condition1);
                                }
                            } else {
                                $SelCoup = $this->store_model->get_all_details(COUPONCARDS, array('id' => $couponID));
                                $CountValue = $SelCoup->row()->purchase_count + 1;
                                $condition = array('id' => $couponID);
                                $dataArr = array('purchase_count' => $CountValue);
                                $this->store_model->update_details(COUPONCARDS, $dataArr, $condition);
                            }
                        }

                        // Update Paid Status
                        $this->store_model->update_details(STORE_ARB, array('status' => 'Paid'), array('invoice_no' => $invoice));
                        $this->store_model->simple_insert(STORE_FRONT, array('user_id' => $this->checkLogin('U'), 'store_name' => $this->data['userDetails']->row()->brand_name));
                        $dataArr = array(
                            'store_payment' => 'Paid'
//							'ref_comm_purchaser' => $this->config->item('ref_comm_purchaser_ug'),
//							'ref_comm_seller' => $this->config->item('ref_comm_seller_ug'),
//							'sell_commission' => $this->config->item('sell_commission_ug')
                        );
                        $condition = array('id' => $this->checkLogin('U'));
                        $this->store_model->update_details(USERS, $dataArr, $condition);

                        //Send Mail
                        $this->send_subscription_success_mail($invoice);

                        $this->data['Confirmation'] = 'subscription_success';
                        $this->data['errors'] = 'Payment Success';
                        $this->load->view('site/order/order.php', $this->data);
                    }
                }
            } else {

                if ($invoice != '') {
                    $this->store_model->commonDelete(STORE_ARB, array('invoice_no' => $invoice));
                }
                $this->session->unset_userdata('cde');
                $this->session->unset_userdata('cde_user');
                $this->session->unset_userdata('InvoiceNo');
                $this->setErrorMessage('error', 'Invalid details');
                redirect(base_url());
            }
        } else {
            redirect('login');
        }
    }

    public function paypalfailure() {
        if ($this->checkLogin('U') != '') {
            $cde = $this->session->userdata('cde');
            $cde_user = $this->session->userdata('cde_user');
            $invoice = $this->session->userdata('InvoiceNo');
            $key = 'team-fancyy-clone-tweaks';
            if ($invoice != '') {
                $this->store_model->commonDelete(STORE_ARB, array('invoice_no' => $invoice));
            }
            $this->session->unset_userdata('cde');
            $this->session->unset_userdata('cde_user');
            if (($this->encrypt->decode($cde, $key) == $invoice) && ($this->encrypt->decode($cde_user, $key) == $this->checkLogin('U'))) {
                $this->data['Confirmation'] = 'subscription_fail';
                $this->data['errors'] = 'Payment Failed';
                $this->load->view('site/order/order.php', $this->data);
            } else {
                $this->session->unset_userdata('InvoiceNo');
                $this->setErrorMessage('error', 'Invalid details');
                redirect(base_url());
            }
        } else {
            redirect('login');
        }
    }

    public function paypalnotify() {
        if ($this->checkLogin('U') != '') {
            $cde = $this->session->userdata('cde');
            $cde_user = $this->session->userdata('cde_user');
            $invoice = $this->session->userdata('InvoiceNo');
            $key = 'team-fancyy-clone-tweaks';
            if ($invoice != '') {
                $this->store_model->commonDelete(STORE_ARB, array('invoice_no' => $invoice));
            }
            $this->session->unset_userdata('cde');
            $this->session->unset_userdata('cde_user');
            if (($this->encrypt->decode($cde, $key) == $invoice) && ($this->encrypt->decode($cde_user, $key) == $this->checkLogin('U'))) {
                $this->data['Confirmation'] = 'subscription_fail';
                $this->data['errors'] = 'Payment Failed';
                $this->load->view('site/order/order.php', $this->data);
            } else {
                $this->session->unset_userdata('InvoiceNo');
                $this->setErrorMessage('error', 'Invalid details');
                redirect(base_url());
            }
        } else {
            redirect('login');
        }
    }

    public function send_subscription_success_mail($invoice_no = '') {
        $newsid = '30';
        $template_values = $this->store_model->get_newsletter_template_details($newsid);
        $email = $this->data['userDetails']->row()->email;
        $email_message = "Payment Success. Please update your store setting";
        $subject = $template_values['news_subject'];
        $adminnewstemplateArr = array('email_title' => $this->config->item('email_title'), 'logo' => $this->data['logo']);
        extract($adminnewstemplateArr);
        $header .="Content-Type: text/plain; charset=ISO-8859-1\r\n";
        $message .= '<!DOCTYPE HTML>
				<html>
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
				<meta name="viewport" content="width=device-width"/><body>';
        include('./newsletter/registeration' . $newsid . '.php');
        $message .= '</body>
				</html>';
        if ($template_values['sender_name'] == '' && $template_values['sender_email'] == '') {
            $sender_email = $this->data['siteContactMail'];
            $sender_name = $this->data['siteTitle'];
        } else {
            $sender_name = $template_values['sender_name'];
            $sender_email = $template_values['sender_email'];
        }
        $email_values = array('mail_type' => 'html',
            'from_mail_id' => $sender_email,
            'mail_name' => $sender_name,
            'to_mail_id' => $email,
            'cc_mail_id' => $this->config->item('site_contact_mail'),
            'subject_message' => $template_values['news_subject'],
            'body_messages' => $message,
            'mail_id' => 'register mail'
        );
        $email_send_to_common = $this->store_model->common_email_send($email_values);
    }

    /**
     * Cron function for update the order received status
     *
     */
    public function cron_change_order_status() {
        $Query = 'select p.*,s.status as seller_status
				from ' . PAYMENT . ' as p
				LEFT JOIN ' . USERS . ' as u on p.user_id=u.id
				LEFT JOIN ' . USERS . ' as s on p.sell_id=s.id
				where p.status="Paid"
				and NOW() > DATE_ADD(`p`.`created`, INTERVAL `s`.`return_window`+5 DAY)
				AND `p`.`shipping_status`!="Cancelled" AND `p`.`received_status`="Not received yet"';
        $order_details = $this->product_model->ExecuteQuery($Query);
        //		echo $this->db->last_query()."<pre>";print_r($order_details->result());
        $count = 0;
        if ($order_details->num_rows() > 0) {
            foreach ($order_details->result() as $row) {
                if ($row->seller_status == 'Active') {
                    $this->product_model->update_details(PAYMENT, array('received_status' => 'Product received'), array('id' => $row->id));
                    $count++;
                }
            }
        }
        echo $count . ' records updated';
    }

}
