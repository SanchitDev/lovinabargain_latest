<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * Notifications related functions
 * @author Teamtweaks
 *
 */
class IosNotify extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('cookie', 'date', 'email'));
        $this->load->model('notify_model');
        if ($_SESSION['sMainCategories'] == '') {
            $sortArr1 = array('field' => 'cat_position', 'type' => 'asc');
            $sortArr = array($sortArr1);
            $_SESSION['sMainCategories'] = $this->notify_model->get_all_details(CATEGORY, array('rootID' => '0', 'status' => 'Active'), $sortArr);
        }
        $this->data['mainCategories'] = $_SESSION['sMainCategories'];
        if ($_SESSION['sColorLists'] == '') {
            $_SESSION['sColorLists'] = $this->notify_model->get_all_details(LIST_VALUES, array('list_id' => '1'));
        }
        $this->data['mainColorLists'] = $_SESSION['sColorLists'];
    }


    public function index() {


        if ($this->lang->line('login_required') != '')
            $login_required = stripslashes($this->lang->line('login_required'));
        else
            $login_required = "Login required";

        if ($this->lang->line('follows_you') != '')
            $follows_you = stripslashes($this->lang->line('follows_you'));
        else
            $follows_you = "follows you";

        if ($this->lang->line('featured') != '')
            $featured = stripslashes($this->lang->line('featured'));
        else
            $featured = "featured";

        if ($this->lang->line('commented_on') != '')
            $commented_on = stripslashes($this->lang->line('commented_on'));
        else
            $commented_on = "commented on";

        if ($this->lang->line('no_notifications') != '')
            $no_notifications = stripslashes($this->lang->line('no_notifications'));
        else
            $no_notifications = "No Notifications";

        if ($this->lang->line('no_notifications_available') != '')
            $no_notifications_available = stripslashes($this->lang->line('no_notifications_available'));
        else
            $no_notifications_available = "No notifications available";

        if ($this->lang->line('ago') != '')
            $ago = stripslashes($this->lang->line('ago'));
        else
            $ago = "ago";

        if ($this->lang->line('date_year') != '')
            $date_year = $this->lang->line('date_year');
        else
            $date_year = "Year";

        if ($this->lang->line('date_years') != '')
            $date_years = $this->lang->line('date_years');
        else
            $date_years = "Years";

        if ($this->lang->line('date_month') != '')
            $date_month = $this->lang->line('date_month');
        else
            $date_month = "Month";

        if ($this->lang->line('date_months') != '')
            $date_months = $this->lang->line('date_months');
        else
            $date_months = "Months";

        if ($this->lang->line('date_week') != '')
            $date_week = $this->lang->line('date_week');
        else
            $date_week = "Week";

        if ($this->lang->line('date_weeks') != '')
            $date_weeks = $this->lang->line('date_weeks');
        else
            $date_weeks = "Weeks";

        if ($this->lang->line('date_day') != '')
            $date_day = $this->lang->line('date_day');
        else
            $date_day = "Day";

        if ($this->lang->line('date_days') != '')
            $date_days = $this->lang->line('date_days');
        else
            $date_days = "Days";

        if ($this->lang->line('date_hour') != '')
            $date_hour = $this->lang->line('date_hour');
        else
            $date_hour = "Hour";

        if ($this->lang->line('date_hours') != '')
            $date_hours = $this->lang->line('date_hours');
        else
            $date_hours = "Hours";

        if ($this->lang->line('date_minute') != '')
            $date_minute = $this->lang->line('date_minute');
        else
            $date_minute = "Minute";

        if ($this->lang->line('date_minutes') != '')
            $date_minutes = $this->lang->line('date_minutes');
        else
            $date_minutes = "Minutes";

        if ($this->lang->line('date_second') != '')
            $date_second = $this->lang->line('date_second');
        else
            $date_second = "Second";

        if ($this->lang->line('date_seconds') != '')
            $date_seconds = $this->lang->line('date_seconds');
        else
            $date_seconds = "Seconds";

        if ($this->lang->line('ago') != '')
            $date_ago = $this->lang->line('ago');
        else
            $date_ago = "ago";

        //Language End
        $date_txt_arr = array(
            "Years",
            "Year",
            "Months",
            "Month",
            "Weeks",
            "Week",
            "Days",
            "Day",
            "Hours",
            "Hour",
            "Minutes",
            "Minute",
            "Seconds",
            "Second",
            "ago"
        );
        $date_lg_arr = array(
            $date_years,
            $date_year,
            $date_months,
            $date_month,
            $date_weeks,
            $date_week,
            $date_days,
            $date_day,
            $date_hours,
            $date_hour,
            $date_minutes,
            $date_minute,
            $date_seconds,
            $date_second,
            $date_ago
        );
        $responseArr['status'] = (string)0;
		$responseArr['response'] = '';
        $notification = array();
        $UserId = $_POST['UserId'];
        if ($UserId == '') {
            $responseArr['response'] = 'UserId Null !!';
        } else {

            $this->data['userDetails'] = $this->notify_model->get_all_details(USERS, array('id' => $UserId));
            $notifications = array_filter(explode(',', $this->data['userDetails']->row()->notifications));
            $this->data['likedProducts'] = array();
            $this->data['likedProducts'] = $this->notify_model->get_all_details(PRODUCT_LIKES, array('user_id' =>$UserId));
            $this->data['orderpurchaseProducts'] = $this->notify_model->get_orderpurchase_details($UserId);

		    $searchArr = array();
            if (count($notifications) > 0) {
                if (in_array('wmn-follow', $notifications)) {
                    array_push($searchArr, 'follow');
                }
                if (in_array('wmn-comments_on_fancyd', $notifications)) {
                    array_push($searchArr, 'comment');
                }
                if (in_array('wmn-fancyd', $notifications)) {
                    array_push($searchArr, 'like');
                }
                if (in_array('wmn-featured', $notifications)) {
                    array_push($searchArr, 'featured');
                }
                if (in_array('wmn-comments', $notifications)) {
                    array_push($searchArr, 'own-product-comment');
                }
                if (in_array('wmn-review-comments', $notifications)) {
                	array_push($searchArr, 'review-comments');
                }if (in_array('wmn-purchased', $notifications)) {
                    array_push($searchArr, 'Purchased');
                }
                $likedProductsIdArr = array();

                if ($this->data['likedProducts']->num_rows() > 0) {
                    foreach ($this->data['likedProducts']->result() as $likeProdRow) {
                        array_push($likedProductsIdArr, $likeProdRow->product_id);
                    }
                    array_filter($likedProductsIdArr);
                }

                $orderProductsIdArr = array();
                if ($this->data['orderpurchaseProducts']->num_rows() > 0) {
                	foreach ($this->data['orderpurchaseProducts']->result() as $orderProdRow) {
                		array_push($orderProductsIdArr, $orderProdRow->product_id);
                	}
                	array_filter($orderProductsIdArr);
                }

                if (count($orderProductsIdArr) > 0) {
                	$fields = " p.product_name,p.id,p.seller_product_id,p.image,u.full_name,u.user_name,u.thumbnail,u.feature_product ";
                	$condition = ' where p.status="Publish" and u.status="Active" and p.id in (' . implode(',', $orderProductsIdArr) . ')
			 						or p.status="Publish" and p.user_id=0 and p.id in (' . implode(',', $orderProductsIdArr) . ') ';
                	$orderProductsDetails = $this->notify_model->get_active_sell_products($condition, $fields);
                } else {
                	$orderProductsDetails = '';
                }

                $orderProductsIdArr = array();
                if ($orderProductsDetails!='' && $orderProductsDetails->num_rows() > 0) {
	                foreach ($orderProductsDetails->result() as $orderProductsDetailsRow) {
	                	array_push($orderProductsIdArr, $orderProductsDetailsRow->seller_product_id);
	                }
	                array_filter($orderProductsIdArr);
                }

                if (count($likedProductsIdArr) > 0) {
                    $fields = " p.product_name,p.id,p.seller_product_id,p.image,u.full_name,u.user_name,u.thumbnail,u.feature_product ";
                    $condition = ' where p.status="Publish" and u.status="Active" and p.seller_product_id in (' . implode(',', $likedProductsIdArr) . ')
			 						or p.status="Publish" and p.user_id=0 and p.seller_product_id in (' . implode(',', $likedProductsIdArr) . ') ';
                    $likedProductsDetails = $this->notify_model->get_active_sell_products($condition, $fields);
                } else {
                    $likedProductsDetails = '';
                }
                $addedSellProducts = $this->notify_model->get_all_details(PRODUCT, array('user_id' => $UserId, 'status' => 'Publish'));
                $addedUserProducts = $this->notify_model->get_all_details(USER_PRODUCTS, array('user_id' =>$UserId, 'status' => 'Publish'));
                $addedSellProductsArr = array();
                $addedUserProductsArr = array();
                $addedProductsArr = array();

                if ($addedSellProducts->num_rows() > 0) {
                    foreach ($addedSellProducts->result() as $addedSellProductsRow) {
                        array_push($addedSellProductsArr, $addedSellProductsRow->seller_product_id);
                        array_push($addedProductsArr, $addedSellProductsRow->seller_product_id);
                    }
                }

                if ($addedUserProducts->num_rows() > 0) {
                    foreach ($addedUserProducts->result() as $addedUserProductsRow) {
                        array_push($addedUserProductsArr, $addedUserProductsRow->seller_product_id);
                        array_push($addedProductsArr, $addedUserProductsRow->seller_product_id);
                    }
                }

                if ($orderProductsDetails != '' && $orderProductsDetails->num_rows() > 0) {
                	foreach ($orderProductsDetails->result() as $orderProductsDetailsRow) {
                		array_push($orderProductsDetailsArr, $orderProductsDetailsRow->seller_product_id);
                		array_push($addedProductsArr, $orderProductsDetailsRow->seller_product_id);
                	}
                }

                $activityArr = array_merge($likedProductsIdArr, $addedProductsArr);
                array_push($activityArr, $UserId);
                $allNoty = $this->notify_model->get_latest_notifications($searchArr, $activityArr, $UserId);
                //echo "<pre>";print_r($allNoty->result());die;
				if ($allNoty->num_rows() > 0) {
                    $notyCount = 0;
                    $notyFinal = array();
                    foreach ($allNoty->result() as $allRow) {
                        if ($allRow->activity == 'like') {
                            if (in_array($allRow->activity_id, $addedProductsArr)) {
                                array_push($notyFinal, array('user_id' => $allRow->user_id, 'activity' => 'like', 'activity_id' => $allRow->activity_id, 'created' => $allRow->created));
                                $notyCount++;
                            }
                        } else if ($allRow->activity == 'featured') {
                            if (in_array($allRow->activity_id, $addedProductsArr)) {
                                array_push($notyFinal, array('user_id' => $allRow->user_id, 'activity' => 'featured', 'activity_id' => $allRow->activity_id, 'created' => $allRow->created));
                                $notyCount++;
                            }
                        } else if ($allRow->activity == 'follow') {
                            if ($UserId == $allRow->activity_id) {
                                array_push($notyFinal, array('user_id' => $allRow->user_id, 'activity' => 'follow', 'activity_id' => $allRow->activity_id, 'created' => $allRow->created));
                                $notyCount++;
                            }
                        } else if ($allRow->activity == 'comment') {
                            if (in_array($allRow->activity_id, $likedProductsIdArr)) {
                                array_push($notyFinal, array('user_id' => $allRow->user_id, 'activity' => 'comment', 'activity_id' => $allRow->activity_id, 'created' => $allRow->created, 'comment_id' => $allRow->comment_id));
                                $notyCount++;
                            }
                        } else if ($allRow->activity == 'own-product-comment') {
                            if (in_array($allRow->activity_id, $addedProductsArr)) {
                                array_push($notyFinal, array('user_id' => $allRow->user_id, 'activity' => 'own-product-comment', 'activity_id' => $allRow->activity_id, 'created' => $allRow->created, 'comment_id' => $allRow->comment_id));
                                $notyCount++;
                            }
                        } else if ($allRow->activity == 'review-comments') {
                            if (in_array($allRow->activity_id, $orderProductsIdArr)) {
                                array_push($notyFinal, array('user_id' => $allRow->user_id, 'activity' => 'review-comments', 'activity_id' => $allRow->activity_id, 'created' => $allRow->created, 'comment_id' => $allRow->comment_id));
                                $notyCount++;
                            }
                        }else if ($allRow->activity == 'Purchased') {
                        	if (in_array($allRow->activity_id, $orderProductsIdArr)) {
							    array_push($notyFinal, array('user_id' => $allRow->user_id, 'activity' => 'Purchased', 'activity_id' => $allRow->activity_id, 'created' => $allRow->created, 'comment_id' => $allRow->comment_id));
                                $notyCount++;
                            }
                        }
                   }

                    if ($notyCount > 0) {
                        $s=0;
                        $notification = array();
                        foreach($notyFinal as $notificationList){
                                $ActUsrPrfDtls = $this->notify_model->get_all_details(USERS, array('id' => $notificationList['user_id']));
                                if($ActUsrPrfDtls->row()->user_name != ""){
                                    if($ActUsrPrfDtls->row()->full_name != ""){
                                        $activityName = $ActUsrPrfDtls->row()->full_name;
                                    }else{
                                        $activityName = $ActUsrPrfDtls->row()->user_name;
                                    }
                                    if($ActUsrPrfDtls->row()->thumbnail!=""){
                                        $user_image=base_url().'images/users/'.$ActUsrPrfDtls->row()->thumbnail;
                                    }else{
                                        $user_image=base_url().'images/users/user-thumb1.png';
                                    }
                                    if($notificationList['activity'] != "follow"){
                                        if (in_array($notificationList['activity_id'], $addedSellProductsArr)) {
                                            $table = PRODUCT;
                                        } else if (in_array($notificationList['activity_id'], $addedUserProductsArr)) {
                                            $table = USER_PRODUCTS;
                                        }
                                        if ($table!=''){
	                                        $productDetails = $this->notify_model->get_all_details($table, array('seller_product_id' => $notificationList['activity_id']));
                                        }else {
                                        	$productDetails = '';
                                        }
                                        if($productDetails!='' && $productDetails->num_rows() > 0){
                                            $productUrl = base_url()."json/product?pid=".$productDetails->row()->id."&UserId=".$UserId;
                                            if($table == USER_PRODUCTS){
                                                $productUrl = base_url()."json/userproduct?pid=".$notificationList['activity_id']."&uname=".$this->data['userDetails']->row()->user_name."&UserId=".$UserId;
                                            }
                                            $activityUrl = "";
                                            $InvoiceNo = "";
                                            if ($notificationList['activity']== "Purchased") {
                                                $activityUrl = base_url()."json/settings/productReview?UserId=".$UserId."&sellerId=".$notificationList['user_id']."&InvoiceNumber=".$notificationList['comment_id'];
                                                $InvoiceNo = $notificationList['comment_id'];
                                            }elseif ($notificationList['activity'] == "review-comments") {
                                                $activityUrl = base_url()."json/settings/productReview?UserId=".$UserId."&sellerId=".$notificationList['user_id']."&InvoiceNumber=".$notificationList['comment_id'];
                                                $InvoiceNo = $notificationList['comment_id'];
                                            }
                                            $productImage = "";
                                            if($productDetails->num_rows() > 0){
                                                $imgArr=@explode(',',$productDetails->row()->image);
                                				$productImage = ($imgArr[0]=='') ? base_url().'images/product/dummyProductImage.jpg' : base_url().'images/product/'.$imgArr[0];
                                            }
                                            // $activityTime = strtotime($notificationList['created']);
                                            // $actTime = timespan($activityTime) . ' ' . $ago . '';
                                            $notification[] = array(
                                                                            "activity" => $notificationList['activity'],
                                                                            'activityUserId' => $ActUsrPrfDtls->row()->id,
                                                                            'activityName' => $activityName,
                                                                            'activityImage' => $user_image,
                                                                            "productName" => $productDetails->row()->product_name,
                                                                            "productImage" => $productImage,
                                                                            "invoiceNo" => $InvoiceNo,
                                                                            "activityTime" => $notificationList['created'],
                                                                        );
                                        }
                                    }else{
                                        // $activityTime = strtotime($notificationList['created']);
                                        // $actTime = timespan($activityTime) . ' ' . $ago . '';
                                        $notification[] = array(
                                                                        "activity" => $notificationList['activity'],
                                                                        'activityUserId' => $ActUsrPrfDtls->row()->id,
                                                                        'activityName' => $activityName,
                                                                        'activityImage' => $user_image,
                                                                        "productName" => "",
                                                                        "productImage" => "",
                                                                        "invoiceNo" => "",
                                                                        "activityTime" => $notificationList['created'],
                                                                    );
                                    }
                                }
                        }
                        $responseArr['status'] = "1";
                        $responseArr['response'] = "Success";
                        $responseArr['notification'] = $notification;
                    } else {
                        $responseArr['status'] = "0";
                        $responseArr['response'] = "No Notification Avaliable !!";
                    }
                } else {
                    $responseArr['status'] = "0";
                    $responseArr['response'] = "No Notification Avaliable !!";
                }
            } else {
                $responseArr['status'] = "0";
                $responseArr['response'] = "No Notification Avaliable !!";
            }
        }
        echo json_encode($responseArr);
    }
}
