<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * User related functions
 * @author Teamtweaks
 *
 */
class User extends MY_Controller {

    function __construct() {
        //echo "<pre>";print_r($_REQUEST);echo "</pre>";// die;
        parent::__construct();
        $this->load->helper(array('cookie', 'date', 'form', 'email'));
        $this->load->library(array('encrypt', 'form_validation'));
        $this->load->library('twconnect');
        $this->load->model(array('user_model', 'product_model','cart_model'));
        if ($_SESSION['sMainCategories'] == '') {
            $sortArr1 = array('field' => 'cat_position', 'type' => 'asc');
            $sortArr = array($sortArr1);
            $_SESSION['sMainCategories'] = $this->product_model->get_all_details(CATEGORY, array('rootID' => '0', 'status' => 'Active'), $sortArr);
        }
        $this->data['mainCategories'] = $_SESSION['sMainCategories'];

        if ($_SESSION['sColorLists'] == '') {
            $_SESSION['sColorLists'] = $this->user_model->get_all_details(LIST_VALUES, array('list_id' => '1'));
        }
        $this->data['mainColorLists'] = $_SESSION['sColorLists'];

        $this->data['loginCheck'] = $this->checkLogin('U');
        $this->data['likedProducts'] = array();
        if ($this->data['loginCheck'] != '') {
            $this->data['likedProducts'] = $this->user_model->get_all_details(PRODUCT_LIKES, array('user_id' => $this->checkLogin('U')));
        }
    }
	public function index(){
		
		
	 	
		$PaypalDodirect = unserialize($this->data['paypal_credit_card_settings']['settings']);
                
                    $dodirects = array(
                        'Sandbox' => $PaypalDodirect['mode'],           // Sandbox / testing mode option.
                        'APIUsername' =>$PaypalDodirect['Paypal_API_Username'],     // PayPal API username of the API caller
                        'APIPassword' => $PaypalDodirect['paypal_api_password'],    // PayPal API password of the API caller
                        'APISignature' => $PaypalDodirect['paypal_api_Signature'],  // PayPal API signature of the API caller
                        'APISubject' => '',                                     // PayPal API subject (email address of 3rd party user that has granted API permission for your app)
                        'APIVersion' => '85.0'      // API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
                    );
                   
                    // Show Errors
                    if($dodirects['Sandbox']){
                         
                        ini_set('display_errors', '1');
                    }
                        $this->load->library('paypal/Paypal_pro', $dodirects);
                    
					$DCFields = array(
						'authorizationid' => 'EC-2S851295RD303463M', 				// Required. The authorization identification number of the payment you want to capture. This is the transaction ID returned from DoExpressCheckoutPayment or DoDirectPayment.
						'amt' => '10.00', 							// Required. Must have two decimal places.  Decimal separator must be a period (.) and optional thousands separator must be a comma (,)
						'completetype' => '5.00', 					// Required.  The value Complete indiciates that this is the last capture you intend to make.  The value NotComplete indicates that you intend to make additional captures.
						'currencycode' => 'USD', 					// Three-character currency code
						'invnum' => '', 						// Your invoice number
						'note' => '', 							// Informational note about this setlement that is displayed to the buyer in an email and in his transaction history.  255 character max.
						'softdescriptor' => '', 				// Per transaction description of the payment that is passed to the customer's credit card statement.
						'storeid' => '', 						// ID of the merchant store.  This field is required for point-of-sale transactions.  Max: 50 char
						'terminalid' => ''						// ID of the terminal.  50 char max.  
					);
					
		$PayPalRequestData = array('DCFields' => $DCFields);
		$PayPalResult = $this->paypal_pro->DoCapture($PayPalRequestData);
		
					//$PayPalResult = $this->paypal_pro->GetExpressCheckoutDetails('EC-2S851295RD303463M');
		
                        echo '<pre>';print_r($PayPalResult);die;  
	}
    
	
	
	/**
     *
     * Function for quick signup
     */
    public function quickSignup() {
        $email = $this->input->post('email');
        $returnStr['success'] = '0';
        if (valid_email($email)) {
            $condition = array('email' => $email);
            $duplicateMail = $this->user_model->get_all_details(USERS, $condition);
            if ($duplicateMail->num_rows() > 0) {
                $returnStr['msg'] = 'Email id already exists';
            } else {
                $fullname = substr($email, 0, strpos($email, '@'));
                $checkAvail = $this->user_model->get_all_details(USERS, array('user_name' => $fullname));
                if ($checkAvail->num_rows() > 0) {
                    $avail = FALSE;
                } else {
                    $avail = TRUE;
                    $username = $fullname;
                }
                while (!$avail) {
                    $username = $fullname . rand(1111, 999999);
                    $checkAvail = $this->user_model->get_all_details(USERS, array('user_name' => $username));
                    if ($checkAvail->num_rows() > 0) {
                        $avail = FALSE;
                    } else {
                        $avail = TRUE;
                    }
                }
                if ($avail) {
                    $pwd = $this->get_rand_str('6');
                    $this->user_model->insertUserQuick($fullname, $username, $email, $pwd);
                    $this->session->set_userdata('quick_user_name', $username);
                    $returnStr['msg'] = 'Successfully registered';
                    $returnStr['full_name'] = $fullname;
                    $returnStr['user_name'] = $username;
                    $returnStr['password'] = $pwd;
                    $returnStr['email'] = $email;
                    $returnStr['success'] = '1';
                }
            }
        } else {
            $returnStr['msg'] = "Invalid email id";
        }
        echo json_encode($returnStr);
    }

    /**
     *
     * Function for quick signup update
     */
    public function quickSignupUpdate() {
        $returnStr['success'] = '0';
        $unameArr = $this->config->item('unameArr');
        $username = $this->input->post('username');
        if (!preg_match('/^\w{1,}$/', trim($username))) {
            $returnStr['msg'] = 'User name not valid. Only alphanumeric allowed';
        } elseif (in_array($username, $unameArr)) {
            $returnStr['msg'] = 'User name already exists';
        } else {
            $email = $this->input->post('email');
            $condition = array('user_name' => $username, 'email !=' => $email);
            $duplicateName = $this->user_model->get_all_details(USERS, $condition);
            if ($duplicateName->num_rows() > 0) {
                $returnStr['msg'] = 'Username already exists';
            } else {
                $pwd = $this->input->post('password');
                $fullname = $this->input->post('fullname');
                $this->user_model->updateUserQuick($fullname, $username, $email, $pwd);
                $this->session->set_userdata('quick_user_name', $username);
                $returnStr['msg'] = 'Successfully registered';
                $returnStr['success'] = '1';
            }
        }
        echo json_encode($returnStr);
    }

    public function send_quick_register_mail() {
        if ($this->checkLogin('U') != '') {
            redirect(base_url());
        } else {
            $quick_user_name = $this->session->userdata('quick_user_name');
            if ($quick_user_name == '') {
                redirect(base_url());
            } else {
                $condition = array('user_name' => $quick_user_name);
                $userDetails = $this->user_model->get_all_details(USERS, $condition);
                if ($userDetails->num_rows() == 1) {
                    $this->send_confirm_mail($userDetails);
                    $this->login_after_signup($userDetails);
                    $this->session->set_userdata('quick_user_name', '');

                    $next = $this->input->get('next');

                    if ($userDetails->row()->is_brand == 'yes') {
                        if (isset($next) && $next != '') {
                            redirect(base_url() . $next);
                        } else {
                            redirect(base_url() . 'create-brand');
                        }
                    } else {
                        if (isset($next) && $next != '') {
                            redirect(base_url() . $next);
                        } else {
                            redirect(base_url() . 'onboarding');
                        }
                    }
                } else {
                    redirect(base_url());
                }
            }
        }
    }

    public function registerUser() {
        $returnStr['success'] = '0';
        $unameArr = $this->config->item('unameArr');
        $fullname = $this->input->post('fullname');
        $username = $this->input->post('username');
        if (!preg_match('/^\w{1,}$/', trim($username))) {
            $returnStr['msg'] = 'User name not valid. Only alphanumeric allowed';
        } elseif (in_array($username, $unameArr)) {
            $returnStr['msg'] = 'User name already exists';
        } else {
            $email = $this->input->post('email');
            $pwd = $this->input->post('pwd');
            $brand = $this->input->post('brand');
            if (valid_email($email)) {
                $condition = array('user_name' => $username);
                $duplicateName = $this->user_model->get_all_details(USERS, $condition);
                if ($duplicateName->num_rows() > 0) {
                    $returnStr['msg'] = 'User name already exists';
                } else {
                    $condition = array('email' => $email);
                    $duplicateMail = $this->user_model->get_all_details(USERS, $condition);
                    if ($duplicateMail->num_rows() > 0) {
                        $returnStr['msg'] = 'Email id already exists';
                    } else {
                        $this->user_model->insertUserQuick($fullname, $username, $email, $pwd, $brand);
                        $this->session->set_userdata('quick_user_name', $username);
                        $returnStr['msg'] = 'Successfully registered';
                        $returnStr['success'] = '1';
                    }
                }
            } else {
                $returnStr['msg'] = "Invalid email id";
            }
        }
        echo json_encode($returnStr);
    }

    public function resend_confirm_mail() {
        $mail = $this->input->post('mail');
        if ($mail == '') {
            echo '0';
        } else {
            $condition = array('email' => $mail);
            $userDetails = $this->user_model->get_all_details(USERS, $condition);
            $this->send_confirm_mail($userDetails);
            echo '1';
        }
    }

    public function send_email_confirmation() {
        $returnStr['status_code'] = 0;
        if ($this->checkLogin('U') == '') {
            if ($this->lang->line('login_requ') != '')
                $returnStr['message'] = $this->lang->line('login_requ');
            else
                $returnStr['message'] = 'Login required';
        }else {
            $this->send_confirm_mail($this->data['userDetails']);
            $returnStr['status_code'] = 1;
        }
        echo json_encode($returnStr);
    }

    public function send_confirm_mail($userDetails = '') {
        $uid = $userDetails->row()->id;
        $email = $userDetails->row()->email;
        $randStr = $this->get_rand_str('10');
        $condition = array('id' => $uid);
        $dataArr = array('verify_code' => $randStr);
        $this->user_model->update_details(USERS, $dataArr, $condition);
        $newsid = '3';
        $template_values = $this->user_model->get_newsletter_template_details($newsid);

        $cfmurl = base_url() . 'site/user/confirm_register/' . $uid . "/" . $randStr . "/confirmation";
        $subject = 'From: ' . $this->config->item('email_title') . ' - ' . $template_values['news_subject'];
        $adminnewstemplateArr = array('email_title' => $this->config->item('email_title'), 'logo' => $this->data['logo']);
        extract($adminnewstemplateArr);
        //$ddd =htmlentities($template_values['news_descrip'],null,'UTF-8');
        $header .="Content-Type: text/plain; charset=ISO-8859-1\r\n";

        $message .= '<!DOCTYPE HTML>
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
			<meta name="viewport" content="width=device-width"/><body>';
        include('./newsletter/registeration' . $newsid . '.php');

        $message .= '</body>
			</html>';

        if ($template_values['sender_name'] == '' && $template_values['sender_email'] == '') {
            $sender_email = $this->data['siteContactMail'];
            $sender_name = $this->data['siteTitle'];
        } else {
            $sender_name = $template_values['sender_name'];
            $sender_email = $template_values['sender_email'];
        }

        $email_values = array('mail_type' => 'html',
            'from_mail_id' => $sender_email,
            'mail_name' => $sender_name,
            'to_mail_id' => $email,
            'subject_message' => $template_values['news_subject'],
            'body_messages' => $message,
            'mail_id' => 'register mail'
        );
		
		
        $email_send_to_common = $this->product_model->common_email_send($email_values);
    }

    public function signup_form() {
        if ($this->checkLogin('U') != '') {
            redirect(base_url());
        } else {
            $this->data['next'] = $this->input->get('next');
            $this->data['heading'] = 'Sign up';
            $this->load->view('site/user/signup.php', $this->data);
        }
    }

    /**
     *
     * Loading login page
     */
    public function login_form() {
        if ($this->checkLogin('U') != '') {
            redirect(base_url());
        } else {
            $this->data['next'] = $this->input->get('next');
            //echo $this->data['next'];die;
            $this->data['heading'] = 'Sign in';
            $this->load->view('site/user/login.php', $this->data);
        }
    }

    public function linked_login_user() {
        @session_start();
        if (isset($_SESSION['linkedin_id']) && ($_SESSION['linkedin_id']) != '') {
            $email = $_SESSION['linkedin_email'];
            $username = $_SESSION['linkedin_name'];
            $api_id = $_SESSION['linkedin_id'];
            $fullname = '';
            $condition = array('email' => $email);
            $checkUser = $this->user_model->get_all_details(USERS, $condition);
            if ($checkUser->num_rows() == 1) {
                $userdata = array(
                    'fc_session_user_id' => $checkUser->row()->id,
                    'session_user_name' => $_SESSION['linkedin_name'],
                    'session_user_email' => $_SESSION['linkedin_email']
                );
                $this->session->set_userdata($userdata);
                $this->setErrorMessage('success', 'Login Success');
                redirect(base_url());
            } elseif ($checkUser->num_rows() == 0) {
                $thumbnail = '';
                if (isset($_SESSION['fb_image_name']) && ($_SESSION['fb_image_name']) != '') {
                    $thumbnail = $_SESSION['fb_image_name'];
                }
                $this->user_model->register_via_linkedin($username, $fullname, $email, $api_id, $thumbnail);
                $last_insert_id = $this->user_model->get_last_insert_id();
                $condition = array('id' => $last_insert_id);
                $checkUser = $this->user_model->get_all_details(USERS, $condition);
                if ($checkUser->num_rows() == 1) {
                    $userdata = array(
                        'fc_session_user_id' => $checkUser->row()->id,
                        'session_user_name' => $checkUser->row()->user_name,
                        'session_user_email' => $checkUser->row()->email
                    );
                    $this->session->set_userdata($userdata);
                    $this->setErrorMessage('success', 'Login success');
                    redirect(base_url());
                } else {
                    $this->setErrorMessage('error', 'Invalid Login Details');
                    redirect(base_url());
                }
            } else {
                $this->setErrorMessage('error', 'Invalid Login Details');
                redirect(base_url());
            }
        }
    }

    public function login_user() {
        //echo"asssas";
        //print_r ($this->input->post()); die;
        $this->form_validation->set_rules('email', 'Email Address', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $next = $this->input->post('next');
        if ($this->form_validation->run() === FALSE) {
            if ($this->lang->line('email_pwd_req') != '')
                $lg_err_msg = $this->lang->line('email_pwd_req');
            else
                $lg_err_msg = 'Email and password fields required';
            $this->setErrorMessage('error', $lg_err_msg);
            redirect('login?next=' . urlencode($next));
        }else {
            $email = $this->input->post('email');
            $pwd = md5($this->input->post('password'));
            $condition = array('email' => $email, 'password' => $pwd, 'status' => 'Active');
            $checkUser = $this->user_model->get_all_details(USERS, $condition);

            if ($checkUser->num_rows() == '1') {
                $userdata = array(
                    'fc_session_user_id' => $checkUser->row()->id,
                    'session_user_name' => $checkUser->row()->user_name,
                    'session_user_email' => $checkUser->row()->email
                );
                //				echo "<pre>";print_r($userdata);
                $this->session->set_userdata($userdata);
                //				echo $this->session->userdata('fc_session_user_id');die;
                $datestring = "%Y-%m-%d %H:%i:%s";
                $time = time();
                $newdata = array(
                    'last_login_date' => mdate($datestring, $time),
                    'last_login_ip' => $this->input->ip_address()
                );
                $condition = array('id' => $checkUser->row()->id);
                $this->user_model->update_details(USERS, $newdata, $condition);

                $this->user_model->updategiftcard(GIFTCARDS_TEMP, $this->checkLogin('T'), $checkUser->row()->id);

                if ($this->data['login_succ_msg'] != '')
                    $lg_err_msg = $this->data['login_succ_msg'];
                else
                    $lg_err_msg = 'You are Logged In ...';
                $this->setErrorMessage('success', $lg_err_msg);
                //				$this->session->set_flashdata('loadAfterLog', '1');
                if ($next == 'close') {
                    echo "
					<script>
					window.close();
					</script>
					";
                } else {
                    redirect($next);
                }
            } else {
                if ($this->lang->line('inval_log_det') != '')
                    $lg_err_msg = $this->lang->line('inval_log_det');
                else
                    $lg_err_msg = 'Invalid login details';
                $this->setErrorMessage('error', $lg_err_msg);
                redirect('login?next=' . urlencode($next));
            }
        }
    }

    public function login_after_signup($userDetails = '') {
        if ($userDetails->num_rows() == '1') {
            $userdata = array(
                'fc_session_user_id' => $userDetails->row()->id,
                'session_user_name' => $userDetails->row()->user_name,
                'session_user_email' => $userDetails->row()->email
            );
            $this->session->set_userdata($userdata);
            $datestring = "%Y-%m-%d %H:%i:%s";
            $time = time();
            $newdata = array(
                'last_login_date' => mdate($datestring, $time),
                'last_login_ip' => $this->input->ip_address()
            );
            $condition = array('id' => $userDetails->row()->id);
            $this->user_model->update_details(USERS, $newdata, $condition);

            $this->user_model->updategiftcard(GIFTCARDS_TEMP, $this->checkLogin('T'), $userDetails->row()->id);
        } else {
            redirect(base_url());
        }
    }

    public function confirm_register() {
        $uid = $this->uri->segment(4, 0);
        $code = $this->uri->segment(5, 0);
        $mode = $this->uri->segment(6, 0);
        if ($mode == 'confirmation') {
            $condition = array('verify_code' => $code, 'id' => $uid);
            $checkUser = $this->user_model->get_all_details(USERS, $condition);
            if ($checkUser->num_rows() == 1) {
                $conditionArr = array('id' => $uid, 'verify_code' => $code);
                $dataArr = array('is_verified' => 'Yes');
                $this->user_model->update_details(USERS, $dataArr, $condition);
                $subscribeCheck = $this->user_model->get_all_details(SUBSCRIBERS_LIST, array('subscrip_mail' => $checkUser->row()->email));
                if ($subscribeCheck->num_rows() == 0) {
                    $this->user_model->simple_insert(SUBSCRIBERS_LIST, array('subscrip_mail' => $checkUser->row()->email, 'status' => 'Active'));
                }
                if ($this->lang->line('mail_veri_succc') != '')
                    $lg_err_msg = $this->lang->line('mail_veri_succc');
                else
                    $lg_err_msg = 'Great going ! Your mail ID has been verified';
                $this->setErrorMessage('success', $lg_err_msg);
                $this->login_after_signup($checkUser);
                redirect(base_url());
            }else {
                if ($this->lang->line('inval_conf_link') != '')
                    $lg_err_msg = $this->lang->line('inval_conf_link');
                else
                    $lg_err_msg = 'Invalid confirmation link';
                $this->setErrorMessage('error', $lg_err_msg);
                redirect(base_url());
            }
        }else {
            if ($this->lang->line('inval_conf_link') != '')
                $lg_err_msg = $this->lang->line('inval_conf_link');
            else
                $lg_err_msg = 'Invalid confirmation link';
            $this->setErrorMessage('error', $lg_err_msg);
            redirect(base_url());
        }
    }

    public function logout_user() {
        $datestring = "%Y-%m-%d %H:%i:%s";
        $time = time();
        $newdata = array(
            'last_logout_date' => mdate($datestring, $time)
        );
        $condition = array('id' => $this->checkLogin('U'));
        $this->user_model->update_details(USERS, $newdata, $condition);
        $userdata = array(
            'fc_session_user_id' => '',
            'session_user_name' => '',
            'session_user_email' => '',
            'fc_session_temp_id' => ''
        );
        $this->session->unset_userdata($userdata);

        @session_start();
        unset($_SESSION['token']);
        $twitter_return_values = array('tw_status' => '',
            'tw_access_token' => ''
        );

        $this->session->unset_userdata($twitter_return_values);
        if ($this->lang->line('logout_succ') != '')
            $lg_err_msg = $this->lang->line('logout_succ');
        else
            $lg_err_msg = 'Successfully logged out from your account';
        $this->setErrorMessage('success', $lg_err_msg);
        redirect(base_url());
    }

    public function forgot_password_form() {
        $this->data['heading'] = 'Forgot Password';
        $this->load->view('site/user/forgot_password.php', $this->data);
    }

    public function forgot_password_user() {
        $this->form_validation->set_rules('email', 'Email Address', 'required');
        if ($this->form_validation->run() === FALSE) {
            if ($this->lang->line('email_requ') != '')
                $lg_err_msg = $this->lang->line('email_requ');
            else
                $lg_err_msg = 'Email address required';
            $this->setErrorMessage('error', $lg_err_msg);
            redirect('forgot-password');
        }else {
            $email = $this->input->post('email');
            if (valid_email($email)) {
                $condition = array('email' => $email);
                $checkUser = $this->user_model->get_all_details(USERS, $condition);
                if ($checkUser->num_rows() == '1') {
                    $pwd = $this->get_rand_str('6');
                    $newdata = array('password' => md5($pwd));
                    $condition = array('email' => $email);
                    $this->user_model->update_details(USERS, $newdata, $condition);

                    $this->send_user_password($pwd, $checkUser);
                    if ($this->lang->line('pwd_sen_mail') != '')
                        $lg_err_msg = $this->lang->line('pwd_sen_mail');
                    else
                        $lg_err_msg = 'New password sent to your mail';
                    $this->setErrorMessage('success', $lg_err_msg);
                    redirect('login');
                }else {
                    if ($this->lang->line('mail_not_record') != '')
                        $lg_err_msg = $this->lang->line('mail_not_record');
                    else
                        $lg_err_msg = 'Your email id not matched in our records';
                    $this->setErrorMessage('error', $lg_err_msg);
                    redirect('forgot-password');
                }
            }else {
                if ($this->lang->line('mail_not_valid') != '')
                    $lg_err_msg = $this->lang->line('mail_not_valid');
                else
                    $lg_err_msg = 'Email id not valid';
                $this->setErrorMessage('error', $lg_err_msg);
                redirect('forgot-password');
            }
        }
    }

    public function send_user_password($pwd = '', $query) {
        $newsid = '5';
        $template_values = $this->user_model->get_newsletter_template_details($newsid);
        $adminnewstemplateArr = array('email_title' => $this->config->item('email_title'), 'logo' => $this->data['logo']);
        extract($adminnewstemplateArr);
        $subject = 'From: ' . $this->config->item('email_title') . ' - ' . $template_values['news_subject'];
        $message .= '<!DOCTYPE HTML>
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
			<meta name="viewport" content="width=device-width"/>
			<title>' . $template_values['news_subject'] . '</title>
			<body>';
        include('./newsletter/registeration' . $newsid . '.php');

        $message .= '</body>
			</html>';


        if ($template_values['sender_name'] == '' && $template_values['sender_email'] == '') {
            $sender_email = $this->config->item('site_contact_mail');
            $sender_name = $this->config->item('email_title');
        } else {
            $sender_name = $template_values['sender_name'];
            $sender_email = $template_values['sender_email'];
        }

        $email_values = array('mail_type' => 'html',
            'from_mail_id' => $sender_email,
            'mail_name' => $sender_name,
            'to_mail_id' => $query->row()->email,
            'subject_message' => 'Password Reset',
            'body_messages' => $message,
            'mail_id' => 'forgot'
        );
        $email_send_to_common = $this->product_model->common_email_send($email_values);

        /* 		echo $this->email->print_debugger();die;
         */
    }

    public function add_fancy_item() {
        //echo "sdfsf";die;
        $returnStr['status_code'] = 0;
        if ($this->checkLogin('U') == '') {
            if ($this->lang->line('u_must_login') != '')
                $returnStr['message'] = $this->lang->line('u_must_login');
            else
                $returnStr['message'] = 'You must login';
        }else {
            $tid = $this->input->post('tid');
            $checkProductLike = $this->user_model->get_all_details(PRODUCT_LIKES, array('product_id' => $tid, 'user_id' => $this->checkLogin('U')));
            if ($checkProductLike->num_rows() == 0) {
                $productDetails = $this->user_model->get_all_details(PRODUCT, array('seller_product_id' => $tid));
                if ($productDetails->num_rows() == 0) {
                    $productDetails = $this->user_model->get_all_details(USER_PRODUCTS, array('seller_product_id' => $tid));
                    $productTable = USER_PRODUCTS;
                } else {
                    $productTable = PRODUCT;
                }
                if ($productDetails->num_rows() == 1) {
                    $likes = $productDetails->row()->likes;
                    $dataArr = array('product_id' => $tid, 'user_id' => $this->checkLogin('U'), 'ip' => $this->input->ip_address());
                    $this->user_model->simple_insert(PRODUCT_LIKES, $dataArr);
                    $actArr = array(
                        'activity_name' => 'fancy',
                        'activity_id' => $tid,
                        'user_id' => $this->checkLogin('U'),
                        'activity_ip' => $this->input->ip_address()
                    );
                    $this->user_model->simple_insert(USER_ACTIVITY, $actArr);
                    $datestring = "%Y-%m-%d %H:%i:%s";
                    $time = time();
                    $createdTime = mdate($datestring, $time);
                    $actArr = array(
                        'activity' => 'like',
                        'activity_id' => $tid,
                        'user_id' => $this->checkLogin('U'),
                        'activity_ip' => $this->input->ip_address(),
                        'created' => $createdTime
                    );
                    $this->user_model->simple_insert(NOTIFICATIONS, $actArr);
                    $likes++;
                    $dataArr = array('likes' => $likes);
                    $condition = array('seller_product_id' => $tid);
                    $this->user_model->update_details($productTable, $dataArr, $condition);
                    $totalUserLikes = $this->data['userDetails']->row()->likes;
                    $totalUserLikes++;
                    $this->user_model->update_details(USERS, array('likes' => $totalUserLikes), array('id' => $this->checkLogin('U')));
                    /** ***********Send Message to TWITTER************ */
                    if ($this->data['userDetails']->row()->twitter_id != '') {
                        $TwitterId = $this->data['userDetails']->row()->twitter_id;
                        if ($productDetails->row()->image != '') {
                            $image = base_url() . "images/product/" . $productDetails->row()->image;
                        } else {
                            $image = base_url() . "images/product/no_image.gif";
                        }
                        $short_url = $this->user_model->get_all_details(SHORTURL, array('id' => $productDetails->row()->short_url_id));
                        if ($short_url->num_rows() == 1) {
                            $url = base_url() . 't/' . $short_url->row()->id;
                        }
                        include_once './twittercard/twitter-card.php';
                        $card = new Twitter_Card();


                        $card->setURL('http://www.nytimes.com/2012/02/19/arts/music/amid-police-presence-fans-congregate-for-whitney-houstons-funeral-in-newark.html');
                        $card->setTitle('Parade of Fans for Houston\'s Funeral');
                        $card->setDescription('NEWARK - The guest list and parade of limousines with celebrities emerging from them seemed more suited to a red carpet event in Hollywood or New York than than a gritty stretch of Sussex Avenue near the former site of the James M. Baxter Terrace public housing project here.');
                        $card->setImage('http://graphics8.nytimes.com/images/2012/02/19/us/19whitney-span/19whitney-span-articleLarge.jpg', 600, 330);
                        $send_tweets = $this->twconnect->tw_post('https://api.twitter.com/1.1/statuses/update.json', $card->asHTML());
                        print_r($send_tweets);
                    }
                    /*                     * ***********************END******************** */

                    //die;

                    /*
                     * -------------------------------------------------------
                     * Creating list automatically when user likes a product
                     * -------------------------------------------------------
                     *
                      $listCheck = $this->user_model->get_list_details($tid,$this->checkLogin('U'));
                      if ($listCheck->num_rows() == 0){
                      $productCategoriesArr = explode(',', $productDetails->row()->category_id);
                      if (count($productCategoriesArr)>0){
                      foreach ($productCategoriesArr as $productCategoriesRow){
                      if ($productCategoriesRow != ''){
                      $productCategory = $this->user_model->get_all_details(CATEGORY,array('id'=>$productCategoriesRow));
                      if ($productCategory->num_rows()==1){

                      }
                      }
                      }
                      }
                      }
                     */
                    $returnStr['status_code'] = 1;
                } else {
                    if ($this->lang->line('prod_not_avail') != '')
                        $returnStr['message'] = $this->lang->line('prod_not_avail');
                    else
                        $returnStr['message'] = 'Product not available';
                }
            }
        }
        echo json_encode($returnStr);
    }

    public function remove_fancy_item() {
        $returnStr['status_code'] = 0;
        if ($this->checkLogin('U') == '') {
            if ($this->lang->line('u_must_login') != '')
                $returnStr['message'] = $this->lang->line('u_must_login');
            else
                $returnStr['message'] = 'You must login';
        }else {
            $tid = $this->input->post('tid');
            $checkProductLike = $this->user_model->get_all_details(PRODUCT_LIKES, array('product_id' => $tid, 'user_id' => $this->checkLogin('U')));
            if ($checkProductLike->num_rows() == 1) {
                $productDetails = $this->user_model->get_all_details(PRODUCT, array('seller_product_id' => $tid));
                if ($productDetails->num_rows() == 0) {
                    $productDetails = $this->user_model->get_all_details(USER_PRODUCTS, array('seller_product_id' => $tid));
                    $productTable = USER_PRODUCTS;
                } else {
                    $productTable = PRODUCT;
                }
                if ($productDetails->num_rows() == 1) {
                    $likes = $productDetails->row()->likes;
                    $conditionArr = array('product_id' => $tid, 'user_id' => $this->checkLogin('U'));
                    $this->user_model->commonDelete(PRODUCT_LIKES, $conditionArr);
                    $actArr = array(
                        'activity_name' => 'unfancy',
                        'activity_id' => $tid,
                        'user_id' => $this->checkLogin('U'),
                        'activity_ip' => $this->input->ip_address()
                    );
                    $this->user_model->simple_insert(USER_ACTIVITY, $actArr);
                    $likes--;
                    $dataArr = array('likes' => $likes);
                    $condition = array('seller_product_id' => $tid);
                    $this->user_model->update_details($productTable, $dataArr, $condition);
                    $totalUserLikes = $this->data['userDetails']->row()->likes;
                    $totalUserLikes--;
                    $this->user_model->update_details(USERS, array('likes' => $totalUserLikes), array('id' => $this->checkLogin('U')));
                    $returnStr['status_code'] = 1;
                } else {
                    if ($this->lang->line('prod_not_avail') != '')
                        $returnStr['message'] = $this->lang->line('prod_not_avail');
                    else
                        $returnStr['message'] = 'Product not available';
                }
            }
        }
        echo json_encode($returnStr);
    }

    public function display_user_profile() {
        $username = urldecode($this->uri->segment(2, 0));


        if ($username == 'administrator') {
            $this->data['heading'] = $username;
            $this->load->view('site/user/display_admin_profile');
        } else {
            $userProfileDetails = $this->user_model->get_all_details(USERS, array('user_name' => $username, 'status' => 'Active'));
            if ($userProfileDetails->num_rows() == 1) {
                if ($userProfileDetails->row()->full_name != '') {
                    $this->data['heading'] = $userProfileDetails->row()->full_name;
                } else {
                    $this->data['heading'] = $username;
                }
                if ($userProfileDetails->row()->visibility == 'Only you' && $userProfileDetails->row()->id != $this->checkLogin('U')) {
                    $this->load->view('site/user/display_user_profile_private', $this->data);
                } else {

                    $this->data['productLikeDetails'] = $this->user_model->get_like_details_fully($userProfileDetails->row()->id);
                    $this->data['userProductLikeDetails'] = $this->user_model->get_like_details_fully_user_products($userProfileDetails->row()->id);
                    $this->data['userProfileDetails'] = $userProfileDetails;
                    $this->data['recentActivityDetails'] = $this->user_model->get_activity_details($userProfileDetails->row()->id);
                    $this->data['featureProductDetails'] = $this->product_model->get_featured_details($userProfileDetails->row()->feature_product);

                    $cond = "select count(id) as productCount from ".PRODUCT." where user_id=".$this->data['userProfileDetails']->row()->id;
                    $sellingProductCount = $this->user_model->ExecuteQuery($cond);
                    $cond = "select count(id) as affliateCount from ".USER_PRODUCTS." where user_id=".$this->data['userProfileDetails']->row()->id;
                    $affliateProductCount = $this->user_model->ExecuteQuery($cond);
                    $this->data['totalProductCount'] = $sellingProductCount->row()->productCount+$affliateProductCount->row()->affliateCount;

                    $this->data['follow'] = $this->product_model->view_follow_list($userProfileDetails->row()->id);
                    $this->load->view('site/user/display_user_profile', $this->data);
                }
            } else {
                if ($this->lang->line('user_det_not_avail') != '')
                    $lg_err_msg = $this->lang->line('user_det_not_avail');
                else
                    $lg_err_msg = 'User details not available';
                $this->setErrorMessage('error', $lg_err_msg);
                redirect(base_url());
            }
        }
    }

    public function seller_timeline() {
        $username = urldecode($this->uri->segment(3, 0));
        $this->data['userProfileDetails'] = $this->user_model->get_all_details(USERS, array('user_name' => $username, 'status' => 'Active'));
        if ($this->data['userProfileDetails']->row()->group == "Seller") {
            $this->data['follow'] = $this->product_model->view_follow_list($this->data['userProfileDetails']->row()->id);
            $this->data['recentActivityDetails'] = $this->user_model->get_activity_details($this->data['userProfileDetails']->row()->id,50,'desc','all');
            $cond = "select count(id) as productCount from ".PRODUCT." where user_id=".$this->data['userProfileDetails']->row()->id;
            $sellingProductCount = $this->user_model->ExecuteQuery($cond);
            $cond = "select count(id) as affliateCount from ".USER_PRODUCTS." where user_id=".$this->data['userProfileDetails']->row()->id;
            $affliateProductCount = $this->user_model->ExecuteQuery($cond);
            $this->data['totalProductCount'] = $sellingProductCount->row()->productCount+$affliateProductCount->row()->affliateCount;
            $this->load->view('site/user/display_seller_dashboard', $this->data);
        } else {
            redirect(base_url() . "user/" . $username);
        }
    }

    public function add_follow() {
        $returnStr['status_code'] = 0;
        if ($this->checkLogin('U') != '') {
            $follow_id = $this->input->post('user_id');
            $followingListArr = explode(',', $this->data['userDetails']->row()->following);
            if (!in_array($follow_id, $followingListArr)) {
                $followingListArr[] = $follow_id;
                $newFollowingList = implode(',', $followingListArr);
                $followingCount = $this->data['userDetails']->row()->following_count;
                $followingCount++;
                $dataArr = array('following' => $newFollowingList, 'following_count' => $followingCount);
                $condition = array('id' => $this->checkLogin('U'));
                $this->user_model->update_details(USERS, $dataArr, $condition);
                $followUserDetails = $this->user_model->get_all_details(USERS, array('id' => $follow_id));
                if ($followUserDetails->num_rows() == 1) {
                    $followersListArr = explode(',', $followUserDetails->row()->followers);
                    if (!in_array($this->checkLogin('U'), $followersListArr)) {
                        $followersListArr[] = $this->checkLogin('U');
                        $newFollowersList = implode(',', $followersListArr);
                        $followersCount = $followUserDetails->row()->followers_count;
                        $followersCount++;
                        $dataArr = array('followers' => $newFollowersList, 'followers_count' => $followersCount);
                        $condition = array('id' => $follow_id);
                        $this->user_model->update_details(USERS, $dataArr, $condition);
                    }
                }
                $actArr = array(
                    'activity_name' => 'follow',
                    'activity_id' => $follow_id,
                    'user_id' => $this->checkLogin('U'),
                    'activity_ip' => $this->input->ip_address()
                );
                $this->user_model->simple_insert(USER_ACTIVITY, $actArr);
                $datestring = "%Y-%m-%d %H:%i:%s";
                $time = time();
                $createdTime = mdate($datestring, $time);
                $actArr = array(
                    'activity' => 'follow',
                    'activity_id' => $follow_id,
                    'user_id' => $this->checkLogin('U'),
                    'activity_ip' => $this->input->ip_address(),
                    'created' => $createdTime
                );
                $this->user_model->simple_insert(NOTIFICATIONS, $actArr);
                $this->send_noty_mail($followUserDetails->result_array());
                $returnStr['status_code'] = 1;
            } else {
                $returnStr['status_code'] = 1;
            }
        }
        echo json_encode($returnStr);
    }

    public function add_follows() {
        $returnStr['status_code'] = 0;
        if ($this->checkLogin('U') != '') {
            $follow_ids = $this->input->post('user_ids');
            $follow_ids_arr = explode(',', $follow_ids);
            $followingListArr = explode(',', $this->data['userDetails']->row()->following);
            foreach ($follow_ids_arr as $flwRow) {
                if (in_array($flwRow, $followingListArr)) {
                    if (($key = array_search($flwRow, $follow_ids_arr)) !== false) {
                        unset($follow_ids_arr[$key]);
                    }
                }
            }
            if (count($follow_ids_arr) > 0) {
                $newfollowingListArr = array_merge($followingListArr, $follow_ids_arr);
                $newFollowingList = implode(',', $newfollowingListArr);
                $followingCount = $this->data['userDetails']->row()->following_count;
                $newCount = count($follow_ids_arr);
                $followingCount = $followingCount + $newCount;
                $dataArr = array('following' => $newFollowingList, 'following_count' => $followingCount);
                $condition = array('id' => $this->checkLogin('U'));
                $this->user_model->update_details(USERS, $dataArr, $condition);
                $conditionStr = 'where id IN (' . implode(',', $follow_ids_arr) . ')';
                $followUserDetailsArr = $this->user_model->get_users_details($conditionStr);
                if ($followUserDetailsArr->num_rows() > 0) {
                    foreach ($followUserDetailsArr->result() as $followUserDetails) {
                        $followersListArr = explode(',', $followUserDetails->followers);
                        if (!in_array($this->checkLogin('U'), $followersListArr)) {
                            $followersListArr[] = $this->checkLogin('U');
                            $newFollowersList = implode(',', $followersListArr);
                            $followersCount = $followUserDetails->followers_count;
                            $followersCount++;
                            $dataArr = array('followers' => $newFollowersList, 'followers_count' => $followersCount);
                            $condition = array('id' => $followUserDetails->id);
                            $this->user_model->update_details(USERS, $dataArr, $condition);
                            $datestring = "%Y-%m-%d %H:%i:%s";
                            $time = time();
                            $createdTime = mdate($datestring, $time);
                            $actArr = array(
                                'activity' => 'follow',
                                'activity_id' => $followUserDetails->id,
                                'user_id' => $this->checkLogin('U'),
                                'activity_ip' => $this->input->ip_address(),
                                'created' => $createdTime
                            );
                            $this->user_model->simple_insert(NOTIFICATIONS, $actArr);
                            $this->send_noty_mails($followUserDetails);
                        }
                    }
                }
                $returnStr['status_code'] = 1;
            } else {
                $returnStr['status_code'] = 1;
            }
        }
        echo json_encode($returnStr);
    }

    public function delete_follow() {
        $returnStr['status_code'] = 0;
        if ($this->checkLogin('U') != '') {
            $follow_id = $this->input->post('user_id');
            $followingListArr = explode(',', $this->data['userDetails']->row()->following);
            if (in_array($follow_id, $followingListArr)) {
                if (($key = array_search($follow_id, $followingListArr)) !== false) {
                    unset($followingListArr[$key]);
                }
                $newFollowingList = implode(',', $followingListArr);
                $followingCount = $this->data['userDetails']->row()->following_count;
                $followingCount--;
                $dataArr = array('following' => $newFollowingList, 'following_count' => $followingCount);
                $condition = array('id' => $this->checkLogin('U'));
                $this->user_model->update_details(USERS, $dataArr, $condition);
                $followUserDetails = $this->user_model->get_all_details(USERS, array('id' => $follow_id));
                if ($followUserDetails->num_rows() == 1) {
                    $followersListArr = explode(',', $followUserDetails->row()->followers);
                    if (in_array($this->checkLogin('U'), $followersListArr)) {
                        if (($key = array_search($this->checkLogin('U'), $followersListArr)) !== false) {
                            unset($followersListArr[$key]);
                        }
                        $newFollowersList = implode(',', $followersListArr);
                        $followersCount = $followUserDetails->row()->followers_count;
                        $followersCount--;
                        $dataArr = array('followers' => $newFollowersList, 'followers_count' => $followersCount);
                        $condition = array('id' => $follow_id);
                        $this->user_model->update_details(USERS, $dataArr, $condition);
                    }
                }
                $actArr = array(
                    'activity_name' => 'unfollow',
                    'activity_id' => $follow_id,
                    'user_id' => $this->checkLogin('U'),
                    'activity_ip' => $this->input->ip_address()
                );
                $this->user_model->simple_insert(USER_ACTIVITY, $actArr);
                $returnStr['status_code'] = 1;
            } else {
                $returnStr['status_code'] = 1;
            }
        }
        echo json_encode($returnStr);
    }

    public function display_user_added() {
        $username = urldecode($this->uri->segment(2, 0));
        $userProfileDetails = $this->user_model->get_all_details(USERS, array('user_name' => $username));
        if ($userProfileDetails->num_rows() == 1) {
            if ($userProfileDetails->row()->visibility == 'Only you' && $userProfileDetails->row()->id != $this->checkLogin('U')) {
                $this->load->view('site/user/display_user_profile_private', $this->data);
            } else {
                if ($userProfileDetails->row()->full_name != '') {
                    $this->data['heading'] = $userProfileDetails->row()->full_name . ' - Products';
                } else {
                    $this->data['heading'] = $username . ' - Products';
                }
                $this->data['userProfileDetails'] = $userProfileDetails;
                $this->data['recentActivityDetails'] = $this->user_model->get_activity_details($userProfileDetails->row()->id);
                $this->data['addedProductDetails'] = $this->product_model->view_product_details(' where p.user_id=' . $userProfileDetails->row()->id . ' and p.status="Publish"');
                $this->data['notSellProducts'] = $this->product_model->view_notsell_product_details(' where p.user_id=' . $userProfileDetails->row()->id . ' and p.status="Publish"');

                $cond = "select count(id) as productCount from ".PRODUCT." where user_id=".$this->data['userProfileDetails']->row()->id;
                $sellingProductCount = $this->user_model->ExecuteQuery($cond);
                $cond = "select count(id) as affliateCount from ".USER_PRODUCTS." where user_id=".$this->data['userProfileDetails']->row()->id;
                $affliateProductCount = $this->user_model->ExecuteQuery($cond);
                $this->data['totalProductCount'] = $sellingProductCount->row()->productCount+$affliateProductCount->row()->affliateCount;

                $this->data['follow'] = $this->product_model->view_follow_list($userProfileDetails->row()->id);
                $this->load->view('site/user/display_user_added', $this->data);
            }
        } else {
            redirect(base_url());
        }
    }

    public function display_user_lists() {

        $username = urldecode($this->uri->segment(2, 0));
        $userProfileDetails = $this->user_model->get_all_details(USERS, array('user_name' => $username));
        if ($userProfileDetails->num_rows() == 1) {
            if ($userProfileDetails->row()->visibility == 'Only you' && $userProfileDetails->row()->id != $this->checkLogin('U')) {
                $this->load->view('site/user/display_user_profile_private', $this->data);
            } else {
                if ($userProfileDetails->row()->full_name != '') {
                    $this->data['heading'] = $userProfileDetails->row()->full_name . ' - Lists';
                } else {
                    $this->data['heading'] = $username . ' - Lists';
                }
                $this->data['userProfileDetails'] = $userProfileDetails;
                $this->data['recentActivityDetails'] = $this->user_model->get_activity_details($userProfileDetails->row()->id);
                $this->data['listDetails'] = $this->product_model->get_all_details(LISTS_DETAILS, array('user_id' => $userProfileDetails->row()->id));
                $this->data['follow'] = $this->product_model->view_follow_list($userProfileDetails->row()->id);
                $cond = "select count(id) as productCount from ".PRODUCT." where user_id=".$this->data['userProfileDetails']->row()->id;
                $sellingProductCount = $this->user_model->ExecuteQuery($cond);
                $cond = "select count(id) as affliateCount from ".USER_PRODUCTS." where user_id=".$this->data['userProfileDetails']->row()->id;
                $affliateProductCount = $this->user_model->ExecuteQuery($cond);
                $this->data['totalProductCount'] = $sellingProductCount->row()->productCount+$affliateProductCount->row()->affliateCount;
                //$str = $this->db->last_query();
                //echo $str; die;
                if ($this->data['listDetails']->num_rows() > 0) {
                    foreach ($this->data['listDetails']->result() as $listDetailsRow) {
                        $this->data['listImg'][$listDetailsRow->id] = '';
                        if ($listDetailsRow->product_id != '') {
                            $pidArr = array_filter(explode(',', $listDetailsRow->product_id));

                            $productDetails = '';
                            if (count($pidArr) > 0) {
                                foreach ($pidArr as $pidRow) {
                                    if ($pidRow != '') {
                                        $productDetails = $this->product_model->get_all_details(PRODUCT, array('seller_product_id' => $pidRow, 'status' => 'Publish'));
                                        if ($productDetails->num_rows() == 0) {
                                            $productDetails = $this->product_model->get_all_details(USER_PRODUCTS, array('seller_product_id' => $pidRow, 'status' => 'Publish'));
                                        }
                                        if ($productDetails->num_rows() == 1)
                                            break;
                                    }
                                }
                            }
                            if ($productDetails != '' && $productDetails->num_rows() == 1) {
                                $this->data['listImg'][$listDetailsRow->id] = $productDetails->row()->image;
                            }
                        }
                    }
                }
            //    echo'<pre>';print_r($this->data['listImg']);die;
                //print_r($this->data);die;
                $this->load->view('site/user/display_user_lists', $this->data);
            }
        } else {
            redirect(base_url());
        }
    }

    public function display_user_follow() {

        $username = urldecode($this->uri->segment(2, 0));
        $userProfileDetails = $this->user_model->get_all_details(USERS, array('user_name' => $username));
        //echo $this->db->last_query(); die;
        if ($userProfileDetails->num_rows() == 1) {
            if ($userProfileDetails->row()->visibility == 'Only you' && $userProfileDetails->row()->id != $this->checkLogin('U')) {
                $this->load->view('site/user/display_user_profile_private', $this->data);
            } else {
                if ($userProfileDetails->row()->full_name != '') {
                    $this->data['heading'] = $userProfileDetails->row()->full_name . ' - Following Lists';
                } else {
                    $this->data['heading'] = $username . ' - Following Lists';
                }
                $this->data['userProfileDetails'] = $userProfileDetails;

                $cond = "select count(id) as productCount from ".PRODUCT." where user_id=".$this->data['userProfileDetails']->row()->id;
                $sellingProductCount = $this->user_model->ExecuteQuery($cond);
                $cond = "select count(id) as affliateCount from ".USER_PRODUCTS." where user_id=".$this->data['userProfileDetails']->row()->id;
                $affliateProductCount = $this->user_model->ExecuteQuery($cond);
                $this->data['totalProductCount'] = $sellingProductCount->row()->productCount+$affliateProductCount->row()->affliateCount;

                
                $this->data['recentActivityDetails'] = $this->user_model->get_activity_details($userProfileDetails->row()->id);
                //	echo $this->db->last_query();[user_id] => 152
                $user_id = $this->data['recentActivityDetails']->result_array();

                $userid = $user_id[0]['user_id'];
                if ($userid == '') {
                    $userid = 0;
                }
                $this->data['listDetails'] = $this->product_model->view_follow_list($userid);
                //echo $this->db->last_query(); die;
                if ($this->data['listDetails']->num_rows() > 0) {
                    foreach ($this->data['listDetails']->result() as $listDetailsRow) {
                        $this->data['listImg'][$listDetailsRow->id] = '';
                        if ($listDetailsRow->product_id != '') {
                            $pidArr = array_filter(explode(',', $listDetailsRow->product_id));

                            $productDetails = '';
                            if (count($pidArr) > 0) {
                                foreach ($pidArr as $pidRow) {
                                    if ($pidRow != '') {
                                        $productDetails = $this->product_model->get_all_details(PRODUCT, array('seller_product_id' => $pidRow, 'status' => 'Publish'));
                                        if ($productDetails->num_rows() == 0) {
                                            $productDetails = $this->product_model->get_all_details(USER_PRODUCTS, array('seller_product_id' => $pidRow, 'status' => 'Publish'));
                                        }
                                        if ($productDetails->num_rows() == 1)
                                            break;
                                    }
                                }
                            }
                            if ($productDetails != '' && $productDetails->num_rows() == 1) {
                                $this->data['listImg'][$listDetailsRow->id] = $productDetails->row()->image;
                            }
                        }
                    }
                }
                $this->load->view('site/user/display_admin_follow', $this->data);
            }
        } else {
            redirect(base_url());
        }
    }

    public function display_user_wants() {
        $username = urldecode($this->uri->segment(2, 0));
        $userProfileDetails = $this->user_model->get_all_details(USERS, array('user_name' => $username));
        if ($userProfileDetails->num_rows() == 1) {
            if ($userProfileDetails->row()->visibility == 'Only you' && $userProfileDetails->row()->id != $this->checkLogin('U')) {
                $this->load->view('site/user/display_user_profile_private', $this->data);
            } else {
                if ($userProfileDetails->row()->full_name != '') {
                    $this->data['heading'] = $userProfileDetails->row()->full_name . ' - Wants';
                } else {
                    $this->data['heading'] = $username . ' - Wants';
                }
                $this->data['userProfileDetails'] = $userProfileDetails;

                $cond = "select count(id) as productCount from ".PRODUCT." where user_id=".$this->data['userProfileDetails']->row()->id;
                $sellingProductCount = $this->user_model->ExecuteQuery($cond);
                $cond = "select count(id) as affliateCount from ".USER_PRODUCTS." where user_id=".$this->data['userProfileDetails']->row()->id;
                $affliateProductCount = $this->user_model->ExecuteQuery($cond);
                $this->data['totalProductCount'] = $sellingProductCount->row()->productCount+$affliateProductCount->row()->affliateCount;


                $this->data['recentActivityDetails'] = $this->user_model->get_activity_details($userProfileDetails->row()->id);

                $wantList = $this->user_model->get_all_details(WANTS_DETAILS, array('user_id' => $userProfileDetails->row()->id));
                $this->data['wantProductDetails'] = $this->product_model->get_wants_product($wantList);
                $this->data['notSellProducts'] = $this->product_model->get_notsell_wants_product($wantList);
                $this->data['follow'] = $this->product_model->view_follow_list($userProfileDetails->row()->id);
                $this->load->view('site/user/display_user_wants', $this->data);
            }
        } else {
            redirect(base_url());
        }
    }

    public function display_user_owns() {
        $username = urldecode($this->uri->segment(2, 0));
        $userProfileDetails = $this->user_model->get_all_details(USERS, array('user_name' => $username));
        if ($userProfileDetails->num_rows() == 1) {
            if ($userProfileDetails->row()->visibility == 'Only you' && $userProfileDetails->row()->id != $this->checkLogin('U')) {
                $this->load->view('site/user/display_user_profile_private', $this->data);
            } else {
                if ($userProfileDetails->row()->full_name != '') {
                    $this->data['heading'] = $userProfileDetails->row()->full_name . ' - Owns';
                } else {
                    $this->data['heading'] = $username . ' - Owns';
                }
                $this->data['userProfileDetails'] = $userProfileDetails;
                $cond = "select count(id) as productCount from ".PRODUCT." where user_id=".$this->data['userProfileDetails']->row()->id;
                $sellingProductCount = $this->user_model->ExecuteQuery($cond);
                $cond = "select count(id) as affliateCount from ".USER_PRODUCTS." where user_id=".$this->data['userProfileDetails']->row()->id;
                $affliateProductCount = $this->user_model->ExecuteQuery($cond);
                $this->data['totalProductCount'] = $sellingProductCount->row()->productCount+$affliateProductCount->row()->affliateCount;


                $this->data['recentActivityDetails'] = $this->user_model->get_activity_details($userProfileDetails->row()->id);
                $this->data['follow'] = $this->product_model->view_follow_list($userProfileDetails->row()->id);

                $productIdsArr = array_filter(explode(',', $userProfileDetails->row()->own_products));
                $productIds = '';
                if (count($productIdsArr) > 0) {
                    foreach ($productIdsArr as $pidRow) {
                        if ($pidRow != '') {
                            $productIds .= $pidRow . ',';
                        }
                    }
                    $productIds = substr($productIds, 0, -1);
                }
                if ($productIds != '') {
                    $this->data['ownsProductDetails']=$ownsProductDetails = $this->product_model->view_product_details(' where p.seller_product_id in (' . $productIds . ') and p.status="Publish"');
                    $this->data['notSellProducts'] = $this->product_model->view_notsell_product_details(' where p.seller_product_id in (' . $productIds . ') and p.status="Publish"');
                } else {
                    $this->data['addedProductDetails'] = '';
                    $this->data['notSellProducts'] = '';
                }#echo '<pre>'; print_r($ownsProductDetails->result());die;
                $this->load->view('site/user/display_user_owns', $this->data);
            }
        } else {
            redirect(base_url());
        }
    }

    public function display_user_following() {
        $username = urldecode($this->uri->segment(2, 0));
        $userProfileDetails = $this->user_model->get_all_details(USERS, array('user_name' => $username));
        if ($userProfileDetails->num_rows() == 1) {
            if ($userProfileDetails->row()->visibility == 'Only you' && $userProfileDetails->row()->id != $this->checkLogin('U')) {
                $this->load->view('site/user/display_user_profile_private', $this->data);
            } else {
                if ($userProfileDetails->row()->full_name != '') {
                    $this->data['heading'] = $userProfileDetails->row()->full_name . ' - Following';
                } else {
                    $this->data['heading'] = $username . ' - Following';
                }
                $this->data['userProfileDetails'] = $userProfileDetails;
                $this->data['recentActivityDetails'] = $this->user_model->get_activity_details($userProfileDetails->row()->id);
                $fieldsArr = array('*');
                $searchName = 'id';
                $searchArr = explode(',', $userProfileDetails->row()->following);
                $joinArr = array();
                $sortArr = array();
                $limit = '';
                $this->data['followingUserDetails'] = $followingUserDetails = $this->product_model->get_fields_from_many(USERS, $fieldsArr, $searchName, $searchArr, $joinArr, $sortArr, $limit);
                if ($followingUserDetails->num_rows() > 0) {
                    foreach ($followingUserDetails->result() as $followingUserRow) {
                        $this->data['followingUserLikeDetails'][$followingUserRow->id] = $this->user_model->get_userlike_products($followingUserRow->id);
                    }
                }
                $this->load->view('site/user/display_user_following', $this->data);
            }
        } else {
            redirect(base_url());
        }
    }

    public function display_user_followers() {
        $username = urldecode($this->uri->segment(2, 0));
        $userProfileDetails = $this->user_model->get_all_details(USERS, array('user_name' => $username));
        if ($userProfileDetails->num_rows() == 1) {
            if ($userProfileDetails->row()->visibility == 'Only you' && $userProfileDetails->row()->id != $this->checkLogin('U')) {
                $this->load->view('site/user/display_user_profile_private', $this->data);
            } else {
                if ($userProfileDetails->row()->full_name != '') {
                    $this->data['heading'] = $userProfileDetails->row()->full_name . ' - Followers';
                } else {
                    $this->data['heading'] = $username . ' - Followers';
                }
                $this->data['userProfileDetails'] = $userProfileDetails;
                $this->data['recentActivityDetails'] = $this->user_model->get_activity_details($userProfileDetails->row()->id);
                $fieldsArr = array('*');
                $searchName = 'id';
                $searchArr = explode(',', $userProfileDetails->row()->followers);
                $joinArr = array();
                $sortArr = array();
                $limit = '';
                $this->data['followingUserDetails'] = $followingUserDetails = $this->product_model->get_fields_from_many(USERS, $fieldsArr, $searchName, $searchArr, $joinArr, $sortArr, $limit);
                if ($followingUserDetails->num_rows() > 0) {
                    foreach ($followingUserDetails->result() as $followingUserRow) {
                        $this->data['followingUserLikeDetails'][$followingUserRow->id] = $this->user_model->get_userlike_products($followingUserRow->id);
                    }
                }
                $this->load->view('site/user/display_user_followers', $this->data);
            }
        } else {
            redirect(base_url());
        }
    }

    /* This function triggers when like button clicked! */

    public function add_list_when_fancyy() {

        $returnStr['status_code'] = 0;
        $returnStr['listCnt'] = '';
        $returnStr['wanted'] = 0;
        $uniqueListNames = array();
        if ($this->checkLogin('U') == '') {
            if ($this->lang->line('login_requ') != '')
                $returnStr['message'] = $this->lang->line('login_requ');
            else
                $returnStr['message'] = 'Login required';
        }else {
            $tid = $this->input->post('tid');
            $firstCatName = '';
            $firstCatDetails = '';
            $count = 1;

            //Adding lists which was not already created from product categories
            $productDetails = $this->user_model->get_all_details(PRODUCT, array('seller_product_id' => $tid));
            if ($productDetails->num_rows() == 0) {
                $productDetails = $this->user_model->get_all_details(USER_PRODUCTS, array('seller_product_id' => $tid));
            }
            if ($productDetails->num_rows() == 1) {
                $productCatArr = explode(',', $productDetails->row()->category_id);
                if (count($productCatArr) > 0) {
                    $productCatNameArr = array();
                    foreach ($productCatArr as $productCatID) {
                        if ($productCatID != '') {
                            $productCatDetails = $this->user_model->get_all_details(CATEGORY, array('id' => $productCatID));
                            if ($productCatDetails->num_rows() == 1) {
                                if ($count == 1) {
                                    $firstCatName = $productCatDetails->row()->cat_name;
                                }
                                $listConditionArr = array('name' => $productCatDetails->row()->cat_name, 'user_id' => $this->checkLogin('U'));
                                $listCheck = $this->user_model->get_all_details(LISTS_DETAILS, $listConditionArr);
                                if ($count == 1) {
                                    $firstCatDetails = $listCheck;
                                }
                                if ($listCheck->num_rows() == 0) {
                                    $this->user_model->simple_insert(LISTS_DETAILS, $listConditionArr);
                                    $userDetails = $this->user_model->get_all_details(USERS, array('id' => $this->checkLogin('U')));
                                    $listCount = $userDetails->row()->lists;
                                    if ($listCount < 0 || $listCount == '') {
                                        $listCount = 0;
                                    }
                                    $listCount++;
                                    $this->user_model->update_details(USERS, array('lists' => $listCount), array('id' => $this->checkLogin('U')));
                                }
                                $count++;
                            }
                        }
                    }
                }
            }

            //Check the product id in list table
            $checkListsArr = $this->user_model->get_list_details($tid, $this->checkLogin('U'));

            if ($checkListsArr->num_rows() == 0) {

                //Add the product id under the first category name
                if ($firstCatName != '') {
                    $listConditionArr = array('name' => $firstCatName, 'user_id' => $this->checkLogin('U'));
                    if ($firstCatDetails == '' || $firstCatDetails->num_rows() == 0) {
                        $dataArr = array('product_id' => $tid);
                    } else {
                        $productRowArr = explode(',', $firstCatDetails->row()->product_id);
                        $productRowArr[] = $tid;
                        $newProductRowArr = implode(',', $productRowArr);
                        $dataArr = array('product_id' => $newProductRowArr);
                    }
                    $this->user_model->update_details(LISTS_DETAILS, $dataArr, $listConditionArr);
                    $listCntDetails = $this->user_model->get_all_details(LISTS_DETAILS, $listConditionArr);
                    if ($listCntDetails->num_rows() == 1) {
                        array_push($uniqueListNames, $listCntDetails->row()->id);
                        $returnStr['listCnt'] .= '<li class="selected"><label for="' . $listCntDetails->row()->id . '"><input type="checkbox" checked="checked" id="' . $listCntDetails->row()->id . '" name="' . $listCntDetails->row()->id . '">' . $listCntDetails->row()->name . '</label></li>';
                    }
                }
            } else {

                //Get all the lists which contain this product
                foreach ($checkListsArr->result() as $checkListsRow) {
                    array_push($uniqueListNames, $checkListsRow->id);
                    $returnStr['listCnt'] .= '<li class="selected"><label for="' . $checkListsRow->id . '"><input type="checkbox" checked="checked" id="' . $checkListsRow->id . '" name="' . $checkListsRow->id . '">' . $checkListsRow->name . '</label></li>';
                }
            }
            $all_lists = $this->user_model->get_all_details(LISTS_DETAILS, array('user_id' => $this->checkLogin('U')));
            if ($all_lists->num_rows() > 0) {
                foreach ($all_lists->result() as $all_lists_row) {
                    if (!in_array($all_lists_row->id, $uniqueListNames)) {
                        $returnStr['listCnt'] .= '<li><label for="' . $all_lists_row->id . '"><input type="checkbox" id="' . $all_lists_row->id . '" name="' . $all_lists_row->id . '">' . $all_lists_row->name . '</label></li>';
                    }
                }
            }

            //Check the product wanted status
            $wantedProducts = $this->user_model->get_all_details(WANTS_DETAILS, array('user_id' => $this->checkLogin('U')));
            if ($wantedProducts->num_rows() == 1) {
                $wantedProductsArr = explode(',', $wantedProducts->row()->product_id);
                if (in_array($tid, $wantedProductsArr)) {
                    $returnStr['wanted'] = 1;
                }
            }
            $returnStr['status_code'] = 1;
        }
        echo json_encode($returnStr);
    }


    public function add_item_to_lists() {
        $returnStr['status_code'] = 0;
        if ($this->checkLogin('U') == '') {
            if ($this->lang->line('u_must_login') != '')
                $returnStr['message'] = $this->lang->line('u_must_login');
            else
                $returnStr['message'] = 'You must login';
        }else {
            $tid = $this->input->post('tid');
            $lid = $this->input->post('list_ids');
            $listDetails = $this->user_model->get_all_details(LISTS_DETAILS, array('id' => $lid));
            if ($listDetails->num_rows() == 1) {
                $product_ids = explode(',', $listDetails->row()->product_id);
                if (!in_array($tid, $product_ids)) {
                    array_push($product_ids, $tid);
                }
                $new_product_ids = implode(',', $product_ids);
                $this->user_model->update_details(LISTS_DETAILS, array('product_id' => $new_product_ids), array('id' => $lid));
                $returnStr['status_code'] = 1;
            }
        }
        echo json_encode($returnStr);
    }

    public function remove_item_from_lists() {
        $returnStr['status_code'] = 0;
        if ($this->checkLogin('U') == '') {
            if ($this->lang->line('u_must_login') != '')
                $returnStr['message'] = $this->lang->line('u_must_login');
            else
                $returnStr['message'] = 'You must login';
        }else {
            $tid = $this->input->post('tid');
            $lid = $this->input->post('list_ids');
            $listDetails = $this->user_model->get_all_details(LISTS_DETAILS, array('id' => $lid));
            if ($listDetails->num_rows() == 1) {
                $product_ids = explode(',', $listDetails->row()->product_id);
                if (in_array($tid, $product_ids)) {
                    if (($key = array_search($tid, $product_ids)) !== false) {
                        unset($product_ids[$key]);
                    }
                }
                $new_product_ids = implode(',', $product_ids);
                $this->user_model->update_details(LISTS_DETAILS, array('product_id' => $new_product_ids), array('id' => $lid));
                $returnStr['status_code'] = 1;
            }
        }
        echo json_encode($returnStr);
    }

    public function add_want_tag() {
        $returnStr['status_code'] = 0;
        if ($this->checkLogin('U') == '') {
            if ($this->lang->line('u_must_login') != '')
                $returnStr['message'] = $this->lang->line('u_must_login');
            else
                $returnStr['message'] = 'You must login';
        }else {
            $tid = $this->input->post('thing_id');
            $wantDetails = $this->user_model->get_all_details(WANTS_DETAILS, array('user_id' => $this->checkLogin('U')));
            if ($wantDetails->num_rows() == 1) {
                $product_ids = explode(',', $wantDetails->row()->product_id);
                if (!in_array($tid, $product_ids)) {
                    array_push($product_ids, $tid);
                }
                $new_product_ids = implode(',', $product_ids);
                $this->user_model->update_details(WANTS_DETAILS, array('product_id' => $new_product_ids), array('user_id' => $this->checkLogin('U')));
            } else {
                $dataArr = array('user_id' => $this->checkLogin('U'), 'product_id' => $tid);
                $this->user_model->simple_insert(WANTS_DETAILS, $dataArr);
            }
            $wantCount = $this->data['userDetails']->row()->want_count;
            if ($wantCount <= 0 || $wantCount == '') {
                $wantCount = 0;
            }
            $wantCount++;
            $dataArr = array('want_count' => $wantCount);
            $ownProducts = explode(',', $this->data['userDetails']->row()->own_products);
            if (in_array($tid, $ownProducts)) {
                if (($key = array_search($tid, $ownProducts)) !== false) {
                    unset($ownProducts[$key]);
                }
                $ownCount = $this->data['userDetails']->row()->own_count;
                $ownCount--;
                $dataArr['own_count'] = $ownCount;
                $dataArr['own_products'] = implode(',', $ownProducts);
            }
            $this->user_model->update_details(USERS, $dataArr, array('id' => $this->checkLogin('U')));
            $returnStr['status_code'] = 1;
        }
        echo json_encode($returnStr);
    }

    public function delete_want_tag() {
        $returnStr['status_code'] = 0;
        if ($this->checkLogin('U') == '') {
            if ($this->lang->line('u_must_login') != '')
                $returnStr['message'] = $this->lang->line('u_must_login');
            else
                $returnStr['message'] = 'You must login';
        }else {
            $tid = $this->input->post('thing_id');
            $wantDetails = $this->user_model->get_all_details(WANTS_DETAILS, array('user_id' => $this->checkLogin('U')));
            if ($wantDetails->num_rows() == 1) {
                $product_ids = explode(',', $wantDetails->row()->product_id);
                if (in_array($tid, $product_ids)) {
                    if (($key = array_search($tid, $product_ids)) !== false) {
                        unset($product_ids[$key]);
                    }
                }
                $new_product_ids = implode(',', $product_ids);
                $this->user_model->update_details(WANTS_DETAILS, array('product_id' => $new_product_ids), array('user_id' => $this->checkLogin('U')));
                $wantCount = $this->data['userDetails']->row()->want_count;
                if ($wantCount <= 0 || $wantCount == '') {
                    $wantCount = 1;
                }
                $wantCount--;
                $this->user_model->update_details(USERS, array('want_count' => $wantCount), array('id' => $this->checkLogin('U')));
                $returnStr['status_code'] = 1;
            }
        }
        echo json_encode($returnStr);
    }

    public function create_list() {
        $returnStr['status_code'] = 0;
        if ($this->checkLogin('U') == '') {
            if ($this->lang->line('u_must_login') != '')
                $returnStr['message'] = $this->lang->line('u_must_login');
            else
                $returnStr['message'] = 'You must login';
        }else {
            $tid = $this->input->post('tid');
            $list_name = $this->input->post('list_name');
            $category_id = $this->input->post('category_id');
            $checkList = $this->user_model->get_all_details(LISTS_DETAILS, array('name' => $list_name, 'user_id' => $this->checkLogin('U')));
            if ($checkList->num_rows() == 0) {
                $dataArr = array('user_id' => $this->checkLogin('U'), 'name' => $list_name, 'product_id' => $tid);
                if ($category_id != '') {
                    $dataArr['category_id'] = $category_id;
                }
                $this->user_model->simple_insert(LISTS_DETAILS, $dataArr);
                $userDetails = $this->user_model->get_all_details(USERS, array('id' => $this->checkLogin('U')));
                $listCount = $userDetails->row()->lists;
                if ($listCount < 0 || $listCount == '') {
                    $listCount = 0;
                }
                $listCount++;
                $this->user_model->update_details(USERS, array('lists' => $listCount), array('id' => $this->checkLogin('U')));
                $returnStr['list_id'] = $this->user_model->get_last_insert_id();
                $returnStr['new_list'] = 1;
            } else {
                $productArr = explode(',', $checkList->row()->product_id);
                if (!in_array($tid, $productArr)) {
                    array_push($productArr, $tid);
                }
                $product_id = implode(',', $productArr);
                $dataArr = array('product_id' => $product_id);
                if ($category_id != '') {
                    $dataArr['category_id'] = $category_id;
                }
                $this->user_model->update_details(LISTS_DETAILS, $dataArr, array('user_id' => $this->checkLogin('U'), 'name' => $list_name));
                $returnStr['list_id'] = $checkList->row()->id;
                $returnStr['new_list'] = 0;
            }
            $returnStr['status_code'] = 1;
        }
        echo json_encode($returnStr);
    }

    public function search_users() {
        $search_key = $this->input->post('term');
        $returnStr = array();
        if ($search_key != '') {
            $userList = $this->user_model->get_search_user_list($search_key, $this->checkLogin('U'));
            if ($userList->num_rows() > 0) {
                $i = 0;
                foreach ($userList->result() as $userRow) {
                    $userArr['id'] = $userRow->id;
                    $userArr['fullname'] = $userRow->full_name;
                    $userArr['username'] = $userRow->user_name;
                    if ($userRow->thumbnail != '') {
                        $userArr['image_url'] = 'images/users/' . $userRow->thumbnail;
                    } else {
                        $userArr['image_url'] = 'images/users/user-thumb1.png';
                    }
                    array_push($returnStr, $userArr);
                    $i++;
                }
            }
        }
        echo json_encode($returnStr);
    }

    public function seller_signup_form() {
        if ($this->checkLogin('U') == '') {
            redirect(base_url());
        } else {
            if ($this->data['userDetails']->row()->is_verified == 'No') {
                if ($this->lang->line('cfm_mail_fst') != '')
                    $lg_err_msg = $this->lang->line('cfm_mail_fst');
                else
                    $lg_err_msg = 'Please confirm your email first';
                $this->setErrorMessage('error', $lg_err_msg);
                redirect(base_url());
            }else {
                
                if ($this->data['userDetails']->row()->request_status == 'Not Requested') {
                    $this->data['heading'] = 'Seller Signup';
                    $this->load->view('site/user/seller_register', $this->data);
                }else{
                    redirect();
                }
            
            }
        }
    }

    public function create_brand_form() {
        if ($this->checkLogin('U') == '') {
            redirect(base_url());
        } else {
                
                if ($this->data['userDetails']->row()->request_status == 'Not Requested') {
                    $this->data['heading'] = 'Seller Signup';
                    $this->load->view('site/user/seller_register', $this->data);
                }else{
                    redirect();
                }
        }
    }

    public function seller_signup() {
        if ($this->checkLogin('U') == '') {
            redirect(base_url());
        } else {
            if ($this->data['userDetails']->row()->is_verified == 'No') {
                if ($this->lang->line('cfm_mail_fst') != '')
                    $lg_err_msg = $this->lang->line('cfm_mail_fst');
                else
                    $lg_err_msg = 'Please confirm your email first';
                $this->setErrorMessage('error', $lg_err_msg);
                redirect('create-brand');
            }else {
                $dataArr = array(
                    'request_status' => 'Pending'
                );
                $this->user_model->commonInsertUpdate(USERS, 'update', array(), $dataArr, array('id' => $this->checkLogin('U')));
                if ($this->lang->line('sell_reg_succ_msg') != '')
                    $lg_err_msg = $this->lang->line('sell_reg_succ_msg');
                else
                    $lg_err_msg = 'Welcome onboard ! Our team is evaluating your request. We will contact you shortly';
                $this->setErrorMessage('success', $lg_err_msg);
                redirect(base_url());
            }
        }
    }

    public function view_purchase() {
        if ($this->checkLogin('U') == '') {
            show_404();
        } else {
            $uid = $this->uri->segment(2, 0);
            $dealCode = $this->uri->segment(3, 0);
            if ($uid != $this->checkLogin('U')) {
                show_404();
            } else {
                $purchaseList = $this->user_model->get_purchase_list($uid, $dealCode);
                $invoice = $this->get_invoice($purchaseList);
                
                echo $invoice;
            }
        }
    }

    public function view_order() {
        if ($this->checkLogin('U') == '') {
            show_404();
        } else {
            $uid = $this->uri->segment(2, 0);
            $dealCode = $this->uri->segment(3, 0);
            if ($uid != $this->checkLogin('U')) {
                show_404();
            } else {
                $orderList = $this->user_model->get_order_list($uid, $dealCode);

                $invoice = $this->get_invoice($orderList);
                echo $invoice;
            }
        }
    }

    public function get_invoice($PrdList) {
        $shipAddRess = $this->user_model->get_all_details(SHIPPING_ADDRESS, array('id' => $PrdList->row()->shippingid));

        $newsid = '19';
        $template_values = $this->product_model->get_newsletter_template_details($newsid);
        $adminnewstemplateArr = array(
            'logo' => $this->data['logo'],
            'meta_title' => $this->config->item('meta_title'),
            'ship_fullname' => stripslashes($shipAddRess->row()->full_name),
            'ship_address1' => stripslashes($shipAddRess->row()->address1),
            'ship_address2' => stripslashes($shipAddRess->row()->address2),
            'ship_city' => stripslashes($shipAddRess->row()->city),
            'ship_country' => stripslashes($shipAddRess->row()->country),
            'ship_state' => stripslashes($shipAddRess->row()->state),
            'ship_postalcode' => stripslashes($shipAddRess->row()->postal_code),
            'ship_phone' => stripslashes($shipAddRess->row()->phone),
            'bill_fullname' => stripslashes($PrdList->row()->full_name),
            'bill_address1' => stripslashes($PrdList->row()->address),
            'bill_address2' => stripslashes($PrdList->row()->address2),
            'bill_city' => stripslashes($PrdList->row()->city),
            'bill_country' => stripslashes($PrdList->row()->country),
            'bill_state' => stripslashes($PrdList->row()->state),
            'bill_postalcode' => stripslashes($PrdList->row()->postal_code),
            'bill_phone' => stripslashes($PrdList->row()->phone_no),
            'invoice_number' => $PrdList->row()->dealCodeNumber,
            'payment_type' => $PrdList->row()->payment_type,
            'invoice_date' => date("F j, Y g:i a", strtotime($PrdList->row()->created))
        );

        extract($adminnewstemplateArr);
        $subject = $template_values['news_subject'];
        $message1 .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width"/></head>
<title>' . $template_values['news_subject'] . '</title>
<body>

';

        include('./newsletter/registeration' . $newsid . '.php');


        $message = stripslashes(substr($message, 0, strrpos($message, '</tbody>')));
        $message = stripslashes(substr($message, 0, strrpos($message, '</tbody>')));
        $message1 .= $message;
        $disTotal = 0;
        $grantTotal = 0;
		//echo '<pre>';print_r($PrdList->result());
        foreach ($PrdList->result() as $cartRow) {
            if ($cartRow->image != '') {
                $InvImg = @explode(',', $cartRow->image);
            } else {
                $InvImg = @explode(',', $cartRow->old_image);
            }
			if ($cartRow->type == 'cart') {
				$unitPrice = ($cartRow->price * (0.01 * $cartRow->product_tax_cost)) + $cartRow->product_shipping_cost + $cartRow->price;
            }else if($cartRow->type == 'auction'){
				$unitPrice = ($cartRow->price * (0.01 * $cartRow->product_tax_cost)) + $cartRow->price;
			}else{
					$unitPrice = ($cartRow->price * (0.01 * $cartRow->product_tax_cost)) + $cartRow->product_shipping_cost + $cartRow->price;
			}	
			//echo $unitPrice;die;
			$uTot = $unitPrice * $cartRow->quantity;
            if ($cartRow->attr_name != '' || $cartRow->attr_type) {
                $atr = '<br>' . $cartRow->attr_type . ' / ' . $cartRow->attr_name;
            } else if ($cartRow->old_attr_name != '') {
                $atr = '<br>' . $cartRow->old_attr_name;
            } else {
                $atr = '';
            }
            $product_name = $cartRow->product_name;
            if ($product_name == '') {
                $product_name = $cartRow->old_product_name;
            }
            if ($cartRow->product_type == 'physical') {
                $dwnldcode = 'Not applicable';
            } else {

                $dwnldcode = 'Copy & paste this code to download product: ' . $cartRow->product_download_code;
            }
            $message1.='<tr>
            <td style="border-right:1px solid #cecece; text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;"><img src="' . base_url() . PRODUCTPATH . $InvImg[0] . '" alt="' . stripslashes($cartRow->product_name) . '" width="70" /></span></td>
			<td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">' . $product_name . $atr . '</span></td>
			<td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">' . $dwnldcode . '</span></td>
            <td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">' . strtoupper($cartRow->quantity) . '</span></td>
            <td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">' . $this->data['currencySymbol'] . number_format($unitPrice, 2, '.', '') . '</span></td>
            <td style="text-align:center;border-top:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">' . $this->data['currencySymbol'] . number_format($uTot, 2, '.', '') . '</span></td>
        </tr>';
		
            $grantTotal = $grantTotal + $uTot;
        }
		
        $private_total = $grantTotal - $PrdList->row()->discountAmount;
		if ($cartRow->type == 'cart') {
			$private_total = $private_total + $PrdList->row()->tax + $PrdList->row()->shippingcost;
		}else if($cartRow->type == 'auction'){
			$private_total = $private_total;
		}
        
        $message1.='</table></td> </tr><tr><td colspan="3"><table border="0" cellspacing="0" cellpadding="0" style=" margin:10px 0px; width:99.5%;"><tr>
			<td width="460" valign="top" >';
        if ($PrdList->row()->note != '') {
            $message1.='<table width="97%" border="0"  cellspacing="0" cellpadding="0"><tr>
                <td width="87" ><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:left; width:100%; font-weight:bold; color:#000000; line-height:38px; float:left;">Note:</span></td>

            </tr>
			<tr>
                <td width="87"  style="border:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:left; width:97%; color:#000000; line-height:24px; float:left; margin:10px;">' . stripslashes($PrdList->row()->note) . '</span></td>
            </tr></table>';
        }

        if ($PrdList->row()->order_gift == 1) {
            $message1.='<table width="97%" border="0"  cellspacing="0" cellpadding="0"  style="margin-top:10px;"><tr>
                <td width="87"  style="border:1px solid #cecece;"><span style="font-size:16px; font-weight:bold; font-family:Arial, Helvetica, sans-serif; text-align:center; width:97%; color:#000000; line-height:24px; float:left; margin:10px;">This Order is a gift</span></td>
            </tr></table>';
        }
		$shipping_cost = '0.00';
		if($PrdList->row()->shippingcost != ''){
			$shipping_cost = number_format($PrdList->row()->shippingcost, 2, '.', '');
		}
		
		$disc_amt = '0.00';
		if($PrdList->row()->discountAmount != ''){
			$disc_amt = number_format($PrdList->row()->discountAmount, '2', '.', '');
		}
		
        $message1.='</td>
            <td width="174" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #cecece;">
            <tr bgcolor="#f3f3f3">
                <td width="87"  style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:center; width:100%; font-weight:bold; color:#000000; line-height:38px; float:left;">Sub Total</span></td>
                <td  style="border-bottom:1px solid #cecece;" width="69"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%; float:left;">' . $this->data['currencySymbol'] . number_format($grantTotal, '2', '.', '') . '</span></td>
            </tr>
			<tr>
                <td width="87"  style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; text-align:center; width:100%; font-weight:bold; color:#000000; line-height:38px; float:left;">Discount Amount</span></td>
                <td  style="border-bottom:1px solid #cecece;" width="69"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%; float:left;">' . $this->data['currencySymbol'] . $disc_amt . '</span></td>
            </tr>
		<tr bgcolor="#f3f3f3">
            <td width="31" style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; text-align:center; width:100%; color:#000000; line-height:38px; float:left;">Shipping Cost</span></td>
                <td  style="border-bottom:1px solid #cecece;" width="69"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%;  float:left;">' . $this->data['currencySymbol'] . $shipping_cost . '</span></td>
              </tr>
			  <tr>
            <td width="31" style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; text-align:center; width:100%; color:#000000; line-height:38px; float:left;">Shipping Tax</span></td>
                <td  style="border-bottom:1px solid #cecece;" width="69"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%;  float:left;">' . $this->data['currencySymbol'] . number_format($PrdList->row()->tax, 2, '.', '') . '</span></td>
              </tr>
			  <tr bgcolor="#f3f3f3">
                <td width="87" style="border-right:1px solid #cecece;"><span style="font-size:13px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000000; line-height:38px; text-align:center; width:100%; float:left;">Grand Total</span></td>
                <td width="31"><span style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:38px; text-align:center; width:100%;  float:left;">' . $this->data['currencySymbol'] . number_format($private_total, '2', '.', '') . '</span></td>
              </tr>
            </table></td>
            </tr>
        </table></td>
        </tr>
    </table>
        </div>

        <!--end of left-->


            <div style="width:27.4%; margin-right:5px; float:right;">


            </div>

        <div style="clear:both"></div>

    </div>
    </div></body></html>';
        return $message1;
    }

    public function change_order_status() {
        if ($this->checkLogin('U') == '') {
            show_404();
        } else {
            $uid = $this->input->post('seller');

            if ($uid != $this->checkLogin('U')) {
                show_404();
            } else {
                $returnStr['status_code'] = 0;
                $dealCode = $this->input->post('dealCode');
                $status = $this->input->post('value');
                $dataArr = array('shipping_status' => $status);
                $conditionArr = array('dealCodeNumber' => $dealCode, 'sell_id' => $uid);
				$this->user_model->update_details(PAYMENT, $dataArr, $conditionArr);
				if($status =='Delivered'){
					$orderDetails = $this->user_model->get_all_details(PAYMENT, array('dealCodeNumber'=>$dealCode));
					$userId = $orderDetails->first_row()->user_id;
					$sellerId = $orderDetails->first_row()->sell_id;
					$user = $this->user_model->get_all_details(USERS, array('id'=>$userId));
					$seller = $this->user_model->get_all_details(USERS, array('id'=>$sellerId));
					$link = base_url().'order-review/'.$userId.'/'.$sellerId.'/'.$dealCode;
					$newsid = '36';
					$template_values = $this->product_model->get_newsletter_template_details($newsid);
					$adminnewstemplateArr = array('logo' => $this->data['logo'], 'meta_title' => $this->config->item('meta_title'), 'full_name' => $user->row()->user_name, 'link'=> $link);
					extract($adminnewstemplateArr);
					$subject = 'From: ' . $this->config->item('email_title') . ' - ' . $template_values['news_subject'];
					$message .= '<!DOCTYPE HTML>
					<html>
					<head>
					<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
					<meta name="viewport" content="width=device-width"/>
					<title>' . $template_values['news_subject'] . '</title><body>';
					include('./newsletter/registeration' . $newsid . '.php');
					$message .= '</body>
					</html>';
					if($template_values['sender_name'] == '' && $template_values['sender_email'] == '') {
						$sender_email = $this->data['siteContactMail'];
						$sender_name = $this->data['siteTitle'];
					}else{
						$sender_name = $template_values['sender_name'];
						$sender_email = $template_values['sender_email'];
					}
					$email_values = array('mail_type' => 'html',
						'from_mail_id' => $seller->row()->email,
						'mail_name' => $seller->row()->user_name,
						'to_mail_id' => $user->row()->email,
						'subject_message' => $subject,
						'body_messages' => $message
					);
					$email_send_to_common = $this->product_model->common_email_send($email_values);
				}
                $returnStr['status_code'] = 1;
                echo json_encode($returnStr);
            }
        }
    }

    public function display_user_lists_home() {

        $lid = $this->uri->segment('4', '0');
        $uname = $this->uri->segment('2', '0');
        $this->data['user_profile_details'] = $userProfileDetails = $this->user_model->get_all_details(USERS, array('user_name' => $uname));
        if ($userProfileDetails->row()->visibility == 'Only you' && $userProfileDetails->row()->id != $this->checkLogin('U')) {
            $this->load->view('site/user/display_user_profile_private', $this->data);
        } else {
            $this->data['list_details'] = $list_details = $this->product_model->get_all_details(LISTS_DETAILS, array('id' => $lid, 'user_id' => $this->data['user_profile_details']->row()->id));
            if ($this->data['list_details']->num_rows() == 0) {
                show_404();
            } else {
                if ($userProfileDetails->row()->full_name == '') {
                    $this->data['heading'] = $uname . ' - List';
                } else {
                    $this->data['heading'] = $userProfileDetails->row()->full_name . ' - List';
                }
                $searchArr = array_filter(explode(',', $list_details->row()->product_id));
                if (count($searchArr) > 0) {
                    $fieldsArr = array(PRODUCT . '.*', USERS . '.user_name', USERS . '.full_name');
                    $condition = array(PRODUCT . '.status' => 'Publish');
                    $joinArr1 = array('table' => USERS, 'on' => USERS . '.id=' . PRODUCT . '.user_id', 'type' => '');
                    $joinArr = array($joinArr1);
                    $this->data['product_details'] = $product_details = $this->product_model->get_fields_from_many(PRODUCT, $fieldsArr, PRODUCT . '.seller_product_id', $searchArr, $joinArr, '', '', $condition);
                    $this->data['totalProducts'] = count($searchArr);
                    $fieldsArr = array(USER_PRODUCTS . '.*', USERS . '.user_name', USERS . '.full_name');
                    $condition = array(USER_PRODUCTS . '.status' => 'Publish');
                    $joinArr1 = array('table' => USERS, 'on' => USERS . '.id=' . USER_PRODUCTS . '.user_id', 'type' => '');
                    $joinArr = array($joinArr1);
                    $this->data['notsell_product_details'] = $this->product_model->get_fields_from_many(USER_PRODUCTS, $fieldsArr, USER_PRODUCTS . '.seller_product_id', $searchArr, $joinArr, '', '', $condition);
                } else {
                    $this->data['notsell_product_details'] = '';
                    $this->data['product_details'] = '';
                    $this->data['totalProducts'] = 0;
                }
                $this->load->view('site/user/user_list_home', $this->data);
            }
        }
    }

    public function display_user_lists_followers() {
        $lid = $this->uri->segment('4', '0');
        $uname = $this->uri->segment('2', '0');
        $this->data['user_profile_details'] = $userProfileDetails = $this->user_model->get_all_details(USERS, array('user_name' => $uname));
        if ($userProfileDetails->row()->visibility == 'Only you' && $userProfileDetails->row()->id != $this->checkLogin('U')) {
            $this->load->view('site/user/display_user_profile_private', $this->data);
        } else {
            $this->data['list_details'] = $list_details = $this->product_model->get_all_details(LISTS_DETAILS, array('id' => $lid, 'user_id' => $this->data['user_profile_details']->row()->id));
            if ($this->data['list_details']->num_rows() == 0) {
                show_404();
            } else {
                if ($userProfileDetails->row()->full_name == '') {
                    $this->data['heading'] = $uname . ' - List';
                } else {
                    $this->data['heading'] = $userProfileDetails->row()->full_name . ' - List';
                }
                $fieldsArr = '*';
                $searchArr = explode(',', $list_details->row()->followers);
                $this->data['user_details'] = $user_details = $this->product_model->get_fields_from_many(USERS, $fieldsArr, 'id', $searchArr);
                if ($user_details->num_rows() > 0) {
                    foreach ($user_details->result() as $userRow) {
                        $fieldsArr = array(PRODUCT_LIKES . '.*', PRODUCT . '.product_name', PRODUCT . '.image', PRODUCT . '.id as PID');
                        $searchArr = array($userRow->id);
                        $joinArr1 = array('table' => PRODUCT, 'on' => PRODUCT_LIKES . '.product_id=' . PRODUCT . '.seller_product_id', 'type' => '');
                        $joinArr = array($joinArr1);
                        $sortArr1 = array('field' => PRODUCT . '.created', 'type' => 'desc');
                        $sortArr = array($sortArr1);
                        $this->data['product_details'][$userRow->id] = $this->product_model->get_fields_from_many(PRODUCT_LIKES, $fieldsArr, PRODUCT_LIKES . '.user_id', $searchArr, $joinArr, $sortArr, '5');
                    }
                }
                $fieldsArr = array(PRODUCT . '.*', USERS . '.user_name', USERS . '.full_name');
                $searchArr = array_filter(explode(',', $list_details->row()->product_id));
                if (count($searchArr) > 0) {
                    $this->data['totalProducts'] = count($searchArr);
                } else {
                    $this->data['totalProducts'] = 0;
                }

                $this->load->view('site/user/user_list_followers', $this->data);
            }
        }
    }

    public function follow_list() {
        $returnStr['status_code'] = 0;
        $lid = $this->input->post('lid');
        if ($this->checkLogin('U') != '') {
            $listDetails = $this->product_model->get_all_details(LISTS_DETAILS, array('id' => $lid));
            $followersArr = explode(',', $listDetails->row()->followers);
            $followersCount = $listDetails->row()->followers_count;
            $oldDetails = explode(',', $this->data['userDetails']->row()->following_user_lists);
            if (!in_array($lid, $oldDetails)) {
                array_push($oldDetails, $lid);
            }
            if (!in_array($this->checkLogin('U'), $followersArr)) {
                array_push($followersArr, $this->checkLogin('U'));
                $followersCount++;
            }
            $this->product_model->update_details(USERS, array('following_user_lists' => implode(',', $oldDetails)), array('id' => $this->checkLogin('U')));
            $this->product_model->update_details(LISTS_DETAILS, array('followers' => implode(',', $followersArr), 'followers_count' => $followersCount), array('id' => $lid));
            $returnStr['status_code'] = 1;
        }
        echo json_encode($returnStr);
    }

    public function unfollow_list() {
        $returnStr['status_code'] = 0;
        $lid = $this->input->post('lid');
        if ($this->checkLogin('U') != '') {
            $listDetails = $this->product_model->get_all_details(LISTS_DETAILS, array('id' => $lid));
            $followersArr = explode(',', $listDetails->row()->followers);
            $followersCount = $listDetails->row()->followers_count;
            $oldDetails = explode(',', $this->data['userDetails']->row()->following_user_lists);
            if (in_array($lid, $oldDetails)) {
                if ($key = array_search($lid, $oldDetails) !== false) {
                    unset($oldDetails[$key]);
                }
            }
            if (in_array($this->checkLogin('U'), $followersArr)) {
                if ($key = array_search($this->checkLogin('U'), $followersArr) !== false) {
                    unset($followersArr[$key]);
                }
                $followersCount--;
            }
            $this->product_model->update_details(USERS, array('following_user_lists' => implode(',', $oldDetails)), array('id' => $this->checkLogin('U')));
            $this->product_model->update_details(LISTS_DETAILS, array('followers' => implode(',', $followersArr), 'followers_count' => $followersCount), array('id' => $lid));
            $returnStr['status_code'] = 1;
        }
        echo json_encode($returnStr);
    }

    public function edit_user_lists() {
        if ($this->checkLogin('U') == '') {
            redirect('login');
        } else {
            $lid = $this->uri->segment('4', '0');
            $uname = $this->uri->segment('2', '0');
            if ($uname != $this->data['userDetails']->row()->user_name) {
                show_404();
            } else {
                $this->data['user_profile_details'] = $this->user_model->get_all_details(USERS, array('user_name' => $uname));
                $this->data['list_details'] = $list_details = $this->product_model->get_all_details(LISTS_DETAILS, array('id' => $lid, 'user_id' => $this->data['user_profile_details']->row()->id));
                if ($this->data['list_details']->num_rows() == 0) {
                    show_404();
                } else {
                    $this->data['list_category_details'] = $this->user_model->get_all_details(CATEGORY, array('id' => $this->data['list_details']->row()->category_id));
                    $this->data['heading'] = 'Edit List';
                    $this->load->view('site/user/edit_user_list', $this->data);
                }
            }
        }
    }

    public function edit_user_list_details() {
        if ($this->checkLogin('U') == '') {
            redirect('login');
        } else {
            $lid = $this->input->post('lid');
            $uid = $this->input->post('uid');
            if ($uid != $this->checkLogin('U')) {
                show_404();
            } else {
                $list_title = $this->input->post('setting-title');
                $catID = $this->input->post('category');
                $duplicateCheck = $this->user_model->get_all_details(LISTS_DETAILS, array('user_id' => $uid, 'id !=' => $lid, 'name' => $list_title));
                if ($duplicateCheck->num_rows() > 0) {
                    if ($this->lang->line('list_tit_exist') != '')
                        $lg_err_msg = $this->lang->line('list_tit_exist');
                    else
                        $lg_err_msg = 'List title already exists';
                    $this->setErrorMessage('error', $lg_err_msg);
                    echo '<script>window.history.go(-1);</script>';
                }else {
                    if ($catID == '') {
                        $catID = 0;
                    }
                    $this->user_model->update_details(LISTS_DETAILS, array('name' => $list_title, 'category_id' => $catID), array('id' => $lid, 'user_id' => $uid));
                    if ($this->lang->line('list_updat_succ') != '')
                        $lg_err_msg = $this->lang->line('list_updat_succ');
                    else
                        $lg_err_msg = 'List updated successfully';
                    $this->setErrorMessage('success', $lg_err_msg);
                    echo '<script>window.history.go(-1);</script>';
                }
            }
        }
    }

    public function delete_user_list() {
        $returnStr['status_code'] = 0;
        if ($this->checkLogin('U') == '') {
            if ($this->lang->line('login_requ') != '')
                $returnStr['message'] = $this->lang->line('login_requ');
            else
                $returnStr['message'] = 'Login required';
        }else {
            $lid = $this->input->post('lid');
            $uid = $this->input->post('uid');
            if ($uid != $this->checkLogin('U')) {
                if ($this->lang->line('u_cant_del_othr_lst') != '')
                    $returnStr['message'] = $this->lang->line('u_cant_del_othr_lst');
                else
                    $returnStr['message'] = 'You can\'t delete other\'s list';
            }else {
                $list_details = $this->user_model->get_all_details(LISTS_DETAILS, array('id' => $lid, 'user_id' => $uid));
                if ($list_details->num_rows() == 1) {
                    $followers_id = $list_details->row()->followers;
                    if ($followers_id != '') {
                        $searchArr = array_filter(explode(',', $followers_id));
                        $fieldsArr = array('following_user_lists', 'id');
                        $followersArr = $this->user_model->get_fields_from_many(USERS, $fieldsArr, 'id', $searchArr);
                        if ($followersArr->num_rows() > 0) {
                            foreach ($followersArr->result() as $followersRow) {
                                $listArr = array_filter(explode(',', $followersRow->following_user_lists));
                                if (in_array($lid, $listArr)) {
                                    if (($key = array_search($lid, $listArr)) != false) {
                                        unset($listArr[$key]);
                                        $this->user_model->update_details(USERS, array('following_user_lists' => implode(',', $listArr)), array('id' => $followersRow->id));
                                    }
                                }
                            }
                        }
                    }
                    $this->user_model->commonDelete(LISTS_DETAILS, array('id' => $lid, 'user_id' => $this->checkLogin('U')));
                    $listCount = $this->data['userDetails']->row()->lists;
                    $listCount--;
                    if ($listCount == '' || $listCount < 0) {
                        $listCount = 0;
                    }
                    $this->user_model->update_details(USERS, array('lists' => $listCount), array('id' => $this->checkLogin('U')));
                    $returnStr['url'] = base_url() . 'user/' . $this->data['userDetails']->row()->user_name . '/lists';
                    if ($this->lang->line('list_del_succ') != '')
                        $lg_err_msg = $this->lang->line('list_del_succ');
                    else
                        $lg_err_msg = 'List deleted successfully';
                    $this->setErrorMessage('success', $lg_err_msg);
                    $returnStr['status_code'] = 1;
                }else {
                    if ($this->lang->line('lst_not_avail') != '')
                        $returnStr['message'] = $this->lang->line('lst_not_avail');
                    else
                        $returnStr['message'] = 'List not available';
                }
            }
        }
        echo json_encode($returnStr);
    }

    public function image_crop() {
        if ($this->checkLogin('U') == '') {
            redirect('login');
        } else {
            $uid = $this->uri->segment(2, 0);
            if ($uid != $this->checkLogin('U')) {
                show_404();
            } else {
                $this->data['heading'] = 'Cropping Image';
                $this->load->view('site/user/crop_image', $this->data);
            }
        }
    }

    public function image_crop_process() {
        if ($this->checkLogin('U') == '') {
            redirect('login');
        } else {


// 			print_r($_FILES);
// 			print_r($_POST);
// 			die;

            $targ_w = $targ_h = 240;
            $jpeg_quality = 90;

            $src = 'images/users/' . $this->data['userDetails']->row()->thumbnail;
            $ext = substr($src, strpos($src, '.') + 1);
            if ($ext == 'png') {
                $jpgImg = imagecreatefrompng($src);
                imagejpeg($jpgImg, $src, 90);
            }
            $img_r = imagecreatefromjpeg($src);
            $dst_r = ImageCreateTrueColor($targ_w, $targ_h);

            //			list($width, $height) = getimagesize($src);

            imagecopyresampled($dst_r, $img_r, 0, 0, $_POST['x1'], $_POST['y1'], $targ_w, $targ_h, $_POST['w'], $_POST['h']);
            //		imagecopyresized($dst_r,$img_r,0,0,$_POST['x1'],$_POST['y1'],	$targ_w,$targ_h,$_POST['w'],$_POST['h']);
            //		imagecopyresized($dst_r, $img_r,0,0, $_POST['x1'],$_POST['y1'], $_POST['x2'],$_POST['y2'],1024,980);
            //			header('Content-type: image/jpeg');
            imagejpeg($dst_r, 'images/users/' . $this->data['userDetails']->row()->thumbnail);
            if ($this->lang->line('prof_photo_change_succ') != '')
                $lg_err_msg = $this->lang->line('prof_photo_change_succ');
            else
                $lg_err_msg = 'Profile photo changed successfully';
            $this->setErrorMessage('success', $lg_err_msg);
            redirect('user/' . $this->data['userDetails']->row()->user_name);
            exit;
        }
    }

    public function send_noty_mail($followUserDetails = array()) {
        if (count($followUserDetails) > 0) {
            $emailNoty = explode(',', $followUserDetails[0]['email_notifications']);
            if (in_array('following', $emailNoty)) {
                $newsid = '7';
                $template_values = $this->product_model->get_newsletter_template_details($newsid);
                $adminnewstemplateArr = array('logo' => $this->data['logo'], 'meta_title' => $this->config->item('meta_title'), 'full_name' => $followUserDetails[0]['full_name'], 'cfull_name' => $this->data['userDetails']->row()->full_name, 'user_name' => $this->data['userDetails']->row()->user_name);
                extract($adminnewstemplateArr);
                $subject = 'From: ' . $this->config->item('email_title') . ' - ' . $template_values['news_subject'];
                $message .= '<!DOCTYPE HTML>
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
			<meta name="viewport" content="width=device-width"/>
			<title>' . $template_values['news_subject'] . '</title><body>';
                include('./newsletter/registeration' . $newsid . '.php');

                $message .= '</body>
			</html>';


                if ($template_values['sender_name'] == '' && $template_values['sender_email'] == '') {
                    $sender_email = $this->data['siteContactMail'];
                    $sender_name = $this->data['siteTitle'];
                } else {
                    $sender_name = $template_values['sender_name'];
                    $sender_email = $template_values['sender_email'];
                }

                $email_values = array('mail_type' => 'html',
                    'from_mail_id' => $sender_email,
                    'mail_name' => $sender_name,
                    'to_mail_id' => $followUserDetails[0]['email'],
                    'subject_message' => $subject,
                    'body_messages' => $message
                );
                $email_send_to_common = $this->product_model->common_email_send($email_values);
            }
        }
    }

    public function send_noty_mails($followUserDetails = array()) {
        if (count($followUserDetails) > 0) {
            $emailNoty = explode(',', $followUserDetails->email_notifications);
            if (in_array('following', $emailNoty)) {

                $newsid = '7';
                $template_values = $this->product_model->get_newsletter_template_details($newsid);
                $adminnewstemplateArr = array('logo' => $this->data['logo'], 'meta_title' => $this->config->item('meta_title'), 'full_name' => $followUserDetails[0]['full_name'], 'cfull_name' => $this->data['userDetails']->row()->full_name, 'user_name' => $this->data['userDetails']->row()->user_name);
                extract($adminnewstemplateArr);
                $subject = 'From: ' . $this->config->item('email_title') . ' - ' . $template_values['news_subject'];
                $message .= '<!DOCTYPE HTML>
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
			<meta name="viewport" content="width=device-width"/>
			<title>' . $template_values['news_subject'] . '</title><body>';
                include('./newsletter/registeration' . $newsid . '.php');

                $message .= '</body>
			</html>';

                if ($template_values['sender_name'] == '' && $template_values['sender_email'] == '') {
                    $sender_email = $this->data['siteContactMail'];
                    $sender_name = $this->data['siteTitle'];
                } else {
                    $sender_name = $template_values['sender_name'];
                    $sender_email = $template_values['sender_email'];
                }

                $email_values = array('mail_type' => 'html',
                    'from_mail_id' => $sender_email,
                    'mail_name' => $sender_name,
                    'to_mail_id' => $followUserDetails->email,
                    'subject_message' => $subject,
                    'body_messages' => $message
                );
                $email_send_to_common = $this->product_model->common_email_send($email_values);
            }
        }
    }

    public function order_review() {

        if ($this->checkLogin('U') == '') {
            show_404();
        } else {
            $uid = $this->uri->segment(2, 0);
            $sid = $this->uri->segment(3, 0);
            $dealCode = $this->uri->segment(4, 0);
            if ($uid == $this->checkLogin('U')) {
                $view_mode = 'user';
            } else if ($sid == $this->checkLogin('U')) {
                $view_mode = 'seller';
            } else {
                $view_mode = '';
            }
            if ($view_mode == '') {
                show_404();
            } else {
                if ($view_mode == 'seller') {
                    $this->db->select('p.*,pAr.attr_name as attr_type,sp.attr_name');
                    $this->db->from(PAYMENT . ' as p');
                    $this->db->join(SUBPRODUCT . ' as sp', 'sp.pid = p.attribute_values', 'left');
                    $this->db->join(PRODUCT_ATTRIBUTE . ' as pAr', 'pAr.id = sp.attr_id', 'left');
                    $this->db->where('p.sell_id = "' . $sid . '" and p.status = "Paid" and p.dealCodeNumber = "' . $dealCode . '"');
                    $order_details = $this->db->get();
                    //$order_details = $this->user_model->get_all_details(PAYMENT,array('dealCodeNumber'=>$dealCode,'status'=>'Paid','sell_id'=>$sid));
                } else {
                    //$order_details = $this->user_model->get_all_details(PAYMENT,array('dealCodeNumber'=>$dealCode,'status'=>'Paid'));
                    $this->db->select('p.*,pAr.attr_name as attr_type,sp.attr_name');
                    $this->db->from(PAYMENT . ' as p');
                    $this->db->join(SUBPRODUCT . ' as sp', 'sp.pid = p.attribute_values', 'left');
                    $this->db->join(PRODUCT_ATTRIBUTE . ' as pAr', 'pAr.id = sp.attr_id', 'left');
                    $this->db->where("p.status = 'Paid' and p.dealCodeNumber = '" . $dealCode . "'");
                    $order_details = $this->db->get();
                }
                if ($order_details->num_rows() == 0) {
                    show_404();
                } else {
                    if ($view_mode == 'user') {
                        $this->data['user_details'] = $this->data['userDetails'];
                        $this->data['seller_details'] = $this->user_model->get_all_details(USERS, array('id' => $sid));
                    } elseif ($view_mode == 'seller') {
                        $this->data['user_details'] = $this->user_model->get_all_details(USERS, array('id' => $uid));
                        $this->data['seller_details'] = $this->data['userDetails'];
                    }
                    foreach ($order_details->result() as $order_details_row) {
                        $this->data['prod_details'][$order_details_row->product_id] = $this->user_model->get_all_details(PRODUCT, array('id' => $order_details_row->product_id));
                    }
                    $this->data['view_mode'] = $view_mode;
                    $this->data['order_details'] = $order_details;
                    $sortArr1 = array('field' => 'date', 'type' => 'desc');
                    $sortArr = array($sortArr1);
                    $this->data['order_comments'] = $this->user_model->get_all_details(REVIEW_COMMENTS, array('deal_code' => $dealCode), $sortArr);
                    $this->load->view('site/user/display_order_reviews', $this->data);
                }
            }
        }
    }

    /*     * ******* Coding for display add feedback form for user product ******** */

    public function display_user_product_feedback($product_id) {
        if ($this->checkLogin('U') == '') {
            redirect('login');
        } else {
            $id = array('id' => $product_id);
            $this->data['userVal'] = $this->product_model->get_all_details(PRODUCT, $id);
            $this->data['feedback_details'] = $this->product_model->get_all_details(PRODUCT_FEEDBACK, array('voter_id' => $this->checkLogin('U'), 'product_id' => $product_id));
            $this->load->view('site/user/add_user_product_feedback', $this->data);
        }
    }

    /*     * ******* Coding for add feedback for user product ******** */

    public function add_user_product_feedback() {
        $user_id = $this->input->post('rate');
        $rating = $this->input->post('rating_value');
        $title = $this->input->post('title');
        $description = $this->input->post('description');
        $product_id = $this->input->post('product_id');
        $seller_id = $this->input->post('seller_id');
        if ($user_id != '') {
            $this->user_model->simple_insert(PRODUCT_FEEDBACK, array('title' => $title, 'description' => $description, 'product_id' => $product_id, 'seller_id' => $seller_id, 'rating' => $rating, 'voter_id' => $user_id, 'status' => 'Active'));
            if ($this->lang->line('ur_feedback_add_succ') != '')
                $lg_err_msg = $this->lang->line('ur_feedback_add_succ');
            else
                $lg_err_msg = 'Your feedback added successfully';
            $this->setErrorMessage('success', $lg_err_msg);
            //redirect($base_url);
            echo "<script>window.history.go(-1)</script>";
        }
    }

    public function post_order_comment() {
        if ($this->checkLogin('U') != '') {
        	$datestring = "%Y-%m-%d %H:%i:%s";
        	$time = time();
        	$createdTime = mdate($datestring, $time);

        	$cmtI = $this->user_model->commonInsertUpdate(REVIEW_COMMENTS, 'insert', array(), array('date'=>$createdTime), '');
            $cmtID = $this->user_model->get_last_insert_id();
            $allDetails = $this->user_model->get_all_details(REVIEW_COMMENTS, array('id' => $cmtID))->result_array();
            $prodDetails = $this->user_model->get_all_details(PRODUCT, array('id' => $allDetails[0]['product_id']))->result_array();

            $datestring = "%Y-%m-%d %H:%i:%s";
			$time = time();
            $createdTime = mdate($datestring, $time);

            $actArr = array(
                'activity' => 'review-comments',
                'activity_id' => $prodDetails[0]['seller_product_id'],
                'user_id' => $this->checkLogin('U'),
                'activity_ip' => $this->input->ip_address(),
                'created' => $createdTime,
                'comment_id' => $this->input->post('deal_code')
            );

            $this->send_comment_noty_mail($cmtID, $prodDetails[0]['seller_product_id']);
            $this->user_model->simple_insert(NOTIFICATIONS, $actArr);
        }
    }

    public function send_comment_noty_mail($cmtID = '0', $pid = '0') {
        if ($this->checkLogin('U') != '') {
            if ($cmtID != '0' && $pid != '0') {
                        	$commentDetails = $this->user_model->get_all_details(REVIEW_COMMENTS, array('id' => $cmtID));

                        	if ($commentDetails->num_rows() == 1) {

                        		$commentorDetail =  $this->user_model->get_all_details(USERS, array('id' => $commentDetails->row()->commentor_id));

                        		$paymentDetails = $this->user_model->get_all_details(PAYMENT, array('dealCodeNumber' => $commentDetails->row()->deal_code,'product_id'=>$commentDetails->row()->product_id));

                        		if($paymentDetails->row()->sell_id == $commentDetails->row()->commentor_id){
                        			$viewerDetail = $this->user_model->get_all_details(USERS, array('id' => $paymentDetails->row()->user_id));;
                        		}else{
                        			$viewerDetail = $this->user_model->get_all_details(USERS, array('id' => $paymentDetails->row()->sell_id));;
                        		}

                        		$emailNoty = explode(',', $viewerDetail->row()->email_notifications);
                        		if (in_array('review-comments', $emailNoty)) {

	                            $discussion_url = '<a href="'.base_url().'order-review/'.$paymentDetails->row()->user_id.'/'.$paymentDetails->row()->sell_id.'/'.$paymentDetails->row()->dealCodeNumber.'">#'.$paymentDetails->row()->dealCodeNumber.'</a>';

	                            $newsid = '32';
	                            $template_values = $this->user_model->get_newsletter_template_details($newsid);
	                            $adminnewstemplateArr = array('email_title' => $this->config->item('email_title'), 'logo' => $this->data['logo'], 'receiver' => $viewerDetail->row()->full_name, 'sender' => $commentorDetail->row()->full_name,'discussion_url'=>$discussion_url);
	                            extract($adminnewstemplateArr);
	                            $subject = $template_values['news_subject'];

	                            $message .= '<!DOCTYPE HTML>
									<html>
									<head>
									<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
									<meta name="viewport" content="width=device-width"/>
									<title>' . $template_values['news_subject'] . '</title>
									<body>';
	                            include('./newsletter/registeration' . $newsid . '.php');

	                            $message .= '</body>
									</html>';

	                            if ($template_values['sender_name'] == '' && $template_values['sender_email'] == '') {
	                                $sender_email = $this->data['siteContactMail'];
	                                $sender_name = $this->data['siteTitle'];
	                            } else {
	                                $sender_name = $template_values['sender_name'];
	                                $sender_email = $template_values['sender_email'];
	                            }

	                            $email_values = array('mail_type' => 'html',
	                                'from_mail_id' => $sender_email,
	                                'mail_name' => $sender_name,
	                                'to_mail_id' => $viewerDetail->row()->email,
	                                'subject_message' => $subject,
	                                'body_messages' => $message
	                            );

	                            //print_r($email_values); die;
	                            $email_send_to_common = $this->product_model->common_email_send($email_values);
                        	}
                        }
            }
        }
    }

    public function change_received_status() {
        if ($this->checkLogin('U') != '') {
            $status = $this->input->post('status');
            $rid = $this->input->post('rid');
            $this->user_model->update_details(PAYMENT, array('received_status' => $status), array('id' => $rid));
        }
    }

    /*     * ****************Invite Friends******************* */

    public function invite_friends() {
        if ($this->checkLogin('U') == '') {
            redirect('login');
        } else {

            if($this->config->item('storefront_fees_month') == '0' && $this->config->item('storefront_fees_year') == '0'){
            $this->data['sellers_list'] = $this->user_model->get_all_details(USERS,array('group'=>'Seller','status'=>'Active'));
            }else{
                $condition = 'select u.*,s.store_name,s.logo_image, Count(p.id) as ProductCount from '.USERS.' as u left join '.STORE_FRONT.' as s on s.user_id = u.id left join '.PRODUCT.' as p on u.id = p.user_id where u.group ="Seller" and u.store_payment ="Paid" and p.status="Publish" and u.status = "Active" and NOT FIND_IN_SET(u.id ,"'.implode(",", $featuredId).'") group by u.id';
                $this->data['sellers_list'] = $sellers_list = $this->user_model->ExecuteQuery($condition);
            }            
            $this->data['heading'] = 'Invite Friends';
            $this->load->view('site/user/invite_friends', $this->data);
        }
    }

    public function app_twitter() {
        $returnStr['status_code'] = 1;
        $returnStr['url'] = base_url() . 'twtest/get_twitter_user';
        $returnStr['message'] = '';
        echo json_encode($returnStr);
    }

    public function find_friends_twitter() {
        $returnStr['status_code'] = 1;
        $returnStr['url'] = base_url() . 'twtest/invite_friends';
        $returnStr['message'] = '';
        echo json_encode($returnStr);
    }

    public function find_friends_gmail() {
        $returnStr['status_code'] = 1;
        //error_reporting(0);
        include_once './invite_friends/GmailOath.php';
        include_once './invite_friends/Config.php';
        $oauth = new GmailOath($consumer_key, $consumer_secret, $argarray, $debug, $callback);
        $getcontact = new GmailGetContacts();
        $access_token = $getcontact->get_request_token($oauth, false, true, true);
        $this->session->set_userdata('oauth_token', $access_token['oauth_token']);
        $this->session->set_userdata('oauth_token_secret', $access_token['oauth_token_secret']);
        $returnStr['url'] = "https://www.google.com/accounts/OAuthAuthorizeToken?oauth_token=" . $oauth->rfc3986_decode($access_token['oauth_token']);
        $returnStr['message'] = '';
        echo json_encode($returnStr);
    }

    public function find_friends_gmail_callback() {
        include_once './invite_friends/GmailOath.php';
        include_once './invite_friends/Config.php';
        //error_reporting(0);
        $oauth = new GmailOath($consumer_key, $consumer_secret, $argarray, $debug, $callback);
        $getcontact_access = new GmailGetContacts();

        $request_token = $oauth->rfc3986_decode($this->input->get('oauth_token'));
        $request_token_secret = $oauth->rfc3986_decode($this->session->userdata('oauth_token_secret'));
        $oauth_verifier = $oauth->rfc3986_decode($this->input->get('oauth_verifier'));

        $contact_access = $getcontact_access->get_access_token($oauth, $request_token, $request_token_secret, $oauth_verifier, false, true, true);
        $access_token = $oauth->rfc3986_decode($contact_access['oauth_token']);
        $access_token_secret = $oauth->rfc3986_decode($contact_access['oauth_token_secret']);
        $contacts = $getcontact_access->GetContacts($oauth, $access_token, $access_token_secret, false, true, $emails_count);

        $count = 0;
        foreach ($contacts as $k => $a) {
            $final = end($contacts[$k]);
            foreach ($final as $email) {
                $this->send_invite_mail($email["address"]);
                $count++;
            }
        }
        if ($count > 0) {
            echo "
			<script>
				alert('Invitations sent successfully');
				window.close();
			</script>
			";
        } else {
            echo "
			<script>
				window.close();
			</script>
			";
        }
    }

    public function send_invite_mail($to = '') {
        if ($to != '') {
            $newsid = '16';
            $template_values = $this->product_model->get_newsletter_template_details($newsid);
            $adminnewstemplateArr = array('logo' => $this->data['logo'], 'siteTitle' => $this->data['siteTitle'], 'meta_title' => $this->config->item('meta_title'), 'full_name' => $this->data['userDetails']->row()->full_name, 'user_name' => $this->data['userDetails']->row()->user_name);
            extract($adminnewstemplateArr);
            $subject = $template_values['news_subject'];
            $message .= '<!DOCTYPE HTML>
					<html>
					<head>
					<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
					<meta name="viewport" content="width=device-width"/>
					<title>' . $template_values['news_subject'] . '</title><body>';
            include('./newsletter/registeration' . $newsid . '.php');

            $message .= '</body>
					</html>';


            if ($template_values['sender_name'] == '' && $template_values['sender_email'] == '') {
                $sender_email = $this->data['siteContactMail'];
                $sender_name = $this->data['siteTitle'];
            } else {
                $sender_name = $template_values['sender_name'];
                $sender_email = $template_values['sender_email'];
            }

            $email_values = array('mail_type' => 'html',
                'from_mail_id' => $sender_email,
                'mail_name' => $sender_name,
                'to_mail_id' => $to,
                'subject_message' => $subject,
                'body_messages' => $message
            );
            $email_send_to_common = $this->product_model->common_email_send($email_values);
        }
    }

    public function bids(){
        if ($this->checkLogin('U') != '') {
            $loginId = $this->checkLogin ( 'U' );
            $this->data['bid_messages'] = $this->product_model->get_bid_messages($loginId);
			// echo $this->db->last_query();die;
            $this->load->view ( 'site/user/bids', $this->data );
        }
        else{
             redirect('login');
        }
    
    }
	public function auction_prodcut_list(){
        if ($this->checkLogin('U') != '') {
            $loginId = $this->checkLogin ( 'U' );
            $this->data['auctions'] = $this->product_model->get_auctions($loginId);
			$this->load->view ( 'site/user/auctions', $this->data );
        }
        else{
             redirect('login');
        }
    
    }
	public function auction_detail(){
        if ($this->checkLogin('U') != '') {
            $loginId 	= $this->checkLogin ( 'U' );
			$auction_id = $this->uri->segment(2);
			$check 		= $this->user_model->get_all_details(PAYMENT,array('id'=>$auction_id));
			
			if($check->num_rows() == 1){
				$this->data['auction'] 		= $this->product_model->get_auction_detail($loginId,$auction_id);
				// echo '<pre>';print_r($this->data['auction']->result());die;
				$transaction_id				= $this->data['auction']->row()->paypal_transaction_id;
				$this->data['conversation'] = $this->user_model->get_all_details(BID_MESSAGE,array('transaction_id'=>$transaction_id, 'status' => 'Accept'));
				$this->data['offersList'] 	= $this->user_model->get_all_details(BID_MESSAGE,array('transaction_id'=>$transaction_id, 'type' => 'offer'));
			 	/* echo $this->db->last_query();
				echo '<pre>';print_r($this->data['offersList']->result());die; */
				$this->user_model->update_details(PAYMENT,array('read_status'=>'read'),array('id'=>$auction_id,'type'=>'auction'));
				$this->load->view ( 'site/user/auction_detail', $this->data );
			}else{
				redirect();
			}
        }
        else{
             redirect('login');
        }
    
    }
    public function send_message(){
        
        if ($this->checkLogin('U') != '') {
        
            $sender_id      =   $this->checkLogin('U');
            $receiver_id    =   $this->input->post ( 'receiver_id' );
            $BidId          =   $this->input->post ( 'BidId' );
            $subject        =   'New Message';
            $message        =   trim($this->input->post ( 'message' ));
            if($sender_id != '' && $receiver_id != '' && $BidId != '' && $subject != '' && $message != ''){
                $statusQry      =   $this->user_model->get_all_details ( BID_MESSAGE, array ('BidId' => $BidId));
                $status         =   $statusQry->row()->status;
                $dataArr        = array(
                                        'senderId'  		=> 	$sender_id ,
                                        'receiverId'		=> 	$receiver_id ,
                                        'BidId'     		=> 	$BidId ,
                                        'subject'   		=> 	$subject ,
                                        'message'   		=> 	$message,
                                        'status'    		=> 	$status,
										'transaction_id'	=>	$statusQry->row()->transaction_id,
                                        'type'      		=> 	'bid'
                                 );
                $this->user_model->simple_insert(BID_MESSAGE, $dataArr);
                $this->user_model->update_details(BID_MESSAGE,array('auction_read'=>'No'),array('type'=>'auction','BidId'=>$BidId));
                $this->user_model->update_details(BID_MESSAGE,array('auction_read'=>'No'),array('type'=>'offer','BidId'=>$BidId));
                $this->setErrorMessage('success', 'Message Sent Successfully');
                redirect('conversation/'.$BidId);
            }else{ 
                $this->setErrorMessage('Error', 'Oops..Some Parameter missing.Please try again later.');
                redirect('conversation/'.$BidId);
            }
        
        }
        else{
             redirect('login');
        }
    }
    
    public function conversation(){
        
            $BidId = $this->uri->segment ( 2, 0 );
            $this->data['BidId'] = $BidId;
            $this->data['userId'] = $this->checkLogin ( 'U' );
            if($this->checkLogin ( 'U' ) != '' && $BidId != ''){
            
                $this->data['conversationDetails'] = $this->user_model->get_all_details ( BID_MESSAGE, array ('BidId' => $BidId ),array(array('field'=>'id', 'type'=>'asc')));
                
                $this->user_model->update_details(BID_MESSAGE,array('msg_read' => 'Yes','auction_read'=>'Yes'),array('receiverId'=>$this->checkLogin( 'U' ),'BidId'=> $BidId) );
                $this->user_model->update_details(BID_MESSAGE,array('rspn_read' => 'YES','auction_FR_status'=>'Opened'),array('receiverId'=>$this->checkLogin( 'U'),'BidId'=> $BidId,'type'=>'auction' ) );
                
                $condition1 =   array('BidId'=>$BidId, 'type'=>'auction');
                $this->data['auction_detail'] = $this->user_model->get_all_details(BID_MESSAGE,$condition1);
                $this->data['payment_detail'] = $this->user_model->get_all_details(PAYMENT,array('paypal_transaction_id'=>$this->data['auction_detail']->row()->transaction_id));
                // echo '<pre>';print_r($this->data['payment_detail']->row());die;
                $condition112 =   array('id'=>$this->data['payment_detail']->row()->product_id);
                $this->data['paid_product_detail'] = $this->user_model->get_all_details(PRODUCT,$condition112);
                
                $condition2 =   array('id'=> $this->data['auction_detail']->row()->auction_prdId, 'status'=>'Publish');
                $this->data['auction_product_detail'] = $this->user_model->get_all_details(PRODUCT,$condition2);
                
                $condition3 =   array('id'=> $this->data['auction_detail']->row()->productId, 'status'=>'Publish');
                $this->data['currenct_auction_product_detail'] = $this->user_model->get_all_details(PRODUCT,$condition3);
                
                $this->user_model->update_details(BID_MESSAGE,array('msg_read' => 'Yes'),array('receiverId'=>$this->checkLogin( 'U' ),'BidId'=> $BidId) );
            
                
                $temp[] = $this->data['conversationDetails']->row()->senderId;
                $temp[] = $this->data['conversationDetails']->row()->receiverId;
                $productId = $this->data['productId'] = $this->data['conversationDetails']->row()->productId;

                if(!in_array($this->checkLogin ( 'U' ), $temp))redirect();
                if($this->checkLogin ( 'U' ) == $temp[0])
                {
                    $this->data['sender_id'] = $temp[0];
                    $this->data['receiver_id'] = $temp[1];
                }
                else
                {
                    $this->data['sender_id'] = $temp[1];
                    $this->data['receiver_id'] = $temp[0];
                }
                $condition4 = array('BidId'=>$BidId,'type'=>'offer' );
                
                $this->data['offer_details'] =  $this->user_model->get_all_details(BID_MESSAGE,$condition4);
                $this->data['senderDetails'] = $this->user_model->get_all_details ( USERS, array ('id' => $this->data['sender_id'] ));

                $this->data['receiverDetails'] = $this->user_model->get_all_details ( USERS, array ('id' => $this->data['receiver_id'] ));

                $this->data['productDetails'] = $this->user_model->get_all_details ( PRODUCT, array ('id' => $productId));
                
                $this->data['BidId'] = $BidId;
                
                // $this->data['products'] = $this->product_model->get_all_details(PRODUCT, array('user_id'=>$this->checkLogin ( 'U' ), 'status'=>'Publish'));
                
                $this->load->view ( 'site/user/conversation', $this->data );
            
        }
        
        else{
            redirect('login');
        }
    
    }
    
    public function send_offer(){
            
        if($this->checkLogin ( 'U' ) != ''){
            $offer_price     	= $this->input->post('offer_price');
            $BidId           	= $this->input->post('BidId');
            $receiver_id     	= $this->input->post('receiver_id');
            $sender_id       	= $this->checkLogin ( 'U' );
			$auction_prdct_id 	= $this->user_model->get_all_details(BID_MESSAGE,array('type'=>'auction','BidId'=>$BidId));
			$product_details 	= $this->user_model->get_all_details(PRODUCT,array('id'=>$auction_prdct_id->row()->productId));
            $seller_product_id 	= $product_details->row()->seller_product_id;
			
			if($sender_id != '' && $offer_price != '' && $BidId != '' && $receiver_id != '' && $seller_product_id != ''){
				
                $dataArr        = array(
                                            'senderId'    => $sender_id ,
                                            'receiverId'  => $receiver_id ,
                                            'offer_price' => $offer_price ,
                                            'BidId'       => $BidId ,
                                            'productId'   => $auction_prdct_id->row()->productId ,
                                            'product_name'=> $product_details->row()->product_name ,
                                            'product_sell_id'=> $product_details->row()->user_id ,
                                            'product_img'=> explode(',',$product_details->row()->image)[0] ,
                                            'transaction_id'=> $auction_prdct_id->row()->transaction_id ,
                                            'subject'     => $subject ,
                                            'offer_status'=> 'Pending',
                                            'type'        => 'offer',
                                            'subject'     => 'New Offer',
                                            'msg_read'    => 'No'
                                     );
                    $this->user_model->simple_insert(BID_MESSAGE, $dataArr);
					$this->user_model->update_details(PAYMENT,array('read_status'=>'unread'),array('type'=>'auction','paypal_transaction_id'=>$auction_prdct_id->row()->transaction_id));
					
					$datestring             =   "%Y-%m-%d %h:%i:%s";
					$time                   =   time();
					$createdTime            =   mdate($datestring, $time);
				
					$notificationArr =  array(  	'user_id'       =>  $this->checkLogin('U'),
													'created'       =>  $createdTime,
													'activity_id'   =>  $seller_product_id,
													'activity'      =>  "Sent New Offer",
													'activity_ip'   =>  $this->input->ip_address()
											);
					$this->product_model->simple_insert(NOTIFICATIONS,$notificationArr);
                    $this->setErrorMessage('success', 'Offer Sent Successfully');
                    redirect('conversation/'.$BidId);
            }else{
                $this->setErrorMessage('Error', 'Oops..Some Parameter missing.Please try again later.');
                redirect('conversation/'.$BidId);
            }
                
        }
        
        else{
            redirect('login');
        }
    
    }
    
    public function update_offer(){
            
        if($this->checkLogin ( 'U' ) != ''){
            $offer_price     = $this->input->post('offer_price');
            $BidId           = $this->input->post('BidId');
            $receiver_id     = $this->input->post('receiver_id');
            $sender_id       = $this->checkLogin ( 'U' );
            if($sender_id != '' && $BidId != '' && $receiver_id != ''){
                $dataArr    = 	array(
                                    'BidId'       => $BidId ,
                                    'type'     => 'offer',
                                           
                                );
				$dataArr2    = 	array(
									'BidId'       => $BidId ,
									'type'        => 'auction',
								);
                $auction_detail =   $this->user_model->get_all_details(BID_MESSAGE, $dataArr2);
                if( $offer_price != ''){
                    $this->user_model->update_details(BID_MESSAGE,array(
																		'offer_status'=>'Pending',
																		'status'=>'Pending',
																		'offer_price'=>$offer_price,
																		'msg_read'=> 'No'
																		
																	),$dataArr);
                    
					$auction_prdct_id 	= $this->user_model->get_all_details(BID_MESSAGE,array('type'=>'auction','BidId'=>$BidId));
					$this->user_model->update_details(PAYMENT,array('read_status'=>'unread'),array('paypal_transaction_id'=>$auction_prdct_id->row()->transaction_id));
					
					$product_details 	= $this->user_model->get_all_details(PRODUCT,array('id'=>$auction_prdct_id->row()->productId));
					$seller_product_id 	= $product_details->row()->seller_product_id;
			
					$datestring             =   "%Y-%m-%d %h:%i:%s";
					$time                   =   time();
					$createdTime            =   mdate($datestring, $time);
				
					$notificationArr =  array(  	'user_id'       =>  $this->checkLogin('U'),
													'created'       =>  $createdTime,
													'activity_id'   =>  $seller_product_id,
													'activity'      =>  "Sent New Offer",
													'activity_ip'   =>  $this->input->ip_address()
											);
					$this->product_model->simple_insert(NOTIFICATIONS,$notificationArr);
					
					$this->setErrorMessage('success', 'Offer Updated Successfully');
                    redirect('conversation/'.$BidId);
                }else{
                    $this->setErrorMessage('Error', 'Please enter the offer price');
                    redirect('conversation/'.$BidId);
                }
                
            }else{
                $this->setErrorMessage('Error', 'Oops..Some Parameter missing.Please try again later.');
                redirect('conversation/'.$BidId);
            }
                
        }
        
        else{
            redirect('login');
        }
    
    }
    
    public function accept_offer(){
            
        if($this->checkLogin ( 'U' ) != ''){
            $BidId           = $this->input->post('BidId');
            $receiver_id     = $this->input->post('receiver_id');
            $sender_id       = $this->checkLogin ( 'U' );
            if($sender_id != '' && $BidId != '' && $receiver_id != ''){
                $dataArr    = array(
                                            'BidId'       => $BidId ,
                                            'type'        => 'offer',
                                     );
                $previous_offer =   $this->user_model->get_all_details(BID_MESSAGE, $dataArr);
                if( $previous_offer->num_rows > 0){
                    $dataArr12  = array(
                                            'BidId'       => $BidId ,
                                            'type'        => 'auction'
                                     );
                    $auction_detail =   $this->user_model->get_all_details(BID_MESSAGE, $dataArr12);
                    
                    if($auction_detail->num_rows() > 0){
                        $transaction_id1 =  $auction_detail->row()->transaction_id;
                    
                    }else{
                        $this->setErrorMessage('Error', 'Oops..Auction  not avalible...Please try again later.');
                        redirect('conversation/'.$BidId);
                    }
                    $product_detail  = $this->user_model->get_all_details(PRODUCT,array('id'=>$auction_detail->row()->productId));
                    if($product_detail->num_rows()> 0){
                        $old_product_name = $product_detail->row()->product_name;
                    }else{
                        $this->setErrorMessage('Error', 'Oops..Product not avalible...Please try again later.');
                        redirect('conversation/'.$BidId);
                    }
                    $user_id = $this->checkLogin ( 'U' );
                    $sell_id = $receiver_id;
                    $product_id = $auction_detail->row()->productId;
                    $offer_price = $previous_offer->row()->offer_price;
                    $paymnt_detail  = $this->user_model->get_all_details(PAYMENT,array('paypal_transaction_id'=>$transaction_id1,'user_id'=>$user_id,'do_capture'=>'Pending'));
                    /*** update offer product and seller id ***/
                    if($paymnt_detail->num_rows > 0){
                        $this->user_model->update_details(PAYMENT,array('sell_id'=>$sell_id,'product_id'=>$product_id,'price'=>$offer_price,'total'=>$offer_price,'old_product_name'=>$old_product_name),array('paypal_transaction_id'=>$transaction_id1,'user_id'=>$user_id));
                        $transaction_id = $paymnt_detail->row()->paypal_transaction_id;
                    }else{
                        $this->setErrorMessage('Error', 'Oops..Already accepted the offer.');
                        redirect('conversation/'.$BidId);
                    }
                    /*** update offer product and seller id ***/
                    $paymnt_details     = $this->user_model->get_all_details(PAYMENT,array('paypal_transaction_id'=>$transaction_id,'user_id'=>$user_id));
                    
                    $transaction_amount = $paymnt_details->row()->total;
                    $currencyType       = $this->data['currencyType'];
                    if( $currencyType != ''){
                        $currencyType   = 'USD';
                    }
                    
                    $PaypalDodirect = unserialize($this->data['paypal_credit_card_settings']['settings']);
                
                    $dodirects = array(
                        'Sandbox' => $PaypalDodirect['mode'],           // Sandbox / testing mode option.
                        'APIUsername' =>$PaypalDodirect['Paypal_API_Username'],     // PayPal API username of the API caller
                        'APIPassword' => $PaypalDodirect['paypal_api_password'],    // PayPal API password of the API caller
                        'APISignature' => $PaypalDodirect['paypal_api_Signature'],  // PayPal API signature of the API caller
                        'APISubject' => '',                                     // PayPal API subject (email address of 3rd party user that has granted API permission for your app)
                        'APIVersion' => '85.0'      // API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
                    );
                    /*$dodirects = array(
                    'Sandbox' => $PaypalDodirect['mode'],           // Sandbox / testing mode option.
                    'APIUsername' => 'vinubusiness1-facilitator_api1.gmail.com',    // PayPal API username of the API caller
                    'APIPassword' => '1380197781',  // PayPal API password of the API caller
                    'APISignature' => 'AQU0e5vuZCvSg-XJploSa.sGUDlpAERCJ01cF3SVakQxc3HQqQXQ.e4d',   // PayPal API signature of the API caller
                    'APISubject' => '',                                     // PayPal API subject (email address of 3rd party user that has granted API permission for your app)
                    'APIVersion' => '85.0'      // API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
                );*/
                    // Show Errors
                    if($dodirects['Sandbox']){
                         
                        ini_set('display_errors', '1');
                    }
                        $this->load->library('paypal/Paypal_pro', $dodirects);
                    
                        $DCFields = array(
                                        'authorizationid' => $transaction_id,               // Required. The authorization identification number of the payment you want to capture. This is the transaction ID returned from DoExpressCheckoutPayment or DoDirectPayment.
                                        'amt'             => $transaction_amount,                           // Required. Must have two decimal places.  Decimal separator must be a period (.) and optional thousands separator must be a comma (,)
                                        'completetype'    => 'NotComplete',                     // Required. Choose "Complete" or "NotComplete".  The value Complete indiciates that this is the last capture you intend to make.  The value NotComplete indicates that you intend to make additional captures. Complete — This is the last capture you intend to make. NotComplete — You intend to make additional captures.
                                        'currencycode'    => $currencyType,                 // Three-character currency code
                                        'invnum'          => '',                        // Your invoice number
                                        'note'            => '',                            // Informational note about this setlement that is displayed to the buyer in an email and in his transaction history.  255 character max.
                                        'softdescriptor'  => '',                // Per transaction description of the payment that is passed to the customer's credit card statement.
                                        'storeid'         => '',                        // ID of the merchant store.  This field is required for point-of-sale transactions.  Max: 50 char
                                        'terminalid'      => ''                     // ID of the terminal.  50 char max.  
                                    );
                                    
                        $PayPalRequestData = array('DCFields' => $DCFields);
                        
                        $PayPalResult = $this->paypal_pro->DoCapture($PayPalRequestData);
                        // echo '<pre>';print_r($PayPalResult);die;
                        if(!$this->paypal_pro->APICallSuccessful($PayPalResult['ACK']))
                        {
                            $errors = array('Errors'=>$PayPalResult['ERRORS']);
                            $newerrors = $errors['Errors'][0]['L_LONGMESSAGE'];
                            $this->setErrorMessage('error',$newerrors);
                            redirect('conversation/'.$BidId,$this->data);
                        }
                        else
                        {   
                            /***  update payment status   ***/
							
                            $this->user_model->update_details(PAYMENT,array('status'=>'Paid','do_capture'=>'Success'),array('paypal_transaction_id'=>$transaction_id,'user_id'=>$user_id));
                            
                            /***  update offer status   ***/
							
							/*****  product listing commission (flat)  + commission Fee (%) *****/
								
								$seller_id   	= $this->user_model->get_all_details(PRODUCT,array('id'=>$product_id))->row()->user_id;
								$seller_type   	= $this->user_model->get_all_details(USERS,array('id'=>$seller_id,'group'=>'Seller'))->row()->store_payment;
								$type   		= 'normal';
								if($seller_type == 'Paid'){
									$type   	= 'upgraded';
								}
									$product_listing_fee = '';
									$commission_fee = '';
									$productprice = $transaction_amount;
									$commission_detail = $this->cart_model->get_commision_price($productprice,$type);
									
									if( $commission_detail->num_rows > 0 ){
										$product_listing_fee = $commission_detail->row()->product_fee; // flat
										$commission_fee = $commission_detail->row()->booking_fee; // percentage 
									}
									$commission = ($commission_fee * $productprice)/100;
								$condition = array('paypal_transaction_id'=>$transaction_id,'user_id'=>$user_id);
								$datArry = array('product_listing_fee' => $product_listing_fee,'commission'=> $commission);
								$this->user_model->update_details(PAYMENT, $datArry, $condition);
								
								/*****  product listing commission (flat)  + commission Fee (%) *****/
								
                            $this->user_model->update_details(BID_MESSAGE,array('offer_status'=>'Accept','msg_read'=>'NO'),$dataArr);
                    
                            $dataArr1   = array(
                                            'BidId'       => $BidId ,
                                            'type'        => 'auction'
                                     );
                            $this->user_model->update_details(BID_MESSAGE,array('offer_status'=>'Accept','status'=>'Accept','rspn_read'=>'NO'),$dataArr1);
                            $dataArr18   = array(
                                            'BidId'       => $BidId ,
                                            'type'        => 'offer'
                                     );
                            $this->user_model->update_details(BID_MESSAGE,array('offer_status'=>'Accept','status'=>'Accept'),$dataArr18);
                            
							
							$auction_prdct_id 	= $this->user_model->get_all_details(BID_MESSAGE,array('type'=>'auction','BidId'=>$BidId));
							$product_details 	= $this->user_model->get_all_details(PRODUCT,array('id'=>$auction_prdct_id->row()->productId));
							$seller_product_id 	= $product_details->row()->seller_product_id;
					
							$datestring             =   "%Y-%m-%d %h:%i:%s";
							$time                   =   time();
							$createdTime            =   mdate($datestring, $time);
						
							$notificationArr =  array(  	'user_id'       =>  $this->checkLogin('U'),
															'created'       =>  $createdTime,
															'activity_id'   =>  $seller_product_id,
															'activity'      =>  "Accept-Offer",
															'activity_ip'   =>  $this->input->ip_address()
											);
							$this->product_model->simple_insert(NOTIFICATIONS,$notificationArr);
					
					
                            $this->setErrorMessage('success', 'Offer Accepted Successfully');
                            //redirect('conversation/'.$BidId);
                            redirect('purchases');
                        } 
                    
                }else{
                    $this->setErrorMessage('Error', 'Oops..Something Went Wrong.Please try again.');
                    redirect('conversation/'.$BidId);
                }
                
            }else{
                $this->setErrorMessage('Error', 'Oops..Some Parameter missing.Please try again later.');
                redirect('conversation/'.$BidId);
            }
                
        }
        
        else{
            redirect('login');
        }
    
    }
    
    public function decline_offer(){
            
        if($this->checkLogin ( 'U' ) != ''){
            $BidId           = $this->input->post('BidId');
            $receiver_id     = $this->input->post('receiver_id');
            $sender_id       = $this->checkLogin ( 'U' );
            if($sender_id != '' && $BidId != '' && $receiver_id != ''){
                $dataArr    = array(
                                            'BidId'       =>  $BidId ,
                                            'type'        =>  'offer',
                                            
                                     );
                $previous_offer =   $this->user_model->get_all_details(BID_MESSAGE, $dataArr);
                if( $previous_offer->num_rows > 0){
                    $this->user_model->update_details(BID_MESSAGE,array('offer_status'=>'Decline','msg_read'=>'NO'),$dataArr);
						
					$bids =   $this->user_model->get_all_details(BID_MESSAGE,array('BidId' =>  $BidId ));
					if($bids->num_rows() == 2 ){
						$dataArr1   = array(
                                            'BidId' => $BidId ,
                                            //'type ' => 'auction'
                                     );
						$this->user_model->update_details(BID_MESSAGE,array('rspn_read'=>'NO'),$dataArr1);
					}else{
						$dataArr1   = array(
                                            'BidId'   => $BidId ,
                                            'type !=' => 'auction'
                                     );
						$this->user_model->update_details(BID_MESSAGE,array('status'=>'Decline','msg_read'=>'NO'),$dataArr1);
					}
                   
				    $auction_prdct_id 	= $this->user_model->get_all_details(BID_MESSAGE,array('type'=>'auction','BidId'=>$BidId));
					$product_details 	= $this->user_model->get_all_details(PRODUCT,array('id'=>$auction_prdct_id->row()->productId));
					$seller_product_id 	= $product_details->row()->seller_product_id;
					
					$datestring             =   "%Y-%m-%d %h:%i:%s";
					$time                   =   time();
					$createdTime            =   mdate($datestring, $time);
					
					$notificationArr =  array(  	'user_id'       =>  $this->checkLogin('U'),
													'created'       =>  $createdTime,
													'activity_id'   =>  $seller_product_id,
													'activity'      =>  "Declined-Offer",
													'activity_ip'   =>  $this->input->ip_address()
											);
					$this->product_model->simple_insert(NOTIFICATIONS,$notificationArr);
					
                    $this->setErrorMessage('success', 'Offer Declined Successfully');
                    redirect('conversation/'.$BidId);
                }else{
                    $this->setErrorMessage('Error', 'Oops..Something Went Wrong.Please try again.');
                    redirect('conversation/'.$BidId);
                }
                
            }else{
                $this->setErrorMessage('Error', 'Oops..Some Parameter missing.Please try again later.');
                redirect('conversation/'.$BidId);
            }
                
        }
        
        else{
            redirect('login');
        }
    
    }
    
    public function reject_offer(){
            
        if($this->checkLogin ( 'U' ) != ''){
            $BidId           = $this->input->post('BidId');
            $receiver_id     = $this->input->post('receiver_id');
            $sender_id       = $this->checkLogin ( 'U' );
            if($sender_id != '' && $BidId != '' && $receiver_id != ''){
                $dataArr    = array(
                                            'BidId'       => $BidId ,
                                            'type'        => 'offer'
                                     );
                $previous_offer =   $this->user_model->get_all_details(BID_MESSAGE, $dataArr);
                
                if( $previous_offer->num_rows > 0){
                    $this->user_model->update_details(BID_MESSAGE,array('offer_status'=>'Reject','msg_read'=>'NO'),$dataArr);
                    $bids =   $this->user_model->get_all_details(BID_MESSAGE,array('BidId' =>  $BidId ));
					if($bids->num_rows() == 2 ){
						$dataArr1   = array(
                                            'BidId' => $BidId ,
                                            'type ' => 'auction'
                                     );
						$this->user_model->update_details(BID_MESSAGE,array('rspn_read'=>'NO'),$dataArr1);
					}else{
						$dataArr1   = 	array(
												'BidId' => $BidId ,
												'type !=' => 'auction'
										);
						$this->user_model->update_details(BID_MESSAGE,array('status'=>'Reject','msg_read'=>'NO'),$dataArr1);
					}
					
					$auction_prdct_id 	= $this->user_model->get_all_details(BID_MESSAGE,array('type'=>'auction','BidId'=>$BidId));
					$product_details 	= $this->user_model->get_all_details(PRODUCT,array('id'=>$auction_prdct_id->row()->productId));
					$seller_product_id 	= $product_details->row()->seller_product_id;
			
					$datestring             =   "%Y-%m-%d %h:%i:%s";
					$time                   =   time();
					$createdTime            =   mdate($datestring, $time);
				
					$notificationArr =  array(  	'user_id'       =>  $this->checkLogin('U'),
													'created'       =>  $createdTime,
													'activity_id'   =>  $seller_product_id,
													'activity'      =>  "Reject-Offer",
													'activity_ip'   =>  $this->input->ip_address()
											);
					$this->product_model->simple_insert(NOTIFICATIONS,$notificationArr);
					
                    $this->setErrorMessage('success', 'Offer Rejected Successfully');
                    redirect('conversation/'.$BidId);
                }else{
                    $this->setErrorMessage('Error', 'Oops..Something Went Wrong.Please try again.');
                    redirect('conversation/'.$BidId);
                }
                
            }else{
                $this->setErrorMessage('Error', 'Oops..Some Parameter missing.Please try again later.');
                redirect('conversation/'.$BidId);
            }
                
        }
        
        else{
            redirect('login');
        }
    
    }

  
  
    public function captcha_refresh(){
                    $img_path   = 'captcha/';
                    $img_url    = 'captcha/';
                    $font_path  = 'system/fonts/texb.ttf';
                    $values     =   array(
                                        'word' => '',
                                        'word_length' => 8,
                                        'img_path'    => $img_path,
                                        'img_url'     => $img_url,
                                        'font_path'   => $font_path,
                                        'img_width'   => '150',
                                        'img_height'  => 50,
                                        'expiration'  => 3600
                                    );
                        $data = create_captcha($values);
                    $this->session->set_userdata('captchaWord',$data['word']);
                    echo $data['image'];
    }

    public function getstateslist(){
        if($this->checkLogin ( 'U' ) != ''){
            $countryCode = $this->input->post('countryCode');
            if($countryCode != ''){
                $country = $this->user_model->get_all_details(COUNTRIES,array('country_code'=>$countryCode));
                $stateList = $this->user_model->get_all_details(STATES,array('country_id'=>$country->row()->id));
                 //echo '<pre>';print_r($stateList->result());    
                if($stateList->num_rows() > 0){
                        $state1 .= '<option value="">-------------------- SELECT --------------------</option>';
                    foreach($stateList->result() as $state){

                        $state1 .= '<option value="'.$state->name.'">'.$state->name.'</option>';
                    }

                    echo $state1;
                }else{
                    echo '<option value="">No States Available</option>';
                }
            }

        }else{
            echo 'login';
        }
       
    }
    public function getcitiesList(){
        if($this->checkLogin ( 'U' ) != ''){
            $stateName = $this->input->post('stateName');
            if($stateName != ''){
                $state = $this->user_model->get_all_details(STATES,array('name'=>$stateName));
                $citiesList = $this->user_model->get_all_details(CITIES,array('state_id'=>$state->row()->id));
                 //echo '<pre>';print_r($stateList->result());    
                if($citiesList->num_rows() > 0){
                        $city1 .= '<option value="">-------------------- SELECT --------------------</option>';
                    foreach($citiesList->result() as $city){

                        $city1 .= '<option value="'.$city->name.'">'.$city->name.'</option>';
                    }

                    echo $city1;
                }else{
                    echo '<option value="">No Cities Available</option>';
                }
            }

        }else{
            echo 'login';
        }
       
    }

    /*     * ************************************************ */
}

/* End of file user.php */
/* Location: ./application/controllers/site/user.php */
