<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * User Settings related functions
 * @author Teamtweaks
 *
 */
 
class Groupgift extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper(array('cookie','date','form','email'));
		$this->load->library(array('encrypt','form_validation','pagination'));
		$this->load->model('groupgift_model','group_gift');
		if($_SESSION['sMainCategories'] == ''){
			$sortArr1 = array('field'=>'cat_position','type'=>'asc');
			$sortArr = array($sortArr1);
			$_SESSION['sMainCategories'] = $this->group_gift->get_all_details(CATEGORY,array('rootID'=>'0','status'=>'Active'),$sortArr);
		}
		$this->data['mainCategories'] = $_SESSION['sMainCategories'];
		if($_SESSION['sColorLists'] == ''){
			$_SESSION['sColorLists'] = $this->group_gift->get_all_details(LIST_VALUES,array('list_id'=>'1'));
		}
		$this->data['mainColorLists'] = $_SESSION['sColorLists'];
		$this->data['loginCheck'] = $this->checkLogin('U');
		$this->data['likedProducts'] = array();
		if($this->data['loginCheck'] != ''){
		   $this->data['likedProducts'] = $this->group_gift->get_all_details(PRODUCT_LIKES,array('user_id'=>$this->checkLogin('U')));
		}
		define("API_LOGINID",$this->config->item('payment_2'));
		/***********************Adaptive Payment configuration***************/
		$this->config->load('paypal_adaptive');
		$config = array(
			'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
			'APIUsername' => $this->config->item('APIUsername'), 	// PayPal API username of the API caller
			'APIPassword' => $this->config->item('APIPassword'), 	// PayPal API password of the API caller
			'APISignature' => $this->config->item('APISignature'), 	// PayPal API signature of the API caller
			'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
			'APIVersion' => $this->config->item('APIVersion'), 		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
			'DeviceID' => $this->config->item('DeviceID'), 
			'ApplicationID' => $this->config->item('ApplicationID'), 
			'DeveloperEmailAccount' => $this->config->item('DeveloperEmailAccount')
		);
		$this->load->library('paypal/Paypal_adaptive', $config);
		/****************************END***********************/
	}
	public function index(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$this->data['heading'] = 'Group Gifts';
			$this->data['groupgiftList'] = $this->group_gift->get_groupgift_list($this->checkLogin('U'));
			if($this->data['groupgiftList']->num_rows() >0){
			   foreach($this->data['groupgiftList']->result() as $_groupgiftlist){
					$current_date = date('m-d-Y');
					$date = $_groupgiftlist->created;
					$date = strtotime($date);
					$date = strtotime("+".$this->config->item('expiry_days')." day", $date);
					$expired_date = date('m-d-Y', $date);
					if($expired_date < $current_date && $_groupgiftlist->status=='Active'){
						$this->group_gift->update_details(GROUP_GIFTS,array('status'=>'Unsuccessful'),array('id'=>$_groupgiftlist->id));
					}
					
				   $this->data['contributeDetails'][$_groupgiftlist->id] = $this->group_gift->get_all_details(GROUPGIFT_PAYMENT,array('groupgift_id'=>$_groupgiftlist->id));
			   }
			}
			$this->data['groupgiftPayment'] = $this->group_gift->get_all_details(GROUPGIFT_PAYMENT,array('user_id'=>$this->checkLogin('U')));
			$this->load->view('site/groupgifts/groupgifts_list',$this->data);
		} 
	}
	public function InsertProduct(){
		if($this->checkLogin('U')!=''){
			$shippingCost =0;
			$tax =0;
			parse_str($this->input->post('data'), $searcharray);
			$taxList = $this->group_gift->get_all_details(COUNTRY_LIST,array('status'=>'Active','country_code'=>$searcharray['country']));
			if($taxList->num_rows() ==1){
				$tax = $taxList->row()->shipping_tax;
			}
			$product_details = $this->group_gift->get_all_details(PRODUCT,array('seller_product_id'=>$searcharray['product_id']));
			if($product_details->num_rows()==1){
				if($product_details->row()->served_specific==1){
					$contryDetails = $this->group_gift->get_all_details(SHIPPING_COST,array('product_id'=>$searcharray['product_id'],'country_code'=>$searcharray['country']));
					if($contryDetails->num_rows()==1){
						if($searcharray['gift_qty']>1){
							$shippingCost = $contryDetails->row()->anotheritem_ship_cost;
						}else{
							$shippingCost = $contryDetails->row()->separate_ship_cost;
						}	
					}
				}else{
					$contryDetails = $this->group_gift->get_all_details(SHIPPING_COST,array('product_id'=>$searcharray['product_id'],'country_code'=>$searcharray['country']));
					if($contryDetails->num_rows()==1){
						if($searcharray['gift_qty']>1){
							$shippingCost = $contryDetails->row()->anotheritem_ship_cost;
						}else{
							$shippingCost = $contryDetails->row()->separate_ship_cost;
						}
					}else{
						if($searcharray['gift_qty']>1){
							$shippingCost = $product_details->row()->another_shipping_cost;
						}else{
							$shippingCost = $product_details->row()->shipping_cost;
						}
					}
				}
			}
			if($searcharray['seller_id'] ==''){
				$gistseller_id = 0;
			}else{
				$giftseller_id = $searcharray['seller_id'];
			}
			$seller_id = $this->group_gift->get_all_details(GROUP_GIFTS,array('gift_seller_id'=>$giftseller_id));
			if($seller_id->num_rows() ==1){
				$excludeArr =array('gift_total','data','user_image','seller_id','url','ship_value','recipient_image','gift_seller_id','long_url','fb_image','sug_img');
				$current_date = strtotime(date('Y-m-d'));
				$expired = strtotime("+".$this->config->item('expiry_days')." day", $current_date);
				$ecpired_date = date('Y-m-d', $expired);
				$dataArr = array(
					'status' => 'Active',
					'expired_date'=> $ecpired_date,
					'gift_total'=> $searcharray['gift_total'],
					'gift_name'=> $searcharray['gift_name'],
					'gift_description'=> $searcharray['gift_description'],
					'gift_notes'=> $searcharray['gift_notes']
				);
				$this->group_gift->commonInsertUpdate(GROUP_GIFTS,'update',$excludeArr,$dataArr,array('gift_seller_id'=>$giftseller_id)); 
				$returnArr['shippingval'] = $seller_id->row()->recipient_name.'+'.$seller_id->row()->address1.','.$seller_id->row()->address2.'+'.$seller_id->row()->city.'+'.$seller_id->row()->country.'+'.$searcharray['gift_name'].'+'.$searcharray['gift_description'].'+'.$seller_id->row()->gift_seller_id.'+'.$shippingCost.'+'.$tax.'+'.$seller_id->row()->recipient_image.'+'.$seller_id->row()->quantity.'+'.$seller_id->row()->price;	   
			}else{
				$excludeArr = array('data','gift_qty','user_image','recipient_image','gift_seller_id','seller_id','long_url','fb_image','sug_img');
				if($searcharray['sug_img']!=''){
					  $file_name = $searcharray['sug_img'];
				}elseif($searcharray['fb_image']!=''){
					$img = file_get_contents($searcharray['fb_image']);
					$file_name = time().".jpg";
					$upload_path = './images/users/';
					file_put_contents($upload_path.$file_name, $img);
				}elseif($searcharray['user_image']!=''){
					$file_name = $searcharray['user_image'];
				}else{
					$config['overwrite'] = FALSE;
					$config['allowed_types'] = 'jpg|jpeg|gif|png';
					$config['upload_path'] = './images/users/';
					$this->load->library('upload', $config);
					if($this->upload->do_upload('recipient_image')){
						$upload_data = $this->upload->data(); 
						$file_name = $upload_data['file_name'];	
					}
				}		
				$gift_seller_id = mktime();
				$get_seller_id = $this->group_gift->get_all_details(GROUP_GIFTS,array('gift_seller_id'=>$gift_seller_id));
				while($get_seller_id->num_rows()>0){
					$gift_seller_id = mktime();
				}
				$check_url = $this->group_gift->get_all_details(SHORTURL,array('product_id'=>$gift_seller_id,'user_id'=>$this->checkLogin('U')));
				if($check_url->num_rows()>0){
					$short_url = $check_url->row()->short_url;
				}else{
					$short_url = $this->get_rand_str('6');
					$checkId = $this->group_gift->get_all_details(SHORTURL,array('short_url'=>$short_url));
					while($checkId->num_rows()>0){
						$short_url = $this->get_rand_str('6');
						$checkId = $this->group_gift->get_all_details(SHORTURL,array('short_url'=>$short_url));
					}
					$url = $searcharray['long_url'].$gift_seller_id;
					$this->group_gift->simple_insert(SHORTURL,array('short_url'=>$short_url,'long_url'=>$url,'product_id'=>$gift_seller_id,'user_id'=>$this->checkLogin('U')));
					$get_url = $this->group_gift->get_all_details(SHORTURL,array('short_url'=>$short_url,'long_url'=>$url,'product_id'=>$gift_seller_id,'user_id'=>$this->checkLogin('U')));
					if($get_url->num_rows() >0){
						$short_url_id = $get_url->row()->id; 
					}
				}
				if($file_name!=''){
					$dataArr = array('user_id'=>$this->checkLogin('U'),'gift_seller_id'=>$gift_seller_id,
					'status' =>'Inactive','short_url_id'=>$short_url_id,'recipient_image'=>$file_name,'sell_id'=>$product_details->row()->user_id,'quantity'=>$searcharray['gift_qty']);
				}else{
					$dataArr = array('user_id'=>$this->checkLogin('U'),'gift_seller_id' =>$gift_seller_id,
					'status' =>'Inactive','short_url_id'=>$short_url_id,'sell_id'=>$product_details->row()->user_id,'quantity'=>$searcharray['gift_qty']);
				}
				$dataArr['recipient_name'] = $searcharray['recipient_name'];
				$dataArr['recipient_fullname'] = $searcharray['recipient_fullname'];
				$dataArr['address1'] = $searcharray['address1'];
				$dataArr['address2'] = $searcharray['address2'];
				$dataArr['country'] = $searcharray['country'];
				$dataArr['state'] = $searcharray['state'];
				$dataArr['city'] = $searcharray['city'];
				$dataArr['zip_code'] = $searcharray['zip_code'];
				$dataArr['telephone'] = $searcharray['telephone'];
				$dataArr['product_id'] = $searcharray['product_id'];
				$dataArr['price'] = $searcharray['gift_price'];
				$this->group_gift->commonInsertUpdate(GROUP_GIFTS,'insert',$excludeArr,$dataArr,array());
				$returnArr['shippingval'] = $shippingCost.'+'.$tax.'+'.$searcharray['country'].'+'.$searcharray['gift_qty'].'+'.$gift_seller_id.'+'.$searcharray['gift_price'];
			}
			echo json_encode($returnArr);
		}else{
			redirect('login');
		}
	}
	public function updateGrouptProduct(){
		if($this->input->post('cancelled')== ''){
			$excludeArr = array('seller_id');
			$dataArr =array();
			$this->group_gift->commonInsertUpdate(GROUP_GIFTS,'update',$excludeArr,$dataArr,array('gift_seller_id'=>$this->input->post('seller_id')));
		}else{
			$excludeArr = array('group_id','cancelled');
			$dataArr =array('status'=>'Cancelled');
			$this->group_gift->commonInsertUpdate(GROUP_GIFTS,'update',$excludeArr,$dataArr,array('gift_seller_id'=>$this->input->post('group_id')));
		}
	}
	public function search_user(){
		if($this->checkLogin('U')!=''){
			$search = $this->input->post('search');
			$Query = "SELECT * FROM ".USERS." WHERE user_name LIKE '%$search%' AND status ='Active'";
			$search_result = $this->group_gift->ExecuteQuery($Query);
			if($search_result->num_rows() >0){
				foreach($search_result->result() as $result){
					$imgArr = array_filter(explode(',', $result->thumbnail));
					$img = 'user-thumb1.png';
					foreach($imgArr as $imgVal){
						if($imgVal != ''){
							$img = $imgVal;
							break;
						}
					}
					echo '<div class="show" align="left">';
					echo '<span class="name"><img width="40" height="40" src="images/users/'.$img.'"/>'.$result->user_name.'</span>&nbsp;<br/></div>';
				}
			}
		}else{
			redirect('login');
		}
	}
	public function get_user_result(){
		if($this->checkLogin('U')!=''){
			$username = trim($this->input->post('user_name'));
			$Query = "SELECT * FROM ".USERS." WHERE user_name = '$username' AND status ='Active'";
			$get_user_details = $this->group_gift->ExecuteQuery($Query);
			if($get_user_details->num_rows() ==1){
				$values = $get_user_details->row()->full_name.'+'.$get_user_details->row()->user_name.'+'.$get_user_details->row()->thumbnail.'+'.$get_user_details->row()->address.'+'.$get_user_details->row()->address2.'+'.$get_user_details->row()->city.'+'.$get_user_details->row()->state.'+'.$get_user_details->row()->country.'+'.$get_user_details->row()->postal_code.'+'.$get_user_details->row()->phone_no;
				echo $values;
			} 
		}else{
			redirect('login');
		}
	}
	public function display_groupProduct_detail(){
		if($this->checkLogin('U')!=''){
			$p_id = $this->uri->segment(2,0);
			$Query = "select gf.*,p.id as pid,p.seller_product_id,p.product_name,p.seourl,p.likes,p.image,p.description,u.user_name,u.thumbnail,st.short_url from ".GROUP_GIFTS." as gf join ".PRODUCT." as p on (gf.product_id = p.seller_product_id) join ".USERS." as u on (gf.user_id = u.id) join ".SHORTURL." as st on (st.id = gf.short_url_id) where gf.gift_seller_id = ".$p_id."";
			$gift_ProductDetails = $this->group_gift->ExecuteQuery($Query);
			//echo "<pre>";print_r($gift_ProductDetails->result()); die;
			if($gift_ProductDetails->num_rows() ==1){
				$this->data['gift_ProductDetails'] = $gift_ProductDetails;
				$condition = array('user_id'=> $this->checkLogin('U'),'group_product_id'=> $gift_ProductDetails->row()->gift_seller_id);
				$group_product_count = $this->group_gift->get_all_details(GROUPGIFT_COMMENTS,$condition);
				$this->data['group_count'] = $group_product_count;
				$Query = "select gf.*,u.full_name,u.user_name,u.thumbnail,c.comments ,u.email,c.id,c.status,c.user_id as CUID from ".GROUPGIFT_COMMENTS." c LEFT JOIN ".USERS." u on u.id=c.user_id LEFT JOIN ".GROUP_GIFTS." gf on gf.gift_seller_id=c.group_product_id where c.user_id = ".$this->checkLogin('U')." AND c.group_product_id = ".$gift_ProductDetails->row()->gift_seller_id."";
				$this->data['giftproductComment'] = $this->group_gift->ExecuteQuery($Query);
				
			}

			$contribute_details= $this->group_gift->get_all_details(GROUPGIFT_PAYMENT,array('groupgift_id'=>$gift_ProductDetails->row()->gift_seller_id,'status'=>'Paid'));
			$this->data['contribute_details'] = $contribute_details;
			$cond = "select * from ".GROUPGIFT_SETTINGS." where id = '1'";
			$this->data['expiry_date'] = $this->group_gift->ExecuteQuery($cond);
			
			$this->data['heading'] = 'Group Gifts Product';
			$this->load->view('site/groupgifts/groupgifts_product_detail',$this->data);
		}else{
			redirect('login');
		}
	}
	public function payment_contribute_details(){
		if($this->checkLogin('U')!=''){
			$p_id = $this->uri->segment(3,0);
			$Query = "select gf.*,p.id as pid,p.seller_product_id,p.product_name,p.seourl,p.likes,p.image,p.description,u.user_name,u.thumbnail,u.email from ".GROUP_GIFTS." as gf join ".PRODUCT." as p on (gf.product_id = p.seller_product_id) join ".USERS." as u on (gf.user_id = u.id) where gf.gift_seller_id = ".$p_id." and gf.status ='Active'";
			$gift_ProductDetails = $this->group_gift->ExecuteQuery($Query);
			//echo"<pre>"; print_r($gift_ProductDetails->result()); die;
			if($gift_ProductDetails->num_rows() ==1){
			$this->data['gift_ProductDetails'] = $gift_ProductDetails;
			$this->data['sellerDetails'] = $this->group_gift->get_all_details(USERS,array('id'=>$gift_ProductDetails->row()->sell_id));
			$contribute_details= $this->group_gift->get_all_details(GROUPGIFT_PAYMENT,array('groupgift_id'=>$gift_ProductDetails->row()->gift_seller_id,'status'=>'Paid'));
			$this->data['contribute_details'] = $contribute_details;
			$cond = "select * from ".GROUPGIFT_SETTINGS."";
			$this->data['expiry_date'] = $this->group_gift->ExecuteQuery($cond);
			$this->data['country_list'] = $this->group_gift->get_all_details(COUNTRY_LIST,array('status'=>'Active'));
			$this->data['heading'] = 'Group Gift Contrbutes';
			$this->load->view('site/groupgifts/contribute_detail',$this->data);
			}else{
				redirect('gifts/'.$p_id);
			}
		}else{
			redirect('login');
		}
	}
	public function groupgift_comment(){
		$returnStr['status_code'] = 0;
		if($this->checkLogin('U')!=''){
			$productId = $this->input->post('group_product_id');
			$comments = $this->input->post('comments');
			$datestring = "%Y-%m-%d %h:%i:%s";
			$time = time();
			$createdTime = mdate($datestring,$time);
			$excludeArr = array();
			$dataArr = array('user_id' =>$this->checkLogin('U'),'Status' => "Inactive");
			$this->group_gift->commonInsertUpdate(GROUPGIFT_COMMENTS,'insert',$excludeArr,$dataArr,array());
			$cmtID = $this->group_gift->get_last_insert_id();
			preg_match_all("/(@\w+)/", $comments, $hash_tags);
			if(count($hash_tags[0])>0){
				foreach($hash_tags[0] as $row){
					$user_name = substr($row,1);
					$userCheck = $this->group_gift->get_all_details(USERS,array('user_name'=>$user_name,'status'=>'Active'));
					if ($userCheck->num_rows()==1){
						$actArr = array(
							'activity'		=>	'tagged',
							'activity_id'	=>	$userCheck->row()->id,
							'user_id'		=>	$this->checkLogin('U'),
							'activity_ip'	=>	$this->input->ip_address(),
							'created'		=>	$createdTime,
							'comment_id'	=> $cmtID
						);
						$this->group_gift->simple_insert(NOTIFICATIONS,$actArr);
					}
				}
			}
			preg_match_all("/(#\w+)/", $comments, $hash_tags);
			if(count($hash_tags[0])>0){
				foreach ($hash_tags[0] as $row){
					if($row != ''){
						$dup_check = $this->group_gift->get_all_details(TAGS_PRODUCT,array('tag_name'=>$row));
						if ($dup_check->num_rows() == 0){
							$dataArr = array(
								'tag_name' 	=> $row,
								'user_id'	=> $uid,
								'products'	=> $productId,
								'products_count'	=> 1
							);
							$this->group_gift->simple_insert(TAGS_PRODUCT,$dataArr);
						}else{
							$product_id_arr = array_unique(array_filter(explode(',', $dup_check->row()->products)));
							if (!in_array($productId, $product_id_arr)){
								$product_id_arr[] = $productId;
								$products_count = count($product_id_arr);
								$dataArr = array(
									'products'	=>	implode(',', $product_id_arr),
									'products_count' => $products_count
								);
								$this->group_gift->update_details(TAGS_PRODUCT,$dataArr,array('tag_name'=>$row));
							}
						}
						$actArr = array(
									'activity'		=>	'own-product-comment',
									'activity_id'	=>	$productId,
									'user_id'		=>	$this->checkLogin('U'),
									'activity_ip'	=>	$this->input->ip_address(),
									'created'		=>	$createdTime,
									'comment_id'	=> $cmtID
						);
						$this->group_gift->simple_insert(NOTIFICATIONS,$actArr);
					}
				}
			}
			/* $this->send_comment_noty_mail($cmtID,$productId);
			$this->send_comment_noty_mail_to_admin($cmtID,$productId); */
			$returnStr['status_code'] = 1;
		}else{
			redirect('login');  
		}
		echo json_encode($returnStr);
	}
	public function send_comment_noty_mail($cmtID='0',$pid='0'){
		if ($this->checkLogin('U')!=''){
			if ($cmtID != '0' && $pid != '0'){
				$productUserDetails = $this->group_gift->get_product_full_details($pid);
				if ($productUserDetails->num_rows()==1){
					$emailNoty = explode(',', $productUserDetails->row()->email_notifications);
					if (in_array('comments', $emailNoty)){
						$commentDetails = $this->group_gift->view_product_comments_details('where c.id='.$cmtID);
						if ($commentDetails->num_rows() == 1){
							if ($productUserDetails->prodmode == 'seller'){
								$prodLink = base_url().'things/'.$productUserDetails->row()->id.'/'.url_title($productUserDetails->row()->product_name,'-');
							}else {
								$prodLink = base_url().'user/'.$productUserDetails->row()->user_name.'/things/'.$productUserDetails->row()->seller_product_id.'/'.url_title($productUserDetails->row()->product_name,'-');
							}

							$newsid='8';
							$template_values=$this->order_model->get_newsletter_template_details($newsid);
							$adminnewstemplateArr=array('email_title'=> $this->config->item('email_title'),'logo'=> $this->data['logo'],'full_name'=>$commentDetails->row()->full_name,'product_name'=>$productUserDetails->row()->product_name,'user_name'=>$commentDetails->row()->user_name);
							extract($adminnewstemplateArr);
							$subject = $template_values['news_subject'];

							$message .= '<!DOCTYPE HTML>
								<html>
								<head>
								<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
								<meta name="viewport" content="width=device-width"/>
								<title>'.$template_values['news_subject'].'</title>
								<body>';
							include('./newsletter/registeration'.$newsid.'.php');

							$message .= '</body>
								</html>';

							if($template_values['sender_name']=='' && $template_values['sender_email']==''){
								$sender_email=$this->data['siteContactMail'];
								$sender_name=$this->data['siteTitle'];
							}else{
								$sender_name=$template_values['sender_name'];
								$sender_email=$template_values['sender_email'];
							}

							$email_values = array('mail_type'=>'html',
												'from_mail_id'=>$sender_email,
												'mail_name'=>$sender_name,
												'to_mail_id'=>$productUserDetails->row()->email,
												'subject_message'=>$subject,
												'body_messages'=>$message
							);
							$email_send_to_common = $this->group_gift->common_email_send($email_values);
						}
					}
				}
			}
		}
	}

	public function send_comment_noty_mail_to_admin($cmtID='0',$pid='0'){
		if ($this->checkLogin('U')!=''){
			if ($cmtID != '0' && $pid != '0'){
				$productUserDetails = $this->group_gift->get_product_full_details($pid);
				if ($productUserDetails->num_rows()==1){
					$commentDetails = $this->group_gift->view_product_comments_details('where c.id='.$cmtID);
					if ($commentDetails->num_rows() == 1){
						if ($productUserDetails->prodmode == 'seller'){
							$prodLink = base_url().'things/'.$productUserDetails->row()->id.'/'.url_title($productUserDetails->row()->product_name,'-');
						}else {
							$prodLink = base_url().'user/'.$productUserDetails->row()->user_name.'/things/'.$productUserDetails->row()->seller_product_id.'/'.url_title($productUserDetails->row()->product_name,'-');
						}

						$newsid='20';
						$template_values=$this->order_model->get_newsletter_template_details($newsid);
						$adminnewstemplateArr=array('email_title'=> $this->config->item('email_title'),'logo'=> $this->data['logo'],'full_name'=>$commentDetails->row()->full_name,'product_name'=>$productUserDetails->row()->product_name,'user_name'=>$commentDetails->row()->user_name);
						extract($adminnewstemplateArr);
						$subject = $template_values['news_subject'];

						$message .= '<!DOCTYPE HTML>
								<html>
								<head>
								<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
								<meta name="viewport" content="width=device-width"/>
								<title>'.$template_values['news_subject'].'</title>
								<body>';
						include('./newsletter/registeration'.$newsid.'.php');

						$message .= '</body>
								</html>';

						if($template_values['sender_name']=='' && $template_values['sender_email']==''){
							$sender_email=$this->data['siteContactMail'];
							$sender_name=$this->data['siteTitle'];
						}else{
							$sender_name=$template_values['sender_name'];
							$sender_email=$template_values['sender_email'];
						}

						$email_values = array('mail_type'=>'html',
												'from_mail_id'=>$sender_email,
												'mail_name'=>$sender_name,
												'to_mail_id'=>$this->data['siteContactMail'],
												'subject_message'=>$subject,
												'body_messages'=>$message
						);
						$email_send_to_common = $this->group_gift->common_email_send($email_values);
					}
				}
			}
		}
	}
	public function approve_comment(){
		$returnStr['status_code'] = 0;
		if ($this->checkLogin('U')!=''){
			$cid = $this->input->post('cid');
			$this->group_gift->update_details(GROUPGIFT_COMMENTS,array('status'=>'Active'),array('id'=>$cid));
			$returnStr['status_code'] = 1;
		}
		echo json_encode($returnStr);
	}	
	public function delete_comment(){
		$returnStr['status_code'] = 0;
		if ($this->checkLogin('U')!=''){
			$cid = $this->input->post('cid');
			$this->group_gift->commonDelete(GROUPGIFT_COMMENTS,array('id'=>$cid));
			$returnStr['status_code'] = 1;
		}
		echo json_encode($returnStr);
	}
	public function load_short_url(){
		$short_url = $this->uri->segment(2,0);
		if ($short_url != ''){
			$url_details = $this->group_gift->get_all_details(GROUPGIFT_SHORTURL,array('short_url'=>$short_url));
			if ($url_details->num_rows()==1){
				redirect($url_details->row()->long_url);
			}else {
				show_error('Invalid short url provided. Make sure the url you typed is correct. <a href="'.base_url().'">Click here</a> to go home page.','404','Invalid Url');
			}
		}
	}
	public function getCountry(){
		$country_list = $this->group_gift->get_all_details(COUNTRY_LIST,array('status'=>'Active'));
		if($country_list->num_rows() >0){
			foreach($country_list->result() as $country){
				echo "<option value =".$country->country_code.">".$country->name."</option>";
			}  
		}
	}
	public function getcurrentCountry(){
		$code = $this->input->post('code');
		$country_list = $this->group_gift->get_all_details(COUNTRY_LIST,array('status'=>'Active'));
		if($country_list->num_rows() >0){
			foreach($country_list->result() as $country){
				if($country->country_code == $code){
					echo "<option value =".$country->country_code." selected='selected'>".$country->name."</option>";
				}else{
					echo "<option value =".$country->country_code.">".$country->name."</option>";
				}
			}  
		}
	}
	public function suggest_friends(){
		if($this->checkLogin('U')!=''){
			$following_id = $this->data['userDetails']->row()->following;
			$ids = array_filter(explode(',',$following_id));
			$limit =0;
			foreach($ids as $id){
			   if($limit>=2){
				 break;
			   }
			   $Query = "select * from ".USERS." where id =".$id." AND status='Active'";
			   $sug_user_Details = $this->group_gift->ExecuteQuery($Query);
			   if($sug_user_Details->num_rows()==1){
				   if($sug_user_Details->row()->thumbnail!=''){
					  $user_image = $sug_user_Details->row()->thumbnail;
				   }else{
					  $user_image = "user-thumb1.png";
				   }
				   $html = '<li class="inner_suggested_friends" id="'.$limit.'" onclick="sug_fri_li(this.id);">';
				   $html .= '<img id="img-'.$limit.'" src="images/users/'.$user_image.'"/><br/>';
				   $html .= '<span id="user-'.$limit.'">'.$sug_user_Details->row()->user_name.'</span>';
				   $html .= '<input id="image-'.$limit.'" type="hidden" name="sug_img" value="'.$user_image.'">';
				   $html .='</li>';
				   echo $html;
			   }
			   $limit++;		   
			}
		}else{
			redirect('login');
		}
	}
	public function order_payment(){
		if($this->uri->segment(3)=='success'){
			$condition = array('DealcodeNo'=>$this->uri->segment(5),'user_id'=>$this->uri->segment(4));
			$excludeArr = array();
			$dataArr = array('status'=>'Paid');
			$this->group_gift->commonInsertUpdate(GROUPGIFT_PAYMENT,'update',$excludeArr,$dataArr,$condition);
			/****************Check status***********/
				$giftDetails = $this->group_gift->get_all_details(GROUPGIFT_PAYMENT,$condition);
				$contributeAmt = "select sum(amount) as totalAmt from ".GROUPGIFT_PAYMENT." where groupgift_id = '".$giftDetails->row()->groupgift_id."'";
				$contributorsAmt = $this->group_gift->ExecuteQuery($contributeAmt);
				$giftAmt = "select gf.*,p.product_name,p.image from ".GROUP_GIFTS." as gf left join ".PRODUCT." as p on (p.id = gf.product_id) where gift_seller_id = '".$giftDetails->row()->groupgift_id."'";
				$giftdetailsAmt = $this->group_gift->ExecuteQuery($giftAmt);
				if($giftdetailsAmt->row()->gift_total == $contributorsAmt->row()->totalAmt){
					$excludeArr1 = array();
					$dataArr1 = array('status'=>'Successful');
					$condition1 = array('gift_seller_id'=>$giftdetailsAmt->row()->gift_seller_id);
					$this->group_gift->commonInsertUpdate(GROUP_GIFTS,'update',$excludeArr1,$dataArr1,$condition1);
				}
			
			/*****************End*******************/
			/************Mail Function**************/
				/*********************Mail to Admin*******************/
				$AdminEmail = $this->config->item('site_contact_mail');
				$UserDetails = $this->group_gift->get_all_details(USERS,array('id'=>$this->uri->segment(4)));
				$UserEmail = $UserDetails->row()->email;
				$groupgiftId = $giftdetailsAmt->row()->gift_seller_id;
				$groupgiftName = $giftdetailsAmt->row()->gift_name;
				$fullname = $UserEmail = $UserDetails->row()->user_name;
				$productName = $giftdetailsAmt->row()->product_name;
				$productImage = $giftdetailsAmt->row()->image;
				$amountPaid = $giftDetails->row()->amount;
				
				$newsid='28';
				$template_values=$this->group_gift->get_newsletter_template_details($newsid);
				$subject = 'From: '.$this->config->item('email_title').' - '.$template_values['news_subject'];
				$adminnewstemplateArr=array('email_title'=> $this->config->item('email_title'),'logo'=> $this->data['logo']);
				extract($adminnewstemplateArr);
				//$ddd =htmlentities($template_values['news_descrip'],null,'UTF-8');
				$header .="Content-Type: text/plain; charset=ISO-8859-1\r\n";
				$message .= '<!DOCTYPE HTML>
					<html>
					<head>
					<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
					<meta name="viewport" content="width=device-width"/><body>';
				include('./newsletter/registeration'.$newsid.'.php');
				$message .= '</body>
					</html>';
				if($template_values['sender_name']=='' && $template_values['sender_email']==''){
					$sender_email=$this->data['siteContactMail'];
					$sender_name=$this->data['siteTitle'];
				}else{
					$sender_name=$template_values['sender_name'];
					$sender_email=$template_values['sender_email'];
				}
				$email_values = array('mail_type'=>'html',
					'from_mail_id'=>$sender_email,
					'mail_name'=>$sender_name,
					'to_mail_id'=>$UserEmail,
					'cc_mail_id'=>$AdminEmail,
					'subject_message'=>$template_values['news_subject'],
					'body_messages'=>$message,
					'mail_id'=>'register mail'
					);
				$email_send_to_common = $this->product_model->common_email_send($email_values);
			/*****************End*******************/
			$this->data['Confirmation'] = 'contributor';
			$this->load->view('site/order/order.php',$this->data);
		}elseif($this->uri->segment(3)=='failure'){
			$condition = array('DealcodeNo'=>$this->uri->segment(5),'user_id'=>$this->uri->segment(4));
			$this->data['contriDetails'] = $this->group_gift->get_all_details(GROUPGIFT_PAYMENT,$condition);
			$this->data['Confirmation'] = 'contributor-failure';
			$this->load->view('site/order/order.php',$this->data);
		}
	}
	public function seller_payment(){
		if($this->uri->segment(3)=='success'){
			$condition = array('sell_id'=>$this->uri->segment(4),'transactionId'=>$this->uri->segment(5));
			$excludeArr = array();
			$dataArr = array('status'=>'Paid');
			$this->group_gift->commonInsertUpdate(GROUPGIFT_PAYTO_SELLER,'update',$excludeArr,$dataArr,$condition);
			/************Mail Function**************/
				/* $newsid='24';
				$template_values=$this->group_gift->get_newsletter_template_details($newsid);
				$subject = 'From: '.$this->config->item('email_title').' - '.$template_values['news_subject'];
				$adminnewstemplateArr=array('email_title'=> $this->config->item('email_title'),'logo'=> $this->data['logo']);
				extract($adminnewstemplateArr);
				//$ddd =htmlentities($template_values['news_descrip'],null,'UTF-8');
				$header .="Content-Type: text/plain; charset=ISO-8859-1\r\n";
				$message .= '<!DOCTYPE HTML>
					<html>
					<head>
					<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
					<meta name="viewport" content="width=device-width"/><body>';
				include('./newsletter/registeration'.$newsid.'.php');
				$message .= '</body>
					</html>';
				if($template_values['sender_name']=='' && $template_values['sender_email']==''){
					$sender_email=$this->data['siteContactMail'];
					$sender_name=$this->data['siteTitle'];
				}else{
					$sender_name=$template_values['sender_name'];
					$sender_email=$template_values['sender_email'];
				}
				$email_values = array('mail_type'=>'html',
					'from_mail_id'=>$sender_email,
					'mail_name'=>$sender_name,
					'to_mail_id'=>$UserEmail,
					'cc_mail_id'=>$AdminEmail,
					'subject_message'=>$template_values['news_subject'],
					'body_messages'=>$message,
					'mail_id'=>'register mail'
					);
				$email_send_to_common = $this->product_model->common_email_send($email_values); */
			/*****************End*******************/
			$this->setErrorMessage('success','Payment Completed Sucessfully');
			redirect('admin/groupgift/contributors_paid');
		}else{
			$this->setErrorMessage('error',$this->uri->segment(3));
			redirect('admin/groupgift/contributors_paid');
		}
	}
	public function getShippingCost(){
		$country = $this->input->post('country');
		$Query = "select sc.* from ".SHIPPING_COST." as sc left join ".PRODUCT." as p on (p.seller_product_id = sc.product_id) where sc.country_code ='".$country."' and sc.product_id = '".$this->input->post('pid')."'";
		$productShip = $this->group_gift->ExecuteQuery($Query);
		if($productShip->num_rows()==1){
			echo $seprate_cost = $productShip->row()->separate_ship_cost;
		}else{
			echo "0";
		}
	}
	public function update_expire(){
		 $gift_seller_id = $this->input->post('ids');
		 $excludeArr = array('ids');
		 $dataArr =array();
		 $this->group_gift->commonInsertUpdate(GROUP_GIFTS,'update',$excludeArr,$dataArr,array('id'=>$this->input->post('id')));
		 redirect('gifts/'.$gift_seller_id);
	}
	public function delete_gift(){
		$id = $this->input->post('seller_id');
		$this->group_gift->commonDelete(GROUP_GIFTS,array('gift_seller_id'=>$id));
		echo "1";
	}	
	public function validate_country(){
		$code = $this->input->post('country');
		$pid = $this->input->post('pid');
		$productDetails = $this->group_gift->get_all_details(PRODUCT,array('seller_product_id'=>$pid));
		if($productDetails->num_rows()==1){
			if($productDetails->row()->served_specific==1){
				$contryDetails = $this->group_gift->get_all_details(SHIPPING_COST,array('product_id'=>$pid,'country_code'=>$code));
				if($contryDetails->num_rows()==1){
					$returnArr['status'] = 1;
				}
				else{
					$returnArr['status'] = 0;
				}
			}else{
				$returnArr['status'] = 2;
			}
		}
		echo json_encode($returnArr);
	}
	
}
/* End of file user_settings.php */
/* Location: ./application/controllers/site/user_settings.php */