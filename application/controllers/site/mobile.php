<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * User related functions
 * @author Teamtweaks
 *
 */

class Mobile extends MY_Controller {
	function __construct(){

        parent::__construct();
		$this->load->helper(array('cookie','date','form','email'));
		$this->load->library(array('encrypt','form_validation'));
		$this->load->model(array('order_model','mobile_model','product_model','user_model','cart_model','minicart_model','groupgift_model','store_model','giftcards_model'));
		if($_SESSION['sMainCategories'] == ''){
			$sortArr1 = array('field'=>'cat_position','type'=>'asc');
			$sortArr = array($sortArr1);
			$_SESSION['sMainCategories'] = $this->product_model->get_all_details(CATEGORY,array('rootID'=>'0','status'=>'Active'),$sortArr);
		}
			/* $ua = strtolower($_SERVER['HTTP_USER_AGENT']);
			if(stripos($ua,'shopsymobileapp') === false) {
			show_404();
			} */
			//	$commonId=intval($_GET['commonId']);
			//	$cartCount=0;
			// if($commonId>0){
			// 	$cartCount=$this->mobile_model->mini_cart_view($commonId);
			// }
			// $this->data["cartCount"]=$cartCount;
			// $this->data["commonId"]=$commonId;
			// $temp_id = substr(number_format(time() * rand(),0,'',''),0,8);
			// $this->data['likedProducts'] = $this->mobile_model->get_all_details(PRODUCT_LIKES, array('user_id' => $commonId));
			// $this->data['orderpurchaseProducts'] = $this->mobile_model->get_orderpurchase_details($commonId);
			// $GlobalUserName='';

		$UserId = intval($_GET['UserId']);
		if($UserId == ""){
			$UserId = intval($_POST['UserId']);
		}
		if($UserId != ''){
			$UserProfileDetails = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
			$user_country = $UserProfileDetails->row()->country;
			$check_country_lg  = $this->product_model->get_all_details(LANGUAGES,array('country_code'=>$user_country,'status'=>'Active'));
			if ($check_country_lg->num_rows()>0){
				$defaultLanguage = $check_country_lg->row()->lang_code;
			} else {
				$defaultLanguage = $this->data['defaultLg'][0]['lang_code'];
			}
			$selectedLanguage = $UserProfileDetails->row()->language;
			if ($defaultLanguage==''){
				$defaultLanguage = 'en';
			}
			$filePath = APPPATH."language/".$selectedLanguage."/".$selectedLanguage."_lang.php";
			if($selectedLanguage != ''){
				if(!(is_file($filePath))){
					$this->lang->load($defaultLanguage, $defaultLanguage);
					$this->data['selectedLanguage']=$defaultLanguage;
				}else{
					$this->lang->load($selectedLanguage, $selectedLanguage);
					$this->data['selectedLanguage']=$selectedLanguage;
				}
			}
			else{
				$this->lang->load($defaultLanguage, $defaultLanguage);
				$this->data['selectedLanguage']=$defaultLanguage;
			}
		}

		/*Currency Settings*/
		// if($UserId > 0){
		// 	$checkUserPreference=$this->mobile_model->get_all_details(USERS,array('id' => $UserId));
		// 	if($checkUserPreference->num_rows()>=1){
		// 		$countryCurrency=$this->mobile_model->get_all_details(COUNTRY_LIST,array('country_code' => $checkUserPreference->row()->country));
		// 		$condition = array('currency_type'=> $countryCurrency->row()->currency_type);
		// 		if($countryCurrency->row()->currency_type == ""){
		// 			$condition = array('currency_type'=>'USD');
		// 		}
		// 		$GlobalUserName=$checkUserPreference->row()->user_name;
		// 	}else{
		// 		$condition = array('currency_type'=>'USD');
		// 	}
		// } else {
		// 	$condition = array('currency_type'=>'USD');
		// }
		// $CurrencyVal=$this->product_model->get_all_details(COUNTRY_LIST,$condition);
		// $this->data["currencySymbol"]=$CurrencyVal->row()->currency_symbol;
		// $this->data["currencyCode"]=$CurrencyVal->row()->currency_type;
		#echo $this->data["currencySymbol"].'|'.$this->data["currencyCode"].'|'.$this->data["currencyValue"];
    }


	/**
	 *
	 * Loading Category Json Page
	*/

	public function index(){
		$this->db->select('id,cat_name,image,rootID');
		$this->db->from(CATEGORY);
		$this->db->where('status','Active');
		$CategoryVal = $this->db->get();
		#echo '<pre>'; print_r($CategoryVal);
	}

	public function send_confirm_mail($userDetails=''){
		$uid = $userDetails->row()->id;
		$email = $userDetails->row()->email;
		$name = $userDetails->row()->full_name;
		$randStr = $this->get_rand_str('10');
		$condition = array('id'=>$uid);
		$dataArr = array('verify_code'=>$randStr);
		$this->db->where('id', $uid);
		$this->db->update(USERS, $dataArr);
		$newsid='3';
		$this->db->select('*');
		$this->db->from(NEWSLETTER);
		$this->db->where('id',$newsid);
		$this->db->where('status','Active');
		$template_values = $this->db->get()->result_array();
		$cfmurl = base_url().'site/user/confirm_register/'.$uid."/".$randStr."/confirmation";
		$subject = 'From: '.$this->config->item('email_title').' - '.$template_values[0]['news_subject'];
		$adminnewstemplateArr=array('email_title'=> $this->config->item('email_title'),'logo'=> $this->data['logo'],'footer_content'=> $this->config->item('footer_content'));
		extract($adminnewstemplateArr);
		//$ddd =htmlentities($template_values['news_descrip'],null,'UTF-8');
		$header .="Content-Type: text/plain; charset=ISO-8859-1\r\n";
		$message .= '<!DOCTYPE HTML>
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
			<meta name="viewport" content="width=device-width"/><body>';
		include('./newsletter/registeration'.$newsid.'.php');
		$message .= '</body>
			</html>';
		if($template_values[0]['sender_name']=='' && $template_values[0]['sender_email']==''){
			$sender_email=$this->data['siteContactMail'];
			$sender_name=$this->data['siteTitle'];
		}else{
			$sender_name=$template_values[0]['sender_name'];
			$sender_email=$template_values[0]['sender_email'];
		}
		$email_values = array('mail_type'=>'html',
							'from_mail_id'=>$sender_email,
							'mail_name'=>$sender_name,
							'to_mail_id'=>$email,
							'subject_message'=>$template_values[0]['news_subject'],
							'body_messages'=>$message
							);
		$email_send_to_common = $this->mobile_model->common_email_send($email_values);
	}


	public function send_message(){
		$subject='';$msg='';$msgId='';
		$sender_id=$this->input->post('userId');
		$receiver_id=$this->input->post('receiverId');
		$tid=intval($this->input->post('convId'));
		$subject=$this->input->post('subject');
		$message=$this->input->post('message');
		$selectCol="`id`,`email`,`user_name`";
		$senderVal = $this->mobile_model->get_column_details(USERS,array('id'=>$sender_id),$selectCol);
		$receiverVal = $this->mobile_model->get_column_details(USERS,array('id'=>$receiver_id),$selectCol);
		if($senderVal->num_rows()>0 && $receiverVal->num_rows()>0){
			$sender_id=$senderVal->row()->id;
			$sender_email=$senderVal->row()->email;
			$sender_name=$senderVal->row()->user_name;
			$receiver_id=$receiverVal->row()->id;
			$receiver_email=$receiverVal->row()->email;
			$receiver_name=$receiverVal->row()->user_name;
			if($senderVal->row()->thumbnail!=""){
				$senderImage=base_url().'images/users/thumb/'.$senderVal->row()->thumbnail;
			}else{
				$senderImage=base_url().'images/users/thumb/profile_pic.png';
			}
			if($subject!="" && $message!=""){
				if($tid == '' || $tid =='0'){
					$tid = time();
				}
				$datestring = "%Y-%m-%d %H:%i:%s";
				$time = time();
				$createdTime = mdate($datestring,$time);
				$msgTime=date('Y-m-d H:i:s');
				$dataArr = array('sender_email'=>$sender_email,
											'sender_id'=>$sender_id,
											'receiver_email'=>$receiver_email,
											'receiver_id'=>$receiver_id,
											'subject'=>$subject,
											'message'=>$message,
											'dataAdded'=>$createdTime,
											'tid'=>$tid);
				$this->mobile_model->simple_insert(CONTACTPEOPLE,$dataArr);
				$subject=trim(strip_tags($subject));
				$msg=trim(strip_tags($message));
				$mTime=$this->get_relative_by_date($createdTime);
				$msgId=$this->db->insert_id();
				$actArr = array('activity'=>"message",
										'activity_id'=>$receiver_id,
										'user_id'	=>$sender_id,
										'activity_ip'=>$this->input->ip_address(),
										'created'=>$createdTime,
										'comment_id'=>$tid);
				$this->mobile_model->simple_insert(NOTIFICATIONS,$actArr);
				$pmessage=$message;
				/*Push Message Starts*/
				$message='You received a message from '.$sender_name.' on '.$this->config->item('email_title');
				$type='message';
		//		$this->sendPushNotification($receiver_id,$message,$type,array($tid,$sender_id,$senderImage,$sender_name,$pmessage,$subject,$msgId));
				/*Push Message Ends*/
				$status=1;
				$message="Sent Successfully";
			}else{
				$status=0;
				$message="Values are missing";
			}
		}else{
			$status=0;
			$message="Sending Failed";
		}
		$json_encode = json_encode(array("status" =>(string)$status,"message" =>$message,"subject" =>(string)$subject,"msg" =>(string)$msg,"msgId" =>(string)$msgId,"msgTime" =>(string)$mTime));
		echo $json_encode;
	}


	public function send_user_password($pwd='',$query,$code,$id){
		$newsid='5';
		$template_values=$this->mobile_model->get_newsletter_template_details($newsid);
		$adminnewstemplateArr=array('email_title'=> $this->config->item('email_title'),'logo'=> $this->data['logo']);
		extract($adminnewstemplateArr);
		$pwdlnk=base_url().'resetPassword/'.$code.'/'.$id.'';
		//$cfmurl = base_url().'site/user/confirm_change_email/'.$uid."/".$encode_email."/".$randStr."/confirmation";
		$subject = 'From: '.$this->config->item('email_title').' - '.$template_values['news_subject'];
  		$message .= '<!DOCTYPE HTML>
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
			<meta name="viewport" content="width=device-width"/>
			<title>'.$template_values['news_subject'].'</title>
			<body>';
		include('./newsletter/registeration'.$newsid.'.php');
		$message .= '</body>
			</html>';
		if($template_values['sender_name']=='' && $template_values['sender_email']==''){
			$sender_email=$this->config->item('site_contact_mail');
			$sender_name=$this->config->item('email_title');
		}else{
			$sender_name=$template_values['sender_name'];
			$sender_email=$template_values['sender_email'];
		}
		$email_values = array('mail_type'=>'html',
							'from_mail_id'=>$sender_email,
							'mail_name'=>$sender_name,
							'to_mail_id'=>$query->row()->email,
							'subject_message'=>'Password Reset',
							'body_messages'=>$message
							);
		$email_send_to_common = $this->mobile_model->common_email_send($email_values);
	}

	public function user_login(){
		$returnArr['status'] = '0';
		$returnArr['message']='';
		$UserNamestr = $this->input->post('username');
		$PassWordstr = $this->input->post('password');
		if($UserNamestr !="" && $PassWordstr !=""){
			$condition = '(email = \''.$UserNamestr.'\' OR user_name = \''.$UserNamestr.'\') AND password=\''.md5($PassWordstr).'\'';
			$userInfoDetails = $this->mobile_model->get_all_details(USERS,$condition);
			if($userInfoDetails->num_rows()==1){
				if($userInfoDetails->row()->status == "Active"){
					$userInfo = 'Success';
					if($userInfoDetails->row()->thumbnail!=""){
						$userImage=base_url().'images/users/'.$userInfoDetails->row()->thumbnail;
					}else{
						$userImage=base_url().'images/users/profile_pic.png';
					}
					/*push update*/
					$UDID='';
					if($_POST['uu_id']!=""){
						$device_type='android';
						$UDID = $_POST['uu_id'];
					}else if($_POST['deviceToken']!=""){
						$device_type='ios';
						$UDID = $_POST['deviceToken'];
						$deviceID = $_POST['deviceID'];
					}
					if($UDID!=""){
						$this->mobile_model->insertupdatePushKey($userInfoDetails->row()->id,$UDID,'user',$device_type);
					}
					$this->mobile_model->update_details(SHOPPING_CART,array("user_id"=>$userInfoDetails->row()->id),array("user_id"=>$this->data["commonId"]));
					$returnArr['status'] = '1';
					$message = "Success";
					if($this->lang->line('json_success') != ""){
						$message = stripslashes($this->lang->line('json_success'));
					}
					$returnArr['response'] = $message;
					if($userInfoDetails->row()->thumbnail!=""){
						$user_image=base_url().'images/users/'.$userInfoDetails->row()->thumbnail;
					}else{
						$user_image=base_url().'images/users/user-thumb1.png';
					}
					/* 	$condition = array('seller_id'=>$userInfoDetails->row()->id);
					$sellerInfoDetails = $this->mobile_model->get_all_details(SELLER,$condition);
					$seller_flag= 'No';
					if($sellerInfoDetails->num_rows() > 0){
					$seller_flag= 'Yes';
					} */
					$returnArr['user_image'] = $user_image;
					$returnArr['user_id'] = (string)$userInfoDetails->row()->id;
					$returnArr['user_name'] = $userInfoDetails->row()->user_name;
					$returnArr['email'] = $userInfoDetails->row()->email;
					$returnArr['user_fullname'] = $userInfoDetails->row()->full_name;
					$returnArr['type'] = "normal";
				}else{
					$message = "Your Account is deactivated !! Please contact Admin !!";
					if($this->lang->line('json_account_deact') != ""){
						$message = stripslashes($this->lang->line('json_account_deact'));
					}
					$returnArr['response'] = $message;
				}
			}else{
				$message = "Invalid User";
				if($this->lang->line('json_invalid_user') != ""){
					$message = stripslashes($this->lang->line('json_invalid_user'));
				}
				$returnArr['message'] = $message;
			}
		}else{
			$returnArr['message'] = 'Some Parameters are missing !!';
		}
		echo json_encode($returnArr);
	}

	public function facebookLogin(){
		//error_reporting(0);
		$returnArr['status'] = '0';
		$returnArr['response']='';
		$email = $_POST['email'];
		$username = $_POST['username'];
		$first_name = $_POST['firstName'];
		$first_name = utf8_decode($first_name);
		$profile_image = $_POST['profile_image'];
		$condition = '(email = \''.addslashes($email).'\' OR user_name = \''.addslashes($email).'\') AND status=\'Active\'';
		$userInfoDetails = $this->mobile_model->get_all_details(USERS,$condition);
		if($userInfoDetails->num_rows()==1){
				if($userInfoDetails->row()->thumbnail!=""){
					$userImage=base_url().'images/users/'.$userInfoDetails->row()->thumbnail;
				}else{
					$userImage=base_url().'images/users/user-thumb1.png';
				}
				$newdata = array(
							               'last_login_date' => date("Y-m-d h:i:s"),
				//			               'last_login_ip' => $ipAddress
							    	);
				$condition = array('id' => $userInfoDetails->row()->id);
				$this->mobile_model->update_details(USERS,$newdata,$condition);
					$UDID='';
					if($_POST['uu_id']!=""){
						$device_type='android';
						$UDID = $_POST['uu_id'];
					}else if($_POST['deviceToken']!=""){
						$device_type='ios';
						$UDID = $_POST['deviceToken'];
						$deviceID = $_POST['deviceID'];
					}
					if($UDID!=""){
						$this->mobile_model->insertupdatePushKey($userInfoDetails->row()->id,$UDID,'user',$device_type);
					}
				if($userInfoDetails->row()->thumbnail!=""){
					$user_image=base_url().'images/users/'.$userInfoDetails->row()->thumbnail;
				}else{
					$user_image=base_url().'images/users/user-thumb1.png';
				}
				$returnArr['status'] = '1';
				$message = "Success";
				if($this->lang->line('json_success') != ""){
					$message = stripslashes($this->lang->line('json_success'));
				}
				$returnArr['response'] = $message;
				$returnArr['registeredUser'] = "1";
				$returnArr['userImage'] = $user_image;
				$returnArr['userId'] = (string)$userInfoDetails->row()->id;
				$returnArr['userName'] = $userInfoDetails->row()->user_name;
				$returnArr['email'] = $userInfoDetails->row()->email;
				$returnArr['userFullname'] = utf8_encode($userInfoDetails->row()->full_name);
				$returnArr['type'] = "facebook";
		}else{
			if($email!=" "){
				$passwordstr=$this->get_rand_str(8);
				$this->db->select('id');
				$this->db->from(USERS);
				$this->db->where('email',$email);
				$userNameDetails = $this->db->get();
				if($userNameDetails->num_rows()>0){
					$message = "User Already Exist";
					if($this->lang->line('json_user_exists') != ""){
					    $message = stripslashes($this->lang->line('json_user_exists'));
					}
					$returnArr['response'] = $message;
					if($this->lang->line('json_user_exists') != ""){
					    $message = stripslashes($this->lang->line('json_user_exists'));
					}
					$returnArr['response'] = $message;
				}else{
					$chckUserName = strtok($email,'@');
					$this->db->select('id');
					$this->db->from(USERS);
					$this->db->where('user_name',$chckUserName);
					$userNamechecking = $this->db->get();
					$s = 0;
					while ($userNamechecking->num_rows() > 0) {
						$chckUserName = $chckUserName.$s;
						$this->db->select('id');
						$this->db->from(USERS);
						$this->db->where('user_name',$chckUserName);
						$userNamechecking = $this->db->get();
						$s++;
					}
					// if($userNamechecking->num_rows() > 0){
					// 		$count = $userNamechecking->num_rows();
					// 		$count = $count+1;
					// 		$chckUserName = $chckUserName.$count;
					// }
					if($profile_image!=""){
						$imgPath ='images/users/temp/';
						$img =$profile_image;
						$imgName = time(). ".jpg";
						$imageFormat = array('data:image/jpeg;base64', 'data:image/png;base64', 'data:image/jpg;base64', 'data:image/gif;base64');
						$img = str_replace($imageFormat, '', $img);
						$data = base64_decode($img);
						$image = @imagecreatefromstring($data);
						if ($image !== false) {
							$uploadPath = $imgPath . $imgName;
							imagejpeg($image, $uploadPath, 100);
							imagedestroy($image);
						} else {
							$message = "An error occurred";
							if($this->lang->line('json_error_occured') != ""){
							    $message = stripslashes($this->lang->line('json_error_occured'));
							}
							$returnArr['response'] = $message;
						}
						if (isset($uploadPath)) {
							$filename = base_url($uploadPath);
							list($width, $height) = getimagesize($filename);
							$newwidth = 100;
							$newheight = 100;
							$sourceImage = @imagecreatefromjpeg($filename);
							imagecopyresized($thumbImage, $sourceImage, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
						}else {
							$message = "Invalid File";
							if($this->lang->line('json_invalid_file') != ""){
							    $message = stripslashes($this->lang->line('json_invalid_file'));
							}
							$returnArr['response'] = $message;
						}
						$new_name = $imgName;
						@copy("./images/users/temp/".$imgName, './images/users/'.$new_name);
						@copy("./images/users/temp/".$imgName, './images/users/thumb/'.$new_name);
						$this->imageResizeWithSpace(200, 200, $new_name, './images/users/thumb/');
						$this->imageResizeWithSpace(600, 600, $new_name, './images/users/');


						$dir =getcwd().DIRECTORY_SEPARATOR ."images".DIRECTORY_SEPARATOR ."users".DIRECTORY_SEPARATOR."temp".DIRECTORY_SEPARATOR ;
						$interval = strtotime('-24 hours');
						foreach (glob($dir."*.*") as $file){
							if (filemtime($file) <= $interval ){
								if(!is_dir($file) ){
									unlink($file);
								}
							}
						}
						$thumbnail = $imgName;
						}else{
							$thumbnail='';
						}
						$dataArr = array(
													'loginUserType'=>'facebook',
													'full_name'=>$first_name,
													'user_name'=>$chckUserName,
													'thumbnail'=>$thumbnail,
													'email'=>$email,
													'password'=>md5($passwordstr),
													'status'=>'Active',
													'is_verified'=>'No',
													'commision'=>$this->config->item('product_commission')
										);
						$this->db->insert(USERS,$dataArr);
						$this->db->select('*');
						$this->db->from(USERS);
						$this->db->where('email',$email);
						$checkUser = $this->db->get();
						$UDID='';
						if($_POST['uu_id']!=""){
							$device_type='android';
							$UDID = $_POST['uu_id'];
						}else if($_POST['deviceToken']!=""){
							$device_type='ios';
							$UDID = $_POST['deviceToken'];
							$deviceID = $_POST['deviceID'];
						}
						if($UDID!=""){
							$this->mobile_model->insertupdatePushKey($checkUser->row()->id,$UDID,'user',$device_type);
						}
						//$this->mobile_model->update_details(SHOPPING_CART,array("user_id"=>$checkUser->row()->id),array("user_id"=>$this->data["commonId"]));
						$userDetails=$checkUser;
						$this->send_confirm_mail($userDetails);
						if($checkUser->row()->thumbnail!=""){
							$userImage=base_url().'images/users/'.$checkUser->row()->thumbnail;
						}else{
							$userImage=base_url().'images/users/user-thumb1.png';
						}
						$this->load->library('curl');
						$url = base_url().'wp_change_user_role.php';
						$post_data = array (
								"un" => $checkUser->row()->user_name,
								"pd" => $passwordstr,
								"em" => $email
						);
						$output = $this->curl->simple_get($url, $post_data);
						$userInfoDetails = $this->mobile_model->get_all_details(USERS,array('email'=>$email));
						if($userInfoDetails->row()->thumbnail!=""){
							$user_image=base_url().'images/users/'.$userInfoDetails->row()->thumbnail;
						}else{
							$user_image=base_url().'images/users/user-thumb1.png';
						}
						$returnArr['status'] = '1';
						$message = "Success";
						if($this->lang->line('json_success') != ""){
							$message = stripslashes($this->lang->line('json_success'));
						}
						$returnArr['response'] = $message;
						$returnArr['userImage'] = $user_image;
						$returnArr['userId'] = (string)$userInfoDetails->row()->id;
						$returnArr['userName'] = $userInfoDetails->row()->user_name;
						$returnArr['email'] = $userInfoDetails->row()->email;
						$returnArr['userFullname'] = utf8_encode($userInfoDetails->row()->full_name);
						$returnArr['type'] = "facebook";
					}
			}else{
				$returnArr['response'] = 'Some Parameters are Missing';
			}
		}
		$json_encode = json_encode($returnArr);
		echo $json_encode;
	}


	public function google_login(){

		$returnArr['status'] = '0';
		$returnArr['response']='';
		$email =$_POST['email'];
		$first_name =$_POST['firstname'];
		$profile_image =$_POST['profile_image'];
		$profileExt =$_POST['imageExt'];
		$condition = '(email = \''.addslashes($email).'\' OR user_name = \''.addslashes($email).'\') AND status=\'Active\'';
		$userInfoDetails = $this->mobile_model->get_all_details(USERS,$condition);
		if($userInfoDetails->num_rows()==1){
			//$this->mobile_model->update_details(SHOPPING_CART,array("user_id"=>$userInfoDetails->row()->id),array("user_id"=>$this->data["commonId"]));
				$newdata = array(
	               'last_login_date' => date("Y-m-d h:i:s"),
	            //'last_login_ip' => $this->input->ip_address()
	    	        );
				$condition = array('id' => $userInfoDetails->row()->id);
				$this->mobile_model->update_details(USERS,$newdata,$condition);
					$UDID='';
					if($_POST['uu_id']!=""){
						$device_type='android';
						$UDID = $_POST['uu_id'];
					}else if($_POST['deviceToken']!=""){
						$device_type='ios';
						$UDID = $_POST['deviceToken'];
						$deviceID = $_POST['deviceID'];
					}
					if($UDID!=""){
						$this->mobile_model->insertupdatePushKey($userInfoDetails->row()->id,$UDID,'user',$device_type);
					}
				if($userInfoDetails->row()->thumbnail!=""){
					$userImage=base_url().'images/users/'.$userInfoDetails->row()->thumbnail;
				}else{
					$userImage=base_url().'images/users/user-thumb1.png';
				}
				$returnArr['status'] = '1';
				$message = "Success";
				if($this->lang->line('json_success') != ""){
					$message = stripslashes($this->lang->line('json_success'));
				}
				$returnArr['response'] = $message;
				$returnArr['user_id'] = (string)$userInfoDetails->row()->id;
				$returnArr['user_fullname'] = $userInfoDetails->row()->full_name;
				$returnArr['user_name'] = $userInfoDetails->row()->user_name;
				$returnArr['user_image'] = $userImage;
				$returnArr['email'] = $userInfoDetails->row()->email;
				$returnArr['type'] = "google";
				/*End*/
		}else{

			if($email!=''){
				$passwordstr=$this->get_rand_str(8);
				$this->db->select('id');
				$this->db->from(USERS);
				$this->db->where('email',$email);
				$userNameDetails = $this->db->get();
				if($userNameDetails->num_rows()>0){
					$message = "User Already Exist";
					if($this->lang->line('json_user_exists') != ""){
					    $message = stripslashes($this->lang->line('json_user_exists'));
					}
					$returnArr['response'] = $message;
				}else{
					$chckUserName = strtok($email,'@');
					$this->db->select('id');
					$this->db->from(USERS);
					$this->db->where('user_name',$chckUserName);
					$userNamechecking = $this->db->get();
					$s = 0;
					while ($userNamechecking->num_rows() > 0) {
						$chckUserName = $chckUserName.$s;
						$this->db->select('id');
						$this->db->from(USERS);
						$this->db->where('user_name',$chckUserName);
						$userNamechecking = $this->db->get();
						$s++;
					}
					// if($userNamechecking->num_rows() > 0){
					// 		$count = $userNamechecking->num_rows();
					// 		$count = $count+1;
					// 		$chckUserName = $chckUserName.$count;
					// }
					if (!empty($_POST['profile_image'])) {
						$imgPath ='images/users/temp/';
						$img = $_POST['profile_image'];
						$imgName = time(). ".jpg";
						$imageFormat = array('data:image/jpeg;base64', 'data:image/png;base64', 'data:image/jpg;base64', 'data:image/gif;base64');
						$img = str_replace($imageFormat, '', $img);
						$data = base64_decode($img);
						$image = @imagecreatefromstring($data);
						if ($image !== false) {
							$uploadPath = $imgPath . $imgName;
							imagejpeg($image, $uploadPath, 100);
							imagedestroy($image);
						} else {
							$message = "An error occurred";
							if($this->lang->line('json_error_occured') != ""){
							    $message = stripslashes($this->lang->line('json_error_occured'));
							}
							$returnArr['response'] = $message;
						}
						if (isset($uploadPath)) {
							/* Creating Thumbnail image with the size of 100 X 100 */
							$filename = base_url($uploadPath);
							list($width, $height) = getimagesize($filename);
							$newwidth = 100;
							$newheight = 100;
							//$thumbImage = @imagecreatetruecolor($newwidth, $newheight);
							$sourceImage = @imagecreatefromjpeg($filename);
							imagecopyresized($thumbImage, $sourceImage, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
							// if ($thumbImage !== false) {
							// 	/* Not uploading to thumb folder */
							// 	$thumbImgPath = 'images/users/thumb/';
							// 	$thumpUploadPath = $thumbImgPath . $imgName;
							// 	imagejpeg($thumbImage, $thumpUploadPath, 100);
							// }
						}else {
							$message = "Invalid File";
							if($this->lang->line('json_invalid_file') != ""){
							    $message = stripslashes($this->lang->line('json_invalid_file'));
							}
							$returnArr['response'] = $message;
						}
						$new_name = $imgName;
						@copy("./images/users/temp/".$imgName, './images/users/'.$new_name);
						@copy("./images/users/temp/".$imgName, './images/users/thumb/'.$new_name);
						$this->imageResizeWithSpace(200, 200, $new_name, './images/users/thumb/');
						$this->imageResizeWithSpace(600, 600, $new_name, './images/users/');

						$thumbnail = $new_name;
						$dir = getcwd().DIRECTORY_SEPARATOR ."images".DIRECTORY_SEPARATOR ."users".DIRECTORY_SEPARATOR."temp".DIRECTORY_SEPARATOR ;
						$interval = strtotime('-24 hours');
						foreach (glob($dir."*.*") as $file){
							if (filemtime($file) <= $interval ){
								if(!is_dir($file)){
									unlink($file);
								}
							}
						}
					}else{
						$thumbnail='';
					}
					$dataArr = array(
					'loginUserType'=>'google',
					'full_name'=>$first_name,
					'user_name'=>$chckUserName,
					'thumbnail'=>$thumbnail,
					'email'=>$email,
					'password'=>md5($passwordstr),
					'status'=>'Active',
					'is_verified'=>'No',
					'commision'=>$this->config->item('product_commission'));
					$this->db->insert(USERS,$dataArr);
					$this->db->select('*');
					$this->db->from(USERS);
					$this->db->where('email',$email);
					$checkUser = $this->db->get();

					$UDID='';
					if($_POST['uu_id']!=""){
						$device_type='android';
						$UDID = $_POST['uu_id'];
					}else if($_POST['deviceToken']!=""){
						$device_type='ios';
						$UDID = $_POST['deviceToken'];
						$deviceID = $_POST['deviceID'];
					}
					if($UDID!=""){
						$this->mobile_model->insertupdatePushKey($checkUser->row()->id,$UDID,'user',$device_type);
					}
				//$this->mobile_model->update_details(SHOPPING_CART,array("user_id"=>$checkUser->row()->id),array("user_id"=>$this->data["commonId"]));
					$userDetails=$checkUser;
					$this->send_confirm_mail($userDetails);
					$this->load->library('curl');
					$url = base_url().'wp_change_user_role.php';
					$post_data = array (
							"un" => $checkUser->row()->user_name,
							"pd" => $passwordstr,
							"em" => $email
					);
					$output = $this->curl->simple_get($url, $post_data);
					$userInfoDetails = $this->mobile_model->get_all_details(USERS,array('email'=>$email));
					if($userInfoDetails->row()->thumbnail != ""){
						$user_image=base_url().'images/users/'.$userInfoDetails->row()->thumbnail;
					}else{
						$user_image=base_url().'images/users/user-thumb1.png';
					}
					$returnArr['status'] = '1';
					$message = "Success";
					if($this->lang->line('json_success') != ""){
						$message = stripslashes($this->lang->line('json_success'));
					}
					$returnArr['response'] = $message;
					$returnArr['user_id'] = (string)$userInfoDetails->row()->id;
					$returnArr['user_fullname'] = $userInfoDetails->row()->full_name;
					$returnArr['user_name'] = $userInfoDetails->row()->user_name;
					$returnArr['email'] = $userInfoDetails->row()->email;
					$returnArr['user_image'] = $user_image;
					$returnArr['type'] = "google";
				}
			}else{
				$returnArr['response'] = 'Invalid Input';
			}
		}
		$json_encode = json_encode($returnArr);
		echo $json_encode;
	}

	public function twitter_login(){

		$returnArr['status'] = '0';
		$returnArr['response']='';
		$email =$_POST['email'];
		$username = $_POST['username'];
		$first_name =$_POST['firstname'];
		$profile_image =$_POST['profile_image'];
		$profileExt =$_POST['imageExt'];
		$condition = '(email = \''.addslashes($email).'\' OR user_name = \''.addslashes($email).'\') AND status=\'Active\'';
		$userInfoDetails = $this->mobile_model->get_all_details(USERS,$condition);
		if($userInfoDetails->num_rows()==1){
			//$this->mobile_model->update_details(SHOPPING_CART,array("user_id"=>$userInfoDetails->row()->id),array("user_id"=>$this->data["commonId"]));
				$newdata = array(
	               'last_login_date' => date("Y-m-d h:i:s"),
	            //   'last_login_ip' => $this->input->ip_address()
	    	        );
				$condition = array('id' => $userInfoDetails->row()->id);
				$this->mobile_model->update_details(USERS,$newdata,$condition);
				/*push update*/
					$UDID='';
					if($_POST['uu_id']!=""){
						$device_type='android';
						$UDID = $_POST['uu_id'];
					}else if($_POST['deviceToken']!=""){
						$device_type='ios';
						$UDID = $_POST['deviceToken'];
						$deviceID = $_POST['deviceID'];
					}
					if($UDID!=""){
						$this->mobile_model->insertupdatePushKey($userInfoDetails->row()->id,$UDID,'user',$device_type);
					}
					if($userInfoDetails->row()->thumbnail!=""){
						$user_image=base_url().'images/users/'.$userInfoDetails->row()->thumbnail;
					}else{
						$user_image=base_url().'images/users/user-thumb1.png';
					}
				$returnArr['status'] = '1';
				$message = "Success";
				if($this->lang->line('json_success') != ""){
					$message = stripslashes($this->lang->line('json_success'));
				}
				$returnArr['response'] = $message;
				$returnArr['user_id'] = (string)$userInfoDetails->row()->id;
				$returnArr['user_fullname'] = $userInfoDetails->row()->full_name;
				$returnArr['user_name'] = $userInfoDetails->row()->user_name;
				$returnArr['email'] = $userInfoDetails->row()->email;
				$returnArr['user_image'] = $user_image;
				$returnArr['type'] = "twitter";
		}else{
			if($email!=''){

				$passwordstr=$this->get_rand_str(8);
				$this->db->select('id');
				$this->db->from(USERS);
				$this->db->where('email',$email);
				$userNameDetails = $this->db->get();

				if($userNameDetails->num_rows()>0){
					$message = "User Already Exist";
					if($this->lang->line('json_user_exists') != ""){
					    $message = stripslashes($this->lang->line('json_user_exists'));
					}
					$returnArr['response'] = $message;
				}else{
					if($username == ""){
						$chckUserName = strtok($email,'@');
						$this->db->select('id');
						$this->db->from(USERS);
						$this->db->where('user_name',$chckUserName);
						$userNamechecking = $this->db->get();
						$s = 0;
						while ($userNamechecking->num_rows() > 0) {
							$chckUserName = $chckUserName.$s;
							$this->db->select('id');
							$this->db->from(USERS);
							$this->db->where('user_name',$chckUserName);
							$userNamechecking = $this->db->get();
							$s++;
						}
					}else{
						$chckUserName = $username;
					}

					// if($userNamechecking->num_rows() > 0){
					// 		$count = $userNamechecking->num_rows();
					// 		$count = $count+1;
					// 		$chckUserName = $chckUserName.$count;
					// }
					if (!empty($_POST['profile_image'])) {
						$imgPath ='images/users/temp/';
						$img = $_POST['profile_image'];
						$imgName = time(). ".jpg";
						$imageFormat = array('data:image/jpeg;base64', 'data:image/png;base64', 'data:image/jpg;base64', 'data:image/gif;base64');
						$img = str_replace($imageFormat, '', $img);
						$data = base64_decode($img);
						$image = @imagecreatefromstring($data);
						if ($image !== false) {
							$uploadPath = $imgPath . $imgName;
							imagejpeg($image, $uploadPath, 100);
							imagedestroy($image);
						} else {
							$message = "An error occurred";
							if($this->lang->line('json_error_occured') != ""){
							    $message = stripslashes($this->lang->line('json_error_occured'));
							}
							$returnArr['response'] = $message;
						}
						if (isset($uploadPath)) {
							/* Creating Thumbnail image with the size of 100 X 100 */
							$filename = base_url($uploadPath);
							list($width, $height) = getimagesize($filename);
							$newwidth = 100;
							$newheight = 100;
							//$thumbImage = @imagecreatetruecolor($newwidth, $newheight);
							$sourceImage = @imagecreatefromjpeg($filename);
							imagecopyresized($thumbImage, $sourceImage, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
							// if ($thumbImage !== false) {
							// 	/* Not uploading to thumb folder */
							// 	$thumbImgPath = 'images/users/thumb/';
							// 	$thumpUploadPath = $thumbImgPath . $imgName;
							// 	imagejpeg($thumbImage, $thumpUploadPath, 100);
							// }
						}else {
							$message = "Invalid File";
							if($this->lang->line('json_invalid_file') != ""){
							    $message = stripslashes($this->lang->line('json_invalid_file'));
							}
							$returnArr['response'] = $message;
						}
						$new_name = $imgName;
						@copy("./images/users/temp/".$imgName, './images/users/'.$new_name);
						@copy("./images/users/temp/".$imgName, './images/users/thumb/'.$new_name);

						$this->imageResizeWithSpace(200, 200, $new_name, './images/users/thumb/');
						$this->imageResizeWithSpace(600, 600, $new_name, './images/users/');

						$thumbnail = $new_name;
						$dir = getcwd().DIRECTORY_SEPARATOR ."images".DIRECTORY_SEPARATOR ."users".DIRECTORY_SEPARATOR."temp".DIRECTORY_SEPARATOR ;
						$interval = strtotime('-24 hours');
						foreach (glob($dir."*.*") as $file){
							if (filemtime($file) <= $interval ){
								if(!is_dir($file) ){
									unlink($file);
								}
							}
						}
					}else{
						$thumbnail='';
					}
					$dataArr = array(
					'loginUserType'=>'twitter',
					'full_name'=>$first_name,
					'user_name'=>$chckUserName,
					'thumbnail'=>$thumbnail,
					'email'=>$email,
					'password'=>md5($passwordstr),
					'status'=>'Active',
					'is_verified'=>'No',
					'commision'=>$this->config->item('product_commission'));
					$this->db->insert(USERS,$dataArr);
					$this->db->select('*');
					$this->db->from(USERS);
					$this->db->where('email',$email);
					$checkUser = $this->db->get();
					/*push update*/
					$UDID='';
					if($_POST['uu_id']!=""){
						$device_type='android';
						$UDID = $_POST['uu_id'];
					}else if($_POST['deviceToken']!=""){
						$device_type='ios';
						$UDID = $_POST['deviceToken'];
						$deviceID = $_POST['deviceID'];
					}
					if($UDID!=""){
						$this->mobile_model->insertupdatePushKey($checkUser->row()->id,$UDID,'user',$device_type);
					}
				//$this->mobile_model->update_details(SHOPPING_CART,array("user_id"=>$checkUser->row()->id),array("user_id"=>$this->data["commonId"]));
					$userDetails=$checkUser;
					$this->send_confirm_mail($userDetails);
					$this->load->library('curl');
					$url = base_url().'wp_change_user_role.php';
					$post_data = array (
							"un" => $checkUser->row()->user_name,
							"pd" => $passwordstr,
							"em" => $email
					);
					$output = $this->curl->simple_get($url, $post_data);
					$userInfoDetails = $this->mobile_model->get_all_details(USERS,array('email'=>$email));
					if($userInfoDetails->row()->thumbnail!=""){
						$user_image=base_url().'images/users/'.$userInfoDetails->row()->thumbnail;
					}else{
						$user_image=base_url().'images/users/user-thumb1.png';
					}
					$returnArr['status'] = '1';
					$message = "Success";
					if($this->lang->line('json_success') != ""){
						$message = stripslashes($this->lang->line('json_success'));
					}
					$returnArr['response'] = $message;
					$returnArr['user_id'] = (string)$userInfoDetails->row()->id;
					$returnArr['user_fullname'] = $userInfoDetails->row()->full_name;
					$returnArr['user_name'] = $userInfoDetails->row()->user_name;
					$returnArr['email'] = $userInfoDetails->row()->email;
					$returnArr['user_image'] = $user_image;
					$returnArr['type'] = "twitter";
				}
			}else{
				$returnArr['response'] = 'Invalid Input';
			}
		}
		$json_encode = json_encode($returnArr);
		echo $json_encode;
	}



	public function user_register(){
		$returnArr['status'] = '0';
		$message = "Failure";
		if($this->lang->line('json_failure') != ""){
			$message = stripslashes($this->lang->line('json_failure'));
		}
		$returnArr['response'] = $message;
		$fullnamestr = $_POST['fullname'];
		//$lastnamestr = $_POST['lastname'];
		$emailstr = $_POST['email'];
		$passwordstr = $_POST['password'];
		$usernamestr1 = $_POST['username'];
		$usernamestr = stripslashes($usernamestr1.trim());

		$this->db->select('*');
		$this->db->from(USERS);
		$this->db->where('user_name',$usernamestr);
		$userNameDetails = $this->db->get();

		$this->db->select('*');
		$this->db->from(USERS);
		$this->db->where('email',$emailstr);
		$userEmailDetails = $this->db->get();

		if($fullnamestr=='' ||  $emailstr=='' || $passwordstr=='' || $usernamestr1=='') {
			$message = "Failure";
			if($this->lang->line('json_failure') != ""){
				$message = stripslashes($this->lang->line('json_failure'));
			}
			$returnArr['response'] = $message;
		}else if($userNameDetails->num_rows()==1){
			$message = "Username Already Exist";
			if($this->lang->line('json_username_exist') != ""){
				$message = stripslashes($this->lang->line('json_username_exist'));
			}
			$returnArr['response'] = $message;
		}else if($userEmailDetails->num_rows()==1){
			$message = "Email Address Already Exist";
			if($this->lang->line('json_email_exist') != ""){
			    $message = stripslashes($this->lang->line('json_email_exist'));
			}
			$returnArr['message'] = $message;
		}else{
			$dataArr = array('loginUserType'=>'mobile','full_name'=>$fullnamestr,'user_name'=>$usernamestr,'email'=>$emailstr,'password'=>md5($passwordstr),'status'=>'Active','is_verified'=>'No','commision'=>$this->config->item('product_commission'));

			$this->db->insert(USERS,$dataArr);
			$this->db->select('*');
			$this->db->from(USERS);
			$this->db->where('email',$emailstr);
			$checkUser = $this->db->get();


			/*push update*/
			$UDID='';
			if($_POST['uu_id']!=""){
				$device_type='android';
				$UDID = $_POST['uu_id'];
			}else if($_POST['deviceToken']!=""){
				$device_type='ios';
				$UDID = $_POST['deviceToken'];
				$deviceID = $_POST['deviceID'];
			}
			if($UDID!=""){
				$this->mobile_model->insertupdatePushKey($checkUser->row()->id,$UDID,'user',$device_type);
			}
			/*End*/

		//	$this->mobile_model->update_details(SHOPPING_CART,array("user_id"=>$checkUser->row()->id),array("user_id"=>$this->data["commonId"]));
			$userDetails=$checkUser;
			$this->send_confirm_mail($userDetails);

			if($checkUser->row()->thumbnail!=""){
				$user_image=base_url().'images/users/'.$checkUser->row()->thumbnail;
			}else{
				$user_image=base_url().'images/users/user-thumb1.png';
			}
			/*Blog registration Starts*/
			// $this->load->library('curl');
			// $url = base_url().'wp_change_user_role.php';
			// $post_data = array (
			// 		"un" => $checkUser->row()->user_name,
			// 		"pd" => $passwordstr,
			// 		"em" => $emailstr
			// );
			// $output = $this->curl->simple_get($url, $post_data);
			/*Blog registration Ends*/

			$returnArr['status'] = '1';
			$message = "Success";
			if($this->lang->line('json_success') != ""){
				$message = stripslashes($this->lang->line('json_success'));
			}
			$returnArr['response'] = $message;
			$returnArr['user_image'] = $user_image;
			$returnArr['user_id'] = (string)$checkUser->row()->id;
			$returnArr['user_name'] = $checkUser->row()->user_name;
			$returnArr['email'] = $checkUser->row()->email;
			$returnArr['user_fullname'] = $checkUser->row()->full_name;
		}
		$json_encode = json_encode($returnArr);
		echo $json_encode;
	}



	public function homepage(){
		$pg = intval($_GET['pg']);
		$UserId = "0";
		$userdetails = array();
		$returnArr['cartCount'] = '0';
		if(true){
			if(!empty($_GET['UserId'])){
				$UserId = $_GET['UserId'];
			}
			
			$UserProfileDetails = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
			$returnArr['UserGroup'] = isset($UserProfileDetails->row()->group)? $UserProfileDetails->row()->group :"" ;
			if($UserProfileDetails->row()->thumbnail!=""){
				$user_image=base_url().'images/users/'.$UserProfileDetails->row()->thumbnail;
			}else{
				$user_image=base_url().'images/users/user-thumb1.png';
			}
			$userdetails[] = array(
				"UserId" => $UserProfileDetails->row()->id,
				"UserName" => $UserProfileDetails->row()->user_name,
				"UserImage" => $user_image
			);
			$this->db->select('a.*,b.product_name,b.seourl,b.image,b.id as prdid,b.price as orgprice');
			$this->db->from(SHOPPING_CART . ' as a');
			$this->db->join(PRODUCT . ' as b', 'b.id = a.product_id');
			if(!empty($UserId)){
				$this->db->where('a.user_id = ' . $UserId);
			}
			$cartMiniVal = $this->db->get();
			if ($cartMiniVal->num_rows() > 0) {
				foreach ($cartMiniVal->result() as $CartRow) {
					$cartMiniQty = $cartMiniQty + $CartRow->quantity;
				}
			}
			$giftMiniRes = $this->minicart_model->get_all_details(GIFTCARDS_TEMP, array('user_id' => $UserId));
			$SubcribeMiniRes = $this->minicart_model->get_all_details(FANCYYBOX_TEMP, array('user_id' => $UserId));
			$countMiniVal = $giftMiniRes->num_rows() + $cartMiniQty + $SubcribeMiniRes->num_rows();
			$returnArr['cartCount'] = (string)$countMiniVal;
		}




		 if ($pg!='') {
			$paginationVal = $pg * 10;
			$limitPaging = $paginationVal . ',5';
		} else {
		$limitPaging = '5';
		}
		$newPage = $pg + 1;
		$qry_str = '?pg=' . $newPage;
		$nextpgArr[] = array('url'=>base_url().'json/homepage'.$qry_str);
		$layoutList = $this->product_model->view_controller_details();
        // $sellingProductDetails = $this->mobile_model->view_product_details(" where p.status='Publish' and ((p.quantity > 0 and p.product_type='physical')) order by p.created desc limit ". $limitPaging);
        // $affiliateProductDetails = $this->mobile_model->view_notsell_product_details(" where p.status='Publish' and u.status='Active' order by p.created desc limit ". $limitPaging);
		if ($layoutList->row()->product_control == 'affiliates') {
            $sellingProductDetails = array();
        } else {
            $sellingProductDetails = $this->mobile_model->view_product_details(" where p.status='Publish' and ((p.quantity > 0 and p.product_type='physical') or (p.product_type='digital')) and u.group='Seller' and u.status='Active' or p.status='Publish' and ((p.quantity > 0 and p.product_type='physical') or (p.product_type='digital')) and p.user_id=0 order by p.created desc limit " . $limitPaging);
        }
        if ($layoutList->row()->product_control == 'selling') {
            $affiliateProductDetails = array();
        } else {
            $affiliateProductDetails = $this->mobile_model->view_notsell_product_details(" where p.status='Publish' and u.status='Active' or p.status='Publish' and p.user_id=0 order by p.created desc limit " . $limitPaging);
        }
		$productDetails = $this->mobile_model->get_sorted_array_mobile($sellingProductDetails, $affiliateProductDetails, 'created', 'desc');
		foreach($productDetails as $prodRow){
			$likedStatus = "No";
			$url = "";
			$shareurl = "";
			if($UserId != ""){
				$cond = "select user_id from ".PRODUCT_LIKES." where product_id=".$prodRow['seller_product_id']." and user_id=".$UserId;
				$likedUsersList = $this->mobile_model->ExecuteQuery($cond);
				if($likedUsersList->num_rows() > 0){
					$likedStatus = "Yes";
				}
				$shareurl = $prodRow['shareUrl'].'?ref='.$UserProfileDetails->row()->user_name;
				$url =$prodRow['url'].'&UserId='.$UserId;
			}
			$imgArr=@explode(',',$prodRow['image']);
			/* $productImage = ($imgArr[0]=='') ? '' : base_url().'images/product/'.$imgArr[0]; */
			$images=array();
			foreach($imgArr as $img){
				if($img!="")$images[]=base_url().'images/product/'.$img;
			}
			$prodArr[]=array(
										'id'=>$prodRow['id'],
										'seller_product_id'=>$prodRow['seller_product_id'],
										'UserId' =>$prodRow['user_id'],
										'product_name'=>$prodRow['product_name'],
										'seourl'=>$prodRow['seourl'],
										'description'=>strip_tags($prodRow['description']),
										'shippingPolicies'=>strip_tags($prodRow['shipping_policies']),
										'image'=>$images,
										'price'=>$prodRow['price'],
										'sale_price'=>$prodRow['sale_price'],
										'user_name'=>$prodRow['user_name'],
										'likes'=>$prodRow['likes'],
										'liked' => $likedStatus,
										'url'=>$url,
										'shareUrl' => $shareurl,
										'type' => $prodRow['type'],
										'userUrl' => 	$normalUserUrl = 'http://192.168.1.251:8081/product-working/fancyclone-v3/json/user/seller-timeline?username='.$prodRow['user_name']
										);
		}
		$returnArr['productDetails']=$prodArr;
		$returnArr['userdetails']=$userdetails;
		$returnArr['nextUrl']=$nextpgArr;
		echo json_encode($returnArr);
	}


	public function product() {
		$returnArr['status'] = '0';
		$returnArr['response'] = '';
		$thisUserId = $_GET['UserId'];
		if($thisUserId == ""){
			$thisUserId ="0";
		}
		if($thisUserId == ""){
			$returnArr['response'] = 'UserId Null !!';
		}else{
			$thisUserProfileDetails = $this->mobile_model->get_all_details(USERS, array('id' => $thisUserId));
			$shippingCheck = $this->mobile_model->get_all_details(SHIPPING_ADDRESS, array('user_id' => $thisUserId));
			if($shippingCheck->num_rows() > 0){
				$shippingAddressAdded = "Yes";
			}else{
				$shippingAddressAdded = "No";
			}

			$followingListArr = explode(',', $thisUserProfileDetails->row()->following);
			$ownProductsArr = explode(',', $thisUserProfileDetails->row()->own_products);

		 	$pid=intval($_GET['pid']);
			if($pid == " "){
				$returnArr['response'] = 'ProductId Null !!';
			}else{
				$condition = "  where p.status='Publish' and u.group='Seller' and u.status='Active' and p.id='" . $pid . "' or p.status='Publish' and p.user_id=0 and p.id='" . $pid . "'";
				$this->data['productDetails'] = $this->mobile_model->view_product_details($condition);
				if ($this->data['productDetails']->num_rows()>0){
					$thisProdUserId = $this->data['productDetails']->row()->user_id;
					$ThisUseraddedProd = $this->mobile_model->GetThisUserAddedProduct($thisProdUserId);
					$PrdAttrVal = $this->mobile_model->view_subproduct_details_join($pid);
					$attributesCountValue = $PrdAttrVal->num_rows();
					$productAttributes = array();
					$productAttributesList = array();
					foreach ($PrdAttrVal->result() as $attributes) {
						$productAttributes[][$attributes->attr_type]= array(
																'id' => $attributes->pid,
																'value' => $attributes->attr_name,
																'price' => $attributes->attr_price
											);
						if(!in_array($attributes->attr_type,$productAttributesList)){
							$productAttributesList[]=$attributes->attr_type;
						}
					}
					$productAttributesListarray=array();
					foreach($productAttributesList as $row){
					   $productAttributesListarray[]=array('values'=>$row);
					}
					$userType = "NormalUser";
					$storeBanner = base_url().'images/store/banner-dummy.jpg';
					$storeLogo = base_url().'images/store/dummy-logo.png';
					$storeTagline = "";
					$storeDescription = "";
					$storeName ="";
					if($this->data['productDetails']->row()->store_payment == "Paid"){
						$store_details = $this->mobile_model->get_store_details($thisProdUserId);
					//	print_r($store_details->result());die;
						$storeName = $store_details->row()->store_name;
						$userType = "UpgradedUser";
						if($store_details->row()->cover_image !=""){
							$storeBanner = base_url().'images/store/'.$store_details->row()->cover_image;
						}
						if($store_details->row()->logo_image !=""){
							$storeLogo =  base_url().'images/store/'.$store_details->row()->logo_image;
						}
						$storeTagline = $store_details->row()->tagline;
						$storeDescription = $store_details->row()->description;
					}
					$imgArr=@explode(',',$this->data['productDetails']->row()->image);
					$images=array();
					foreach($imgArr as $img){
					if($img!="") $images[]=base_url().'images/product/'.$img;
					}
					$userImage = base_url().'images/users/user-thumb1.png';
					if($this->data['productDetails']->row()->thumbnail != ""){
						$userImage = base_url().'images/users/'.$this->data['productDetails']->row()->thumbnail;
					}
					$normalUserUrl ='';
						if($userType == 'NormalUser' ){
							$normalUserUrl = 'http://192.168.1.251:8081/product-working/fancyclone-v3/json/user/seller-timeline?username='.$this->data['productDetails']->row()->user_name;
						}

						$following = 'No';
						if(in_array($this->data['productDetails']->row()->user_id,$followingListArr)){$following = 'Yes';}
						$owned = 0;
						if(in_array($this->data['productDetails']->row()->seller_product_id,$ownProductsArr)){$owned=1;}

						$must = 'No';
						$cond = "select user_id from ".PRODUCT_LIKES." where product_id=".$this->data['productDetails']->row()->seller_product_id." and user_id=".$thisUserId;
						$likedUsersList = $this->mobile_model->ExecuteQuery($cond);
						$likedStatus = "No";
						if($likedUsersList->num_rows() > 0){
							$likedStatus = "Yes";
						}
						if($this->data['productDetails']->row()->attribute_must != "") $must = 'Yes';
						$productDetailsArr[] = array('id'=>$this->data['productDetails']->row()->id,
																	'seller_product_id'=>$this->data['productDetails']->row()->seller_product_id,
																	'UserId' => $this->data['productDetails']->row()->user_id,
																	'image'=>$images,
																	'product_name'=>$this->data['productDetails']->row()->product_name,
																	'description'=>strip_tags($this->data['productDetails']->row()->description),
																	'user_name'=>isset($this->data['productDetails']->row()->user_name)?$this->data['productDetails']->row()->user_name:"",
																	'price'=>$this->data['productDetails']->row()->price,
																	'sale_price'=>$this->data['productDetails']->row()->sale_price,
																	'shippingCost' =>  $this->data['productDetails']->row()->shipping_cost,
																	'taxCost' =>  $this->data['productDetails']->row()->tax_cost,
																	'likes'=>$this->data['productDetails']->row()->likes,
																	'liked' => $likedStatus,
																	'category_id'=>$this->data['productDetails']->row()->category_id,
																	'attributesCountValue'=> $attributesCountValue,
																	'AttributeMust' => $must,
																//	'productAttributes' => $productAttributes,
																	'productQuantity' => $this->data['productDetails']->row()->quantity,
																	'productType' => $this->data['productDetails']->row()->product_type,
																	'UserImage'=>$userImage,
																	'userType' => $userType,
																	'normalUserUrl' => $normalUserUrl,
																	'storeBanner' => $storeBanner,
																	'storeLogo' => $storeLogo,
																	'storeTagline' => $storeTagline,
																	'storeDescription' => strip_tags($storeDescription),
																	'storeName' => $storeName,
																	'storeFullName' => $thisUserProfileDetails->row()->full_name,
																	'storeFollowing' => $following,
																	'shareUrl' => base_url().'things/'.$this->data['productDetails']->row()->id.'/'.$this->data['productDetails']->row()->product_name.'?ref='.$thisUserProfileDetails->row()->user_name,
																	"shippingAddressAdded" => $shippingAddressAdded,
																	"owned" => $owned
																);
			        if ($this->data['productDetails']->num_rows() == 1) {
						if($thisUserId == $this->data['productDetails']->row()->user_id){
							$this->data['productComment'] = $this->product_model->view_product_comments_details('where c.product_id=' . $this->data['productDetails']->row()->seller_product_id . ' order by c.dateAdded desc');
							$this->data['productComment'] = $this->data['productComment']->result();
						}else{
							$productComment1 = $this->product_model->view_product_comments_details('where c.status="Active" and c.product_id=' . $this->data['productDetails']->row()->seller_product_id . ' order by c.dateAdded desc');
							$productComment2 = $this->product_model->view_product_comments_details('where c.user_id='.$thisUserId.' and c.product_id=' . $this->data['productDetails']->row()->seller_product_id . ' order by c.dateAdded desc');
							$this->data['productComment'] = array_merge($productComment1->result(),$productComment2->result());
						}
			            $catArr = explode(',', $this->data['productDetails']->row()->category_id);
			            if (count($catArr) > 0) {
			                foreach ($catArr as $cat) {
			                    if ($cat != '') {
			                        $condition = ' where FIND_IN_SET("' . $cat . '",p.category_id) and p.quantity>0 and p.status="Publish" and u.group="Seller" and u.status="Active" and p.id != "' . $pid . '" or p.status="Publish" and p.quantity > 0 and p.user_id=0 and FIND_IN_SET("' . $cat . '",p.category_id) and p.id != "' . $pid . '" limit 10';
			                        $relatedProductDetails = $this->product_model->view_product_details($condition);
			                        if ($relatedProductDetails->num_rows() > 0) {
			                            foreach ($relatedProductDetails->result() as $relatedProduct) {
			                                if (!in_array($relatedProduct->id, $relatedArr)) {
			                                    array_push($relatedArr, $relatedProduct->id);
			                                    $relatedProdArr[] = $relatedProduct;
			                                }
			                            }
			                        }
			                    }
			                }
			            }
			        }
					 $this->data['relatedProductsArr'] = $relatedProdArr;
					 $relProductDetailsArr=array();
					 foreach($this->data['relatedProductsArr'] as $relatedProductRow){
							$imgArr=@explode(',',$relatedProductRow->image);
							$images=array();
							foreach($imgArr as $img){
							if($img!="")$images[]=base_url().'images/product/'.$img;
							}
							$relProductDetailsArr[] = array('id'=>$relatedProductRow->id,
												'seller_product_id'=>$relatedProductRow->seller_product_id,
												'image'=>$images,
												'description'=>strip_tags($relatedProductRow->description),
												'shippingPolicies'=>strip_tags($relatedProductRow->shipping_policies),
												'product_name'=>$relatedProductRow->product_name,
												'user_name'=>isset($relatedProductRow->user_name)?$relatedProductRow->user_name:"",
												'price'=>$relatedProductRow->price,
												'sale_price'=>$relatedProductRow->sale_price,
												'likes'=>$relatedProductRow->likes,
												'category_id'=>$relatedProductRow->category_id,
												'url'=>base_url().'json/product?pid='.$relatedProductRow->id.'&UserId='.$thisUserId
												);
						}
						$UserAddedProductsArr = array();

						if($ThisUseraddedProd->num_rows > 0){
							foreach($ThisUseraddedProd->result() as $UserAddedProd){
								   $imgArr=@explode(',',$UserAddedProd->image);
								   $images=array();
								   foreach($imgArr as $img){
								   if($img!="")
								   $images[]=base_url().'images/product/'.$img;
								   }
								   $UserAddedProductsArr[] = array('id'=>$UserAddedProd->id,
													   'seller_product_id'=>$UserAddedProd->seller_product_id,
													   'image'=>$images,
													   'product_name'=>$UserAddedProd->product_name,
													   'description'=>strip_tags($UserAddedProd->description),
													   'shippingPolicies'=>strip_tags($UserAddedProd->shipping_policies),
													   'user_name'=>isset($UserAddedProd->user_name)?$UserAddedProd->user_name:"",
													   'price'=>$UserAddedProd->price,
													   'sale_price'=>$UserAddedProd->sale_price,
													   'likes'=>$UserAddedProd->likes,
													   'category_id'=>$UserAddedProd->category_id,
													   'url'=>base_url().'json/product?pid='.$UserAddedProd->id.'&UserId='.$thisUserId
													   );
							}
						}
					$productComment = $this->data['productComment'];
					$ProductsComments = array();
					$ProductsCommentsCount = count($productComment);
					foreach($productComment as $comments){
					 $defaultuserImg = 'user-thumb1.png';
					 $userImage =base_url().'images/users/'.$defaultuserImg;
					 if(!empty($comments->thumbnail)){
						 $userImage =base_url().'images/users/'.$comments->thumbnail;
					 }
						$ProductsComments[]=array(
			 		   											'CommenterFullName'	  => $comments->full_name,
			 													'CommenterUserName'	=> $comments->user_name,
			 													'CommenterImage' => $userImage,
			 													'comments'	=> $comments->comments,
			 													'CommenterEmail' =>	$comments->email,
			 													'commentId'	=> $comments->id,
			 													'status' =>	$comments->status,
			 													'CommenterId'=>	$comments->CUID,
			 													'date' => $comments->dateAdded
			 												);
				   }
			        $this->data['seller_product_details'] = $this->product_model->get_all_details(PRODUCT, array('user_id' => $this->data['productDetails']->row()->user_id, 'id !=' => $pid, 'status' => 'Publish'));
			        $this->data['seller_affiliate_products'] = $this->product_model->get_all_details(USER_PRODUCTS, array('user_id' => $this->data['productDetails']->row()->user_id));
					$this->data['product_feedback']=$product_feedback = $this->mobile_model->product_feedback_view($this->data['productDetails']->row()->user_id);
					$json_encode = array("productDetails" => $productDetailsArr,"ProductsComments"=>array_reverse($ProductsComments),	'commentsCount' => (string)$ProductsCommentsCount,"ThisUseraddedProd"=>$UserAddedProductsArr,'productAttributesList'=>$productAttributesListarray,"ProductAttribute"=>$productAttributes,"relatedProduct" => $relProductDetailsArr);
					$returnArr['status'] = '1';
					$message = "Success";
					if($this->lang->line('json_success') != ""){
						$message = stripslashes($this->lang->line('json_success'));
					}
					$returnArr['response'] = $message;
					$returnArr['productPage']=$json_encode;
				}else{
					$returnArr['response'] = 'Product details not available !!';
				}
			}
		}
		echo json_encode($returnArr);
	}

	/*
	 	!! >>>>>>>>>>>> Product Comments <<<<<<<<<<<< !!
	*/
	public function insert_product_comment() {
		$returnArr['status'] = '0';
		$returnArr['response'] = '';
		$uid = $this->input->post('user_id');
		if($uid == ""){
			$returnArr['response'] = 'UserId Null !!';
		}else{
			$comments = $this->input->post('comments');
			$product_id = $this->input->post('sellerProductId');
			if($comments == "" || $product_id == ""){
				$returnArr['response'] = 'Some Parameters Are Missing !!';
			}else{
				$datestring = "%Y-%m-%d %H:%i:%s";
				$time = time();
				$conditionArr = array(
													'comments' => $comments,
													'user_id' => $uid,
													'product_id' => $product_id,
													'status' => 'InActive',
													'dateAdded' => mdate($datestring, $time)
												);
				$this->order_model->simple_insert(PRODUCT_COMMENTS, $conditionArr);
				$cmtID = $this->order_model->get_last_insert_id();
				$cond = "select * from ".PRODUCT_COMMENTS." where id=".$cmtID;
				$commentDetail = $this->product_model->ExecuteQuery($cond);
				$cond2 = "select * from ".USERS." where id=".$commentDetail->row()->user_id;
				$userDetail = $this->product_model->ExecuteQuery($cond2);
				$defaultuserImg = 'user-thumb1.png';
				$userImage =base_url().'images/users/'.$defaultuserImg;
				if($userDetail->row()->thumbnail != "" ){
					$userImage =base_url().'images/users/'.$userDetail->row()->thumbnail;
				}
				$ProductsComments[]=array(
														'CommenterFullName'	  => $userDetail->row()->full_name,
														'CommenterUserName'	=> $userDetail->row()->user_name,
														'CommenterImage' => $userImage,
														'comments'	=> $commentDetail->row()->comments,
														'CommenterEmail' =>	$userDetail->row()->email,
														'commentId'	=> $commentDetail->row()->id,
														'status' =>	$commentDetail->row()->status,
														'CommenterId'=>	$commentDetail->row()->user_id
													//	'date' => $commentDetail->row()->dateAdded
													);
				$datestring = "%Y-%m-%d %H:%i:%s";
				$time = time();
				$createdTime = mdate($datestring, $time);
				$actArr = array(
											'activity' => 'own-product-comment',
											'activity_id' => $product_id,
											'user_id' => $uid,
										//	'activity_ip' => $this->input->ip_address(),
											'created' => $createdTime,
											'comment_id' => $cmtID
										);
				$this->order_model->simple_insert(NOTIFICATIONS, $actArr);
				$this->send_comment_noty_mail($cmtID, $product_id);
				$this->send_comment_noty_mail_to_admin($cmtID, $product_id);
				$returnArr['status'] = (string)1;
				$message = "Your Comment is Waiting For Approvel";
				if($this->lang->line('json_comment_approvel') != ""){
				    $message = stripslashes($this->lang->line('json_comment_approvel'));
				}
				$returnArr['response'] = $message;
				$returnArr['ProductsComments'] = $ProductsComments;


				$this->data['userDetails'] = $this->order_model->get_all_details(USERS, array('id' => $uid));
		          $productDetails = $this->order_model->get_all_details(PRODUCT, array('seller_product_id' => $product_id));
		          $productOfUser = $this->order_model->get_all_details(USERS, array('id' => $productDetails->row()->user_id));
		          $deive_type = '';
		          if($productOfUser->row()->gcm_buyer_id!=''){
			          $device_id = $productOfUser->row()->gcm_buyer_id;
			          $deive_type = 'ANDROID';
			          if($deive_type!=''){
			               $action = 'comment';
			               if($this->data['userDetails']->row()->full_name != ""){
			                    $name = $this->data['userDetails']->row()->full_name;
			               }else{
			                    $name = $this->data['userDetails']->row()->user_name;
			               }
			               $imgArr=@explode(',',$productDetails->row()->image);
			               $productImage = ($imgArr[0]=='') ? '' : base_url().'images/product/'.$imgArr[0];
			               $cond = "select user_id from ".PRODUCT_LIKES." where product_id=".$productDetails->row()->seller_product_id." and user_id=".$productDetails->row()->user_id;
			               $likedUsersList = $this->order_model->ExecuteQuery($cond);
			               $likedStatus = "No";
			               if($likedUsersList->num_rows() > 0){
			                    $likedStatus = "Yes";
			               }
			               $productUrl = base_url().'json/product?pid='.$productDetails->row()->id;
			               $urls = $productUrl.'&UserId='.$productDetails->row()->user_id;
			               $shipngdtls = $productDetails->row()->shipping_policies;
			               if($productDetails->row()->shipping_policies == ""){
			                    $shipngdtls = ".";
			               }
			               $urlval = array($productDetails->row()->id,
			                                       $productDetails->row()->seller_product_id,
			                                       $productDetails->row()->user_id,
			                                       $productDetails->row()->product_name,
			                                       $urls,
			                                       $productDetails->row()->seourl,
			                                     //  strip_tags($desdtls),
			                                       strip_tags($shipngdtls),
			                                       $productImage,
			                                       $productDetails->row()->price,
			                                       $productDetails->row()->sale_price,
			                                       $productOfUser->row()->user_name,
			                                       $this->data['userDetails']->row()->user_name ,
			                                       $productDetails->row()->likes,
			                                       $likedStatus
			                                   );
			               $message = ucwords($name).' '.'commented on your product'. ' "'.$productDetails->row()->product_name.'"';
			               $regIds = array($device_id);
			               $this->sendPushNotification($regIds, $message, $action, $deive_type,$urlval);
			          }
		     	}
			}
		}
		echo json_encode($returnArr);
	}

	/*
	 	!! >>>>>>>>>>>> Delete Product Comments <<<<<<<<<<<< !!
	*/
	public function deleteProductComment() {
		$returnArr['status'] = '0';
		$returnArr['response'] = '';
		$UserId = $_POST['UserId'];
        if ($UserId != '') {
            $cid = $_POST['commentId'];
			if($cid != ""){
				$this->product_model->commonDelete(PRODUCT_COMMENTS, array('id' => $cid));
	            $returnArr['status'] = (string)1;
				$message = "Comment Successfully Deleted";
				if($this->lang->line('json_comment_delete') != ""){
				    $message = stripslashes($this->lang->line('json_comment_delete'));
				}
				$returnArr['response'] = $message;
			}else{
				$returnArr['response'] = 'CommentId Null !!';
			}
        }else{
			$returnArr['response'] = 'UserId Null !!';
		}
        echo json_encode($returnArr);
	}

	/*
	 	!! >>>>>>>>>>>> Approve Product Comments <<<<<<<<<<<< !!
	*/
	public function ApproveProductComment() {
		$returnArr['status'] = '0';
		$returnArr['response'] = '';
		$cid = $_POST['commentId'];
		$product_id = $_POST['sellerProductId'];
		$user_id = $_POST['UserId'];
		if ($user_id != '') {
			if($cid != "" && $product_id != ""){
				$this->product_model->update_details(PRODUCT_COMMENTS, array('status' => 'Active'), array('id' => $cid));
				$datestring = "%Y-%m-%d %h:%i:%s";
				$time = time();
				$createdTime = mdate($datestring, $time);
				$this->product_model->commonDelete(NOTIFICATIONS, array('comment_id' => $cid));
				$actArr = array(
					'activity' => 'comment',
					'activity_id' => $product_id,
					'user_id' => $user_id,
					'activity_ip' => $this->input->ip_address(),
					'comment_id' => $cid,
					'created' => $createdTime
				);
				$this->product_model->simple_insert(NOTIFICATIONS, $actArr);
				$this->send_comment_noty_mail($product_id, $cid);
				$returnArr['status'] = '1';
				$message = "Comment Successfully Approved";
				if($this->lang->line('json_comment_approved') != ""){
				    $message = stripslashes($this->lang->line('json_comment_approved'));
				}
				$returnArr['response'] = $message;
			}else{
				$returnArr['response'] = 'Some Parameters are Missing !!';
			}
		}else{
			$returnArr['response'] = 'UserId Null !!';
		}
		echo json_encode($returnArr);
	}

	public function send_comment_noty_mail($cmtID = '0', $pid = '0') {
		if ($cmtID != '0' && $pid != '0') {
			$productUserDetails = $this->product_model->get_product_full_details($pid);
			if ($productUserDetails->num_rows() == 1) {
				$emailNoty = explode(',', $productUserDetails->row()->email_notifications);
				if (in_array('comments', $emailNoty)) {
					$commentDetails = $this->product_model->view_product_comments_details('where c.id=' . $cmtID);
					if ($commentDetails->num_rows() == 1) {
						if ($productUserDetails->prodmode == 'seller') {
							$prodLink = base_url() . 'things/' . $productUserDetails->row()->id . '/' . url_title($productUserDetails->row()->product_name, '-');
						} else {
							$prodLink = base_url() . 'user/' . $productUserDetails->row()->user_name . '/things/' . $productUserDetails->row()->seller_product_id . '/' . url_title($productUserDetails->row()->product_name, '-');
						}
						$newsid = '8';
						$template_values = $this->order_model->get_newsletter_template_details($newsid);
						$adminnewstemplateArr = array('email_title' => $this->config->item('email_title'), 'logo' => $this->data['logo'], 'full_name' => $commentDetails->row()->full_name, 'product_name' => $productUserDetails->row()->product_name, 'user_name' => $commentDetails->row()->user_name);
						extract($adminnewstemplateArr);
						$subject = $template_values['news_subject'];
						$message .= '<!DOCTYPE HTML>
							<html>
							<head>
							<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
							<meta name="viewport" content="width=device-width"/>
							<title>' . $template_values['news_subject'] . '</title>
							<body>';
						include('./newsletter/registeration' . $newsid . '.php');
						$message .= '</body>
							</html>';
						if ($template_values['sender_name'] == '' && $template_values['sender_email'] == '') {
							$sender_email = $this->data['siteContactMail'];
							$sender_name = $this->data['siteTitle'];
						} else {
							$sender_name = $template_values['sender_name'];
							$sender_email = $template_values['sender_email'];
						}
						$email_values = array('mail_type' => 'html',
							'from_mail_id' => $sender_email,
							'mail_name' => $sender_name,
							'to_mail_id' => $productUserDetails->row()->email,
							'subject_message' => $subject,
							'body_messages' => $message
						);
						$email_send_to_common = $this->product_model->common_email_send($email_values);
					}
				}
			}
		}
	}


	public function send_comment_noty_mail_to_admin($cmtID = '0', $pid = '0') {
		if ($cmtID != '0' && $pid != '0') {
			$productUserDetails = $this->product_model->get_product_full_details($pid);
			if ($productUserDetails->num_rows() == 1) {
				$commentDetails = $this->product_model->view_product_comments_details('where c.id=' . $cmtID);
				if ($commentDetails->num_rows() == 1) {
					if ($productUserDetails->prodmode == 'seller') {
						$prodLink = base_url() . 'things/' . $productUserDetails->row()->id . '/' . url_title($productUserDetails->row()->product_name, '-');
					} else {
						$prodLink = base_url() . 'user/' . $productUserDetails->row()->user_name . '/things/' . $productUserDetails->row()->seller_product_id .'/'.url_title($productUserDetails->row()->product_name, '-');
					}
					$newsid = '20';
					$template_values = $this->order_model->get_newsletter_template_details($newsid);
					$adminnewstemplateArr = array(
																'email_title' => $this->config->item('email_title'),
																'logo' => $this->data['logo'],
																'full_name' => $commentDetails->row()->full_name,
																'product_name' => $productUserDetails->row()->product_name,
																'user_name' => $commentDetails->row()->user_name
															);
					extract($adminnewstemplateArr);
					$subject = $template_values['news_subject'];
					$message .= '<!DOCTYPE HTML>
							<html>
							<head>
							<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
							<meta name="viewport" content="width=device-width"/>
							<title>' . $template_values['news_subject'] . '</title>
							<body>';
					include('./newsletter/registeration' . $newsid . '.php');
					$message .= '</body>
							</html>';
					if ($template_values['sender_name'] == '' && $template_values['sender_email'] == '') {
						$sender_email = $this->data['siteContactMail'];
						$sender_name = $this->data['siteTitle'];
					} else {
						$sender_name = $template_values['sender_name'];
						$sender_email = $template_values['sender_email'];
					}
					$email_values = array('mail_type' => 'html',
						'from_mail_id' => $sender_email,
						'mail_name' => $sender_name,
						'to_mail_id' => $this->data['siteContactMail'],
						'subject_message' => $subject,
						'body_messages' => $message
					);
					$email_send_to_common = $this->product_model->common_email_send($email_values);
				}
			}
		}
	}

	/*
	 	!! >>>>>>>>>>>> User Product Details <<<<<<<<<<<< !!
	*/
	public function userproduct(){
		$returnArr['status'] = '0';
		$returnArr['response'] = '';
		$uname = $_GET['uname'];
        $pid = $_GET['pid'];
		$thisUserId = $_GET['UserId'];
		if($thisUserId == ""){
			$thisUserId ="0";
		}
		if($pid == "" ||$thisUserId == ""){
			$returnArr['response'] = 'Some Parameters are Missing !!';
		}else{
			$thisUserProfileDetails = $this->mobile_model->get_all_details(USERS, array('id' => $thisUserId));
			$this->data['productUserDetails'] = $this->mobile_model->get_all_details(USERS, array('user_name' => $uname));
			$productDetails = $this->mobile_model->view_notsell_product_details(' where p.seller_product_id="' . $pid . '" and p.status="Publish"');
			if ($productDetails->num_rows() == 1) {
				$imgArr=@explode(',',$productDetails->row()->image);
				$images=array();
				foreach($imgArr as $img){
					if($img!="")$images[]=base_url().'images/product/'.$img;
				}
				$cond = "select user_id from ".PRODUCT_LIKES." where product_id=".$productDetails->row()->seller_product_id." and user_id=".$thisUserId;
				$likedUsersList = $this->mobile_model->ExecuteQuery($cond);
				$likedStatus = "No";
				if($likedUsersList->num_rows() > 0){
					$likedStatus = "Yes";
				}
				$ownProductsArr = array();
				if($thisUserId != ""){
					$thisUserProfileDetails = $this->mobile_model->get_all_details(USERS, array('id' => $thisUserId));
					$ownProductsArr = explode(',', $thisUserProfileDetails->row()->own_products);
				}
				$owned =0 ;
				if(in_array($productDetails->row()->seller_product_id,$ownProductsArr)){$owned=1;}
				$productDetailsArr[] = array(
																'id'=>$productDetails->row()->id,
																'UserId' => $productDetails->row()->user_id,
																'seller_product_id'=>$productDetails->row()->seller_product_id,
																'image'=>$images,
																'product_name'=>$productDetails->row()->product_name,
																'user_name'=>$productDetails->row()->user_name,
																'price'=>$productDetails->row()->price,
																'sale_price'=>$productDetails->row()->sale_price,
																'likes'=>$productDetails->row()->likes,
																'liked' => $likedStatus,
																'owned' => $owned,
																'category_id'=>$productDetails->row()->category_id,
																'shareUrl' => base_url().'user/'.$productDetails->row()->user_name.'/things/'.$productDetails->row()->seller_product_id.'/'.$productDetails->row()->product_name.'?ref='.$thisUserProfileDetails->row()->user_name,
													);
	            $categoryArr = explode(',', $productDetails->row()->category_id);
	            $catID = 0;
	            if (count($categoryArr) > 0) {
	                foreach ($categoryArr as $catRow) {
	                    if ($catRow != '') {
	                        $catID = $catRow;
	                        break;
	                    }
	                }
	            }
	            $this->data['relatedProductsArr'] = $this->mobile_model->get_products_by_category($catID);
				$relProductDetailsArr=array();
				$extrProductDetails = array();
				foreach($this->data['relatedProductsArr']->result() as $relatedProductRow){
						$imgArr=@explode(',',$relatedProductRow->image);
						$images=array();
						foreach($imgArr as $img){
							if($img!="")
							$images[]=base_url().'images/product/'.$img;
						}
						$relProductDetailsArr[] = array('id'=>$relatedProductRow->id,
																			'seller_product_id'=>$relatedProductRow->seller_product_id,
																			'image'=>$images,
																			'product_name'=>$relatedProductRow->product_name,
																			'user_name'=>$relatedProductRow->user_name,
																			'description'=>strip_tags($relatedProductRow->description),
																			'shippingPolicies'=>strip_tags($relatedProductRow->shipping_policies),
																			'price'=>$relatedProductRow->price,
																			'sale_price'=>$relatedProductRow->sale_price,
																			'likes'=>$relatedProductRow->likes,
																			'category_id'=>$relatedProductRow->category_id,
																			'url'=>base_url().'json/product?pid='.$relatedProductRow->id
																);
				}
				$affiliateProductDetails = $this->mobile_model->view_notsell_product_details("where p.seller_product_id !='".$pid."' and p.status='Publish' and u.status='Active' order by p.created desc");
				foreach($affiliateProductDetails->result() as $relatedProductRow){
						$imgArr=@explode(',',$relatedProductRow->image);
						$images=array();
						foreach($imgArr as $img){
							if($img!="")$images[]=base_url().'images/product/'.$img;
						}
						$extrProductDetails[] = array('id'=>$relatedProductRow->id,
																			'seller_product_id'=>$relatedProductRow->seller_product_id,
																			'image'=>$images,
																			'product_name'=>$relatedProductRow->product_name,
																			'user_name'=>$relatedProductRow->user_name,
																			'description'=>strip_tags($relatedProductRow->description),
																//			'shippingPolicies'=>$relatedProductRow->shipping_policies,
																//			'price'=>$relatedProductRow->price,
																//			'sale_price'=>$relatedProductRow->sale_price,
																			'likes'=>$relatedProductRow->likes,
																			'category_id'=>$relatedProductRow->category_id,
																			'url'=>base_url().'json/userproduct?pid='.$relatedProductRow->seller_product_id.'&uname='.$relatedProductRow->user_name
																);
				}
				$returnArr['status'] = '1';
				$message = "Success";
				if($this->lang->line('json_success') != ""){
					$message = stripslashes($this->lang->line('json_success'));
				}
				$returnArr['response'] = $message;
				$json_encode = array("productDetails" => $productDetailsArr,"relatedProduct" => $relProductDetailsArr,"extrProductDetails"=>$extrProductDetails);
				$returnArr['productPage']=$json_encode;
	        } else {
					$json_encode = array("productDetails" => array(),"relatedProduct" => array());
					$returnArr['productPage']=$json_encode;
	    	}
		}
		echo json_encode($returnArr);
	}

	/*
	 	!! >>>>>>>>>>>> View Store Details <<<<<<<<<<<< !!
	*/
	public function view_store() {
		if($_GET['UserId']!=""){
			$thisUserId = $_GET['UserId'];
		}else{
			$thisUserId =0;
		}

		$thisUserProfileDetails = $this->mobile_model->get_all_details(USERS, array('id' => $thisUserId));
		$followingListArr = explode(',', $thisUserProfileDetails->row()->following);
		$condition = 'select s.id as sid,s.store_name,s.description,s.cover_image,s.logo_image,s.privacy,s.tagline,
			u.* from ' . STORE_FRONT . ' as s left join ' . USERS . ' as u on u.id = s.user_id where u.user_name= "' . $_GET['name'] . '"';
		$userDetails = $this->mobile_model->ExecuteQuery($condition);
		
    	//echo"<pre>";print_r($userDetails->result());die;
		if ($userDetails->num_rows() >= 1) {
            	$this->data['user_Details'] = $userDetails;
			$coverImage = base_url().'images/store/banner-dummy.jpg';
			$logoImage = base_url().'images/store/dummy-logo.png';
			if($userDetails->row()->cover_image !=''){
				$coverImage = base_url().'images/store/'.$userDetails->row()->cover_image;
			}
			if($userDetails->row()->logo_image !=''){
				$logoImage = base_url().'images/store/'.$userDetails->row()->logo_image;
			}
			$following = 'No';
			if(in_array($userDetails->row()->id,$followingListArr)){$following = 'Yes';}
			$user_Details[]=array(
				'store_id'=>$userDetails->row()->sid,
				'store_name'=>$userDetails->row()->store_name,
				'userId'=>$userDetails->row()->id,
				'user_name'=>$userDetails->row()->user_name,
				'full_name'=>$userDetails->row()->full_name,
				'followers'=>$userDetails->row()->followers,
				'followers_count'=>$userDetails->row()->followers_count,
				'description'=>strip_tags($userDetails->row()->description),
				'cover_image'=>$coverImage,
				'logo_image'=>$logoImage,
				'privacy'=>$userDetails->row()->privacy,
				'tagline'=>$userDetails->row()->tagline,
				'following' => $following
			);
		} else {
			$userDetails = $this->mobile_model->get_all_details(USERS, array('user_name' => $_GET['name']));
			$this->data['user_Details'] = $userDetails;
			$user_Details[]=array('user_name'=>$userDetails->row()->user_name);
		}
		
        if ($_GET['sort_by_price']) {
            if ($_GET['sort_by_price'] == 'desc') {
                $orderBy = ' order by p.sale_price desc';
            } else {
                $orderBy = ' order by p.sale_price asc';
            }
        } else {
            $orderBy = ' order by p.created desc ';
        }
        $this->data['product_count'] = $productDetails = $this->mobile_model->get_all_details(PRODUCT, array('user_id' => $userDetails->row()->id, 'status' => "Publish"));
        $cat_ids = array();
        $immediate_shipping = false;
        $list_ids = array();
        if ($productDetails->num_rows() > 0) {
            foreach ($productDetails->result() as $product_row) {
                if ($product_row->ship_immediate == 'true')
                    $immediate_shipping = true;
                $product_cat = $product_row->category_id;
                if ($product_cat != '') {
                    $product_cat_arr = array_filter(explode(',', $product_cat));
                    $cat_ids = array_unique(array_merge($cat_ids, $product_cat_arr));
                }
                $product_lists = $product_row->list_value;
                if ($product_lists != '') {
                    $product_lists_arr = array_filter(explode(',', $product_lists));
                    $list_ids = array_unique(array_merge($list_ids, $product_lists_arr));
                }
            }
        }
        $this->data['store_category'] = $cat_ids;
		$store_category[]=array('store_category'=>$cat_ids);
        $this->data['immediate_shipping'] = $immediate_shipping;
		$immediate_shipping[]=array('immediate_shipping'=>$immediate_shipping);
		$returnArr['user_details']=$user_Details;
		$returnArr['store_category']=$store_category;
		$returnArr['immediate_shipping']=$immediate_shipping;
        $this->data['list_ids'] = $list_ids;
		if(!empty( $this->data['list_ids'])){
			$list_ids=array('list_ids'=>$this->data['list_ids']);
			$returnArr['list_ids']=$list_ids;
		}
        $this->data['liked_count'] = $this->mobile_model->get_all_details(PRODUCT_LIKES, array('user_id' => $userDetails->row()->id));
		$like_count[] = array('count'=>$this->data['liked_count']->num_rows());
		$returnArr['like_count']=$like_count;
        $product_feedback = $this->mobile_model->product_feedback_view($productDetails->row()->user_id);
        $this->data['all_feedback'] = $product_feedback;
		if(empty($product_feedback)){
		$rating[]=array('rating'=>0);
		$returnArr['rating']=$rating;
		}
		foreach($product_feedback as $feedback) {
			$rating[]=array('rating'=>$feedback['rating']);
		}
		$returnArr['rating']=$rating;
        if ($_GET['is']) {
            $whereCond .= ' and p.ship_immediate = "' . $_GET['is'] . '"';
        }
        if ($_GET['p']) {
            $price = explode('-', $_GET['p']);
            $whereCond .= ' and p.sale_price >= "' . $price[0] . '" and p.sale_price <= "' . $price[1] . '"';
        }
        if ($_GET['q']) {
            $whereCond .= ' and p.product_name LIKE "%' . $_GET['q'] . '%"';
        }
        if ($_GET['c']) {
            $condition = " where list_value_seourl = '" . $_GET['c'] . "'";
            $listID = $this->mobile_model->getAttrubteValues($condition);
            $whereCond .= ' and FIND_IN_SET("' . $listID->row()->id . '",p.list_value)';
        }
        if ($_GET['products'] == 'products' && $_GET['category'] == ''|| $_GET['products'] == '' ) {
            $whereCond = ' where u.id=' . $userDetails->row()->id . '' . $whereCond . ' and p.status="Publish"';
            $searchProd = $whereCond . ' ' . $orderBy . ' ';
            $productList = $this->mobile_model->searchShopyByCategory($searchProd);
            $this->data['productDetails'] = $productList;
            foreach($this->data['productDetails']->result() as $productDetailLists) {
				$cond = "select user_id from ".PRODUCT_LIKES." where product_id=".$productDetailLists->seller_product_id." and user_id=".$thisUserId;
				$likedUsersList = $this->mobile_model->ExecuteQuery($cond);
				$likedStatus = "No";
				if($likedUsersList->num_rows() > 0){
					$likedStatus = "Yes";
				}
				$imgArr=explode(',', $productDetailLists->image);
				$prodImage = array();
				foreach($imgArr as $img){
				if($img!="") {$prodImage[]=base_url().'images/product/'.$img;}
				}
				$prodUrl = base_url().'json/product?pid='.$productDetailLists->id.'&UserId='.$thisUserId;
				$productDetailsArr[]=array(
											'id'=>$productDetailLists->id,
											'seller_product_id'=>$productDetailLists->seller_product_id,
											'product_name'=>$productDetailLists->product_name,
											'description'=>strip_tags($productDetailLists->description),
											'shippingPolicies'=>strip_tags($productDetailLists->shipping_policies),
											'image'=>$prodImage,
											'sale_price'=>$productDetailLists->sale_price,
											'url' => $prodUrl,
											'UserId' =>$productDetailLists->user_id,
											'seourl'=>$productDetailLists->seourl,
											'price'=>$productDetailLists->price,
											'user_name'=>$productDetailLists->user_name,
											'likes'=>$productDetailLists->likes,
											'liked' => $likedStatus,
											'shareUrl' => base_url().'things/'.$productDetailLists->id.'/'.$productDetailLists->user_name.'?ref='.$thisUserProfileDetails->row()->user_name,
											'type' => 'SellingProduct',
											'userUrl' => 'http://192.168.1.251:8081/product-working/fancyclone-v3/json/user/seller-timeline?username='.$productDetailLists->user_name
										);
				}
			$returnArr['productDetails']=$productDetailsArr;
			echo json_encode($returnArr);
        }  elseif ($_GET['category'] != '') {
            $cate_id = $this->mobile_model->get_all_details(CATEGORY, array('cat_name' => urldecode($_GET['category'])));
            $whereCond = ' where FIND_IN_SET(' . $cate_id->row()->id . ',p.category_id) and user_id = ' . $userDetails->row()->id . '' . $whereCond . ' and p.status="Publish"';
            $searchProd = $whereCond . ' ' . $orderBy . ' ';
            $productList = $this->mobile_model->searchShopyByCategory($searchProd);
            $this->data['productDetails'] = $productList;
			            foreach($this->data['productDetails']->result() as $productDetailLists) {
									$productDetailsArr[]=array(
																'id'=>$productDetailLists->id,
																'seller_product_id'=>$productDetailLists->seller_product_id,
																'product_name'=>$productDetailLists->product_name,
																'image'=>base_url().'images/product/'.$productDetailLists->image,
																'sale_price'=>$productDetailLists->sale_price);
						}
			$returnArr['productDetails']=$productDetailsArr;
			echo json_encode($returnArr);
        }
		// elseif($_GET['reviews'] == 'reviews') {;
        //     $this->data['product_feedback'] = $product_feedback;
		// 	echo '<pre>'; print_r($this->data['product_feedback']);
		// 	die;
        // }
    }


	/*
		!! >>>>>>>>>>>>>> User List <<<<<<<<<<<<<< !!
	*/
	public function display_user_lists(){
		$returnArr['status'] = "0";
		$returnArr['response'] = "";
		$username = $_GET['uname'];
		if($username != ""){
			$userProfileDetails = $this->mobile_model->get_all_details(USERS,array('user_name'=>$username));
			if ($userProfileDetails->num_rows()==1){
				if ($userProfileDetails->row()->visibility == 'Only you' && $userProfileDetails->row()->id != $this->data["commonId"]){
					$this->load->view('site/user/display_user_profile_private',$this->data);
				}else {
					$this->data['userProfileDetails'] = $userProfileDetails;
					$userProfileArr[]=array(
												'user_name'=>$userProfileDetails->row()->user_name,
												'likes'=>$userProfileDetails->row()->likes
											);
				//	$this->data['recentActivityDetails'] = $this->mobile_model->get_activity_details($userProfileDetails->row()->id);
					$this->data['listDetails'] = $this->mobile_model->get_all_details(LISTS_DETAILS,array('user_id'=>$userProfileDetails->row()->id));
					foreach($this->data['listDetails']->result() as $resultArr){
						if ($resultArr->product_id != ''){
							$pidArr = array_filter(explode(',', $resultArr->product_id));
							$productDetails = '';
							if (count($pidArr)>0){
								foreach ($pidArr as $pidRow){
									if ($pidRow!=''){
										$productDetails = $this->product_model->get_all_details(PRODUCT,array('seller_product_id'=>$pidRow,'status'=>'Publish'));
										if ($productDetails->num_rows()==0){
											$productDetails = $this->product_model->get_all_details(USER_PRODUCTS,array('seller_product_id'=>$pidRow,'status'=>'Publish'));
										}
										if ($productDetails->num_rows()==1)break;
									}
								}
							}
						//	if ($productDetails != '' && $productDetails->num_rows()==1){
								// $this->data['listImg'][$listDetailsRow->id] = $productDetails->row()->image;
							//	$returnArr['listImg'][$listDetailsRow->id] = $productDetails->row()->image;
						//	}
						$listUrl = base_url().'json/user/lists?lid='.$resultArr->id.'&uname='.$username.'&UserId='.$userProfileDetails->row()->id;
						$LImage = str_replace(","," ",$productDetails->row()->image);
						$listImage = base_url().'images/product/'.$LImage;
						$listDetailsArr[] = array('id'=>$resultArr->id,
														'name'=>$resultArr->name,
														'product_id'=>$resultArr->product_id,
														'user_id'=>$resultArr->user_id,
														'product_count'=>$resultArr->product_count,
														'followers_count'=>$resultArr->followers_count,
														'TotalProductsCount' => strval(count(array_filter(explode(',',$resultArr->product_id)))),
														'listImage' => $listImage,
														'listUrl' => $listUrl
														);
						}

					}
					$returnArr['status'] = "1";
					$message = "Success";
					if($this->lang->line('json_success') != ""){
						$message = stripslashes($this->lang->line('json_success'));
					}
					$returnArr['response'] = $message;
					$returnArr['userProfileDetails']=$userProfileArr;
					$returnArr['listDetails']=$listDetailsArr;
					$this->data['follow'] = $this->mobile_model->view_follow_list($userProfileDetails->row()->id);
					$returnArr['total_follows']=$this->data['follow']->num_rows();
				}
			}else {
				$message = "No data found";
				if($this->lang->line('json_no_data_found') != ""){
				    $message = stripslashes($this->lang->line('json_no_data_found'));
				}
				$returnArr['response'] = $message;
			}
		}else{
			$returnArr['response'] =  'UserName Null !!';
		}
		echo json_encode($returnArr);
	}


	/*
		!! >>>>>>>>>>>>>> RECENT ACTIVITES OF USER <<<<<<<<<<<<<< !!
	*/
	public function display_user_lists_home() {
		$returnArr['status'] = "0";
		$returnArr['response'] = "";
		$lid = intval($_GET['lid']);
		$uname = $_GET['uname'];
		$thisUserId = $_GET['UserId'];
		if($lid == "" && $uname == "" && $thisUserId == ""){
			$returnArr['response'] = "Some Parameters are missing !!";
		}else{
			$thisUserProfileDetails = $this->mobile_model->get_all_details(USERS, array('id' => $thisUserId));
			$this->data['user_profile_details'] = $userProfileDetails = $this->mobile_model->get_all_details(USERS, array('user_name' => $uname));
			if ($userProfileDetails->row()->visibility == 'Only you' && $userProfileDetails->row()->id != $this->data["commonId"]) {
				$this->load->view('site/user/display_user_profile_private', $this->data);
			} else {
				$this->data['list_details'] = $list_details = $this->mobile_model->get_all_details(LISTS_DETAILS, array('id' => $lid, 'user_id' =>$this->data['user_profile_details']->row()->id));
				if ($this->data['list_details']->num_rows() == 0) {
					$message = "No lists avaliable";
					if($this->lang->line('json_no_list_found') != ""){
					    $message = stripslashes($this->lang->line('json_no_list_found'));
					}
					$returnArr['response'] = $message;
				} else {
					$searchArr = array_filter(explode(',', $list_details->row()->product_id));
					if (count($searchArr) > 0) {
						$fieldsArr = array(PRODUCT . '.*', USERS . '.user_name', USERS . '.full_name');
						$condition = array(PRODUCT . '.status' => 'Publish');
						$joinArr1 = array('table' => USERS, 'on' => USERS . '.id=' . PRODUCT . '.user_id', 'type' => '');
						$joinArr = array($joinArr1);
						$product_details = $this->product_model->get_fields_from_many(PRODUCT, $fieldsArr, PRODUCT . '.seller_product_id', $searchArr, $joinArr, '', '', $condition);
						$this->data['totalProducts'] = strval(count($searchArr));
						$fieldsArr = array(USER_PRODUCTS . '.*', USERS . '.user_name', USERS . '.full_name');
						$condition = array(USER_PRODUCTS . '.status' => 'Publish');
						$joinArr1 = array('table' => USERS, 'on' => USERS . '.id=' . USER_PRODUCTS . '.user_id', 'type' => '');
						$joinArr = array($joinArr1);
						$notsell_product_details= $this->product_model->get_fields_from_many(USER_PRODUCTS, $fieldsArr, USER_PRODUCTS . '.seller_product_id', $searchArr, $joinArr, '', '', $condition);
					}
					$FinalproductDetails = $this->mobile_model->get_sorted_array_mobile($product_details, $notsell_product_details, 'created', 'desc');
					foreach($FinalproductDetails as $prodRow){
						$cond = "select user_id from ".PRODUCT_LIKES." where product_id=".$prodRow['seller_product_id']." and user_id=".$thisUserId;
						$likedUsersList = $this->mobile_model->ExecuteQuery($cond);
						$likedStatus = "No";
						if($likedUsersList->num_rows() > 0){
							$likedStatus = "Yes";
						}
						$imgArr=@explode(',',$prodRow['image']);
						$images=array();
						foreach($imgArr as $img){
							if($img!="")$images[]=base_url().'images/product/'.$img;
						}
						$prodArr[]=array(
										'id'=>$prodRow['id'],
										'seller_product_id'=>$prodRow['seller_product_id'],
										'product_name'=>$prodRow['product_name'],
										'seourl'=>$prodRow['seourl'],
										'description'=>strip_tags($prodRow['description']),
										'shippingPolicies'=>strip_tags($prodRow['shipping_policies']),
										'image'=>$images,
										'price'=>$prodRow['price'],
										'UserId' =>$prodRow['user_id'],
										'sale_price'=>$prodRow['sale_price'],
										'user_name'=>$prodRow['user_name'],
										'likes'=>$prodRow['likes'],
										'liked' => $likedStatus,
										'url'=>$prodRow['url'].'&UserId='.$thisUserId,
										'type' => $prodRow['type'],
										'shareUrl' => base_url().'things/'.$prodRow['id'].'/'.$prodRow['user_name'].'?ref='.$thisUserProfileDetails->row()->user_name,
										'userUrl' => 'http://192.168.1.251:8081/product-working/fancyclone-v3/json/user/seller-timeline?username='.$prodRow['user_name']
										);
					}
					foreach($list_details->result() as $listProduct){
						$listsProducts[]=array(
											'name'=>$listProduct->name,
											'user_id'=>$listProduct->user_id,
											'product_id'=>$listProduct->product_id,
											'followers'=>$listProduct->followers,
											'id'=>$listProduct->id,
											'user_id'=>$listProduct->user_id,
											'category_id'=>$listProduct->category_id,
											'followers_count'=>$listProduct->followers_count
										);
					}
					$returnArr['status'] = "1";
					$message = "Success";
					if($this->lang->line('json_success') != ""){
						$message = stripslashes($this->lang->line('json_success'));
					}
					$returnArr['response'] = $message;
					$returnArr['totalProducts'] = $this->data['totalProducts'];
					$returnArr['productDetails'] = $prodArr;
					$returnArr['listsProducts'] = $listsProducts;
				}
			}
		}
		echo json_encode($returnArr);
    }

	/*
		!! >>>>>>>>>>>>>> RECENT ACTIVITES OF USER <<<<<<<<<<<<<< !!
	*/
	public function seller_timeline() {
		$returnArr['status'] = "0";
		$returnArr['response'] = "";
		$thisUserId = $_GET['UserId'];
		$username = $_GET['username'];
		$username = trim($username);
		if($thisUserId == "" && $username == ""){
			$returnArr['response'] = "Some parameters are missing !!";
		}else{
			$thisUserProfileDetails = $this->mobile_model->get_all_details(USERS, array('id' => $thisUserId));
			$followingListArr = explode(',', $thisUserProfileDetails->row()->following);
			 $this->data['userProfileDetails'] = $this->mobile_model->get_all_details(USERS, array('user_name' => $username, 'status' => 'Active'));
			/* !! >>>>>>>>>>>>>>>> USER  WANTS PRODUCTS<<<<<<<<<<<<<<  !! */
				$wantList = $this->mobile_model->get_all_details(WANTS_DETAILS, array('user_id' => $this->data['userProfileDetails']->row()->id));
				$WantsProductDetail =array();
			//	print_r($wantList->result());die;
				if($wantList->num_rows() > 0 && $wantList->row()->product_id != ""){
					$wantProductDetails = $this->mobile_model->get_wants_product($wantList);
					$notSellProducts    = $this->mobile_model->get_notsell_wants_product($wantList);
					$WantProductDetail= $this->mobile_model->get_sorted_array_mobile($wantProductDetails, $notSellProducts, 'created', 'desc');
					foreach($WantProductDetail as $wantsProduct){
						$cond = "select user_id from ".PRODUCT_LIKES." where product_id=".$wantsProduct['seller_product_id']." and user_id=".$thisUserId;
						$likedUsersList = $this->mobile_model->ExecuteQuery($cond);
						$likedStatus = "No";
						if($likedUsersList->num_rows() > 0){
							$likedStatus = "Yes";
						}
								$WantsProductDetail[]=array(
									'product_name'=>$wantsProduct['product_name'],
									'full_name'=>$wantsProduct['full_name'],
									'user_name'=>$wantsProduct['user_name'],
									'id'=>$wantsProduct['id'],
									'UserId' =>$wantsProduct['user_id'],
									'price'=>$wantsProduct['price'],
									'sale_price'=>$wantsProduct['sale_price'],
									'seller_product_id'=>$wantsProduct['seller_product_id'],
									'user_id'=>$wantsProduct['user_id'],
									'likes'=>$wantsProduct['likes'],
									'liked' => $likedStatus,
									'image'=>base_url().'images/product/'.str_replace(',','',$wantsProduct['image']),
									'description'=>strip_tags($wantsProduct['description']),
									'shippingPolicies'=>strip_tags($wantsProduct['shipping_policies']),
									'productUrl'=>$wantsProduct['url'].'&UserId='.$thisUserId,
									'shareUrl' => $wantsProduct['shareUrl'].'?ref='.$thisUserProfileDetails->row()->user_name,
									'seourl'=>$wantsProduct['seourl'],
									'type' => $wantsProduct['type'],
									'UserTimeline' => 'http://192.168.1.251:8081/product-working/fancyclone-v3/json/user/seller-timeline?username='.$wantsProduct['user_name']
								);
					}
				}

				/* !! >>>>>>>>>>>>>>>> USER  WANTS PRODUCTS<<<<<<<<<<<<<<  !! */

				$follow = $this->mobile_model->view_follow_list($this->data['userProfileDetails']->row()->id);
				$this->data['recentActivityDetails'] = $this->mobile_model->get_activity_details($this->data['userProfileDetails']->row()->id);
				$userProfileDetails = array();
				foreach($this->data['userProfileDetails']->result() as $user){
					$fb_link = $user->facebook;
					if (substr($fb_link, 0,4)!='http') $fb_link='http://'.$fb_link;
					$tw_link = $user->twitter;
					if (substr($tw_link, 0,4)!='http') $tw_link='http://'.$tw_link;
					$go_link = $user->google;
					if (substr($go_link, 0,4)!='http') $go_link='http://'.$go_link;
					$userThumbnail = base_url().'images/users/user-thumb1.png';
					if($user->thumbnail !='') $userThumbnail = base_url().'images/users/'.$user->thumbnail;
					$following = 'No';
					if(in_array($user->id,$followingListArr)){$following = 'Yes';}
					$userProfileDetails[] = array(
													'id'=> $user->id,
													'userName'=> $user->user_name,
													'fullName'=> $user->full_name,
													'likesCount'=> $user->likes,
													'productsCount'=> $user->products,
													'listsCount'=> $user->lists,
													'email'=> $user->email,
													'wantCount'=> strval(count($WantProductDetail)),
													'ownCount'=> $user->own_count,
													'userThumbnail'=> $userThumbnail,
													'web_url'=> $user->web_url,
													'location'=> $user->location,
													'about'=> $user->about,
													'facebook'=> $fb_link,
													'twitter'=> $tw_link,
													'google'=> $go_link,
													//'verifiedUser'=> $user->is_verified,
													'group'=> $user->group,
													'followersCount'=> $user->followers_count,
													'followingCount'=> $user->following_count,
													'followCount' => strval($follow->num_rows()),
													'following' => $following,
													'storePayment'=> $user->store_payment,
													'storeStatus' => $user->request_status
												);
				}


				/* !! >>>>>>>>>>>>>>>> USER RECENT ACTIVITY<<<<<<<<<<<<<<  !! */

				$userReacentActitvity = array();
				if($this->lang->line('date_year') != '')
				   $date_year = $this->lang->line('date_year');
				else
				   $date_year = "Year";

				if($this->lang->line('date_years') != '')
				   $date_years = $this->lang->line('date_years');
				else
				   $date_years = "Years";

				if($this->lang->line('date_month') != '')
				   $date_month = $this->lang->line('date_month');
				else
				   $date_month = "Month";

				if($this->lang->line('date_months') != '')
				   $date_months = $this->lang->line('date_months');
				else
				   $date_months = "Months";

				if($this->lang->line('date_week') != '')
				   $date_week = $this->lang->line('date_week');
				else
				   $date_week = "Week";

				if($this->lang->line('date_weeks') != '')
				   $date_weeks = $this->lang->line('date_weeks');
				else
				   $date_weeks = "Weeks";

				if($this->lang->line('date_day') != '')
				   $date_day = $this->lang->line('date_day');
				else
				   $date_day = "Day";

				if($this->lang->line('date_days') != '')
				   $date_days = $this->lang->line('date_days');
				else
				   $date_days = "Days";

				if($this->lang->line('date_hour') != '')
				   $date_hour = $this->lang->line('date_hour');
				else
				   $date_hour = "Hour";

				if($this->lang->line('date_hours') != '')
				   $date_hours = $this->lang->line('date_hours');
				else
				   $date_hours = "Hours";

				if($this->lang->line('date_minute') != '')
				   $date_minute = $this->lang->line('date_minute');
				else
				   $date_minute = "Minute";

				if($this->lang->line('date_minutes') != '')
				   $date_minutes = $this->lang->line('date_minutes');
				else
				   $date_minutes = "Minutes";

				if($this->lang->line('date_second') != '')
				   $date_second = $this->lang->line('date_second');
				else
				   $date_second = "Second";

				if($this->lang->line('date_seconds') != '')
				   $date_seconds = $this->lang->line('date_seconds');
				else
				   $date_seconds = "Seconds";

				if($this->lang->line('ago') != '')
				   $date_ago = $this->lang->line('ago');
				else
				   $date_ago = "ago";

				$date_lg_arr = array($date_years, $date_year,$date_months,$date_month,$date_weeks,$date_week,$date_days,$date_day,$date_hours,$date_hour,$date_minutes,$date_minute,$date_seconds,$date_second,$date_ago);
				$date_txt_arr = array( "Years", "Year", "Months", "Month","Weeks", "Week","Days","Day","Hours", "Hour","Minutes", "Minute", "Seconds", "Second", "ago");
				$userReacentActitvity = array();
				foreach($this->data['recentActivityDetails']->result() as $userActivities){
					$thumbnail = base_url().'images/users/user-thumb1.png';
					if($userActivities->thumbnail !='') $thumbnail = base_url().'images/users/'.$userActivities->thumbnail;
					$productImage =base_url().'images/product/dummyProductImage.jpg';
					if($userActivities->image !='') $productImage = base_url().'images/product/'.$userActivities->image;
					$activityTime = strtotime($userActivities->activity_time);
					$actTime = timespan($activityTime).' ago';
					$actTime = str_replace($date_txt_arr, $date_lg_arr, $actTime);
					$userReacentActitvity[] = array(
																		'id' => $userActivities->id,
																		'activityName'=>  $userActivities->activity_name,
																		'activityId' => $userActivities->activity_id,
																		'userId' => $userActivities->user_id,
																		'productName' => $userActivities->product_name,
																		'productID' => $userActivities->productID,
																		'productImage' => $productImage,
																		'time' => $actTime,
																		'userProductName' => $userActivities->user_product_name,
																		'fullName' => $userActivities->full_name,
																		'userName' => $userActivities->user_name,
																		'thumnail' => $thumbnail
																	);
				}
				/* !! >>>>>>>>>>>>>>>> USER ADDED PRODUCTS<<<<<<<<<<<<<<  !! */
				$this->data['addedProductDetails']=$addedProductDetails = $this->product_model->view_product_details(' where p.user_id=' .$this->data['userProfileDetails']->row()->id . ' and p.status="Publish"');
				$this->data['notSellProducts'] = $affiliateProductDetails =  $this->product_model->view_notsell_product_details(' where p.user_id=' . $this->data['userProfileDetails']->row()->id . ' and p.status="Publish"');
				$productDetails =  $this->data['productDetails'] = $this->mobile_model->get_sorted_array_mobile($addedProductDetails, $affiliateProductDetails, 'created', 'desc');
				$userAddedProducts = array();
				foreach($productDetails as $addedProduct){
					$cond = "select user_id from ".PRODUCT_LIKES." where product_id=".$addedProduct['seller_product_id']." and user_id=".$thisUserId;
					$likedUsersList = $this->mobile_model->ExecuteQuery($cond);
					$likedStatus = "No";
					if($likedUsersList->num_rows() > 0){
						$likedStatus = "Yes";
					}
					$imgArr=@explode(',',$addedProduct['image']);
					$images=array();
					foreach($imgArr as $img){
					if($img!="")
						$images[]=base_url().'images/product/'.$img;
						}
							$userAddedProducts[]=array(

																	'seller_product_id'=>$addedProduct['seller_product_id'],
																	'UserId' =>$addedProduct['user_id'],
																	'seourl'=>$addedProduct['seourl'],
																	'price'=>$addedProduct['price'],
																	'sale_price'=>$addedProduct['sale_price'],
																	'liked' => $likedStatus,
																	'productUrl'=>$addedProduct['url'].'&UserId='.$thisUserId,
																	'shareUrl' => $addedProduct['shareUrl'].'?ref='.$thisUserProfileDetails->row()->user_name,
																	'id'=>$addedProduct['id'],
																	'description'=>strip_tags($addedProduct['description']),
																	'shippingPolicies'=>strip_tags($addedProduct['shipping_policies']),
																	'product_name'=>$addedProduct['product_name'],
																	'full_name'=>$addedProduct['full_name'],
																	'user_name'=>$addedProduct['user_name'],
																	'user_id'=>$addedProduct['user_id'],
																	'category_id'=>$addedProduct['category_id'],
																	'likes'=>$addedProduct['likes'],
																	'image'=>$images,
																	'type' => $addedProduct['type'],
																	'UserTimeline' => 'http://192.168.1.251:8081/product-working/fancyclone-v3/json/user/seller-timeline?username='.$addedProduct['user_name']
																	);
							}


				/* !! >>>>>>>>>>>>>>>> USER LIKED PRODUCTS<<<<<<<<<<<<<<  !! */

				$this->data['listDetails'] = $this->mobile_model->get_all_details(LISTS_DETAILS,array('user_id'=>$this->data['userProfileDetails']->row()->id));
				$listDetailsArr = array();
				foreach($this->data['listDetails']->result() as $resultArr){
					if ($resultArr->product_id != ''){
						$pidArr = array_filter(explode(',', $resultArr->product_id));
						$productDetails = '';
						if (count($pidArr)>0){
							foreach ($pidArr as $pidRow){
								if ($pidRow!=''){
									$productDetails = $this->product_model->get_all_details(PRODUCT,array('seller_product_id'=>$pidRow,'status'=>'Publish'));
									if ($productDetails->num_rows()==0){
										$productDetails = $this->product_model->get_all_details(USER_PRODUCTS,array('seller_product_id'=>$pidRow,'status'=>'Publish'));
									}
									if ($productDetails->num_rows()==1)break;
								}
							}
						}
					$listUrl = base_url().'json/user/lists?lid='.$resultArr->id.'&uname='.$username.'&UserId='.$thisUserId;
					$LImage = str_replace(","," ",$productDetails->row()->image);
					$listImage = base_url().'images/product/'.$LImage;
					$listDetailsArr[] = array('id'=>$resultArr->id,
													'name'=>$resultArr->name,
													'product_id'=>$resultArr->product_id,
													'user_id'=>$resultArr->user_id,
													'product_count'=>$resultArr->product_count,
													'followers_count'=>$resultArr->followers_count,
													'TotalProductsCount' => strval(count(array_filter(explode(',',$resultArr->product_id)))),
													'listImage' => $listImage,
													'listUrl' => $listUrl
													);
					}
				}


			/* !! >>>>>>>>>>>>>>>> USER  FOLLOWS<<<<<<<<<<<<<<  !! */
					$userFollows = array();
					foreach($follow->result() as $userFollow){
							$userFollows[] = array(
																'id' => $userFollow->id,
																'name' => $userFollow->name,
																'UserId' =>$userFollow->user_id,
																'productId' => $userFollow->product_id,
																'followers' => $userFollow->followers,
																'banner' => $userFollow->banner,
																'categoryId' => $userFollow->category_id,
																'contributors' => $userFollow->contributors,
																'contributorsInvited' => $userFollow->contributors_invited,
																'productCount' => $userFollow->product_count,
																'followersCount' => $userFollow->followers_count,
																'pname' => $userFollow->pname,
															);
					}


				/* !! >>>>>>>>>>>>>>>> USER  OWN PRODUCTS<<<<<<<<<<<<<<  !! */
						$productIdsArr = array_filter(explode(',', $this->data['userProfileDetails']->row()->own_products));
		                $productIds = '';
		                if (count($productIdsArr) > 0) {
		                    foreach ($productIdsArr as $pidRow) {
		                        if ($pidRow != '') {
		                            $productIds .= $pidRow . ',';
		                        }
		                    }
		                    $productIds = substr($productIds, 0, -1);
		                }
		                if ($productIds != '') {
		                    $this->data['ownsProductDetails']=$ownsProductDetails= $this->product_model->view_product_details(' where p.seller_product_id in (' . $productIds . ') and p.status="Publish"');
		                    $this->data['notSellProducts'] =$notSellProducts= $this->product_model->view_notsell_product_details(' where p.seller_product_id in (' . $productIds . ') and p.status="Publish"');
		                } else {
		                    $this->data['addedProductDetails'] = '';
		                    $this->data['notSellProducts'] = '';
		                }
						$ownsProductDetail= $this->mobile_model->get_sorted_array_mobile($ownsProductDetails, $notSellProducts, 'created', 'desc');
						$ownsProducts = array();
						foreach($ownsProductDetail as $ownsProduct){
							$cond = "select user_id from ".PRODUCT_LIKES." where product_id=".$ownsProduct['seller_product_id']." and user_id=".$thisUserId;
							$likedUsersList = $this->mobile_model->ExecuteQuery($cond);
							$likedStatus = "No";
							if($likedUsersList->num_rows() > 0){
								$likedStatus = "Yes";
							}
									$ownsProducts[]=array(
										'seller_product_id'=>$ownsProduct['seller_product_id'],
										'UserId' =>$ownsProduct['user_id'],
										'seourl'=>$ownsProduct['seourl'],
										'price'=>$ownsProduct['price'],
										'sale_price'=>$ownsProduct['sale_price'],
										'productUrl1'=> $ownsProduct['url'].'&UserId='.$thisUserId,
										'shareUrl' => $ownsProduct['shareUrl'].'?ref='.$thisUserProfileDetails->row()->user_name,
										'product_name'=>$ownsProduct['product_name'],
										'full_name'=>$ownsProduct['full_name'],
										'user_name'=>$ownsProduct['user_name'],
										'id'=>$ownsProduct['id'],
										'user_id'=>$ownsProduct['user_id'],
										'likes'=>$ownsProduct['likes'],
										'liked' => $likedStatus,
										'image'=>base_url().'images/product/'.str_replace(',','',$ownsProduct['image']),
										'description'=>strip_tags($ownsProduct['description']),
										'shippingPolicies'=>strip_tags($ownsProduct['shipping_policies']),
										'productUrl'=>$ownsProduct['url'],
										'type' => $ownsProduct['type'],
										'UserTimeline' => 'http://192.168.1.251:8081/product-working/fancyclone-v3/json/user/seller-timeline?username='.$ownsProduct['user_name']
										);
						}

					$productLikeDetails= $this->mobile_model->get_like_details_fully($this->data['userProfileDetails']->row()->id);
					$userProductLikeDetails= $this->mobile_model->get_like_details_fully_user_products($this->data['userProfileDetails']->row()->id);
					$userLikeProduct= $this->mobile_model->get_sorted_array_mobile($productLikeDetails, $userProductLikeDetails, 'created', 'desc');
					$userLikedProducts = array();
					foreach($userLikeProduct as $userLike){
						$cond = "select user_id from ".PRODUCT_LIKES." where product_id=".$userLike['seller_product_id']." and user_id=".$thisUserId;
						$likedUsersList = $this->mobile_model->ExecuteQuery($cond);
						$likedStatus = "No";
						if($likedUsersList->num_rows() > 0){
							$likedStatus = "Yes";
						}
								$userLikedProducts[]=array(
									'seller_product_id'=>$userLike['seller_product_id'],
									'UserId' =>$userLike['user_id'],
									'seourl'=>$userLike['seourl'],
									'price'=>$userLike['price'],
									'sale_price'=>$userLike['sale_price'],
									'liked' => $likedStatus,
									'productUrl2'=>$userLike['url'].'&UserId='.$thisUserId,
									'shareUrl' => $userLike['shareUrl'].'?ref='.$thisUserProfileDetails->row()->user_name,
									'product_name'=>$userLike['product_name'],
									'full_name'=>$userLike['full_name'],
									'user_name'=>$userLike['user_name'],
									'id'=>$userLike['id'],
									'user_id'=>$userLike['user_id'],
									'likes'=>$userLike['likes'],
									'image'=>base_url().'images/product/'.str_replace(',','',$userLike['image']),
									'description'=>strip_tags($userLike['description']),
									'shippingPolicies'=>strip_tags($userLike['shipping_policies']),
									'productUrl'=>$userLike['url'],
									'type' => $userLike['type'],
									'UserTimeline' => 'http://192.168.1.251:8081/product-working/fancyclone-v3/json/user/seller-timeline?username='.$userLike['user_name']
									);
					}
			$returnArr['status'] = "1";
			$message = "Success";
			if($this->lang->line('json_success') != ""){
				$message = stripslashes($this->lang->line('json_success'));
			}
			$returnArr['response'] = $message;
			$returnArr['userProfileDetails'] = $userProfileDetails;  /* !! >>>>>>>>>>>>>>>> USER PROFILE<<<<<<<<<<<<<<  !! */
			$returnArr['userReacentActitvity'] = $userReacentActitvity; /* !! >>>>>>>>>>>>>>>> USER RECENT ACTIVITY <<<<<<<<<<<<<<  !! */
			$returnArr['userAddedProducts'] = $userAddedProducts;  /* !! >>>>>>>>>>>>>>>> USER ADDED PRODUCTS<<<<<<<<<<<<<<  !! */
			$returnArr['listDetails']=$listDetailsArr;  /* !! >>>>>>>>>>>>>>>> USER LIKED PRODUCTS<<<<<<<<<<<<<<  !! */
			$returnArr['userFollows']=$userFollows;  /* !! >>>>>>>>>>>>>>>> USER  FOLLOWS<<<<<<<<<<<<<<  !! */
			$returnArr['ownsProduct'] = $ownsProducts; 	/* !! >>>>>>>>>>>>>>>> USER  OWN PRODUCTS<<<<<<<<<<<<<<  !! */
			$returnArr['WantsProductDetail'] = $WantsProductDetail; /* !! >>>>>>>>>>>>>>>> USER  WANTS <<<<<<<<<<<<<<  !! */
			$returnArr['userLikedProducts'] = $userLikedProducts; /* !! >>>>>>>>>>>>>>>> USER  WANTS <<<<<<<<<<<<<<  !! */
		}
		echo json_encode($returnArr);
	}


	/*
	* !! >>>>>>>>>>>>> Product Liking Function  <<<<<<<<<<<< !!
	*/
 	public function featured_product(){
		$this->data['featured_products']=$featured_products = $this->mobile_model->get_featured_products();
		if($featured_products->num_rows > 0){
				 foreach($this->data['featured_products']->result() as $featuredProResult){
					$imgArr=@explode(',',$featuredProResult->image);
					/* $productImage = ($imgArr[0]=='') ? '' : base_url().'images/product/'.$imgArr[0]; */
					$images=array();
					foreach($imgArr as $img){
					if($img!="")$images[]=base_url().'images/product/'.$img;
					}
					$featured_productsArr[]=array(
												'id'=> $featuredProResult->id,
												'seller_product_id'=> $featuredProResult->seller_product_id,
												'product_name'=> $featuredProResult->product_name,
												'seourl'=> $featuredProResult->seourl,
												'description'=>strip_tags($featuredProResult->description),
												'shippingPolicies'=> strip_tags($featuredProResult->shipping_policies),
												'image'=>$images,
												'price'=> $featuredProResult->price,
												'sale_price'=>$featuredProResult->sale_price,
												'user_name'=>$featuredProResult->user_name,
												'likes'=>$featuredProResult->likes,
												'url'=> $ar1_row['url'] = base_url().'json/product?pid='.$featuredProResult->id,
												'type' =>'sellerProduct',
												'userUrl' => 	$normalUserUrl = 'http://192.168.1.251:8081/product-working/fancyclone-v3/json/user/seller-timeline?username='.$featuredProResult->user_name
												);
				}
			$returnArr['featuredProductsList']=$featured_productsArr;
		}else {
			$returnArr['featuredProductsList']=array();
		}
		echo json_encode($returnArr);
 	}

	/*
	* !! >>>>>>>>>>>>> Product Liking Function  <<<<<<<<<<<< !!
	*/
	public function add_fancy_item() {
		$returnArr['status_code'] = "0";
		$returnArr['response'] = "";
		$userId = trim($_GET['userId']);
		$tid = trim($_GET['tid']);
		$ipAdrress = trim($_GET['ipAdrress']);
		if($userId == "" && $tid==""){
			$returnArr['response'] = "Some parameters are missing";
		}else{
			$this->data['userProfileDetails'] = $this->mobile_model->get_all_details(USERS, array('id' => $userId, 'status' => 'Active'));
			$checkProductLike = $this->mobile_model->get_all_details(PRODUCT_LIKES, array('product_id' => $tid, 'user_id' =>$userId));
			if ($checkProductLike->num_rows() == 0) {
					$productDetails = $this->mobile_model->get_all_details(PRODUCT, array('seller_product_id' => $tid));
                    $productOfUser = $this->user_model->get_all_details(USERS, array('id' => $productDetails->row()->user_id));
				if ($productDetails->num_rows() == 0) {
						$productDetails = $this->mobile_model->get_all_details(USER_PRODUCTS, array('seller_product_id' => $tid));
						$productOfUser = $this->user_model->get_all_details(USERS, array('id' => $productDetails->row()->user_id));
	                    $productType= "UserProduct";
	                    $productUrl = base_url().'json/userproduct?pid='.$tid.'&uname='.$productOfUser->row()->user_name;
						$productTable = USER_PRODUCTS;
				} else {
						$productType= "SellingProduct";
	                    $productUrl = base_url().'json/product?pid='.$productDetails->row()->id;
						$productTable = PRODUCT;
				}
				if ($productDetails->num_rows() == 1) {
					$likes = $productDetails->row()->likes;
					$dataArr = array('product_id' => $tid, 'user_id' => $userId, 'ip' => $ipAdrress);
					$this->mobile_model->simple_insert(PRODUCT_LIKES, $dataArr);
					$actArr = array(
						'activity_name' => 'fancy',
						'activity_id' => $tid,
						'user_id' => $userId,
						'activity_ip' => $ipAdrress
					);
					$this->mobile_model->simple_insert(USER_ACTIVITY, $actArr);
					$datestring = "%Y-%m-%d %H:%i:%s";
					$time = time();
					$createdTime = mdate($datestring, $time);
					$actArr = array(
						'activity' => 'like',
						'activity_id' => $tid,
						'user_id' => $userId,
						'activity_ip' => $ipAdrress,
						'created' => $createdTime
					);
					$this->mobile_model->simple_insert(NOTIFICATIONS, $actArr);
					$likes++;
					$dataArr = array('likes' => $likes);
					$condition = array('seller_product_id' => $tid);
					$this->mobile_model->update_details($productTable, $dataArr, $condition);
					$totalUserLikes = $this->data['userProfileDetails']->row()->likes;
					$totalUserLikes++;
					$this->mobile_model->update_details(USERS, array('likes' => $totalUserLikes), array('id' =>$userId));
					$returnArr['status_code'] = 1;

					$this->data['userDetails'] = $this->user_model->get_all_details(USERS, array('id' => $userId));
	                    $deive_type = '';
	                    if($productOfUser->row()->gcm_buyer_id!=''){
	                        $device_id = $productOfUser->row()->gcm_buyer_id;
	                        $deive_type = 'ANDROID';
								if($deive_type!=''){
									$action = 'like';
									if($this->data['userDetails']->row()->full_name != ""){
										$name = $this->data['userDetails']->row()->full_name;
									}else{
										$name = $this->data['userDetails']->row()->user_name;
									}
									$imgArr=@explode(',',$productDetails->row()->image);
									$productImage = ($imgArr[0]=='') ? '' : base_url().'images/product/'.$imgArr[0];
									$cond = "select user_id from ".PRODUCT_LIKES." where product_id=".$productDetails->row()->seller_product_id." and user_id=".$productDetails->row()->user_id;
									$likedUsersList = $this->user_model->ExecuteQuery($cond);
									$likedStatus = "No";
									if($likedUsersList->num_rows() > 0){
										$likedStatus = "Yes";
									}
									$urls = $productUrl.'&UserId='.$productDetails->row()->user_id;
									$shipngdtls = $productDetails->row()->shipping_policies;
									if($productDetails->row()->shipping_policies == ""){
										$shipngdtls = ".";
									}
									$desdtls = $productDetails->row()->description;
									if($productDetails->row()->description == ""){
										$desdtls = ".";
									}
									$urlval = array(
												$productType,
												$productDetails->row()->id,
												$productDetails->row()->seller_product_id,
												$productDetails->row()->user_id,
												$productDetails->row()->product_name,
												$urls,
												$productDetails->row()->seourl,
												strip_tags($desdtls),
												strip_tags($shipngdtls),
												$productImage,
												$productDetails->row()->price,
												$productDetails->row()->sale_price,
												$productOfUser->row()->user_name,
												$this->data['userDetails']->row()->user_name ,
												$productDetails->row()->likes,
												$likedStatus
											);
									$message = ucwords($name).' '.'liked your product'. ' "'.$productDetails->row()->product_name.'"';
									$regIds = array($device_id);
									$this->sendPushNotification($regIds, $message, $action, $deive_type,$urlval);
								}
	                    }
				} else {
					if ($this->lang->line('prod_not_avail') != ''){
						$returnArr['message'] = $this->lang->line('prod_not_avail');
					}
					else{
						$returnArr['message'] = 'Product not available';
					}
				}
			}else{
				if ($this->lang->line('json_already_liked') != ''){
					$returnArr['message'] = $this->lang->line('json_already_liked');
				}
				else{
					$returnArr['message'] = 'You have already liked this product';
				}
			}
		}
		echo json_encode($returnArr);
	}

	/*
	* !! >>>>>>>>>>>>> Product Unliking Function  <<<<<<<<<<<< !!
	*/
	public function remove_fancy_item() {
	    $returnArr['status_code'] = 0;
		$userId = trim($_GET['userId']);
		$tid = trim($_GET['tid']);
		$ipAdrress = trim($_GET['ipAdrress']);
		$this->data['userProfileDetails'] = $this->mobile_model->get_all_details(USERS, array('id' => $userId, 'status' => 'Active'));
	    $checkProductLike = $this->mobile_model->get_all_details(PRODUCT_LIKES, array('product_id' => $tid, 'user_id' => $userId));
        if ($checkProductLike->num_rows() == 1) {
            $productDetails = $this->mobile_model->get_all_details(PRODUCT, array('seller_product_id' => $tid));
            if ($productDetails->num_rows() == 0) {
                $productDetails = $this->mobile_model->get_all_details(USER_PRODUCTS, array('seller_product_id' => $tid));
                $productTable = USER_PRODUCTS;
            } else {
                $productTable = PRODUCT;
            }
            if ($productDetails->num_rows() == 1) {
                $likes = $productDetails->row()->likes;
                $conditionArr = array('product_id' => $tid, 'user_id' => $userId);
                $this->mobile_model->commonDelete(PRODUCT_LIKES, $conditionArr);
                $actArr = array(
                    'activity_name' => 'unfancy',
                    'activity_id' => $tid,
                    'user_id' => $userId,
                    'activity_ip' => $ipAdrress
                );
                $this->mobile_model->simple_insert(USER_ACTIVITY, $actArr);
                $likes--;
                $dataArr = array('likes' => $likes);
                $condition = array('seller_product_id' => $tid);
                $this->mobile_model->update_details($productTable, $dataArr, $condition);
                $totalUserLikes = $this->data['userProfileDetails']->row()->likes;
                $totalUserLikes--;
                $this->mobile_model->update_details(USERS, array('likes' => $totalUserLikes), array('id' => $userId));
                $returnArr['status_code'] = 1;
            } else {
                if ($this->lang->line('prod_not_avail') != '')
                    $returnArr['message'] = $this->lang->line('prod_not_avail');
                else
                    $returnArr['message'] = 'Product not available';
            }
        }
	    echo json_encode($returnArr);
	}


	/*
	* !! >>>>>>>>>>>>> Product Unliking Function  <<<<<<<<<<<< !!
	*/
	public function add_list_when_fancyy() {
	        $returnArr['status_code'] = 0;
	        $returnArr['listCnt'] = '';
	        $returnArr['wanted'] = 0;
	        $uniqueListNames = array();
          //  $tid = $this->input->post('tid');
            $tid= $_GET['tid'];
            $userId = $_GET['userId'];
            $firstCatName = '';
            $firstCatDetails = '';
            $count = 1;
            $ListCount  =  array();
            //Adding lists which was not already created from product categories
            $productDetails = $this->mobile_model->get_all_details(PRODUCT, array('seller_product_id' => $tid));
            if ($productDetails->num_rows() == 0) {
                $productDetails = $this->mobile_model->get_all_details(USER_PRODUCTS, array('seller_product_id' => $tid));
            }
            if ($productDetails->num_rows() == 1) {
                $productCatArr = explode(',', $productDetails->row()->category_id);
                if (count($productCatArr) > 0) {
                    $productCatNameArr = array();
                    foreach ($productCatArr as $productCatID) {
                        if ($productCatID != '') {
                            $productCatDetails = $this->mobile_model->get_all_details(CATEGORY, array('id' => $productCatID));
                            if ($productCatDetails->num_rows() == 1) {
                                if ($count == 1) {
                                    $firstCatName = $productCatDetails->row()->cat_name;
                                }
                                $listConditionArr = array('name' => $productCatDetails->row()->cat_name, 'user_id' => $userId);
                                $listCheck = $this->mobile_model->get_all_details(LISTS_DETAILS, $listConditionArr);
                                if ($count == 1) {
                                    $firstCatDetails = $listCheck;
                                }
                                if ($listCheck->num_rows() == 0) {
                                    $this->mobile_model->simple_insert(LISTS_DETAILS, $listConditionArr);
                                    $userDetails = $this->mobile_model->get_all_details(USERS, array('id' => $userId));
                                    $listCount = $userDetails->row()->lists;
                                    if ($listCount < 0 || $listCount == '') {
                                        $listCount = 0;
                                    }
                                    $listCount++;
                                    $this->mobile_model->update_details(USERS, array('lists' => $listCount), array('id' => $userId));
                                }
                                $count++;
                            }
                        }
                    }
                }
            }
	            //Check the product id in list table
            $checkListsArr = $this->mobile_model->get_list_details($tid, $userId);
            if ($checkListsArr->num_rows() == 0) {
                //Add the product id under the first category name
                if ($firstCatName != '') {
                    $listConditionArr = array('name' => $firstCatName, 'user_id' => $userId);
                    if ($firstCatDetails == '' || $firstCatDetails->num_rows() == 0) {
                        $dataArr = array('product_id' => $tid);
                    } else {
                        $productRowArr = explode(',', $firstCatDetails->row()->product_id);
                        $productRowArr[] = $tid;
                        $newProductRowArr = implode(',', $productRowArr);
                        $dataArr = array('product_id' => $newProductRowArr);
                    }
                    $this->mobile_model->update_details(LISTS_DETAILS, $dataArr, $listConditionArr);

                    $listCntDetails = $this->mobile_model->get_all_details(LISTS_DETAILS, $listConditionArr);
                    if ($listCntDetails->num_rows() == 1) {
                        array_push($uniqueListNames, $listCntDetails->row()->id);
                        $ListCount[] = array(
                        								'ListId'  =>  $listCntDetails->row()->id,
                        								'Checked'  =>  'Checked',
                        								'ListName'  =>  $listCntDetails->row()->name
                        								);
                        $returnArr['listCnt'] =  $ListCount ;

                    }
                }
            } else {
                foreach ($checkListsArr->result() as $checkListsRow) {
                    array_push($uniqueListNames, $checkListsRow->id);
                        $ListCount[] = array(
                        								'ListId'  =>  $checkListsRow->id,
                        								'Checked'  =>  'Checked',
                        								'ListName'  =>  $checkListsRow->name
                        								);
                        $returnArr['listCnt'] =  $ListCount ;
                }
            }

            $all_lists = $this->mobile_model->get_all_details(LISTS_DETAILS, array('user_id' => $userId));
            if ($all_lists->num_rows() > 0) {
                foreach ($all_lists->result() as $all_lists_row) {
                    if (!in_array($all_lists_row->id, $uniqueListNames)) {
                        $ListCount[] = array(
                        								'ListId'  =>  $all_lists_row->id,
                        								'Checked'  =>  '',
                        								'ListName'  =>  $all_lists_row->name
                        								);
                        $returnArr['listCnt'] =  $ListCount ;


                    }
                }
            }

	        //Check the product wanted status
            $wantedProducts = $this->mobile_model->get_all_details(WANTS_DETAILS, array('user_id' => $userId));
            if ($wantedProducts->num_rows() == 1) {
                $wantedProductsArr = explode(',', $wantedProducts->row()->product_id);
                if (in_array($tid, $wantedProductsArr)) {
                    $returnArr['wanted'] = 1;
                }
            }
            $returnArr['status_code'] = 1;
	        echo json_encode($returnArr);
	}

	/*
	* !! >>>>>>>>>>>>> Product Adding To List Function  <<<<<<<<<<<< !!
	*/
	public function add_item_to_lists() {
		$returnArr['status_code'] = 0;
		$tid  =  $_GET['tid'];
		$lid  =  $_GET['listId'];
		$listDetails = $this->mobile_model->get_all_details(LISTS_DETAILS, array('id' => $lid));
		if ($listDetails->num_rows() == 1) {
			$product_ids = explode(',', $listDetails->row()->product_id);
			if (!in_array($tid, $product_ids)) {
				array_push($product_ids, $tid);
			}
			$new_product_ids = implode(',', $product_ids);
			$this->mobile_model->update_details(LISTS_DETAILS, array('product_id' => $new_product_ids), array('id' => $lid));
			$returnArr['status_code'] = 1;
		}
		echo json_encode($returnArr);
	}

	/*
	* !! >>>>>>>>>>>>> Product Removing From List Function  <<<<<<<<<<<< !!
	*/
	public function remove_item_from_lists() {
		$returnArr['status_code'] = 0;
		$tid  =  $_GET['tid'];
		$lid  =  $_GET['listId'];
		$listDetails = $this->mobile_model->get_all_details(LISTS_DETAILS, array('id' => $lid));
		if ($listDetails->num_rows() == 1) {
			$product_ids = explode(',', $listDetails->row()->product_id);
			if (in_array($tid, $product_ids)) {
				if (($key = array_search($tid, $product_ids)) !== false) {
					unset($product_ids[$key]);
				}
			}
			$new_product_ids = implode(',', $product_ids);
			$this->mobile_model->update_details(LISTS_DETAILS, array('product_id' => $new_product_ids), array('id' => $lid));
			$returnArr['status_code'] = 1;
		}
		echo json_encode($returnArr);
	}

	/*
	* !! >>>>>>>>>>>>> Add Product To Want List <<<<<<<<<<<< !!
	*/
	public function add_want_tag() {
        $returnArr['status_code'] = 0;
		$tid  =  $_GET['tid'];
		$userId  =  $_GET['UserId'];
		$this->data['userProfileDetails'] = $this->mobile_model->get_all_details(USERS, array('id' => $userId, 'status' => 'Active'));
        $wantDetails = $this->mobile_model->get_all_details(WANTS_DETAILS, array('user_id' => $userId));
        if ($wantDetails->num_rows() == 1) {
            $product_ids = explode(',', $wantDetails->row()->product_id);
            if (!in_array($tid, $product_ids)) {
                array_push($product_ids, $tid);
            }
            $new_product_ids = implode(',', $product_ids);
            $this->mobile_model->update_details(WANTS_DETAILS, array('product_id' => $new_product_ids), array('user_id' => $userId));
        }else {
            $dataArr = array('user_id' => $userId, 'product_id' => $tid);
            $this->mobile_model->simple_insert(WANTS_DETAILS, $dataArr);
        }
        $wantCount = $this->data['userProfileDetails']->row()->want_count;
        if ($wantCount <= 0 || $wantCount == '') {
            $wantCount = 0;
        }
        $wantCount++;
        $dataArr = array('want_count' => $wantCount);
        $ownProducts = explode(',', $this->data['userProfileDetails']->row()->own_products);
        if (in_array($tid, $ownProducts)) {
            if (($key = array_search($tid, $ownProducts)) !== false) {
                unset($ownProducts[$key]);
            }
            $ownCount = $this->data['userProfileDetails']->row()->own_count;
            $ownCount--;
            $dataArr['own_count'] = $ownCount;
            $dataArr['own_products'] = implode(',', $ownProducts);
        }
        $this->mobile_model->update_details(USERS, $dataArr, array('id' => $userId));
        $returnArr['status_code'] = 1;
        echo json_encode($returnArr);
    }


	/*
	* !! >>>>>>>>>>>>> Create New Product List <<<<<<<<<<<< !!
	*/

	public function create_list() {
        $returnArr['status_code'] = 0;
		$tid = $_GET['tid'];
        $list_name = $_GET['listName'];
		$category_id = $_GET['categoryId'];
		$userId  =  $_GET['UserId'];
        $checkList = $this->mobile_model->get_all_details(LISTS_DETAILS, array('name' => $list_name, 'user_id' => $userId));
        if ($checkList->num_rows() == 0) {
            $dataArr = array('user_id' => $userId, 'name' => $list_name, 'product_id' => $tid);
            if ($category_id != '') {
                $dataArr['category_id'] = $category_id;
            }
            $this->mobile_model->simple_insert(LISTS_DETAILS, $dataArr);
			$lastInsertId = $this->mobile_model->get_last_insert_id();
            $userDetails = $this->mobile_model->get_all_details(USERS, array('id' => $userId));
            $listCount = $userDetails->row()->lists;
            if ($listCount < 0 || $listCount == '') {
                $listCount = 0;
            }
            $listCount++;
            $this->mobile_model->update_details(USERS, array('lists' => $listCount), array('id' => $userId));
            $returnArr['list_id'] =  $this->mobile_model->get_last_insert_id();
			$returnArr['new_list'] = 1;
			$ListDetails = $this->mobile_model->get_all_details(LISTS_DETAILS, array('id' => $lastInsertId, 'user_id' => $userId));
			$ListCount[] = array(
											'ListId'  =>  $ListDetails->row()->id,
											'Checked'  =>  'Checked',
											'ListName'  =>  $ListDetails->row()->name
											);
			$returnArr['ListDetails'] =  $ListCount ;
        } else {
            $productArr = explode(',', $checkList->row()->product_id);
            if (!in_array($tid, $productArr)) {
                array_push($productArr, $tid);
            }
            $product_id = implode(',', $productArr);
            $dataArr = array('product_id' => $product_id);
            if ($category_id != '') {
                $dataArr['category_id'] = $category_id;
            }
            $this->mobile_model->update_details(LISTS_DETAILS, $dataArr, array('user_id' =>$userId, 'name' => $list_name));
            $returnArr['list_id'] = $checkList->row()->id;
            $returnArr['new_list'] = 0;
			$ListDetails = $this->mobile_model->get_all_details(LISTS_DETAILS, array('id' => $checkList->row()->id, 'user_id' => $userId));
			$ListCount[] = array(
											'ListId'  =>  $ListDetails->row()->id,
											'Checked'  =>  'Checked',
											'ListName'  =>  $ListDetails->row()->name
											);
			$returnArr['ListDetails'] =  $ListCount ;
        }
        $returnArr['status_code'] = 1;
        echo json_encode($returnArr);
    }


	/*
	* !! >>>>>>>>>>>>> Remove Product From Want List <<<<<<<<<<<< !!
	*/
	public function delete_want_tag() {
		$returnArr['status_code'] = 0;
		$tid  =  $_GET['tid'];
		$userId  =  $_GET['UserId'];
		$wantDetails = $this->mobile_model->get_all_details(WANTS_DETAILS, array('user_id' => $userId));
		$this->data['userProfileDetails'] = $this->mobile_model->get_all_details(USERS, array('id' => $userId, 'status' => 'Active'));
		if ($wantDetails->num_rows() == 1) {
			$product_ids = explode(',', $wantDetails->row()->product_id);
			if (in_array($tid, $product_ids)) {
				if (($key = array_search($tid, $product_ids)) !== false) {
					unset($product_ids[$key]);
				}
			}
			$new_product_ids = implode(',', $product_ids);
			$this->mobile_model->update_details(WANTS_DETAILS, array('product_id' => $new_product_ids), array('user_id' => $userId));
			$wantCount = $this->data['userProfileDetails']->row()->want_count;
			if ($wantCount <= 0 || $wantCount == '') {
				$wantCount = 1;
			}
			$wantCount--;
			$this->mobile_model->update_details(USERS, array('want_count' => $wantCount), array('id' => $userId));
			$returnArr['status_code'] = 1;
		}
		echo json_encode($returnArr);
	}


	/*
	* !! >>>>>>>>>>>>> Add Product to Want List <<<<<<<<<<<< !!
	*/
	public function add_have_tag() {
		$returnArr['status_code'] = 0;
		$tid  =  $_GET['tid'];
		$uid  =  $_GET['UserId'];
		$this->data['userProfileDetails'] = $this->mobile_model->get_all_details(USERS, array('id' => $uid, 'status' => 'Active'));
		if ($uid != '') {
			$ownArr = explode(',', $this->data['userProfileDetails']->row()->own_products);
			$ownCount = $this->data['userProfileDetails']->row()->own_count;
			if (!in_array($tid, $ownArr)) {
				array_push($ownArr, $tid);
				$ownCount++;
				$dataArr = array('own_products' => implode(',', $ownArr), 'own_count' => $ownCount);
				$wantProducts = $this->mobile_model->get_all_details(WANTS_DETAILS, array('user_id' => $uid));
				if ($wantProducts->num_rows() == 1) {
					$wantProductsArr = explode(',', $wantProducts->row()->product_id);
					if (in_array($tid, $wantProductsArr)) {
						if (($key = array_search($tid, $wantProductsArr)) !== false) {
							unset($wantProductsArr[$key]);
						}
						$wantsCount = $this->data['userProfileDetails']->row()->want_count;
						$wantsCount--;
						$dataArr['want_count'] = $wantsCount;
						$this->mobile_model->update_details(WANTS_DETAILS, array('product_id' => implode(',', $wantProductsArr)), array('user_id' => $uid));
					}
				}
				$this->mobile_model->update_details(USERS, $dataArr, array('id' => $uid));
				$returnArr['status_code'] = 1;
			}
		}
		echo json_encode($returnArr);
	}


	/*
	* !! >>>>>>>>>>>>> Remove Product From Want List <<<<<<<<<<<< !!
	*/

	public function delete_have_tag() {
		$returnArr['status_code'] = 0;
		$tid  =  $_GET['tid'];
		$uid  =  $_GET['UserId'];
		$this->data['userProfileDetails'] = $this->mobile_model->get_all_details(USERS, array('id' => $uid, 'status' => 'Active'));
		if ($uid != '') {
			$ownArr = explode(',', $this->data['userProfileDetails']->row()->own_products);
			$ownCount = $this->data['userProfileDetails']->row()->own_count;
			if (in_array($tid, $ownArr)) {
				if ($key = array_search($tid, $ownArr) !== false) {
					unset($ownArr[$key]);
					$ownCount--;
				}
				$this->mobile_model->update_details(USERS, array('own_products' => implode(',', $ownArr), 'own_count' => $ownCount), array('id' => $uid));
				$returnArr['status_code'] = 1;
			}
		}
		echo json_encode($returnArr);
	}


	/*
	* !! >>>>>>>>>>>>> List Details For Editing <<<<<<<<<<<< !!
	*/
	public function edit_user_lists() {
		$lid  =  $_GET['lid'];
		$uname  =  $_GET['uname'];
        $user  = $this->mobile_model->get_all_details(USERS, array('user_name' => $uname));
		$userThumbnail = base_url().'images/users/user-thumb1.png';
		if($user->row()->thumbnail !='') $userThumbnail = base_url().'images/users/'.$user->row()->thumbnail;
		$userProfileDetails[] = array(
													'id'=> $user->row()->id,
													'userName'=> $user->row()->user_name,
													'fullName'=> $user->row()->full_name,
													'email'=> $user->row()->email,
													'userThumbnail'=> $userThumbnail,
													'group'=> $user->row()->group
												);
		$list_details = $this->mobile_model->get_all_details(LISTS_DETAILS, array('id' => $lid, 'user_id' => $user->row()->id));
		$listDetails[] =array(
									'id'  =>  $list_details->row()->id,
									'name'  =>  $list_details->row()->name,
									'userId'  =>  $list_details->row()->user_id,
									'productId'  =>  $list_details->row()->product_id,
									//	'followers'  =>  $list_details->row()->followers,
									//	'banner'  =>  $list_details->row()->banner,
									'categoryId'  =>  $list_details->row()->category_id,
									'contributors'  =>  $list_details->row()->contributors,
									'contributorsInvited'  =>  $list_details->row()->contributors_invited,
									'productCount'  =>  $list_details->row()->product_count,
									'followersCount'  =>  $list_details->row()->followers_count
								);

		$list_category_details  = $this->mobile_model->get_all_details(CATEGORY, array('id' => $list_details->row()->category_id));
		$listCategoryDetails = array();
		if($list_category_details->num_rows() >0){
			$listCategoryDetails[] =array(
										'id'  =>  $list_category_details->row()->id,
										'categoryName'  =>  $list_category_details->row()->cat_name,
										'rootID'  =>  $list_category_details->row()->rootID,
										'seourl'  =>  $list_category_details->row()->seourl,
										'status'  =>  $list_category_details->row()->status
										//	'image'  =>  $list_category_details->row()->image,
										//'catPosition'  =>  $list_category_details->row()->cat_position,
										//	'seo_title'  =>  $list_category_details->row()->seo_title,
										//	'seo_keyword'  =>  $list_category_details->row()->seo_keyword,
										//	'seo_description'  =>  $list_category_details->row()->seo_description,
										//	'dateAdded'  =>  $list_category_details->row()->dateAdded
									);
		}
		foreach($_SESSION['sMainCategories']->result() as $MainCategories){
			$categoryList[]  =  array(
													'id'  =>  $MainCategories->id,
													'categoryName'  =>  $MainCategories->cat_name,
													'rootID'  =>  $MainCategories->rootID,
													'seourl'  =>  $MainCategories->seourl,
													'status'  =>  $MainCategories->status
													//'image'  =>  $_SESSION['sMainCategories']->row()->image,
													//'catPosition'  =>  $list_category_details->row()->cat_position,
													//	'seo_title'  =>  $list_category_details->row()->seo_title,
													//	'seo_keyword'  =>  $list_category_details->row()->seo_keyword,
													//	'seo_description'  =>  $list_category_details->row()->seo_description,
													//	'dateAdded'  =>  $list_category_details->row()->dateAdded
										);

		}
		$returnArr['userProfileDetails'] = $userProfileDetails;
		$returnArr['listDetails'] = $listDetails;
		$returnArr['listCategoryDetails'] = $listCategoryDetails;
		$returnArr['categoryList'] = $categoryList;
		echo json_encode($returnArr);
    }

	/*
	* !! >>>>>>>>>>>>> Edited User List Save Function  <<<<<<<<<<<< !!
	*/

	public function edit_user_list_details() {
		$returnArr['status_code'] = 0;
		$lid = $_POST['listId'];
		$uid = $_POST['userId'];
		$list_title = $_POST['titleName'];
		$catID = $_POST['categoryId'];
		$duplicateCheck = $this->mobile_model->get_all_details(LISTS_DETAILS, array('user_id' => $uid, 'id !=' => $lid, 'name' => $list_title));
		if ($duplicateCheck->num_rows() > 0) {
			$returnArr['status_code'] = 0;
		}else {
			if ($catID == '') {
				$catID = 0;
			}
			$this->mobile_model->update_details(LISTS_DETAILS, array('name' => $list_title, 'category_id' => $catID), array('id' => $lid, 'user_id' => $uid));
			$returnArr['status_code'] = 1;
		}
		echo json_encode($returnArr);
	}

	/*
	* !! >>>>>>>>>>>>> Delete User List  <<<<<<<<<<<< !!
	*/
	public function delete_user_list(){
		$returnArr['status_code'] = 0;
		$lid = $_POST['listId'];
		$uid = $_POST['userId'];
		$this->data['userProfileDetails'] = $this->mobile_model->get_all_details(USERS, array('id' => $uid, 'status' => 'Active'));
		$list_details = $this->mobile_model->get_all_details(LISTS_DETAILS, array('id' => $lid, 'user_id' => $uid));
		if ($list_details->num_rows() == 1) {
			$followers_id = $list_details->row()->followers;
			if ($followers_id != '') {
				$searchArr = array_filter(explode(',', $followers_id));
				$fieldsArr = array('following_user_lists', 'id');
				$followersArr = $this->mobile_model->get_fields_from_many(USERS, $fieldsArr, 'id', $searchArr);
				if ($followersArr->num_rows() > 0) {
					foreach ($followersArr->result() as $followersRow) {
						$listArr = array_filter(explode(',', $followersRow->following_user_lists));
						if (in_array($lid, $listArr)) {
							if (($key = array_search($lid, $listArr)) != false) {
								unset($listArr[$key]);
								$this->mobile_model->update_details(USERS, array('following_user_lists' => implode(',', $listArr)), array('id' => $followersRow->id));
							}
						}
					}
				}
			}
			$this->mobile_model->commonDelete(LISTS_DETAILS, array('id' => $lid, 'user_id' => $uid));
			$listCount = $this->data['userProfileDetails']->row()->lists;
			$listCount--;
			if ($listCount == '' || $listCount < 0) {
				$listCount = 0;
			}
			$this->mobile_model->update_details(USERS, array('lists' => $listCount), array('id' => $uid));
		//	$returnArr['url'] = base_url() . 'user/' . $this->data['userProfileDetails']->row()->user_name . '/lists';
			$returnArr['status_code'] = 1;
		}else {
			if ($this->lang->line('lst_not_avail') != '')
				$returnArr['message'] = $this->lang->line('lst_not_avail');
			else
				$returnArr['message'] = 'List not available';
		}
		echo json_encode($returnArr);
	}

	/*
	* !! >>>>>>>>>>>>> Shop Page  <<<<<<<<<<<< !!
	*/
	public function shopPage(){
		$this->data['bannerList'] = $this->mobile_model->get_all_details(BANNER_CATEGORY,array('status'=>'Publish'));
		$this->data['recentProducts'] = $this->mobile_model->view_product_details(" where p.status='Publish' and p.quantity > 0 and u.group='Seller' and u.status='Active' or p.status='Publish' and p.quantity > 0 and p.user_id=0 order by p.created desc limit 20");
		foreach($this->data['bannerList']->result() as $bannerlist)
		{
			$bannerlistArr[]=array('id'=>$bannerlist->id,
									'name'=>$bannerlist->name,
									'image'=>base_url().'images/category/banner/'
									.$bannerlist->image,
									'link'=>$bannerlist->link

			);
		}
		foreach($this->data['recentProducts']->result() as $recentprodList){
			$imgArr=@explode(',',$recentprodList->image);
			$images=array();
			foreach($imgArr as $img){
			if($img!="")
			$images[]=base_url().'images/product/'.$img;break;
			}
			$Username = "";
			if($recentprodList->user_name != "") $Username= $recentprodList->user_name;
			$fullname = "";
			if($recentprodList->full_name != "") $fullname = $recentprodList->full_name;
			$recentProductsArr[]=array('id'=>$recentprodList->id,
															'seller_product_id'=>$recentprodList->seller_product_id,
															'product_name'=>$recentprodList->product_name,
															'seourl'=>$recentprodList->seourl,
															'description'=>strip_tags($recentprodList->description),
															'shippingPolices'=>strip_tags($recentprodList->shipping_policies),
															'image'=>$images,
															'price'=>$recentprodList->price,
															'sale_price'=>$recentprodList->sale_price,
															'user_name'=> $Username,
															'full_name'=> $fullname,
															'type' => 'SellerProduct',
															'user_id'=>$recentprodList->user_id,
															'likes'=>$recentprodList->likes,
															'url'  =>   base_url().'json/product?pid='.$recentprodList->id
														);
		}

		$sortArr1 = array('field'=>'cat_position','type'=>'asc');
		$sortArr = array($sortArr1);
		$sMainCategories = $this->product_model->get_all_details(CATEGORY,array('rootID'=>'0','status'=>'Active'),$sortArr);
		foreach($sMainCategories->result() as $MainCategories){
			$categoryImage = base_url().'images/category/dummy.jpg';
			if($MainCategories->image != ""){
				$categoryImage = base_url().'images/category/'.$MainCategories->image;
			}
			$categoryList[]  =  array(
													'id'  =>  $MainCategories->id,
													'categoryName'  =>  $MainCategories->cat_name,
													'rootID'  =>  $MainCategories->rootID,
													'seourl'  =>  $MainCategories->seourl,
													'status'  =>  $MainCategories->status,
													'image'  =>  $categoryImage,
													'url'      =>   base_url().'json/shopby/'.$MainCategories->seourl
													//'catPosition'  =>  $list_category_details->row()->cat_position,
													//	'seo_title'  =>  $list_category_details->row()->seo_title,
													//	'seo_keyword'  =>  $list_category_details->row()->seo_keyword,
													//	'seo_description'  =>  $list_category_details->row()->seo_description,
													//	'dateAdded'  =>  $list_category_details->row()->dateAdded
										);

		}

		$countryList= $this->mobile_model->get_all_details(COUNTRY_LIST, array(), array(array('field' => 'name', 'type' => 'asc')));
		$countryLists = array();
		if($countryList->num_rows() > 0){
			foreach($countryList->result() as $country){
					$countryLists[] = array(
														'categoryName' => $country->name,
														'id' => $country->country_code
												);
			}
		}
		$returnArr['CountryLists'] = $countryLists;
		$returnArr['categoryList'] = $categoryList;
		$returnArr['bannerList']=$bannerlistArr;
		$returnArr['recentProducts']=$recentProductsArr;
		echo json_encode($returnArr);
	}

	/*
	* !! >>>>>>>>>>>>> Forgot Password  <<<<<<<<<<<< !!
	*/
	public function forgot_password_user() {
		$returnArr['status']  =  '0';
		$returnArr['response']  =  '';
		$email = $_POST['email'];
		if (valid_email($email)) {
			$condition = array('email' => $email);
			$checkUser = $this->mobile_model->get_all_details(USERS, $condition);
			if ($checkUser->num_rows() == '1') {
				$pwd = $this->get_rand_str('6');
				$newdata = array('password' => md5($pwd));
				$condition = array('email' => $email);
				$this->mobile_model->update_details(USERS, $newdata, $condition);
				$this->send_user_password($pwd, $checkUser);
				if ($this->lang->line('pwd_sen_mail') != ''){
					$lg_err_msg = $this->lang->line('pwd_sen_mail');
				}else{
					$lg_err_msg = 'New password sent to your mail';
				}
				$returnArr['response']  =  $lg_err_msg;
				$returnArr['status']  =  '1';
			}else {
				if ($this->lang->line('mail_not_record') != ''){
					$lg_err_msg = $this->lang->line('mail_not_record');
				}else{
					$lg_err_msg = 'Your email id not matched in our records';
				}
				$returnArr['response']  =  $lg_err_msg;
			}
		}else {
			if ($this->lang->line('mail_not_valid') != ''){
				$lg_err_msg = $this->lang->line('mail_not_valid');
			}else{
				$lg_err_msg = 'Email id not valid';
			}
			$returnArr['response']  =  $lg_err_msg;
		}
		echo json_encode($returnArr);
	}

	/*
	* !! >>>>>>>>>>>>> This Function to Upload User Product Picture    <<<<<<<<<<<< !!
	*/
	public function UploadAffProductImage(){
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		if (!empty($_POST['AffProductImage'])) {
			$imgPath ='images/temp/';
			$img = $_POST['AffProductImage'];
			$imgName = time(). ".jpg";
			$imageFormat = array('data:image/jpeg;base64', 'data:image/png;base64', 'data:image/jpg;base64', 'data:image/gif;base64');
			$img = str_replace($imageFormat, '', $img);
			$data = base64_decode($img);
			$image = @imagecreatefromstring($data);
			if ($image !== false) {
				$uploadPath = $imgPath . $imgName;
				imagejpeg($image, $uploadPath, 100);
				imagedestroy($image);
			} else {
				$message = "An error occurred";
				if($this->lang->line('json_error_occured') != ""){
				    $message = stripslashes($this->lang->line('json_error_occured'));
				}
				$returnArr['response'] = $message;
			}
			if (isset($uploadPath)) {
				/* Creating Thumbnail image with the size of 100 X 100 */
				$filename = base_url($uploadPath);
				list($width, $height) = getimagesize($filename);
				$newwidth = 100;
				$newheight = 100;
				//$thumbImage = @imagecreatetruecolor($newwidth, $newheight);
				$sourceImage = @imagecreatefromjpeg($filename);
				imagecopyresized($thumbImage, $sourceImage, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
				// if ($thumbImage !== false) {
				// 	/* Not uploading to thumb folder */
				// 	$thumbImgPath = 'images/users/thumb/';
				// 	$thumpUploadPath = $thumbImgPath . $imgName;
				// 	imagejpeg($thumbImage, $thumpUploadPath, 100);
				// }
			} else {
				$message = "Invalid File";
							if($this->lang->line('json_invalid_file') != ""){
							    $message = stripslashes($this->lang->line('json_invalid_file'));
							}
							$returnArr['response'] = $message;
			}
		$returnArr['status'] = '1';
		$returnArr['response'] = $imgName;
		}else{
			$message = "Please Upload Image";
			if($this->lang->line('json_upload_image') != ""){
			    $message = stripslashes($this->lang->line('json_upload_image'));
			}
			$returnArr['response'] = $message;
		}
		$json_encode = json_encode($returnArr, JSON_PRETTY_PRINT);
		echo $json_encode;
	}

	/*
	* !! >>>>>>>>>>>>> This Function to Upload User Products    <<<<<<<<<<<< !!
	*/
	public function UploadAffProduct() {
        $returnArr['status_code'] = 0;
        $returnArr['response'] = '';
		$UserId = $_POST['UserId'];
        if ($UserId != '') {
		$this->data['userProfileDetails'] = $this->mobile_model->get_all_details(USERS, array('id' => $UserId, 'status' => 'Active'));
		$short_url = $this->get_rand_str('6');
		$checkId = $this->mobile_model->get_all_details(SHORTURL, array('short_url' => $short_url));
		while ($checkId->num_rows() > 0) {
			$short_url = $this->get_rand_str('6');
			$checkId = $this->mobile_model->get_all_details(SHORTURL, array('short_url' => $short_url));
		}
		$result = $this->mobile_model->add_user_product($UserId, $short_url);
		$product_id = $this->db->insert_id();
		$user_details = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
		if ($user_details->row()->group == "Seller") {
			$user_product = $this->mobile_model->get_all_details(USER_PRODUCTS, array('id' => $product_id));
			$dataArr = array('activity_name' => "add_product",
						'activity_id' => $user_product->row()->seller_product_id,
						'user_id' => $UserId
						);
			$this->mobile_model->simple_insert(USER_ACTIVITY, $dataArr);
		}
		if ($result['image'] != ''){
			$new_name = $result['image'];
			@copy("./images/temp/".$result['image'], './images/product/'.$new_name);
			@copy("./images/temp/".$result['image'], './images/product/thumb/'.$new_name);

			$this->imageResizeWithSpace(200, 200, $new_name, './images/product/thumb/');
			$this->imageResizeWithSpace(600, 600, $new_name, './images/product/');
		}

		$dir = getcwd().DIRECTORY_SEPARATOR ."images".DIRECTORY_SEPARATOR ."temp".DIRECTORY_SEPARATOR ;
		$interval = strtotime('-24 hours');
		foreach (glob($dir."*.*") as $file){
			if (filemtime($file) <= $interval ){
				if(!is_dir($file) ){
					unlink($file);
				}
			}
		}

		$returnArr['status_code'] = 1;
		$userDetails = $this->data['userProfileDetails'];
		$total_added = $userDetails->row()->products;
		$total_added++;
		$this->mobile_model->update_details(USERS, array('products' => $total_added), array('id' => $UserId));
		$returnArr['ProductUrl'] = base_url() . 'json/userproduct?pid='.$result['pid'].'&uname='.$userDetails->row()->user_name.'&UserId='.$UserId;
		$returnArr['status_code'] = 1;
		$message = "Successfully Uploaded";
		if($this->lang->line('json_upload_success') != ""){
		    $message = stripslashes($this->lang->line('json_upload_success'));
		}
		$returnArr['response'] = $message;
        }
        echo json_encode($returnArr);
    }

	/*
	* !! >>>>>>>>>>>>> List Of Categories  <<<<<<<<<<<< !!
	*/
	public function CatForAffProduct(){
		foreach($_SESSION['sMainCategories']->result() as $MainCategories){
			// $categoryImage = base_url().'images/category/dummy.jpg';
			// if($MainCategories->image != ""){
			// 	$categoryImage = base_url().'images/category/'.$MainCategories->image;
			// }
			$categoryList[]  =  array(
							'id'  =>  $MainCategories->id,
							'categoryName'  =>  $MainCategories->cat_name,
							'rootID'  =>  $MainCategories->rootID,
							'seourl'  =>  $MainCategories->seourl,
							'status'  =>  $MainCategories->status
							//'image'  =>  $categoryImage
							//'catPosition'  =>  $list_category_details->row()->cat_position,
							//'seo_title'  =>  $list_category_details->row()->seo_title,
							//'seo_keyword'  =>  $list_category_details->row()->seo_keyword,
							//'seo_description'  =>  $list_category_details->row()->seo_description,
							//'dateAdded'  =>  $list_category_details->row()->dateAdded
							);

		}
		$returnArr['categoryList'] = $categoryList;
		echo json_encode($returnArr);
	}

	/*
	* !! >>>>>>>>>>>>> Seller Signup Form  <<<<<<<<<<<< !!
	*/
	public function sellerSignupForm() {
		$returnArr['status'] = '0';
        $returnArr['response'] = '';
		$UserId  =  $_POST['UserId'];
		if($UserId != ""){
			$countryLists = array();
			$this->data['userProfileDetails'] = $this->mobile_model->get_all_details(USERS, array('id' => $UserId, 'status' => 'Active'));
	        if ($this->data['userProfileDetails']->row()->is_verified == 'No') {
	            if ($this->lang->line('cfm_mail_fst') != ''){
	                $lg_err_msg = $this->lang->line('cfm_mail_fst');
				}else{
	                $lg_err_msg = 'Please confirm your email first';
				}
				$returnArr['status'] = '0';
			    $returnArr['response'] = $lg_err_msg;
	        }else {
				    if ($this->data['userProfileDetails']->row()->request_status == 'Not Requested'){
						$countryList= $this->mobile_model->get_all_details(COUNTRY_LIST, array(), array(array('field' => 'name', 'type' => 'asc')));
						foreach($countryList->result() as $country){
								$countryLists[] = array(
																	'categoryName' => $country->name,
																	'id' => $country->country_code
															);
						}
						$returnArr['status'] = '1';
						$returnArr['CountryLists'] = $countryLists;
						$message = "Success";
						if($this->lang->line('json_success') != ""){
							$message = stripslashes($this->lang->line('json_success'));
						}
						$returnArr['response'] = $message;
					}else if($this->data['userProfileDetails']->row()->request_status == 'Pending'){
						$lg_err_msg = 'Our team is evaluating your request. We will contact you shortly';
						$returnArr['status'] = '0';
						$returnArr['response'] = $lg_err_msg;
					}else if($this->data['userProfileDetails']->row()->request_status == 'Approved'){
						$lg_err_msg = 'Approved';
						$returnArr['status'] = '1';
						$returnArr['response'] = $lg_err_msg;
					}else if($this->data['userProfileDetails']->row()->request_status == 'Rejected'){
						$lg_err_msg = 'Your request has been rejected, Please contact Admin';
						$returnArr['status'] = '0';
						$returnArr['response'] = $lg_err_msg;
					}
	        }
		}else{
			$returnArr['status'] = '0';
	        $returnArr['response'] = 'Please Login';
		}
		echo json_encode($returnArr);
    }

	/*
	* !! >>>>>>>>>>>>> Seller Signup <<<<<<<<<<<< !!
	*/
	public function seller_signup() {
		$returnArr['status_code'] = 0;
		$returnArr['response'] = '';
		$UserId  =  $_POST['UserId'];
		if(!empty($UserId)){
			$this->data['userProfileDetails'] = $this->mobile_model->get_all_details(USERS, array('id' => $UserId, 'status' => 'Active'));
	        if ($this->data['userProfileDetails']->row()->is_verified == 'Yes') {
				$excludeArr = array('UserId');
				$dataArr = array('request_status' => 'Pending');
				$this->mobile_model->commonInsertUpdate(USERS, 'update', $excludeArr, $dataArr, array('id' => $UserId));
				$lg_err_msg = 'Welcome onboard ! Our team is evaluating your request. We will contact you shortly';
				$returnArr['status_code'] = 1;
				$returnArr['response'] = $lg_err_msg;
			}else{
				if ($this->lang->line('cfm_mail_fst') != '')
	                $lg_err_msg = $this->lang->line('cfm_mail_fst');
	            else
	                $lg_err_msg = 'Please confirm your email first';
					$returnArr['status_code'] = 0;
			        $returnArr['response'] = $lg_err_msg;
			}
		}else{
			$returnArr['status_code'] = 0;
	        $returnArr['response'] = 'Please Login';
		}
		echo json_encode($returnArr);
	}

	/*
	* !! >>>>>>>>>>>>> Follow User <<<<<<<<<<<< !!
	*/
	public function addFollow() {
        $returnArr['status_code'] = 0;
		$returnArr['response'] = '';
		$UserId  =  $_POST['UserId'];
        if ($UserId != ''){
            $follow_id = $_POST['FollowingUserId'];
			$this->data['userProfileDetails'] = $this->mobile_model->get_all_details(USERS, array('id' => $UserId, 'status' => 'Active'));
            $followingListArr = explode(',', $this->data['userProfileDetails']->row()->following);
            if (!in_array($follow_id, $followingListArr)) {
                $followingListArr[] = $follow_id;
                $newFollowingList = implode(',', $followingListArr);
                $followingCount = $this->data['userProfileDetails']->row()->following_count;
                $followingCount++;
                $dataArr = array('following' => $newFollowingList, 'following_count' => $followingCount);
                $condition = array('id' => $UserId);
                $this->mobile_model->update_details(USERS, $dataArr, $condition);
                $followUserDetails = $this->mobile_model->get_all_details(USERS, array('id' => $follow_id));
                if ($followUserDetails->num_rows() == 1) {
                    $followersListArr = explode(',', $followUserDetails->row()->followers);
                    if (!in_array($UserId, $followersListArr)) {
                        $followersListArr[] = $UserId;
                        $newFollowersList = implode(',', $followersListArr);
                        $followersCount = $followUserDetails->row()->followers_count;
                        $followersCount++;
                        $dataArr = array('followers' => $newFollowersList, 'followers_count' => $followersCount);
                        $condition = array('id' => $follow_id);
                        $this->mobile_model->update_details(USERS, $dataArr, $condition);
                    }

	                $actArr = array(
	                    'activity_name' => 'follow',
	                    'activity_id' => $follow_id,
	                    'user_id' => $UserId,
	                    'activity_ip' => $this->input->ip_address()
	                );
	                $this->mobile_model->simple_insert(USER_ACTIVITY, $actArr);
	                $datestring = "%Y-%m-%d %H:%i:%s";
	                $time = time();
	                $createdTime = mdate($datestring, $time);
	                $actArr = array(
	                    'activity' => 'follow',
	                    'activity_id' => $follow_id,
	                    'user_id' => $UserId,
	                    'activity_ip' => $this->input->ip_address(),
	                    'created' => $createdTime
	                );
				$this->mobile_model->simple_insert(NOTIFICATIONS, $actArr);
				//$this->send_noty_mail($followUserDetails->result_array());
				$returnArr['status_code'] = 1;
				$returnArr['response'] = 'Following';

				/* Sending push notification process starts */
				$deive_type = '';
					if($followUserDetails->row()->gcm_buyer_id!=''){
						$device_id = $followUserDetails->row()->gcm_buyer_id;
						$deive_type = 'ANDROID';
	                         if($this->data['userProfileDetails']->row()->full_name != ""){
	                              $name = $this->data['userProfileDetails']->row()->full_name;
	                         }else{
	                              $name = $this->data['userProfileDetails']->row()->user_name;
	                         }
						if($deive_type!=''){
							$action = 'following';
							$urlval = array($follow_id,$UserId,$this->data['userProfileDetails']->row()->user_name);
							$message = ucwords($name).' '.'following your profile';
							$regIds = array($device_id);
							$this->sendPushNotification($regIds, $message, $action, $deive_type,$urlval);
						}
					}

                }

            } else {
			$returnArr['status_code'] = 1;
			$returnArr['response'] = 'Following';
            }
        }else{
			$returnArr['status_code'] = 0;
			$returnArr['response'] = 'Please Login';
		}
        echo json_encode($returnArr);
    }


	/*
	* !! >>>>>>>>>>>>> UnFollow User <<<<<<<<<<<< !!
	*/
	public function deleteFollow() {
		$returnArr['status_code'] = 0;
		$returnArr['response'] = '';
			$UserId  =  $_POST['UserId'];
	        if ($UserId != '') {
	            $follow_id = $_POST['FollowingUserId'];
				$this->data['userProfileDetails'] = $this->mobile_model->get_all_details(USERS, array('id' => $UserId, 'status' => 'Active'));
				$followingListArr = explode(',', $this->data['userProfileDetails']->row()->following);
			if (in_array($follow_id, $followingListArr)) {
				if (($key = array_search($follow_id, $followingListArr)) !== false) {
					unset($followingListArr[$key]);
				}
				$newFollowingList = implode(',', $followingListArr);
				$followingCount = $this->data['userProfileDetails']->row()->following_count;
				$followingCount--;
				$dataArr = array('following' => $newFollowingList, 'following_count' => $followingCount);
				$condition = array('id' => $UserId);
				$this->mobile_model->update_details(USERS, $dataArr, $condition);
				$followUserDetails = $this->mobile_model->get_all_details(USERS, array('id' => $follow_id));
				if ($followUserDetails->num_rows() == 1) {
					$followersListArr = explode(',', $followUserDetails->row()->followers);
					if (in_array($UserId, $followersListArr)) {
						if (($key = array_search($UserId, $followersListArr)) !== false) {
							unset($followersListArr[$key]);
						}
						$newFollowersList = implode(',', $followersListArr);
						$followersCount = $followUserDetails->row()->followers_count;
						$followersCount--;
						$dataArr = array('followers' => $newFollowersList, 'followers_count' => $followersCount);
						$condition = array('id' => $follow_id);
						$this->mobile_model->update_details(USERS, $dataArr, $condition);
					}
				}
				$actArr = array(
					'activity_name' => 'unfollow',
					'activity_id' => $follow_id,
					'user_id' => $UserId,
					'activity_ip' => $this->input->ip_address()
				);
				$this->mobile_model->simple_insert(USER_ACTIVITY, $actArr);
				$returnArr['status_code'] = (string)1;
				$returnArr['response'] = 'Unfollow';
			} else {
				$returnArr['status_code'] = (string)1;
				$returnArr['response'] = 'Unfollow';
			}
		}
		echo json_encode($returnArr);
	}

	/*
	* !! >>>>>>>>>>>>> Search List <<<<<<<<<<<< !!
	*/

	public function Store(){
		$returnArr['status'] = (string)0;
		$returnArr['response'] = '';
		$UserId = $_GET['UserId'];
		$storeDetails = array();
		$followingListArr = array();
		if($UserId != ""){
			$userProfileDetails = $this->mobile_model->get_all_details(USERS, array('id' => $UserId, 'status' => 'Active'));
			$followingListArr = explode(',', $userProfileDetails->row()->following);
		}
		$this->load->model('seller_plan_model','seller_plan');
        /******Remove free sellers updated by admin*****/
        $now = date("Y-m-d h:i:s");
        $Query = "SELECT user_id FROM ".STORE_FRONT." WHERE expiresOn < '".$now."' AND expiresOn != ''";
        $expiredUsers = $this->seller_plan->ExecuteQuery($Query);
        if($expiredUsers->num_rows > 0){
            foreach($expiredUsers->result() as $expiredId){
                $expiredList[] = $expiredId->user_id;
            }
            $Query = "DELETE FROM ".STORE_FRONT." WHERE user_id IN (".implode(',',$expiredList).") ";
            $this->seller_plan->ExecuteQuery($Query);
            $Query = "DELETE FROM ".STORE_ARB." WHERE user_id IN (".implode(',',$expiredList).") AND payment_type = 'Free' ";
            $this->seller_plan->ExecuteQuery($Query);
            $Query = "UPDATE ".USERS." SET store_payment = 'Cancelled' WHERE id IN (".implode(',',$expiredList).") ";
            $this->seller_plan->ExecuteQuery($Query);
        }
        /******Remove free sellers updated by admin*****/

    	$featured_seller = $this->seller_plan->get_featured_sellers();
    	if($featured_seller->num_rows > 0){
    		$featured = $featured_seller->result();
    		$featuredId = array();
    		foreach ($featured as $seller) {
    			$featuredId[] = $seller->id;
    		}
    	}

		$featuredStoreDetails = array();
		if($featured_seller->num_rows() > 0){
			$condition = 'select u.*,s.store_name,s.logo_image,s.cover_image from '.USERS.' as u left join '.STORE_FRONT.' as s on s.user_id = u.id where u.group ="Seller" and u.store_payment ="Paid" and u.status = "Active" and FIND_IN_SET(u.id ,"'.implode(",", $featuredId).'") group by u.id';
            $featuredStoreLIst = $this->user_model->ExecuteQuery($condition);
			foreach($featuredStoreLIst->result() as $featuredStore){
				$storeBanner = base_url().'images/store/banner-dummy.jpg';
				$storeLogo = base_url().'images/store/dummy-logo.png';
				$storeTagline = "";
				$storeDescription = "";
				$storeName ="";
				$storeName = $featuredStore->store_name;
				if($featuredStore->cover_image !=""){
					$storeBanner = base_url().'images/store/'.$featuredStore->cover_image;
				}
				if($featuredStore->logo_image !=""){
					$storeLogo =  base_url().'images/store/'.$featuredStore->logo_image;
				}
				$storeTagline =$featuredStore->tagline;
				$storeDescription =$featuredStore->description;

				$userThumbnail = base_url().'images/users/user-thumb1.png';
				if($featuredStore->thumbnail !='') $userThumbnail = base_url().'images/users/'.$featuredStore->thumbnail;
				$following = 'No';
				if(in_array($featuredStore->id,$followingListArr)){$following = 'Yes';}
				if($storeName != ""){
					$cond = "select count(id) as productCount from ".PRODUCT." where user_id=".$featuredStore->id." and status='Publish'";
					$result = $this->mobile_model->ExecuteQuery($cond);
					$featuredStoreDetails[] = array(
												'UserId' => $featuredStore->id,
												'fullName' => $featuredStore->full_name,
												'userName' => $featuredStore->user_name,
												'UserImage' => $userThumbnail,
												'state' => $featuredStore->state,
												'country' => $featuredStore->country,
												'storeName' => trim($storeName),
												'storeBanner' => trim($storeBanner),
												'storeLogo' => $storeLogo,
												'storeDescription' =>strip_tags($storeDescription),
												'storeFollowing' => $following,
												'productCount' => $result->row()->productCount
												);
				}
			}
		}

        if($this->config->item('storefront_fees_month') == '0' && $this->config->item('storefront_fees_year') == '0'){
                $sellers_list = $this->user_model->get_all_details(USERS,array('group'=>'Seller','status'=>'Active'));
        }else{
            $condition = 'select u.*,s.store_name,s.logo_image,s.cover_image from '.USERS.' as u left join '.STORE_FRONT.' as s on s.user_id = u.id where u.group ="Seller" and u.store_payment ="Paid" and u.status = "Active" and NOT FIND_IN_SET(u.id ,"'.implode(",", $featuredId).'") group by u.id';
            $sellers_list = $sellers_list = $this->user_model->ExecuteQuery($condition);
        }

		if($sellers_list->num_rows() > 0){
			foreach($sellers_list->result() as $store){
				$storeBanner = base_url().'images/store/banner-dummy.jpg';
				$storeLogo = base_url().'images/store/dummy-logo.png';
				$storeTagline = "";
				$storeDescription = "";
				$storeName ="";
				$storeName = $store->store_name;
				if($store->cover_image !=""){
					$storeBanner = base_url().'images/store/'.$store->cover_image;
				}
				if($store->logo_image !=""){
					$storeLogo =  base_url().'images/store/'.$store->logo_image;
				}
				$storeTagline =$store->tagline;
				$storeDescription =$store->description;

				$userThumbnail = base_url().'images/users/user-thumb1.png';
				if($store->thumbnail !='') $userThumbnail = base_url().'images/users/'.$store->thumbnail;
				$following = 'No';
				if(in_array($store->id,$followingListArr)){$following = 'Yes';}
				if($storeName != ""){
					$cond = "select count(id) as productCount from ".PRODUCT." where user_id=".$store->id." and status='Publish'";
					$result = $this->mobile_model->ExecuteQuery($cond);
					$storeDetails[] = array(
												'UserId' => $store->id,
												'fullName' => $store->full_name,
												'userName' => $store->user_name,
												'UserImage' => $userThumbnail,
												'state' => $store->state,
												'country' => $store->country,
												'storeName' => trim($storeName),
												'storeBanner' => trim($storeBanner),
												'storeLogo' => $storeLogo,
												'storeDescription' =>strip_tags($storeDescription),
												'storeFollowing' => $following,
												'productCount' => $result->row()->productCount
												);
				}
			}
		}
		$returnArr['status'] = (string)1;
		$message = "Success";
		if($this->lang->line('json_success') != ""){
			$message = stripslashes($this->lang->line('json_success'));
		}
		$returnArr['response'] = $message;
		$returnArr['featuredStoreDetails'] = $featuredStoreDetails;
		$returnArr['storeList'] = $storeDetails;
		echo json_encode($returnArr);
	}

	/*
	* !! >>>>>>>>>>>>> Search Suggestions <<<<<<<<<<<< !!
	*/
	public function searchSuggestions(){
		$returnArr['status_code'] = 0;
		$returnArr['response'] = '';
		$search_key = $_GET['q'];
		$type = $_GET['type'];
		$UserId = $_GET['UserId'];
		$followingListArr = array();
		if($UserId != ""){
			$userProfileDetails = $this->mobile_model->get_all_details(USERS, array('id' => $UserId, 'status' => 'Active'));
			$followingListArr = explode(',', $userProfileDetails->row()->following);
		}

		if($type == '0'){
			$type= '';
		}
		$count = $_GET['pg'];
		if($search_key !=''){
			if ($search_key != '' && $type==''){
			$limit=10;
			$offset=0;
			$sellingProductDetails = $this->mobile_model->get_products_search_results($search_key,$limit,$offset);
			$affiliateProductDetails = $this->mobile_model->get_user_products_search_results($search_key,$limit,$offset);
			$productDetails =  $this->data['productDetails'] = $this->mobile_model->get_sorted_array_mobile($sellingProductDetails, $affiliateProductDetails, 'created', 'desc');
			$prodArr = array();
			if(count($productDetails) > 0){
				foreach($productDetails as $prodRow){
					$likedStatus = "No";
					$url = "";
					if($UserId != ""){
						$cond = "select user_id from ".PRODUCT_LIKES." where product_id=".$prodRow['seller_product_id']." and user_id=".$UserId;
						$likedUsersList = $this->mobile_model->ExecuteQuery($cond);
						if($likedUsersList->num_rows() > 0){
							$likedStatus = "Yes";
						}
						$url = $prodRow['url'].'&UserId='.$UserId;
					}



					$imgArr=@explode(',',$prodRow['image']);
					$images=array();
					foreach($imgArr as $img){
					if($img!="")
						$images[]=base_url().'images/product/'.$img;
					}
					$prodArr[]=array(
												'id'=>$prodRow['id'],
												'seller_product_id'=>$prodRow['seller_product_id'],
												'product_name'=>$prodRow['product_name'],
												'seourl'=>$prodRow['seourl'],
												'description'=>strip_tags($prodRow['description']),
												'shippingPolicies'=>strip_tags($prodRow['shipping_policies']),
												'image'=>$images,
												'price'=>$prodRow['price'],
												'sale_price'=>$prodRow['sale_price'],
												'user_name'=>$prodRow['user_name'],
												'likes'=>$prodRow['likes'],
												'liked' => $likedStatus,
												'url'=>$url,
												'type' => $prodRow['type'],
												'userUrl' => 	$normalUserUrl = 'http://192.168.1.251:8081/product-working/fancyclone-v3/json/user/seller-timeline?username='.$prodRow['user_name']
												);
				}
			}
			$returnArr['productDetails']=$prodArr;
			$usersCount=20;
			$offset=0;
			$userDetails = $this->mobile_model->get_user_search_results($search_key,$usersCount,$offset);
			$userProfileDetails = array();
			if ($userDetails->num_rows()>0){
				foreach($userDetails->result() as $user){
					$userThumbnail = base_url().'images/users/user-thumb1.png';
					if($user->thumbnail !='') $userThumbnail = base_url().'images/users/'.$user->thumbnail;
					$following = 'No';
					if(in_array($user->id,$followingListArr)){$following = 'Yes';}
					$userProfileDetails[] = array(
																'id'=> $user->id,
																'userName'=> $user->user_name,
																'fullName'=> $user->full_name,
																'email'=> $user->email,
																'userThumbnail'=> $userThumbnail,
																'group'=> $user->group,
																'following' => $following
															);
				}
			}
			$returnArr['userProfileDetails']=$userProfileDetails;
			$this->data['featured_seller'] = $this->mobile_model->get_featured_sellers();
			if($this->data['featured_seller']->num_rows > 0){
				$featured = $this->data['featured_seller']->result();
				$featuredId = array();
				foreach ($featured as $seller) {
					$featuredId[] = $seller->id;
				}
			}
			if($this->config->item('storefront_fees_month') == '0' && $this->config->item('storefront_fees_year') == '0'){
					$condition = " Select * from ".USERS." where 'group'='Seller' and 'status'=>'Active' and  user_name like '%".$search_key."%'";
					$sellers_list = $this->mobile_model->get_all_details($condition);
			}else{
				$condition = 'select u.*,s.store_name,s.logo_image,s.cover_image,s.tagline , s.description from '.USERS.' as u left join '.STORE_FRONT.' as s on s.user_id = u.id where u.group ="Seller" and u.store_payment ="Paid" and u.status = "Active" and NOT FIND_IN_SET(u.id ,"'.implode(",", $featuredId).'") and full_name like "%'.$search_key.'%"  or u.group ="Seller" and u.store_payment ="Paid" and u.status = "Active" and user_name like "%'.$search_key.'%" and NOT FIND_IN_SET(u.id ,"'.implode(",", $featuredId).'") or s.store_name like "%'.$search_key.'%"';
				$sellers_list  = $this->mobile_model->ExecuteQuery($condition);

			}
			$storeDetails = array();
			//print_r($sellers_list->result());die;
			if($sellers_list->num_rows() > 0){
				foreach($sellers_list->result() as $store){
					$storeBanner = base_url().'images/store/banner-dummy.jpg';
					$storeLogo = base_url().'images/store/dummy-logo.png';
					$storeTagline = "";
					$storeDescription = "";
					$storeName ="";
					$storeName = $store->store_name;
					if($store->cover_image !=""){
						$storeBanner = base_url().'images/store/'.$store->cover_image;
					}
					if($store->logo_image !=""){
						$storeLogo =  base_url().'images/store/'.$store->logo_image;
					}
					$storeTagline =$store->tagline;
					$storeDescription =$store->description;
					$userThumbnail = base_url().'images/users/user-thumb1.png';
					if($store->thumbnail !='') $userThumbnail = base_url().'images/users/'.$store->thumbnail;

					$following = 'No';
					if(in_array($store->id,$followingListArr)){$following = 'Yes';}
					$storeDetails[] = array(
												'UserId' => $store->id,
												'fullName' => $store->full_name,
												'userName' => $store->user_name,
												'UserImage' => $userThumbnail,
												'state' => $store->state,
												'country' => $store->country,
												'storeName' => $storeName,
												'storeBanner' => $storeBanner,
												'storeLogo' => $storeLogo,
												'storeDescription' =>strip_tags($storeDescription),
												'storeFollowing' => $following,
												);
				}
			}
			$returnArr['status_code'] = 1;
			$message = "Success";
			if($this->lang->line('json_success') != ""){
				$message = stripslashes($this->lang->line('json_success'));
			}
			$returnArr['response'] = $message;
			$returnArr['Stores']=$storeDetails;
		}else{
			if($type=='product'){
				$limit=10;
				$offset = 10*$count;
				$sellingProductDetails = $this->mobile_model->get_products_search_results($search_key,$limit,$offset);
				$affiliateProductDetails = $this->mobile_model->get_user_products_search_results($search_key,$limit,$offset);
				$productDetails =  $this->data['productDetails'] = $this->mobile_model->get_sorted_array_mobile($sellingProductDetails, $affiliateProductDetails, 'created', 'desc');
				$prodArr = array();
				if(count($productDetails) > 0){
					foreach($productDetails as $prodRow){

						$likedStatus = "No";
						$url = "";
						if($UserId != ""){
							$cond = "select user_id from ".PRODUCT_LIKES." where product_id=".$prodRow['seller_product_id']." and user_id=".$UserId;
							$likedUsersList = $this->mobile_model->ExecuteQuery($cond);
							if($likedUsersList->num_rows() > 0){
								$likedStatus = "Yes";
							}
							$url = $prodRow['url'].'&UserId='.$UserId;
						}

						$imgArr=@explode(',',$prodRow['image']);
						$images=array();
						foreach($imgArr as $img){
						if($img!="")
							$images[]=base_url().'images/product/'.$img;
						}
						$prodArr[]=array(
													'id'=>$prodRow['id'],
													'seller_product_id'=>$prodRow['seller_product_id'],
													'product_name'=>$prodRow['product_name'],
													'seourl'=>$prodRow['seourl'],
													'description'=>strip_tags($prodRow['description']),
													'shippingPolicies'=>strip_tags($prodRow['shipping_policies']),
													'image'=>$images,
													'price'=>$prodRow['price'],
													'sale_price'=>$prodRow['sale_price'],
													'user_name'=>$prodRow['user_name'],
													'likes'=>$prodRow['likes'],
													'liked' => $likedStatus,
													'url'=>$url,
													'type' => $prodRow['type'],
													'userUrl' => 	$normalUserUrl = 'http://192.168.1.251:8081/product-working/fancyclone-v3/json/user/seller-timeline?username='.$prodRow['user_name']
													);
					}
				}
				$returnArr['status_code'] = 1;
				$message = "Success";
			if($this->lang->line('json_success') != ""){
				$message = stripslashes($this->lang->line('json_success'));
			}
			$returnArr['response'] = $message;
				$returnArr['productDetails']=$prodArr;
			}else if($type=='people'){
				$usersCount=20;
				$offset=10*$count;
				$userDetails = $this->mobile_model->get_user_search_results($search_key,$usersCount,$offset);
				$userProfileDetails = array();
				if ($userDetails->num_rows()>0){
					foreach($userDetails->result() as $user){
						$userThumbnail = base_url().'images/users/user-thumb1.png';
						if($user->thumbnail !='') $userThumbnail = base_url().'images/users/'.$user->thumbnail;
						$following = 'No';
						if(in_array($user->id,$followingListArr)){$following = 'Yes';}
						$userProfileDetails[] = array(
																	'id'=> $user->id,
																	'userName'=> $user->user_name,
																	'fullName'=> $user->full_name,
																	'email'=> $user->email,
																	'userThumbnail'=> $userThumbnail,
																	'group'=> $user->group,
																	'following' => $following
																);
					}
				}
				$returnArr['status_code'] = 1;
				$message = "Success";
			if($this->lang->line('json_success') != ""){
				$message = stripslashes($this->lang->line('json_success'));
			}
			$returnArr['response'] = $message;
				$returnArr['userProfileDetails']=$userProfileDetails;
			}else if($type=='store'){
				$limit=20;
				$offset=10*$count;
				$this->data['featured_seller'] = $this->mobile_model->get_featured_sellers();
				if($this->data['featured_seller']->num_rows > 0){
					$featured = $this->data['featured_seller']->result();
					$featuredId = array();
					foreach ($featured as $seller) {
						$featuredId[] = $seller->id;
					}
				}
				if($this->config->item('storefront_fees_month') == '0' && $this->config->item('storefront_fees_year') == '0'){
						$condition = " Select * from ".USERS." where 'group'='Seller' and 'status'=>'Active' and  user_name like '%".$search_key."%' limit". $limit." offset ".$offset;
						$sellers_list = $this->mobile_model->get_all_details($condition);
				}else{
					$condition = 'select u.*,s.store_name,s.logo_image,s.cover_image,s.tagline , s.description from '.USERS.' as u left join '.STORE_FRONT.' as s on s.user_id = u.id where u.group ="Seller" and u.store_payment ="Paid" and u.status = "Active" and NOT FIND_IN_SET(u.id ,"'.implode(",", $featuredId).'") and full_name like "%'.$search_key.'%"  or u.group ="Seller" and u.store_payment ="Paid" and u.status = "Active" and user_name like "%'.$search_key.'%" and NOT FIND_IN_SET(u.id ,"'.implode(",", $featuredId).'") or s.store_name like "%'.$search_key.'%" limit '.$limit.' offset '.$offset;
					$sellers_list  = $this->mobile_model->ExecuteQuery($condition);

				}
				$storeDetails = array();
				if($sellers_list->num_rows() > 0){
					foreach($sellers_list->result() as $store){
						$storeBanner = base_url().'images/store/banner-dummy.jpg';
						$storeLogo = base_url().'images/store/dummy-logo.png';
						$storeTagline = "";
						$storeDescription = "";
						$storeName ="";
						$storeName = $store->store_name;
						if($store->cover_image !=""){
							$storeBanner = base_url().'images/store/'.$store->cover_image;
						}
						if($store->logo_image !=""){
							$storeLogo =  base_url().'images/store/'.$store->logo_image;
						}
						$storeTagline =$store->tagline;
						$storeDescription =$store->description;
						$userThumbnail = base_url().'images/users/user-thumb1.png';
						if($store->thumbnail !='') $userThumbnail = base_url().'images/users/'.$store->thumbnail;

						$following = 'No';
						if(in_array($store->id,$followingListArr)){$following = 'Yes';}

						$storeDetails[] = array(
													'UserId' => $store->id,
													'fullName' => $store->full_name,
													'userName' => $store->user_name,
													'UserImage' => $userThumbnail,
													'state' => $store->state,
													'country' => $store->country,
													'storeName' => $storeName,
													'storeBanner' => $storeBanner,
													'storeLogo' => $storeLogo,
													'storeDescription' =>strip_tags($storeDescription),
													'storeFollowing' => $following,
													);
					}
				}
				$returnArr['status_code'] = 1;
				$message = "Success";
			if($this->lang->line('json_success') != ""){
				$message = stripslashes($this->lang->line('json_success'));
			}
			$returnArr['response'] = $message;
				$returnArr['Stores']=$storeDetails;
			}
		}
	}else{
		$returnArr['status_code'] = 0;
		$returnArr['response'] = 'Please Enter Something To search !!';
	}
		echo json_encode($returnArr);
	}

	/*
	* !! >>>>>>>>>>>>> Add Seller Product Form <<<<<<<<<<<< !!
	*/

	public function AddSellerProductForm() {
		$returnArr['status_code'] = 0;
		$returnArr['response'] = '';
		$UserId = $_GET['UserId'];
		if ($UserId == '') {
			$returnArr['response'] = 'UserId Null !! Please Login';
		} else {
			$this->data['userProfileDetails'] = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
			$productDetails=array();
			$userType = $this->data['userProfileDetails']->row()->group;
			if ($userType == 'Seller'){
				$pid = $_GET['ProductId'];
				$product_Details = $this->mobile_model->get_all_details(USER_PRODUCTS, array('seller_product_id' => $pid));
				if ($product_Details->num_rows() == 1) {
					if ($product_Details->row()->user_id == $this->data['userProfileDetails']->row()->id) {
						$shipping_Cost_Details = $this->mobile_model->get_all_details(SHIPPING_COST, array('product_id' => $pid));
						$productDetails[]=array(
																'id'  => $product_Details->row()->id,
																'sellerProductId' => $product_Details->row()->seller_product_id,
																'productName' => $product_Details->row()->product_name,
																'seourl' => $product_Details->row()->seourl,
																'excerpt' => $product_Details->row()->excerpt,
																'categoryId' => $product_Details->row()->category_id,
																'image' => $product_Details->row()->image,
																'status' => $product_Details->row()->status,
																'userId' => $product_Details->row()->user_id,
																'shortUrlId' => $product_Details->row()->short_url_id,
																'likes' => $product_Details->row()->likes
																);
						$returnArr['productDetails'] = $productDetails;
						$returnArr['status_code'] = 1;
						$message = "Success";
						if($this->lang->line('json_success') != ""){
							$message = stripslashes($this->lang->line('json_success'));
						}
						$returnArr['response'] = $message;
					}else {
						$returnArr['status_code'] = 0;
						$returnArr['response'] = 'You cant add other\'s product as seller product';
					}
				} else {
					$returnArr['status_code'] = 0;
					$returnArr['response'] = 'Product Not Found';
				}
			} else {
				$returnArr['status_code'] = 0;
				$returnArr['response'] = 'Subscribe as Seller';
			}
		}
		echo json_encode($returnArr);
	}

	public function search_shopby(){
		$searchResult = explode('?',$_SERVER['REQUEST_URI']);
		
     	$searchCriteriaArr = explode('/',$searchResult[0]);
		$searchCriteria = $searchCriteriaArr[count($searchCriteriaArr)-1];	
			
		$searchCriteriaBreadCump = explode('shopby/',$searchResult[0]);

		$searchCriteriaBreadCumpFinal = explode('shopby/',$searchCriteriaBreadCump[1]);

		
//		if($_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == '192.168.1.253') {
//			$urlVal = str_replace('fancyclone/','', $_SERVER['REQUEST_URI']);
//		} else {
		$urlVal = $_SERVER['REQUEST_URI'];
//		}
		$urlVal = substr($urlVal, strpos($urlVal, '/shopby'));		
		$completeQury = $urlVal = substr($urlVal,1);
		$urlValArrVal = explode('?',$urlVal);
		$urlVal = $urlValArrVal[0];
		$searchResultPg = explode('?pg=', $searchResult[1]);
		if($searchResult[1] != ''){
			$finalValQry1 = '?'.$searchResult[1];
			$finalValQry = $urlVal.$finalValQry1;
			$finalValQryArr =explode('&pg=',$finalValQry);
			$finalValQry = $finalValQryArr[0].'&pg';
		} else {
			$finalValQry = $urlVal	.'?pg';
		}
		$newPage = 1;
		if($this->input->get('pg') != ''){
				$paginationVal = $this->input->get('pg')*20;
				$limitPaging = ' limit '.$paginationVal;
		} else {
				$limitPaging = ' limit 20';
		}
		$newPage = $this->input->get('pg')+1;
		$paginationDisplay  = '<a title="'.$newPage.'" class="btn-more" href="'.base_url().$finalValQry.'='.$newPage.'" style="display: none;">See More Products</a>';
		$this->data['paginationDisplay'] = $paginationDisplay;
		unset($_SERVER['REQUEST_URI']);
		$this->session->unset_userdata('sSearchCondition','');
		$this->session->unset_userdata('sSearchQueryString','');
		$this->session->set_userdata('sSearchCondition',$urlValArrVal[0]);
		$this->session->set_userdata('sSearchQueryString',$urlValArrVal[1]);
		$breadCumps .= '<ul class="breadcrumbs">
        					    <li><a class="shop-home" href="'.base_url().'shopby/all">Shop</a></li>';
		if($searchCriteria != 'all') {

			$condition = " where c.seourl = '".$searchCriteria."'";
			$catID = $this->mobile_model->getCategoryValues(' c.*,sbc.id as subcat_id,sbc.seourl as subcat_seourl,sbc.cat_name as subcat_sub_cat_name ',$condition);
			$listSubCat = $catID->result();
			$listSubCatSelBox = '<select style="display: none;" class="shop-select sub-category selectBox" edge="true">
              <option value="">'.$catID->row()->cat_name.'</option>';
			foreach ($listSubCat as $listSub){
				$listSubCatSelBox.= '<option value="'.base_url().$urlVal.'/'.$listSub->subcat_seourl.'">'.$listSub->subcat_sub_cat_name.'</option>';
			}
			$listSubCatSelBox.= '</select>';
			$searchCriteriaBreadCumpArr = @explode('/',trim($searchCriteriaBreadCumpFinal[0]));
			if(count($searchCriteriaBreadCumpArr)>1) {
				$link_str = base_url().'shopby';
				for($i=0;$i<count($searchCriteriaBreadCumpArr);$i++) {
					if($searchCriteriaBreadCumpArr[$i]) {
						$condition = " where c.seourl = '".$searchCriteriaBreadCumpArr[$i]."' limit 0,1";
						$Paging = $this->mobile_model->getCategoryValues(' c.*',$condition);
						$link_str .= '/'.$Paging->row()->seourl;
 				        $breadCumps .=  '<li class="last">/ <a href="'.$link_str.'">'.$Paging->row()->cat_name.'</a></li>';
					}
				}
			} else {
				$breadCumps .=  '<li class="last">/ <a href="'.base_url().'shopby/'.$catID->row()->seourl.'">'.$catID->row()->cat_name.'</a></li>';
			}
		}else {
			$urlVal = str_replace('/all','',$urlVal);
			$listSubCatSelBox = '<select style="display: none;" class="shop-select sub-category selectBox" edge="true">
              <option value="">All Category</option>';
			foreach($_SESSION['sMainCategories']->result() as $listSub){
				$listSubCatSelBox.= '<option value="'.base_url().$urlVal.'/'.$listSub->seourl.'">'.$listSub->cat_name.'</option>';
			}
			$listSubCatSelBox.= '</select>';
		}
		$breadCumps .= '</ul>';
		$this->data['listSubCatSelBox'] = $listSubCatSelBox;
		$this->data['breadCumps'] = $breadCumps;
		if($_GET['p']){
			if($_GET['p']!="Any"){
				  $price = explode('-',$_GET['p']);
			      $whereCond .= ' and p.sale_price >= "'.$price[0].'" and p.sale_price <= "'.$price[1].'"';
		
			}
		 }
		if($this->input->get('is')){
			if($this->input->get('is')!='null'){
				$whereCond .= ' and p.ship_immediate = "'.$this->input->get('is').'"';
			}
			
		}
		if($this->input->get('q')){
			$whereCond .= ' and p.product_name LIKE "%'.$this->input->get('q').'%"';
		}

		/* if($this->input->get('c')){

			$condition = " where list_value_seourl = '".$this->input->get('c')."'";
			$listID = $this->category_model->getAttrubteValues($condition);
			$whereCond .= ' and FIND_IN_SET("'.$listID->row()->id.'",p.list_value)';
		} */
		/*if($this->input->get('c')){
		$condition = " where name = '".$this->input->get('c')."'";
		//print_r($condition);die;
			$countryID = $this->category_model->getCountryValues($condition);
			echo("<pre>");print_r($countryID->result());die;
			$whereCond .= ' and p.user_id = "'.$countryID->row()->u_id.'"';
		}*/

		if($this->input->get('c')){
			if($this->input->get('c') != 'All'){

			$condition = " where name = '".$this->input->get('c')."'";
			$countryID = $this->mobile_model->getCountryValues($condition);
			$whereCond .= ' and u.country = "'.$countryID->row()->country_code.'"';
			}

		}
		if($this->input->get('sort_by_price')){
			($this->input->get('sort_by_price') == 'desc') ? $orderbyVal = $this->input->get('sort_by_price') : $orderbyVal ='';
			$orderBy = ' order by p.sale_price '.$orderbyVal.'';
		}else {
			$orderBy = ' order by p.created desc ';
		}
		if($searchCriteria != 'all') {

			$whereCond = ' where FIND_IN_SET("'.$catID->row()->id.'",p.category_id) '.$whereCond.' and p.quantity>0 and p.status="Publish" and u.group="Seller" and u.status="Active" or FIND_IN_SET("'.$catID->row()->id.'",p.category_id) '.$whereCond.' and p.status="Publish" and p.quantity > 0 and p.user_id=0';
			$searchProd = $whereCond.' '.$orderBy.' '.$limitPaging.' ';
			$totalProd = $whereCond.' '.$orderBy.' '.$limitPaging.' ';
		} else {

			$whereCond = ' where p.id != "" '.$whereCond.' and p.quantity>0 and p.status="Publish" and u.group="Seller" and u.status="Active" or p.id != "" '.$whereCond.' and p.status="Publish" and p.quantity > 0 and p.user_id=0';
			$searchProd = $whereCond.' '.$orderBy.' '.$limitPaging.' ';
			$totalProd = $whereCond.' '.$orderBy.' '.$limitPaging.' ';
		}
		

		$productList = $this->mobile_model->searchShopyByCategory($searchProd);
		
		$productListArr = array();
		foreach($productList->result() as $productListRow){
		$imgArr=@explode(',',$productListRow->image);
		//$images = ($imgArr[0]=='') ? '' : base_url().'images/product/'.$imgArr[0];
		$images =base_url().'images/product/dummyProductImage.jpg';
		if($productListRow->image !='') $images = base_url().'images/product/'.$imgArr[0];
		$productListArr[]=array(
										'id'=>$productListRow->id,
										'seller_prodcut_id'=>$productListRow->seller_product_id,
										'product_name'=>$productListRow->product_name,
										// 'image'=> base_url().'images/product/'.$productListRow->image,
										'image'=> $images,
										'UserId'=>$productListRow->user_id,
										'description'=>strip_tags($productListRow->description),
										'shippingPolicies'=>strip_tags($productListRow->shipping_policies),
										'sale_price'=>$productListRow->sale_price,
										'user_name'=>$productListRow->user_name,
										'price'=>$productListRow->price,
										'sale_price'=>$productListRow->sale_price,
										'user_name'=>$productListRow->user_name,
										'likes'=>$productListRow->likes,
										'url'=>base_url().'json/product?pid='.$productListRow->id,
										'type' => 'SellerProduct',
										'userUrl' => 'http://192.168.1.251:8081/product-working/fancyclone-v3/json/user/seller-timeline?username='.$productListRow->user_name
										);
		}
		$returnArr['productListDetails']=$productListArr;
		echo json_encode($returnArr);
	}

	public function countryCategory(){
		$returnArr['status'] = (string)0;
		$returnArr['response'] = '';
		$sortArr1 = array('field'=>'cat_position','type'=>'asc');
		$sortArr = array($sortArr1);
		$sMainCategories = $this->product_model->get_all_details(CATEGORY,array('rootID'=>'0','status'=>'Active'),$sortArr);
		foreach($sMainCategories->result() as $MainCategories){
			$categoryImage = base_url().'images/category/dummy.jpg';
			if($MainCategories->image != ""){
				$categoryImage = base_url().'images/category/'.$MainCategories->image;
			}
			$categoryList[]  =  array(
													'id'  =>  $MainCategories->id,
													'categoryName'  =>  $MainCategories->cat_name,
													'rootID'  =>  $MainCategories->rootID,
													'seourl'  =>  $MainCategories->seourl,
													//'status'  =>  $MainCategories->status,
													//'image'  =>  $categoryImage,
													//'url'      =>   base_url().'json/shopby/'.$MainCategories->seourl
													//'catPosition'  =>  $list_category_details->row()->cat_position,
													//	'seo_title'  =>  $list_category_details->row()->seo_title,
													//	'seo_keyword'  =>  $list_category_details->row()->seo_keyword,
													//	'seo_description'  =>  $list_category_details->row()->seo_description,
													//	'dateAdded'  =>  $list_category_details->row()->dateAdded
										);

		}
		$countryList= $this->mobile_model->get_all_details(COUNTRY_LIST, array(), array(array('field' => 'name', 'type' => 'asc')));
		$countryLists = array();
		if($countryList->num_rows() > 0){
			foreach($countryList->result() as $country){
					$countryLists[] = array(
														'categoryName' => $country->name,
														'id' => $country->country_code
												);
			}
		}
		$returnArr['status'] = (string)1;
		$message = "Success";
		if($this->lang->line('json_success') != ""){
			$message = stripslashes($this->lang->line('json_success'));
		}
		$returnArr['response'] = $message;
		$returnArr['CountryLists'] = $countryLists;
		$returnArr['categoryList'] = $categoryList;
		echo json_encode($returnArr);
	}

	public function coutryList(){
		$countryList= $this->mobile_model->get_all_details(COUNTRY_LIST, array(), array(array('field' => 'name', 'type' => 'asc')));
		$countryLists = array();
		if($countryList->num_rows() > 0){
			foreach($countryList->result() as $country){
					$countryLists[] = array(
														'countryId' => $country->id,
														'categoryName' => $country->name,
														'id' => $country->country_code
												);
			}
		}
		$returnArr['CountryLists'] = $countryLists;
		echo json_encode($returnArr);
	}

	public function cart(){
		$returnArr['status_code'] = 0;
		$returnArr['response'] = '';
		$UserId = $_GET['UserId'];
		$ShipId = $_GET['shipId'];
		if ($UserId != '') {
			$MainShipCost = 0;
			$MainTaxCost = 0;
			$cartQty = 0;
			$shipVal = $this->mobile_model->get_all_details(SHIPPING_ADDRESS, array('user_id' => $UserId));
			if ($shipVal->num_rows() > 0) {
				$shipValID = $this->mobile_model->get_all_details(SHIPPING_ADDRESS, array('user_id' => $UserId, 'primary' => 'Yes'));
				$dataArr = array('shipping_id' => $shipValID->row()->id);
				$condition = array('user_id' => $UserId);
				$this->mobile_model->update_details(FANCYYBOX_TEMP, $dataArr, $condition);
				$ShipCostVal = $this->mobile_model->get_all_details(COUNTRY_LIST, array('country_code' => $shipValID->row()->country));
				$MainShipCost = $ShipCostVal->row()->shipping_cost;
				$MainTaxCost = $ShipCostVal->row()->shipping_tax;
				$dataArr2 = array('shipping_cost' => $MainShipCost, 'tax' => $MainTaxCost);
				$this->mobile_model->update_details(SHOPPING_CART, $dataArr2, $condition);
			}
			/* >>>>>>>>>>>> Discount Amount <<<<<<<<<<< */
			$this->db->select('discountAmount,couponID,couponCode,coupontype');
			$this->db->from(SHOPPING_CART);
			$this->db->where('user_id = ' . $UserId);
			$discountQuery = $this->db->get();
			$disAmt = $discountQuery->row()->discountAmount;
			$couponID = '';
			$coupontype = '';
			$couponCode = '';
			if ($disAmt > 0) {
				$couponID =$discountQuery->row()->couponID;
				$coupontype =$discountQuery->row()->coupontype;
				$couponCode =  $discountQuery->row()->couponCode;
			}
			/* >>>>>>>>>>>> Cart Details <<<<<<<<<<< */
			$cart_Details = $this->mobile_model->getProductsInCart($UserId);
			$cartAmt = 0; $cartShippingAmt = 0;$cartTaxAmt = 0;$s = 0;$ship_need = 0;
			$cartDetails = array();
			if($cart_Details->num_rows() > 0){
				foreach ($cart_Details->result() as $CartRow) {
					$subShipping = $this->cart_model->get_all_details(SUB_SHIPPING, array('product_id'=> $CartRow->product_id));
					$countryCode = [];
					$everyelse =[];
					if($subShipping->num_rows() >0){
						foreach($subShipping->result() as $_subShipping){
							$countryCode[] = $_subShipping->ship_code;
							$everyelse[] = $_subShipping->ship_name;
						}
					}
					if($ShipId == "empty"){
						$shippingAddressOfUser = $this->cart_model->get_all_details(SHIPPING_ADDRESS, array('user_id' => $UserId,'primary'=>'Yes'));
						$ShipId = $shippingAddressOfUser->row()->id;
					}else{
						$shippingAddressOfUser = $this->cart_model->get_all_details(SHIPPING_ADDRESS, array('user_id' => $UserId,'id'=>$ShipId));
					}
					if(in_array($shippingAddressOfUser->row()->country, $countryCode)){
						$prodIds[] = $CartRow->product_id;
					}elseif(in_array('Everywhere Else', $everyelse)){
						$prodIds[] = $CartRow->product_id;
					}else{
						$UnprodIds[] = $CartRow->product_id;
					}
				}
				$s=0;
				foreach ($cart_Details->result() as $CartRow) {
					if($CartRow->product_type == 'physical'){
						$ship_need++;
						if($ShipId =='empty'){
							$shipping = $this->cart_model->get_all_details(SHIPPING_ADDRESS, array('user_id' => $userid,'primary'=>'Yes'));
							if($shipping->num_rows() >0){
								$ShipId = $shipping->row()->id;
							}
						}
					}
					$attri = array_filter(explode(',', $CartRow->attribute_values));
					$curdate = date('Y-m-d');
					$newImg = @explode(',', $CartRow->image);
					$productUrl =  base_url().'things/' . $CartRow->prdid . '/' . $CartRow->seourl;
					$productImage =  base_url().'images/product/' . $newImg[0];
					$productName  = $CartRow->product_name;
					$productId = $CartRow->prdid;
					$productAttributes = array();
					$productAttributesList= array();
					foreach ($attri as $_attributes) {
						$Query = "select b.attr_name as attr_type,a.attr_name from " . SUBPRODUCT . " as a left join " . PRODUCT_ATTRIBUTE . " as b on a.attr_id = b.id where a.pid = '" . $_attributes . "'";
						$attributes = $this->mobile_model->ExecuteQuery($Query);
						if ($attributes->num_rows() > 0) {
							$productAttributesList[] = array('values' => $attributes->row()->attr_type);
							$productAttributes[][$attributes->row()->attr_type]= array(
																									' name'=>$attributes->row()->attr_type,
																									' value' => $attributes->row()->attr_name
																									);
						}
					}
					$productPrice = $this->data['currencySymbol'] . $CartRow->price;
					if ($CartRow->product_type == 'physical') {
						$productQuantity  =  $CartRow->quantity;
						$totalProductQuantity  =  $CartRow->mqty;
						$QuantityUpdatable = 'Yes';
					} else {
						$productQuantity  =  $CartRow->quantity;
						$totalProductQuantity  = '';
						$QuantityUpdatable = 'No';
					}
					$totalProductPrice = $this->data['currencySymbol']. $CartRow->indtotal;
				//	$productCost  =  $CartRow->product_shipping_cost + $CartRow->price + ($CartRow->price * 0.01 * $CartRow->product_tax_cost);
				//	$amount  =  ($CartRow->product_shipping_cost + $CartRow->price + ($CartRow->price * 0.01 * $CartRow->product_tax_cost)) * $CartRow->quantity;
				//	$cartAmt = $cartAmt + (($CartRow->product_shipping_cost + $CartRow->price + ($CartRow->price * 0.01 * $CartRow->product_tax_cost)) * $CartRow->quantity);
					// if($getshipcost->num_rows() >0){
					// 	$cartShippingAmt = number_format(($getshipcost->row()->separate_ship_cost * $CartRow->quantity), 2, '.', '');
					// }else{
		            //     $cartShippingAmt = number_format(($CartRow->shipping_cost * $CartRow->quantity), 2, '.', '');
					// }
					// if ($CartRow->ship_immediate == 'true') {
					// 	$productRefShipping ='Immediate';
					// } else {
					// 	$productRefShipping =date('d/m', strtotime($curdate)) . ' - ' . date('d/m', strtotime($curdate . ' + 10 day'));
					// }
					// if($getshipcost->num_rows()>0){
					// 	$totalShipCost = $totalShipCost + ($getshipcost->row()->separate_ship_cost * $CartRow->quantity);
					// }else{
					// 	$totalShipCost = $totalShipCost + ($CartRow->shipping_cost * $CartRow->quantity);
					// }
					if(in_array($CartRow->product_id, $prodIds)){
						$shippingPossible = "yes";
						if($ShipId!=''){
							$Query = "SELECT * FROM (SELECT MAX(`sub`.`ship_cost`) AS maxCost, MAX(`sub`.`ship_other_cost`) AS maxOtherCost, `c`.`name` FROM (`fc_sub_shipping` as sub) JOIN `fc_shipping_address` as ship ON `ship`.`country` = `sub`.`ship_code` JOIN `fc_country` as c ON `c`.`country_code` = `ship`.`country` WHERE `ship`.`id` = ".$ShipId." AND `sub`.`status` = 'Active' AND `sub`.`product_id` IN (".implode(',', $prodIds).")) AS A WHERE A.maxCost IS NOT NULL";
							$shipCostDetails = $this->cart_model->ExecuteQuery($Query);
							if($shipCostDetails->num_rows() > 0){
								$shipCost = $shipCostDetails->row()->maxCost;
								$shipOtherCost = $shipCostDetails->row()->maxOtherCost;
							}else{
								$Query = "SELECT * FROM (SELECT MAX(`ship_cost`) AS maxCost, MAX(`ship_other_cost`) AS maxOtherCost FROM (`fc_sub_shipping`) WHERE `status` = 'Active' AND `ship_id` = 232 AND `product_id` IN (".implode(',', $prodIds).")) AS A WHERE A.maxCost IS NOT NULL";
								$shipCostDetails = $this->cart_model->ExecuteQuery($Query);
								if($shipCostDetails->num_rows() > 0){
									$shipCost = $shipCostDetails->row()->maxCost;
									$shipOtherCost = $shipCostDetails->row()->maxOtherCost;
								}
							}
						}else{
							$Query = "SELECT * FROM (SELECT MAX(`ship_cost`) AS maxCost, MAX(`ship_other_cost`) AS maxOtherCost FROM (`fc_sub_shipping`) WHERE `status` = 'Active' AND `ship_id` = 232 AND `product_id` IN (".implode(',', $prodIds).")) AS A WHERE A.maxCost IS NOT NULL";
							$shipCostDetails = $this->cart_model->ExecuteQuery($Query);
							if($shipCostDetails->num_rows() > 0){
								$shipCost = $shipCostDetails->row()->maxCost;
								$shipOtherCost = $shipCostDetails->row()->maxOtherCost;
							}
						}
						$condition = "select * from ".SHOPPING_CART." where `product_id` IN(".implode(',',$prodIds).") and user_id=".$UserId;
						$cartProduct = $this->cart_model->ExecuteQuery($condition);
						if($shipCostDetails->num_rows() > 0){
							if($cartProduct->num_rows() <= 1){
								$shippingValue =0;
								if($CartRow->quantity >1){
									$Quantity = $CartRow->quantity - 1;
									$shippingValue = $shipCost + ($shipOtherCost * $Quantity);
								}else{
									$shippingValue = $shipCost;
								}
							}else{
								if($s == 0){
									$shippingValue =0;
									if($CartRow->quantity > 1){
										$Quantity = $CartRow->quantity - 1;
										$shippingValue = $shipCost + ($shipOtherCost * $Quantity);
									}else{
										$shippingValue = $shipCost;
									}
								}else{
									$shippingValue =0;
									$shippingValue = $shipOtherCost * $CartRow->quantity;
								}
							}
							if ($CartRow->ship_immediate == 'true') {
								$productRefShipping =$shippingValue. '(Immediate)';
								$productRefShipping = (string)$productRefShipping;
							} else {
								if($CartRow->ship_duration != ""){
									$productRefShipping = number_format($shippingValue,2). '('.$CartRow->ship_duration.')';
								}else{
									$productRefShipping = number_format($shippingValue,2);
								}
								$productRefShipping = (string)$productRefShipping;
							}
							$productCost =  (($CartRow->product_shipping_cost + $CartRow->price + ($CartRow->price * 0.01 * $CartRow->product_tax_cost)));
							$productTotalCost = (($CartRow->product_shipping_cost + $CartRow->price + ($CartRow->price * 0.01 * $CartRow->product_tax_cost)) * $CartRow->quantity);
							$cartAmt = $cartAmt + (($CartRow->product_shipping_cost + $CartRow->price + ($CartRow->price * 0.01 * $CartRow->product_tax_cost)) * $CartRow->quantity);
						//	$cartShippingAmt = $cartShippingAmt + ($CartRow->product_shipping_cost * $CartRow->quantity);
							$cartTaxAmt = $cartTaxAmt + ($CartRow->product_tax_cost * $CartRow->quantity);
							$TotalShipCost = $TotalShipCost + $shippingValue;
							$cartQty = $cartQty + $CartRow->quantity;

							$shippingPossible = "Yes";
							$cartDetails[]  =  array(
																's' => (string)$s,
																'cartId' => $CartRow->id,
																'productUrl' => $productUrl,
																'productImage' => $productImage,
																'productName' => $productName,
																'productId' => $productId,
																'productType' => $CartRow->product_type,
																'productAttributesList' => $productAttributesList,
																'productAttributes' => $productAttributes,
																'productQuantity' => $productQuantity,
																'totalProductQuantity' => $totalProductQuantity,
																'QuantityUpdatable' => $QuantityUpdatable,
																'productRefShipping' => $productRefShipping,
																'cartShippingAmt' => number_format($shippingValue,2,'.',''),
																'cartTaxAmt' => number_format($cartTaxAmt, 2, '.', ''),
																'cartQty' => (string)$cartQty,
																'productCost' => number_format($productCost, 2, '.', ''),
																'Amount'  =>  number_format($productTotalCost, 2, '.', ''),
																'shippingPossible' => $shippingPossible,
														);
						}else{
							$shippingPossible = "No";
							$cartDetails[]  =  array(
																's' => (string)$s,
																'cartId' => $CartRow->id,
																'productUrl' => $productUrl,
																'productImage' => $productImage,
																'productName' => $productName,
																'productId' => $productId,
																'productType' => $CartRow->product_type,
																'productAttributesList' => $productAttributesList,
																'productAttributes' => $productAttributes,
																'productQuantity' => $productQuantity,
																'totalProductQuantity' => $totalProductQuantity,
																'QuantityUpdatable' => $QuantityUpdatable,
																'productRefShipping' => "This product cant be shipped to your address !!",
																'cartShippingAmt' => number_format($shippingValue,2,'.',''),
																'cartTaxAmt' => number_format($cartTaxAmt, 2, '.', ''),
																'cartQty' => (string)$cartQty,
																'productCost' => number_format($productCost, 2, '.', ''),
																'Amount'  =>  number_format($productTotalCost, 2, '.', ''),
																'shippingPossible' => $shippingPossible,
														);
						}
					}else{
						$shippingPossible = "No";
						$cartDetails[]  =  array(
															's' => (string)$s,
															'cartId' => $CartRow->id,
															'productUrl' => $productUrl,
															'productImage' => $productImage,
															'productName' => $productName,
															'productId' => $productId,
															'productType' => $CartRow->product_type,
															'productAttributesList' => $productAttributesList,
															'productAttributes' => $productAttributes,
															'productQuantity' => $productQuantity,
															'totalProductQuantity' => $totalProductQuantity,
															'QuantityUpdatable' => $QuantityUpdatable,
															'productRefShipping' => "This product cant be shipped to your address !!",
															'cartShippingAmt' => number_format($shippingValue,2,'.',''),
															'cartTaxAmt' => number_format($cartTaxAmt, 2, '.', ''),
															'cartQty' => (string)$cartQty,
															'productCost' => number_format($productCost, 2, '.', ''),
															'Amount'  =>  number_format($productTotalCost, 2, '.', ''),
															'shippingPossible' => $shippingPossible,
													);
					}
				$s++;
			}
			$returnArr['cartDetails'] = $cartDetails;

		/* >>>>>>>>>>>> Total Amount  <<<<<<<<<<< */
			$cartSAmt = $TotalShipCost;
			$cartTAmt = ($cartAmt * 0.01 * $MainTaxCost);
			$grantAmt = $cartAmt + $cartSAmt + $cartTAmt;
			if($disAmt > 0){
				$grantAmt = $grantAmt-$disAmt;
			}
			$totalAmount=array(
										'CartItemTotal'  =>  number_format($cartAmt, 2, '.', ''),
										'MainTaxCost' => $MainTaxCost,
										'discountAmt' => $disAmt,
										'couponCode' => $couponCode,
										'couponID' => $couponID,
										'coupontype' => $coupontype,
										'CartShippingAmount'  =>  number_format($cartSAmt, 2, '.', ''),
										'CartTaxAmount'  =>  number_format($cartTAmt, 2, '.', ''),
										'GrantAmount'  =>  number_format($grantAmt, 2, '.', '')
									);
			$returnArr['totalAmount'] = $totalAmount;
		/* >>>>>>>>>>>> Shipping Address <<<<<<<<<<< */
			$shippingAddress  =  array();
			$PrimaryAddress  =  array();
			foreach ($shipVal->result() as $Shiprow) {
				if ($Shiprow->primary == 'Yes') {
					$PrimaryAddress = array(
														'id'  =>  $Shiprow->id,
														'FullName'  =>  $Shiprow->full_name,
														'Address1'  =>  $Shiprow->address1,
														'city'  =>  $Shiprow->city,
														'state'  =>  $Shiprow->state,
														'postalCode'  =>  $Shiprow->postal_code,
														'country'  =>  $Shiprow->country,
														'phone'  =>  $Shiprow->phone
													);
					if($ShipId != ""){
						if($ShipId == $Shiprow->id){
							$optionsValues = 'selected';
						}else{
							$optionsValues = '';
						}
					}else{
						$optionsValues = 'selected';
					}
				}else {
					if($ShipId != ""){
						if($ShipId == $Shiprow->id){
							$optionsValues = 'selected';
						}else{
							$optionsValues = '';
						}
					}else{
						$optionsValues = '';
					}
				}
				$shippingAddress[]  =  array(
														'selectedAddress' =>  $optionsValues,
														'id'  =>  $Shiprow->id,
														'FullName'  =>  $Shiprow->full_name,
														'Address1'  =>  $Shiprow->address1,
														'city'  =>  $Shiprow->city,
														'state'  =>  $Shiprow->state,
														'postalCode'  =>  $Shiprow->postal_code,
														'country'  =>  $Shiprow->country,
														'phone'  =>  $Shiprow->phone
													);
				}
				$countryList= $this->mobile_model->get_all_details(COUNTRY_LIST, array(), array(array('field' => 'name', 'type' => 'asc')));
				$countryLists = array();
				foreach($countryList->result() as $country){
						$countryLists[] = array(
															'Country' => $country->name,
															'countryCode' => $country->country_code
													);
				}
			/* >>>>>>> Payment Methods <<<<<<<< */
				$condition = 'select * from '.PAYMENT_GATEWAY.' where status="Enable"';
				$payment_Gateway = $this->mobile_model->ExecuteQuery($condition);
				$paymentGateway = array();
				foreach ($payment_Gateway->result() as $_payment) {
					$paymentGateway[]=array(
														'gatewayId' => $_payment->id,
														'gatewayName' => $_payment->gateway_name
													);
				}
				$returnArr['PrimaryAddress'] = $PrimaryAddress;
				$returnArr['shippingAddress'] = $shippingAddress;
				$returnArr['paymentGateway'] = $paymentGateway;
				$returnArr['countryLists'] = $countryLists;
				$returnArr['status_code'] = (string)1;
				$message = "Success";
			if($this->lang->line('json_success') != ""){
				$message = stripslashes($this->lang->line('json_success'));
			}
			$returnArr['response'] = $message;
			}else{
				$returnArr['response'] = 'No Items in Cart';
			}
		} else {
				$returnArr['response'] = 'User Id Null, Please Log in';
		}
		echo json_encode($returnArr);
	}

	public function updateCartQuantity(){
		$returnArr['status'] = '0';
		$returnArr['response'] = '';
		$UserId = $_GET['UserId'];
		$cartId = $_GET['cartId'];
		$productId = $_GET['productId'];
		$cartQuantity = $_GET['cartQuantity'];
		$shipId = $_GET['shipId'];
		$totalQuantity = $_GET['totalQuantity'];
		if ($productId != '' && $UserId != '' && $cartId  != '' && $cartQuantity  != '' && $shipId  != '' && $totalQuantity != '') {
			$chckCartQuantity = $cartQuantity;
			if($chckCartQuantity > $totalQuantity){
				$errorTxt = 'Maximum stock available for this product is '.$totalQuantity;
				$returnArr['response'] = $errorTxt;
			}else{
				$excludeArr = array('cartQuantity', 'totalQuantity','UserId','cartId','productId','shipId');
		        $productVal = $this->mobile_model->get_all_details(SHOPPING_CART, array('user_id' => $UserId, 'id' => $cartId));
		        $newQty = $cartQuantity;
		        $indTotal = ($productVal->row()->price + ($productVal->row()->price * 0.01 * $productVal->row()->product_tax_cost) ) * $newQty;
		        $dataArr = array('quantity' => $newQty, 'indtotal' => $indTotal, 'total' => $indTotal);
		        $condition = array('id' => $productVal->row()->id);
		        $this->mobile_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
				// $rowIndTotal = ($productVal->row()->price * $newQty);
				// || -------->>>>>>>> Shipping calculation
				// $getshipcountry = $this->mobile_model->get_all_details(SHIPPING_ADDRESS, array('id'=> $shipId));
				// $product = $this->mobile_model->get_all_details(PRODUCT, array('id'=>$productId));
				// $getshipcost = $this->mobile_model->get_all_details(SHIPPING_COST, array('product_id' => $product->row()->seller_product_id,'country_code'=> $getshipcountry->row()->country));
		        // $ShipCostVal = $this->mobile_model->get_all_details(PRODUCT, array('id' =>$productId));
		        // $cond = "select separate_ship_cost from ".SHIPPING_COST." where product_id=".$ShipCostVal->row()->seller_product_id." && country_code='".$getshipcountry->row()->country."'";
		        // $separateShippingCost = $this->mobile_model->ExecuteQuery($cond);
		        // if(intval($separateShippingCost->row()->separate_ship_cost) != ""){
		        //     $MainShipCost = intval($separateShippingCost->row()->separate_ship_cost);
		        // }else{
		        //     $MainShipCost =  intval($ShipCostVal->row()->shipping_cost);
		        // }
		        // $rowshipcost = ($MainShipCost * $newQty);
				// // || -------->>>>>>>> Shipping calculation
				//
				// $CartVal = $this->mobile_model->mani_cart_total($UserId, $shipId);
		        // $updatedDetails =  array('indTotal'=>$indTotal,'rowIndTotal'=>$rowIndTotal,'rowshipcost'=>$rowshipcost);
				// $updatedProductDetails = array(
				// 	'productQuantity' => $newQty,
				// 	'productAmount' => number_format($updatedDetails['rowIndTotal'], 2, '.', ''),
				// 	'productShippingAmount' => number_format($updatedDetails['rowshipcost'], 2, '.', '')
				// );
				// $updatedCartDetails = array(
				// 	'itemTotal' => number_format($CartVal['cartAmt'], 2, '.', ''),
				// 	'shipping' => number_format($CartVal['cartSAmt'], 2, '.', ''),
				// 	'discount' => number_format($CartVal['discountAmount'], 2, '.', ''),
				// 	'tax' => number_format($CartVal['cartTAmt'], 2, '.', ''),
				// 	'grantAmount' => number_format($CartVal['grantAmt'], 2, '.', '')
				// );
			$returnArr['status'] = '1';
			$message = "Success";
			if($this->lang->line('json_success') != ""){
				$message = stripslashes($this->lang->line('json_success'));
			}
			$returnArr['response'] = $message;
			// $returnArr['cartCount'] = (string)$CartVal['countVal'];
			// $returnArr['UpdatedProductDetails'] = $updatedProductDetails;
			// $returnArr['UpdatedCartDetails'] = $updatedCartDetails;
			}
        } else {
			$returnArr['response'] = 'Some Parameters Missing !!';
        }
		echo json_encode($returnArr);
	}

	public function removeCartProduct(){
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = $_GET['UserId'];
		$delt_id = $_GET['cartId'];
		$shipId = $_GET['shipId'];
		$CondID =  $_GET['cart'];
		if($UserId !=" " && $delt_id !=" "){
			if ($CondID == 'gift') {
				$this->db->delete(GIFTCARDS_TEMP, array('id' => $delt_id));
				$GiftVal= $this->cart_model->mani_gift_total($UserId);
			} elseif ($CondID == 'subscribe') {
				$this->db->delete(FANCYYBOX_TEMP, array('id' => $delt_id));
				$SubscribeVal = $this->cart_model->mani_subcribe_total($UserId);
			} elseif ($CondID == 'cart') {
				$this->db->delete(SHOPPING_CART, array('id' => $delt_id));
			// 	$resultValues = $this->mobile_model->mani_cart_total($UserId,$shipId);
			// 	$updatedCartDetails = array(
			// 		'itemTotal' => number_format($resultValues['cartAmt'], 2, '.', ''),
			// 		'shipping' => number_format($resultValues['cartSAmt'], 2, '.', ''),
			// 		'discount' => number_format($resultValues['discountAmount'], 2, '.', ''),
			// 		'tax' => number_format($resultValues['cartTAmt'], 2, '.', ''),
			// 		'grantAmount' => number_format($resultValues['grantAmt'], 2, '.', '')
			// 	);
			// $CartVal = $this->mobile_model->mani_cart_total($UserId);
			$returnArr['status'] = '1';
			$message = "Success";
			if($this->lang->line('json_success') != ""){
				$message = stripslashes($this->lang->line('json_success'));
			}
			$returnArr['response'] = $message;
			// $returnArr['cartCount'] = (string)$CartVal['countVal'];
			// $returnArr['UpdatedCartDetails'] = $updatedCartDetails;
			}
		}else{
			$returnArr['response'] = 'Some Parameters are Misssing';
		}
		echo json_encode($returnArr);
	}

	/*
	!! >>>>>>>>>> Delete Shippping Address <<<<<<<<<<< !!
	*/
	public function ShippingDeleteAddress() {
		$returnArr['status_code'] = 0;
		$returnArr['response'] = '';
		$UserId  =  $_GET['UserId'];
        if ($UserId == '') {
			$returnArr['response'] = 'UserId Null !!';
        }else {
				$shippingAddrId = $_GET['shippingAddrId'];
				if($shippingAddrId !=""){
					$checkAddrCount = $this->cart_model->get_all_details(SHIPPING_ADDRESS, array('id' => $shippingAddrId, 'primary' => 'Yes'));
					if ($checkAddrCount->num_rows == 0) {
							$this->cart_model->commonDelete(SHIPPING_ADDRESS, array('id' => $shippingAddrId));
							$returnArr['status_code'] = 1;
							$returnArr['response'] = 'Successfully Deleted';
					}else {
							$returnArr['response'] = 'Primary Address Cannot be Deleted';
					}
				}else{
					$returnArr['response'] = 'Shipping Address Id is Empty';
				}
        }
		echo json_encode($returnArr);
    }

	/*
	!! >>>>>>>>>> Insert & Update Shippping Address <<<<<<<<<<< !!
	*/
	public function insertEditShippingAddress() {
		$returnArr['status_code'] = 0;
		$returnArr['response'] = '';
		$UserId  =  $_POST['user_id'];
        if ($UserId == '') {
			$returnArr['response'] = 'UserId Null !!';
        } else {
            $shipID = $_POST['ship_id'];
            $is_default = '';
            $is_default = $_POST['set_default'];
            if ($is_default == 'Yes') {
                $primary = 'Yes';
            } else {
                $primary = 'No';
            }
            $checkAddrCount = $this->mobile_model->get_all_details(SHIPPING_ADDRESS, array('user_id' => $UserId));
            if ($checkAddrCount->num_rows == 0) {
                $primary = 'Yes';
            }
            $excludeArr = array('ship_id', 'set_default');
            $dataArr = array('primary' => $primary);
            $condition = array('id' => $shipID);
            if ($shipID == '') {
                $this->mobile_model->commonInsertUpdate(SHIPPING_ADDRESS, 'insert', $excludeArr, $dataArr, $condition);
                $shipID = $this->mobile_model->get_last_insert_id();
				$condition1 = array('id' => $shipID, 'user_id' => $UserId);
                $insertedShipAddr = $this->mobile_model->get_all_details(SHIPPING_ADDRESS, $condition1);
				$optionsValues = '';
				$shippingAddress[]  =  array(
														'selectedAddress' =>  $optionsValues,
														'id'  =>  $insertedShipAddr->row()->id,
														'FullName'  =>  $insertedShipAddr->row()->full_name,
														'Address1'  =>  $insertedShipAddr->row()->address1,
														'city'  =>  $insertedShipAddr->row()->city,
														'state'  =>  $insertedShipAddr->row()->state,
														'postalCode'  =>  $insertedShipAddr->row()->postal_code,
														'country'  =>  $insertedShipAddr->row()->country,
														'phone'  =>  $insertedShipAddr->row()->phone
												 );
                if ($this->lang->line('ship_add_succ') != ''){
                    $lg_err_msg = $this->lang->line('ship_add_succ');
				}else{
                    $lg_err_msg = 'Your Shipping address is added successfully !';
				}
				$returnArr['status_code'] = 1;
				$returnArr['response'] = $lg_err_msg;
				$returnArr['shippingAddress'] =$shippingAddress;
            }else {
                $this->mobile_model->commonInsertUpdate(SHIPPING_ADDRESS, 'update', $excludeArr, $dataArr, $condition);
                if ($this->lang->line('ship_updat_succ') != ''){
                    $lg_err_msg = $this->lang->line('ship_updat_succ');
				}else{
                    $lg_err_msg = 'Shipping address updated successfully';
				}
				$returnArr['status_code'] = 1;
				$returnArr['response'] = $lg_err_msg;
            }
            if ($primary == 'Yes') {
                $condition = array('id !=' => $shipID, 'user_id' => $UserId);
                $dataArr = array('primary' => 'No');
                $this->mobile_model->update_details(SHIPPING_ADDRESS, $dataArr, $condition);
            } else {
                $condition = array('primary' => 'Yes', 'user_id' => $UserId);
                $checkPrimary = $this->mobile_model->get_all_details(SHIPPING_ADDRESS, $condition);
                if ($checkPrimary->num_rows() == 0) {
                    $condition = array('id' => $shipID, 'user_id' => $UserId);
                    $dataArr = array('primary' => 'Yes');
                    $this->mobile_model->update_details(SHIPPING_ADDRESS, $dataArr, $condition);
                }
            }
        }
		echo json_encode($returnArr);
    }

	/*
	!! >>>>>>>>>>>> Add Product To Cart <<<<<<<<<<<< !!
	*/

	public function cartCheckout() {
		$returnArr['status'] = "0";
		$returnArr['response'] = "";
        $checkout_type = $_GET['checkout_type'];
		$userid = $_GET['UserId'];
		if($userid == ""){
			$returnArr['response'] = "UserId Null !!";
		}else{
			$this->data['userDetails'] = $this->cart_model->get_all_details(USERS, array('id' => $userid));
	        if (isset($checkout_type) && $checkout_type == 'giftpurchase') {
	            $paymentType = $this->input->post('giftpayment_method');
	            $paymentGateway = $this->cart_model->get_all_details(PAYMENT_GATEWAY, array('status' => 'Enable', 'id' => $paymentType));
	        } else {
			if ($_GET['ship_need'] > 0 && $_GET['Ship_address_val'] == '') {
				if ($this->lang->line('add_ship_addr_msg') != ''){
					$lg_err_msg = $this->lang->line('add_ship_addr_msg');
				}
				else{
					$lg_err_msg = 'Please Add the Shipping address';
					$returnArr['response'] = $lg_err_msg;
				}
	          }else {
	                if ($_GET['ship_need'] > 0) {
	                    $shipping_address = $this->cart_model->get_all_details(SHIPPING_ADDRESS, array('id' => $_GET['Ship_address_val']));
	                    $userid = $_GET['UserId'];
	                    if ($this->data['userDetails']->row()->postal_code == '' || $this->data['userDetails']->row()->postal_code == 0) {
	                        $this->cart_model->update_details(USERS, array('postal_code' => $shipping_address->row()->postal_code), array('id' => $userid));
	                    }
	                    if ($this->data['userDetails']->row()->address == '') {
	                        $this->cart_model->update_details(USERS, array('address' => $shipping_address->row()->address1), array('id' => $userid));
	                    }
	                    if ($this->data['userDetails']->row()->address2 == '') {
	                        $this->cart_model->update_details(USERS, array('address2' => $shipping_address->row()->address2), array('id' => $userid));
	                    }
	                    if ($this->data['userDetails']->row()->city == '') {
	                        $this->cart_model->update_details(USERS, array('city' => $shipping_address->row()->city), array('id' => $userid));
	                    }
	                    if ($this->data['userDetails']->row()->state == '') {
	                        $this->cart_model->update_details(USERS, array('state' => $shipping_address->row()->state), array('id' => $userid));
	                    }
	                    if ($this->data['userDetails']->row()->country == '') {
	                        $this->cart_model->update_details(USERS, array('country' => $shipping_address->row()->country), array('id' => $userid));
	                    }
	                    if ($this->data['userDetails']->row()->phone_no == '') {
	                        $this->cart_model->update_details(USERS, array('phone_no' => $shipping_address->row()->phone), array('id' => $userid));
	                    }
	                }
	                $paymentType = $_GET['paymentId'];
	                /*                 * **********Payment Methods************ */
	                $paymentGateway = $this->cart_model->get_all_details(PAYMENT_GATEWAY, array('status' => 'Enable', 'id' => $paymentType));
	                /*                 * **********End************ */
	                $dealcodeNumber = $this->mobile_model->addPaymentCart($userid, $paymentGateway->row()->gateway_name, $this->input->get('ship_products'));
					$returnUrlDate[] = array(
						'UserId' => $userid,
						'paymentId' => $paymentType,
						'invoiceNumber' => $dealcodeNumber,
						'AppWebView' => "1"
					);
 					$returnArr['status'] = "1";
					$message = "Success";
					if($this->lang->line('json_success') != ""){
						$message = stripslashes($this->lang->line('json_success'));
					}
					$returnArr['response'] = $message;
					$returnArr['UrlData'] = $returnUrlDate;
					redirect('json/checkout/cart?UserId='.$userid.'&paymentId='.$paymentType.'&invoiceNumber='.$dealcodeNumber.'&AppWebView=1');
	            }
	        }
		}
	//	echo json_encode($returnArr);
    }


	/*
	!! >>>>>>>>>>>> Add Product To Cart <<<<<<<<<<<< !!
	*/
	public function addProductsToCart() {
		$returnArr['status_code'] = 0;
		$returnArr['response'] = '';
		$excludeArr = array('addtocart', 'attr_color', 'totalQuantity','UserId');
		$dataArrVal = array();
		$UserId = $_POST['UserId'];
		$mqty = $this->input->post('totalQuantity');
		foreach ($this->input->post() as $key => $val) {
			if (!(in_array($key, $excludeArr))) {
				$dataArrVal[$key] = trim(addslashes($val));
			}
		}
		$datestring = date('Y-m-d H:i:s', now());
		$indTotal = ( $this->input->post('price') + $this->input->post('product_shipping_cost') + ($this->input->post('price') * 0.01 * $this->input->post('product_tax_cost')) ) * $this->input->post('quantity');
		$dataArry_data = array('created' => $datestring, 'user_id' => $UserId, 'indtotal' => $indTotal, 'total' => $indTotal);
		$dataArr = array_merge($dataArrVal, $dataArry_data);
		$condition = '';
		$this->data['productVal'] = $this->cart_model->get_all_details(SHOPPING_CART, array('user_id' => $UserId, 'product_id' => $this->input->post('product_id'), 'attribute_values' => $this->input->post('attribute_values')));
		$for_qty_check = $this->cart_model->get_all_details(SHOPPING_CART, array('user_id' => $UserId, 'product_id' => $this->input->post('product_id')));
		if ($for_qty_check->num_rows > 0) {
			$new_tot_qty = 0;
			foreach ($for_qty_check->result() as $for_qty_check_row) {
				$new_tot_qty += $for_qty_check_row->quantity;
			}
			$new_tot_qty += $this->input->post('quantity');
			if ($new_tot_qty <= $mqty) {
				if ($this->data['productVal']->num_rows > 0) {
					$newQty = $this->data['productVal']->row()->quantity + $this->input->post('quantity');
					$indTotal = ( $this->input->post('price') + $this->input->post('product_shipping_cost') + ($this->input->post('price') * 0.01 * $this->input->post('product_tax_cost')) ) * $newQty;
					$dataArr = array('quantity' => $newQty, 'indtotal' => $indTotal, 'total' => $indTotal);
					$condition = array('id' => $this->data['productVal']->row()->id);
					$this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
				} else {
					$this->cart_model->commonInsertUpdate(SHOPPING_CART, 'insert', $excludeArr, $dataArr, $condition);
				}
			} else {
				$cart_qty = $new_tot_qty - $this->input->post('quantity');
				//echo 'Error|' . $cart_qty;
				//$returnArr['response'] = 'Error, Inavlid Quantity'.$cart_qty;
				$returnArr['response'] = "Maximum Quantity: ".$mqty.'. Already in your cart: '.$cart_qty;
				echo json_encode($returnArr);
				die;
			}
		} else {
			$this->cart_model->commonInsertUpdate(SHOPPING_CART, 'insert', $excludeArr, $dataArr, $condition);
		}
		if ($this->input->post('UserId') != '') {
			$mini_cart_lg = array();
			if ($this->lang->line('items') != '')
				$mini_cart_lg['lg_items'] = stripslashes($this->lang->line('items'));
			else
				$mini_cart_lg['lg_items'] = "items";
			if ($this->lang->line('header_description') != '')
				$mini_cart_lg['lg_description'] = stripslashes($this->lang->line('header_description'));
			else
				$mini_cart_lg['lg_description'] = "Description";
			if ($this->lang->line('qty') != '')
				$mini_cart_lg['lg_qty'] = stripslashes($this->lang->line('qty'));
			else
				$mini_cart_lg['lg_qty'] = "Qty";
			if ($this->lang->line('giftcard_price') != '')
				$mini_cart_lg['lg_price'] = stripslashes($this->lang->line('giftcard_price'));
			else
				$mini_cart_lg['lg_price'] = "Price";
			if ($this->lang->line('order_sub_total') != '')
				$mini_cart_lg['lg_sub_tot'] = stripslashes($this->lang->line('order_sub_total'));
			else
				$mini_cart_lg['lg_sub_tot'] = "Order Sub Total";
			if ($this->lang->line('proceed_to_checkout') != '')
				$mini_cart_lg['lg_proceed'] = stripslashes($this->lang->line('proceed_to_checkout'));
			else
				$mini_cart_lg['lg_proceed'] = "Proceed to Checkout";

			/**  Mini cart Lg  **/
			//echo 'Success|' . $this->cart_model->mini_cart_view($UserId, $mini_cart_lg);
			$returnArr['status_code'] = 1;
			$returnArr['response'] = 'Successfully Added To Cart';
		}else {
			$returnArr['response'] = 'UserId Null';
			//echo 'login|lgoin';
		}
		echo json_encode($returnArr);
	}

	/*
	||  >>>>>>>>>>>> Check Coupen Code <<<<<<<<<<<< ||
	*/

	public function CheckCoupenCode() {
		$returnArr['status_code'] = 0;
		$returnArr['response'] = '';
		$UserId = $this->input->post('UserId');
        $Code = $this->input->post('code');
		$cartAmount = $this->input->post('cartAmount');
        $amount = $this->input->post('totalAmount');
        $shipamount = $this->input->post('shipAmount');
		$taxAmount = $this->input->post('taxAmount');
		if($cartAmount > 0){
			if($Code != ""){
					if($amount != "" && $shipamount != "" && $UserId !="" && $taxAmount !=""){
						$response = $this->mobile_model->Check_Code_Val($Code, $amount, $shipamount, $UserId);
						$returnArr = explode('|',$response);
						if ($response == 1) {
							$returnArr['response'] = 'Entered code is invalid';
						} else if ($response == 2) {
							$returnArr['response'] = 'Code is already used';
						} else if ($response == 3) {
							$returnArr['response'] = 'Please add more items in the cart and enter the coupon code';
						} else if ($response == 4) {
							$returnArr['response'] = 'Entered Coupon code is not valid for this product';
						} else if ($response == 5) {
							$returnArr['response'] = 'Entered Coupon code is expired';
						} else if ($response == 6) {
							$returnArr['response'] = 'Entered code is Not Valid';
						} else if ($response == 7) {
							$returnArr['response'] = 'Please add more items quantity in the particular category or product, for using this coupon code';
						} else if ($response == 8) {
							$returnArr['response'] = 'Entered Gift code is expired';
						} else if ($returnArr[0] == 'Success') {
						    $CartVal = $this->mobile_model->mani_cart_coupon_sucess($UserId,$taxAmount,$shipamount,$cartAmount);
							$discountedCartAmt = explode('|',$CartVal);
							$coupenType = "";
							if(!empty($returnArr[2])) $coupenType = $returnArr[2] ;
							$priceDetails=array(
															'cart_amount' => $discountedCartAmt[0],
															'cart_ship_amount' => $discountedCartAmt[1],
															'cart_tax_amount' => $discountedCartAmt[2],
															'cart_total_amount' => $discountedCartAmt[3],
															'discount_Amt' => $discountedCartAmt[4],
															'ItemTotal' => $discountedCartAmt[0],
															'Shipping' => $discountedCartAmt[1],
															'Tax' => $discountedCartAmt[2],
															'GrantAmount' => $discountedCartAmt[3],
															'Discount' => $discountedCartAmt[4],
															'CouponCode' => $Code,
															'Coupon_id' => $returnArr[1],
															'couponType' => $coupenType
													);
							$returnArr['status_code'] = 1;
							$message = "Success";
							if($this->lang->line('json_success') != ""){
								$message = stripslashes($this->lang->line('json_success'));
							}
							$returnArr['response'] = $message;
							$returnArr['priceDetails'] = $priceDetails;
						}
				}else{
					$returnArr['response'] = 'Some Parameters are missing !!';
				}
			}else{
				$returnArr['response'] = 'Please Enter Code';
			}
		}else{
			$returnArr['response'] = 'Please add items in cart and enter the coupon code';
		}
		echo json_encode($returnArr);
    }

	/*
	||  >>>>>>>>>>>> Remove Coupen Code <<<<<<<<<<<< ||
	*/

	public function removeCoupenCode() {
		$returnArr['status_code'] = 0;
		$returnArr['response'] = '';
		$UserId = $this->input->post('UserId');
		if($UserId != ""){
			$Code = $this->input->post('code');
			$cartAmount = $this->input->post('cartAmount');
	        $amount = $this->input->post('totalAmount');
	        $shipamount = $this->input->post('shipAmount');
			$taxAmount = $this->input->post('taxAmount');
			if($amount != "" && $shipamount != "" && $UserId !="" && $taxAmount !="" && $Code != ""){
				$this->mobile_model->Check_Code_Val_Remove($UserId);
		    //    $CartVal = $this->cart_model->mani_cart_coupon_sucess($UserId);
				$CartVal = $this->mobile_model->mani_cart_coupon_sucess($UserId,$taxAmount,$shipamount,$cartAmount);
				$discountedCartAmt = explode('|',$CartVal);
				$coupenType = "";
				$priceDetails=array(
												'cart_amount' => $discountedCartAmt[0],
												'cart_ship_amount' => $discountedCartAmt[1],
												'cart_tax_amount' => $discountedCartAmt[2],
												'cart_total_amount' => $discountedCartAmt[3],
												'discount_Amt' => $discountedCartAmt[4],
												'ItemTotal' => $discountedCartAmt[0],
												'Shipping' => $discountedCartAmt[1],
												'Tax' => $discountedCartAmt[2],
												'GrantAmount' => $discountedCartAmt[3],
												'Discount' => $discountedCartAmt[4],
												'CouponCode' => $Code,
												'Coupon_id' => '0',
												'couponType' => $coupenType
										);
				$returnArr['status_code'] = 1;
				$message = "Success";
			if($this->lang->line('json_success') != ""){
				$message = stripslashes($this->lang->line('json_success'));
			}
			$returnArr['response'] = $message;
				$returnArr['priceDetails'] = $priceDetails;
			}else{
				$returnArr['response'] = 'Some Parameters are missing !!';
			}
		}else{
			$returnArr['response'] = 'UserId Null !!';
		}
		echo json_encode($returnArr);
	}

	/*
	||  >>>>>>>>>>>> User purchases <<<<<<<<<<<< ||
	*/
	public function userPurchases() {
		$returnArr['status_code'] = 0;
		$returnArr['response'] = '';
		$UserId = $_GET['UserId'];
		if($UserId != " "){
			$user_Detail = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
			$purchasesList = $this->mobile_model->get_purchase_details($UserId);
			$purchaseLists = array();
			foreach($purchasesList->result() as $purchase){
				$purchaseLists[]=array(
													'id'=> $purchase->id,
													'UserGroup' => $user_Detail->row()->group,
													'product_id'=>$purchase->product_id,
													'sell_id'=>$purchase->sell_id,
													'user_id'=>$purchase->user_id,
													'invoice' => $purchase->dealCodeNumber,
													'orderDate'=>$purchase->modified,
													'paymentType'=>$purchase->payment_type,
													'paymentStatus'=>$purchase->status,
													'total'=>$purchase->total,
													'shippingStatus'=>$purchase->shipping_status,
											);
			}
			if ($this->lang->line('purchases__invoice') != '') {
				$value1 = stripslashes($this->lang->line('purchases__invoice'));
			} else{
				$value1 = "Invoice";
			}
			if ($this->lang->line('purchases__paystatus') != '') {
				$value2 = stripslashes($this->lang->line('purchases__paystatus'));
			} else{
				$value2 = "Payment Status";
			}
			if ($this->lang->line('purchases_shipstatus') != '') {
				$value3 = stripslashes($this->lang->line('purchases_shipstatus'));
			} else{
				$value3 = "Shipping Status";
			}
			if ($this->lang->line('purchases_total') != '') {
				$value4 = stripslashes($this->lang->line('purchases_total'));
			} else{
				$value4 = "Total";
			}
			if ($this->lang->line('purchases_orddate') != '') {
				$value5 = stripslashes($this->lang->line('purchases_orddate'));
			} else{
				$value5 = "Order-Date";
			}
			if ($this->lang->line('purchases_option') != '') {
				$value6 = stripslashes($this->lang->line('purchases_option'));
			} else{
				$value6 = "Option";
			}
			$purchaseListsTableHead[] = array(
																'value1' => $value1,
																'value2' => $value2,
																'value3' => $value3,
																'value4' => $value4,
																'value5' => $value5,
																'value6' => $value6
															);
			$returnArr['status_code'] = 1;
			$message = "Success";
			if($this->lang->line('json_success') != ""){
				$message = stripslashes($this->lang->line('json_success'));
			}
			$returnArr['response'] = $message;
			$returnArr['purchaseListsTableHead'] = $purchaseListsTableHead;
			$returnArr['purchaseLists'] = $purchaseLists;
		}else{
			$returnArr['response'] = 'UserId Null';
		}
		echo json_encode($returnArr);
	}

	/*
	||  >>>>>>>>>>>> User purchases <<<<<<<<<<<< ||
	*/
	public function userProfileSettings() {
		$returnArr['status_code'] = 0;
		$returnArr['response'] = '';
		$UserId = $_GET['UserId'];
		if($UserId != ""){
			$user_Detail = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
			$UserDetails = array();
			if($user_Detail->num_rows() > 0){
				$userImg =  base_url().'images/users/user-thumb1.png';
				if ($user_Detail->row()->thumbnail != ''){
					$userImg = base_url().'images/users/'.$user_Detail->row()->thumbnail;
				}
				$UserDetails[] = array(
											'id' => $user_Detail->row()->id,
											'full_name' => $user_Detail->row()->full_name,
											'user_name' => $user_Detail->row()->user_name,
											'web_url' => $user_Detail->row()->web_url,
											'location' => $user_Detail->row()->location,
											'twitter' => $user_Detail->row()->twitter,
											'facebook' => $user_Detail->row()->facebook,
											'google' => $user_Detail->row()->google,
											'birthday' => $user_Detail->row()->birthday,
											'about' => $user_Detail->row()->about,
											'email' => $user_Detail->row()->email,
											'age' => $user_Detail->row()->age,
											'gender' => $user_Detail->row()->gender,
											'userImg' => $userImg,
											);
				$returnArr['status_code'] = 1;
				$message = "Success";
			if($this->lang->line('json_success') != ""){
				$message = stripslashes($this->lang->line('json_success'));
			}
			$returnArr['response'] = $message;
				$returnArr['UserDetails'] = $UserDetails;
			}else{
				$returnArr['response'] = 'User Not Found !! ';
			}
		}else{
			$returnArr['response'] = 'UserId Null !! ';
		}
		echo json_encode($returnArr);
	}

	/*
	* !! >>>>>>>>>>>>> This Function to Upload User Product Picture    <<<<<<<<<<<< !!
	*/
	public function UploadUserImage(){
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = $_POST['UserId'];
		if($UserId !=''){
			if (!empty($_POST['UserProfImage'])) {
				$imgPath ='images/users/temp/';
				$img = $_POST['UserProfImage'];
				$imgName = time(). ".jpg";
				$imageFormat = array('data:image/jpeg;base64', 'data:image/png;base64', 'data:image/jpg;base64', 'data:image/gif;base64');
				$img = str_replace($imageFormat, '', $img);
				$data = base64_decode($img);
				$image = @imagecreatefromstring($data);
				if ($image !== false) {
					$uploadPath = $imgPath . $imgName;
					imagejpeg($image, $uploadPath, 100);
					imagedestroy($image);
				} else {
					$message = "An error occurred";
							if($this->lang->line('json_error_occured') != ""){
							    $message = stripslashes($this->lang->line('json_error_occured'));
							}
							$returnArr['response'] = $message;
				}
				if (isset($uploadPath)) {
					/* Creating Thumbnail image with the size of 100 X 100 */
					$filename = base_url($uploadPath);
					list($width, $height) = getimagesize($filename);
					$newwidth = 100;
					$newheight = 100;
					//$thumbImage = @imagecreatetruecolor($newwidth, $newheight);
					$sourceImage = @imagecreatefromjpeg($filename);
					imagecopyresized($thumbImage, $sourceImage, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
					// if ($thumbImage !== false) {
					// 	/* Not uploading to thumb folder */
					// 	$thumbImgPath = 'images/users/thumb/';
					// 	$thumpUploadPath = $thumbImgPath . $imgName;
					// 	imagejpeg($thumbImage, $thumpUploadPath, 100);
					// }
				}else {
					$message = "Invalid File";
							if($this->lang->line('json_invalid_file') != ""){
							    $message = stripslashes($this->lang->line('json_invalid_file'));
							}
							$returnArr['response'] = $message;
				}
			$new_name = $imgName;
			@copy("./images/users/temp/".$imgName, './images/users/'.$new_name);
			@copy("./images/users/temp/".$imgName, './images/users/thumb/'.$new_name);
		//	$this->imageResizeWithSpace(200, 200, $new_name, './images/users/thumb/');
		//	$this->imageResizeWithSpace(600, 600, $new_name, './images/users/');
			$dataArr = array('thumbnail'=> $new_name);
			$condition = array('id' => $UserId);
			$this->user_model->update_details(USERS, $dataArr, $condition);
			$dir = getcwd().DIRECTORY_SEPARATOR ."images".DIRECTORY_SEPARATOR ."users".DIRECTORY_SEPARATOR."temp".DIRECTORY_SEPARATOR ;
			$interval = strtotime('-24 hours');
			foreach (glob($dir."*.*") as $file){
				if (filemtime($file) <= $interval ){
					if(!is_dir($file) ){
						unlink($file);
					}
				}
			}
			$returnArr['status'] = '1';
			$returnArr['image'] = base_url().'images/users/'.$imgName;
			$returnArr['response'] = $imgName;
			}else{
				$returnArr['response'] = "Please Upload Image";
			}
		}else{
			$returnArr['response'] = 'UserId Null !!';
		}
		$json_encode = json_encode($returnArr, JSON_PRETTY_PRINT);
		echo $json_encode;
	}

	/*
	* !! >>>>>>>>>>>>> This Function to Update User Profile    <<<<<<<<<<<< !!
	*/

	public function updateUserProfile() {
        $inputArr = array();
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = $_POST['UserId'];
		if ($UserId == '') {
            $returnArr['response'] = 'UserId Null !!';
        } else {
			$update = '0';
            $email = $this->input->post('email');
            if ($email != '') {
                if (valid_email($email)) {
                    $condition = array('email' => $email, 'id !=' => $UserId);
                    $duplicateMail = $this->user_model->get_all_details(USERS, $condition);
                    if ($duplicateMail->num_rows() > 0) {
                        $returnArr['response'] = 'Email already exists';
                    } else {
                        $inputArr['email'] = $email;
                        $update = '1';
                    }
                } else {
                    $returnArr['response'] = 'Invalid email';
                }
            } else {
                $update = '1';
            }
            if ($update == '1') {
            //    $birthday = $this->input->post('b_year') . '-' . $this->input->post('b_month') . '-' . $this->input->post('b_day');
				$birthday = $this->input->post('birthday');
                $excludeArr = array('birthday', 'email','UserId');
                $inputArr['birthday'] = $birthday;
                $condition = array('id' => $UserId);
                $this->user_model->commonInsertUpdate(USERS, 'update', $excludeArr, $inputArr, $condition);
                $this->setErrorMessage('success', $lg_err_msg);
                $returnArr['status'] = '1';
				$returnArr['response'] = 'Profile Updated Successfully';
            }
        }
		echo json_encode($returnArr);
    }


	/*
	* !! >>>>>>>>>>>>>  User Preferencs    <<<<<<<<<<<< !!
	*/
	public function userPreferencesSettings() {
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = $_POST['UserId'];
        if ($UserId== ''){
        	$returnArr['response'] = 'UserId Null !!';
        }else{
			$user_Detail = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
			$activeLangList = array();
			foreach($this->data['activeLgs'] as $actLangs){
				if(strtolower($actLangs['lang_code']) == strtolower($user_Detail->row()->language)){
					$languageName = $actLangs['name'];
				}
				$selected = "No";
				if(strtolower($user_Detail->row()->language) == strtolower($actLangs['lang_code'])){
					$selected = "Yes";
				}
				$activeLangList[] = array(
													'id' => $actLangs['id'],
													'name' => $actLangs['name'],
													'langCode' => strtolower($actLangs['lang_code']),
													'default' => $actLangs['default'],
													'country_code' => $actLangs['country_code'],
													'selected' => $selected
												);
			}
			$UserPreference[] = array(
												'languageCode'=> $user_Detail->row()->language,
												'languageName' =>$languageName,
												'visibility' => $user_Detail->row()->visibility,
												'displayLists' => $user_Detail->row()->display_lists,
												);
			$returnArr['status'] = '1';
			$message = "Success";
			if($this->lang->line('json_success') != ""){
				$message = stripslashes($this->lang->line('json_success'));
			}
			$returnArr['response'] = $message;
			$returnArr['UserPreference'] = $UserPreference;
			$returnArr['ActiveLanguagesLists'] = $activeLangList;
        }
		echo json_encode($returnArr);
    }

	/*
	* !! >>>>>>>>>>>>>  Update User Preferencs    <<<<<<<<<<<< !!
	*/
	public function updateUserPreferences() {
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = $_POST['UserId'];
        if ($UserId== '') {
        	$returnArr['response'] = 'UserId Null !!';
        } else {
		$excludeArr = array('UserId');
		$this->user_model->commonInsertUpdate(USERS, 'update', $excludeArr, array(), array('id' => $UserId));
		$returnArr['status'] = '1';
		$message = "Success";
			if($this->lang->line('json_success') != ""){
				$message = stripslashes($this->lang->line('json_success'));
			}
			$returnArr['response'] = $message;
        }
		echo json_encode($returnArr);
    }

	/*
	* !! >>>>>>>>>>>>>  Update User Password    <<<<<<<<<<<< !!
	*/
	public function changeUserPassword() {
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = $_POST['UserId'];
        if ($UserId == '') {
            $returnArr['response'] = 'UserId Null !!';
        } else {
			$userDetails = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
			$currentpassword = $_POST['currentpassword'];
			if($userDetails->row()->password != md5($currentpassword)){
				if ($this->lang->line('chck_crnt_pwd') != ''){
					$lg_err_msg = $this->lang->line('chck_crnt_pwd');
				}
				else{
					$lg_err_msg = 'Current Password is Incorrect';
				}
				$returnArr['response'] = $lg_err_msg;
			}else{
	            $pwd = $_POST['pass'];
	            $cfmpwd = $_POST['confirmpass'];
	            if ($pwd != '' && $cfmpwd != '' && strlen($pwd) > 5) {
	                if ($pwd == $cfmpwd) {
	                    $dataArr = array('password' => md5($pwd));
	                    $condition = array('id' => $UserId);
	                    $this->user_model->update_details(USERS, $dataArr, $condition);
	                    if ($this->lang->line('pwd_cge_succ') != ''){
	                        $lg_err_msg = $this->lang->line('pwd_cge_succ');
					}else{
	                    	$lg_err_msg = 'Password changed successfully';
					}
					$returnArr['status'] = '1';
					$returnArr['response'] = $lg_err_msg;
	                }else {
	                    if ($this->lang->line('pwd_donot_match') != ''){
	                        $lg_err_msg = $this->lang->line('pwd_donot_match');
					}else{
	                        $lg_err_msg = 'Passwords does not match';
					}
					$returnArr['response'] = $lg_err_msg;
	                }
	            }else {
	               $lg_err_msg = 'Password Should Have Atleast Six Characters';
	               $returnArr['response'] = $lg_err_msg;
	            }
	        }
		}
		echo json_encode($returnArr);
    }


	/*
	* !! >>>>>>>>>>>>>  User Notifications    <<<<<<<<<<<< !!
	*/
	public function UserNotifications() {
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = $_GET['UserId'];
		$UserEmailNotifications  = array();
		$UserNotifications = array();
		$siteUpdates = array();
        if ($UserId== '') {
        	$returnArr['response'] = 'UserId Null !!';
        }else{
			$userDetails = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
			$email_Notifications = explode(',', $userDetails->row()->email_notifications);
			$emailNotfication = array();
			$liked = LIKED_BUTTON;
			if (is_array($email_Notifications)) {
				$emailNotifications = $email_Notifications;
			}
		 	if($this->lang->line('notify_some_follu') != '') {
				$emailFollow = stripslashes($this->lang->line('notify_some_follu'));
			} else{
				$emailFollow = "When someone follows you";
			}
			if ($this->lang->line('notify_comm_things') != '') {
				$emailLiked = stripslashes($this->lang->line('notify_comm_things')).' '.$liked;
			} else{
				$emailLiked = "When someone comments on a thing you ".' '.$liked ;
			}
			if ($this->lang->line('notify_thing_feature') != '') {
				$emailNoti = stripslashes($this->lang->line('notify_thing_feature'));
			} else{
				$emailNoti = "When one of your things is featured";
			}
			if ($this->lang->line('cmt_on_ur_thing') != '') {
				$emailComts =  stripslashes($this->lang->line('cmt_on_ur_thing'));
			} else{
				$emailComts =  "When someone comments on your thing";
			}
			if ($this->lang->line('cmt_on_ur_thing') != '') {
				$emailDash =  stripslashes($this->lang->line('cmt_on_ur_thing_seller_dashboard'));
			} else{
				$emailDash =  "When someone comments on your seller dashboard";
			}
			$UserNotifications['review-comments'] = $emailDash;
			$UserNotifications['comments'] = $emailComts;
			$UserNotifications['following'] = $emailFollow;
			$UserNotifications['comments_on_fancyd'] = $emailLiked;
			$UserNotifications['featured'] = $emailNoti;
			foreach($UserNotifications as $key => $value){
				$checked = 'off';
				if (in_array($key, $emailNotifications)) $checked = 'on';
					$UserEmailNotifications[]=array(
																		'title' => $UserNotifications[$key],
																		'key' => $key,
																		'status' => $checked
																	);
			}
			$notificationArray = array();
			$noty = explode(',', $userDetails->row()->notifications);
			if (is_array($noty)) {
				$notifications = $noty;
			}
			if ($this->lang->line('notify_some_follu') != '') {
			   $emailFollow = stripslashes($this->lang->line('notify_some_follu'));
		   } else{
			   $emailFollow = "When someone follows you";
		   }
		   if ($this->lang->line('notify_comm_things') != '') {
			   $noti_commts = stripslashes($this->lang->line('notify_comm_things')).' '.LIKED_BUTTON;
		   } else{
			   $noti_commts = "When someone comments on a thing you".' '.LIKED_BUTTON;
		   }
		   if ($this->lang->line('notify_when_some') != '') {
			   $notiFancy = stripslashes($this->lang->line('notify_when_some')).' '.LIKE_BUTTON.' '.stripslashes($this->lang->line('notify_ur_thing'));
		   } else{
			   $notiFancy = "When someone".' '.LIKE_BUTTON.' '. "one of your things";
		   }
		   if ($this->lang->line('notify_thing_feature') != '') {
			   $noitFea = stripslashes($this->lang->line('notify_thing_feature'));
		   } else{
			   $noitFea = "When one of your things is featured";
		   }
		   if ($this->lang->line('cmt_on_ur_thing') != '') {
			   $notiComts = stripslashes($this->lang->line('cmt_on_ur_thing'));
		   } else{
			   $notiComts = "When someone comments on your thing";
		   }
		   if ($this->lang->line('cmt_on_seller_discussion') != '') {
			   $notiRevCmts = stripslashes($this->lang->line('cmt_on_seller_discussion'));
		   } else{
			   $notiRevCmts = "When someone comments on a product's seller discussion";
		   }
		   if ($this->lang->line('cmt_on_seller_purchase') != '') {
			   $notiPurchse = stripslashes($this->lang->line('cmt_on_seller_purchase'));
		   } else{
			   $notiPurchse = "When someone Purchase your product";
		   }
			$notificationArray['wmn-follow'] = $emailFollow;
			$notificationArray['wmn-comments_on_fancyd'] = $noti_commts;
			$notificationArray['wmn-fancyd'] = $notiFancy;
			$notificationArray['wmn-featured'] = $noitFea;
			$notificationArray['wmn-comments'] = $notiComts;
			$notificationArray['wmn-review-comments'] = $notiRevCmts;
			$notificationArray['wmn-purchased'] = $notiPurchse;
			foreach($notificationArray as $key => $value){
				$checked = 'off';
				if (in_array($key, $notifications)) $checked = 'on';
					$User_Notifications[]=array(
																		'title' => $notificationArray[$key],
																		'key' => $key,
																		'status' => $checked
																	);
			}
			if ($this->lang->line('notify_send_news') != '') {
				$sndUp = stripslashes($this->lang->line('notify_send_news')).' '.$this->data['siteTitle'];
			} else{
				$sndUp = "Send news about".' '.$this->data['siteTitle'];
			}
			$siteUpdatesNews = 'off';
			if ($userDetails->row()->updates == '1')$siteUpdatesNews = "on";
				$siteUpdates[] = array(
													'title' => $sndUp,
													'key' => 'updates',
													'status' => $siteUpdatesNews
											);
			$returnArr['status'] = '1';
			$message = "Success";
			if($this->lang->line('json_success') != ""){
				$message = stripslashes($this->lang->line('json_success'));
			}
			$returnArr['response'] = $message;
		}
		$returnArr['Email'] = $UserEmailNotifications;
		$returnArr['Notifications'] = $User_Notifications;
		$returnArr['Updates'] = $siteUpdates;
		echo json_encode($returnArr);
    }

	/*
	* !! >>>>>>>>>>>>>  Update User Notifications    <<<<<<<<<<<< !!
	*/
	public function updateUserNotifications() {
		$returnArr['status'] = '0';
		$returnArr['response'] = '';
		$UserId = $_POST['UserId'];
        if ($UserId == '') {
            $returnArr['response'] = 'UserId Null !!';
        } else {
			$this->data['userDetails'] = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
			$email = $_POST['email'];
			$notification = $_POST['notification'];
			$emailStr = $email;
		 	$notyStr = $notification;
            $updates = $_POST['updates'];
            $updates = ($updates == '') ? '0' : '1';
            $dataArr = array(
                'email_notifications' => $emailStr,
                'notifications' => $notyStr,
                'updates' => $updates
            );
            $condition = array('id' => $UserId);
            $this->user_model->update_details(USERS, $dataArr, $condition);
            if ($updates == 1) {
                $checkEmail = $this->user_model->get_all_details(SUBSCRIBERS_LIST, array('subscrip_mail' => $this->data['userDetails']->row()->email));
                if ($checkEmail->num_rows() == 0) {
                    $this->user_model->simple_insert(SUBSCRIBERS_LIST, array('subscrip_mail' => $this->data['userDetails']->row()->email, 'active' => 1, 'status' => 'Active'));
                }
            } else {
                $this->user_model->commonDelete(SUBSCRIBERS_LIST, array('subscrip_mail' => $this->data['userDetails']->row()->email));
            }
            if ($this->lang->line('noty_sav_succ') != ''){
                $lg_err_msg = $this->lang->line('noty_sav_succ');
			}else{
                $lg_err_msg = 'Notifications settings saved successfully';
			}
			$returnArr['status'] = '1';
			$returnArr['response'] = $lg_err_msg;
        }
		echo json_encode($returnArr);
    }

	/*
	* !! >>>>>>>>>>>>>  Manage FancyBox    <<<<<<<<<<<< !!
	*/

	public function manageFancyybox(){
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = $_GET['UserId'];
		if($UserId == ""){
			$returnArr['response'] = 'UserId Null !!';
		}else{
			$userDetails = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
	        $manageFancyybox = $this->mobile_model->get_subscriptions_list($UserId);
			if($manageFancyybox->num_rows() > 0){
					foreach( $manageFancyybox->result() as $subscribeRow){
			 		   $subscribeRowArr[]=array(
			 												   'name'=>$subscribeRow->name,
			 												   'created'=>$subscribeRow->created,
			 												   'total'=>$subscribeRow->total,
			 												   'invoice_no'=>$subscribeRow->invoice_no
			 											   );
			 	   }
			$returnArr['status'] = '1';
		   		$message = "Success";
			if($this->lang->line('json_success') != ""){
				$message = stripslashes($this->lang->line('json_success'));
			}
			$returnArr['response'] = $message;
			}else{
				$returnArr['response'] = 'No Subscriptions List Avaloable';
			}
			if($this->lang->line('purchases__invoice') != '') { $value1 =  stripslashes($this->lang->line('purchases__invoice')); } else $value1 =  "Invoice";
			if($this->lang->line('manage_subsname') != '') { $value2 =  stripslashes($this->lang->line('manage_subsname')); } else $value2 =  "Subscription Name";
			if($this->lang->line('purchases_total') != '') { $value3 =  stripslashes($this->lang->line('purchases_total')); } else $value3 =  "Total";
			if($this->lang->line('order_date') != '') { $value4 =  stripslashes($this->lang->line('order_date')); } else $value4 =  "Date";
			$subscribeTabelHead[] = array(
															'value1' => $value1,
															'value2' => $value2,
															'value3' => $value3,
															'value4' => $value4,
														);
			$returnArr['subscribeTabelHead'] = $subscribeTabelHead;
			$returnArr['subscribeList'] = $subscribeRowArr;
		}
		echo json_encode($returnArr);
    }

	/*
	* !! >>>>>>>>>>>>>  Shipping Settings    <<<<<<<<<<<< !!
	*/

	public function shippingSettings() {
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = $_GET['UserId'];
		if($UserId == ""){
			$returnArr['response'] = 'UserId Null !!';
		}else{
            $countryList = $this->mobile_model->get_all_details(COUNTRY_LIST, array(), array(array('field' => 'name', 'type' => 'asc')));
            $shippingList = $this->mobile_model->get_all_details(SHIPPING_ADDRESS, array('user_id' => $UserId));
            foreach( $shippingList->result() as $shipping){
			   $shippinglists[]=array(
			   									'id'=>$shipping->id,
											   'full_name'=>$shipping->full_name,
											   'nick_name'=>$shipping->nick_name,
											   'address1'=>$shipping->address1,
											   'address2'=>$shipping->address2,
											   'city'=>$shipping->city,
											   'district'=>$shipping->district,
											   'state'=>$shipping->state,
											   'country'=>$shipping->country,
											   'postal_code'=>$shipping->postal_code,
											   'phone'=>$shipping->phone,
											   'primary'=>$shipping->primary
										   );
		   }
		    if($this->lang->line('shipping_default') != '') { $value1 = stripslashes($this->lang->line('shipping_default')); } else $value1 = "Default";
		    if($this->lang->line('shipping_nickname') != '') { $value2 = stripslashes($this->lang->line('shipping_nickname')); } else $value2 = "Nick Name";
		    if($this->lang->line('shipping_address_comm') != '') { $value3 = stripslashes($this->lang->line('shipping_address_comm')); } else $value3 = "Address";
		    if($this->lang->line('shipping_phone') != '') { $value4 = stripslashes($this->lang->line('shipping_phone')); } else $value4 = "Phone";
		    if($this->lang->line('purchases_option') != '') { $value5 = stripslashes($this->lang->line('purchases_option')); } else $value5 = "Option";
			  $shippinglistsTableHead[]=array(
																  'value1' => $value1,
																  'value2' => $value2,
																  'value3' => $value3,
																  'value4' => $value4,
																  'value5' => $value5,
															  );
		   $returnArr['status'] = 1;
		   $message = "Success";
			if($this->lang->line('json_success') != ""){
				$message = stripslashes($this->lang->line('json_success'));
			}
			$returnArr['response'] = $message;
		   $returnArr['shippinglistsTableHead'] = $shippinglistsTableHead;
		   $returnArr['shippinglists'] = $shippinglists;
	   }
		echo json_encode($returnArr);
    }

	/*
	* !! >>>>>>>>>>>>>  Gift Cards <<<<<<<<<<<< !!
	*/
	public function userGiftcards() {
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId= $_GET['UserId'];
		$giftcardslists = array();
		if($UserId == ""){
			$returnArr['response'] = 'UserId Null !!';
		}else{
			$userDetails = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
			$giftcardsList =$this->data['giftcardsList'] = $this->user_model->get_gift_cards_list($userDetails->row()->email);
			 foreach( $giftcardsList->result() as $giftcards) {
				$status = $giftcards->card_status;
				if ($status == 'not used') {
					$expDate = strtotime($giftcards->expiry_date);
					if ($expDate < time()) {
						$status = 'expired';
					}
				}
			   $giftcardslists[]=array(
												   'code'=>$giftcards->code,
												   'recipient_name'=>$giftcards->recipient_name,
												   'sender_mail'=>$giftcards->sender_mail,
												   'sender_name'=>$giftcards->sender_name,
												   'price_value'=>$giftcards->price_value,
												   'expiry_date'=>$giftcards->expiry_date,
												   'card_status'=>ucwords($status)
											   );
		   }
			if ($this->lang->line('giftcard_code') != '') {
				$value1 = stripslashes($this->lang->line('giftcard_code'));
				} else
				$value1 = "Code";
		   if ($this->lang->line('giftcard_sendername') != '') {
			   $value2 = stripslashes($this->lang->line('giftcard_sendername'));
		   } else
			   $value2 = "Sender Name";
		   if ($this->lang->line('giftcard_sender_mail') != '') {
			   $value3 = stripslashes($this->lang->line('giftcard_sender_mail'));
		   } else
			   $value3 = "Sender Mail";
		   if ($this->lang->line('giftcard_price') != '') {
			   $value4 = stripslashes($this->lang->line('giftcard_price'));
		   } else
			   $value4 = "Price";
		   if ($this->lang->line('giftcard_expireson') != '') {
			   $value5 = stripslashes($this->lang->line('giftcard_expireson'));
		   } else
			   $value5 = "Expires on";
		   if ($this->lang->line('giftcard_card_stats') != '') {
			   $value6 = stripslashes($this->lang->line('giftcard_card_stats'));
		   } else
			   $value6 = "Card Status";
			$giftcardslistsTableHead[] = array(
														'value1' => $value1,
														'value2' => $value2,
														'value3' => $value3,
														'value4' => $value4,
														'value5' => $value5,
														'value6' => $value6
													);
		   $returnArr['status'] = 1;
		   $message = "Success";
			if($this->lang->line('json_success') != ""){
				$message = stripslashes($this->lang->line('json_success'));
			}
			$returnArr['response'] = $message;
		   $returnArr['giftcardslistsTableHead'] = $giftcardslistsTableHead;
		   $returnArr['giftcardslists'] = $giftcardslists;
	   }
		echo json_encode($returnArr);
	}

	/*
	* !! >>>>>>>>>>>>> Users Orders Lists <<<<<<<<<<<< !!
	*/
	public function userOrders() {
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = $_GET['UserId'];
		if($UserId != ""){
			$userDetails = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
			if($userDetails->row()->group != "User"){
				$usersOrderList = $this->user_model->get_user_orders_list($UserId);
				if($usersOrderList->num_rows() > 0){
					$usersOrders = array();
					$shippingStatusList= array();
					if ($this->lang->line('order_pending') != '') {
						$status1 =  stripslashes($this->lang->line('order_pending'));
					} else{
						$status1 =  "Pending";
					}
					if ($this->lang->line('order_processed') != '') {
						$status2 =  stripslashes($this->lang->line('order_processed'));
					} else{
						$status2 =  "Processed";
					}
					if ($this->lang->line('order_delivered') != '') {
						$status3 =  stripslashes($this->lang->line('order_delivered'));
					} else{
						$status3 =  "Delivered";
					}
					if ($this->lang->line('order_returnred') != '') {
						$status4 =  stripslashes($this->lang->line('order_returnred'));
					} else{
						$status4 =  "Returned";
					}
					if ($this->lang->line('order_reshipp') != '') {
						$status5 =  stripslashes($this->lang->line('order_reshipp'));
					} else{
						$status5 =  "Re-Shipped";
					}
					if ($this->lang->line('order_cancelled') != '') {
						$status6 =  stripslashes($this->lang->line('order_cancelled'));
					} else{
						$status6 =  "Cancelled";
					}
					$shippingStatusList	= array('status1' => $status1,'status2' => $status2,'status3' => $status3,'status4' => $status4,'status5' => $status5,'status6' => $status6);
					foreach($usersOrderList->result() as $ordersList){
						if ($this->lang->line('view_invoice') != '') {
							$Option1 = stripslashes($this->lang->line('view_invoice'));
						} else{
							$Option1 =  "View Invoice";
						}
						 if ($ordersList->status == 'Paid') {
								if ($this->lang->line('buyer_discuss') != '') {
									$Option2 = stripslashes($this->lang->line('buyer_discuss'));
								} else
									$Option2 = "Buyer Discussion";
						}else if($ordersList->status == 'Pending' && $ordersList->payment_type == 'Cash on Delivery' && $ordersList->sell_id == $UserId){
							if ($this->lang->line('buyer_pending') != '') {
								$Option2 = stripslashes($this->lang->line('buyer_pending'));
							} else{
								$Option2 = "Confirm payment";
							}
						}
						$usersOrders[] = array(
															'SellId' => $ordersList->sell_id,
															'Invoice' => $ordersList->dealCodeNumber,
															'PaymentStatus' => $ordersList->status,
															'ShippingStatus' => $ordersList->shipping_status,
															'TotalPrice' => $ordersList->TotalPrice,
															'Created' => $ordersList->created,
															'Option1' => $Option1,
															'Option2' => $Option2
														);
					}
					if ($this->lang->line('purchases__invoice') != '') {
						$value1 = stripslashes($this->lang->line('purchases__invoice'));
					} else{
						$value1 = "Invoice";
					}
					if ($this->lang->line('purchases__paystatus') != '') {
						$value2 = stripslashes($this->lang->line('purchases__paystatus'));
					} else{
						$value2 = "Payment Status";
					}
					if ($this->lang->line('purchases_shipstatus') != '') {
						$value3 = stripslashes($this->lang->line('purchases_shipstatus'));
					} else{
						$value3 = "Shipping Status";
					}
					if ($this->lang->line('purchases_orddate') != '') {
						$value4 = stripslashes($this->lang->line('order_date'));
					} else{
						$value4 = "Date";
					}
					if ($this->lang->line('purchases_option') != '') {
						$value5 = stripslashes($this->lang->line('purchases_option'));
					} else{
						$value5 = "Option";
					}
					$usersOrdersTableHead[] = array(
																		'value1' => $value1,
																		'value2' => $value2,
																		'value3' => $value3,
																		'value4' => $value4,
																		'value5' => $value5
																);
					$returnArr['status'] = '1';
					$returnArr['response'] = 'Success !!';
					$returnArr['shippingStatusList'] = $shippingStatusList;
					$returnArr['usersOrdersTableHead'] =$usersOrdersTableHead;
					$returnArr['usersOrders'] =$usersOrders;
				}else{
					$returnArr['response'] = 'No Order Details Avaliable !!';
				}
			}else{
				$returnArr['response'] = 'This User is not a Seller';
			}
		}else{
			$returnArr['response'] = 'UserId Null !!';
		}
		echo json_encode($returnArr);
    }

	/*
	* !! >>>>>>>>>>>>> Change Shipping Status <<<<<<<<<<<< !!
	*/
	public function changeShippingStatus() {
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = $_POST['UserId'];
		if ($UserId== '') {
            $returnArr['response'] = 'UserId Null !!';
        } else {
            $sellId = $_POST['sellId'];
			if($sellId == ""){
				$returnArr['response'] = 'SellId Null !!';
			}else{
				if ($sellId != $UserId) {
	                $returnArr['response'] = 'UserId & SellId Not Same !!';
	            } else {
	                $dealCode =$_POST['dealCode'];
	                $status = $_POST['ShippingStatus'];
					if($dealCode != "" && $status != ""){
						$dataArr = array('shipping_status' => $status);
		                $conditionArr = array('dealCodeNumber' => $dealCode, 'sell_id' => $sellId);
		                $this->user_model->update_details(PAYMENT, $dataArr, $conditionArr);
		                $returnArr['status'] = 1;
						$returnArr['response'] = 'Successfully Updated ';
					}else{
						$returnArr['response'] = 'Some Parameters are Missing !!';
					}
	            }
			}
        }
		echo json_encode($returnArr);
	}

	/*
	* !! >>>>>>>>>>>>> Update Buyer Status <<<<<<<<<<<< !!
	*/
	public function updateBuyerStatus() {
		$returnArr['status'] = (string)0;
		$returnArr['response'] = '';
		$user_id = $_POST['UserId'];
        if ($user_id == '') {
            $returnArr['response'] = 'UserId Null !!';
        } else {
            $seller_id = $_POST['SellId'];
            $invoice_id = $_POST['Invoice'];
			if($seller_id == "" || $invoice_id == ""){
				$returnArr['response'] = 'Some Paremters are Missing !!';
			}else{
				$Query = "select p.*,u.commision,u.cash_on_delivery from " . PAYMENT . " as p left join " . USERS . " as u on u.id=p.sell_id where p.dealCodeNumber = '" . $invoice_id . "'";
	            $seller_list = $this->user_model->ExecuteQuery($Query);
	            if ($seller_list->num_rows() > 0) {
	                $admin_commision = ($seller_list->row()->sumtotal * ($seller_list->row()->commision / 100));
	                $cod = $seller_list->row()->cash_on_delivery + $admin_commision;
	                $this->user_model->update_details(USERS, array('cash_on_delivery' => $cod), array('id' => $seller_id));
					$this->user_model->update_details(PAYMENT, array('status' => 'Paid'), array('user_id' => $user_id, 'sell_id' => $seller_id, 'dealCodeNumber' => $invoice_id));
					$returnArr['status'] = (string)1;
		            $returnArr['response'] = 'Success, Payment Confirmed';
	            }else{
					$returnArr['response'] = 'Invoice Number Not Found';
				}
			}
        }
		echo json_encode($returnArr);
    }

	/*
	* !! >>>>>>>>>>>>> Group Gifts <<<<<<<<<<<< !!
	*/
	public function GroupGifts() {
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$user_id = $_GET['UserId'];
		$groupGiftList = array();
		$groupGiftPayment = array();
		if ($user_id ==''){
			$returnArr['response'] = 'UserId Null !!';
		}else {
			$groupgiftList = $this->groupgift_model->get_groupgift_list($user_id);
			if($groupgiftList->num_rows() >0){
			   foreach($groupgiftList->result() as $_groupgiftlist){
					$current_date = date('m-d-Y');
					$date = $_groupgiftlist->created;
					$date = strtotime($date);
					$date = strtotime("+".$this->config->item('expiry_days')." day", $date);
					$expired_date = date('m-d-Y', $date);
					if($expired_date < $current_date && $_groupgiftlist->status=='Active'){
						$this->groupgift_model->update_details(GROUP_GIFTS,array('status'=>'Unsuccessful'),array('id'=>$_groupgiftlist->id));
					}
				   $this->data['contributeDetails'][$_groupgiftlist->id] = $this->groupgift_model->get_all_details(GROUPGIFT_PAYMENT,array('groupgift_id'=>$_groupgiftlist->id));
					if($_groupgiftlist->status == "Cancelled") {
					   $Status =  "Canceled(by creator)";
					} elseif($_groupgiftlist->status == "Cancelled Admin"){
						   $Status =  "Canceled(by admin)";
					}elseif($_groupgiftlist->status == "Unsuccessful"){
						  $Status =  $_groupgiftlist->status;
					}else{
						$Status =  $_groupgiftlist->status;
					}
					$date = $_groupgiftlist->created;
					$date = strtotime($date);
					$date = strtotime("+".$this->config->item('expiry_days')." day", $date);
					$endDate =  date('M d, Y, h:i', $date);
					 if($_groupgiftlist->status == "Cancelled" || $_groupgiftlist->status == "Cancelled Admin") {
						$Contribution = "Cancelled";
 			 		} elseif($_groupgiftlist->status == "Successful") {
						$Contribution = $_groupgiftlist->status ;
					 } elseif($_groupgiftlist->status == "Unsuccessful"){
						$Contribution = "Expired" ;
					 }else{
						$Contribution = "Pending";
					}
				   $groupGiftList[] = array(
					   										'SllerID'=> $_groupgiftlist->gift_seller_id,
														   'GiftName' => $_groupgiftlist->gift_name,
									   		   			   'Status' => $Status,
														   'EndDate' => $endDate,
														   'Contribution' => $Contribution
													   );
			   }
			}
			$groupgiftPayment = $this->groupgift_model->get_all_details(GROUPGIFT_PAYMENT,array('user_id'=>$user_id));
			if($groupgiftPayment->num_rows() > 0){
				foreach($groupgiftPayment->result() as $row){
					$groupGiftPayment[] = array(
																'GiftId' => $row->groupgift_id,
																'ContributorName' => $row->contributor_name,
																'PaymentType' => 'Paypal',
																'Amount' => $row->amount,
																'Transaction Id' => $row->transaction_id,
																'Status' =>  $row->status,
																'Created Date' => $row->created,
															);
				}
			}
			if($this->lang->line('groupgift_name') != ''){ $value1 = stripslashes($this->lang->line('groupgift_name')); } else $value1 = "Gift Name";
			if($this->lang->line('groupgift_status') != ''){ $value2 = stripslashes($this->lang->line('groupgift_status')); } else $value2 = "Status";
			if($this->lang->line('groupgift_end_date') != ''){ $value3 = stripslashes($this->lang->line('groupgift_end_date')); } else $value3 = "End Date";
			if($this->lang->line('groupgift_contribution') != ''){ $value4 = stripslashes($this->lang->line('groupgift_contribution')); } else $value4 = "Contribution";
			$groupGiftListTableHead[] = array(
																	'value1' => $value1,
																	'value2' => $value2,
																	'value3' => $value3,
																	'value4' => $value4,
																);
			 if($this->lang->line('groupgift_id') != '') { $value1 = stripslashes($this->lang->line('groupgift_id')); } else $value1 = "Gift Id";
			 if($this->lang->line('contributor_name') != '') { $value2 = stripslashes($this->lang->line('contributor_name')); } else $value2 = "Contributor Name";
			 if($this->lang->line('groupgift_payment_type') != '') { $value3 = stripslashes($this->lang->line('groupgift_payment_type')); } else $value3 = "Payment Type";
			 if($this->lang->line('groupgift_payment_amt') != '') { $value4 = stripslashes($this->lang->line('groupgift_payment_amt')); } else $value4 = "Amount";
			 if($this->lang->line('groupgift_payment_txid') != '') { $value5 = stripslashes($this->lang->line('groupgift_payment_txid')); } else $value5 = "Transaction Id";
			 if($this->lang->line('groupgift_status') != '') { $value6 = stripslashes($this->lang->line('groupgift_status')); } else $value6 = "Status";
			 if($this->lang->line('groupgift_payment_created') != '') { $value7 = stripslashes($this->lang->line('groupgift_payment_created')); } else $value7 = "Created Date";
			 $groupGiftPaymentTableHead[] = array(
		 																	'value1' => $value1,
		 																	'value2' => $value2,
		 																	'value3' => $value3,
		 																	'value4' => $value4,
																			'value5' => $value5,
																			'value6' => $value6,
																			'value7' => $value7,
		 																);

		$returnArr['status'] = '1';
			$message = "Success";
			if($this->lang->line('json_success') != ""){
				$message = stripslashes($this->lang->line('json_success'));
			}
			$returnArr['response'] = $message;
			$returnArr['groupGiftListTableHead'] = $groupGiftListTableHead;
			$returnArr['groupGiftList'] = $groupGiftList;
			$returnArr['groupGiftPaymentTableHead'] = $groupGiftPaymentTableHead;
			$returnArr['groupGiftPayment'] = $groupGiftPayment;
		}
	echo json_encode($returnArr);
	}

	/*
		|| >>>>>>>>>>>>> Seller Featured Plan <<<<<<<<<<<< ||
	*/
	// public function sellerFeaturedPlan() {
	// 	$returnArr['status'] = 0;
	// 	$returnArr['response'] = '';
	// 	$featuredDetails = array();
	// 	$featuredProductsDetails = array();
	// 	$UserId = intval($_GET['UserId']);
	// 	if($UserId != ""){
	// 		$this->load->model('seller_plan_model', 'seller_plan');
	//         $isFeatured = $this->seller_plan->get_all_details(FEATURED_SELLER, array('userId' => $UserId, 'isExpired' => '0'));
	//         if ($isFeatured->num_rows > 0) {
	// 			$featuredProducts = $this->seller_plan->get_featured_product($UserId, $isFeatured->row()->id);
	// 			$featuredDetails[] = array(
	// 														"FeaturedFrom" =>   $isFeatured->row()->featuredOn ,
	// 														"FeaturedTo" =>   $isFeatured->row()->expiresOn ,
	// 														"PlanPrice" =>     $currencySymbol.$isFeatured->row()->planPrice ,
	// 														"PlanPeriod" =>   $isFeatured->row()->planPeriod,
	// 														"MaxFeatured Product" => $isFeatured->row()->productCount
	// 												);
	// 			foreach($featuredProducts  as $prodRow){
	// 				$imgArr=$prodRow['image'];
	// 				$productImage = base_url().'images/product/'.$imgArr;
	// 				$featuredProductsDetails[]=array(
	// 																		'id'=>$prodRow['id'],
	// 																		'seller_product_id'=>$prodRow['seller_product_id'],
	// 																		'UserId' =>$prodRow['user_id'],
	// 																		'product_name'=>$prodRow['product_name'],
	// 																	//	'seourl'=>$prodRow['seourl'],
	// 																	//	'description'=>$prodRow['description'],
	// 																	//	'shippingPolicies'=>$prodRow['shipping_policies'],
	// 																		'image'=>$productImage
	// 																	//	'price'=>$prodRow['price'],
	// 																	//	'sale_price'=>$prodRow['sale_price'],
	// 																	//	'user_name'=>$prodRow['user_name'],
	// 																	//	'likes'=>$prodRow['likes'],
	// 																	//	'liked' => $likedStatus,
	// 																	//	'url'=>$prodRow['url'],
	// 																	//	'type' => $prodRow['type'],
	// 																	//	'userUrl' => 	$normalUserUrl = 'http://192.168.1.251:8081/product-working/fancyclone-v3/json/user/seller-timeline?username='.$prodRow['user_name']
	// 																		);
	// 			}
	// 		$returnArr['status'] = '1';
	// 			$message = "Success";
			// if($this->lang->line('json_success') != ""){
			// 	$message = stripslashes($this->lang->line('json_success'));
			// }
			// $returnArr['response'] = $message;
	// 			$returnArr['featuredPlanDetails'] = $featuredDetails;
	// 			$returnArr['featuredProductsDetails'] = $featuredProductsDetails;
	//         }else{
	// 			$returnArr['status'] = 'noFeaturedProducts';
	// 			$returnArr['response'] = '';
	// 		}
	//         $featuredSellerProducts = $this->product_model->view_product_details(' where p.user_id=' . $UserId . ' and p.status="Publish"');
	// 	}else{
	// 		$returnArr['response'] = 'UserId Null !!';
	// 	}
	// 	echo json_encode($returnArr);
    // }
	//
	// /*
	// 	|| >>>>>>>>>>>>> check Featured Seller <<<<<<<<<<<< ||
	// */
	// public function checkFeaturedSeller(){
	// 	$returnArr['status'] = 0;
	// 	$returnArr['response'] = '';
	// 	$UserId = intval($_GET['UserId']);
	// 	if($UserId != ""){
	// 		$this->load->model('seller_plan_model', 'seller_plan');
	//         $this->data['is_featured'] = $this->seller_plan->get_all_details(FEATURED_SELLER, array('userId' => $UserId, 'isExpired' => '0'));
	//         if ($this->data['is_featured']->num_rows == 0) {
	//             $this->data['plan'] = $this->seller_plan->get_all_details(SELLER_PLAN, array());
	//             $this->load->view('site/user/view_featured_plan', $this->data);
	//         } else {
	//             $this->setErrorMessage('danger', 'Already plan subscribed');
	//             redirect(base_url() . 'featured');
	//         }
	// 	}else{
	// 		$returnArr['response'] = 'UserId Null !!';
	// 	}
	// 	echo json_encode($returnArr);
	// }

	/*
	|| <<<<<<<<<<<<< Add Seller Product Details >>>>>>>>>>> ||
	*/
	public function addSellerProductDetail(){
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = intval($_GET['UserId']);
		$formFeildsName = array();
		$formTabs = array();
		$productDetails = array();
		$shippingCostDetail = array();
		$specificCountryShippingList = array();
		if($UserId == ""){
            $returnArr['response'] = 'UserId Null !!';
        } else {
			$userDetails = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
			$countryList= $this->mobile_model->get_all_details(COUNTRY_LIST, array(), array(array('field' => 'name', 'type' => 'asc')));
            $userType = $userDetails->row()->group;
            if ($userType == 'Seller') {
                $pid = $this->input->get('sellerProductId');
				$mode = $this->input->get('mode');
				if($pid == ""){
					$returnArr['response'] = 'Seller Product Id Null !!';
				}else{
					if($mode == 'edit'){
						$productDetails = $this->product_model->get_all_details(PRODUCT, array('seller_product_id' => $pid));
						$shippingCostDetails = $this->product_model->get_all_details(SHIPPING_COST, array('product_id' => $pid));
						if($shippingCostDetails->num_rows()>0){
							foreach ($shippingCostDetails->result() as $shippingCostRow){
								foreach ($countryList->result() as $row){
									if ($row->country_code == $shippingCostRow->country_code){
										$selected = "yes";
										$specificCountryShippingList[] = array(
											'selected' => $selected,
											'countryCode' => $row->country_code,
											'countryName' => $row->name,
											'shippingCost' => $shippingCostRow->separate_ship_cost
										);
									}
								}
							}
						}
					}else{
						$productDetails = $this->product_model->get_all_details(USER_PRODUCTS, array('seller_product_id' => $pid));
					}
	                if ($productDetails->num_rows() == 1) {
	                    if ($productDetails->row()->user_id == $userDetails->row()->id) {
	                        // $this->data['productDetails'] = $productDetails;
	                        $this->data['editmode'] = '0';
	                        $shippingCostDetails = $this->product_model->get_all_details(SHIPPING_COST, array('product_id' => $pid));
							$returnArr['status'] = '1';
							$message = "Success";
			if($this->lang->line('json_success') != ""){
				$message = stripslashes($this->lang->line('json_success'));
			}
			$returnArr['response'] = $message;
							if($this->lang->line('product_details') != '') { $value1 = stripslashes($this->lang->line('product_details')); } else $value1 =  "Details";
							if($this->lang->line('product_categories') != '') { $value2 = stripslashes($this->lang->line('product_categories')); } else $value2 =  "Categories";
							if($this->lang->line('display_lists') != '') { $value3 = stripslashes($this->lang->line('display_lists')); } else $value3 =  "List";
							if($this->lang->line('product_images') != '') { $value4 = stripslashes($this->lang->line('product_images')); } else $value4 =  "Images";
							if($this->lang->line('header_attr') != '') { $value5 = stripslashes($this->lang->line('header_attr')); } else $value5 =  "Attribute";
							if($this->lang->line('product_seo') != '') { $value6 = stripslashes($this->lang->line('product_seo')); } else $value6 =  "SEO";
							$formTabs[] = array(
															"value1" => $value1,
															"value2" => $value2,
															"value3" => $value3,
															"value4" => $value4,
															"value5" => $value5,
															"value6" => $value6,
														);

							if($this->lang->line('header_name') != '') { $value1 =  stripslashes($this->lang->line('header_name')); } else $value1 =  "Name";
						    if($this->lang->line('header_description') != '') { $value2 =  stripslashes($this->lang->line('header_description')); } else $value2 =  "Description";
						    if($this->lang->line('shipping_policies') != '') { $value3 =  stripslashes($this->lang->line('shipping_policies')); } else $value3 =  "Shipping & policies";
						    if($this->lang->line('product_excerpt') != '') { $value4 =  stripslashes($this->lang->line('product_excerpt')); } else $value4 =  "Excerpt";
 						    if($this->lang->line('lg_product_type') != '') { $value5 =  stripslashes($this->lang->line('lg_product_type')); } else $value5 =  "Product Type";
						    if($this->lang->line('product_quantity') != '') { $value6 =  stripslashes($this->lang->line('product_quantity')); } else $value6 =  "Quantity";
						    if($this->lang->line('product_ship_imd') != '') { $value7 =  stripslashes($this->lang->line('product_ship_imd')); } else $value7 =  "Shipping Immediately";
						    if($this->lang->line('product_sku') != '') { $value8 =  stripslashes($this->lang->line('product_sku')); } else $value8 =  "SKU";
						    if($this->lang->line('product_weight') != '') { $value9 =  stripslashes($this->lang->line('product_weight')); } else $value9 =  "Weight";
						    if($this->lang->line('header_youtube') != '') { $value10 =  stripslashes($this->lang->line('header_youtube')); } else $value10 =  "Youtube Link";
						    if($this->lang->line('giftcard_price') != '') { $value11 =  stripslashes($this->lang->line('giftcard_price')); } else $value11 =  "Price";
						    if($this->lang->line('product_sale_price') != '') { $value12 =  stripslashes($this->lang->line('product_sale_price')); } else $value12 =  "Sale Price";
						    if($this->lang->line('attribute_must') != '') { $value13 =  stripslashes($this->lang->line('attribute_must')); } else $value13 =  "Attribute must";
						    if($this->lang->line('default_shipping_cost') != '') { $value14 =  stripslashes($this->lang->line('default_shipping_cost')); } else $value14 =  "Default Shipping Cost";
						   $value15 =  mirage_lg('lg_add_specific_ship', 'Add specific country shipping cost');
						    if($this->lang->line('header_add') != '') { $value16 =  stripslashes($this->lang->line('header_add')); } else $value15 =  "Add";
							if($this->lang->line('product_remove') != '') { $value17 =  stripslashes($this->lang->line('product_remove')); } else $value16 =  "Remove";
							$formFeildsName[] = array(
																		'value1' => $value1,
																		'value2' => $value2,
																		'value3' => $value3,
																		'value4' => $value4,
																		'value5' => $value5,
																		'value6' => $value6,
																		'value7' => $value7,
																		'value8' => $value8,
																		'value9' => $value9,
																		'value10' => $value10,
																		'value11' => $value11,
																		'value12' => $value12,
																		'value13' => $value13,
																		'value14' => $value14,
																		'value15' => $value15,
																		'value16' => $value16,
																		'value17' => $value17,
																	);
							$countryLists = array();
							foreach($countryList->result() as $country){
									$countryLists[] = array(
																		'country_code' => $country->country_code,
																		'countryName' => $country->name
																);
							}
							$productDetail[] = array(
								'product_name' => (string)$productDetails->row()->product_name,
								'PID' => (string)$productDetails->row()->seller_product_id,
								'description' => strip_tags($productDetails->row()->description),
								'shipping_policies' => strip_tags($productDetails->row()->shipping_policies),
								'excerpt' => (string)$productDetails->row()->excerpt,
								'product_type' => (string)$productDetails->row()->product_type,
								'quantity' => (string)$productDetails->row()->quantity,
								'ship_immediate' => (string)$productDetails->row()->ship_immediate,
								'sku' => (string)$productDetails->row()->sku,
								'weight' => (string)$productDetails->row()->weight,
								'youtube' => (string)$productDetails->row()->youtube,
								'price' => (string)$productDetails->row()->price,
								'sale_price' => (string)$productDetails->row()->sale_price,
								'attribute_must' => (string)$productDetails->row()->attribute_must,
								'default_ship_cost' => (string)$productDetails->row()->default_ship_cost
							);
							$returnArr['formTabs'] = $formTabs;
							$returnArr['formFeildsName'] = $formFeildsName;
							$returnArr['productDetails'] = $productDetail;
							$returnArr['specificCountryShippingList'] = $specificCountryShippingList;
							$returnArr['countryLists'] = $countryLists;
	                    } else {
	                        $returnArr['response'] = 'Product UserId and UserId Mismatch !!';
	                    }
	                } else {
	                    $returnArr['response'] = 'Product Not Found !!';
	                }
				}
            } else {
                $returnArr['response'] = 'You are not a Seller. Please Sign up as Sller !!';
            }
        }
		echo json_encode($returnArr);
	}


	/*
	|| <<<<<<<<<<<<< Category Details >>>>>>>>>>> ||
	*/
	public function  sellerProductCategoryDetails(){
		$returnArr['status'] = '0';
		$returnArr['response'] = '';
		$pid = $_GET['PID'];
		if($pid == ""){
			$returnArr['response'] = 'SellerProduct Id Null !!';
		}else{
			$productDetails = $this->product_model->get_all_details(PRODUCT, array('seller_product_id' => $pid));
			if($productDetails->num_rows == 0){
				$returnArr['response'] = 'Product Not Found !!';
			}else{
				$catListArr = explode(',', $productDetails->row()->category_id);
				$CategoryList = array();
				$nullValue= array();
				$select_qry = "select * from ".CATEGORY." where rootID=0 and status='Active'";
				$categoryList = $this->mobile_model->ExecuteQuery($select_qry);
				if($categoryList == ""){
					$returnArr['response'] = 'No categories Found For This Product !!';
				}else{
					foreach($categoryList->result() as $rowMain){
						$checked1 = 'no';
						if(in_array($rowMain->id,$catListArr))$checked1 = 'yes';
						$select_qry2 = "select * from ".CATEGORY." where rootID=".$rowMain->id." and status='Active'";
						$categoryList2 = $this->mobile_model->ExecuteQuery($select_qry2);
						if($categoryList2->num_rows() > 0){
							foreach($categoryList2->result() as $rowsubcat1){
								$checked2 = 'no';
								if(in_array($rowsubcat1->id,$catListArr))$checked2 = 'yes';
								$select_qry3 = "select * from ".CATEGORY." where rootID=".$rowsubcat1->id." and status='Active'";
								$categoryList3 = $this->mobile_model->ExecuteQuery($select_qry3);
								if($categoryList3->num_rows() > 0){
									foreach($categoryList3->result() as $rowsubcat2){
										$checked3 = 'no';
										if(in_array($rowsubcat2->id,$catListArr))$checked3 = 'yes';
										$thirdCategory[] = array(
																					'catId' => $rowsubcat2->id,
																					'catName' => $rowsubcat2->cat_name,
																					'checked' => $checked3,
																					'seourl' => $rowsubcat2->seourl,
																					$rowsubcat2->seourl => $nullValue
																				);
									}
								}
								$secondCategory[] = array(
																			'catId' => $rowsubcat1->id,
																			'catName' => $rowsubcat1->cat_name,
																			'checked' => $checked2,
																			'seourl' => $rowsubcat1->seourl,
																			$rowsubcat1->seourl => $thirdCategory
																		);
								$thirdCategory =array();
							}
						}
						$CategoryList[] = array(
																	'catId' => $rowMain->id,
																	'catName' => $rowMain->cat_name,
																	'checked' => $checked1,
																	'seourl' => $rowMain->seourl,
																	$rowMain->seourl => $secondCategory
																);
						$secondCategory = array();
					}
				}
				$returnArr['status'] = '1';
				$message = "Success";
			if($this->lang->line('json_success') != ""){
				$message = stripslashes($this->lang->line('json_success'));
			}
			$returnArr['response'] = $message;
				$returnArr['CategoryList'] = $CategoryList;
			}
		}
		echo json_encode($returnArr);
	}

	/*
	|| <<<<<<<<<<<<< List Details >>>>>>>>>>> ||
	*/
	public function  sellerProductListDetails(){
		$returnArr['status'] = '0';
		$returnArr['response'] = '';
		$pid = $_GET['PID'];
		$mode = $_GET['mode'];
		$listId = $_GET['listId'];
		if($pid == ""){
			$returnArr['response'] = 'SellerProduct Id Null !!';
		}else{
			if($mode == 'add' && $listId != ""){
				$listValues = array();
	            $list_Values = $this->product_model->get_all_details(LIST_VALUES, array('list_id' => $listId));
	            if ($list_Values->num_rows() > 0) {
	                foreach ($list_Values->result() as $listRow) {
						$listValues[] = array(
														'listId' => $listRow->id,
														'listName' => $listRow->list_value
													);
	                }
	            }
				$returnArr['status'] = '1';
				$message = "Success";
			if($this->lang->line('json_success') != ""){
				$message = stripslashes($this->lang->line('json_success'));
			}
			$returnArr['response'] = $message;
				$returnArr['listValues'] = $listValues;
			}else{
				$listNames = array();
				$listValues = array();
				$selectedListDetails = array();
				$productDetails = $this->product_model->get_all_details(PRODUCT, array('seller_product_id' => $pid));
				$list_names = $productDetails->row()->list_name;
				$list_names_arr = explode(',', $list_names);
				$list_values = $productDetails->row()->list_value;
				$list_values_arr = explode(',', $list_values);
				$mainListValues = $this->product_model->get_all_details(LIST_VALUES, array('list_id' => '1'));
				$list_Value = $this->product_model->get_all_details(LIST_VALUES, array('list_id' => '1'));
				if($this->lang->line('color') != '') { $listName = stripslashes($this->lang->line('color')); } else $listName = "Color";
				$listNames[] = array('value' => '1','names' => $listName);
				if ($list_Value->num_rows() > 0) {
	                foreach ($list_Value->result() as $listRow) {
						$listValues[] = array(
														'listId' => $listRow->id,
														'listValue' => $listRow->list_value
													);
	                }
	            }
				$newListArray = array_merge($listNames,$listValues);
				if (count($list_names_arr)>0){
					foreach ($list_names_arr as $list_names_key=>$list_names_val){
						$value = '';
						if ($mainListValues->num_rows()>0){
							$selected = 'no';
							if ($list_names_val == 1){
								$selected="Yes";
							}
						 $value="1";
						 if($this->lang->line('color') != '') { $listName = stripslashes($this->lang->line('color')); } else $listName = "Color";
						}
						foreach ($list_Value->result() as $listRows) {
							$selectedListValueDetails = array();
		                    if ($listRows->id ==  $list_values_arr[$list_names_key]) {
								$selectedListValueDetails[] = array(
																			'listValueId' => $listRows->id,
											   								'listValueName' =>$listRows->list_value
																		);
		                    }
		                }
						$selectedListDetails[] = array(
							'selectedListName' => $listName,
							'selectedListid' => $value,
							'selected' => $selected,
							'selectedListValueDetails' => $selectedListValueDetails
						);
					}
					$returnArr['status'] = '1';
					$message = "Success";
					if($this->lang->line('json_success') != ""){
						$message = stripslashes($this->lang->line('json_success'));
					}
					$returnArr['response'] = $message;
					//$returnArr['listNames'] = $listNames;
					$returnArr['listValues'] = $newListArray;
					$returnArr['selectedListDetails'] = $selectedListDetails;
				}
			}
		}
		echo json_encode($returnArr);
	}

	/*
	|| <<<<<<<<<<<<< Selling Product Images List and Remove Images >>>>>>>>>>> ||
	*/
	public function  SellingProductImages(){
		$returnArr['status'] = '0';
		$returnArr['response'] = '';
		$pid = $_GET['PID'];
		$mode = $_GET['mode'];
		if($pid != ""){
			$productDetails = $this->product_model->get_all_details(PRODUCT, array('seller_product_id' => $pid));
			if($mode == 'remove'){
		        $id = $_GET['imageId'];
				if($id == ""){
					$returnArr['response'] = 'ImageId Null !!';
				}else{
					$imgArr = explode(',', $productDetails->row()->image);
					$newImageArray = array_filter($imgArr);
					if(count($newImageArray) > 1){
						$image = $newImageArray[$id];
						unset($newImageArray[$id]);
						$dir = getcwd().DIRECTORY_SEPARATOR ."images".DIRECTORY_SEPARATOR ."product".DIRECTORY_SEPARATOR ;
						foreach (glob($dir."*.*") as $file){
							if ($file == $dir.$image ){
								if(!is_dir($file) ){
									unlink($file);
								}
							}
						}
						$dir1 = getcwd().DIRECTORY_SEPARATOR ."images".DIRECTORY_SEPARATOR ."product".DIRECTORY_SEPARATOR."thumb".DIRECTORY_SEPARATOR ;
						foreach (glob($dir1."*.*") as $file1){
							if ($file1 == $dir1.$image ){
								if(!is_dir($file1) ){
									unlink($file1);
								}
							}
						}
						$newImageString = implode(',',$newImageArray);
						$dataArr = array('image' => $newImageString);
			            $condition = array('seller_product_id' => $pid);
			            $this->product_model->update_details(PRODUCT, $dataArr, $condition);
						$returnArr['status'] = '1';
						$returnArr['response'] = 'Image removed Successfully ';
					}else{
						$returnArr['response'] = 'You cant delete all the images !!';
					}
				}
			}else{
				$imgArr = explode(',', $productDetails->row()->image);
				$i = 0;
				foreach($imgArr as $imgRow){
					if(!empty($imgRow)){
						$imageDetails = base_url().'images/product/'.$imgRow;
						$images[] = array(
													'imageId' => (string)$i,
													'ImageName' => trim($imgRow),
													'images' => $imageDetails
												);
					}
				$i++;
				}
				$returnArr['status'] = '1';
				$message = "Success";
			if($this->lang->line('json_success') != ""){
				$message = stripslashes($this->lang->line('json_success'));
			}
			$returnArr['response'] = $message;
				$returnArr['productId'] = $productDetails->row()->id;
				$returnArr['images'] = $images;
			}
		}else{
			$returnArr['response'] = 'Seller Product Id Null !!';
		}
		echo json_encode($returnArr);
	}


	/*
	|| <<<<<<<<<<<<< Attributes Details >>>>>>>>>>> ||
	*/
	public function  sellerProductAttributesDetails(){
		$returnArr['status'] = '0';
		$returnArr['response'] = '';
		$pid = $_GET['PID'];
		$attributesTypesList = array();
		$selectedArrtibuteList = array();
		if($pid == ""){
			$returnArr['response'] = 'SellerProduct Id Null !!';
		}else{
			$productDetails = $this->product_model->get_all_details(PRODUCT, array('seller_product_id' => $pid));
			$PrdattrVal = $this->product_model->view_product_atrribute_details();
			foreach($PrdattrVal->result() as $row){
				$attributesTypesList[] = array(
																'attributeTypeId' =>$row->id,
																'attributeType' => $row->attr_name
															);
			}
			$SubPrdVal = $this->product_model->view_subproduct_details($productDetails->row()->id);
			foreach ($SubPrdVal->result_array() as $SubPrdValS){
				foreach($PrdattrVal->result() as $row){
					if( $row->id == $SubPrdValS['attr_id'] ){
						$selectedArrtibuteList[] = array(
																			'id' => $SubPrdValS['pid'],
																			'attributeTypeId' =>$row->id,
																			'attributeType' => $row->attr_name,
																			'attributeName' => $SubPrdValS['attr_name'],
														   					'attributePrice' => $SubPrdValS['attr_price'],
																			'attributeQuantity' => $SubPrdValS['attr_qty']
																		);
					}
				}
			}
			$returnArr['status'] = '1';
			$message = "Success";
			if($this->lang->line('json_success') != ""){
				$message = stripslashes($this->lang->line('json_success'));
			}
			$returnArr['response'] = $message;
			$returnArr['attributesTypesList'] = $attributesTypesList;
			$returnArr['selectedArrtibuteList'] = $selectedArrtibuteList;
		}
		echo json_encode($returnArr);
	}


	/*
	|| <<<<<<<<<<<<< Remove Attributes Details >>>>>>>>>>> ||
	*/
	public function  sellerProductRemoveAttributesDetails(){
		$returnArr['status'] = '0';
		$returnArr['response'] = '';
		$attributeId = $_GET['attributeId'];
		if($attributeId == ""){
			$returnArr['response'] = 'Attribute Id Null !!';
		}else{
			$this->product_model->commonDelete(SUBPRODUCT, array('pid' => $attributeId));
			$returnArr['status'] = '1';
			$returnArr['response'] = 'Successfully Removed';
		}
		echo json_encode($returnArr);
	}


	/*
	|| <<<<<<<<<<<<< Seo Details >>>>>>>>>>> ||
	*/
	public function  sellerProductSeoDetails(){
		$returnArr['status'] = '0';
		$returnArr['response'] = '';
		$pid = $_GET['PID'];
		$seoDetails = array();
		if($pid == ""){
			$returnArr['response'] = 'SellerProduct Id Null !!';
		}else{
			$productDetails = $this->product_model->get_all_details(PRODUCT, array('seller_product_id' => $pid));
			$seoDetails[] = array(
				'metaTitle' => $productDetails->row()->meta_title,
				'metaKeyword' => $productDetails->row()->meta_keyword,
				'metaDescription' => strip_tags($productDetails->row()->meta_description)
			);
			$returnArr['status'] = '1';
			$message = "Success";
			if($this->lang->line('json_success') != ""){
				$message = stripslashes($this->lang->line('json_success'));
			}
			$returnArr['response'] = $message;
			$returnArr['seoDetails'] = $seoDetails;
		}
		echo json_encode($returnArr);
	}

	/*
	|| <<<<<<<<<<<<< Seo Details >>>>>>>>>>> ||
	*/
	public function  sellerProductShippingDetails(){
		$returnArr['status'] = '0';
		$returnArr['response'] = '';
		$pid = $_GET['PID'];
		$sellerProductShippingDetails = array();
		$defaultShippingDetails = array();
		if($pid == ""){
			$returnArr['response'] = 'SellerProduct Id Null !!';
		}else{
			$productDetails = $this->mobile_model->get_all_details(PRODUCT, array('seller_product_id' => $pid));
			$ProductdShippingDetails = $this->mobile_model->sellerProductShippingDetails($productDetails->row()->id);
			if($ProductdShippingDetails->num_rows() > 0){
				foreach ($ProductdShippingDetails->result() as $data) {
					$sellerProductShippingDetails[] = array(
						'productId' => $data->product_id,
						'countryId' => $data->ship_id,
						'countryCode' => $data->ship_code,
						'countryName' => $data->ship_name,
						'byItself' => $data->ship_cost,
						'withAnotherItem' => $data->ship_other_cost,
					);
				}
			}
			$countryListDetails= $this->mobile_model->get_all_details(COUNTRY_LIST, array("country_code" => $productDetails->row()->country_code));
			$defaultShippingDetails = array(
				"duration" => $productDetails->row()->ship_duration,
				"defaultCountryCode" => $productDetails->row()->country_code,
				"defaultCountryName" => $countryListDetails->row()->name
			);
			$returnArr['status'] = '1';
			$message = "Success";
			if($this->lang->line('json_success') != ""){
				$message = stripslashes($this->lang->line('json_success'));
			}
			$returnArr['response'] = $message;
			$returnArr['defaultShippingDetails'] = $defaultShippingDetails;
			$returnArr['sellerProductShippingDetails'] = $sellerProductShippingDetails;
		}
		echo json_encode($returnArr);
	}

	/*
	|| <<<<<<<<<<<<< DOC Deatils of Selling Product >>>>>>>>>>> ||
	*/
	public function sellingProductDocDetails($value=''){
		$returnArr['status'] = '0';
		$returnArr['response'] = '';
		$pid = $_GET['PID'];
		$productDocDetails = array();
		if($pid != ""){
			$productDetails = $this->mobile_model->get_all_details(PRODUCT, array('seller_product_id' => $pid));
			if($productDetails->num_rows() == 1){
				if($productDetails->row()->product_doc != ""){
					$docType = end(explode('.',$productDetails->row()->product_doc));
					$docType = strtolower($docType);
					$docLink = base_url()."pdf-doc/pdf/".$productDetails->row()->product_doc;
					$productDocDetails[] = array(
															"productId" => $productDetails->row()->id,
															"productSellerId" => $productDetails->row()->seller_product_id,
															"DocName" => $productDetails->row()->product_doc,
															"DocType" => $docType,
															"DocLink" => $docLink,
														);
					$returnArr['status'] = '1';
					$message = "Success";
					if($this->lang->line('json_success') != ""){
						$message = stripslashes($this->lang->line('json_success'));
					}
					$returnArr['response'] = $message;
				}
				$returnArr['productDocDetails'] = $productDocDetails;
			}else{
				$message = 'Product Not Avaliable';
				if($lang['json_product_not_avaliable'] != ""){
					$message = $lang['json_product_not_avaliable'];
				}
				$returnArr['response'] = $message;
			}
		}else{
			$returnArr['response'] = 'ProductId Null !!';
		}
		echo json_encode($returnArr);
	}
	/*
	|| <<<<<<<<<<<<< DOC Deatils of Selling Product >>>>>>>>>>> ||
	*/

	/*
	|| <<<<<<<<<<<<< Selling Product Image Deletion >>>>>>>>>>> ||
	*/
	public function deleteProductImage(){
		$returnArr['status'] = (string)0;
		$returnArr['response'] = '';
		$UserId = $_POST['UserId'];
		if($UserId != ""){
			$pid = $_POST['sellerProductId'];
			if($pid != ""){
				$productDetail = $this->product_model->get_all_details(PRODUCT, array('seller_product_id' => $pid,'user_id' => $UserId));
				if($productDetail->num_rows() == 1){
					$images = $productDetail->row()->image;
					$imagesArray = explode(',',$images);
					$imagesArray = array_filter($imagesArray);
					if(count($imagesArray) > 1){
						$imageName = $_POST['imageName'];
						if($imageName != ""){
							$imageKey = array_keys($imagesArray,trim($imageName));
							if(count($imageKey) > 0){
								unset($imagesArray[$imageKey[0]]);
								$images = implode(',',$imagesArray);
								$dataArr = array('image' => $images);
								$condition = array('seller_product_id' => $pid,'user_id' => $UserId);
								$this->product_model->update_details(PRODUCT, $dataArr, $condition);
								$returnArr['status'] = (string)1;
								$returnArr['response'] = 'Image Deleted Successfully';
							}else{
								$returnArr['response'] = 'Image Not Found !!';
							}
						}else{
							$returnArr['response'] = 'Image Name Null !!';
						}
					}else{
						$returnArr['response'] = 'You cant delete this Image. It must have atleat one image !!';
					}
				}else{
					$returnArr['response'] = 'Something Went Wrong !!';
				}
			}else{
				$returnArr['response'] = 'ProductId Null !!';
			}
		}else{
			$returnArr['response'] = 'UserId Null !!';
		}
		echo json_encode($returnArr);
	}
	/*
	|| <<<<<<<<<<<<< Selling Product Image Deletion >>>>>>>>>>> ||
	*/


	/*
	|| <<<<<<<<<<<<< Update Seller Product Details >>>>>>>>>>> ||
	*/
	public function  updateSellingProduct(){
		$returnArr['status'] = (string)0;
		$returnArr['response'] = '';
		$UserId = intval($_POST['UserId']);
		if( $UserId == ""){
			$returnArr['response'] = 'UserId Null !!';
		}else{
	        $mode = $this->input->post('mode');
			if($mode == ""){
				$returnArr['response'] = 'Please Enter Mode !!';
			}else{
		        $pid = $this->input->post('PID');
				if($pid == ""){
					$returnArr['response'] = 'Enter Seller Product Id !!';
				}else{
			        $default_ship_cost = $this->input->post('default_ship_cost');
			        $shipping_cost = $this->input->post('shipping_cost');
			        $excludeArr = array('UserId','PID', 'nextMode', 'mode','served_specific', 'changeorder', 'imaged', 'gateway_tbl_length', 'category_id', 'attribute_name', 'attribute_val', 'separate_cost', 'separate_anotheritem', 'country_code', 'default_ship_cost');

					/* || <<<<<< Product Details >>>>>  ||  */
			        if ($mode == 'detail') {
			            $price_range = 0;
			            $price = $this->input->post('sale_price');
			            if ($price > 0 && $price < 21) {
			                $price_range = '1-20';
			            } else if ($price > 20 && $price < 101) {
			                $price_range = '21-100';
			            } else if ($price > 100 && $price < 201) {
			                $price_range = '101-200';
			            } else if ($price > 200 && $price < 501) {
			                $price_range = '201-500';
			            } else if ($price > 500) {
			                $price_range = '501+';
			            }
			            if ($pid == '') {
			                $old_product_details = array();
			            } else {
			                $old_product_details = $this->product_model->get_all_details(PRODUCT, array('seller_product_id' => $pid));
			            }
			            $dataArr = array('seller_product_id' => $pid);
			            $checkProduct = $this->product_model->get_all_details(PRODUCT, $dataArr);
			            if ($checkProduct->num_rows() == 0) {
			                $userProduct = $this->product_model->get_all_details(USER_PRODUCTS, $dataArr);
			                if ($userProduct->num_rows() == 1) {
			                    $dataArr['image'] = $userProduct->row()->image;
			                    $dataArr['seourl'] = url_title($this->input->post('product_name'), '-');
			                    $dataArr['user_id'] = $userProduct->row()->user_id;
			                    $dataArr['price_range'] = $price_range;
			                    $dataArr['category_id'] = $userProduct->row()->category_id;
			                    $dataArr['shipping_cost'] = $default_ship_cost;
			                    $youtube_link = $this->input->post('youtube');
			                    parse_str(parse_url($youtube_link, PHP_URL_QUERY), $youtube_id);
			                    $dataArr['youtube'] = $youtube_id['v'];
								if($this->input->post('attribute_must') == 'yes'){
									$dataArr['attribute_must'] = $this->input->post('attribute_must');
								}
								$dataArr['product_type'] = $this->input->post('product_type');
			                    $this->product_model->commonInsertUpdate(PRODUCT, 'insert', $excludeArr, $dataArr);
			                    $product_id = $this->product_model->get_last_insert_id();
			                    $this->update_price_range_in_table('add', $price_range, $product_id, $old_product_details);
			                    $this->product_model->commonDelete(USER_PRODUCTS, array('seller_product_id' => $pid));
			                    $excludeShippArr = array('UserId ','mode','PID', 'served_specific', 'nextMode', 'changeorder', 'imaged', 'gateway_tbl_length', 'category_id', 'attribute_name', 'attribute_val', 'separate_cost', 'country_code', 'default_ship_cost', 'product_name', 'description', 'shipping_policies', 'excerpt', 'quantity', 'ship_immediate', 'sku', 'weight', 'price', 'sale_price', 'separate_ship_cost', 'separate_anotheritem', 'country_code', 'product_id', 'youtube', 'product_type');
			                    $country_codeArr = $this->input->post('country_code');
			                    $separate_costArr = $this->input->post('separate_cost');
			                    $shipCostDetails = $this->product_model->get_all_details(SHIPPING_COST, array('product_id' => $pid));
			                    $this->product_model->commonDelete(SHIPPING_COST, array('product_id' => $pid));
			                    $i = 0;
			                    foreach ($country_codeArr as $country_codeRow) {
			                        $shippArr = array(
			                            'country_code' => $country_codeRow,
			                            'separate_ship_cost' => $separate_costArr[$i],
			                            'product_id' => $pid
			                        );
					                $this->product_model->commonInsertUpdate(SHIPPING_COST, 'insert', $excludeShippArr, $shippArr);
					                $i++;
			                    }
			                    if ($this->lang->line('change_saved') != ''){
			                        $lg_err_msg = $this->lang->line('change_saved');
								}else{
			                        $lg_err_msg = 'Yeah ! changes have been saved';
								}
								$returnArr['status'] = '1';
								$returnArr['response'] = $lg_err_msg;
			                }
			            } else {
			                $dataArr['seourl'] = url_title($this->input->post('product_name'), '-');
			                $dataArr['price_range'] = $price_range;
			                $dataArr['shipping_cost'] = $default_ship_cost;
			                $dataArr['attribute_must'] = $this->input->post('attribute_must');
			                $youtube_link = $this->input->post('youtube');
			                parse_str(parse_url($youtube_link, PHP_URL_QUERY), $youtube_id);
			                $dataArr['youtube'] = $youtube_id['v'];
			                $dataArr['product_type'] = $this->input->post('product_type');
			                $excludeShippArr = array('UserId ','mode','PID', 'nextMode', 'served_specific', 'changeorder', 'imaged', 'gateway_tbl_length', 'category_id', 'attribute_name', 'attribute_val', 'separate_cost', 'country_code', 'default_ship_cost', 'product_name', 'description', 'shipping_policies', 'excerpt', 'quantity', 'ship_immediate', 'sku', 'weight', 'price', 'sale_price', 'separate_ship_cost', 'separate_anotheritem', 'country_code', 'product_id', 'youtube', 'product_type', 'attribute_must');
			                $country_codeArr = $this->input->post('country_code');
			                $separate_costArr = $this->input->post('separate_cost');
			                $this->product_model->commonDelete(SHIPPING_COST, array('product_id' => $pid));
			                $i = 0;
			                foreach ($country_codeArr as $country_codeRow) {
			                    $shippArr = array(
			                        'country_code' => $country_codeRow,
			                        'separate_ship_cost' => $separate_costArr[$i],
			                        'product_id' => $pid
			                    );
			                    $this->product_model->commonInsertUpdate(SHIPPING_COST, 'insert', $excludeShippArr, $shippArr);
			                    $i++;
			                }
			                $this->product_model->commonInsertUpdate(PRODUCT, 'update', $excludeArr, $dataArr, array('seller_product_id' => $pid));
			                $this->update_price_range_in_table('edit', $price_range, $old_product_details->row()->id, $old_product_details);
			                if ($this->lang->line('change_saved') != '')
			                    $lg_err_msg = $this->lang->line('change_saved');
			                else{
			                    $lg_err_msg = 'Yeah ! changes have been saved';
							}

							// $productDetailArray = array();
							// $productDetail = $this->product_model->get_all_details(PRODUCT, array('seller_product_id' => $pid));
							// if($productDetail->num_rows() == 1){
							// 	$prodImage = "";
							// 	if($productDetail->row()->image){
							// 		$image = explode(",",$productDetail->row()->image);
							// 		$imageLink = "";
							// 		if($image[0] != ""){
							// 			$imageLink = base_url()."images/product/".$image[0];
							// 		}
							// 	}
							// 	$userDetail = $this->mobile_model->get_all_details(USERS, array('id' => $productDetail->row()->user_id));
							// 	$loggedUserDetails = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
							// 	$productDetailArray[] = array(
							// 											"pid" => $productDetail->row()->id,
							// 											"price" => $productDetail->row()->sale_price,
							// 											"SellerProdcutId" => $productDetail->row()->seller_product_id,
							// 											"Image" => $imageLink,
							// 											"productname" => $productDetail->row()->product_name,
							// 											"ShippingPolicy" => strip_tags($productDetail->row()->shipping_policies),
							// 											"Description" => strip_tags($productDetail->row()->description),
							// 											"AppUserName" => $loggedUserDetails->row()->user_name,
							// 											"ProductUserName" => $userDetail->row()->user_name,
							// 											"ProUserId" => $productDetail->row()->user_id,
							// 											"Url" => base_url().'json/product?pid='.$productDetail->row()->id.'&UserId='.$UserId,
							// 										);
							// }
							$returnArr['status'] = '1';
							$returnArr['response'] = $lg_err_msg;
							//$returnArr['productDetails'] = $productDetailArray;
			            }
			        }else if ($mode == 'seo') {
			            $this->product_model->commonInsertUpdate(PRODUCT, 'update', $excludeArr, array(), array('seller_product_id' => $pid));
						$condition = "  where p.status='Publish' and u.group='Seller' and u.status='Active' and p.seller_product_id='" . $pid . "' or p.status='Publish' and p.user_id=0 and p.seller_product_id='" . $pid . "'";
						$productDetails = $this->mobile_model->view_product_details($condition);
						$cond = "select user_id from ".PRODUCT_LIKES." where product_id=".$pid." and user_id=".$UserId;
						$likedUsersList = $this->mobile_model->ExecuteQuery($cond);
						$cond1 = "select * from ".PRODUCT_LIKES." where product_id=".$pid;
						$likedCounts = $this->mobile_model->ExecuteQuery($cond1);
						$likedCount = $likedCounts->num_rows();
						$likedStatus = "No";
						if($likedUsersList->row()->user_id == $UserId){
							$likedStatus = "Yes";
						}
						$imgArr=@explode(',',$productDetails->row()->image);
						$images=array();
						foreach($imgArr as $img){
							if($img!="")$images[]=base_url().'images/product/'.$img;
						}
						$prodDetail[]=array(
													'id'=>$productDetails->row()->id,
													'seller_product_id'=>$productDetails->row()->seller_product_id,
													'UserId' =>$productDetails->row()->user_id,
													'product_name'=>$productDetails->row()->product_name,
													'seourl'=>$productDetails->row()->seourl,
													'description'=>strip_tags($productDetails->row()->description),
													'shippingPolicies'=>strip_tags($productDetails->row()->shipping_policies),
													'image'=>$images,
													'price'=>$productDetails->row()->price,
													'sale_price'=>$productDetails->row()->sale_price,
													'user_name'=>$productDetails->row()->user_name,
													'likes'=>(string)$likedCount,
													'liked' => $likedStatus,
													'url'=>base_url().'json/product?pid='.$productDetails->row()->id.'&UserId='.$UserId,
													'shareUrl' => base_url().'things/'.$productDetails->row()->id.'/'.$productDetails->row()->product_name.'?ref='.$productDetails->row()->user_name,
													'type' => "SellerProduct",
													'userUrl' => 'http://192.168.1.251:8081/product-working/fancyclone-v3/json/user/seller-timeline?username='.$productDetails->row()->user_name.'&UserId='.$productDetails->row()->user_id,
													);
			            if ($this->lang->line('change_saved') != ''){
			                $lg_err_msg = $this->lang->line('change_saved');
						}
			            else{
			                $lg_err_msg = 'Yeah ! changes have been saved';
						}
				        $returnArr['status'] = '1';
						$returnArr['response'] = $lg_err_msg;
						$returnArr['response'] = $prodDetail;
			        }else if ($mode == 'images') {
						if (!empty($_POST['product_image'])) {
							$imgPath ='images/temp/';
							$img = $_POST['product_image'];
							$imgExt = $_POST['imageExt'];
							$imgExt = strtolower($imgExt);
							if($imgExt == ""){
								$returnArr['response'] = "Image Extension is Null !!";
							}else{
								$imgName = time(). ".".$imgExt;
								$imageFormat = array('data:image/jpeg;base64', 'data:image/png;base64', 'data:image/jpg;base64', 'data:image/gif;base64');
								$img = str_replace($imageFormat, '', $img);
								$data = base64_decode($img);
								$image = @imagecreatefromstring($data);
								if ($image !== false) {
									$uploadPath = $imgPath . $imgName;
									if($imgExt == 'jpeg' || $imgExt == "jpg"){
										imagejpeg($image, $uploadPath, 100);
									}else if($imgExt == 'gif'){
										imagegif($image, $uploadPath);
									}else if($imgExt == 'png'){
										imagepng($image, $uploadPath);
									}
									imagedestroy($image);
								} else {
									$message = "An error occurred";
							if($this->lang->line('json_error_occured') != ""){
							    $message = stripslashes($this->lang->line('json_error_occured'));
							}
							$returnArr['response'] = $message;
								}
								if (isset($uploadPath)) {
									$filename = base_url($uploadPath);
									list($width, $height) = getimagesize($filename);
									$newwidth = 100;
									$newheight = 100;
									$sourceImage = @imagecreatefromjpeg($filename);
									imagecopyresized($thumbImage, $sourceImage, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
								}else {
									$message = "Invalid File";
							if($this->lang->line('json_invalid_file') != ""){
							    $message = stripslashes($this->lang->line('json_invalid_file'));
							}
							$returnArr['response'] = $message;
								}
								$new_name = $imgName;
								@copy("./images/temp/".$imgName, './images/product/'.$new_name);
								@copy("./images/temp/".$imgName, './images/product/thumb/'.$new_name);
								$this->imageResizeWithSpace(200, 200, $new_name, './images/product/thumb/');
								$this->imageResizeWithSpace(600, 600, $new_name, './images/product/');
								$productDetails = $this->product_model->get_all_details(PRODUCT, array('seller_product_id' => $pid));
								$imgArr = explode(',', $productDetails->row()->image);
								array_push($imgArr,$new_name);
								$newImagesString = implode(',',$imgArr);
								$dataArr = array('image'=> $newImagesString);
					            $condition = array('seller_product_id' => $pid);
					            $this->user_model->update_details(PRODUCT, $dataArr, $condition);
								$dir = getcwd().DIRECTORY_SEPARATOR ."images".DIRECTORY_SEPARATOR."temp".DIRECTORY_SEPARATOR ;
								$interval = strtotime('-24 hours');
								foreach (glob($dir."*.*") as $file){
									if (filemtime($file) <= $interval ){
										if(!is_dir($file) ){
											unlink($file);
										}
									}
								}
								$returnArr['status'] = '1';
								if ($this->lang->line('change_saved') != '')
					                $lg_err_msg = $this->lang->line('change_saved');
					            else
					                $lg_err_msg = 'Yeah ! changes have been saved';
					                $returnArr['response'] = $lg_err_msg;
							}
						}else{
							$returnArr['response'] = "Please Upload Image";
						}
			        }else if($mode == 'shipping'){
						$shippingDuration =  $_POST['shippingDuration'];
						$defaultCountryCode =  $_POST['defaultCountryCode'];
						$dataArray = array("country_code" => $defaultCountryCode);
						$condition = array('seller_product_id' => $pid);
						$this->product_model->update_details(PRODUCT, $dataArray,$condition);
						if($pid!=''){
							$product = $this->product_model->get_all_details(PRODUCT, array('seller_product_id'=>$pid));
							if($_POST['shipping_to'] != ''){
								$this->product_model->commonDelete(SUB_SHIPPING,array('product_id'=>$product->row()->id));
								$ship_to = explode(',',$_POST['shipping_to']);
								$ship_to_id = explode(',',$_POST['ship_to_id']);
								$cost_individual = explode(',',$_POST['shipping_cost']);
								$cost_with_another = explode(',',$_POST['shipping_with_another']);
								// echo "<pre>";print_r($ship_to);
								// echo "<pre>";print_r($ship_to_id);
								// echo "<pre>";print_r($cost_individual);
								// echo "<pre>";print_r($cost_with_another);
								// die;
								for($i=0; $i < sizeof($ship_to); $i++){
									$ship_name = @explode('|', $ship_to[$i]);
									if($ship_to[$i] == 'EverywhereElse'){
										$shipName = 'Everywhere Else';
										$shipId = 232;
									} else {
										$shipName = $ship_name[2];
										$shipCode = $ship_name[1];
										$shipId = $ship_to_id[$i];
									}
									$seourlBase = $seourl = url_title($shipName, '-', TRUE);
									$seourl_check = '0';
									$duplicate_url = $this->product_model->get_all_details(SUB_SHIPPING,array('ship_seourl'=>$seourl));
									if($duplicate_url->num_rows()>0){
										$seourl = $seourlBase.'-'.$duplicate_url->num_rows();
									}else{
										$seourl_check = '1';
									}
									$urlCount = $duplicate_url->num_rows();
									while($seourl_check == '0'){
										$urlCount++;
										$duplicate_url = $this->product_model->get_all_details(SUB_SHIPPING,array('ship_seourl'=>$seourl));
										if ($duplicate_url->num_rows()>0){
											$seourl = $seourlBase.'-'.$urlCount;
										}else {
											$seourl_check = '1';
										}
									}
									$dataArrShip = array('product_id' => $product->row()->id,'ship_id' => $shipId, 'ship_name' => $shipName,'ship_cost' => $cost_individual[$i],'ship_seourl' => $seourl,'ship_code'=> $shipCode,'ship_other_cost' => $cost_with_another[$i]);
									$this->product_model->simple_insert(SUB_SHIPPING,$dataArrShip);
								}
							}
						}
						if($this->lang->line('change_saved')!= ''){
			                $lg_err_msg = $this->lang->line('change_saved');
						}else{
			                $lg_err_msg = 'Yeah !! changes have been saved';
						}
						$returnArr['status'] = "1";
						$returnArr['response'] = $lg_err_msg;
					}else if ($mode == 'doc') {
			            $config['overwrite'] = FALSE;
			            $config['allowed_types'] = 'pdf|doc|docx|txt|csv|xls|text|jpg|jpeg|gif|png|psd|bmp';
			            $config['upload_path'] = './pdf-doc/pdf/';
			            $this->load->library('upload', $config);
			            $pdfImage = array();
			            if ($this->upload->do_upload('pdfupload')) {
			                $fileDetails = $this->upload->data();
			                $filename = $fileDetails['file_name'];
			                $pdfImage = array('product_doc' => $filename);
			            } else {
			                print_r($this->upload->display_errors());
			            }
			            $this->product_model->update_details(PRODUCT, $pdfImage, array('seller_product_id' => $pid));
						$returnArr['status'] = '1';
						if ($this->lang->line('change_saved') != ''){
							$lg_err_msg = $this->lang->line('change_saved');
						}else{
							$lg_err_msg = 'Yeah ! changes have been saved';
						}
						$returnArr['response'] = $lg_err_msg;
			        }else if ($mode == 'categories') {
			            if ($this->input->post('category_id') != '') {
			            //    $category_id = implode(',', $this->input->post('category_id'));
						$category_id = $this->input->post('category_id');
			            } else {
			                $category_id = '';
			            }
			            $dataArr = array('category_id' => $category_id);
			            $this->product_model->update_details(PRODUCT, $dataArr, array('seller_product_id' => $pid));
						$returnArr['status'] = '1';
						if ($this->lang->line('change_saved') != ''){
							$lg_err_msg = $this->lang->line('change_saved');
						}else{
							$lg_err_msg = 'Yeah ! changes have been saved';
						}
						$returnArr['response'] = $lg_err_msg;
			        }else if ($mode == 'list') {
			            $list_name_str = $list_val_str = '';
						$list_name_str = $this->input->post('attribute_name');
			            $list_val_str = $this->input->post('attribute_val');
			            // $list_name_arr = $this->input->post('attribute_name');
			            // $list_val_arr = $this->input->post('attribute_val');
			            // if (is_array($list_name_arr) && count($list_name_arr) > 0) {
			            //     $list_name_str = implode(',', $list_name_arr);
			            //     $list_val_str = implode(',', $list_val_arr);
			            // }
			            $dataArr = array('list_name' => $list_name_str, 'list_value' => $list_val_str);
			            $this->product_model->update_details(PRODUCT, $dataArr, array('seller_product_id' => $pid));
			            if (is_array($list_val_arr)) {
			                foreach ($list_val_arr as $list_val_row) {
			                    $list_val_details = $this->product_model->get_all_details(LIST_VALUES, array('id' => $list_val_row));
			                    if ($list_val_details->num_rows() == 1) {
			                        $product_count = $list_val_details->row()->product_count;
			                        $products_in_this_list = $list_val_details->row()->products;
			                        $products_in_this_list_arr = explode(',', $products_in_this_list);
			                        if (!in_array($pid, $products_in_this_list_arr)) {
			                            array_push($products_in_this_list_arr, $pid);
			                            $product_count++;
			                            $list_update_values = array(
			                                'products' => implode(',', $products_in_this_list_arr),
			                                'product_count' => $product_count
			                            );
			                            $list_update_condition = array('id' => $list_val_row);
			                            $this->product_model->update_details(LIST_VALUES, $list_update_values, $list_update_condition);
			                        }
			                    }
			                }
			            }
						$returnArr['status'] = '1';
						if ($this->lang->line('change_saved') != ''){
							$lg_err_msg = $this->lang->line('change_saved');
						}else{
							$lg_err_msg = 'Yeah ! changes have been saved';
						}
						$returnArr['response'] = $lg_err_msg;
			        }else if ($mode == 'attribute') {
			            $dataArr = array('seller_product_id' => $pid);
			            $checkProduct = $this->product_model->get_all_details(PRODUCT, $dataArr);
			            if ($checkProduct->num_rows() == 1) {
			                $prodId = $checkProduct->row()->id;
			                $Attr_name_str = $Attr_val_str = '';
							$Attr_type_arr = explode(',', $this->input->post('product_attribute_type'));
							$Attr_name_arr = explode(',', $this->input->post('product_attribute_name'));
							$Attr_val_arr = explode(',', $this->input->post('product_attribute_val'));
							$Attr_qty_arr = explode(',',$this->input->post('product_attribute_qty'));
			                if (is_array($Attr_type_arr) && count($Attr_type_arr) > 0) {
			                    for ($k = 0; $k < sizeof($Attr_type_arr); $k++) {
			                        $dataSubArr = '';
									$dataSubArr = array('product_id' => $prodId, 'attr_id' => $Attr_type_arr[$k], 'attr_name' => $Attr_name_arr[$k], 'attr_price' => $Attr_val_arr[$k],'attr_qty'=>$Attr_qty_arr[$k]);
			                        $this->product_model->add_subproduct_insert($dataSubArr);
			                    }
							/*
							|| ===|| New Changes ||=== ||
							*/
								$existQty = $this->db->select_sum('attr_qty','Qty')->from(SUBPRODUCT)->where('product_id',$prodId)->get();
								if($this->input->post('attribute_must') =='yes'){
									$newQty = $existQty->row()->Qty;
									$this->product_model->update_details(PRODUCT,array('quantity'=>$newQty),array('id'=>$prodId));
								}else{
									$_product = $this->product_model->get_all_details(PRODUCT,array('id'=>$prodId));
									$sumQty = $existQty->row()->Qty;
									if($sumQty > $_product->row()->quantity){
										$newQty = $sumQty;
										$this->product_model->update_details(PRODUCT,array('quantity'=>$newQty),array('id'=>$prodId));
									}
								}
							/*
							|| ===|| New Changes ||=== ||
							*/
			                }

							$returnArr['status'] = '1';
							if ($this->lang->line('change_saved') != ''){
								$lg_err_msg = $this->lang->line('change_saved');
							}else{
								$lg_err_msg = 'Yeah ! changes have been saved';
							}
							$returnArr['response'] = $lg_err_msg;
			            }else {
			                if ($this->lang->line('prod_not_found_db') != ''){
			                    $lg_err_msg = $this->lang->line('prod_not_found_db');
							}else{
			                    $lg_err_msg = 'Product not found in database';
							}
							$returnArr['response'] = $lg_err_msg;
			            }
			        }else {
			            $returnArr['response'] = 'Invalid Mode !!';
			        }
				}
			}
		}
		echo json_encode($returnArr);
    }

	public function update_price_range_in_table($mode = '', $price_range = '', $product_id = '0', $old_product_details = '') {
        $list_values = $this->product_model->get_all_details(LIST_VALUES, array('list_value' => $price_range));
        if ($list_values->num_rows() == 1) {
            $products = explode(',', $list_values->row()->products);
            $product_count = $list_values->row()->product_count;
            if ($mode == 'add') {
                if (!in_array($product_id, $products)) {
                    array_push($products, $product_id);
                    $product_count++;
                }
            } else if ($mode == 'edit') {
                $old_price_range = '';
                if ($old_product_details != '' && count($old_product_details) > 0 && $old_product_details->num_rows() == 1) {
                    $old_price_range = $old_product_details->row()->price_range;
                }
                if ($old_price_range != '' && $old_price_range != $price_range) {
                    $old_list_values = $this->product_model->get_all_details(LIST_VALUES, array('list_value' => $old_price_range));
                    if ($old_list_values->num_rows() == 1) {
                        $old_products = explode(',', $old_list_values->row()->products);
                        $old_product_count = $old_list_values->row()->product_count;
                        if (in_array($product_id, $old_products)) {
                            if (($key = array_search($product_id, $old_products)) !== false) {
                                unset($old_products[$key]);
                                $old_product_count--;
                                $updateArr = array('products' => implode(',', $old_products), 'product_count' => $old_product_count);
                                $updateCondition = array('list_value' => $old_price_range);
                                $this->product_model->update_details(LIST_VALUES, $updateArr, $updateCondition);
                            }
                        }
                    }
                    if (!in_array($product_id, $products)) {
                        array_push($products, $product_id);
                        $product_count++;
                    }
                } else if ($old_price_range != '' && $old_price_range == $price_range) {
                    if (!in_array($product_id, $products)) {
                        array_push($products, $product_id);
                        $product_count++;
                    }
                }
            }
            $updateArr = array('products' => implode(',', $products), 'product_count' => $product_count);
            $updateCondition = array('list_value' => $price_range);
            $this->product_model->update_details(LIST_VALUES, $updateArr, $updateCondition);
        }
    }


	/*
	|| >>>>>>>>>>>>>>>>> Delete Seller Product <<<<<<<<<<<<<<<<  ||
	*/
	public function deleteSellerProduct(){
		$returnArr['status'] = (string)0;
		$returnArr['response'] = '';
		$pid = $_GET['ProductSellerId'];
		$UserId = $_GET['UserId'];
		if($pid == ""){
			$returnArr['response'] = 'Product Seller Id Null !!';
		}else{
	        if ($UserId == '') {
	            $returnArr['response'] = 'UserId Null !!';
	        } else {
	            $productDetails = $this->product_model->get_all_details(USER_PRODUCTS, array('seller_product_id' => $pid));
	            if ($productDetails->num_rows() == 1) {
	                if ($productDetails->row()->user_id == $UserId) {
	                    $this->product_model->commonDelete(USER_PRODUCTS, array('seller_product_id' => $pid));
	                    $this->update_counts_after_deletion($productDetails,$UserId);
	                    if ($this->lang->line('prod_del_succ') != ''){
	                        $lg_err_msg = $this->lang->line('prod_del_succ');
						}else{
	                        $lg_err_msg = 'Product deleted successfully';
						}
						$returnArr['status'] = (string)1;
						$returnArr['response'] =$lg_err_msg;
	                }else {
						$message = "Sorry, You Cant Delete This Product";
						if($this->lang->line('json_cant_delete_product') != ""){
						    $message = stripslashes($this->lang->line('json_cant_delete_product'));
						}
						$returnArr['response'] = $message;
	                }
	            } else {
	                $productDetails = $this->product_model->get_all_details(PRODUCT, array('seller_product_id' => $pid));
	                if ($productDetails->num_rows() == 1){
	                    if ($productDetails->row()->user_id == $UserId) {
	                        $this->product_model->commonDelete(PRODUCT, array('seller_product_id' => $pid));
	                        $this->update_counts_after_deletion($productDetails,$UserId);
	                        $this->product_model->commonDelete(SHOPPING_CART, array('product_id' => $productDetails->row()->id));
	                        if ($this->lang->line('prod_del_succ') != ''){
	                            $lg_err_msg = $this->lang->line('prod_del_succ');
							}else{
	                            $lg_err_msg = 'Product deleted successfully';
							}
						$returnArr['status'] = (string)1;
	                    $returnArr['response'] =$lg_err_msg;
	                    }else {
							$message = "Sorry, You Cant Delete This Product";
							if($this->lang->line('json_cant_delete_product') != ""){
							    $message = stripslashes($this->lang->line('json_cant_delete_product'));
							}
							$returnArr['response'] = $message;
	                    }
	                } else {
	                	$returnArr['response'] ="Sorry, Check Product Seller Id !! ";
	                }
	            }
	        }
		}
		echo json_encode($returnArr);
    }

	public function update_counts_after_deletion($productDetails,$UserId){
        $this->data['userDetails'] = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
        $productCount = $this->data['userDetails']->row()->products;
        $productCount--;
        $this->product_model->update_details(USERS, array('products' => $productCount), array('id' => $UserId));
        //--------------------
        //User likes count
        $liked_users = $this->product_model->get_all_details(PRODUCT_LIKES, array('product_id' => $productDetails->row()->seller_product_id));
        if ($liked_users->num_rows() > 0) {
            foreach ($liked_users->result() as $liked_users_row) {
                $liked_user = $this->product_model->get_all_details(USERS, array('id' => $liked_users_row->user_id));
                if ($liked_user->num_rows() > 0) {
                    $likes_count = $liked_user->row()->likes;
                    $likes_count--;
                    if ($likes_count < 0) {
                        $likes_count = 0;
                    }
                    $this->product_model->update_details(USERS, array('likes' => $likes_count), array('id' => $liked_user->row()->id));
                }
            }
            $this->product_model->commonDelete(PRODUCT_LIKES, array('product_id' => $productDetails->row()->seller_product_id));
        }
        //--------------------
        //Wants count
        $wanted_users = $this->product_model->get_all_details(WANTS_DETAILS, array('product_id' => $productDetails->row()->seller_product_id));
        if ($wanted_users->num_rows() > 0) {
            foreach ($wanted_users->result() as $wu_row) {
                $wanted_user = $this->product_model->get_all_details(USERS, array('id' => $wu_row->user_id));
                if ($wanted_user->num_rows() > 0) {
                    $wanted_count = $wanted_user->row()->want_count;
                    if ($wanted_count > 0) {
                        $wanted_count--;
                    }
                    $this->product_model->update_details(USERS, array('want_count' => $wanted_count), array('id' => $wanted_user->row()->id));
                }
            }
            $this->product_model->commonDelete(WANTS_DETAILS, array('product_id' => $productDetails->row()->seller_product_id));
        }
        //--------------------
        //Owns count
        $owned_user_qry = "select * from " . USERS . " where FIND_IN_SET('" . $productDetails->row()->seller_product_id . "',own_products)";
        $owned_user = $this->product_model->ExecuteQuery($owned_user_qry);
        if ($owned_user->num_rows() > 0) {
            foreach ($owned_user->result() as $owned_user_row) {
                $own_products = array_unique(array_filter(explode(',', $owned_user_row->own_products)));
                if (($key = array_search($productDetails->row()->seller_product_id, $own_products)) !== false) {
                    unset($own_products[$key]);
                    $own_count = count($own_products);
                    $this->product_model->update_details(USERS, array('own_products' => implode(',', $own_products), 'own_count' => $own_count), array('id' => $owned_user_row->id));
                }
            }
        }
        //--------------------
        //Notifications table
        $this->product_model->commonDelete(NOTIFICATIONS, array('activity_id' => $productDetails->row()->seller_product_id));
        $this->product_model->commonDelete(USER_ACTIVITY, array('activity_id' => $productDetails->row()->seller_product_id));
        //--------------------
        //Comments table
        $this->product_model->commonDelete(PRODUCT_COMMENTS, array('product_id' => $productDetails->row()->seller_product_id));
        //--------------------
        //Update lists count
        $list_qry = "select * from " . LISTS_DETAILS . " where FIND_IN_SET('" . $productDetails->row()->seller_product_id . "',product_id)";
        $lists_details = $this->product_model->ExecuteQuery($list_qry);
        if ($lists_details->num_rows() > 0) {
            foreach ($lists_details->result() as $list_row) {
                $list_products = array_unique(array_filter(explode(',', $list_row->product_id)));
                if (($key = array_search($productDetails->row()->seller_product_id, $list_products)) !== false) {
                    unset($list_products[$key]);
                    $prod_count = count($list_products);
                    $this->product_model->update_details(LISTS_DETAILS, array('product_id' => implode(',', $list_products), 'product_count' => $prod_count), array('id' => $list_row->id));
                }
            }
        }
        //--------------------
    }

	/*
	|| >>>>>>>>>>>>>>>>> Web View Invoice - Purchase <<<<<<<<<<<<<<<<  ||
	*/
	public function viewPurchaseInvoice() {
		$returnArr['status'] = (string)0;
		$returnArr['response'] = '';
		$UserId = intval($_GET['UserId']);
		$mode = $_GET['mode'];
        if($UserId == ''){
            $returnArr['response'] = ' UserId Null !!';
			echo json_encode($returnArr);
        }else{
			$InvoiceNumber = $_GET['InvoiceNumber'];
		    if($InvoiceNumber != ''){
				if($mode == ""){
					$returnArr['response'] = ' Mode Null !!';
					echo json_encode($returnArr);
				}else{
					if($mode == "purchase"){
						$purchaseList = $this->user_model->get_purchase_list($UserId, $InvoiceNumber);
					}else{
						$purchaseList = $this->user_model->get_order_list($UserId, $InvoiceNumber);
					}
					$shipAddRess = $this->user_model->get_all_details(SHIPPING_ADDRESS, array('id' => $purchaseList->row()->shippingid));
					$newsid = '19';
					$template_values = $this->product_model->get_newsletter_template_details($newsid);
					$this->data['title'] = $template_values['news_subject'];
		            $this->data['logo'] = $this->data['logo'];
		            $this->data['meta_title'] = $this->config->item('meta_title');
		            $this->data['ship_fullname'] = stripslashes($shipAddRess->row()->full_name);
		            $this->data['ship_address1'] = stripslashes($shipAddRess->row()->address1);
		            $this->data['ship_address2'] = stripslashes($shipAddRess->row()->address2);
		            $this->data['ship_city'] = stripslashes($shipAddRess->row()->city);
		            $this->data['ship_country'] = stripslashes($shipAddRess->row()->country);
		            $this->data['ship_state'] = stripslashes($shipAddRess->row()->state);
		            $this->data['ship_postalcode'] = stripslashes($shipAddRess->row()->postal_code);
		            $this->data['ship_phone'] = stripslashes($shipAddRess->row()->phone);
		            $this->data['bill_fullname'] = stripslashes($purchaseList->row()->full_name);
		            $this->data['bill_address1'] = stripslashes($purchaseList->row()->address);
		            $this->data['bill_address2'] = stripslashes($purchaseList->row()->address2);
		            $this->data['bill_city'] = stripslashes($purchaseList->row()->city);
		            $this->data['bill_country'] = stripslashes($purchaseList->row()->country);
		            $this->data['bill_state'] = stripslashes($purchaseList->row()->state);
		            $this->data['bill_postalcode'] = stripslashes($purchaseList->row()->postal_code);
		            $this->data['bill_phone'] = stripslashes($purchaseList->row()->phone_no);
		            $this->data['invoice_number'] = $purchaseList->row()->dealCodeNumber;
		            $this->data['payment_type'] = $purchaseList->row()->payment_type;
		            $this->data['invoice_date'] = date("F j, Y g:i a", strtotime($purchaseList->row()->created));
					$items = "";
					foreach ($purchaseList->result() as $cartRow) {
			            if ($cartRow->image != '') {
			                $InvImg = @explode(',', $cartRow->image);
			            } else {
			                $InvImg = @explode(',', $cartRow->old_image);
			            }
			            $unitPrice = ($cartRow->price * (0.01 * $cartRow->product_tax_cost)) + $cartRow->product_shipping_cost + $cartRow->price;
			            $uTot = $unitPrice * $cartRow->quantity;
			            if ($cartRow->attr_name != '' || $cartRow->attr_type) {
			                $atr = '<br>' . $cartRow->attr_type . ' / ' . $cartRow->attr_name;
			            } else if ($cartRow->old_attr_name != '') {
			                $atr = '<br>' . $cartRow->old_attr_name;
			            } else {
			                $atr = '';
			            }
			            $product_name = $cartRow->product_name;
			            if ($product_name == '') {
			                $product_name = $cartRow->old_product_name;
			            }
			            if ($cartRow->product_type == 'physical') {
			                $dwnldcode = 'Not applicable';
			            } else {
			                $dwnldcode = 'Copy & paste this code to download product: ' . $cartRow->product_download_code;
			            }
					$items .= '<div class="product-list"><div class="container"><div class="col-md-2 no-padding"><p>Bag Items</p><p class="clearfix"><img src="'. base_url() . PRODUCTPATH . $InvImg[0] .'" title="" alt="' . stripslashes($cartRow->product_name) . '" /></p></div><div class="col-md-2 no-padding"><p>Product Name</p> <p>' . $product_name . $atr . '</p></div><div class="col-md-2 no-padding"><p>Code</p><p>' . $dwnldcode . '</p></div><div class="col-md-2 no-padding"><p>Qty</p><p>' . strtoupper($cartRow->quantity) . '</p></div><div class="col-md-2 no-padding"><p>Unit Price</p>
					<p>' . $this->data['currencySymbol'] . number_format($unitPrice, 2, '.', '') . '</p></div><div class="col-md-2 no-padding"><p>Sub Total</p><p>' . $this->data['currencySymbol'] . number_format($uTot, 2, '.', '') . '</p></div></div></div>';
			            $grantTotal = $grantTotal + $uTot;
			        }
					$private_total = $grantTotal - $purchaseList->row()->discountAmount;
			        $private_total = $private_total + $purchaseList->row()->tax + $purchaseList->row()->shippingcost;
					$items .= '<div class="product-price">
						<div class="container">
							<div class="pull-right">
								<div class="total-row clearfix ">
									<p>Sub Total</p>
									<p>' . $this->data['currencySymbol'] . number_format($grantTotal, '2', '.', '') . '</p>
								</div>
								<div class="total-row clearfix">
									<p>Discount Amount</p>
									<p>' . $this->data['currencySymbol'] . number_format($purchaseList->row()->discountAmount, '2', '.', '') . '</p>
								</div>
								<div class="total-row clearfix">
									<p>Shipping Cost</p>
									<p>' . $this->data['currencySymbol'] . number_format($purchaseList->row()->shippingcost, 2, '.', '') . '</p>
								</div>
								<div class="total-row clearfix">
									<p>Shipping Tax</p>
									<p>' . $this->data['currencySymbol'] . number_format($purchaseList->row()->tax, 2, '.', '') . '</p>
								</div>
								<div class="total-row clearfix">
									<p>Grand Total</p>
									<p>' . $this->data['currencySymbol'] . number_format($private_total, '2', '.', '') . '</p>
								</div>
							</div>
						</div>
					</div>';
					$this->data['items'] = $items;
					$this->data['appWeb'] = "1";
					$this->load->view('mobile/userSettings/invoice', $this->data);
				}
			}else{
				$returnArr['response'] = ' Invoice Number Null !!';
				echo json_encode($returnArr);
			}
        }
    }


	/*
	|| >>>>>>>>>>>>>>>>> Web View Review - Purchase <<<<<<<<<<<<<<<<  ||
	*/
	public function productReview() {
		$returnArr['status'] = (string)0;
		$returnArr['response'] = '';
		$UserId = intval($_GET['UserId']);
		$this->data['UserId'] = $UserId;
        if ($UserId == '') {
            $returnArr['response'] = 'UserId Null !!';
			echo json_encode($returnArr);
        } else {
			$sid = intval($_GET['sellerId']);
			if($sid == ""){
				$returnArr['response'] = 'SellerId Null !!';
				echo json_encode($returnArr);
			}else{
				$dealCode = intval($_GET['InvoiceNumber']);
	            //$dealCode = $this->uri->segment(4, 0);
				if($dealCode == ""){
					$returnArr['response'] = 'Invoice Number Null !!';
					echo json_encode($returnArr);
				}else{
		            $view_mode = 'user';
		         	if ($sid == $UserId) {
		                $view_mode = 'seller';
		            }
	                if ($view_mode == 'seller') {
	                    $this->db->select('p.*,pAr.attr_name as attr_type,sp.attr_name');
	                    $this->db->from(PAYMENT . ' as p');
	                    $this->db->join(SUBPRODUCT . ' as sp', 'sp.pid = p.attribute_values', 'left');
	                    $this->db->join(PRODUCT_ATTRIBUTE . ' as pAr', 'pAr.id = sp.attr_id', 'left');
	                    $this->db->where('p.sell_id = "' . $sid . '" and p.status = "Paid" and p.dealCodeNumber = "' . $dealCode . '"');
	                    $order_details = $this->db->get();
	                    //$order_details = $this->user_model->get_all_details(PAYMENT,array('dealCodeNumber'=>$dealCode,'status'=>'Paid','sell_id'=>$sid));
	                } else {
	                    //$order_details = $this->user_model->get_all_details(PAYMENT,array('dealCodeNumber'=>$dealCode,'status'=>'Paid'));
	                    $this->db->select('p.*,pAr.attr_name as attr_type,sp.attr_name');
	                    $this->db->from(PAYMENT . ' as p');
	                    $this->db->join(SUBPRODUCT . ' as sp', 'sp.pid = p.attribute_values', 'left');
	                    $this->db->join(PRODUCT_ATTRIBUTE . ' as pAr', 'pAr.id = sp.attr_id', 'left');
	                    $this->db->where("p.status = 'Paid' and p.dealCodeNumber = '" . $dealCode . "'");
	                    $order_details = $this->db->get();
	                }
	                if ($order_details->num_rows() == 0) {
						$message = "Sorry, No Such Order Found";
						if($this->lang->line('json_no_order') != ""){
						    $message = stripslashes($this->lang->line('json_no_order'));
						}
						$returnArr['response'] = $message;
						echo json_encode($returnArr);
	                } else {
	                    if ($view_mode == 'user') {
	                        $this->data['user_details'] = $this->user_model->get_all_details(USERS, array('id' => $UserId));
	                        $this->data['seller_details'] = $this->user_model->get_all_details(USERS, array('id' => $sid));
	                    } elseif ($view_mode == 'seller') {
	                        $this->data['user_details'] = $this->user_model->get_all_details(USERS, array('id' => $UserId));
	                        $this->data['seller_details'] = $this->user_model->get_all_details(USERS, array('id' => $sid));
	                    }
	                    foreach ($order_details->result() as $order_details_row) {
	                        $this->data['prod_details'][$order_details_row->product_id] = $this->user_model->get_all_details(PRODUCT, array('id' => $order_details_row->product_id));
	                    }
	                    $this->data['view_mode'] = $view_mode;
	                    $this->data['order_details'] = $order_details;
	                    $sortArr1 = array('field' => 'date', 'type' => 'desc');
	                    $sortArr = array($sortArr1);
	                    $this->data['order_comments'] = $this->user_model->get_all_details(REVIEW_COMMENTS, array('deal_code' => $dealCode), $sortArr);
						$this->data['appWeb'] = "1";
	                    $this->load->view('mobile/userSettings/OrderReviews', $this->data);
	                }
				}
			}
        }
    }

	public function change_received_status() {
            $status = $this->input->post('status');
            $rid = $this->input->post('rid');
            $this->user_model->update_details(PAYMENT, array('received_status' => $status), array('id' => $rid));
    }

	public function post_order_comment() {
		$datestring = "%Y-%m-%d %H:%i:%s";
		$time = time();
		$createdTime = mdate($datestring, $time);
		$userId = $this->input->post('commentor_id');
		$cmtI = $this->user_model->commonInsertUpdate(REVIEW_COMMENTS, 'insert', array(), array('date'=>$createdTime), '');
		$cmtID = $this->user_model->get_last_insert_id();
		$allDetails = $this->user_model->get_all_details(REVIEW_COMMENTS, array('id' => $cmtID))->result_array();
		$prodDetails = $this->user_model->get_all_details(PRODUCT, array('id' => $allDetails[0]['product_id']))->result_array();
		$datestring = "%Y-%m-%d %H:%i:%s";
		$time = time();
		$createdTime = mdate($datestring, $time);
		$actArr = array(
							 'activity' => 'review-comments',
							 'activity_id' => $prodDetails[0]['seller_product_id'],
							 'user_id' => $userId,
							// 'activity_ip' => $this->input->ip_address(),
							 'created' => $createdTime,
							 'comment_id' => $this->input->post('deal_code')
						);
		$this->send_comment_noty_mail($cmtID, $prodDetails[0]['seller_product_id']);
		$this->user_model->simple_insert(NOTIFICATIONS, $actArr);
	}

	public function userProductFeedback() {
		$product_id = $this->uri->segment(2);
		$UserId = $this->uri->segment(3);
        $id = array('id' => $product_id);
        $this->data['userVal'] = $this->product_model->get_all_details(PRODUCT, $id);
        $this->data['feedback_details'] = $this->product_model->get_all_details(PRODUCT_FEEDBACK, array('voter_id' => $UserId, 'product_id' => $product_id));
		$this->data['UserId'] = $UserId;
		$this->data['appWeb'] = "1";
        $this->load->view('mobile/userSettings/productFeedback', $this->data);
    }

	public function add_user_product_feedback() {
		$user_id = $this->input->post('rate');
		$rating = $this->input->post('rating_value');
		$title = $this->input->post('title');
		$description = $this->input->post('description');
		$product_id = $this->input->post('product_id');
		$seller_id = $this->input->post('seller_id');
		if ($user_id != '') {
			$this->user_model->simple_insert(PRODUCT_FEEDBACK, array('title' => $title, 'description' => $description, 'product_id' => $product_id, 'seller_id' => $seller_id, 'rating' => $rating, 'voter_id' => $user_id, 'status' => 'InActive'));
			if ($this->lang->line('ur_feedback_add_succ') != '')
				$lg_err_msg = $this->lang->line('ur_feedback_add_succ');
			else
				$lg_err_msg = 'Your feedback added successfully';
				$this->data['appWeb'] = "1";
				echo "<script>window.history.go(-1)</script>";
		}
	}

	/*
	|| >>>>>>>>>>>>>>>>> Get Store Fees <<<<<<<<<<<<<<<<  ||
	*/
	public function getStoreFees(){
		$stmnt = '';
		if ($this->lang->line('product_need_pay') != '') {
			$stmnt .= stripslashes($this->lang->line('product_need_pay'));
		} else $stmnt .= "You need to pay ";
			$stmnt .= $this->data['currencySymbol'].$this->config->item('storefront_fees_month');
		if ($this->lang->line('store_up_fees') != '') {
			$stmnt .= stripslashes($this->lang->line('store_up_fees'));
		} else $stmnt .= " for store fees per month";
			$stmnt2 ='';
		if ($this->lang->line('product_need_pay') != '') {
			$stmnt2 .= stripslashes($this->lang->line('product_need_pay'));
		} else {
			$stmnt2 .= "You need to pay ";
		}
		$stmnt2 .= $this->data['currencySymbol'].$this->config->item('storefront_fees_year');
		if ($this->lang->line('store_up_fees') != '') {
			$stmnt2 .=  stripslashes($this->lang->line('store_up_fees'));
		} else {
			$stmnt2 .=  " for store fees per year";
		}
		$fees[] = array(
			'month' =>$this->config->item('storefront_fees_month'),
			'year' =>$this->config->item('storefront_fees_year'),
		);
		$feesStatement[] = array(
			'month' => $stmnt,
			'year' => $stmnt2,
		);
		$returnArr['status'] = (string)1;
		$message = "Success";
		if($this->lang->line('json_success') != ""){
			$message = stripslashes($this->lang->line('json_success'));
		}
		$returnArr['response'] = $message;
		$returnArr['feesStatement'] = $feesStatement;
		$returnArr['fees'] = $fees;
		echo json_encode($returnArr);
	}

	/*
	|| >>>>>>>>>>>>>>>>> Open Store <<<<<<<<<<<<<<<<  ||
	*/

	public function openStore() {
		$returnArr['status'] = (string)0;
		$returnArr['response'] = '';
		$UserId = intval($_GET['UserId']);
        if ($UserId == '') {
            $returnArr['response'] = 'UserId Null !!';
			echo json_encode($returnArr);
        } else {
			$plan_value = intval($_GET['plan_value']);
			if($plan_value == ""){
				$message = "Choose Plan Type";
				if($this->lang->line('json_plan_type') != ""){
				    $message = stripslashes($this->lang->line('json_plan_type'));
				}
				$returnArr['response'] = $message;
				echo json_encode($returnArr);
			}else{
				$this->data['userDetails'] = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
	            $userType = $this->data['userDetails']->row()->group;
	            if ($userType == 'Seller') {
	                if ($this->data['userDetails']->row()->store_payment != 'Paid') {
	                        $InvoiceNo = mt_rand();
	                        $checkId = $this->store_model->get_all_details(STORE_ARB, array('invoice_no' => $InvoiceNo));
	                        while ($checkId->num_rows() > 0) {
	                            $InvoiceNo = mt_rand();
	                            $checkId = $this->store_model->get_all_details(STORE_ARB, array('invoice_no' => $InvoiceNo));
	                        }
							$this->data['UserId'] = $UserId;
							$this->data['InvoiceNo'] = $InvoiceNo;
							$this->data['plan_value'] = $plan_value;
		                    $this->data['editmode'] = '0';
		                    $this->data['countryList'] = $this->store_model->get_all_details(COUNTRY_LIST, array());
							$this->data['appWeb'] = "1";
		                    $this->load->view('mobile/checkout/store', $this->data);
	                } else {
						$message = "You have already subscribed to open a store";
						if($this->lang->line('json_alredy_subscribed_store') != ""){
						    $message = stripslashes($this->lang->line('json_alredy_subscribed_store'));
						}
						$returnArr['response'] = $message;
						echo json_encode($returnArr);
	                }
	            } else {
					$message = "You have already subscribed to open a store";
					if($this->lang->line('Sign up as Seller') != ""){
						$message = stripslashes($this->lang->line('Sign up as Seller'));
					}
					$returnArr['response'] = $message;
					echo json_encode($returnArr);
	            }
			}
        }
    }

	public function paypalsuccess() {
		$UserId = $this->uri->segment(4);
        if ($UserId != '') {
			$this->data['userDetails'] = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
			$invoice = trim($this->uri->segment(5));
            if ($invoice != "" && $UserId != "") {
                $status_chk = $this->store_model->get_all_details(STORE_ARB, array('invoice_no' => $invoice, 'status' => 'Paid'));
                if ($status_chk->num_rows() == 0) {
                    $pay_details = $this->store_model->get_all_details(STORE_ARB, array('invoice_no' => $invoice, 'status' => 'Pending'));
                    if ($pay_details->num_rows() > 0) {
                        $couponID = $pay_details->row()->couponid;
                        $couponAmont = $pay_details->row()->discount;
                        $couponType = $pay_details->row()->coupontype;
                        // Update Coupon
                        if ($couponID != 0) {
                            if ($couponType == 'Gift') {
                                $SelGift = $this->store_model->get_all_details(GIFTCARDS, array('id' => $couponID));
                                $GiftCountValue = $SelGift->row()->used_amount + $couponAmont;
                                $condition = array('id' => $couponID);
                                $dataArr = array('used_amount' => $GiftCountValue);
                                $this->store_model->update_details(GIFTCARDS, $dataArr, $condition);
                                if ($SelGift->row()->price_value <= $GiftCountValue) {

                                    $condition1 = array('id' => $couponID);
                                    $dataArr1 = array('card_status' => 'redeemed');
                                    $this->store_model->update_details(GIFTCARDS, $dataArr1, $condition1);
                                }
                            } else {
                                $SelCoup = $this->store_model->get_all_details(COUPONCARDS, array('id' => $couponID));
                                $CountValue = $SelCoup->row()->purchase_count + 1;
                                $condition = array('id' => $couponID);
                                $dataArr = array('purchase_count' => $CountValue);
                                $this->store_model->update_details(COUPONCARDS, $dataArr, $condition);
                            }
                        }
                        // Update Paid Status
                        $this->store_model->update_details(STORE_ARB, array('status' => 'Paid'), array('invoice_no' => $invoice));
                        $this->store_model->simple_insert(STORE_FRONT, array('user_id' => $UserId, 'store_name' => $this->data['userDetails']->row()->brand_name));
                        $dataArr = array(
                            'store_payment' => 'Paid'
//							'ref_comm_purchaser' => $this->config->item('ref_comm_purchaser_ug'),
//							'ref_comm_seller' => $this->config->item('ref_comm_seller_ug'),
//							'sell_commission' => $this->config->item('sell_commission_ug')
                        );
                        $condition = array('id' => $UserId);
                        $this->store_model->update_details(USERS, $dataArr, $condition);
                        //Send Mail
                        $this->send_subscription_success_mail($invoice, $UserId);
                        $this->data['Confirmation'] = 'subscription_success';
                        $this->data['errors'] = 'Payment Success';
						$this->data['appWeb'] = "1";
                        $this->load->view('mobile/order/storeSuccess.php', $this->data);
                    }
                }
            }
        }
    }

	public function send_subscription_success_mail($invoice_no = '', $UserId) {
		$this->data['userDetails'] = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
        $newsid = '30';
        $template_values = $this->store_model->get_newsletter_template_details($newsid);
        $email = $this->data['userDetails']->row()->email;
        $email_message = "Payment Success. Please update your store setting";
        $subject = $template_values['news_subject'];
        $adminnewstemplateArr = array('email_title' => $this->config->item('email_title'), 'logo' => $this->data['logo']);
        extract($adminnewstemplateArr);
        $header .="Content-Type: text/plain; charset=ISO-8859-1\r\n";
        $message .= '<!DOCTYPE HTML>
				<html>
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
				<meta name="viewport" content="width=device-width"/><body>';
        include('./newsletter/registeration' . $newsid . '.php');
        $message .= '</body>
				</html>';
        if ($template_values['sender_name'] == '' && $template_values['sender_email'] == '') {
            $sender_email = $this->data['siteContactMail'];
            $sender_name = $this->data['siteTitle'];
        } else {
            $sender_name = $template_values['sender_name'];
            $sender_email = $template_values['sender_email'];
        }
        $email_values = array('mail_type' => 'html',
            'from_mail_id' => $sender_email,
            'mail_name' => $sender_name,
            'to_mail_id' => $email,
            'cc_mail_id' => $this->config->item('site_contact_mail'),
            'subject_message' => $template_values['news_subject'],
            'body_messages' => $message,
            'mail_id' => 'register mail'
        );
        $email_send_to_common = $this->store_model->common_email_send($email_values);
    }

	public function paypalfailure() {

		$UserId = $this->uri->segment(4);
        if ($UserId != '') {
			$invoice = $this->uri->segment(5);
			// $cde = $this->uri->segment(6);
			// $cde_user = $this->uri->segment(7);
            // $cde = $this->session->userdata('cde');
            // $cde_user = $this->session->userdata('cde_user');
            // $invoice = $this->session->userdata('InvoiceNo');
            // $key = 'team-fancyy-clone-tweaks';
            if ($invoice != '') {
                $this->store_model->commonDelete(STORE_ARB, array('invoice_no' => $invoice));
            }
            // $this->session->unset_userdata('cde');
            // $this->session->unset_userdata('cde_user');
        if ($invoice != "" && $UserId != "") {
                $this->data['Confirmation'] = 'subscription_fail';
                $this->data['errors'] = 'Payment Failed';
				$this->data['appWeb'] = "1";
                $this->load->view('mobile/order/storeFailure.php', $this->data);
            }
        }
    }

	public function paypalnotify() {
		$UserId = $this->uri->segment(4);
		if ($UserId != '') {
			$invoice = $this->uri->segment(5);
			// $cde = $this->uri->segment(6);
			// $cde_user = $this->uri->segment(7);
            // $cde = $this->session->userdata('cde');
            // $cde_user = $this->session->userdata('cde_user');
            // $invoice = $this->session->userdata('InvoiceNo');
        //    $key = 'team-fancyy-clone-tweaks';
            if ($invoice != '') {
                $this->store_model->commonDelete(STORE_ARB, array('invoice_no' => $invoice));
            }
            // $this->session->unset_userdata('cde');
            // $this->session->unset_userdata('cde_user');
            if ($invoice != "" && $UserId != "") {
                $this->data['Confirmation'] = 'subscription_fail';
                $this->data['errors'] = 'Payment Failed';
				$this->data['appWeb'] = "1";
                $this->load->view('mobile/order/storeFailure.php', $this->data);
            }
        }
    }

	/*
	|| >>>>>>>>>> Store Settings <<<<<<<<<<< !!
	*/
	public function storeDetails() {
		$returnArr['status'] = (string)0;
		$returnArr['response'] = '';
		$UserId = intval($_GET['UserId']);
        if ($UserId == '') {
            $returnArr['response'] = 'UserId Null !!';
        } else {
            $store_Details = $this->store_model->get_all_details(STORE_FRONT, array('user_id' => $UserId));
			$storeDetails = array();
			if($store_Details->num_rows() >0){
				$storeLogo = base_url().'images/store/dummy-logo.png';
				if($store_Details->row()->logo_image != ""){
					$storeLogo = base_url().'images/store/'.$store_Details->row()->logo_image;
				}
				$storeCover = base_url().'images/store/banner-dummy.jpg';
				if($store_Details->row()->cover_image != ""){
					$storeCover = base_url().'images/store/'.$store_Details->row()->cover_image;
				}
				$storeDetails[] = array(
														'storeId' => $store_Details->row()->id,
														'UserId' => $store_Details->row()->user_id,
														'store_name' => $store_Details->row()->store_name,
														'storeLogo' => $storeLogo,
														'storeCover' => $storeCover,
														'tagline' => $store_Details->row()->tagline,
														'dateAdded' => $store_Details->row()->dateAdded
													);
				$returnArr['status'] = (string)1;
				$message = "Success";
			if($this->lang->line('json_success') != ""){
				$message = stripslashes($this->lang->line('json_success'));
			}
			$returnArr['response'] = $message;
				$returnArr['storeDetails'] = $storeDetails;
			}else{
				$message = "Please subscribe to open a store";
				if($this->lang->line('json_subs_open_store') != ""){
				    $message = stripslashes($this->lang->line('json_subs_open_store'));
				}
				$returnArr['response'] = $message;
			}
        }
		echo json_encode($returnArr);
    }

	/*
	|| >>>>>>>>>> Upadte Store Settings <<<<<<<<<<< !!
	*/

	public function InsertEditstore() {
		$returnArr['status'] = (string)0;
		$returnArr['response'] = '';
		$UserId = intval($_POST['UserId']);
        if ($UserId == '') {
            $returnArr['response'] = 'UserId Null !!';
        } else {
			if (!empty($_POST['logo-file'])) {
				$imgPath ='images/store/temp/';
				$img = $_POST['logo-file'];
				$imgName = time()."l.jpg";
				$imageFormat = array('data:image/jpeg;base64', 'data:image/png;base64', 'data:image/jpg;base64', 'data:image/gif;base64');
				$img = str_replace($imageFormat, '', $img);
				$data = base64_decode($img);
				$image = @imagecreatefromstring($data);
				if ($image !== false) {
					$uploadPath = $imgPath . $imgName;
					imagejpeg($image, $uploadPath, 100);
					imagedestroy($image);
					if (isset($uploadPath)) {
						/* Creating Thumbnail image with the size of 100 X 100 */
						$filename = base_url($uploadPath);
						list($width, $height) = getimagesize($filename);
						$newwidth = 100;
						$newheight = 100;
						//$thumbImage = @imagecreatetruecolor($newwidth, $newheight);
						$sourceImage = @imagecreatefromjpeg($filename);
						imagecopyresized($thumbImage, $sourceImage, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
						// if ($thumbImage !== false) {
						// 	/* Not uploading to thumb folder */
						// 	$thumbImgPath = 'images/users/thumb/';
						// 	$thumpUploadPath = $thumbImgPath . $imgName;
						// 	imagejpeg($thumbImage, $thumpUploadPath, 100);
						// }
						$new_name = $imgName;
						@copy("./images/store/temp/".$imgName, './images/store/'.$new_name);
						$dir = getcwd().DIRECTORY_SEPARATOR ."images".DIRECTORY_SEPARATOR ."store".DIRECTORY_SEPARATOR."temp".DIRECTORY_SEPARATOR ;
						$interval = strtotime('-24 hours');
						foreach (glob($dir."*.*") as $file){
							if (filemtime($file) <= $interval ){
								if(!is_dir($file) ){
									unlink($file);
								}
							}
						}
						$logo_image = $imgName;
					}else{
						$returnArr['response'] = 'There is something wrong in "Uploading Path" !!';
					}
				}else{
					$returnArr['response'] = 'There is something wrong with "Logo Image" !!';
				}
			}

			if (!empty($_POST['banner-file'])) {
				$imgPath ='images/store/temp/';
				$img = $_POST['banner-file'];
				$imgName = time()."b.jpg";
				$imageFormat = array('data:image/jpeg;base64', 'data:image/png;base64', 'data:image/jpg;base64', 'data:image/gif;base64');
				$img = str_replace($imageFormat, '', $img);
				$data = base64_decode($img);
				$image = @imagecreatefromstring($data);
				if ($image !== false) {
					$uploadPath = $imgPath . $imgName;
					imagejpeg($image, $uploadPath, 100);
					imagedestroy($image);
					if (isset($uploadPath)) {
						/* Creating Thumbnail image with the size of 100 X 100 */
						$filename = base_url($uploadPath);
						list($width, $height) = getimagesize($filename);
						$newwidth = 100;
						$newheight = 100;
						//$thumbImage = @imagecreatetruecolor($newwidth, $newheight);
						$sourceImage = @imagecreatefromjpeg($filename);
						imagecopyresized($thumbImage, $sourceImage, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
						// if ($thumbImage !== false) {
						// 	/* Not uploading to thumb folder */
						// 	$thumbImgPath = 'images/users/thumb/';
						// 	$thumpUploadPath = $thumbImgPath . $imgName;
						// 	imagejpeg($thumbImage, $thumpUploadPath, 100);
						// }
						$new_name = $imgName;
						@copy("./images/store/temp/".$imgName, './images/store/'.$new_name);
						$dir = getcwd().DIRECTORY_SEPARATOR ."images".DIRECTORY_SEPARATOR ."store".DIRECTORY_SEPARATOR."temp".DIRECTORY_SEPARATOR ;
						$interval = strtotime('-24 hours');
						foreach (glob($dir."*.*") as $file){
							if (filemtime($file) <= $interval ){
								if(!is_dir($file) ){
									unlink($file);
								}
							}
						}
						$banner_image = $imgName;
					}else{
						$returnArr['response'] = 'There is something wrong in "Uploading Path" !!';
					}
				}else{
					$returnArr['response'] = 'There is something wrong with "Banner Image" !!';
				}
			}

            $excludeArr = array('logo-file', 'banner-file', 'store_id', 'store_name','UserId');
            if ($logo_image == '' && $banner_image == '') {
                $dataArr = array();
            } elseif ($logo_image == '') {
                $dataArr = array('cover_image' => $banner_image);
            } elseif ($banner_image == '') {
                $dataArr = array('logo_image' => $logo_image);
            } else {
                $dataArr = array('logo_image' => $logo_image, 'cover_image' => $banner_image);
            }
            if ($this->input->post('store_id') == '') {
                $condition = array();
                $this->store_model->commonInsertUpdate(STORE_FRONT, 'insert', $excludeArr, $dataArr, $condition);
				$returnArr['status'] = (string)1;
				$message = "Store details saved successfully";
				if($this->lang->line('json_store_details_success') != ""){
				    $message = stripslashes($this->lang->line('json_store_details_success'));
				}
				$returnArr['response'] = $message;
            } else {
                $condition = array('id' => $this->input->post('store_id'));
                $this->store_model->commonInsertUpdate(STORE_FRONT, 'update', $excludeArr, $dataArr, $condition);
				$returnArr['status'] = (string)1;
				$message = "Store details updated successfully";
				if($this->lang->line('json_store_updated_success') != ""){
				    $message = stripslashes($this->lang->line('json_store_updated_success'));
				}
				$returnArr['response'] = $message;
            }
        }
		echo json_encode($returnArr);
    }


	/*
	|| >>>>>>>>>> Store Images Delete function <<<<<<<<<<< !!
	*/
	public function deleteStoreImage() {
		$returnArr['status'] = (string)0;
		$returnArr['response'] = '';
		$UserId = intval($_POST['UserId']);
        if ($UserId == '') {
            $returnArr['response'] = 'UserId Null !!';
        } else {
            $condition = array('user_id' => $UserId);
            if ($this->input->post('delete') == 'banner') {
                $dataArr = array('cover_image' => '');
            } else {
                $dataArr = array('logo_image' => '');
            }
            $this->store_model->update_details(STORE_FRONT, $dataArr, $condition);
            if ($this->input->post('delete') == 'banner') {
                $lg_err_msg = 'Store banner deleted successfully';
            } else {
                $lg_err_msg = 'store logo deleted successfully';
            }
			$returnArr['status'] = '1';
			$returnArr['response'] = $lg_err_msg;
        }
        echo json_encode($returnArr);
    }


	/*
	|| >>>>>>>>>> Store Privacy Settings <<<<<<<<<<< !!
	*/
	public function storePrivacySettings() {
		$returnArr['status'] = (string)0;
		$returnArr['response'] = '';
		$UserId = intval($_GET['UserId']);
		if ($UserId == '') {
			$returnArr['response'] = 'UserId Null !!';
        } else {
            $store_Details = $this->store_model->get_all_details(STORE_FRONT, array('user_id' => $UserId));
			if($store_Details->num_rows() >0 ){
				$storeDetails[] = array(
													'storeId' => $store_Details->row()->id,
													'privacy' => $store_Details->row()->privacy
												);
				$returnArr['status'] = (string)1;
				$message = "Success";
			if($this->lang->line('json_success') != ""){
				$message = stripslashes($this->lang->line('json_success'));
			}
			$returnArr['response'] = $message;
				$returnArr['storeDetails'] = $storeDetails;
			}
        }
		echo json_encode($returnArr);
    }

	/*
	|| >>>>>>>>>> Store Privacy update <<<<<<<<<<< !!
	*/
	public function storePrivacyUpdate() {
		$returnArr['status'] = (string)0;
		$returnArr['response'] = '';
		$UserId = intval($_POST['UserId']);
		if ($UserId == '') {
			$returnArr['response'] = 'UserId Null !!';
        } else {
            $excludeArr = array('storeId','UserId');
            $dataArr = array('privacy' => $this->input->post('privacy'), 'user_id' => $this->input->post('UserId'));
            if ($this->input->post('storeId') == '') {
                $condition = array();
                $this->store_model->commonInsertUpdate(STORE_FRONT, 'insert', $excludeArr, $dataArr, $condition);
				$returnArr['status'] = (string)1;
				$message = "Store details saved successfully";
				if($this->lang->line('json_store_details_success') != ""){
				    $message = stripslashes($this->lang->line('json_store_details_success'));
				}
				$returnArr['response'] = $message;
            } else {
                $condition = array('id' => $this->input->post('storeId'));
                $this->store_model->commonInsertUpdate(STORE_FRONT, 'update', $excludeArr, $dataArr, $condition);
				$returnArr['status'] = (string)1;
				$message = "Store details updated successfully";
				if($this->lang->line('json_store_updated_success') != ""){
				    $message = stripslashes($this->lang->line('json_store_updated_success'));
				}
				$returnArr['response'] = $message;
            }
        }
		echo json_encode($returnArr);
    }

	/*
	|| >>>>>>>>>> Store About Settings <<<<<<<<<<< !!
	*/
	public function storeAboutSettings() {
		$returnArr['status'] = (string)0;
		$returnArr['response'] = '';
		$UserId = intval($_GET['UserId']);
		if ($UserId == '') {
			$returnArr['response'] = 'UserId Null !!';
        } else {
            $store_Details = $this->store_model->get_all_details(STORE_FRONT, array('user_id' => $UserId));
			if($store_Details->num_rows() >0 ){
				$storeDetails[] = array(
													'storeId' => $store_Details->row()->id,
													'description' => strip_tags($store_Details->row()->description)
												);
				$returnArr['status'] = (string)1;
				$message = "Success";
				if($this->lang->line('json_success') != ""){
					$message = stripslashes($this->lang->line('json_success'));
				}
				$returnArr['response'] = $message;
				$returnArr['storeDetails'] = $storeDetails;
			}
        }
		echo json_encode($returnArr);
    }

	/*
	|| >>>>>>>>>> Store About update <<<<<<<<<<< !!
	*/
	public function storeAboutUpdate() {
		$returnArr['status'] = (string)0;
		$returnArr['response'] = '';
		$UserId = intval($_POST['UserId']);
		if ($UserId == '') {
			$returnArr['response'] = 'UserId Null !!';
        } else {
            $excludeArr = array('storeId','UserId');
            $dataArr = array('description' => $this->input->post('description'), 'user_id' => $this->input->post('UserId'));
            if ($this->input->post('storeId') == '') {
                $condition = array();
                $this->store_model->commonInsertUpdate(STORE_FRONT, 'insert', $excludeArr, $dataArr, $condition);
				$returnArr['status'] = (string)1;
				$message = "Store details saved successfully";
				if($this->lang->line('json_store_details_success') != ""){
				    $message = stripslashes($this->lang->line('json_store_details_success'));
				}
				$returnArr['response'] = $message;
            } else {
                $condition = array('id' => $this->input->post('storeId'));
                $this->store_model->commonInsertUpdate(STORE_FRONT, 'update', $excludeArr, $dataArr, $condition);
				$returnArr['status'] = (string)1;
				$message = "Store details updated successfully";
				if($this->lang->line('json_store_updated_success') != ""){
				    $message = stripslashes($this->lang->line('json_store_updated_success'));
				}
				$returnArr['response'] = $message;
            }
        }
		echo json_encode($returnArr);
    }

	/*
	|| >>>>>>>>>> Store Product Settings <<<<<<<<<<< !!
	*/
	public function productSettings() {
		$returnArr['status'] = (string)0;
		$returnArr['response'] = '';
		$UserId = intval($_GET['UserId']);
		if ($UserId == '') {
			$returnArr['response'] = 'UserId Null !!';
		}else{
			$WantProductDetail = $this->store_model->get_all_details(PRODUCT, array('user_id' => $UserId));
			$thisUserProfileDetails = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
			foreach($WantProductDetail->result() as $wantsProduct){
				$cond = "select user_id from ".PRODUCT_LIKES." where product_id=".$wantsProduct->seller_product_id." and user_id=".$UserId;
				$likedUsersList = $this->mobile_model->ExecuteQuery($cond);
				$likedStatus = "No";
				if($likedUsersList->num_rows() > 0){
					$likedStatus = "Yes";
				}
				$imgArr=@explode(',',$wantsProduct->image);
				/* $productImage = ($imgArr[0]=='') ? '' : base_url().'images/product/'.$imgArr[0]; */
				$images=array();
				foreach($imgArr as $img){
					if($img!="")$images[]=base_url().'images/product/'.$img;
				}
						$WantsProductDetail[]=array(
							'product_name'=>$wantsProduct->product_name,
							'full_name'=>$thisUserProfileDetails->row()->full_name,
							'user_name'=>$thisUserProfileDetails->row()->user_name,
							'id'=>$wantsProduct->id,
							'UserId' =>$wantsProduct->user_id,
							'price'=>$wantsProduct->price,
							'sale_price'=>$wantsProduct->sale_price,
							'seller_product_id'=>$wantsProduct->seller_product_id,
							'user_id'=>$wantsProduct->user_id,
							'likes'=>$wantsProduct->likes,
							'liked' => $likedStatus,
							'image'=>$images,
							'status'=> $wantsProduct->status,
							'description'=>strip_tags($wantsProduct->description),
							'shippingPolicies'=>strip_tags($wantsProduct->shipping_policies),
							'purchased'=>$wantsProduct->purchasedCount,
							'remain'=>$wantsProduct->quantity,
							'productUrl'=>base_url().'json/product?pid='.$wantsProduct->id.'&UserId='.$UserId,
							'shareUrl' => base_url().'things/'.$wantsProduct->id.'/'.$wantsProduct->product_name.'?ref='.$thisUserProfileDetails->row()->user_name,
							'seourl'=>$wantsProduct->seourl,
							'type' => 'SellingProduct',
							'UserTimeline' => 'http://192.168.1.251:8081/product-working/fancyclone-v3/json/user/seller-timeline?username='.$wantsProduct->user_name
						);
			}
			$returnArr['status'] = (string)1;
			$message = "Success";
			if($this->lang->line('json_success') != ""){
				$message = stripslashes($this->lang->line('json_success'));
			}
			$returnArr['response'] = $message;
			$returnArr['ProductDetails'] = $WantsProductDetail;
		}
		echo json_encode($returnArr);
    }


	/*
	|| >>>>>>>>>> Store Product Status Settings <<<<<<<<<<< !!
	*/
	public function ProductChangeStatus() {
		$returnArr['status'] = (string)0;
		$returnArr['response'] = '';
		$UserId = intval($_POST['UserId']);
        if ($UserId != '') {
			$pid = $this->input->post('productSellerId');
            $mode = $this->input->post('mode');
            $status = $this->input->post('status');
			if($mode != ""){
				if ($mode == 'sellingProduct') {
	                $tbl = PRODUCT;
	            } else {
	                $tbl = USER_PRODUCTS;
	            }
	            if ($pid != '' && $status != "") {
	                $product_details = $this->product_model->get_all_details($tbl, array('seller_product_id' => $pid));
	                if ($product_details->num_rows() > 0) {
	                    $dataArr = array('status' => $status);
	                    $product_details = $this->product_model->update_details($tbl, $dataArr, array('seller_product_id' => $pid));
						$returnArr['status'] = (string)1;
						$message = "Status changed successfully";
						if($this->lang->line('json_status_success') != ""){
						    $message = stripslashes($this->lang->line('json_status_success'));
						}
						$returnArr['response'] = $message;
	                }
	            }else{
					$returnArr['response'] = 'Some parameters are missing !!';
				}
			}else{
				$returnArr['response'] = 'Mode null !!';
			}
        } else {
            $returnArr['response'] = 'UserId null !!';
        }
		echo json_encode($returnArr);
    }

	/*
	|| >>>>>>>>>> Store Product Status Settings <<<<<<<<<<< !!
	*/
	// public function ChangeShippingAddress() {
	// 	$returnArr['status'] = (string)0;
	// 	$returnArr['response'] = '';
	// 	$UserId = intval($_POST['UserId']);
	// 	$add_id  = $_POST['shipId'];
	// 	$amt  = $_POST['cartAmount'];
	// 	$disamt  = $_POST['discountAmount'];
	// 	$productId = $_POST['productId'];
	// 	$productQuantity = $_POST['productQty'];
	// 	if ($add_id !="" && $amt !="" && $disamt !="" && $productId !="" && $productQuantity !="" && $UserId != "") {
	// 		$ChangeAdds = $this->cart_model->get_all_details(SHIPPING_ADDRESS, array('user_id' => $UserId, 'id' => $add_id));
	// 		$ShipCostVal = $this->cart_model->get_all_details(COUNTRY_LIST, array('country_code' => $ChangeAdds->row()->country));
	// 		$MainShipCost = number_format($ShipCostVal->row()->shipping_cost, 2, '.', '');
	//
	// 		$countryTaxCost = $ShipCostVal->row()->shipping_tax;
	// 		if($productId != ""){
	// 			$productId = rtrim($productId, ",");
	// 			$pid = explode(',',$productId);
	// 			$productQuantity = rtrim($productQuantity, ",");
	// 			$pQty = explode(',',$productQuantity);
	// 			$productCount = count($pid);
	// 			$MainShipCost = 0;
	// 			$separateProductCost = '';
	// 			for($i=0; $i<$productCount;$i++){
	// 				$ShipCostVal = $this->cart_model->get_all_details(PRODUCT, array('id' =>$pid[$i]));
	// 				$cond = "select separate_ship_cost from ".SHIPPING_COST." where product_id=".$ShipCostVal->row()->seller_product_id." && country_code='".$ChangeAdds->row()->country."'";
	// 				$separateShippingCost = $this->cart_model->ExecuteQuery($cond);
	// 				if(intval($separateShippingCost->row()->separate_ship_cost) != ""){
	// 					$productShippingCost = $pQty[$i] * (intval($separateShippingCost->row()->separate_ship_cost));
	// 					$MainShipCost = $MainShipCost + $productShippingCost;
	// 					$separateProductCost[] =array("productShippingCost".$i => number_format(($productShippingCost), 2, '.', ''));
	// 				}else{
	// 					$productShippingCost = $pQty[$i] * (intval($ShipCostVal->row()->shipping_cost));
	// 					$MainShipCost = $MainShipCost + $productShippingCost;
	// 					$separateProductCost[] =array("productShippingCost".$i => number_format(($productShippingCost), 2, '.', ''));
	// 				}
	// 			}
	// 			$MainTaxCost = number_format(($amt * 0.01 * $countryTaxCost), 2, '.', '');
	// 			$TotalAmts = number_format(( ($amt + $MainShipCost + $MainTaxCost) - $disamt), 2, '.', '');
	// 			$condition = array('user_id' => $UserId);
	// 			$dataArr2 = array('shipping_cost' => $MainShipCost, 'tax' => $countryTaxCost);
	// 			$this->cart_model->update_details(SHOPPING_CART, $dataArr2, $condition);
	// 			$cost[] = array(
	// 				"TotalShipCost" => number_format(($MainShipCost), 2, '.', ''),
	// 				"TotalTaxAmount" => number_format(($countryTaxCost), 2, '.', ''),
	// 				"TotalTaxCost" => number_format(($MainTaxCost), 2, '.', ''),
	// 				"GrantTotal" => number_format(($TotalAmts), 2, '.', ''),
	// 			);
	// 			$returnArr['status'] = (string)1;
	// 			$message = "Success";
			// if($this->lang->line('json_success') != ""){
			// 	$message = stripslashes($this->lang->line('json_success'));
			// }
			// $returnArr['response'] = $message;
	// 			$returnArr["ProductShippingCost"] = $separateProductCost;
	// 			$returnArr["Cost"] = $cost;
	// 		}
	// 	} else {
	// 		$returnArr['response'] = 'Some parameters are missing !!';
	// 	}
	// 	echo json_encode($returnArr);
	// }

	public function ChangeShippingAddress() {
		$returnArr['status'] = (string)0;
		$returnArr['response'] = '';
		$UserId = intval($_POST['UserId']);
		$add_id  = $_POST['shipId'];
		$amt  = $_POST['cartAmount'];
		$disamt  = $_POST['discountAmount'];
        if($add_id != '' && $UserId != "" && $amt !="" && $disamt != "") {
            $ChangeAdds = $this->cart_model->get_all_details(SHIPPING_ADDRESS, array('user_id' => $UserId, 'id' => $add_id));
            $ShipCostVal = $this->cart_model->get_all_details(COUNTRY_LIST, array('country_code' => $ChangeAdds->row()->country));
            $MainShipCost = number_format($ShipCostVal->row()->shipping_cost, 2, '.', '');
            $MainTaxCost = number_format(($amt * 0.01 * $ShipCostVal->row()->shipping_tax), 2, '.', '');
            $TotalAmts = number_format((($amt + $MainShipCost + $MainTaxCost) - $disamt), 2, '.', '');
            $condition = array('user_id' => $UserId);
            $dataArr2 = array('shipping_cost' => $MainShipCost, 'tax' => $ShipCostVal->row()->shipping_tax);
            $this->cart_model->update_details(SHOPPING_CART, $dataArr2, $condition);
			$returnArr['status'] = (string)1;
			$message = "Success";
			if($this->lang->line('json_success') != ""){
				$message = stripslashes($this->lang->line('json_success'));
			}
			$returnArr['response'] = $message;
        } else {
            $returnArr['response'] = 'Some parameters are missing !!';
        }
		echo json_encode($returnArr);
    }


	/*
	|| >>>>>>>>>> Product Attribute Count Checking <<<<<<<<<<< !!
	*/
	public function productCartAttributreCountCheck() {
		$returnArr['status'] = (string)0;
		$returnArr['response'] = '';
		$UserId = intval($_POST['UserId']);
		if($UserId != ""){
			$attributeId = trim($_POST['attributeId']);
			$product_id = trim($_POST['product_id']);
			if($attributeId != "" && $product_id != ""){
				$attrPriceVal = $this->product_model->get_all_details(SUBPRODUCT, array('pid' => $attributeId, 'product_id' => $product_id));
				$attributeDetails = array();
		        if ($attrPriceVal->num_rows() == 0) {
		            $product_details = $this->product_model->get_all_details(PRODUCT, array('id' => $product_id));
		            //echo '0|' . $product_details->row()->sale_price;
					$attributeDetails = array(
						"attributeId" => "",
						"attributePrice" => "",
						"attributeQuantity" => "",
						"productPrice" => $product_details->row()->sale_price,
					);
		        } else {
		            //echo $attrPriceVal->row()->attr_id . '|' . $attrPriceVal->row()->attr_price . '|' . $attrPriceVal->row()->attr_qty;
					$attributeDetails = array(
						"attributeId" => $attrPriceVal->row()->attr_id,
						"attributePrice" => $attrPriceVal->row()->attr_price,
						"attributeQuantity" => $attrPriceVal->row()->attr_qty,
						"productPrice" => "",
					);
		        }
				$returnArr['status'] = (string)1;
				$message = "Success";
			if($this->lang->line('json_success') != ""){
				$message = stripslashes($this->lang->line('json_success'));
			}
			$returnArr['response'] = $message;
				$returnArr['attributeDetails'] = $attributeDetails;
			}else{
				$returnArr['response'] = 'Some Parameters are Missing !!';
			}
		}else{
			$returnArr['response'] = 'UserId Null !!';
		}
		echo json_encode($returnArr);
    }
/*
	|| <<<<<<<<<<<<< seller contact  Details >>>>>>>>>>> ||
	*/
	 public function contactform() {
	 	$returnArr['status'] = '0';
		$returnArr['response'] = '';
		$user_id = $this->input->post('user_id');
		$seller_id = $this->input->post('seller_id');
        $product_id = $this->input->post('product_id');
        $question = $this->input->post('message');
        $subject = $this->input->post('subject');
       if($user_id != "" && $seller_id != "" && $product_id != "" && $subject !=""){
	        $dataArrVal = array();
	        $userdetails = $this->product_model->get_all_details(USERS, array('id' =>$user_id));
	        if($userdetails->num_rows() > 0){
		         $userrowdetals = $userdetails->result();
		        $sellerdetails = $this->product_model->get_all_details(USERS, array('id' =>$seller_id));
		        if($sellerdetails->num_rows() > 0){
		         $sellerrowdetals = $userdetails->result();
		         $dataArrVal = array(
		         					'question'=>$question,
		         					'name'=>$userrowdetals[0]->user_name,
		         					'email'=>$userrowdetals[0]->email,
		         					'selleremail'=>$sellerrowdetals[0]->email,
		         					'sellerid'=>$seller_id,
		         					'product_id'=>$product_id
		         					);
		        $this->product_model->simple_insert(CONTACTSELLER, $dataArrVal);
		      /*echo   $this->user_model->get_last_insert_id();
		      echo   $this->db->last_query();
		      die;*/
		        //$contact_id = $this->product_model->get_last_insert_id();
		        $this->data['productVal'] = $this->product_model->get_all_details(PRODUCT, array('id' =>$product_id));
		        $newimages = array_filter(@explode(',', $this->data['productVal']->row()->image));
		        $newsid = '20';
		        $template_values = $this->product_model->get_newsletter_template_details($newsid);
		        $subject = 'From: ' . $this->config->item('email_title') . ' - ' . $template_values['news_subject'];
		        $adminnewstemplateArr = array('email_title' => $this->config->item('email_title'), 'logo' => $this->data['logo'],
		            'name' => $this->input->post('name'),
		            'question' => $this->input->post('question'),
		            'email' => $this->input->post('email'),
		            'phone' => $this->input->post('phone'),
		            'productId' => $this->data['productVal']->row()->id,
		            'productName' => $this->data['productVal']->row()->product_name,
		            'productSeourl' => $this->data['productVal']->row()->seourl,
		            'productImage' => $newimages[0],
		        );
		        extract($adminnewstemplateArr);
		        //$ddd =htmlentities($template_values['news_descrip'],null,'UTF-8');
		        $header .="Content-Type: text/plain; charset=ISO-8859-1\r\n";
		        $message .= '<!DOCTYPE HTML>
					<html>
					<head>
					<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
					<meta name="viewport" content="width=device-width"/><body>';
		        include('./newsletter/registeration' . $newsid . '.php');
		        $message .= '</body>
					</html>';
		        if ($template_values['sender_name'] == '' && $template_values['sender_email'] == '') {
		            $sender_email = $this->data['siteContactMail'];
		            $sender_name = $this->data['siteTitle'];
		        } else {
		            $sender_name = $template_values['sender_name'];
		            $sender_email = $template_values['sender_email'];
		        }
		        $email_values = array('mail_type' => 'html',
		            'from_mail_id' =>$userrowdetals[0]->email,
		            'mail_name' => $sender_name,
		            'to_mail_id' => $sellerrowdetals[0]->email,
		            'cc_mail_id' => $this->config->item('site_contact_mail'),
		            'subject_message' => $subject,
		            'body_messages' => $message
		        );
		        $email_send_to_common = $this->product_model->common_email_send($email_values);
		        $returnArr['status'] = '1';
				$returnArr['response'] = 'Message Sent Successfully!';
		       }else{
		       		$returnArr['response'] = " Invalid Seller  ";
		       }
	    }else{
	    	$returnArr['response'] = " Invalid User ";
	    }
    }else{

			$returnArr['response'] = " Some parameters missing  ";

    }
    echo json_encode($returnArr);
   }
    public function getProductType(){
    	$returnArr['status'] = (string)0;
		$returnArr['response'] = '';
		$product_id = intval($_GET['product_id']);
		if($product_id != ""){
			$product_details = $this->product_model->get_all_details(PRODUCT, array('seller_product_id' => $product_id));
			if ($product_details->num_rows()>0) {
				$returnArr['status'] = (string)1;
				$message = "Success";
			if($this->lang->line('json_success') != ""){
				$message = stripslashes($this->lang->line('json_success'));
			}
			$returnArr['response'] = $message;
				$returnArr['product_type'] = $product_details->row()->product_type;
			}else{
				$message = "Product Not Avaliable";
				if($this->lang->line('json_product_not_avaliable') != ""){
				    $message = stripslashes($this->lang->line('json_product_not_avaliable'));
				}
				$returnArr['response'] = $message;
			}
		}else{
			$returnArr['response'] = 'Product Id Null !!';
		}
		echo json_encode($returnArr);
    }
/*  Digital product download  */

 public function digitalDownload() {
        $returnArr['status'] = (string)0;
		$returnArr['response'] = '';
		$user_id = $_GET['user_id'];
		$product_download_code = $_GET['code'];
		if($user_id !="" && $product_download_code !=""){
            $codeDetails = $this->product_model->get_all_details(PAYMENT, array('product_download_code' => $product_download_code, 'user_id' => $user_id, 'product_code_status' => "Pending"));
            if ($codeDetails->num_rows() == 1) {
                $id = $codeDetails->row()->id;
                $pid = $codeDetails->row()->product_id;
                $prodetails = $this->product_model->get_all_details(PRODUCT, array('id' => $pid));
                if ($prodetails->num_rows() > 0) {
                    $digital = $prodetails->row()->product_doc;
                    if(!empty($digital)){
                    	$imagesdigital = base_url().'pdf-doc/pdf/'.$digital;
                    }else{
                    	$imagesdigital = " No file ";

                    }
                    $returnArr['response'] = " success ";
                    $returnArr['digital_file'] = $imagesdigital;
                    $returnArr['digital_file_name'] = $digital;
                    $returnArr['status'] = 1;
                }else{
                	$returnArr['response'] = " No product  ";
                }
            } else {
                $returnArr['response'] = " No digital file   ";
            }
        }else{
        	$returnArr['response'] = " Some parameters missing  ";
        }
        echo json_encode($returnArr);
    }

	/*
	|| <<<<<<<<<<<<< SELLER FEATURE PLAN >>>>>>>>>>> ||
	*/
	public function sellerFeatured() {
		$returnArr['status'] = '0';
		$returnArr['response'] = '';
		$userId = $_POST['UserId'];
		if($userId != ""){
			$selectedProdIds = array();
			$featuredProductDetails = array();
			$sellingProductList = array();
			$selectedPlan = array();
			$paymentTypes = array();
			$this->load->model('seller_plan_model', 'seller_plan');
			$featuredStorePlan = $this->seller_plan->get_all_details(SELLER_PLAN, array('status'=> 'Active'));
			$planDetails = array();
			$featured = 0;
			$checkFeatured = $this->seller_plan->get_all_details(FEATURED_SELLER, array('userId' => $userId, 'isExpired' => '0'));
		//	echo "<pre>";print_r($checkFeatured->result());die;
			if ($checkFeatured->num_rows > 0){
				$selectedPlan = array(
											"featuredFrom" => $checkFeatured->row()->featuredOn,
											"featuredTo" => $checkFeatured->row()->expiresOn,
											"planPrice" => $checkFeatured->row()->planPrice,
											"planPeriod" => $checkFeatured->row()->planPeriod,
											"maxProductAllowed" => $checkFeatured->row()->productCount,
										);
				$featured = 1;
				$featuredProducts = $this->seller_plan->get_featured_product($userId, $checkFeatured->row()->id);
				if(count($featuredProducts) > 0){
					$i = 0;
					foreach ($featuredProducts as $deatils){
						$productImage =base_url().'images/product/dummyProductImage.jpg';
						if($deatils->image != ""){
							$img = explode(',',$deatils->image);
							$imgArr = array_filter($img);
							$productImage = base_url()."images/product/".$imgArr[0];
						}
						$selectedProdIds[$i] = $deatils->id;
						$featuredProductDetails[] = array(
																		"productID" => $deatils->id,
																		"sellerProductId" => $deatils->seller_product_id,
																		"productName" => $deatils->product_name,
																		"productImage" => $productImage
																	);
					    $i++;
					}
				}
				$sellerProducts = $this->product_model->view_product_details(' where p.user_id=' . $userId . ' and p.status="Publish"');
				if($sellerProducts->num_rows() > 0){
					foreach($sellerProducts->result() as $prodDetails){
						$productImage =base_url().'images/product/dummyProductImage.jpg';
						if($prodDetails->image != ""){
							$img = explode(',',$prodDetails->image);
							$imgArr = array_filter($img);
							$productImage = base_url()."images/product/".$imgArr[0];
						}
						$selected = 0;
						if(in_array($prodDetails->id, $selectedProdIds)){
							$selected = 1;
						}
						$sellingProductList[] = array(
																"productID" => $prodDetails->id,
																"sellerProductId" => $prodDetails->seller_product_id,
																"productName" => $prodDetails->product_name,
																"productImage" => $productImage,
																"selected" => $selected
															);
					}
				}
			}else{
				if($featuredStorePlan->num_rows() > 0){
					foreach ($featuredStorePlan->result() as $ftStrePlan) {
						$planDetails[] = array(
													"planId" => $ftStrePlan->id,
													"planName" => $ftStrePlan->plan_name,
													"planPrice" => $ftStrePlan->plan_price,
													"planPeriod" => $ftStrePlan->plan_period,
													"maxProductAllowed" => $ftStrePlan->plan_promoted_count,
												);
					}
					$paymentGateways = $this->mobile_model->get_all_details(PAYMENT_GATEWAY, array('status' => "Enable"));
					$paymentTypes = array();
					if($paymentGateways->num_rows() > 0){
						foreach ($paymentGateways->result() as $gatewayData) {
							if($gatewayData->id != 4 && $gatewayData->id != 7){
								$paymentTypes[] = array(
														"paymentId" => $gatewayData->id,
														"paymentName" => $gatewayData->gateway_name,
													);
							}
						}
					}
				}
			}
			$returnArr['status'] = 1;
			$returnArr['response'] = 'Success';
			$returnArr['featured'] = $featured;
			$returnArr['planDetails'] = $planDetails;
			$returnArr['paymentTypes'] = $paymentTypes;
			$returnArr['featuredPlanDetails'] = $selectedPlan;
			$returnArr['featuredProductDetails'] = $featuredProductDetails;
			$returnArr['sellingProductList'] = $sellingProductList;
		}else{
			$returnArr['response'] = 'UserId Null !!';
		}
		echo json_encode($returnArr);
	}

	/*
	|| <<<<<<<<<<<<< SELLER FEATURE PLAN >>>>>>>>>>> ||
	*/
	public function payFeaturedPlan() {
		$userId = $_GET['UserId'];
		if($userId != ""){
			$planId = $_GET['planId'];
			if($planId != ""){
				$paymentId = $_GET['paymentId'];
				if($paymentId != ""){
					if($paymentId == "1"){
						$redirect = 'paypalPay';
					}else if($paymentId == "2"){
						$redirect = 'paypalCreditPayment';
					}else if($paymentId == "3"){
						$redirect = 'authorizePayment';
					}else if($paymentId == "4"){
						$redirect = 'stripePayment';
					}else if($paymentId == "5"){
						$redirect = 'featureTwoCheckout';
					}
					redirect(base_url() . 'site/ios/' . $redirect.'/'.$planId.'/'.$userId);
				}else{
					$returnArr['response'] = 'PaymentId Null !!';
				}
			}else{
				$returnArr['response'] = 'PlanId Null !!';
			}
		}else{
			$returnArr['response'] = 'UserId Null !!';
		}
		echo json_encode($returnArr);
	}

	/*
	|| <<<<<<<<<<<<< SELLER FEATURE PLAN PAYPAL PAY >>>>>>>>>>> ||
	*/
	public function paypalPay($planId = "", $userId = "") {
		if($userId != ""){
			if($planId != ""){
				$condition = array('id' => $userId, "status" => "Active");
				$userInfoDetails = $this->mobile_model->get_all_details(USERS,$condition);
				if($userInfoDetails->num_rows() == 1){
					$this->data['userDetails'] = $userInfoDetails;
					$this->load->model('seller_plan_model', 'seller_plan');
			        $this->data['plan'] = $this->seller_plan->get_all_details(SELLER_PLAN, array('id'=>$planId));
					$this->data['countryList'] = $this->mobile_model->get_all_details(COUNTRY_LIST, array(), array(array('field' => 'name', 'type' => 'asc')));
			        $this->load->view('mobile/featured/featuredPayPaypal', $this->data);
				}else{
					$this->data['errorMessage'] = "No Such User Found";
					$this->load->view("mobile/featured/failed",$this->data);
				}
			}else{
				$this->data['errorMessage'] = "PlanId Missing";
				$this->load->view("mobile/featured/failed",$this->data);
			}
		}else{
			$this->data['errorMessage'] = "UserId Missing";
			$this->load->view("mobile/featured/failed",$this->data);
		}
    }

	/*
	|| <<<<<<<<<<<<< SELLER FEATURE PLAN PAYPAL PAYMENT >>>>>>>>>>> ||
	*/
	public function paypal_payment() {
		//print_r($_POST);die;
		$UserId = $_POST['user_id'];
		if($UserId != ""){
			$this->load->model('seller_plan_model', 'seller_plan');
	        $listCheck = $this->seller_plan->get_all_details(FEATURED_SELLER, array('userId' => $UserId, 'isExpired' => '0'));
	        if ($listCheck->num_rows == 0) {
	            $plan = $this->seller_plan->get_all_details(SELLER_PLAN, array('id'=>$this->input->post('plan_id')));
	            $registeredSellers = $this->seller_plan->get_all_details(FEATURED_SELLER, array('isExpired' => '0'));
	            if ($registeredSellers->num_rows < $plan->row()->plan_promoted_count){
	                $time = time();
	                $datestring = "%Y-%m-%d %h:%i:%s";
	                $dataArr = array('userId' => $UserId, 'planPrice' => $plan->row()->plan_price, 'planPeriod' => $plan->row()->plan_period, 'productCount' => $plan->row()->plan_product_count, 'paymentType' => 'Paypal');
	                $dataArr['createdOn'] = mdate($datestring, $time);
	                $this->seller_plan->simple_insert('fc_featured_seller_temp', $dataArr);
	                $temp_id = $this->db->insert_id();
	                $this->load->library('paypal_class');
	                $item_name = $this->config->item('email_title') . ' Products';
	                $totalAmount = $this->input->post('total_price');
	                $loginUserId = $UserId;
	                //DealCodeNumber
	                //$lastFeatureInsertId = $this->session->userdata('randomNo');
	                $quantity = 1;
	                if ($this->input->post('paypalmode') == 'sandbox') {
	                    $this->paypal_class->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';   // testing paypal url
	                } else {
	                    $this->paypal_class->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';     // paypal url
	                }
	                $this->paypal_class->add_field('currency_code', $this->data['currencyType']);

	                $this->paypal_class->add_field('business', $this->input->post('paypalEmail')); // Business Email

	                $this->paypal_class->add_field('return', base_url() . 'site/ios/paypal_return/' . $temp_id.'?AppWebView=1'); // Return URL

	                $this->paypal_class->add_field('cancel_return', base_url() . 'site/ios/paypal_cancel/' . $temp_id.'?AppWebView=1'); // Cancel URL

	                $this->paypal_class->add_field('notify_url', base_url() . 'site/ios/paypal_notify/' . $temp_id.'?AppWebView=1'); // Notify url

	                $this->paypal_class->add_field('custom', $loginUserId . '|' . $temp_id . '|Product'); // Custom Values

	                $this->paypal_class->add_field('item_name', $item_name); // Product Name

	                $this->paypal_class->add_field('user_id', $loginUserId);

	                $this->paypal_class->add_field('quantity', $quantity); // Quantity
	                //echo $totalAmount;die;
	                $this->paypal_class->add_field('amount', $totalAmount); // Price
	                //$this->paypal_class->add_field('amount', 1); // Price
	                //			echo base_url().'order/success/'.$loginUserId.'/'.$lastFeatureInsertId; die;
	                $this->paypal_class->submit_paypal_post();
	            } else {
					$this->data['errorMessage'] = "'Maximum Number of featured Sellers reached";
		            $this->load->view("mobile/featured/failed",$this->data);
	            }
	        } else {
				$this->data['errorMessage'] = "You have Already Subscribed this Plan";
	            $this->load->view("mobile/featured/failed",$this->data);
	        }
		}else{
			$this->data['errorMessage'] = "User Id Missing";
			$this->load->view("mobile/featured/failed",$this->data);
		}
    }

	/*
	|| <<<<<<<<<<<<< SELLER FEATURE PLAN PAYPAL PAYMENT RETURN >>>>>>>>>>> ||
	*/
	public function paypal_return($id) {
	//	print_r($_POST);die;
        if (strtolower($this->input->post('payment_status')) == 'completed') {
            $this->load->model('seller_plan_model', 'seller_plan');
            $temp_store = $this->seller_plan->get_all_details('fc_featured_seller_temp', array('id' => $id));
            if ($temp_store->num_rows > 0) {
                $this->seller_plan->commonDelete('fc_featured_seller_temp', array('id' => $id));
                $userExists = $this->seller_plan->get_all_details(FEATURED_SELLER, array('userId' => $temp_store->row()->userId, 'isExpired' => '0'));
                if ($userExists->num_rows == 0) {
                    $dataArr = array('userId' => $temp_store->row()->userId, 'planPrice' => $temp_store->row()->planPrice, 'planPeriod' => $temp_store->row()->planPeriod, 'productCount' => $temp_store->row()->productCount, 'paymentType' => $temp_store->row()->paymentType);
                    $time = time();
                    $datestring = "%Y-%m-%d %h:%i:%s";
                    $dataArr['featuredOn'] = mdate($datestring, $time);
                    $dataArr['expiresOn'] = date("Y-m-d h:i:s", strtotime($dataArr['featuredOn'] . '+ ' . $temp_store->row()->planPeriod . ' days'));
                    $dataArr['createdOn'] = mdate($datestring, $time);
                    $this->seller_plan->simple_insert(FEATURED_SELLER, $dataArr);
                    $this->seller_plan->commonDelete('fc_featured_seller_temp', array('id' => $id));
					$this->data['errorMessage'] = "Payment Successfull";
					$this->load->view("mobile/featured/success",$this->data);
                }
            }
        } else {
			$this->data['errorMessage'] = "Payment Unsuccessfull";
			$this->load->view("mobile/featured/failed",$this->data);
        }
    }

	/*
	|| <<<<<<<<<<<<< SELLER FEATURE PLAN PAYPAL PAYMENT CANCEL >>>>>>>>>>> ||
	*/
	public function paypal_cancel($id) {
        $this->load->model('seller_plan_model', 'seller_plan');
        $temp_store = $this->seller_plan->get_all_details('fc_featured_seller_temp', array('id' => $id));
        if ($temp_store->num_rows > 0) {
            $this->seller_plan->commonDelete('fc_featured_seller_temp', array('id' => $id));
        };
		$this->data['errorMessage'] = "Payment Cancelled";
		$this->load->view("mobile/featured/failed",$this->data);
    }

	/*
	|| <<<<<<<<<<<<< SELLER FEATURE PLAN PAYPAL PAYMENT NOTIFY >>>>>>>>>>> ||
	*/
	public function paypal_notify($id) {
        $this->load->model('seller_plan_model', 'seller_plan');
        $temp_store = $this->seller_plan->get_all_details('fc_featured_seller_temp', array('id' => $id));
        if ($temp_store->num_rows > 0) {
            $this->seller_plan->commonDelete('fc_featured_seller_temp', array('id' => $id));
            $userExists = $this->seller_plan->get_all_details(FEATURED_SELLER, array('userId' => $temp_store->row()->userId, 'isExpired' => '0'));
            if ($userExists->num_rows == 0) {
                $dataArr = array('userId' => $temp_store->row()->userId, 'planPrice' => $temp_store->row()->planPrice, 'planPeriod' => $temp_store->row()->planPeriod, 'productCount' => $temp_store->row()->productCount);
                $time = time();
                $datestring = "%Y-%m-%d %h:%i:%s";
                $dataArr['featuredOn'] = mdate($datestring, $time);
                $dataArr['expiresOn'] = date("Y-m-d h:i:s", strtotime($dataArr['featuredOn'] . '+ ' . $temp_store->row()->planPeriod . ' days'));
                $dataArr['createdOn'] = mdate($datestring, $time);
                $this->seller_plan->simple_insert(FEATURED_SELLER, $dataArr);
                $this->seller_plan->commonDelete('fc_featured_seller_temp', array('id' => $id));
				$this->data['errorMessage'] = "Payment Successfull";
            }
        }
		$this->load->view("mobile/featured/success",$this->data);
    }

	/*
	|| <<<<<<<<<<<<< SELLER FEATURE PLAN PAYPAL CREDICT PAY >>>>>>>>>>> ||
	*/
	public function paypalCreditPayment($plan_id= "", $userId = "") {
		if($userId != ""){
			if($plan_id != ""){
				$condition = array('id' => $userId, "status" => "Active");
				$userInfoDetails = $this->mobile_model->get_all_details(USERS,$condition);
				if($userInfoDetails->num_rows() == 1){
					$this->load->model('seller_plan_model', 'seller_plan');
					$this->data['plan'] = $this->seller_plan->get_all_details(SELLER_PLAN, array('id'=>$plan_id));
					$this->data['userDetails'] = $userInfoDetails;
					$this->data['countryList'] = $this->mobile_model->get_all_details(COUNTRY_LIST, array(), array(array('field' => 'name', 'type' => 'asc')));
					$this->load->view('mobile/featured/featuredPayPaypalCredict', $this->data);
				}else{
					$this->data['errorMessage'] = "No Such User Found";
					$this->load->view("mobile/featured/failed",$this->data);
				}
			}else{
				$this->data['errorMessage'] = "PlanId Missing";
				$this->load->view("mobile/featured/failed",$this->data);
			}
		}else{
			$this->data['errorMessage'] = "UserId Missing";
			$this->load->view("mobile/featured/failed",$this->data);
		}
    }


	public function paypal_credit_payment() {

		$UserId = $_POST['user_id'];
		if($UserId !=""){
			$this->load->model('seller_plan_model', 'seller_plan');
	        $listCheck = $this->seller_plan->get_all_details(FEATURED_SELLER, array('userId' => $UserId, 'isExpired' => '0'));
	        if ($listCheck->num_rows == 0) {
	            $plan = $this->seller_plan->get_all_details(SELLER_PLAN, array('id'=>$this->input->post('plan_id')));
	            $registeredSellers = $this->seller_plan->get_all_details(FEATURED_SELLER, array('isExpired' => '0'));
	            if ($registeredSellers->num_rows < $plan->row()->plan_promoted_count) {
	                $time = time();
	                $datestring = "%Y-%m-%d %h:%i:%s";
	                $dataArr = array('userId' => $UserId, 'planPrice' => $plan->row()->plan_price, 'planPeriod' => $plan->row()->plan_period, 'productCount' => $plan->row()->plan_product_count, 'paymentType' => 'Credit Card');
	                $dataArr['createdOn'] = mdate($datestring, $time);
	                $this->seller_plan->simple_insert('fc_featured_seller_temp', $dataArr);
	                $temp_id = $this->db->insert_id();
	                $PaypalDodirect = unserialize($this->data['paypal_credit_card_settings']['settings']);
	                $dodirects = array(
	                    'Sandbox' => $PaypalDodirect['mode'], // Sandbox / testing mode option.
	                    'APIUsername' => $PaypalDodirect['Paypal_API_Username'], // PayPal API username of the API caller
	                    'APIPassword' => $PaypalDodirect['paypal_api_password'], // PayPal API password of the API caller
	                    'APISignature' => $PaypalDodirect['paypal_api_Signature'], // PayPal API signature of the API caller
	                    'APISubject' => '', // PayPal API subject (email address of 3rd party user that has granted API permission for your app)
	                    'APIVersion' => '85.0'  // API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
	                );
	                // Show Errors
	                if ($dodirects['Sandbox']) {
	                     
	                    ini_set('display_errors', '1');
	                }
	                $this->load->library('paypal/Paypal_pro', $dodirects);
	                $DPFields = array(
	                    'paymentaction' => '', // How you want to obtain payment.  Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
	                    'ipaddress' => $this->input->ip_address(), // Required.  IP address of the payer's browser.
	                    'returnfmfdetails' => '1'    // Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
	                );
	                $CCDetails = array(
	                    'creditcardtype' => $this->input->post('cardType'), // Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
	                    'acct' => $this->input->post('cardNumber'), // Required.  Credit card number.  No spaces or punctuation.
	                    'expdate' => $this->input->post('CCExpDay') . $this->input->post('CCExpMnth'), // Required.  Credit card expiration date.  Format is MMYYYY
	                    'cvv2' => $this->input->post('creditCardIdentifier'), // Requirements determined by your PayPal account settings.  Security digits for credit card.
	                    'startdate' => '', // Month and year that Maestro or Solo card was issued.  MMYYYY
	                    'issuenumber' => ''       // Issue number of Maestro or Solo card.  Two numeric digits max.
	                );
	                $PayerInfo = array(
	                    'email' => $this->input->post('email'), // Email address of payer.
	                    'payerid' => '', // Unique PayPal customer ID for payer.
	                    'payerstatus' => '', // Status of payer.  Values are verified or unverified
	                    'business' => ''
	                        // Payer's business name.
	                );
	                $PayerName = array(
	                    'salutation' => 'Mr.', // Payer's salutation.  20 char max.
	                    'firstname' => $this->input->post('full_name'), // Payer's first name.  25 char max.
	                    'middlename' => '', // Payer's middle name.  25 char max.
	                    'lastname' => '', // Payer's last name.  25 char max.
	                    'suffix' => ''        // Payer's suffix.  12 char max.
	                );
	                //'x_amount'				=> ,
	                //			'x_email'				=> $this->input->post('email'),

	                $BillingAddress = array(
	                    'street' => $this->input->post('address'), // Required.  First street address.
	                    'street2' => '', // Second street address.
	                    'city' => $this->input->post('city'), // Required.  Name of City.
	                    'state' => $this->input->post('state'), // Required. Name of State or Province.
	                    'countrycode' => $this->input->post('country'), // Required.  Country code.
	                    'zip' => $this->input->post('postal_code'), // Required.  Postal code of payer.
	                    'phonenum' => $this->input->post('phone_no')       // Phone Number of payer.  20 char max.
	                );
	                $ShippingAddress = array(
	                    'shiptoname' => $this->input->post('full_name'), // Required if shipping is included.  Person's name associated with this address.  32 char max.
	                    'shiptostreet' => $this->input->post('address'), // Required if shipping is included.  First street address.  100 char max.
	                    'shiptostreet2' => '', // Second street address.  100 char max.
	                    'shiptocity' => $this->input->post('city'), // Required if shipping is included.  Name of city.  40 char max.
	                    'shiptostate' => $this->input->post('state'), // Required if shipping is included.  Name of state or province.  40 char max.
	                    'shiptozip' => $this->input->post('postal_code'), // Required if shipping is included.  Postal code of shipping address.  20 char max.
	                    'shiptocountry' => $this->input->post('country'), // Required if shipping is included.  Country code of shipping address.  2 char max.
	                    'shiptophonenum' => $this->input->post('phone_no')  // Phone number for shipping address.  20 char max.
	                );

	                $PaymentDetails = array(
	                    'amt' => $this->input->post('total_price'), // Required.  Total amount of order, including shipping, handling, and tax.
	                    'currencycode' => $this->data['currencyType'], // Required.  Three-letter currency code.  Default is USD.
	                    'itemamt' => '', // Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
	                    'shippingamt' => '', // Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
	                    'insuranceamt' => '', // Total shipping insurance costs for this order.
	                    'shipdiscamt' => '', // Shipping discount for the order, specified as a negative number.
	                    'handlingamt' => '', // Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
	                    'taxamt' => '', // Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax.
	                    'desc' => '', // Description of the order the customer is purchasing.  127 char max.
	                    'custom' => '', // Free-form field for your own use.  256 char max.
	                    'invnum' => '', // Your own invoice or tracking number
	                    'buttonsource' => '', // An ID code for use by 3rd party apps to identify transactions.
	                    'notifyurl' => '', // URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
	                    'recurring' => ''      // Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
	                );
	                // For order items you populate a nested array with multiple $Item arrays.
	                // Normally you'll be looping through cart items to populate the $Item array
	                // Then push it into the $OrderItems array at the end of each loop for an entire
	                // collection of all items in $OrderItems.

	                $OrderItems = array();
	                $Item = array(
	                    'l_name' => '', // Item Name.  127 char max.
	                    'l_desc' => '', // Item description.  127 char max.
	                    'l_amt' => '', // Cost of individual item.
	                    'l_number' => '', // Item Number.  127 char max.
	                    'l_qty' => '', // Item quantity.  Must be any positive integer.
	                    'l_taxamt' => '', // Item's sales tax amount.
	                    'l_ebayitemnumber' => '', // eBay auction number of item.
	                    'l_ebayitemauctiontxnid' => '', // eBay transaction ID of purchased item.
	                    'l_ebayitemorderid' => ''     // eBay order ID for the item.
	                );
	                array_push($OrderItems, $Item);
	                $Secure3D = array(
	                    'authstatus3d' => '',
	                    'mpivendor3ds' => '',
	                    'cavv' => '',
	                    'eci3ds' => '',
	                    'xid' => ''
	                );
	                $PayPalRequestData = array(
	                    'DPFields' => $DPFields,
	                    'CCDetails' => $CCDetails,
	                    'PayerInfo' => $PayerInfo,
	                    'PayerName' => $PayerName,
	                    'BillingAddress' => $BillingAddress,
	                    'ShippingAddress' => $ShippingAddress,
	                    //'ShippingAddress' => array(),
	                    'PaymentDetails' => $PaymentDetails,
	                    'OrderItems' => $OrderItems,
	                    'Secure3D' => $Secure3D
	                );
	                $PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);
	                if (!$this->paypal_pro->APICallSuccessful($PayPalResult['ACK'])) {
	                    $this->seller_plan->commonDelete('fc_featured_seller_temp', array('id' => $temp_id));
	                    $errors = array('Errors' => $PayPalResult['ERRORS']);
	                    //$this->load->view('paypal_error',$errors);
	                    $newerrors = $errors['Errors'][0]['L_LONGMESSAGE'];
	                    // $this->setErrorMessage('danger', $newerrors);
	                    // redirect(base_url() . 'featured');
						$this->data['errorMessage'] = $newerrors;
						$this->load->view("mobile/featured/failed",$this->data);
	                } else {
	                    // Successful call.  Load view or whatever you need to do here.
	                    $temp_store = $this->seller_plan->get_all_details('fc_featured_seller_temp', array('id' => $temp_id));
	                    $this->seller_plan->commonDelete('fc_featured_seller_temp', array('id' => $temp_id));
	                    $time = time();
	                    $datestring = "%Y-%m-%d %h:%i:%s";
	                    $dataArr_1['userId'] = $UserId;
	                    $dataArr_1['featuredOn'] = mdate($datestring, $time);
	                    $dataArr_1['expiresOn'] = date("Y-m-d h:i:s", strtotime($dataArr_1['featuredOn'] . '+ ' . $temp_store->row()->planPeriod . ' days'));
	                    $dataArr_1['planPrice'] = $temp_store->row()->planPrice;
	                    $dataArr_1['planPeriod'] = $temp_store->row()->planPeriod;
	                    $dataArr_1['productCount'] = $temp_store->row()->productCount;
	                    $dataArr_1['paymentType'] = $temp_store->row()->paymentType;
	                    $dataArr_1['createdOn'] = mdate($datestring, $time);
	                    $this->seller_plan->simple_insert(FEATURED_SELLER, $dataArr_1);
						$this->data['errorMessage'] = "Payment Successful";
						$this->load->view("mobile/featured/success",$this->data);
	                }
	            } else {
					$this->data['errorMessage'] = "Maximum Number of featured Sellers reached";
					$this->load->view("mobile/featured/failed",$this->data);
	            }
	        } else {
				$this->data['errorMessage'] = "Already plan subscribed";
				$this->load->view("mobile/featured/failed",$this->data);
	        }
		}else{
			$this->data['errorMessage'] = "UserId Missing";
			$this->load->view("mobile/featured/failed",$this->data);
		}
    }

	public function authorizePayment($plan_id='', $userId="") {
		if($userId != ""){
			if($plan_id != ""){
				$condition = array('id' => $userId, "status" => "Active");
				$userInfoDetails = $this->mobile_model->get_all_details(USERS,$condition);
				if($userInfoDetails->num_rows() == 1){
					$this->data['userDetails'] = $userInfoDetails;
					$this->data['countryList'] = $this->mobile_model->get_all_details(COUNTRY_LIST, array(), array(array('field' => 'name', 'type' => 'asc')));
					$this->load->model('seller_plan_model', 'seller_plan');
					$this->data['plan'] = $this->seller_plan->get_all_details(SELLER_PLAN, array('id'=>$plan_id));
					$this->load->view('mobile/featured/featuredPayAuthorizeCredict', $this->data);
				}else{
					$this->data['errorMessage'] = "No Such User Found";
					$this->load->view("mobile/featured/failed",$this->data);
				}
			}else{
				$this->data['errorMessage'] = "PlanId Missing";
				$this->load->view("mobile/featured/failed",$this->data);
			}
		}else{
			$this->data['errorMessage'] = "UserId Missing";
			$this->load->view("mobile/featured/failed",$this->data);
		}
    }


	public function authorize_payment() {
		$userId = $_POST['user_id'];
		if($userId != ""){
			$this->load->model('seller_plan_model', 'seller_plan');
	        $listCheck = $this->seller_plan->get_all_details(FEATURED_SELLER, array('userId' => $userId, 'isExpired' => '0'));
	        if ($listCheck->num_rows == 0) {
	            $plan = $this->seller_plan->get_all_details(SELLER_PLAN, array('id'=>$this->input->post('plan_id')));
	            $registeredSellers = $this->seller_plan->get_all_details(FEATURED_SELLER, array('isExpired' => '0'));
	            if ($registeredSellers->num_rows < $plan->row()->plan_promoted_count) {
	                $time = time();
	                $datestring = "%Y-%m-%d %h:%i:%s";
	                $dataArr = array('userId' => $userId, 'planPrice' => $plan->row()->plan_price, 'planPeriod' => $plan->row()->plan_period, 'productCount' => $plan->row()->plan_product_count, 'paymentType' => 'authorize');
	                $dataArr['createdOn'] = mdate($datestring, $time);
	                $this->seller_plan->simple_insert('fc_featured_seller_temp', $dataArr);
	                $temp_id = $this->db->insert_id();

	                $Auth_Details = unserialize(API_LOGINID);
	                $Auth_Setting_Details = unserialize($Auth_Details['settings']);

	                 
	                define("AUTHORIZENET_API_LOGIN_ID", $Auth_Setting_Details['Login_ID']);    // Add your API LOGIN ID
	                define("AUTHORIZENET_TRANSACTION_KEY", $Auth_Setting_Details['Transaction_Key']); // Add your API transaction key
	                define("API_MODE", $Auth_Setting_Details['mode']);

	                if (API_MODE == 'sandbox') {
	                    define("AUTHORIZENET_SANDBOX", true); // Set to false to test against production
	                } else {
	                    define("AUTHORIZENET_SANDBOX", false);
	                }
	                define("TEST_REQUEST", "FALSE");
	                require_once './authorize/AuthorizeNet.php';

	                $transaction = new AuthorizeNetAIM;
	                $transaction->setSandbox(AUTHORIZENET_SANDBOX);
	                $transaction->setFields(
	                        array(
	                            'amount' => $this->input->post('total_price'),
	                            'card_num' => $this->input->post('cardNumber'),
	                            'exp_date' => $this->input->post('CCExpDay') . '/' . $this->input->post('CCExpMnth'),
	                            'first_name' => $this->input->post('full_name'),
	                            'last_name' => '',
	                            'address' => $this->input->post('address'),
	                            'city' => $this->input->post('city'),
	                            'state' => $this->input->post('state'),
	                            'country' => $this->input->post('country'),
	                            'phone' => $this->input->post('phone_no'),
	                            'email' => $this->input->post('email'),
	                            'card_code' => $this->input->post('creditCardIdentifier'),
	                        )
	                );
	                $response = $transaction->authorizeAndCapture();
	                if ($response->approved) {
	                    $temp_store = $this->seller_plan->get_all_details('fc_featured_seller_temp', array('id' => $temp_id));
	                    $this->seller_plan->commonDelete('fc_featured_seller_temp', array('id' => $temp_id));
	                    $time = time();
	                    $datestring = "%Y-%m-%d %h:%i:%s";
	                    $dataArr_1['userId'] = $userId;
	                    $dataArr_1['featuredOn'] = mdate($datestring, $time);
	                    $dataArr_1['expiresOn'] = date("Y-m-d h:i:s", strtotime($dataArr_1['featuredOn'] . '+ ' . $temp_store->row()->planPeriod . ' days'));
	                    $dataArr_1['planPrice'] = $temp_store->row()->planPrice;
	                    $dataArr_1['planPeriod'] = $temp_store->row()->planPeriod;
	                    $dataArr_1['productCount'] = $temp_store->row()->productCount;
	                    $dataArr_1['paymentType'] = $temp_store->row()->paymentType;
	                    $dataArr_1['createdOn'] = mdate($datestring, $time);
	                    $this->seller_plan->simple_insert(FEATURED_SELLER, $dataArr_1);
						$this->data['errorMessage'] = "Payment Successful";
						$this->load->view("mobile/featured/success",$this->data);
	                } else {
	                    //redirect('site/shopcart/cancel?failmsg='.$response->response_reason_text);
	                    //redirect('order/failure/'.$response->response_reason_text);
	                    $this->seller_plan->commonDelete('fc_featured_seller_temp', array('id' => $temp_id));
						$this->data['errorMessage'] = $response->response_reason_text;
						$this->load->view("mobile/featured/failed",$this->data);
	                }
	            } else {
					$this->data['errorMessage'] = "Maximum Number of featured Sellers reached";
					$this->load->view("mobile/featured/failed",$this->data);
	            }
	        } else {
				$this->data['errorMessage'] = "Already plan subscribed!!";
				$this->load->view("mobile/featured/failed",$this->data);
	        }
		}else{
			$this->data['errorMessage'] = "UserId Missing";
			$this->load->view("mobile/featured/failed",$this->data);
		}
    }

	public function stripePayment($plan_id='', $userId="") {
		if($userId != ""){
			if($plan_id != ""){
				$condition = array('id' => $userId, "status" => "Active");
				$userInfoDetails = $this->mobile_model->get_all_details(USERS,$condition);
				if($userInfoDetails->num_rows() == 1){
					$this->data['userDetails'] = $userInfoDetails;
					$this->data['countryList'] = $this->mobile_model->get_all_details(COUNTRY_LIST, array(), array(array('field' => 'name', 'type' => 'asc')));
					$this->load->model('seller_plan_model', 'seller_plan');
					$this->data['plan'] = $this->seller_plan->get_all_details(SELLER_PLAN, array('id'=>$plan_id));
					$this->load->view('mobile/featured/featuredStripePayment', $this->data);
				}else{
					$this->data['errorMessage'] = "No Such User Found";
					$this->load->view("mobile/featured/failed",$this->data);
				}
			}else{
				$this->data['errorMessage'] = "PlanId Missing";
				$this->load->view("mobile/featured/failed",$this->data);
			}
		}else{
			$this->data['errorMessage'] = "UserId Missing";
			$this->load->view("mobile/featured/failed",$this->data);
		}
    }

	public function stripe_payment(){
		$userId = $_POST['user_id'];
		if($userId != ""){
			$this->load->model('seller_plan_model', 'seller_plan');
	        $listCheck = $this->seller_plan->get_all_details(FEATURED_SELLER, array('userId' => $userId, 'isExpired' => '0'));
	        if ($listCheck->num_rows == 0) {
	            $plan = $this->seller_plan->get_all_details(SELLER_PLAN, array('id'=>$this->input->post('plan_id')));
	            $registeredSellers = $this->seller_plan->get_all_details(FEATURED_SELLER, array('isExpired' => '0'));
	            if ($registeredSellers->num_rows < $plan->row()->plan_promoted_count) {
	                $time = time();
	                $datestring = "%Y-%m-%d %h:%i:%s";
	                $dataArr = array('userId' => $userId, 'planPrice' => $plan->row()->plan_price, 'planPeriod' => $plan->row()->plan_period, 'productCount' => $plan->row()->plan_product_count, 'paymentType' => 'stripe');
	                $dataArr['createdOn'] = mdate($datestring, $time);
	                $this->seller_plan->simple_insert('fc_featured_seller_temp', $dataArr);
	                $temp_id = $this->db->insert_id();
	                $this->load->library('Stripe');
	                $success = "";
	                $error = "";
	                $totalAmount = $this->input->post('total_price');
	                $crdno = $this->input->post('cardNumber');
	                $crdname = $this->input->post('cardType');
	                $year = $this->input->post('CCExpMnth');
	                $mnth = $this->input->post('CCExpDay');
	                $cvv = $this->input->post('creditCardIdentifier');
	                $crd = array('number' => $crdno, 'exp_month' => $mnth, 'exp_year' => $year, 'cvc' => $cvv, 'name' => $crdname);
	                $item_name = $this->config->item('email_title') . ' Products';
	                if ($_POST) {
	                    $success = Stripe::charge_card($totalAmount, $crd);
	                    if (isset($success)) {
	                        $result_success = (array) json_decode($success);
	                        if ($result_success['status'] == "succeeded") {
	                            $temp_store = $this->seller_plan->get_all_details('fc_featured_seller_temp', array('id' => $temp_id));
	                            $this->seller_plan->commonDelete('fc_featured_seller_temp', array('id' => $temp_id));
	                            $time = time();
	                            $datestring = "%Y-%m-%d %h:%i:%s";
	                            $dataArr_1['userId'] = $userId;
	                            $dataArr_1['featuredOn'] = mdate($datestring, $time);
	                            $dataArr_1['expiresOn'] = date("Y-m-d h:i:s", strtotime($dataArr_1['featuredOn'] . '+ ' . $temp_store->row()->planPeriod . ' days'));
	                            $dataArr_1['planPrice'] = $temp_store->row()->planPrice;
	                            $dataArr_1['planPeriod'] = $temp_store->row()->planPeriod;
	                            $dataArr_1['productCount'] = $temp_store->row()->productCount;
	                            $dataArr_1['paymentType'] = $temp_store->row()->paymentType;
	                            $dataArr_1['createdOn'] = mdate($datestring, $time);
	                            $this->seller_plan->simple_insert(FEATURED_SELLER, $dataArr_1);
								$this->data['errorMessage'] = "Payment Successful";
								$this->load->view("mobile/featured/success",$this->data);
	                        } else {
	                            $err = (array) $result_success['error'];
	                            $this->seller_plan->commonDelete('fc_featured_seller_temp', array('id' => $temp_id));
	                            $statustrans['msg'] = $err['message'];
								$this->data['errorMessage'] = $err['message'];
								$this->load->view("mobile/featured/failed",$this->data);
	                        }
	                    }
	                }
	            } else {
					$this->data['errorMessage'] = "Maximum Number of featured Sellers reached";
					$this->load->view("mobile/featured/failed",$this->data);
	            }
	        } else {
				$this->data['errorMessage'] = "Already plan subscribed!!";
				$this->load->view("mobile/featured/failed",$this->data);
	        }
		}else{
			$this->data['errorMessage'] = "UserId Missing";
			$this->load->view("mobile/featured/failed",$this->data);
		}
    }

	public function featureTwoCheckout($plan_id='', $userId="") {
		if($userId != ""){
			if($plan_id != ""){
				$condition = array('id' => $userId, "status" => "Active");
				$userInfoDetails = $this->mobile_model->get_all_details(USERS,$condition);
				if($userInfoDetails->num_rows() == 1){
					$this->data['userDetails'] = $userInfoDetails;
					$this->data['countryList'] = $this->mobile_model->get_all_details(COUNTRY_LIST, array(), array(array('field' => 'name', 'type' => 'asc')));
					$this->load->model('seller_plan_model', 'seller_plan');
					$this->data['plan'] = $this->seller_plan->get_all_details(SELLER_PLAN, array('id'=>$plan_id));
					$this->load->view('mobile/featured/featuredTwoCheckout', $this->data);
				}else{
					$this->data['errorMessage'] = "No Such User Found";
					$this->load->view("mobile/featured/failed",$this->data);
				}
			}else{
				$this->data['errorMessage'] = "PlanId Missing";
				$this->load->view("mobile/featured/failed",$this->data);
			}
		}else{
			$this->data['errorMessage'] = "UserId Missing";
			$this->load->view("mobile/featured/failed",$this->data);
		}
    }


	public function twocheckout_payment() {
		$userId = $_POST['user_id'];
		if($userId != ""){
			$this->load->model('seller_plan_model', 'seller_plan');
	        $listCheck = $this->seller_plan->get_all_details(FEATURED_SELLER, array('userId' => $userId, 'isExpired' => '0'));
	        if ($listCheck->num_rows == 0) {
	            $plan = $this->seller_plan->get_all_details(SELLER_PLAN, array('id'=>$this->input->post('plan_id')));
	            $registeredSellers = $this->seller_plan->get_all_details(FEATURED_SELLER, array('isExpired' => '0'));
	            if ($registeredSellers->num_rows < $plan->row()->plan_promoted_count) {
	                $time = time();
	                $datestring = "%Y-%m-%d %h:%i:%s";
	                $dataArr = array('userId' => $userId, 'planPrice' => $plan->row()->plan_price, 'planPeriod' => $plan->row()->plan_period, 'productCount' => $plan->row()->plan_product_count, 'paymentType' => '2CO');
	                $dataArr['createdOn'] = mdate($datestring, $time);
	                $this->seller_plan->simple_insert('fc_featured_seller_temp', $dataArr);
	                $temp_id = $this->db->insert_id();
	                $this->load->library('Twocheckout');
	                $paypal_settings = unserialize($this->config->item('payment_5'));
	                $settings = unserialize($paypal_settings['settings']);
	                Twocheckout::privateKey($settings['privateKey']);
	                Twocheckout::sellerId($settings['sellerId']);
	                Twocheckout::sandbox(true);  #Uncomment to use Sandbox
	                $tokenid = $this->input->post('token');
	                $loginUserId = $userId;
	                $totalAmount = $this->input->post('total_price');
	                $quantity = 1;
	                try {
	                    $charge = Twocheckout_Charge::auth(array(
	                                "merchantOrderId" => "123",
	                                "token" => $tokenid,
	                                "currency" => 'USD',
	                                "total" => $totalAmount,
	                                "billingAddr" => array(
	                                    "name" => $this->input->post('full_name'),
	                                    "addrLine1" => $this->input->post('address'),
	                                    "city" => $this->input->post('city'),
	                                    "state" => $this->input->post('state'),
	                                    "zipCode" => $this->input->post('postal_code'),
	                                    "country" => $this->input->post('country'),
	                                    "email" => $this->input->post('email'),
	                                    "phoneNumber" => $this->input->post('phone_no')
	                                ),
	                                "shippingAddr" => array(
	                                    "name" => $this->input->post('full_name'),
	                                    "addrLine1" => $this->input->post('address'),
	                                    "city" => $this->input->post('city'),
	                                    "state" => $this->input->post('state'),
	                                    "zipCode" => $this->input->post('postal_code'),
	                                    "country" => $this->input->post('country'),
	                                    "email" => $this->input->post('email'),
	                                    "phoneNumber" => $this->input->post('phone_no')
	                                )
	                                    ), 'array');

	                    $result = (array) json_decode($charge);
	                    $rescod = $result['response'];
	                    if ($rescod->responseCode == 'APPROVED') {
	                        $temp_store = $this->seller_plan->get_all_details('fc_featured_seller_temp', array('id' => $temp_id));
	                        $this->seller_plan->commonDelete('fc_featured_seller_temp', array('id' => $temp_id));
	                        $time = time();
	                        $datestring = "%Y-%m-%d %h:%i:%s";
	                        $dataArr_1['userId'] = $userId;
	                        $dataArr_1['featuredOn'] = mdate($datestring, $time);
	                        $dataArr_1['expiresOn'] = date("Y-m-d h:i:s", strtotime($dataArr_1['featuredOn'] . '+ ' . $temp_store->row()->planPeriod . ' days'));
	                        $dataArr_1['planPrice'] = $temp_store->row()->planPrice;
	                        $dataArr_1['planPeriod'] = $temp_store->row()->planPeriod;
	                        $dataArr_1['productCount'] = $temp_store->row()->productCount;
	                        $dataArr_1['paymentType'] = $temp_store->row()->paymentType;
	                        $dataArr_1['createdOn'] = mdate($datestring, $time);
	                        $this->seller_plan->simple_insert(FEATURED_SELLER, $dataArr_1);
							$this->data['errorMessage'] = "Payment Successful";
							$this->load->view("mobile/featured/success",$this->data);
	                    } else {
	                        $this->seller_plan->commonDelete('fc_featured_seller_temp', array('id' => $temp_id));
							$this->data['errorMessage'] = $result['exception']->errorMsg;
							$this->load->view("mobile/featured/failed",$this->data);
	                    }
	                } catch (Twocheckout_Error $e) {
	                    $e->getMessage();
	                }
	            } else {
					$this->data['errorMessage'] = "Maximum Number of featured Sellers reached";
					$this->load->view("mobile/featured/failed",$this->data);
	            }
	        } else {
				$this->data['errorMessage'] = "Already plan subscribed!!";
				$this->load->view("mobile/featured/failed",$this->data);
	        }
		}else{
			$this->data['errorMessage'] = "UserId Missing";
			$this->load->view("mobile/featured/failed",$this->data);
		}
    }

	public function addSellerFeaturedProducts() {
		$returnArr['status'] = (string)0;
		$returnArr['response'] = '';
		$UserId = $_POST['UserId'];
		if($UserId != ""){
			$this->load->model('seller_plan_model', 'seller_plan');
			$return['message'] = '0';
			$listCheck = $this->seller_plan->get_all_details(FEATURED_SELLER, array('userId' => $UserId, 'isExpired' => '0'));
			if ($listCheck->num_rows > 0) {
				$sellerProductID = $this->input->post('sellerProductID');
				if($sellerProductID != ""){
					$sellerProductIDArr = explode(',',$sellerProductID);
					$sellerProductIDArrFinal = array_filter($sellerProductIDArr);
					$featuredProducts = serialize($sellerProductIDArrFinal);
					$dataArr['featuredProducts'] = $featuredProducts;
					$productExists = $this->seller_plan->get_all_details(FEATURED_PRODUCTS, array('userId' => $UserId, 'sellerPlan_id' => $listCheck->row()->id));
					if ($productExists->num_rows > 0) {
						$this->seller_plan->update_details(FEATURED_PRODUCTS, $dataArr, array('userId' => $UserId, 'sellerPlan_id' => $listCheck->row()->id));
					} else {
						$dataArr['userId'] = $UserId;
						$dataArr['sellerPlan_id'] = $listCheck->row()->id;
						$this->seller_plan->simple_insert(FEATURED_PRODUCTS, $dataArr);
					}
					$returnArr['status'] = 1;
					$returnArr['response'] = 'Products Successfully Saved as Featured Products';
				}else{
					$returnArr['response'] = 'SellerProductId Null !!';
				}
			}else{
				$returnArr['response'] = 'You are not a featured seller !!';
			}
		}else{
			$returnArr['response'] = 'UserId Null !!';
		}
		echo json_encode($returnArr);
    }


	public function buyGiftCard(){
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = $_POST['UserId'];
		if($UserId != ""){
			$priceValue = $_POST['priceValue'];
			$recipientName = $_POST['recipientName'];
			$recipientEmail = $_POST['recipientEmail'];
			$message = $_POST['message'];
			if($priceValue != "" && $recipientName != "" && $recipientEmail != "" && $message != ""){
				$userDetails = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
				//echo "<pre>";print_r($userDetails->result());die;
				if($userDetails->num_rows() == 1){
					$senderEmail = $userDetails->row()->email;
					$senderName = $userDetails->row()->full_name;
					$dataArrVal = array(
												"user_id" => $UserId,
												"price_value" => $priceValue,
												"recipient_name" => $recipientName,
												"recipient_mail" => $recipientEmail,
												"sender_name" => $senderName,
												"sender_mail" => $senderEmail,
												"description" => $message,
											);
					$datestring = "%Y-%m-%d 23:59:59";
					$code = $this->get_rand_str('10');
					$exp_days = $this->config->item('giftcard_expiry_days');
					$dataArry_data = array(
													'expiry_date' => mdate($datestring, strtotime($exp_days . ' days')),
													'code' => $code,
												);
					$dataArr = array_merge($dataArrVal, $dataArry_data);
					$this->giftcards_model->simple_insert(GIFTCARDS_TEMP, $dataArr);
					$paymentGateways = $this->mobile_model->get_all_details(PAYMENT_GATEWAY, array('status' => "Enable"));
					$paymentTypes = array();
					if($paymentGateways->num_rows() > 0){
						foreach ($paymentGateways->result() as $gatewayData) {
							if($gatewayData->id != 4 && $gatewayData->id != 7){
								$paymentTypes[] = array(
														"paymentId" => $gatewayData->id,
														"paymentName" => $gatewayData->gateway_name,
													);
							}
						}
					}
					$returnArr['status'] = 1;
					$returnArr['response'] = 'Success';
					$returnArr['paymentTypes'] = $paymentTypes;
				}else{
					$returnArr['response'] = " No Such User Found !!";
				}
			}else{
				$returnArr['response'] = " Some Parameters Values are Missing !!";
			}
		}else{
			$returnArr['response'] = " UserId Null !!";
		}
		echo json_encode($returnArr);
	}

	public function giftCardPayment(){
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = $_GET['UserId'];
		if($UserId != ""){
			$paymentType = $_GET['paymentId'];
			if($paymentType != ""){
				redirect('json/checkout/cart?UserId='.$UserId.'&paymentId='.$paymentType.'&checkoutType=gift&AppWebView=1');
			}else{
				$returnArr['response'] = " paymentId Null !!";
			}
		}else{
			$returnArr['response'] = " UserId Null !!";
		}
		echo json_encode($returnArr);
	}

	public function getGiftCards() {
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = $_POST['UserId'];
		if($UserId != ""){
			$userDetails = $this->mobile_model->get_all_details(USERS, array('id' => $UserId));
			if($userDetails->num_rows() == 1){
				$giftcardDetails = $this->user_model->get_gift_cards_list($userDetails->row()->email);
				$giftList = array();
				if($giftcardDetails->num_rows() > 0){
					foreach($giftcardDetails->result() as $data){
						$giftList[] = array(
												"giftCode" => $data->code,
												"senderName" => $data->sender_name,
												"senderEmail" => $data->sender_mail,
												"priceValue" => $data->price_value,
												"expiresOn" => $data->expiry_date,
												"status" => $data->card_status,
											);
					}
				}
				$returnArr['status'] = 1;
				$returnArr['response'] = 'Success';
				$returnArr['giftCardList'] = $giftList;
			}else{
				$returnArr['response'] = " No Such User Found !!";
			}
		}else{
			$returnArr['response'] = " UserId Null !!";
		}
		echo json_encode($returnArr);
    }


	public function cmsPages(){
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = $_GET['UserId'];
		if($UserId != ""){
			$seourl = $_GET['seourl'];
			if($seourl != ""){
				$pageDetails = $this->product_model->get_all_details(CMS,array('seourl'=>$seourl,'status'=>'Publish'));
				if ($pageDetails->num_rows() == 0){
					$returnArr['response'] = "Something Went Wrong !!";
				}else {
					$this->data['heading'] = $pageDetails->row()->meta_title;
					$this->data['pageDetails'] = $pageDetails;
					$this->load->view('mobile/userSettings/cms',$this->data);
				}
			}else{
				$returnArr['response'] = " Seourl Null !!";
				echo json_encode($returnArr);
			}
		}else{
			$returnArr['response'] = " UserId Null !!";
			echo json_encode($returnArr);
		}
	}

	public function resendConfirmationMail(){
		$returnArr['status'] = 0;
		$returnArr['response'] = '';
		$UserId = $_POST['UserId'];
		if($UserId != ""){
			$mail = $_POST['email'];
			if($mail != ""){
				if (valid_email($email)){
					$condition = array('email' => $mail);
		            $userDetails = $this->user_model->get_all_details(USERS, $condition);
					if($userDetails->num_rows() == 1){
						$this->send_confirm_mail($userDetails);
						$returnArr['status'] = 1;
						$returnArr['response'] = 'Verification Mail Sent Successfully !!';
					}else{
						$returnArr['response'] = " No Such User Found !!";
					}
				}else{
					$returnArr['response'] = " Enter Valid Email !!";
				}
			}else{
				$returnArr['response'] = " Email Null !!";
			}
		}else{
			$returnArr['response'] = " UserId Null !!";
			echo json_encode($returnArr);
		}
	}
}
