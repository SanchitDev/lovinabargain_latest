<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * User related functions
 * @author Teamtweaks
 *
 */
class Product extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('cookie', 'date', 'form', 'email', 'text'));
        $this->load->library(array('encrypt', 'form_validation'));
        $this->load->model(array('product_model', 'user_model'));

        if ($_SESSION['sMainCategories'] == '') {
            $sortArr1 = array('field' => 'cat_position', 'type' => 'asc');
            $sortArr = array($sortArr1);
            $_SESSION['sMainCategories'] = $this->product_model->get_all_details(CATEGORY, array('rootID' => '0', 'status' => 'Active'), $sortArr);
        }
        $this->data['mainCategories'] = $_SESSION['sMainCategories'];

        if ($_SESSION['sColorLists'] == '') {
            $_SESSION['sColorLists'] = $this->product_model->get_all_details(LIST_VALUES, array('list_id' => '1'));
        }
        $this->data['mainColorLists'] = $_SESSION['sColorLists'];

        $this->data['loginCheck'] = $this->checkLogin('U');
        $this->data['likedProducts'] = array();
        if ($this->data['loginCheck'] != '') {
            $this->data['likedProducts'] = $this->product_model->get_all_details(PRODUCT_LIKES, array('user_id' => $this->checkLogin('U')));
        }
    }

    public function onboarding() {
        if ($this->checkLogin('U') == '') {
            redirect(base_url());
        } else {
            $this->data['userDetails'] = $this->product_model->get_all_details(USERS, array('id' => $this->checkLogin('U')));
            if ($this->data['userDetails']->num_rows() == 1) {
                if ($this->data['mainCategories']->num_rows() > 0) {
                    foreach ($this->data['mainCategories']->result() as $cat) {
                        //						$condition = " where p.category_id like '".$cat->id.",%' OR p.category_id like '%,".$cat->id."' OR p.category_id like '%,".$cat->id.",%' OR p.category_id='".$cat->id."' order by p.created desc";
                        $condition = " where FIND_IN_SET('" . $cat->id . "',p.category_id) and p.quantity>0 and p.status='Publish' and u.group='Seller' and u.status='Active' or p.status='Publish' and p.quantity > 0 and p.user_id=0 and FIND_IN_SET('" . $cat->id . "',p.category_id) order by p.created desc";
                        $this->data['productDetails'][$cat->cat_name] = $this->product_model->view_product_details($condition);
                    }
                }
                $this->load->view('site/user/onboarding', $this->data);
            } else {
                redirect(base_url());
            }
        }
    }

    public function onboarding_get_products_categories() {
        $returnCnt = '<div id="onboarding-category-items"><ol class="stream vertical">';
        $left = $top = $count = 0;
        $width = 220;
        $productArr = array();
        $catID = explode(',', $this->input->get('categories'));
        if (count($catID) > 0) {
            foreach ($catID as $cat) {
                //				$condition = " where p.category_id like '".$cat.",%' AND p.status = 'Publish' OR p.category_id like '%,".$cat."' AND p.status = 'Publish' OR p.category_id like '%,".$cat.",%' AND p.status = 'Publish' OR p.category_id='".$cat."' AND p.status = 'Publish'";
                $condition = " where FIND_IN_SET('" . $cat . "',p.category_id) and p.quantity>0 and p.status='Publish' and u.group='Seller' and u.status='Active' or p.status='Publish' and p.quantity > 0 and p.user_id=0 and FIND_IN_SET('" . $cat . "',p.category_id) order by p.created desc";
                $productDetails = $this->product_model->view_product_details($condition);
                if ($productDetails->num_rows() > 0) {
                    foreach ($productDetails->result() as $productRow) {
                        if (!in_array($productRow->id, $productArr)) {
                            array_push($productArr, $productRow->id);
                            $img = '';
                            $imgArr = explode(',', $productRow->image);
                            if (count($imgArr) > 0) {
                                foreach ($imgArr as $imgRow) {
                                    if ($imgRow != '') {
                                        $img = $imgRow;
                                        break;
                                    }
                                }
                            }
                            if ($img != '') {
                                $count++;
                                $leftPos = $count % 3;
                                $leftPos = ($leftPos == 0) ? 3 : $leftPos;
                                $leftPos--;
                                if ($count % 3 == 0) {
                                    $topPos = $count / 3;
                                } else {
                                    $topPos = ceil($count / 3);
                                }
                                $topPos--;
                                $leftVal = $leftPos * $width;
                                $topVal = $topPos * $width;
                                $returnCnt .='
									<li style="opacity: 1; top: ' . $topVal . 'px; left: ' . $leftVal . 'px;" class="start_marker_"><span class="pre hide"></span>
										<div class="figure-item">
											<a class="figure-img">
												<span style="background-image:url(\'' . base_url() . 'images/product/' . $img . '\')" class="figure">
													<em class="back"></em>
													<img height="200" data-height="640" data-width="640" src="' . base_url() . 'images/product/' . $img . '"/>
												</span>
											</a>
											<a tid="' . $productRow->seller_product_id . '" class="button fancy noedit" href="#"><span><i></i></span>' . LIKE_BUTTON . '</a>
										</div>
									</li>
								';
                            }
                        }
                    }
                }
            }
        }
        $returnCnt .= '
			</div>
		';
        echo $returnCnt;
    }

    public function onboarding_get_users_follow() {
        $catID = explode(',', $this->input->get('categories'));
        $productArr = array();
        $userArr = array();
        $userCountArr = array();
        $returnArr = array();

        /*         * **********Get Suggested Users List***************************** */

        $returnArr['suggested'] = '<ul class="suggest-list">';
        if (count($catID) > 0) {
            foreach ($catID as $cat) {
                //				$condition = " where p.category_id like '".$cat.",%' AND p.status = 'Publish' OR p.category_id like '%,".$cat."' AND p.status = 'Publish' OR p.category_id like '%,".$cat.",%' AND p.status = 'Publish' OR p.category_id='".$cat."' AND p.status = 'Publish'";
                $condition = " where FIND_IN_SET('" . $cat . "',p.category_id) and p.quantity>0 and p.status='Publish' and u.group='Seller' and u.status='Active' or p.status='Publish' and p.quantity > 0 and p.user_id=0 and FIND_IN_SET('" . $cat . "',p.category_id)";
                $productDetails = $this->product_model->view_product_details($condition);
                if ($productDetails->num_rows() > 0) {
                    foreach ($productDetails->result() as $productRow) {
                        if (!in_array($productRow->id, $productArr)) {
                            array_push($productArr, $productRow->id);
                            if ($productRow->user_id != '') {
                                if (!in_array($productRow->user_id, $userArr)) {
                                    array_push($userArr, $productRow->user_id);
                                    $userCountArr[$productRow->user_id] = 1;
                                } else {
                                    $userCountArr[$productRow->user_id] ++;
                                }
                            }
                        }
                    }
                }
            }
        }
        arsort($userCountArr);
        $limitCount = 0;
        foreach ($userCountArr as $user_id => $products) {
            if ($user_id != '') {
                $condition = array('id' => $user_id, 'is_verified' => 'Yes', 'status' => 'Active');
                $userDetails = $this->product_model->get_all_details(USERS, $condition);
                if ($userDetails->num_rows() == 1) {
                    $condition = array('user_id' => $user_id, 'status' => 'Publish');
                    $userProductDetails = $this->product_model->get_all_details(PRODUCT, $condition);
                    if ($limitCount < 10) {
                        $userImg = $userDetails->row()->thumbnail;
                        if ($userImg == '') {
                            $userImg = 'user-thumb1.png';
                        }
                        $returnArr['suggested'] .= '
							<li><span class="vcard"><img src="' . base_url() . 'images/users/' . $userImg . '"></span>
							<b>' . $userDetails->row()->full_name . '</b><br>
							' . $userDetails->row()->followers_count . ' followers<br>
							' . $userProductDetails->num_rows() . ' things<br>
							<a uid="' . $user_id . '" class="follow-user-link" href="javascript:void(0)">Follow</a>
							<span class="category-thum">';
                        $plimit = 0;
                        if ($userProductDetails->num_rows() > 0) {
                            foreach ($userProductDetails->result() as $userProduct) {
                                if ($plimit > 3) {
                                    break;
                                }
                                $img = '';
                                $imgArr = explode(',', $userProduct->image);
                                if (count($imgArr) > 0) {
                                    foreach ($imgArr as $imgRow) {
                                        if ($imgRow != '') {
                                            $img = $imgRow;
                                            break;
                                        }
                                    }
                                }
                                if ($img != '') {

                                    $returnArr['suggested'] .='<img alt="' . $userProduct->product_name . '" src="' . base_url() . 'images/product/' . $img . '">';
                                    $plimit++;
                                }
                            }
                        }

                        $returnArr['suggested'] .='</span>
							</li>
						';
                        $limitCount++;
                    }
                }
            }
        }
        $returnArr['suggested'] .='</ul>';

        /*         * ******************************************************** */

        /*         * **************Get Top Users For All Categories********* */
        $returnArr['categories'] = '';
        if ($this->data['mainCategories']->num_rows() > 0) {
            foreach ($this->data['mainCategories']->result() as $catRow) {
                if ($catRow->id != '' && $catRow->cat_name != '') {
                    $returnArr['categories'] .= '
					<div style="display:none;" class="intxt ' . url_title($catRow->cat_name, '_', TRUE) . '">
					<p class="stit"><span>' . $catRow->cat_name . '</span>
					<button class="btns-blue-embo btn-followall">Follow All</button></p>
					<ul class="suggest-list">';
                    $userCountArr = $this->product_model->get_top_users_in_category($catRow->id);
                    $limitCount = 0;
                    foreach ($userCountArr as $user_id => $products) {
                        if ($user_id != '') {
                            $condition = array('id' => $user_id, 'is_verified' => 'Yes', 'status' => 'Active');
                            $userDetails = $this->product_model->get_all_details(USERS, $condition);
                            if ($userDetails->num_rows() == 1) {
                                $condition = array('user_id' => $user_id, 'status' => 'Publish');
                                $userProductDetails = $this->product_model->get_all_details(PRODUCT, $condition);
                                if ($limitCount < 10) {
                                    $userImg = $userDetails->row()->thumbnail;
                                    if ($userImg == '') {
                                        $userImg = 'user-thumb1.png';
                                    }
                                    $returnArr['categories'] .= '
											<li><span class="vcard"><img src="' . base_url() . 'images/users/' . $userImg . '"></span>
											<b>' . $userDetails->row()->full_name . '</b><br>
											' . $userDetails->row()->followers_count . ' followers<br>
											' . $userProductDetails->num_rows() . ' things<br>
											<a uid="' . $user_id . '" class="follow-user-link" href="javascript:void(0)">Follow</a>
											<span class="category-thum">';
                                    $plimit = 0;
                                    if ($userProductDetails->num_rows() > 0) {
                                        foreach ($userProductDetails->result() as $userProduct) {
                                            if ($plimit > 3) {
                                                break;
                                            }
                                            $img = '';
                                            $imgArr = explode(',', $userProduct->image);
                                            if (count($imgArr) > 0) {
                                                foreach ($imgArr as $imgRow) {
                                                    if ($imgRow != '') {
                                                        $img = $imgRow;
                                                        break;
                                                    }
                                                }
                                            }
                                            if ($img != '') {

                                                $returnArr['categories'] .='<img alt="' . $userProduct->product_name . '" src="' . base_url() . 'images/product/' . $img . '">';
                                                $plimit++;
                                            }
                                        }
                                    }

                                    $returnArr['categories'] .='</span>
											</li>
										';
                                    $limitCount++;
                                }
                            }
                        }
                    }
                    $returnArr['categories'] .='</ul></div>';
                }
            }
        }

        /*         * ******************************************************* */

        echo json_encode($returnArr);
    }

    public function display_product_shuffle() {
        $productDetails = $this->product_model->view_product_details(' where p.quantity>0 and p.status="Publish" and u.group="Seller" and u.status="Active" or p.status="Publish" and p.quantity > 0 and p.user_id=0');
        if ($productDetails->num_rows() > 0) {
            $productId = array();
            foreach ($productDetails->result() as $productRow) {
                array_push($productId, $productRow->id);
            }
            array_filter($productId);
            shuffle($productId);
            $pid = $productId[0];
            $productName = '';
            foreach ($productDetails->result() as $productRow) {
                if ($productRow->id == $pid) {
                    $productName = $productRow->product_name;
                }
            }
            if ($productName == '') {
                redirect(base_url());
            } else {
                $link = 'things/' . $pid . '/' . url_title($productName, '-');
                redirect($link);
            }
        } else {
            redirect(base_url());
        }
    }

    public function load_short_url() {
        $short_url = $this->uri->segment(2, 0);
        if ($short_url != '') {
            $url_details = $this->product_model->get_all_details(SHORTURL, array('short_url' => $short_url));
            if ($url_details->num_rows() == 1) {
                redirect($url_details->row()->long_url);
            } else {
                show_error('Invalid short url provided. Make sure the url you typed is correct. <a href="' . base_url() . '">Click here</a> to go home page.', '404', 'Invalid Url');
            }
        }
    }

    public function get_short_url() {
        $returnStr['status_code'] = 0;
        if ($this->checkLogin('U') == '') {
            $returnStr['message'] = 'Login required';
        } else {
            $url = $this->input->post('url');
            $product_type = $this->input->post('product_type');
            $pidArr = explode('things/', $url);
            $pidArr = explode('/', $pidArr[1]);
            $pid = $pidArr[0];
            if ($product_type == 'selling') {
                $product_details = $this->product_model->get_all_details(PRODUCT, array('id' => $pid));
                $pid = $product_details->row()->seller_product_id;
            }

            //Check same refer url exists in db
            $check_url = $this->product_model->get_all_details(SHORTURL, array('product_id' => $pid, 'user_id' => $this->checkLogin('U')));

            if ($check_url->num_rows() > 0) {
                $short_url = $check_url->row()->short_url;
            } else {
                $short_url = $this->get_rand_str('6');
                $checkId = $this->product_model->get_all_details(SHORTURL, array('short_url' => $short_url));
                while ($checkId->num_rows() > 0) {
                    $short_url = $this->get_rand_str('6');
                    $checkId = $this->product_model->get_all_details(SHORTURL, array('short_url' => $short_url));
                }
                $this->product_model->simple_insert(SHORTURL, array('short_url' => $short_url, 'long_url' => $url, 'product_id' => $pid, 'user_id' => $this->checkLogin('U')));
            }

            $returnStr['short_url'] = base_url() . 't/' . $short_url;
            $returnStr['status_code'] = 1;
        }
        echo json_encode($returnStr);
    }

    public function display_product_detail() {
    	$this->load->library('encrypt');
        $pid = $this->uri->segment(2, 0);
        $key = "dwnldkey";
        $pdf = $this->session->userdata('pfile');
        $this->session->unset_userdata('pfile');
        $file_name = $this->encrypt->decode($pdf, $key);
        //$file_name = $pdf;

        $splitVal = array_filter(explode('@', $_GET['id']));
        $checkUser = $this->product_model->get_all_details(PAYMENT, array('product_download_code' => $this->input->get('id')));
        if ($file_name != '' && $checkUser->row()->user_id == $this->checkLogin('U')) {
            $fullPath = base_url() . "pdf-doc/pdf/" . $file_name;
            $fsize = filesize($fullPath);
            $path_parts = pathinfo($fullPath);

            $ext = strtolower($path_parts["extension"]);
            switch ($ext) {
                case "pdf":
                    header("Content-Disposition: attachment; filename=\"" . $path_parts["basename"] . "\"");
                    header("Content-type: application/pdf");
                    break;
                default;
                    header("Content-type: application/octet-stream");
                    header("Content-Disposition: attachment; filename=\"" . $path_parts["basename"] . "\"");
            }
            if ($fsize) {
                header("Content-length: $fsize");
            }
            readfile($fullPath);
        }

        $limit = 0;
        $relatedArr = array();
        $relatedProdArr = array();
        //$condition = " where p.id = '".$pid."' AND p.status = 'Publish'";
        $condition = "  where p.status='Publish' and u.group='Seller' and u.status='Active' and p.id='" . $pid . "' or p.status='Publish' and p.user_id=0 and p.id='" . $pid . "'";
        $this->data['productDetails'] = $this->product_model->view_product_details($condition);
		$this->data['PrdShipping'] = $this->product_model->view_subshipping_details_join($pid);
		#echo "<pre>"; print_r($PrdShipping->result());die;
        $PrdAttrVal = $this->product_model->view_subproduct_details_join($pid);
        $attributes = array();
        foreach ($PrdAttrVal->result() as $attrSet) {
            if (array_key_exists($attrSet->attr_type, $attributes)) {
                $attributes[$attrSet->attr_type][] = $attrSet;
            } else {
                $attributes[$attrSet->attr_type][] = $attrSet;
            }
        }
        $this->data['attributes'] = $attributes;
        $this->data['PrdAttrVal'] = $PrdAttrVal;
		# echo "<pre>";print_r($PrdAttrVal);die;

	    $this->data['productComment'] = '';
        if ($this->data['productDetails']->num_rows() == 1) {
            $this->data['productComment'] = $this->product_model->view_product_comments_details('where c.product_id=' . $this->data['productDetails']->row()->seller_product_id . ' order by c.dateAdded desc');

            $catArr = explode(',', $this->data['productDetails']->row()->category_id);
            if(count($catArr) > 0){
                foreach($catArr as $cat){
                    if($limit > 2)
                        break;
                    if($cat != ''){
                        //$condition = " where p.category_id like '".$cat.",%' AND p.status = 'Publish' AND p.id != '".$pid."' OR p.category_id like '%,".$cat."' AND p.status = 'Publish' AND p.id != '".$pid."' OR p.category_id like '%,".$cat.",%' AND p.status = 'Publish' AND p.id != '".$pid."' OR p.category_id='".$cat."' AND p.status = 'Publish' AND p.id != '".$pid."'";
                        $condition = ' where FIND_IN_SET("' . $cat . '",p.category_id) and p.quantity>0 and p.status="Publish" and u.group="Seller" and u.status="Active" and p.id != "' . $pid . '" or p.status="Publish" and p.quantity > 0 and p.user_id=0 and FIND_IN_SET("' . $cat . '",p.category_id) and p.id != "' . $pid . '"';
                        $relatedProductDetails = $this->product_model->view_product_details($condition);
                        if ($relatedProductDetails->num_rows() > 0) {
                            foreach ($relatedProductDetails->result() as $relatedProduct) {
                                if (!in_array($relatedProduct->id, $relatedArr)) {
                                    array_push($relatedArr, $relatedProduct->id);
                                    $relatedProdArr[] = $relatedProduct;
                                    $limit++;
                                }
                            }
                        }
                    }
                }
            }
        }
        $this->data['relatedProductsArr'] = $relatedProdArr;
        $recentLikeArr = $this->product_model->get_recent_like_users($this->data['productDetails']->row()->seller_product_id);
        $recentUserLikes = array();
        if($recentLikeArr->num_rows() > 0){
            foreach ($recentLikeArr->result() as $recentLikeRow) {
                if ($recentLikeRow->user_id != '') {
                    $recentUserLikes[$recentLikeRow->user_id] = $this->product_model->get_recent_user_likes($recentLikeRow->user_id, $this->data['productDetails']->row()->seller_product_id);
                }
            }
        }
        $this->data['recentLikeArr'] = $recentLikeArr;
        $this->data['recentUserLikes'] = $recentUserLikes;
        $this->data['seller_product_details'] = $this->product_model->get_all_details(PRODUCT, array('user_id' => $this->data['productDetails']->row()->user_id, 'id !=' => $pid, 'status' => 'Publish'));
        $this->data['seller_affiliate_products'] = $this->product_model->get_all_details(USER_PRODUCTS, array('user_id' => $this->data['productDetails']->row()->user_id));
        $this->data['conditions'] = $this->product_model->get_all_details(CONDITION, array('status' => 'Active'));
		$pcodition = $this->data['productDetails']->row()->product_condition;
		if($pcodition == '' || $pcodition == 0 ){
			$pcodition = 31;
		}
			$this->data['currentcondition'] = $this->product_model->get_all_details(CONDITION, array('status' => 'Active','id' =>$pcodition));
		
        if ($this->data['productDetails']->row()->meta_title != '') {
            $this->data['meta_title'] = $this->data['productDetails']->row()->meta_title;
            $this->data['heading'] = $this->data['productDetails']->row()->meta_title;
        } else {
            $this->data['meta_title'] = $this->data['productDetails']->row()->product_name;
            $this->data['heading'] = $this->data['productDetails']->row()->product_name;
        }
        if ($this->data['productDetails']->row()->meta_keyword != '') {
            $this->data['meta_keyword'] = $this->data['productDetails']->row()->meta_keyword;
        } else {
            $this->data['meta_keyword'] = $this->data['productDetails']->row()->product_name;
        }
        if ($this->data['productDetails']->row()->meta_description != '') {
            $this->data['meta_description'] = $this->data['productDetails']->row()->meta_description;
        } else {
            $this->data['meta_description'] = $this->data['productDetails']->row()->product_name;
        }
        $this->data['product_feedback'] = $this->product_model->product_feedback_view($this->data['productDetails']->row()->user_id);
		#echo '<pre>'; print_r($this->data['product_feedback']);die;
        $this->data['removeCartProduct'] = $this->product_model->get_all_details(SHOPPING_CART, array('type'=>'cart','product_id' => $pid, 'user_id' => $this->checkLogin('U')));
        $this->data['removeAuctionProduct'] = $this->product_model->get_all_details(SHOPPING_CART, array('type'=>'auction','product_id' => $pid, 'user_id' => $this->checkLogin('U')));
		// echo '<pre>';print_r( $this->data['removeAuctionProduct']->result());die;
        $this->data['image'] = explode(',', $this->data['productDetails']->row()->image);

        $this->load->view('site/product/product_detail', $this->data);
    }

    public function delete_featured_find() {
        $uid = $this->checkLogin('U');
        $dataArr = array('feature_product' => '');
        $condition = array('id' => $uid);
        $this->product_model->update_details(USERS, $dataArr, $condition);
        echo '1';
    }

    public function add_featured_find() {
        $pid = $this->input->post('tid');
        $uid = $this->checkLogin('U');
        $dataArr = array('feature_product' => $pid);
        $condition = array('id' => $uid);
        $this->product_model->update_details(USERS, $dataArr, $condition);
        $datestring = "%Y-%m-%d %h:%i:%s";
        $time = time();
        $createdTime = mdate($datestring, $time);
        $actArr = array(
            'activity' => 'featured',
            'activity_id' => $pid,
            'user_id' => $this->checkLogin('U'),
            'activity_ip' => $this->input->ip_address(),
            'created' => $createdTime
        );
        $this->product_model->simple_insert(NOTIFICATIONS, $actArr);
        $this->send_feature_noty_mail($pid);
        echo '1';
    }

    /* Ajax update for Product Details product */

    public function ajaxProductDetailAttributeUpdate() {
        $attrPriceVal = $this->product_model->get_all_details(SUBPRODUCT, array('pid' => $this->input->post('attId'), 'product_id' => $this->input->post('prdId')));
        if ($attrPriceVal->num_rows() == 0) {
            $product_details = $this->product_model->get_all_details(PRODUCT, array('id' => $this->input->post('prdId')));
            echo '0|' . $product_details->row()->sale_price;
        } else {
            echo $attrPriceVal->row()->attr_id . '|' . $attrPriceVal->row()->attr_price . '|' . $attrPriceVal->row()->attr_qty;
        }
    }

    public function share_with_someone() {
        $returnStr['status_code'] = 0;
        $thing = array();
        $thing['url'] = $this->input->post('url');
        $thing['name'] = $this->input->post('name');
        $thing['id'] = $this->input->post('oid');
        $thing['refid'] = $this->input->post('ooid');
        $thing['msg'] = $this->input->post('message');
        $thing['uname'] = $this->input->post('uname');
        $thing['timage'] = base_url() . $this->input->post('timage');
        $email = $this->input->post('emails');
        $users = $this->input->post('users');
        if (valid_email($email)) {
            $this->send_thing_share_mail($thing, $email);
            $returnStr['status_code'] = 1;
        } else {
            if ($this->lang->line('invalid_email') != '')
                $returnStr['message'] = $this->lang->line('invalid_email');
            else
                $returnStr['message'] = 'Invalid email';
        }
        echo json_encode($returnStr);
    }

    public function send_thing_share_mail($thing = '', $email = '') {

        $newsid = '2';
        $template_values = $this->product_model->get_newsletter_template_details($newsid);
        $adminnewstemplateArr = array(
            'meta_title' => $this->config->item('meta_title'),
            'logo' => $this->data['logo'],
            'uname' => ucfirst($thing['uname']),
            'name' => $thing['name'],
            'url' => $thing['url'],
            'msg' => $thing['msg'],
            'timage' => $thing['timage'],
            'email_title' => $this->config->item('email_title')
        );
        extract($adminnewstemplateArr);
        if ($this->data['userDetails']->row()->full_name != '') {
            $uname = $this->data['userDetails']->row()->full_name;
        }
        $subject = $template_values['news_subject'];
        $message .= '<!DOCTYPE HTML>
								<html>
								<head>
								<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
								<meta name="viewport" content="width=device-width"/>
								<title>' . $adminnewstemplateArr['meta_title'] . ' - Share Things</title>
								<body>';
        include('./newsletter/registeration' . $newsid . '.php');

        $message .= '</body>
								</html>';
        if ($template_values['sender_name'] == '' && $template_values['sender_email'] == '') {
            $sender_email = $this->config->item('site_contact_mail');
            $sender_name = $this->config->item('email_title');
        } else {
            $sender_name = $template_values['sender_name'];
            $sender_email = $template_values['sender_email'];
        }

        $email_values = array('mail_type' => 'html',
            'from_mail_id' => $sender_email,
            'mail_name' => $sender_name,
            'to_mail_id' => $email,
            'subject_message' => $subject,
            'body_messages' => $message
        );
        $email_send_to_common = $this->product_model->common_email_send($email_values);

        /* 		echo $this->email->print_debugger();die;
         */
    }

    public function add_have_tag() {
        $returnStr['status_code'] = 0;
        $tid = $this->input->post('thing_id');
        $uid = $this->checkLogin('U');
        if ($uid != '') {
            $ownArr = explode(',', $this->data['userDetails']->row()->own_products);
            $ownCount = $this->data['userDetails']->row()->own_count;
            if (!in_array($tid, $ownArr)) {
                array_push($ownArr, $tid);
                $ownCount++;
                $dataArr = array('own_products' => implode(',', $ownArr), 'own_count' => $ownCount);
                $wantProducts = $this->product_model->get_all_details(WANTS_DETAILS, array('user_id' => $this->checkLogin('U')));
                if ($wantProducts->num_rows() == 1) {
                    $wantProductsArr = explode(',', $wantProducts->row()->product_id);
                    if (in_array($tid, $wantProductsArr)) {
                        if (($key = array_search($tid, $wantProductsArr)) !== false) {
                            unset($wantProductsArr[$key]);
                        }
                        $wantsCount = $this->data['userDetails']->row()->want_count;
                        $wantsCount--;
                        $dataArr['want_count'] = $wantsCount;
                        $this->product_model->update_details(WANTS_DETAILS, array('product_id' => implode(',', $wantProductsArr)), array('user_id' => $uid));
                    }
                }
                $this->product_model->update_details(USERS, $dataArr, array('id' => $uid));
                $returnStr['status_code'] = 1;
            }
        }
        echo json_encode($returnStr);
    }

    public function delete_have_tag() {
        $returnStr['status_code'] = 0;
        $tid = $this->input->post('thing_id');
        $uid = $this->checkLogin('U');
        if ($uid != '') {
            $ownArr = explode(',', $this->data['userDetails']->row()->own_products);
            $ownCount = $this->data['userDetails']->row()->own_count;
            if (in_array($tid, $ownArr)) {
                if ($key = array_search($tid, $ownArr) !== false) {
                    unset($ownArr[$key]);
                    $ownCount--;
                }
                $this->product_model->update_details(USERS, array('own_products' => implode(',', $ownArr), 'own_count' => $ownCount), array('id' => $uid));
                $returnStr['status_code'] = 1;
            }
        }
        echo json_encode($returnStr);
    }

    public function upload_product_image() {
        $returnStr['status_code'] = 0;
        $config['overwrite'] = FALSE;
        $config['allowed_types'] = 'jpg|jpeg|gif|png';
        //	    $config['max_size'] = 2000;
        $config['upload_path'] = './images/product';
        //$config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);
        if ($this->upload->do_upload('thefile')) {
            $imgDetails = $this->upload->data();
            $returnStr['image']['url'] = base_url() . 'images/product/' . $imgDetails['file_name'];
            $returnStr['image']['width'] = $imgDetails['image_width'];
            $returnStr['image']['height'] = $imgDetails['image_height'];
            $returnStr['image']['name'] = $imgDetails['file_name'];
            //$this->imageResizeWithSpace(600, 600, $imgDetails['file_name'], './images/product/');
            $this->imageResizeWithSpace(210, 210, $imgDetails['file_name'], './images/product/thumb/');
            $returnStr['status_code'] = 1;
        } else {
            if ($this->lang->line('cant_upload') != '')
                $returnStr['message'] = $this->lang->line('cant_upload');
            else
                $returnStr['message'] = 'Can\'t be upload';
        }
        echo json_encode($returnStr);
    }

    public function add_new_thing() {
        $returnStr['status_code'] = 0;
        $returnStr['message'] = '';
        if ($this->checkLogin('U') != '') {
            $short_url = $this->get_rand_str('6');
            $checkId = $this->product_model->get_all_details(SHORTURL, array('short_url' => $short_url));
            while ($checkId->num_rows() > 0) {
                $short_url = $this->get_rand_str('6');
                $checkId = $this->product_model->get_all_details(SHORTURL, array('short_url' => $short_url));
            }
            $result = $this->product_model->add_user_product($this->checkLogin('U'), $short_url);
            $product_id = $this->db->insert_id();
            $user_details = $this->product_model->get_all_details(USERS, array('id' => $this->checkLogin('U')));
            if ($user_details->row()->group == "Seller") {
                $user_product = $this->product_model->get_all_details(USER_PRODUCTS, array('id' => $product_id));
                $dataArr = array('activity_name' => "add_product",
                    'activity_id' => $user_product->row()->seller_product_id,
                    'user_id' => $this->checkLogin('U')
                );
                $this->product_model->simple_insert(USER_ACTIVITY, $dataArr);
            }
            if ($result['image'] != '') {
                $this->imageResizeWithSpace(210, 210, $result['image'], './images/product/thumb/');
                //$this->imageResizeWithSpace(600, 600, $result['image'], './images/product/');
            }
            $returnStr['status_code'] = 1;
            $userDetails = $this->data['userDetails'];
            $total_added = $userDetails->row()->products;
            $total_added++;
            $this->product_model->update_details(USERS, array('products' => $total_added), array('id' => $this->checkLogin('U')));
            $returnStr['thing_url'] = base_url() . 'user/' . $userDetails->row()->user_name . '/things/' . $result['pid'] . '/' . url_title($this->input->post('name'), '-');
        }
        echo json_encode($returnStr);
    }

    public function add_product_image() {
        $this->data['heading'] = 'Adding product image';
        $this->load->view('site/user/add_product_image', $this->data);
    }

    public function add_product_image_process() {
        //	    $config['max_size'] = 2000;
        if ($this->input->post('img_link') != '') {
            chmod($this->input->post('img_link'), 777);
            $config['upload_path'] = $this->input->post('img_link');
        } else {
            $config['upload_path'] = './';
        }
        //$config['encrypt_name'] = TRUE;
        $config['allowed_types'] = '*';
        $config['overwrite'] = TRUE;
        $this->load->library('upload', $config);
        if ($this->upload->do_upload('file_name')) {
            $this->setErrorMessage('success', 'Image uploaded');
            redirect('site/product/add_product_image');
        } else {
            $this->setErrorMessage('error', strip_tags($this->upload->display_errors()));
            redirect('site/product/add_product_image');
        }
    }

    public function add_new_thing_ext() {
        $returnStr['status_code'] = 0;
        $returnStr['message'] = '';
        if ($this->checkLogin('U') != '') {
            $short_url = $this->get_rand_str('6');
            $checkId = $this->product_model->get_all_details(SHORTURL, array('short_url' => $short_url));
            while ($checkId->num_rows() > 0) {
                $short_url = $this->get_rand_str('6');
                $checkId = $this->product_model->get_all_details(SHORTURL, array('short_url' => $short_url));
            }
            $result = $this->product_model->add_user_product_ext($this->checkLogin('U'), $short_url);
            if ($result['image'] != '') {
                $this->imageResizeWithSpace(210, 210, $result['image'], './images/product/thumb/');
            }
            $returnStr['status_code'] = 1;
            $userDetails = $this->data['userDetails'];
            $total_added = $userDetails->row()->products;
            $total_added++;
            $this->product_model->update_details(USERS, array('products' => $total_added), array('id' => $this->checkLogin('U')));
            $returnStr['thing_url'] = base_url() . 'user/' . $userDetails->row()->user_name . '/things/' . $result['pid'] . '/' . url_title($this->input->post('name'), '-');
        }
        echo json_encode($returnStr);
    }

    public function extract_image_urls() {
        include('./simple_html_dom.php');
        //	$returnStr['status_code'] = 0;
        $returnStr['response'] = array();
        $host_name_arr = explode('/', $this->input->get('url'));
        $url = 'http://' . $this->input->get('url');
        $url = str_replace(" ", '%20', $url);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
//		curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.52 Safari/537.17');
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_REFERER, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); //Set curl to return the data instead of printing it to the browser.
        $html_cnt = curl_exec($ch);
        //echo $html_cnt;die;
        /* 		echo "<pre>"; print_r(curl_getinfo($ch)) . '<br/>';
          echo "<pre>"; var_dump(curl_getinfo($ch, CURLINFO_HTTP_CODE)) . '<br/>';
          echo curl_errno($ch) . '<br/>';
          echo curl_error($ch) . '<br/>'; */
        curl_close($ch);
        /* $html_cnt = file_get_html($url); */
        $html_base = new simple_html_dom();
        $html_base->load($html_cnt);
//		var_dump($html_cnt);//die;
        foreach ($html_base->find('img') as $imageURL) {      ////Map Url
            if (substr($imageURL->src, 0, 4) == 'http' || substr($imageURL->src, 0, 2) == '//') {
                //				if (substr($imageURL->src, -3,3)=='jpg' || substr($imageURL->src, -3,3)=='png' || substr($imageURL->src, -3,3)=='gif' || substr($imageURL->src, -4,4)=='jpeg'){
                array_push($returnStr['response'], $imageURL->src);
                $returnStr['alt'][] = $imageURL->alt;
                //				}
            } else {
                //				if (substr($imageURL->src, -3,3)=='jpg' || substr($imageURL->src, -3,3)=='png' || substr($imageURL->src, -3,3)=='gif' || substr($imageURL->src, -4,4)=='jpeg'){
                if (substr($imageURL->src, 0, 1) == '/') {
                    array_push($returnStr['response'], 'http://' . $host_name_arr[0] . $imageURL->src);
                } else {
                    array_push($returnStr['response'], 'http://' . $host_name_arr[0] . '/' . $imageURL->src);
                }
                $returnStr['alt'][] = $imageURL->alt;
                //				}
            }
        }
        foreach ($html_base->find('title') as $titleName) {
            $returnStr['title'][] = $titleName->innertext;
        }
        $html_base->clear();
        unset($html_base);
        //echo "<pre>";print_r($returnStr);die;
        echo json_encode($returnStr);
    }

    public function display_user_thing() {

        $uname = $this->uri->segment(2, 0);
        $pid = $this->uri->segment(4, 0);
        $this->data['productUserDetails'] = $this->product_model->get_all_details(USERS, array('user_name' => $uname));
        $this->data['productDetails'] = $this->product_model->view_notsell_product_details(' where p.seller_product_id="' . $pid . '" and p.status="Publish"');
        if ($this->data['productDetails']->num_rows() == 1) {
            $this->data['heading'] = $this->data['productDetails']->row()->product_name;
            $categoryArr = explode(',', $this->data['productDetails']->row()->category_id);
            $catID = 0;
            if (count($categoryArr) > 0) {
                foreach ($categoryArr as $catRow) {
                    if ($catRow != '') {
                        $catID = $catRow;
                        break;
                    }
                }
            }
            $this->data['relatedProductsArr'] = $this->product_model->get_products_by_category($catID);
            //echo "<pre>";print_r($this->data['relatedProductsArr']);die;
            if ($this->data['productDetails']->row()->meta_title != '') {
                $this->data['meta_title'] = $this->data['productDetails']->row()->meta_title;
            } else {
                $this->data['meta_title'] = $this->data['productDetails']->row()->product_name;
            }
            if ($this->data['productDetails']->row()->meta_keyword != '') {
                $this->data['meta_keyword'] = $this->data['productDetails']->row()->meta_keyword;
            } else {
                $this->data['meta_keyword'] = $this->data['productDetails']->row()->product_name;
            }
            if ($this->data['productDetails']->row()->meta_description != '') {
                $this->data['meta_description'] = $this->data['productDetails']->row()->meta_description;
            } else {
                $this->data['meta_description'] = $this->data['productDetails']->row()->product_name;
            }
            $this->data['image'] = explode(',', $this->data['productDetails']->row()->image);
            $this->load->view('site/product/display_user_product', $this->data);
        } else {
            $this->data['image'] = explode(',', $this->data['productDetails']->row()->image);
            $this->load->view('site/product/product_detail', $this->data);
        }
    }

    public function sales_create() {
        if ($this->checkLogin('U') == '') {
            redirect('login');
        } else {
            $userType = $this->data['userDetails']->row()->group;
            if ($userType == 'Seller') {
                $pid = $this->input->get('ntid');
                $productDetails = $this->product_model->get_all_details(USER_PRODUCTS, array('seller_product_id' => $pid));
                if ($productDetails->num_rows() == 1) {
                    if ($productDetails->row()->user_id == $this->data['userDetails']->row()->id) {
                        $this->data['productDetails'] = $productDetails;
                        $this->data['editmode'] = '0';
                        $this->data['shippingCostDetails'] = $shippingCostDetails = $this->product_model->get_all_details(SHIPPING_COST, array('product_id' => $pid));
                        $this->data ['conditionsList'] = $this->product_model->get_all_details(CONDITION,array('status'=>'Active'));
                        $this->load->view('site/product/edit_seller_product', $this->data);
                    } else {
                        show_404();
                    }
                } else {
                    show_404();
                }
            } else {
                redirect('seller-signup');
            }
        }
    }

    /**
     *
     * Ajax function for delete the product pictures
     */
    public function editPictureProducts() {
        $ingIDD = $this->input->post('imgId');
        $currentPage = $this->input->post('cpage');
        $id = $this->input->post('val');
        $productImage = explode(',', $this->session->userdata('product_image_' . $ingIDD));
        if (count($productImage) < 2) {
            echo json_encode("No");
            exit();
        } else {
            $empImg = 0;
            foreach ($productImage as $product) {
                if ($product != '') {
                    $empImg++;
                }
            }
            if ($empImg < 2) {
                echo json_encode("No");
                exit();
            }
            $this->session->unset_userdata('product_image_' . $ingIDD);
            $resultVar = $this->setPictureProducts($productImage, $this->input->post('position'));
            $insertArrayItems = trim(implode(',', $resultVar)); //need validation here...because the array key changed here

            $this->session->set_userdata(array('product_image_' . $ingIDD => $insertArrayItems));
            $dataArr = array('image' => $insertArrayItems);
            $condition = array('id' => $ingIDD);
            $this->product_model->update_details(PRODUCT, $dataArr, $condition);
            echo json_encode($insertArrayItems);
        }
    }

    public function edit_product_detail() {

        if ($this->checkLogin('U') == '') {
            redirect('login');
        } else {
            $pid = $this->uri->segment(2, 0);
            $viewMode = $this->uri->segment(4, 0);

            $productDetails = $this->product_model->get_all_details(USER_PRODUCTS, array('seller_product_id' => $pid));
            if ($productDetails->num_rows() == 1) {
                if ($productDetails->row()->user_id == $this->checkLogin('U')) {
                    $this->data['productDetails'] = $productDetails;
					$catId = explode(',',$productDetails->row()->category_id);

					$this->data['SubCatId1'] = $this->product_model->get_all_details(CATEGORY,array('rootID'=> $catId[0],'status' =>'Active'))->result();

					$this->data['SubCatId2'] = $this->product_model->get_all_details(CATEGORY,array('rootID'=> $catId[1],'status' =>'Active'))->result();
					$this->load->view('site/product/edit_user_product', $this->data);
                } else {
                    show_404();
                }
            } else {
                $productDetails = $this->product_model->get_all_details(PRODUCT, array('seller_product_id' => $pid));
                $main_category_id = $productDetails->row()->category_id;
                $this->data['CatId']=$catId=explode(',',$main_category_id);
                $this->data['SubCatId1']=$this->product_model->get_all_details(CATEGORY,array('rootID'=> $catId[0],'status' =>'Active'))->result();
                $this->data['SubCatId2']=$this->product_model->get_all_details(CATEGORY,array('rootID'=> $catId[1],'status' =>'Active'))->result();
                            
                $this->data['categoryView'] = $this->product_model->get_category_details($productDetails->row()->category_id);
                $this->data['atrributeValue'] = $this->product_model->view_atrribute_details();
                $this->data['PrdattrVal'] = $this->product_model->view_product_atrribute_details();
                $this->data['SubPrdVal'] = $this->product_model->view_subproduct_details($productDetails->row()->id);
				$this->data['PrdShipping'] = $this->product_model->view_subshipping_details_join($productDetails->row()->id);
                //echo "<pre>";print_r($this->data['PrdShipping']->result());die;
                if ($productDetails->num_rows() == 1) {
                    if ($productDetails->row()->user_id == $this->checkLogin('U')) {
                        $this->data['productDetails'] = $productDetails;
                        $this->data['shippingCostDetails'] = $shippingCostDetails = $this->product_model->get_all_details(SHIPPING_COST, array('product_id' => $pid));
                        $this->data['editmode'] = '1';
                        if ($viewMode == '') {
							
							$this->load->view('site/product/edit_seller_product', $this->data);
                        } else {
                            $this->load->view('site/product/edit_seller_product_' . $viewMode, $this->data);
                        }
                    } else {
                        show_404();
                    }
                } else {
                    show_404();
                }
            }
        }
    }

    public function edit_user_product_process() {
        $mode = $this->input->post('submit');
        $pid = $this->input->post('productID');
        if ($pid != '') {
            if ($mode == 'Upload') {
                $config['overwrite'] = FALSE;
                $config['allowed_types'] = 'jpg|jpeg|gif|png';
                //			    $config['max_size'] = 2000;
                $config['upload_path'] = './images/product';
                $this->load->library('upload', $config);
                if ($this->upload->do_upload('uploadphoto')) {
                    $imgDetails = $this->upload->data();
                    $this->imageResizeWithSpace(600, 600, $imgDetails['file_name'], './images/product/');
                    $this->imageResizeWithSpace(210, 210, $imgDetails['file_name'], './images/product/thumb/');
                    $dataArr['image'] = $imgDetails['file_name'];
                    $this->product_model->update_details(USER_PRODUCTS, $dataArr, array('seller_product_id' => $pid));
                    if ($this->lang->line('poto_chage_succ') != '')
                        $lg_err_msg = $this->lang->line('poto_chage_succ');
                    else
                        $lg_err_msg = 'Photo changed successfully';
                    $this->setErrorMessage('success', $lg_err_msg);
                    echo '<script>window.history.go(-1)</script>';
                }else {
                    if ($this->lang->line('cant_upload') != '')
                        $lg_err_msg = $this->lang->line('cant_upload');
                    else
                        $lg_err_msg = 'Can\'t able to upload';
                    $this->setErrorMessage('error', $lg_err_msg);
                    echo '<script>window.history.go(-1)</script>';
                }
            }else {
                $excludeArr = array('productID', 'submit', 'uploadphoto','level1_sub_cat');
                $dataArr = array(
                    'seourl' => url_title($this->input->post('product_name'), '-'),
                    'modified' => 'now()',
					'category_id' => implode(',', $this->input->post('level1_sub_cat'))
                );
                $this->product_model->commonInsertUpdate(USER_PRODUCTS, 'update', $excludeArr, $dataArr, array('seller_product_id' => $pid));
                if ($this->lang->line('det_updat_succ') != '')
                    $lg_err_msg = $this->lang->line('det_updat_succ');
                else
                    $lg_err_msg = 'Details updated successfully';
                $this->setErrorMessage('success', $lg_err_msg);
                redirect('user/' . $this->data['userDetails']->row()->user_name . '/things/' . $pid . '/' . url_title($this->input->post('product_name'), '-'));
            }
        }
    }

    public function update_price_range_in_table($mode = '', $price_range = '', $product_id = '0', $old_product_details = '') {
        $list_values = $this->product_model->get_all_details(LIST_VALUES, array('list_value' => $price_range));
        if ($list_values->num_rows() == 1) {
            $products = explode(',', $list_values->row()->products);
            $product_count = $list_values->row()->product_count;
            if ($mode == 'add') {
                if (!in_array($product_id, $products)) {
                    array_push($products, $product_id);
                    $product_count++;
                }
            } else if ($mode == 'edit') {
                $old_price_range = '';
                if ($old_product_details != '' && count($old_product_details) > 0 && $old_product_details->num_rows() == 1) {
                    $old_price_range = $old_product_details->row()->price_range;
                }
                if ($old_price_range != '' && $old_price_range != $price_range) {
                    $old_list_values = $this->product_model->get_all_details(LIST_VALUES, array('list_value' => $old_price_range));
                    if ($old_list_values->num_rows() == 1) {
                        $old_products = explode(',', $old_list_values->row()->products);
                        $old_product_count = $old_list_values->row()->product_count;
                        if (in_array($product_id, $old_products)) {
                            if (($key = array_search($product_id, $old_products)) !== false) {
                                unset($old_products[$key]);
                                $old_product_count--;
                                $updateArr = array('products' => implode(',', $old_products), 'product_count' => $old_product_count);
                                $updateCondition = array('list_value' => $old_price_range);
                                $this->product_model->update_details(LIST_VALUES, $updateArr, $updateCondition);
                            }
                        }
                    }
                    if (!in_array($product_id, $products)) {
                        array_push($products, $product_id);
                        $product_count++;
                    }
                } else if ($old_price_range != '' && $old_price_range == $price_range) {
                    if (!in_array($product_id, $products)) {
                        array_push($products, $product_id);
                        $product_count++;
                    }
                }
            }
            $updateArr = array('products' => implode(',', $products), 'product_count' => $product_count);
            $updateCondition = array('list_value' => $price_range);
            $this->product_model->update_details(LIST_VALUES, $updateArr, $updateCondition);
        }
    }

    public function sell_it() {
	//	echo("<pre>");print_r($this->input->post());die;
        $mode = $this->uri->segment(4, 0);
        $pid = $this->input->post('PID');
        $nextMode = $this->input->post('nextMode');
        $default_ship_cost = $this->input->post('default_ship_cost');
        $shipping_cost = $this->input->post('shipping_cost');
        //$another_shipping_cost = $this->input->post('another_shipping_cost');
        $excludeArr = array('PID', 'nextMode', 'served_specific', 'changeorder', 'imaged', 'gateway_tbl_length', 'category_id', 'attribute_name', 'attribute_val', 'separate_cost', 'separate_anotheritem', 'country_code', 'default_ship_cost');
        if ($mode == '1') {
            $price_range = 0;
            $price = $this->input->post('sale_price');
            if ($price > 0 && $price < 21) {
                $price_range = '1-20';
            } else if ($price > 20 && $price < 101) {
                $price_range = '21-100';
            } else if ($price > 100 && $price < 201) {
                $price_range = '101-200';
            } else if ($price > 200 && $price < 501) {
                $price_range = '201-500';
            } else if ($price > 500) {
                $price_range = '501+';
            }
            if ($pid == '') {
                $old_product_details = array();
                //$condition = array('product_name' => $product_name);
            } else {
                $old_product_details = $this->product_model->get_all_details(PRODUCT, array('seller_product_id' => $pid));
                //$condition = array('product_name' => $product_name,'seller_product_id !=' => $pid);
            }
            $dataArr = array('seller_product_id' => $pid);
            $checkProduct = $this->product_model->get_all_details(PRODUCT, $dataArr);
            if ($checkProduct->num_rows() == 0) {
                //echo("xxxxxxx");die;
                $userProduct = $this->product_model->get_all_details(USER_PRODUCTS, $dataArr);
                //echo("<pre>");print_r($userProduct->result());die;
                if ($userProduct->num_rows() == 1) {
                    $dataArr['image'] = $userProduct->row()->image;
                    $dataArr['seourl'] = url_title($this->input->post('product_name'), '-');
                    $dataArr['user_id'] = $userProduct->row()->user_id;
                    $dataArr['price_range'] = $price_range;
                    $dataArr['category_id'] = $userProduct->row()->category_id;
                    $dataArr['shipping_cost'] = $default_ship_cost;
                    //$dataArr['another_shipping_cost']	=	$another_shipping_cost;
                    //youtube id find
                    $youtube_link = $this->input->post('youtube');
                    parse_str(parse_url($youtube_link, PHP_URL_QUERY), $youtube_id);
                    //youtube id find
                    $dataArr['youtube'] = $youtube_id['v'];
                  
                    /* $served_specific = $this->input->post('served_specific');

                      if ($served_specific!=1) $served_specific=0;
                      $dataArr['served_specific']	=	$served_specific; */
                    //print_r($dataArr);die;
                    $this->product_model->commonInsertUpdate(PRODUCT, 'insert', $excludeArr, $dataArr);
                    $product_id = $this->product_model->get_last_insert_id();
                    $this->update_price_range_in_table('add', $price_range, $product_id, $old_product_details);
                    $this->product_model->commonDelete(USER_PRODUCTS, array('seller_product_id' => $pid));
                    $excludeShippArr = array('PID', 'served_specific', 'nextMode', 'changeorder', 'imaged', 'gateway_tbl_length', 'category_id', 'attribute_name', 'attribute_val', 'separate_cost', 'country_code', 'default_ship_cost', 'product_name', 'description', 'shipping_policies', 'excerpt', 'quantity', 'ship_immediate', 'sku', 'weight', 'price', 'sale_price', 'separate_ship_cost', 'separate_anotheritem', 'country_code', 'product_id', 'youtube', 'product_type');
                    $country_codeArr = $this->input->post('country_code');
                    $separate_costArr = $this->input->post('separate_cost');
                    $anotheritem_costArr = $this->input->post('separate_anotheritem');
                    $shipCostDetails = $this->product_model->get_all_details(SHIPPING_COST, array('product_id' => $pid));
                    $this->product_model->commonDelete(SHIPPING_COST, array('product_id' => $pid));
                    $i = 0;
                    foreach ($country_codeArr as $country_codeRow) {
                        $shippArr = array(
                            'country_code' => $country_codeRow,
                            'separate_ship_cost' => $separate_costArr[$i],
                            //'anotheritem_ship_cost'	=>	$anotheritem_costArr[$i],
                            'product_id' => $pid
                        );
                        $this->product_model->commonInsertUpdate(SHIPPING_COST, 'insert', $excludeShippArr, $shippArr);

                        $i++;
                    }
                    if ($this->lang->line('change_saved') != '')
                        $lg_err_msg = $this->lang->line('change_saved');
                    else
                        $lg_err_msg = 'Yeah ! changes have been saved';
                    $this->setErrorMessage('success', $lg_err_msg);
                    $addedProd = $this->session->userdata('prodID');
                    if ($addedProd == '') {
                        $addedProd = array();
                    }
                    array_push($addedProd, $pid);
                    $this->session->set_userdata('prodID', $addedProd);
                    redirect('things/' . $pid . '/edit/' . $nextMode);
                }
            } else {
                //echo("uuuuuu");die;
                $dataArr['seourl'] = url_title($this->input->post('product_name'), '-');
                $dataArr['price_range'] = $price_range;
                $dataArr['shipping_cost'] = $default_ship_cost;
                //$dataArr['another_shipping_cost'] = $another_shipping_cost;
                
                /* $served_specific = $this->input->post('served_specific');
                  if ($served_specific!=1) $served_specific=0;
                  $dataArr['served_specific']	=	$served_specific; */
                //youtube id find
                $youtube_link = $this->input->post('youtube');
                parse_str(parse_url($youtube_link, PHP_URL_QUERY), $youtube_id);
                //youtube id find
				$dataArr['country_code'] = $this->input->post('country_code');
                $dataArr['youtube'] = $youtube_id['v'];
                $excludeShippArr = array('PID', 'nextMode', 'served_specific', 'changeorder', 'imaged', 'gateway_tbl_length', 'category_id', 'attribute_name', 'attribute_val', 'separate_cost', 'country_code', 'default_ship_cost', 'product_name', 'description', 'shipping_policies', 'excerpt', 'quantity', 'ship_immediate', 'sku', 'weight', 'price', 'sale_price', 'separate_ship_cost', 'separate_anotheritem', 'country_code', 'product_id', 'youtube', 'product_type', 'attribute_must');

                $country_codeArr = $this->input->post('country_code');
                $separate_costArr = $this->input->post('separate_cost');
                $anotheritem_costArr = $this->input->post('separate_anotheritem');
                //$shipCostDetails = $this->product_model->get_all_details(SHIPPING_COST,array('product_id'=>$pid));
                $this->product_model->commonDelete(SHIPPING_COST, array('product_id' => $pid));
                $i = 0;
                foreach ($country_codeArr as $country_codeRow) {
                    $shippArr = array(
                        'country_code' => $country_codeRow,
                        'separate_ship_cost' => $separate_costArr[$i],
                        //'anotheritem_ship_cost'	=>	$anotheritem_costArr[$i],
                        'product_id' => $pid
                    );
                    $this->product_model->commonInsertUpdate(SHIPPING_COST, 'insert', $excludeShippArr, $shippArr);

                    $i++;
                }
                $this->product_model->commonInsertUpdate(PRODUCT, 'update', $excludeArr, $dataArr, array('seller_product_id' => $pid));

                $this->update_price_range_in_table('edit', $price_range, $old_product_details->row()->id, $old_product_details);
                if ($this->lang->line('change_saved') != '')
                    $lg_err_msg = $this->lang->line('change_saved');
                else
                    $lg_err_msg = 'Yeah ! changes have been saved';
                $this->setErrorMessage('success', $lg_err_msg);
                redirect('things/' . $pid . '/edit');
            }
        }else if ($mode == 'seo') {
            $this->product_model->commonInsertUpdate(PRODUCT, 'update', $excludeArr, array(), array('seller_product_id' => $pid));
            if ($this->lang->line('change_saved') != '')
                $lg_err_msg = $this->lang->line('change_saved');
            else
                $lg_err_msg = 'Yeah ! changes have been saved';
            $this->setErrorMessage('success', $lg_err_msg);
            redirect('things/' . $pid . '/edit/' . $nextMode);
        }else if ($mode == 'shipping'){
            //echo "<pre>";print_r($_POST);die;
			$excludeArr = array('country_code','shipping_to','ship_to_id','shipping_cost','shipping_with_another','nextMode','PID');
			$countryCode = explode('|', $this->input->post('country_code'));
			$dataArr['country_code'] = $countryCode[1];
            $this->product_model->commonInsertUpdate(PRODUCT, 'update', $excludeArr, $dataArr, array('seller_product_id' => $pid));
			if($pid!=''){
				$product = $this->product_model->get_all_details(PRODUCT, array('seller_product_id'=>$pid));
				if($this->input->post('shipping_to') != ''){
					$this->product_model->commonDelete(SUB_SHIPPING,array('product_id'=>$product->row()->id));
					$ship_to = $this->input->post('shipping_to');
					$ship_to_id = $this->input->post('ship_to_id');
					$cost_individual = $this->input->post('shipping_cost');
					$cost_with_another = $this->input->post('shipping_with_another');
					for($i=0; $i < sizeof($ship_to); $i++){
						$ship_name = @explode('|', $ship_to[$i]);
						if($ship_to[$i] == 'Everywhere Else'){
							$shipName = 'Everywhere Else';
							$shipId = 232;
						} else {
							$shipName = $ship_name[2];
							$shipCode = $ship_name[1];
							$shipId = $ship_to_id[$i];
						}
						$seourlBase = $seourl = url_title($shipName, '-', TRUE);
						$seourl_check = '0';
						$duplicate_url = $this->product_model->get_all_details(SUB_SHIPPING,array('ship_seourl'=>$seourl));
						if($duplicate_url->num_rows()>0){
							$seourl = $seourlBase.'-'.$duplicate_url->num_rows();
						}else{
							$seourl_check = '1';
						}
						$urlCount = $duplicate_url->num_rows();
						while($seourl_check == '0'){
							$urlCount++;
							$duplicate_url = $this->product_model->get_all_details(SUB_SHIPPING,array('ship_seourl'=>$seourl));
							if ($duplicate_url->num_rows()>0){
								$seourl = $seourlBase.'-'.$urlCount;
							}else {
								$seourl_check = '1';
							}
						}
						$dataArrShip = array('product_id' => $product->row()->id,'ship_id' => $shipId, 'ship_name' => $shipName,'ship_cost' => $cost_individual[$i],'ship_seourl' => $seourl,'ship_code'=> $shipCode,'ship_other_cost' => $cost_with_another[$i]);
						$this->product_model->simple_insert(SUB_SHIPPING,$dataArrShip);
					}
				}
			}
            if($this->lang->line('change_saved')!= '')
                $lg_err_msg = $this->lang->line('change_saved');
            else
                $lg_err_msg = 'Yeah ! changes have been saved';
            $this->setErrorMessage('success', $lg_err_msg);
            redirect('things/' . $pid . '/edit/' . $nextMode);
        }else if ($mode == 'images') {
            $config['overwrite'] = FALSE;
            $config['allowed_types'] = 'jpg|jpeg|gif|png';
            //		    $config['max_size'] = 2000;
            $config['upload_path'] = './images/product';
            $this->load->library('upload', $config);
            //echo "<pre>";print_r($_FILES);die;
            $ImageName = '';
            if ($this->upload->do_multi_upload('product_image')) {
                $logoDetails = $this->upload->get_multi_upload_data();
                foreach ($logoDetails as $fileDetails) {
                    $this->imageResizeWithSpace(600, 600, $fileDetails['file_name'], './images/product/');
                    $this->imageResizeWithSpace(210, 210, $fileDetails['file_name'], './images/product/thumb/');
                    $ImageName .= $fileDetails['file_name'] . ',';
                }
            }
            $existingImage = $this->input->post('imaged');

            $newPOsitionArr = $this->input->post('changeorder');
            $imagePOsit = array();

            for ($p = 0; $p < sizeof($existingImage); $p++) {
                $imagePOsit[$newPOsitionArr[$p]] = $existingImage[$p];
            }

            ksort($imagePOsit);
            foreach ($imagePOsit as $keysss => $vald) {
                $imgArraypos[] = $vald;
            }
            $imagArraypo0 = @implode(",", $imgArraypos);
            $allImages = $imagArraypo0 . ',' . $ImageName;

            $dataArr = array('image' => $allImages);
            $this->product_model->update_details(PRODUCT, $dataArr, array('seller_product_id' => $pid));
            if ($this->lang->line('change_saved') != '')
                $lg_err_msg = $this->lang->line('change_saved');
            else
                $lg_err_msg = 'Yeah ! changes have been saved';
            $this->setErrorMessage('success', $lg_err_msg);
            redirect('things/' . $pid . '/edit/' . $nextMode);
        }else if ($mode == 'doc') {
            $config['overwrite'] = FALSE;
            $config['allowed_types'] = 'pdf|doc|docx|txt|csv|xls|text|jpg|jpeg|gif|png|psd|bmp';
            $config['upload_path'] = './pdf-doc/pdf/'; 
            $this->load->library('upload', $config);
            $pdfImage = array();
            if ($this->upload->do_upload('pdfupload')) {
                $fileDetails = $this->upload->data();
                $filename = $fileDetails['file_name'];
                $pdfImage = array('product_doc' => $filename);
            } else {
                print_r($this->upload->display_errors());
            }
            $this->product_model->update_details(PRODUCT, $pdfImage, array('seller_product_id' => $pid));
            if ($this->lang->line('change_saved') != '')
                $lg_err_msg = $this->lang->line('change_saved');
            else
                $lg_err_msg = 'Yeah ! changes have been saved';
            $this->setErrorMessage('success', $lg_err_msg);
            redirect('things/' . $pid . '/edit/doc');
        }else if ($mode == 'categories') {

            $category_id                  = $this->input->post('category_id');
            
            if ($this->input->post('category_id') != ''){
                $sub_category             = $this->input->post('subcategories');
                
                    if( isset($sub_category) && count($sub_category) > 0 && $sub_category!=''){
                        $subcats          = array_filter($sub_category);
                        $sub_categories   = array_unique($subcats);
                        $sub_categor_list = implode(',',$sub_categories);
                        $category_id     .= ','.$sub_categor_list;
                    }
            }else {
                $category_id = '';
            }
            
            $current_category_list          = explode(',',$category_id);
            $current_Product_category_count = count($current_category_list);
            
            $existing_products = $this->product_model->get_all_details(PRODUCT,array('user_id'=>$this->checkLogin('U')));
            foreach($existing_products->result() as $existed_products){
                if( $existed_products->category_id !='' ){
                    
                    $existed_category_list = explode(',',$existed_products->category_id);
                    
                    $existed_Product_category_count = count($existed_category_list);
                        
                        if( $existed_Product_category_count == $current_Product_category_count ){
                              
                            $current_category_last_element  =  end(array_values($current_category_list));
                            $existed_category_last_element  =  end(array_values($existed_category_list));
                            
                            if( $existed_category_last_element == $current_category_last_element ){
                                
                                    /* update all sub categories except last element */
                                    $current_categories = $current_category_list;
                                    array_pop($current_categories);
                                    $categories = $current_categories;
                                    $categories =implode(',',$categories);
                                    if($current_Product_category_count > 1){
                                        $categories = $categories.',';
                                    }
                                        $dataArr = array( 'category_id' => $categories);
                                        $this->product_model->update_details(PRODUCT,$dataArr,array('seller_product_id'=>$pid));
                                
                                $product_detail = $this->product_model->get_all_details(CATEGORY,array('id'=>$existed_category_last_element,'status'=>'Active'));
                                $category_name  = $product_detail->row()->seo;
                                if( !empty($product_detail) && $category_name !='')
                                    $category_name  = $category_name;
                                else
                                    $category_name  = 'Category';
                                $lg_err_msg =  $category_name.' already existed. Try other categories';
                                $this->setErrorMessage('error',$lg_err_msg);
                                redirect('things/'.$pid.'/edit/'.$nextMode);
                            }
                        }
                }
            }
            $cat_id = end(array_values($current_category_list));
            $product_detail = $this->product_model->get_all_details(CATEGORY,array('id'=>$cat_id,'status'=>'Active'));
            $sku_category  = $product_detail->row()->seourl;
            $dataArr = array( 'category_id' => $category_id,'sku_category'=>$sku_category );
            $this->product_model->update_details(PRODUCT,$dataArr,array('seller_product_id'=>$pid));
            
            if ($this->lang->line('change_saved') != '')
                $lg_err_msg = $this->lang->line('change_saved');
            else
                $lg_err_msg = 'Yeah ! changes have been saved';
            $this->setErrorMessage('success', $lg_err_msg);
            redirect('things/' . $pid . '/edit/' . $nextMode);
       
        }else if ($mode == 'list') {
            $list_name_str = $list_val_str = '';
            $list_name_arr = $this->input->post('attribute_name');
            $list_val_arr = $this->input->post('attribute_val');
            if (is_array($list_name_arr) && count($list_name_arr) > 0) {
                $list_name_str = implode(',', $list_name_arr);
                $list_val_str = implode(',', $list_val_arr);
            }
            $dataArr = array('list_name' => $list_name_str, 'list_value' => $list_val_str);
            $this->product_model->update_details(PRODUCT, $dataArr, array('seller_product_id' => $pid));

            //Update the list table
            if (is_array($list_val_arr)) {
                foreach ($list_val_arr as $list_val_row) {
                    $list_val_details = $this->product_model->get_all_details(LIST_VALUES, array('id' => $list_val_row));
                    if ($list_val_details->num_rows() == 1) {
                        $product_count = $list_val_details->row()->product_count;
                        $products_in_this_list = $list_val_details->row()->products;
                        $products_in_this_list_arr = explode(',', $products_in_this_list);
                        if (!in_array($pid, $products_in_this_list_arr)) {
                            array_push($products_in_this_list_arr, $pid);
                            $product_count++;
                            $list_update_values = array(
                                'products' => implode(',', $products_in_this_list_arr),
                                'product_count' => $product_count
                            );
                            $list_update_condition = array('id' => $list_val_row);
                            $this->product_model->update_details(LIST_VALUES, $list_update_values, $list_update_condition);
                        }
                    }
                }
            }
            if ($this->lang->line('change_saved') != '')
                $lg_err_msg = $this->lang->line('change_saved');
            else
                $lg_err_msg = 'Yeah ! changes have been saved';
            $this->setErrorMessage('success', $lg_err_msg);
            redirect('things/' . $pid . '/edit/' . $nextMode);
        }else if ($mode == 'attribute') {
            $dataArr = array('seller_product_id' => $pid);
            $checkProduct = $this->product_model->get_all_details(PRODUCT, $dataArr);
            if ($checkProduct->num_rows() == 1) {
				
                $prodId = $checkProduct->row()->id;
                $Attr_name_str = $Attr_val_str = '';
                $Attr_type_arr = $this->input->post('product_attribute_type');
                $Attr_name_arr = $this->input->post('product_attribute_name');
                $Attr_val_arr = $this->input->post('product_attribute_val');
				$Attr_qty_arr = $this->input->post('product_attribute_qty');
				
                if (is_array($Attr_type_arr) && count($Attr_type_arr) > 0) {
                    for ($k = 0; $k < sizeof($Attr_type_arr); $k++) {
                        $dataSubArr = '';
                        $dataSubArr = array('product_id' => $prodId, 'attr_id' => $Attr_type_arr[$k], 'attr_name' => $Attr_name_arr[$k], 'attr_price' => $Attr_val_arr[$k],'attr_qty'=>$Attr_qty_arr[$k]);
                        $this->product_model->add_subproduct_insert($dataSubArr);
                    }
					$existQty = $this->db->select_sum('attr_qty','Qty')->from(SUBPRODUCT)->where('product_id',$prodId)->get();
					if($this->input->post('attribute_must') =='yes'){
						$newQty = $existQty->row()->Qty;
						$this->product_model->update_details(PRODUCT,array('quantity'=>$newQty,'attribute_must'=>'yes'),array('id'=>$prodId));
					echo $this->db->last_query();
					}else{
						$_product = $this->product_model->get_all_details(PRODUCT,array('id'=>$prodId));
						$sumQty = $existQty->row()->Qty;
						if($sumQty > $_product->row()->quantity){
							$newQty = $sumQty;
							$this->product_model->update_details(PRODUCT,array('quantity'=>$newQty),array('id'=>$prodId));
						}
					}
                }
				$attribute_must = ''; 
				if($this->input->post('attribute_must') =='yes'){
					$attribute_must = 'yes'; 
				}
				$this->product_model->update_details(PRODUCT,array('attribute_must'=>$attribute_must),array('id'=>$prodId));
                if ($this->lang->line('change_saved') != ''){
                    $lg_err_msg = $this->lang->line('change_saved');
                }else{
                    $lg_err_msg = 'Yeah ! changes have been saved';
                }
                $this->setErrorMessage('success', $lg_err_msg);
            }else {
                if ($this->lang->line('prod_not_found_db') != '')
                    $lg_err_msg = $this->lang->line('prod_not_found_db');
                else
                    $lg_err_msg = 'Product not found in database';
                $this->setErrorMessage('error', $lg_err_msg);
            }
            redirect('things/' . $pid . '/edit/' . $nextMode);
        }else {
            show_404();
        }
    }

    public function delete_product() {
        $pid = $this->uri->segment(2, 0);
        if ($this->checkLogin('U') == '') {
            redirect('login');
        } else {
            $productDetails = $this->product_model->get_all_details(USER_PRODUCTS, array('seller_product_id' => $pid));
            if ($productDetails->num_rows() == 1) {
                if ($productDetails->row()->user_id == $this->checkLogin('U')) {
                    $this->product_model->commonDelete(USER_PRODUCTS, array('seller_product_id' => $pid));

                    $this->update_counts_after_deletion($productDetails);

                    if ($this->lang->line('prod_del_succ') != '')
                        $lg_err_msg = $this->lang->line('prod_del_succ');
                    else
                        $lg_err_msg = 'Product deleted successfully';
                    $this->setErrorMessage('success', $lg_err_msg);
                    redirect('user/' . $this->data['userDetails']->row()->user_name . '/added');
                }else {
                    $this->setErrorMessage('error', 'user product check');
                    redirect(base_url());
                }
            } else {
                $productDetails = $this->product_model->get_all_details(PRODUCT, array('seller_product_id' => $pid));
                if ($productDetails->num_rows() == 1) {
                    if ($productDetails->row()->user_id == $this->checkLogin('U')) {
                        $this->product_model->commonDelete(PRODUCT, array('seller_product_id' => $pid));
                        $this->update_counts_after_deletion($productDetails);
						//Delete from Sub Shipping

						$this->product_model->commonDelete(SUB_SHIPPING, array('product_id' => $productDetails->row()->id));

                        //Delete from cart
                        $this->product_model->commonDelete(SHOPPING_CART, array('product_id' => $productDetails->row()->id));

                        if ($this->lang->line('prod_del_succ') != '')
                            $lg_err_msg = $this->lang->line('prod_del_succ');
                        else
                            $lg_err_msg = 'Product deleted successfully';
                        $this->setErrorMessage('success', $lg_err_msg);
                        redirect('user/' . $this->data['userDetails']->row()->user_name . '/added');
                    }else {
                        $this->setErrorMessage('error', 'user id check');
                        redirect(base_url());
                        //show_404();
                    }
                } else {
                    $this->setErrorMessage('error', 'product check');
                    redirect(base_url());
                    //show_404();
                }
            }
        }
    }

    public function update_counts_after_deletion($productDetails) {

        //User added count
        $productCount = $this->data['userDetails']->row()->products;
        if($productCount > 0){
            $productCount--;
        }
        $this->product_model->update_details(USERS, array('products' => $productCount), array('id' => $this->checkLogin('U')));
        //--------------------
        //User likes count
        $liked_users = $this->product_model->get_all_details(PRODUCT_LIKES, array('product_id' => $productDetails->row()->seller_product_id));
        if ($liked_users->num_rows() > 0) {
            foreach ($liked_users->result() as $liked_users_row) {
                $liked_user = $this->product_model->get_all_details(USERS, array('id' => $liked_users_row->user_id));
                if ($liked_user->num_rows() > 0) {
                    $likes_count = $liked_user->row()->likes;
                    $likes_count--;
                    if ($likes_count < 0) {
                        $likes_count = 0;
                    }
                    $this->product_model->update_details(USERS, array('likes' => $likes_count), array('id' => $liked_user->row()->id));
                }
            }
            $this->product_model->commonDelete(PRODUCT_LIKES, array('product_id' => $productDetails->row()->seller_product_id));
        }
        //--------------------
        //Wants count
        $wanted_users = $this->product_model->get_all_details(WANTS_DETAILS, array('product_id' => $productDetails->row()->seller_product_id));
        if ($wanted_users->num_rows() > 0) {
            foreach ($wanted_users->result() as $wu_row) {
                $wanted_user = $this->product_model->get_all_details(USERS, array('id' => $wu_row->user_id));
                if ($wanted_user->num_rows() > 0) {
                    $wanted_count = $wanted_user->row()->want_count;
                    if ($wanted_count > 0) {
                        $wanted_count--;
                    }
                    $this->product_model->update_details(USERS, array('want_count' => $wanted_count), array('id' => $wanted_user->row()->id));
                }
            }
            $this->product_model->commonDelete(WANTS_DETAILS, array('product_id' => $productDetails->row()->seller_product_id));
        }
        //--------------------
        //Owns count
        $owned_user_qry = "select * from " . USERS . " where FIND_IN_SET('" . $productDetails->row()->seller_product_id . "',own_products)";
        $owned_user = $this->product_model->ExecuteQuery($owned_user_qry);
        if ($owned_user->num_rows() > 0) {
            foreach ($owned_user->result() as $owned_user_row) {
                $own_products = array_unique(array_filter(explode(',', $owned_user_row->own_products)));
                if (($key = array_search($productDetails->row()->seller_product_id, $own_products)) !== false) {
                    unset($own_products[$key]);
                    $own_count = count($own_products);
                    $this->product_model->update_details(USERS, array('own_products' => implode(',', $own_products), 'own_count' => $own_count), array('id' => $owned_user_row->id));
                }
            }
        }
        //--------------------
        //Notifications table
        $this->product_model->commonDelete(NOTIFICATIONS, array('activity_id' => $productDetails->row()->seller_product_id));
        $this->product_model->commonDelete(USER_ACTIVITY, array('activity_id' => $productDetails->row()->seller_product_id));
        //--------------------
        //Comments table
        $this->product_model->commonDelete(PRODUCT_COMMENTS, array('product_id' => $productDetails->row()->seller_product_id));
        //--------------------
        //Update lists count
        $list_qry = "select * from " . LISTS_DETAILS . " where FIND_IN_SET('" . $productDetails->row()->seller_product_id . "',product_id)";
        $lists_details = $this->product_model->ExecuteQuery($list_qry);
        if ($lists_details->num_rows() > 0) {
            foreach ($lists_details->result() as $list_row) {
                $list_products = array_unique(array_filter(explode(',', $list_row->product_id)));
                if (($key = array_search($productDetails->row()->seller_product_id, $list_products)) !== false) {
                    unset($list_products[$key]);
                    $prod_count = count($list_products);
                    $this->product_model->update_details(LISTS_DETAILS, array('product_id' => implode(',', $list_products), 'product_count' => $prod_count), array('id' => $list_row->id));
                }
            }
        }
        //--------------------
    }

    public function add_reaction_tag() {
        $returnStr['status_code'] = 0;
        if ($this->checkLogin('U') == '') {
            if ($this->lang->line('u_must_login') != '')
                $returnStr['message'] = $this->lang->line('u_must_login');
            else
                $returnStr['message'] = 'You must login';
        }else {
            $tid = $this->input->post('thing_id');
            $checkProductLike = $this->user_model->get_all_details(PRODUCT_LIKES, array('product_id' => $tid, 'user_id' => $this->checkLogin('U')));
            if ($checkProductLike->num_rows() == 0) {
                $productDetails = $this->user_model->get_all_details(PRODUCT, array('seller_product_id' => $tid));
                if ($productDetails->num_rows() == 0) {
                    $productDetails = $this->user_model->get_all_details(USER_PRODUCTS, array('seller_product_id' => $tid));
                    $productTable = USER_PRODUCTS;
                } else {
                    $productTable = PRODUCT;
                }
                if ($productDetails->num_rows() == 1) {
                    $likes = $productDetails->row()->likes;
                    $dataArr = array('product_id' => $tid, 'user_id' => $this->checkLogin('U'), 'ip' => $this->input->ip_address());
                    $this->user_model->simple_insert(PRODUCT_LIKES, $dataArr);
                    $actArr = array(
                        'activity_name' => 'fancy',
                        'activity_id' => $tid,
                        'user_id' => $this->checkLogin('U'),
                        'activity_ip' => $this->input->ip_address()
                    );
                    $this->user_model->simple_insert(USER_ACTIVITY, $actArr);
                    $likes++;
                    $dataArr = array('likes' => $likes);
                    $condition = array('seller_product_id' => $tid);
                    $this->user_model->update_details($productTable, $dataArr, $condition);
                    $totalUserLikes = $this->data['userDetails']->row()->likes;
                    $totalUserLikes++;
                    $this->user_model->update_details(USERS, array('likes' => $totalUserLikes), array('id' => $this->checkLogin('U')));
                    /*
                     * -------------------------------------------------------
                     * Creating list automatically when user likes a product
                     * -------------------------------------------------------
                     *
                      $listCheck = $this->user_model->get_list_details($tid,$this->checkLogin('U'));
                      if ($listCheck->num_rows() == 0){
                      $productCategoriesArr = explode(',', $productDetails->row()->category_id);
                      if (count($productCategoriesArr)>0){
                      foreach ($productCategoriesArr as $productCategoriesRow){
                      if ($productCategoriesRow != ''){
                      $productCategory = $this->user_model->get_all_details(CATEGORY,array('id'=>$productCategoriesRow));
                      if ($productCategory->num_rows()==1){

                      }
                      }
                      }
                      }
                      }
                     */
                    $returnStr['status_code'] = 1;
                } else {
                    if ($this->lang->line('prod_not_avail') != '')
                        $returnStr['message'] = $this->lang->line('prod_not_avail');
                    else
                        $returnStr['message'] = 'Product not available';
                }
            }else {
                $returnStr['status_code'] = 1;
            }
        }
        echo json_encode($returnStr);
    }

    public function delete_reaction_tag() {
        $returnStr['status_code'] = 0;
        if ($this->checkLogin('U') == '') {
            if ($this->lang->line('u_must_login') != '')
                $returnStr['message'] = $this->lang->line('u_must_login');
            else
                $returnStr['message'] = 'You must login';
        }else {
            $tid = $this->input->post('thing_id');
            $checkProductLike = $this->user_model->get_all_details(PRODUCT_LIKES, array('product_id' => $tid, 'user_id' => $this->checkLogin('U')));
            if ($checkProductLike->num_rows() == 1) {
                $productDetails = $this->user_model->get_all_details(PRODUCT, array('seller_product_id' => $tid));
                if ($productDetails->num_rows() == 0) {
                    $productDetails = $this->user_model->get_all_details(USER_PRODUCTS, array('seller_product_id' => $tid));
                    $productTable = USER_PRODUCTS;
                } else {
                    $productTable = PRODUCT;
                }
                if ($productDetails->num_rows() == 1) {
                    $likes = $productDetails->row()->likes;
                    $conditionArr = array('product_id' => $tid, 'user_id' => $this->checkLogin('U'));
                    $this->user_model->commonDelete(PRODUCT_LIKES, $conditionArr);
                    $actArr = array(
                        'activity_name' => 'unfancy',
                        'activity_id' => $tid,
                        'user_id' => $this->checkLogin('U'),
                        'activity_ip' => $this->input->ip_address()
                    );
                    $this->user_model->simple_insert(USER_ACTIVITY, $actArr);
                    $likes--;
                    $dataArr = array('likes' => $likes);
                    $condition = array('seller_product_id' => $tid);
                    $this->user_model->update_details($productTable, $dataArr, $condition);
                    $totalUserLikes = $this->data['userDetails']->row()->likes;
                    $totalUserLikes--;
                    $this->user_model->update_details(USERS, array('likes' => $totalUserLikes), array('id' => $this->checkLogin('U')));
                    $returnStr['status_code'] = 1;
                } else {
                    if ($this->lang->line('prod_not_avail') != '')
                        $returnStr['message'] = $this->lang->line('prod_not_avail');
                    else
                        $returnStr['message'] = 'Product not available';
                }
            }else {
                $returnStr['status_code'] = 1;
            }
        }
        echo json_encode($returnStr);
    }

    public function loadListValues() {
        $returnStr['listCnt'] = '<option value="">--Select--</option>';
        $lid = $this->input->post('lid');
        $lvID = $this->input->post('lvID');
        if ($lid != '') {
            $listValues = $this->product_model->get_all_details(LIST_VALUES, array('list_id' => $lid));
            if ($listValues->num_rows() > 0) {
                foreach ($listValues->result() as $listRow) {
                    $selStr = '';
                    if ($listRow->id == $lvID) {
                        $selStr = 'selected="selected"';
                    }
                    $returnStr['listCnt'] .= '<option ' . $selStr . ' value="' . $listRow->id . '">' . $listRow->list_value . '</option>';
                }
            }
        }
        echo json_encode($returnStr);
    }

    public function approve_comment() {
        $returnStr['status_code'] = 0;
        if ($this->checkLogin('U') != '') {
            $cid = $this->input->post('cid');
            $this->product_model->update_details(PRODUCT_COMMENTS, array('status' => 'Active'), array('id' => $cid));
            $datestring = "%Y-%m-%d %h:%i:%s";
            $time = time();
            $createdTime = mdate($datestring, $time);
            $product_id = $this->input->post('tid');
            $user_id = $this->input->post('uid');
            $this->product_model->commonDelete(NOTIFICATIONS, array('comment_id' => $cid));
            $actArr = array(
                'activity' => 'comment',
                'activity_id' => $product_id,
                'user_id' => $user_id,
                'activity_ip' => $this->input->ip_address(),
                'comment_id' => $cid,
                'created' => $createdTime
            );
            $this->product_model->simple_insert(NOTIFICATIONS, $actArr);
            $this->send_comment_noty_mail($product_id, $cid);
            $returnStr['status_code'] = 1;
        }
        echo json_encode($returnStr);
    }

    public function delete_comment() {
        $returnStr['status_code'] = 0;
        if ($this->checkLogin('U') != '') {
            $cid = $this->input->post('cid');
            $this->product_model->commonDelete(PRODUCT_COMMENTS, array('id' => $cid));
            $returnStr['status_code'] = 1;
        }
        echo json_encode($returnStr);
    }

    public function send_feature_noty_mail($pid = '0') {
        if ($pid != '0') {
            $productUserDetails = $this->product_model->get_product_full_details($pid);
            if ($productUserDetails->num_rows() > 0) {
                $emailNoty = explode(',', $productUserDetails->row()->email_notifications);
                if (in_array('featured', $emailNoty)) {
                    if ($productUserDetails->prodmode == 'seller') {
                        $prodLink = base_url() . 'things/' . $productUserDetails->row()->id . '/' . url_title($productUserDetails->row()->product_name, '-');
                    } else {
                        $prodLink = base_url() . 'user/' . $productUserDetails->row()->user_name . '/things/' . $productUserDetails->row()->seller_product_id . '/' . url_title($productUserDetails->row()->product_name, '-');
                    }

                    $newsid = '10';
                    $template_values = $this->product_model->get_newsletter_template_details($newsid);
                    $adminnewstemplateArr = array('logo' => $this->data['logo'], 'meta_title' => $this->config->item('meta_title'), 'full_name' => $productUserDetails->row()->full_name, 'cfull_name' => $this->data['userDetails']->row()->full_name, 'product_name' => $productUserDetails->row()->product_name, 'user_name' => $this->data['userDetails']->row()->user_name);
                    extract($adminnewstemplateArr);
                    $subject = 'From: ' . $this->config->item('email_title') . ' - ' . $template_values['news_subject'];
                    $message .= '<!DOCTYPE HTML>
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
			<meta name="viewport" content="width=device-width"/>
			<title>' . $template_values['news_subject'] . '</title><body>';
                    include('./newsletter/registeration' . $newsid . '.php');

                    $message .= '</body>
			</html>';

                    if ($template_values['sender_name'] == '' && $template_values['sender_email'] == '') {
                        $sender_email = $this->data['siteContactMail'];
                        $sender_name = $this->data['siteTitle'];
                    } else {
                        $sender_name = $template_values['sender_name'];
                        $sender_email = $template_values['sender_email'];
                    }

                    $email_values = array('mail_type' => 'html',
                        'from_mail_id' => $sender_email,
                        'mail_name' => $sender_name,
                        'to_mail_id' => $productUserDetails->row()->email,
                        'subject_message' => $subject,
                        'body_messages' => $message
                    );
                    $email_send_to_common = $this->product_model->common_email_send($email_values);
                }
            }
        }
    }

    public function contactform() {
      
        $dataArrVal = array();
        foreach ($this->input->post() as $key => $val) {
            $dataArrVal[$key] = trim(addslashes($val));
        }
        $this->product_model->simple_insert(CONTACTSELLER, $dataArrVal);
        //$contact_id = $this->product_model->get_last_insert_id();
        $this->data['productVal'] = $this->product_model->get_all_details(PRODUCT, array('id' => $this->input->post('product_id')));
        $newimages = array_filter(@explode(',', $this->data['productVal']->row()->image));
        $newsid = '20';
        $template_values = $this->product_model->get_newsletter_template_details($newsid);

        $subject = 'From: ' . $this->config->item('email_title') . ' - ' . $template_values['news_subject'];
        $adminnewstemplateArr = array('email_title' => $this->config->item('email_title'), 'logo' => $this->data['logo'],
            'name' => $this->input->post('name'),
            'question' => $this->input->post('question'),
            'email' => $this->input->post('email'),
            'phone' => $this->input->post('phone'),
            'productId' => $this->data['productVal']->row()->id,
            'productName' => $this->data['productVal']->row()->product_name,
            'productSeourl' => $this->data['productVal']->row()->seourl,
            'productImage' => base_url().'images/product/'.$newimages[0],
        );
        extract($adminnewstemplateArr);
        //$ddd =htmlentities($template_values['news_descrip'],null,'UTF-8');
        $header .="Content-Type: text/plain; charset=ISO-8859-1\r\n";

        $message .= '<!DOCTYPE HTML>
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
			<meta name="viewport" content="width=device-width"/><body>';
        include('./newsletter/registeration' . $newsid . '.php');
        $message .= '</body>
			</html>';
        if ($template_values['sender_name'] == '' && $template_values['sender_email'] == '') {
            $sender_email = $this->data['siteContactMail'];
            $sender_name = $this->data['siteTitle'];
        } else {
            $sender_name = $template_values['sender_name'];
            $sender_email = $template_values['sender_email'];
        }
        $email_values = array('mail_type' => 'html',
            'from_mail_id' => $sender_email,
            'mail_name' => $sender_name,
            'to_mail_id' => $this->input->post('selleremail'),
            'cc_mail_id' => $this->config->item('site_contact_mail'),
            'subject_message' => $template_values['news_subject'],
            'body_messages' => $message
        );

        $email_send_to_common = $this->product_model->common_email_send($email_values);
       
        $this->setErrorMessage('success', 'Message Sent Successfully!');
        echo 'Success';
    }
    public function display_brands(){
        $this->data['heading']  = 'Brand Lists';
        $this->load->view('site/shop/display_brands',$this->data);
    }
    public function send_comment_noty_mail($pid = '0', $cid = '0') {
        if ($pid != '0' && $cid != '0') {
            $likeUserList = $this->product_model->get_like_user_full_details($pid);
            if ($likeUserList->num_rows() > 0) {
                $productUserDetails = $this->product_model->get_product_full_details($pid);
                $commentDetails = $this->product_model->view_product_comments_details('where c.id=' . $cid);
                if ($productUserDetails->num_rows() > 0 && $commentDetails->num_rows() == 1) {
                    foreach ($likeUserList->result() as $likeUserListRow) {
                        $emailNoty = explode(',', $likeUserListRow->email_notifications);
                        if (in_array('comments_on_fancyd', $emailNoty)) {
                            if ($productUserDetails->prodmode == 'seller') {
                                $prodLink = base_url() . 'things/' . $productUserDetails->row()->id . '/' . url_title($productUserDetails->row()->product_name, '-');
                            } else {
                                $prodLink = base_url() . 'user/' . $productUserDetails->row()->user_name . '/things/' . $productUserDetails->row()->seller_product_id . '/' . url_title($productUserDetails->row()->product_name, '-');
                            }

                            $newsid = '8';
                            $template_values = $this->product_model->get_newsletter_template_details($newsid);
                            $adminnewstemplateArr = array('logo' => $this->data['logo'], 'meta_title' => $this->config->item('meta_title'), 'full_name' => $likeUserListRow->full_name, 'cfull_name' => $commentDetails->row()->full_name, 'user_name' => $commentDetails->row()->user_name, 'product_name' => $productUserDetails->row()->product_name);
                            extract($adminnewstemplateArr);
                            $subject = 'From: ' . $this->config->item('email_title') . ' - ' . $template_values['news_subject'];
                            $message .= '<!DOCTYPE HTML>
                                <html>
                                <head>
                                <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                                <meta name="viewport" content="width=device-width"/>
                                <title>' . $template_values['news_subject'] . '</title><body>';
                            include('./newsletter/registeration' . $newsid . '.php');

                            $message .= '</body>
                                </html>';

                            if ($template_values['sender_name'] == '' && $template_values['sender_email'] == '') {
                                $sender_email = $this->data['siteContactMail'];
                                $sender_name = $this->data['siteTitle'];
                            } else {
                                $sender_name = $template_values['sender_name'];
                                $sender_email = $template_values['sender_email'];
                            }

                            $email_values = array('mail_type' => 'html',
                                'from_mail_id' => $sender_email,
                                'mail_name' => $sender_name,
                                'to_mail_id' => $likeUserListRow->email,
                                'subject_message' => $subject,
                                'body_messages' => $message
                            );

                            $email_send_to_common = $this->product_model->common_email_send($email_values);
                        }
                    }
                }
            }
        }
    }

    public function add_product_via_email() {
        $returnStr['status_code'] = 0;
        $returnStr['message'] = '';
        if ($this->checkLogin('U') != '') {

            /*             * *---Update in db---** */
            $userDetails = $this->data['userDetails'];
            $dataArr = array(
                'user_id' => $this->checkLogin('U'),
                'user_name' => $userDetails->row()->user_name,
                'title' => $this->input->post('title'),
                'comment' => $this->input->post('comment')
            );
            $this->product_model->simple_insert(UPLOAD_MAILS, $dataArr);
            /*             * *---Update in db---** */

            /*             * *---Send Mail---** */
            $newsid = '18';
            $template_values = $this->product_model->get_newsletter_template_details($newsid);
            $full_name = $userDetails->row()->full_name;
            if ($full_name == '') {
                $full_name = $userDetails->row()->user_name;
            }
            $thumbnail = '';
            if ($userDetails->row()->thumbnail != '') {
                $thumbnail = '<img width="100px" src="' . base_url() . 'images/users/' . $userDetails->row()->thumbnail . '"/>';
            }
            $adminnewstemplateArr = array(
                'logo' => $this->data['logo'],
                'meta_title' => $this->config->item('meta_title'),
                'user_name' => $userDetails->row()->user_name,
                'title' => $this->input->post('title'),
                'comment' => $this->input->post('comment')
            );
            extract($adminnewstemplateArr);
            $subject = $template_values['news_subject'];
            $message .= '<!DOCTYPE HTML>
                                <html>
                                <head>
                                <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                                <meta name="viewport" content="width=device-width"/>
                                <title>' . $template_values['news_subject'] . '</title><body>';
            include('./newsletter/registeration' . $newsid . '.php');

            $message .= '</body>
                                </html>';

            if ($template_values['sender_name'] == '' && $template_values['sender_email'] == '') {
                $sender_email = $this->data['siteContactMail'];
                $sender_name = $this->data['siteTitle'];
            } else {
                $sender_name = $template_values['sender_name'];
                $sender_email = $template_values['sender_email'];
            }

            $email_values = array('mail_type' => 'html',
                'from_mail_id' => $sender_email,
                'mail_name' => $sender_name,
                'to_mail_id' => $this->data['siteContactMail'],
                'subject_message' => $subject,
                'body_messages' => $message
            );
            $email_send_to_common = $this->product_model->common_email_send($email_values);
            /*             * *---Send Mail---** */

            $returnStr['status_code'] = 1;
            if ($this->lang->line('ur_msg_sent') != '')
                $returnStr['message'] = $this->lang->line('ur_msg_sent');
            else
                $returnStr['message'] = 'Your message sent';
        }else {
            if ($this->lang->line('login_requ') != '')
                $returnStr['message'] = $this->lang->line('login_requ');
            else
                $returnStr['message'] = 'Login required';
        }
        echo json_encode($returnStr);
    }

    public function ajaxProductAttributeUpdate(){

        $conditons = array('pid' => $this->input->post('pid'));
        $dataArr = array('attr_id' => $this->input->post('attId'), 'attr_name' => $this->input->post('attname'), 'attr_price' => $this->input->post('attprice'),'attr_qty' => $this->input->post('attqty'));

		$subproductDetails = $this->product_model->edit_subproduct_update($dataArr, $conditons);
		/******Attribute Quantity Update******/

		$existQty = $this->db->select_sum('attr_qty','Qty')->from(SUBPRODUCT)->where('product_id', $this->input->post('prodId'))->get();

		$_product = $this->product_model->get_all_details(PRODUCT, array('id'=>$this->input->post('prodId')));

		if($_product->row()->attribute_must == 'yes'){
			$newQty = $existQty->row()->Qty;
			$this->product_model->update_details(PRODUCT,array('quantity'=>$newQty),array('id'=>$this->input->post('prodId')));
		}else{
			$sumQty = $existQty->row()->Qty;
			if($sumQty > $_product->row()->quantity){
				$newQty = $sumQty;
				$this->product_model->update_details(PRODUCT,array('quantity'=>$newQty),array('id'=>$this->input->post('prodId')));
			}
		}
    }

    public function remove_attr(){
        if($this->checkLogin('U') != '') {
            $this->product_model->commonDelete(SUBPRODUCT, array('pid' => $this->input->post('pid')));
			/******Attribute Quantity Update******/
			$existQty = $this->db->select_sum('attr_qty','Qty')->from(SUBPRODUCT)->where('product_id',$this->input->post('prodId'))->get();
			$_product = $this->product_model->get_all_details(PRODUCT, array('id'=>$this->input->post('prodId')));
			if($_product->row()->attribute_must == 'yes'){
				$newQty = $existQty->row()->Qty;
				$this->product_model->update_details(PRODUCT,array('quantity'=>$newQty),array('id'=>$this->input->post('prodId')));
			}else{
				$sumQty = $existQty->row()->Qty;
				if($sumQty < $_product->row()->quantity){
					$newQty = $sumQty;
					$this->product_model->update_details(PRODUCT,array('quantity'=>$newQty),array('id'=>$this->input->post('prodId')));
				}
			}
        }
    }

    /**
     *
     * Update unique id for products
     */
    /* 	public function qq(){
      $productDetails = $this->product_model->get_all_details(PRODUCT,array());
      foreach ($productDetails->result() as $row){
      $pid = mktime();
      $checkId = $this->product_model->check_product_id($pid);
      while ($checkId->num_rows()>0){
      $pid = mktime();
      $checkId = $this->product_model->check_product_id($pid);
      }
      $this->product_model->update_details(PRODUCT,array('seller_product_id'=>$pid),array('id'=>$row->id));
      echo $row->id.' , ';
      }
      echo 'rows updated';
      }
     */

    public function update_owns() {
        echo 'Updating Own Products<br/><hr/><br/>';
        $user_list = $this->product_model->get_all_details(USERS, array());
        if ($user_list->num_rows() > 0) {
            foreach ($user_list->result() as $user_details) {
                $own_count = 0;
                $own_products = array_filter(explode(',', $user_details->own_products));
                if (count($own_products) > 0) {
                    $id_str = '';
                    foreach ($own_products as $id_row) {
                        $id_str .= $id_row . ',';
                    }
                    $id_str = substr($id_str, 0, -1);
                    $Query = "select `id` from " . PRODUCT . " where `seller_product_id` in ('" . $id_str . "')";
                    $products = $this->product_model->ExecuteQuery($Query);
                    $own_count += $products->num_rows();
                    $Query = "select `id` from " . USER_PRODUCTS . " where `seller_product_id` in ('" . $id_str . "')";
                    $products = $this->product_model->ExecuteQuery($Query);
                    $own_count += $products->num_rows();
                }
                $this->product_model->update_details(USERS, array('own_count' => $own_count), array('id' => $user_details->id));
                echo $user_details->id . '-*--' . $user_details->user_name . '--*-' . $own_count . '<br/>';
            }
        }
        echo 'Complete.!';
    }

    public function qq() {
        $productDetails = $this->product_model->get_all_details(PRODUCT, array());
        foreach ($productDetails->result() as $row) {
            $pid = $this->get_rand_str('6');
            $checkId = $this->product_model->get_all_details(SHORTURL, array('short_url' => $pid));
            while ($checkId->num_rows() > 0) {
                $pid = $this->get_rand_str('6');
                $checkId = $this->product_model->get_all_details(SHORTURL, array('short_url' => $pid));
            }
            $url = base_url() . 'things/' . $row->id . '/' . url_title($row->product_name, '-');
            $this->product_model->simple_insert(SHORTURL, array('short_url' => $pid, 'long_url' => $url));
            $urlid = $this->product_model->get_last_insert_id();
            $this->product_model->update_details(PRODUCT, array('short_url_id' => $urlid), array('seller_product_id' => $row->seller_product_id));
        }
        echo 'Short urls for selling products added<br/>';
        $productDetails = $this->product_model->view_notsell_product_details('');
        foreach ($productDetails->result() as $row) {
            $pid = $this->get_rand_str('6');
            $checkId = $this->product_model->get_all_details(SHORTURL, array('short_url' => $pid));
            while ($checkId->num_rows() > 0) {
                $pid = $this->get_rand_str('6');
                $checkId = $this->product_model->get_all_details(SHORTURL, array('short_url' => $pid));
            }
            $url = base_url() . 'user/' . $row->user_name . '/things/' . $row->seller_product_id . '/' . url_title($row->product_name, '-');
            $this->product_model->simple_insert(SHORTURL, array('short_url' => $pid, 'long_url' => $url));
            $urlid = $this->product_model->get_last_insert_id();
            $this->product_model->update_details(USER_PRODUCTS, array('short_url_id' => $urlid), array('seller_product_id' => $row->seller_product_id));
        }
        echo 'Short urls for affiliate products added';
    }

    public function qq_update_counts() {
        $qryCount = 0;
        $user_list = $this->product_model->get_all_details(USERS, array());
        $qryCount++;
        if ($user_list->num_rows() > 0) {
            foreach ($user_list->result() as $user_list_row) {

                //Added count
                $sell_products = $this->product_model->get_all_details(PRODUCT, array('user_id' => $user_list_row->id));
                $qryCount++;
                $affil_products = $this->product_model->get_all_details(USER_PRODUCTS, array('user_id' => $user_list_row->id));
                $qryCount++;
                $total_products = $sell_products->num_rows() + $affil_products->num_rows();
                if ($total_products != $user_list_row->products) {
                    $this->product_model->update_details(USERS, array('products' => $total_products), array('id' => $user_list_row->id));
                    $qryCount++;
                }
                //------------------
                //Like count
                $liked_products = $this->product_model->get_all_details(PRODUCT_LIKES, array('user_id' => $user_list_row->id));
                $qryCount++;
                if ($liked_products->num_rows() > 0) {
                    $pl_count = 0;
                    foreach ($liked_products->result() as $pl_row) {
                        $pl_chk = $this->product_model->get_all_details(PRODUCT, array('seller_product_id' => $pl_row->product_id));
                        $qryCount++;
                        if ($pl_chk->num_rows() == 0) {
                            $pl_chk = $this->product_model->get_all_details(USER_PRODUCTS, array('seller_product_id' => $pl_row->product_id));
                            $qryCount++;
                        }
                        if ($pl_chk->num_rows() > 0) {
                            $pl_count++;
                        } else {
                            $this->product_model->commonDelete(PRODUCT_LIKES, array('id' => $pl_row->id));
                            $qryCount++;
                        }
                    }
                } else {
                    $pl_count = 0;
                }
                $this->product_model->update_details(USERS, array('likes' => $pl_count), array('id' => $user_list_row->id));
                $qryCount++;
                //----------------------
                //Wants count
                $wanted_products = $this->product_model->get_all_details(WANTS_DETAILS, array('user_id' => $user_list_row->id));
                $qryCount++;
                $wanted_count = 0;
                if ($wanted_products->num_rows() > 0) {
                    foreach ($wanted_products->result() as $wanted_row) {
                        $wanted_chk = $this->product_model->get_all_details(PRODUCT, array('seller_product_id' => $wanted_row->product_id));
                        $qryCount++;
                        if ($wanted_chk->num_rows() == 0) {
                            $wanted_chk = $this->product_model->get_all_details(USER_PRODUCTS, array('seller_product_id' => $wanted_row->product_id));
                            $qryCount++;
                        }
                        if ($wanted_chk->num_rows() > 0) {
                            $wanted_count++;
                        } else {
                            $this->product_model->commonDelete(WANTS_DETAILS, array('id' => $wanted_row->id));
                            $qryCount++;
                        }
                    }
                }
                $this->product_model->update_details(USERS, array('want_count' => $wanted_count), array('id' => $user_list_row->id));
                $qryCount++;
                //-----------------------
                //Owns Count
                $own_products = array_unique(array_filter(explode(',', $user_list_row->own_products)));
                $own_count = 0;
                $own_products_new = $own_products;
                if (count($own_products) > 0) {
                    foreach ($own_products as $own_row) {
                        $own_chk = $this->product_model->get_all_details(PRODUCT, array('seller_product_id' => $own_row));
                        $qryCount++;
                        if ($own_chk->num_rows() == 0) {
                            $own_chk = $this->product_model->get_all_details(USER_PRODUCTS, array('seller_product_id' => $own_row));
                            $qryCount++;
                        }
                        if ($own_chk->num_rows() > 0) {
                            $own_count++;
                        } else {
                            if (($key = array_search($own_row, $own_products_new)) !== false) {
                                unset($own_products_new[$key]);
                            }
                        }
                    }
                }
                $this->product_model->update_details(USERS, array('own_products' => implode(',', $own_products_new), 'own_count' => $own_count), array('id' => $user_list_row->id));
                $qryCount++;
                //-----------------------
                //Lists count
                $lists_details = $this->product_model->get_all_details(LISTS_DETAILS, array('user_id' => $user_list_row->id));
                $qryCount++;
                $list_count = $lists_details->num_rows();
                $this->product_model->update_details(USERS, array('lists' => $list_count), array('id' => $user_list_row->id));
                $qryCount++;
                //-----------------------
                //Followers count
                $follower_id = array_unique(array_filter(explode(',', $user_list_row->followers)));
                $followers_count = 0;
                $follower_id_new = $follower_id;
                if (count($follower_id) > 0) {
                    foreach ($follower_id as $fl_row) {
                        $fl_chk = $this->product_model->get_all_details(USERS, array('id' => $fl_row));
                        $qryCount++;
                        if ($fl_chk->num_rows() > 0) {
                            $followers_count++;
                        } else {
                            if (($key = array_search($fl_row, $follower_id_new)) !== false) {
                                unset($follower_id_new[$key]);
                            }
                        }
                    }
                }
                $this->product_model->update_details(USERS, array('followers' => implode(',', $follower_id_new), 'followers_count' => $followers_count), array('id' => $user_list_row->id));
                $qryCount++;
                //-----------------------
                //Following count
                $following_id = array_unique(array_filter(explode(',', $user_list_row->following)));
                $following_count = 0;
                $following_id_new = $following_id;
                if (count($following_id) > 0) {
                    foreach ($following_id as $fl_row) {
                        $fl_chk = $this->product_model->get_all_details(USERS, array('id' => $fl_row));
                        $qryCount++;
                        if ($fl_chk->num_rows() > 0) {
                            $following_count++;
                        } else {
                            if (($key = array_search($fl_row, $following_id_new)) !== false) {
                                unset($following_id_new[$key]);
                            }
                        }
                    }
                }
                $this->product_model->update_details(USERS, array('following' => implode(',', $following_id_new), 'following_count' => $following_count), array('id' => $user_list_row->id));
                $qryCount++;
                //-----------------------
            }
        }

        //List counts
        $lists_details = $this->product_model->get_all_details(LISTS_DETAILS, array());
        $qryCount++;
        if ($lists_details->num_rows() > 0) {
            foreach ($lists_details->result() as $list_row) {

                //Product count
                $prod_count = 0;
                $product_ids = array_unique(array_filter(explode(',', $list_row->product_id)));
                $product_ids_new = $product_ids;
                if (count($product_ids) > 0) {
                    foreach ($product_ids as $prd_row) {
                        $prd_chk = $this->product_model->get_all_details(PRODUCT, array('seller_product_id' => $prd_row));
                        $qryCount++;
                        if ($prd_chk->num_rows() == 0) {
                            $prd_chk = $this->product_model->get_all_details(USER_PRODUCTS, array('seller_product_id' => $prd_row));
                            $qryCount++;
                        }
                        if ($prd_chk->num_rows() > 0) {
                            $prod_count++;
                        } else {
                            if (($key = array_search($prd_row, $product_ids_new)) !== false) {
                                unset($product_ids_new[$key]);
                            }
                        }
                    }
                }
                $this->product_model->update_details(LISTS_DETAILS, array('product_id' => implode(',', $product_ids_new), 'product_count' => $prod_count), array('id' => $list_row->id));
                $qryCount++;
                //------------------
                //Followers count
                $followers_count = 0;
                $followers_id = array_unique(array_filter(explode(',', $list_row->followers)));
                $followers_id_new = $followers_id;
                if (count($followers_id) > 0) {
                    foreach ($followers_id as $lf_uid) {
                        $ch_usr = $this->product_model->get_all_details(USERS, array('id' => $lf_uid));
                        $qryCount++;
                        if ($ch_usr->num_rows() > 0) {
                            $followers_count++;
                        } else {
                            if (($key = array_search($lf_uid, $followers_id_new)) !== false) {
                                unset($followers_id_new[$key]);
                            }
                        }
                    }
                }
                $this->product_model->update_details(LISTS_DETAILS, array('followers' => implode(',', $followers_id_new), 'followers_count' => $followers_count), array('id' => $list_row->id));
                $qryCount++;
                //------------------
            }
        }
        //-----------------------
        //Selling Products like
        $product_list = $this->product_model->get_all_details(PRODUCT, array());
        $qryCount++;
        if ($product_list->num_rows() > 0) {
            foreach ($product_list->result() as $prod_row) {
                $like_count_query = "select u.id from " . PRODUCT_LIKES . " pl join " . USERS . " u on u.id=pl.user_id where pl.product_id=" . $prod_row->seller_product_id;
                $qryCount++;
                $like_count = $this->product_model->ExecuteQuery($like_count_query);
                if ($prod_row->likes != $like_count->num_rows()) {
                    $this->product_model->update_details(PRODUCT, array('likes' => $like_count->num_rows()), array('id' => $prod_row->id));
                    $qryCount++;
                }
            }
        }
        //-----------------------
        //Affiliate Products like
        $product_list = $this->product_model->get_all_details(USER_PRODUCTS, array());
        $qryCount++;
        if ($product_list->num_rows() > 0) {
            foreach ($product_list->result() as $prod_row) {
                $like_count_query = "select u.id from " . PRODUCT_LIKES . " pl join " . USERS . " u on u.id=pl.user_id where pl.product_id=" . $prod_row->seller_product_id;
                $qryCount++;
                $like_count = $this->product_model->ExecuteQuery($like_count_query);
                if ($prod_row->likes != $like_count->num_rows()) {
                    $this->product_model->update_details(USER_PRODUCTS, array('likes' => $like_count->num_rows()), array('id' => $prod_row->id));
                    $qryCount++;
                }
            }
        }
        //-----------------------
        echo $qryCount . ' queries executed.';
    }

    public function check_upload() {
        $url = 'http://hugoboss.scene7.com/is/image/hugoboss/20_hbeu50264459_001_10?$re_productSliderCategory$';
//		$img_data = file_get_contents($image_url);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_REFERER, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); //Set curl to return the data instead of printing it to the browser.
        $img_data = curl_exec($ch);
        curl_close($ch);
        echo $url . '<br/>';
        echo $img_data;
        die;
        $img_full_name = substr($image_url, strrpos($image_url, '/') + 1);
        $img_name_arr = explode('.', $img_full_name);
        $img_name = $img_name_arr[0];
        $ext = $img_name_arr[1];
        if ($ext == '') {
            $ext = 'jpg';
        }
        $new_name = str_replace(array(',', '&', '<', '>', '$', '(', ')'), '', $img_name . mktime() . '.' . $ext);
        $new_img = 'images/product/' . $new_name;

        file_put_contents($new_img, $img_data);
    }

    public function change_status() {
        if ($this->checkLogin('U') != '') {
            $pid = $this->uri->segment(2);
            $mode = $this->uri->segment(1);
            $status = $this->uri->segment(4);
            $next = $this->input->get('next');
            if ($mode == 'things') {
                $tbl = PRODUCT;
            } else {
                $tbl = USER_PRODUCTS;
            }
            if ($pid != '') {
                $product_details = $this->product_model->get_all_details($tbl, array('seller_product_id' => $pid));
                if ($product_details->num_rows() > 0) {
                    $dataArr = array('status' => $status);
                    $product_details = $this->product_model->update_details($tbl, $dataArr, array('seller_product_id' => $pid));
                    $this->setErrorMessage('success', 'Status changed successfully');
                }
            }
            if ($next != '') {
                redirect($next);
            } else {
                redirect(base_url());
            }
        } else {
            redirect('login');
        }
    }

    public function checkDownload() {

        $this->load->library('encrypt');
        if ($this->checkLogin('U') == '') {
            redirect(base_url() . 'login');
        } else {
            $code = $this->input->post('code');
            $codeDetails = $this->product_model->get_all_details(PAYMENT, array('product_download_code' => $code, 'user_id' => $this->checkLogin('U'), 'product_code_status' => "Pending"));
            if ($codeDetails->num_rows() == 1) {
                $id = $codeDetails->row()->id;
                $pid = $codeDetails->row()->product_id;
                //$this->product_model->update_details(PAYMENT, array('product_code_status' => "Expired"), array('id' => $id));
                $prodetails = $this->product_model->get_all_details(PRODUCT, array('id' => $pid));
                if ($prodetails->num_rows() > 0) {
                    $key = "dwnldkey";
                    $encrypted_string = $this->encrypt->encode($prodetails->row()->product_doc, $key);
                    //$encrypted_string = $prodetails->row()->product_doc;
                    $this->session->set_userdata('pfile', $encrypted_string);
                    echo $encrypted_string;
                    //echo $this->session->userdata('pfile');
                }
            } else {
                echo 0;
            }
        }
    }

    public function editPictureProductsdoc() {
        $id = $this->input->post('id');
        $this->product_model->update_details(PRODUCT, array('product_doc' => '', 'pdf_images' => ''), array('id' => $id));
        echo json_encode($id);
    }
	public function seller_product_add() {
		if($this->checkLogin ( 'U' ) == ''){
			redirect('login');
		}else{
			if ($this->data ['userDetails']->row ()->group == 'Seller'){
				//$this->data ['categoryView'] = $this->product_model->get_category_details ();
				$this->data ['atrributeValue'] = $this->product_model->view_atrribute_details ();
				$this->data ['PrdattrVal'] = $this->product_model->view_product_atrribute_details ();
				$this->data ['conditionsList'] = $this->product_model->get_all_details(CONDITION,array('status'=>'Active'));
				$this->load->view('site/product/seller_product_add', $this->data);
			} else {
				redirect('seller-signup');
			}
		}
	}
	public function insertEditProduct(){

        //echo "<pre>"; print_r($this->input->post()); die;

		if($this->checkLogin('U') == ''){
			redirect (base_url() . 'login');
		}else{
			$product_name = $this->input->post('product_name');
			$product_id = $this->input->post('productID');
			if($product_name == ''){
				$this->setErrorMessage('error', 'Product name required');
				echo "<script>window.history.go(-1)</script>";
				exit();
			}
			$sale_price = $this->input->post('sale_price');
			if($sale_price == ''){
				$this->setErrorMessage('error', 'Sale price required');
				echo "<script>window.history.go(-1)</script>";
				exit();
			}elseif($sale_price <= 0){
				$this->setErrorMessage ( 'error', 'Sale price must be greater than zero' );
				echo "<script>window.history.go(-1)</script>";
				exit();
			}
			$old_product_details = array();
			$condition = array('product_name' => $product_name);
			$price_range = '';
			$brand_seourl = '';
			
			$excludeArr = array(
				"newBrand", "merchantproduct","subcategories","gateway_tbl_length","imaged","productID","changeorder",
				"status","category_id","attribute_name","attribute_val","attribute_weight",
				"attribute_price","product_image","userID","product_attribute_name","attr_name1",
				"product_attribute_val","attr_val1","attr_type1","product_attribute_type" ,"attr_qty1","product_attribute_qty","shipping_to","ship_to_id","shipping_cost","shipping_with_another","processing_max","processing_min","processing_time_units","ship_duration","country_code"
			);
			$product_status = 'Publish';
			$seourl = url_title($product_name, '-', TRUE);
			if($seourl == ''){
				$seourl = str_replace(' ', '-', $product_name);
			}
			$checkSeo = $this->product_model->get_all_details(PRODUCT, array('seourl' =>$seourl));
			$seo_count = 1;
			while($checkSeo->num_rows() > 0){
				$seourl = $seourl . $seo_count;
				$seo_count ++;
				$checkSeo = $this->product_model->get_all_details(PRODUCT, array('seourl'=> $seourl));
			}
		
			$ImageName = '';
			$list_name_str = $list_val_str = '';
			$list_name_arr = $this->input->post('attribute_name');
			$list_val_arr = $this->input->post('attribute_val');
			if(is_array($list_name_arr) && count($list_name_arr ) > 0){
				$list_name_str = implode ( ',', $list_name_arr );
				$list_val_str = implode ( ',', $list_val_arr );
			}
			/***********Ship Duration*************/
			if($this->input->post('ship_duration') == 'custom'){
				$businessDays = $this->input->post('processing_time_units');
				$processMin = $this->input->post('processing_min');
				$processMax = $this->input->post('processing_max');
				$shipDuration = $processMin.'-'.$processMax.' '.$businessDays;
			}else{
				$shipDuration = $this->input->post('ship_duration');
			}
			/*************Digital File**************/

            //echo "digital"; die;
			if($this->input->post('product_type')=='digital'){
				if(isset($_FILES['pdfupload']['name']) && ($_FILES['pdfupload']['name']!='')){
					$config1['overwrite'] = FALSE;
					$config1['allowed_types'] = 'pdf|doc|docx|txt|csv|xls|text|jpg|jpeg|gif|png|psd|bmp';
					$config1['upload_path'] = './pdf-doc/pdf/';
					$this->load->library('upload', $config1);
					$pdfImage = array();
					if($this->upload->do_upload('pdfupload')){
						$docDetails = $this->upload->data();
						$product_doc = $docDetails['file_name'];
					}else{
						$this->setErrorMessage('error', $this->upload->display_errors());

                         //print_r($_FILES['pdfupload']['name']);
                         //echo $this->upload->display_errors(); die;
						redirect(base_url().'seller-product');
					}
				}else{
					$product_doc ='';
				}
			}else{
				$product_doc ='';
			}


			$countrCode = explode('|', $this->input->post('country_code'));
			if($this->input->post('product_type')=='physical'){
				$country_code = $countrCode[1];
			}else{
				$country_code = '';
			}
			$datestring = "%Y-%m-%d %H:%i:%s";
			$time = time();
			$inputArr = array(
				'created' => mdate( $datestring, $time),
				'seourl' => $seourl,
				'status' => $product_status,
				'list_name' => $list_name_str,
				'list_value' => $list_val_str,
				'price_range' => $price_range,
				'user_id' => $this->checkLogin('U'),
				'seller_product_id' => mktime(),
				'ship_duration' => $shipDuration,
				'country_code'=> $country_code,
				'product_doc'=> $product_doc
			);
			/**********Product Image Upload**********/

            //echo "<pre>";print_r($_POST);  echo "<br>";
            //print_r($_FILES);  echo "<br>";

            $config['overwrite'] = FALSE;
			$config['allowed_types'] = 'jpg|jpeg|gif|png';
			$config['upload_path'] = './images/product/';

            //print_r($config);
			$this->load->library('upload', $config);

          $this->upload->initialize($config);
			if($this->upload->do_multi_upload('product_image')){
				$logoDetails = $this->upload->get_multi_upload_data();
				//echo "<pre>"; print_r($logoDetails);
                foreach($logoDetails as $fileDetails){
                    //echo "<pre>"; print_r($fileDetails); die;
					$this->crop_and_resize_image ( 215, 215, './images/product/', $fileDetails ['file_name'], './images/product/thumb/' );
					$this->crop_and_resize_image ( 600, 600, './images/product/', $fileDetails ['file_name'], './images/product/' );
					$ImageName .= $fileDetails['file_name'].',';
				}
			}
            //else{
                //echo $this->upload->display_errors();
            //}

            //echo "<pre>".$ImageName; die;
			
			$product_data = array('image' => $ImageName);
			if( $this->input->post(newBrand) != ''){
				$brand_name = $this->input->post(newBrand);
				$brand_seourl = url_title($brand_name);
				$this->product_model->simple_insert(BRAND,array('brand_name'=>$brand_name,'status'=>'Active', 'brand_seourl'=>$brand_seourl));
				$product_data = array('image' => $ImageName, 'brand_seo' => $brand_seourl );
			}
			
			$dataArr = array_merge($inputArr, $product_data);
			#echo "<pre>"; print_r($dataArr); die;
			$condition = array();
			$this->product_model->commonInsertUpdate(PRODUCT, 'insert', $excludeArr, $dataArr, $condition);
			$product_id = $this->product_model->get_last_insert_id();
			// Generate short url
			$short_url = $this->get_rand_str('6');
			$checkId = $this->product_model->get_all_details(SHORTURL, array(
				'short_url' => $short_url
			));
			while($checkId->num_rows() > 0){
				$short_url = $this->get_rand_str('6');
				$checkId = $this->product_model->get_all_details(SHORTURL, array(
				'short_url' => $short_url
				));
			}
			$url = base_url().'things/'.$product_id.'/'.url_title($product_name, '-');
			$this->product_model->simple_insert(SHORTURL, array('short_url' =>$short_url,'long_url' => $url));
			$urlid = $this->product_model->get_last_insert_id();
			$this->product_model->update_details(PRODUCT, array('short_url_id' =>$urlid
			),array('id' => $product_id));
			/************Product Attributes*************/
			if($this->input->post('product_type')=='physical'){
				$Attr_name_str = $Attr_val_str = '';
				$Attr_type_arr = $this->input->post('product_attribute_type');
				$Attr_name_arr = $this->input->post('product_attribute_name');
				$Attr_val_arr = $this->input->post('product_attribute_val');
				$Attr_qty_arr = $this->input->post('product_attribute_qty');
				if(is_array($Attr_name_arr) && count($Attr_name_arr) >0){
					for($k = 0; $k < sizeof($Attr_name_arr); $k++){
						$dataSubArr = '';
						$dataSubArr = array (
							'product_id' => $product_id,
							'attr_id' => $Attr_type_arr [$k],
							'attr_name' => $Attr_name_arr [$k],
							'attr_price' => $Attr_val_arr [$k],
							'attr_qty' => $Attr_qty_arr [$k]
						);
						$this->product_model->add_subproduct_insert($dataSubArr);
					}
					if($this->input->post('attribute_must') =='yes'){
						$newQty = array_sum($Attr_qty_arr);
						$this->product_model->update_details(PRODUCT,array('quantity'=>$newQty),array('id'=>$product_id));
					}else{
						$_product = $this->product_model->get_all_details(PRODUCT,array('id'=>$product_id));
						$sumQty = array_sum($Attr_qty_arr);
						if($sumQty > $_product->row()->quantity){
							$newQty = $sumQty;
							$this->product_model->update_details(PRODUCT,array('quantity'=>$newQty),array('id'=>$product_id));
						}
					}
				}
                    $category_id     =     $this->input->post('category_id');
                
                    if ($this->input->post('category_id') != ''){
                        $sub_category             = $this->input->post('subcategories');
                        
                            if( isset($sub_category) && count($sub_category) > 0 && $sub_category!=''){
                                $subcats          = array_filter($sub_category);
                                $sub_categories   = array_unique($subcats);
                                $sub_categor_list = implode(',',$sub_categories);
                                $category_id     .= ','.$sub_categor_list;
                            }
                    }else {
                        $category_id = '';
                    }
                   
                    $current_category_list          = explode(',',$category_id);
                    $current_Product_category_count = count($current_category_list);
                    
                    $existing_products = $this->product_model->get_all_details(PRODUCT,array('user_id'=>$this->checkLogin('U')));
                        foreach($existing_products->result() as $existed_products){
                            if( $existed_products->category_id !='' ){
                                
                                $existed_category_list = explode(',',$existed_products->category_id);
                                
                                $existed_Product_category_count = count($existed_category_list);
                                    
                                    if( $existed_Product_category_count == $current_Product_category_count ){
                                          
                                        $current_category_last_element  =  end(array_values($current_category_list));
                                        $existed_category_last_element  =  end(array_values($existed_category_list));
                                        
                                        if( $existed_category_last_element == $current_category_last_element ){
                                            
                                                /* update all sub categories except last element */
                                                $current_categories = $current_category_list;
                                                array_pop($current_categories);
                                                $categories = $current_categories;
                                                $categories =implode(',',$categories);
                                                if($current_Product_category_count > 1){
                                                    $categories = $categories.',';
                                                }
                                                    $dataArr = array( 'category_id' => $categories);
                                                    $this->product_model->update_details(PRODUCT,$dataArr,array('id'=>$product_id));
                                            
                                            $product_detail = $this->product_model->get_all_details(CATEGORY,array('id'=>$existed_category_last_element,'status'=>'Active'));
                                            $category_name  = $product_detail->row()->seo;
                                            if( !empty($product_detail) && $category_name !='')
                                                $category_name  = $category_name;
                                            else
                                                $category_name  = 'Category';
                                            $lg_err_msg =  $category_name.' already existed. Try other categories';
                                            $this->setErrorMessage('error',$lg_err_msg);
                                            $product_detailss = $this->product_model->get_all_details(PRODUCT,array('id'=>$product_id));
                                            $seller_product_id  = $product_detailss->row()->seller_product_id;
                                            redirect('things/'.$seller_product_id.'/edit/categories');
                                        }
                                    }
                            }
                        }
                        $cat_id = end(array_values($current_category_list));
                        $product_detail = $this->product_model->get_all_details(CATEGORY,array('id'=>$cat_id,'status'=>'Active'));
                        $sku_category  = $product_detail->row()->seourl;
                        $dataArr = array( 'category_id' => $category_id,'sku_category'=>$sku_category );
                        $this->product_model->update_details(PRODUCT,$dataArr,array('id'=>$product_id));
              
		    }
		/**********Product Sub Shipping***********/
		if($this->input->post('product_type')=='physical'){
			if($this->input->post('shipping_to') != ''){
				$ship_to = $this->input->post('shipping_to');
				//echo "<pre>"; print_r($ship_to); die;
				$ship_to_id = $this->input->post('ship_to_id');
				$cost_individual = $this->input->post('shipping_cost');
				$cost_with_another = $this->input->post('shipping_with_another');
				for($i=0; $i < sizeof($ship_to); $i++){
					$ship_name = @explode('|', $ship_to[$i]);
					if($ship_to[$i] == 'Everywhere Else'){
						$shipName = 'Everywhere Else';
						$shipId = 232;
					} else {
						$shipName = $ship_name[2];
						$shipCode = $ship_name[1];
						$shipId = $ship_to_id[$i];
					}
					$seourlBase = $seourl = url_title($shipName, '-', TRUE);
					$seourl_check = '0';
					$duplicate_url = $this->product_model->get_all_details(SUB_SHIPPING,array('ship_seourl'=>$seourl));
					if($duplicate_url->num_rows()>0){
						$seourl = $seourlBase.'-'.$duplicate_url->num_rows();
					}else{
						$seourl_check = '1';
					}
					$urlCount = $duplicate_url->num_rows();
					while($seourl_check == '0'){
						$urlCount++;
						$duplicate_url = $this->product_model->get_all_details(SUB_SHIPPING,array('ship_seourl'=>$seourl));
						if ($duplicate_url->num_rows()>0){
							$seourl = $seourlBase.'-'.$urlCount;
						}else {
							$seourl_check = '1';
						}
					}
					$dataArrShip = array('product_id' => $product_id,'ship_id' => $shipId, 'ship_name' => $shipName,'ship_code'=>$shipCode,'ship_cost' => $cost_individual[$i],'ship_seourl' => $seourl, 'ship_other_cost' => $cost_with_another[$i]);
					$this->product_model->simple_insert(SUB_SHIPPING,$dataArrShip);
				}
			}
		}
		$this->setErrorMessage('success', 'Product added successfully');
		// Update the list table
		if(is_array($list_val_arr )){
			foreach($list_val_arr as $list_val_row){
				$list_val_details = $this->product_model->get_all_details(LIST_VALUES, array(
				'id' => $list_val_row));
				if($list_val_details->num_rows () == 1){
					$product_count = $list_val_details->row()->product_count;
					$products_in_this_list = $list_val_details->row()->products;
					$products_in_this_list_arr = explode(',', $products_in_this_list);
					if(!in_array( $product_id, $products_in_this_list_arr)){
						array_push($products_in_this_list_arr, $product_id);
						$product_count ++;
						$list_update_values = array(
							'products' => implode(',', $products_in_this_list_arr),
							'product_count' => $product_count
						);
						$list_update_condition = array('id' =>$list_val_row);
						$this->product_model->update_details(LIST_VALUES, $list_update_values, $list_update_condition);
					}
				}
			}
		}
		// Update user table count
		if($edit_mode == 'insert'){
			if($this->checkLogin( 'U' ) != ''){
				$user_details = $this->product_model->get_all_details(USERS, array(
				'id' =>$this->checkLogin('U')));
				if($user_details->num_rows () == 1){
					$prod_count = $user_details->row ()->products;
					$prod_count ++;
					$this->product_model->update_details(USERS, array(
					'products'=> $prod_count), array('id' =>$this->checkLogin('U')));
				}
			}
		}
		if($this->input->post('merchantproduct') == 'merchant'){
			redirect(base_url(). 'merchant/products');
		} else{
			redirect(base_url());
		}
		}
	}
	public function load_ajax_shipping_list($i, $countryArr=''){

		$DisablecountryArr = explode(',',urldecode($countryArr));
		$countryList = $this->product_model->get_all_details(COUNTRY_LIST,array(),array(array('field'=>'name','type'=>'asc')))->result();
		echo '<tr id="tab_'.$i.'"><td><p id="shipping_to_'.$i.'_lab" ></p><select name="shipping_to[]" id="shipping_to_'.$i.'" class="shipping_to" onchange="display_sel_val(this);">';
		echo '<option value="">Select a location</option>';
		foreach($countryList as $country){
			if(in_array($country->name, $DisablecountryArr, TRUE)){
				echo '<option value="'.$country->name.'" disabled>'.$country->name.'</option>';
			}else{
				echo'<option value="'.$country->id.'|'.$country->country_code.'">'.$country->name.'</option>';
			}
		}
		if(in_array('Everywhere Else', $DisablecountryArr, TRUE)){
			echo '<option value="232|Everywhere Else" disabled>Everywhere Else</option>';
		}else{
			echo '<option value="232|Everywhere Else">Everywhere Else</option>';
		}
		echo '</select><input type="hidden" name="ship_to_id[]" id="shipping_to_'.$i.'_id" /><input type="hidden" name="shipping_to[]" id="shipping_to_'.$i.'_name" /></td><td><input type="text" value="" placeholder="'.$this->data['currencySymbol'].':" class="form-control shipping_txt_bax"  name="shipping_cost[]" id="shipping_cost_'.$i.'"></td>
		<td><input type="text" value="" placeholder="'.$this->data['currencySymbol'].':" class="form-control shipping_txt_bax"  name="shipping_with_another[]" id="shipping_with_another_'.$i.'"></td>
		<td><a class="close_icon" href="javascript:void(0)" id="'.$i.'"></a></td>
		</tr>';
	}
	public function getSubCategories(){
		$id = $this->input->post('id');
		$getsubcat = $this->product_model->get_all_details(CATEGORY,array('rootID'=>$id));
		if($getsubcat->num_rows()>0){
			$html = '<option value="">select subcategory</option>';
			foreach($getsubcat->result() as $_getsubcat){
				$html .= '<option value="'.$_getsubcat->id.'">'.$_getsubcat->cat_name.'</option>';
			}
		}else{
			$html .="";
		}
		echo $html;
	}
    /**** @bablu load sub category ****/
        public function select_ajax_level1_subcategory(){
          if($this->lang->line('shop_sub_selectcategory') != '') { 
                $sel_cat= stripslashes($this->lang->line('shop_sub_selectcategory')); 
            } 
            else {
                $sel_cat= "Select a sub category";
            }
          
            $selectSubcatval = $this->product_model->get_all_details(CATEGORY,array('rootID'=>$this->input->get('main_cat_id'),'status'=> 'Active'));
            if($selectSubcatval->num_rows() > 0){
                echo '<option value="">'.$sel_cat.'</option>';
                 foreach($selectSubcatval->result() as $MaincatValues) {
                    echo '<option value="'.$MaincatValues->id.'">'.$MaincatValues->cat_name.'</option>'; 
                 } 
            } else {
                echo 'Nocat';
            }
            
        }
    /**** @bablu load sub category ****/
    /**** @bablu  ---New Auction---   #Start****/
    public function new_auctioon(){
        if($this->checkLogin('U') != ''){
			//echo '<pre>';print_r($_POST);
            /*  $s_date                 =   date('Y-m-d',strtotime($this->input->post('start_date')));
             $s_time                 	=   date('H:i:s',strtotime($this->input->post('start_time')));  
             $e_date                 	=   date('Y-m-d',strtotime($this->input->post('expire_date')));
             $e_time                 	=   date('H:i:s',strtotime($this->input->post('expire_time'))); 
             $start_time             	=   $s_date.' '.$s_time;  */
			$duration					= 	$this->input->post('duration');
			$datestring             	=   "%Y-%m-%d %h:%i:%s";
            $time                  		=   time();
            $createdTime 			  	=  	mdate($datestring, $time);
			$start_time					=  	$createdTime;
			$expire_time            	=   Date('Y-m-d h:i:s', strtotime("+".$duration." days")); 
			$sku_category           	=   $this->input->post('sku_cat');
            $transaction_id         	=   $this->input->post('tnst');
            $proId                  	=   $this->input->post('pd');
            $message                	=   trim($this->input->post('message'));
            
            
			$product_detail         	=   $this->product_model->get_all_details(PRODUCT,array('id'=>$proId));
            
            $seller_id              	=   $product_detail->row()->user_id;
            $product_type           	=   $product_detail->row()->product_type;
            $auction_prdId          	=   $this->input->post('pd');
            $user_id                	=   $this->checkLogin('U');
			if($sku_category != ''){
				$seller_list            =   $this->product_model->get_all_sku_sellers($sku_category,$user_id,$proId,$product_type);
				/* echo $this->db->last_query();
				echo '<pre>';print_r($seller_list->result());die;  */
				$price					= 	'';
				$payment_detail         =   $this->product_model->get_all_details(PAYMENT,array('paypal_transaction_id'=>$transaction_id));
				$price 					=   $payment_detail->row()->total;
				
				foreach($seller_list->result() as $seller){
					$seller_id          = $seller->seller_id;
					$seller_email       = $seller->seller_email;
					$product_id         = $seller->product_id;
					$seller_product_id  = $seller->seller_product_id;
					$random_no          = $this->get_rand_num('6');
					$bid                = $random_no + $seller_id;
					$bid_id             = "BID".$bid;
					$AuctionArr         =   array(  'productId'     =>  $product_id,
													'auction_prdId' =>  $auction_prdId,
													'sku_category'  =>  $sku_category,
													'price'         =>  $price,
													'dateAdded'     =>  $createdTime,
													'start_time'    =>  $start_time,
													'expire_time'   =>  $expire_time,
													'BidId'         =>  $bid_id,
													'senderId'      =>  $this->checkLogin ( 'U' ),
													'receiverId'    =>  $seller_id,
													'subject'       =>  'Auction Request : '.$bid_id,
													'message'       =>  $message,
													'type'          =>  'auction',
													'seller'        =>  $seller_id,
													'transaction_id'=>  $transaction_id,
													'Buyer'         =>  $this->checkLogin ( 'U' )
													);
					$this->user_model->simple_insert(BID_MESSAGE, $AuctionArr);
					$this->product_model->update_details(PAYMENT,array( 'read_status'=>'unread','start_date'=>$start_time, 'end_date'=>$expire_time ),array('paypal_transaction_id'=>$transaction_id));
						/* Inserting to Seller Notification Table */
						$notificationArr =  array(  'user_id'       =>  $this->checkLogin('U'),
													'created'       =>  $createdTime,
													'activity_id'   =>  $seller_product_id,
													'activity'      =>  "New Auction Request",
													'activity_ip'   =>  $this->input->ip_address()
											);
						$this->product_model->simple_insert(NOTIFICATIONS,$notificationArr);
						
						
						/* Inserting to Seller Activity Table */
						$activityArray =    array(  'user_id'       =>  $this->checkLogin('U'),
													'activity_time' =>  $createdTime,
													'activity_id'   =>  $product_id,
													'activity_name' =>  "New Auction Request",
													'activity_ip'   =>  $this->input->ip_address()
											);
						$this->product_model->simple_insert(USER_ACTIVITY,$activityArray);
						
				}
				
					
						/* Inserting to Buyer Activity Table */
						$activityArray =    array(  'user_id'       =>  $seller_id,
													'activity_time' =>  $createdTime,
													'activity_id'   =>  $proId,
													'activity_name' =>  "New Auction Request",
													'activity_ip'   =>  $this->input->ip_address()
											);
						$this->product_model->simple_insert(USER_ACTIVITY,$activityArray);          

						/* Sending mail to all sku_category sellers */
							$seller_list = $this->product_model->get_auction_sellerList($sku_category,$proId);
								if($seller_list->num_rows() >0){
									$email ='';
									if($seller_list->num_rows() == 1){
										$email = $seller_list->row()->seller_email;
									}else{
										foreach($seller_list->result() as $sList){
											$email .= $sList->seller_email.',';
										}
									}
									$newsid = '37';
									$template_values = $this->user_model->get_newsletter_template_details($newsid);
									$adminnewstemplateArr = array('email_title' => $this->config->item('email_title'), 'logo' => $this->data['logo']);
									$conversation_link = base_url().'bids';
									$userDetails1 = $this->product_model->get_all_details(USERS,array('id'=>$this->checkLogin('U')));
									$buyer_msg = trim($this->input->post('message'));
									extract($adminnewstemplateArr);
									$subject = 'From: ' . $this->config->item('email_title') . ' - ' . $template_values['news_subject'];
									$message1 .= '<!DOCTYPE HTML>
										<html>
										<head>
										<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
										<meta name="viewport" content="width=device-width"/>
										<title>' . $template_values['news_subject'] . '</title>
										<body>';
									include('./newsletter/registeration' . $newsid . '.php');
									
									$message1 .= '</body>
										</html>';
									
									if ($template_values['sender_name'] == '' && $template_values['sender_email'] == '') {
										$sender_email = $this->config->item('site_contact_mail');
										$sender_name = $this->config->item('email_title');
									} else {
										$sender_name = $template_values['sender_name'];
										$sender_email = $template_values['sender_email'];
									}
									$email_values = array('mail_type' => 'html',
										'from_mail_id' => $sender_email,
										'mail_name' => $sender_name,
										'to_mail_id' => '',
										'bcc_mail_id' => $email,
										'subject_message' => 'Auction Request',
										'body_messages' => $message1,
										'mail_id' => 'New Auction Request'
									);
								   //echo '<pre>';print_r($email_values);die;
									$email_send_to_common = $this->product_model->common_email_send($email_values);
							   
								}
			}
                $this->setErrorMessage("success","Auction sent. Wait while we get you the best deal.");
                
            redirect(base_url('auctions'));       
        
        }else{
            redirect('login');
        }
    }
    /**** @bablu  ---New Auction---   #End****/
}

/*End of file product.php */
/* Location: ./application/controllers/site/product.php */
