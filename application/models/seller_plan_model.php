<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Seller_plan_model extends My_Model
{
	public function __construct()
	{
		parent::__construct();
	}
        public function get_featured_product($userId,$seller_plan_id){
            $Query = "SELECT * FROM ".FEATURED_PRODUCTS." WHERE userId =".$userId." AND sellerPlan_id = ".$seller_plan_id." ";
            $data = $this->ExecuteQuery($Query);
            if($data->num_rows > 0){
            $data = $data->result();
            //print_r(implode(',',unserialize($data[0]->featuredProducts)));
            $Query = "SELECT * FROM ".PRODUCT." WHERE seller_product_id IN (".implode(',',unserialize($data[0]->featuredProducts)).") ";
            $products = $this->ExecuteQuery($Query);
            }
            if($products->num_rows > 0){
            return $products->result();
            }else{
                return array();
            }
            //foreach(unserialize($data[0]->featuredProducts) as $product )
        }
        public function get_featured_sellers(){
            $datestring = "%Y-%m-%d %h:%i:%s";
            $time = time();
            $now = strtotime(mdate($datestring,$time));
            $Query = "SELECT id FROM ".FEATURED_SELLER." WHERE UNIX_TIMESTAMP(expiresOn) < '".$now."' AND isExpired = 0 ";
            $result = $this->ExecuteQuery($Query);
            if($result->num_rows > 0){
                foreach($result->result() as $id){
                    $expired_ids[] = $id->id;
                }
               $Query = "UPDATE ".FEATURED_SELLER." SET isExpired = 1 WHERE id IN (".implode(',',$expired_ids).") ";
               $this->ExecuteQuery($Query);
               $Query = "DELETE FROM ".FEATURED_PRODUCTS." WHERE sellerPlan_id IN (".implode(',',$expired_ids).") ";
               $this->ExecuteQuery($Query);
            }
            $Query = "SELECT u.*, COUNT(p.id) as productCount FROM ".USERS." u LEFT JOIN ".FEATURED_SELLER." s ON s.userId = u.id LEFT JOIN ".PRODUCT." p on u.id=p.user_id WHERE s.isExpired = 0 and p.status='Publish' group by u.id";
            return $this->ExecuteQuery($Query);
        }

        public function get_featured_products(){
            $datestring = "%Y-%m-%d %h:%i:%s";
            $time = time();
            $now = strtotime(mdate($datestring,$time));
            $Query = "SELECT id FROM ".FEATURED_SELLER." WHERE UNIX_TIMESTAMP(expiresOn) < '".$now."' AND isExpired = 0 ";
            $result = $this->ExecuteQuery($Query);
            if($result->num_rows > 0){
                foreach($result->result() as $id){
                    $expired_ids[] = $id->id;
                }
               $Query = "UPDATE ".FEATURED_SELLER." SET isExpired = 1 WHERE id IN (".implode(',',$expired_ids).") ";
               $this->ExecuteQuery($Query);
               $Query = "DELETE FROM ".FEATURED_PRODUCTS." WHERE sellerPlan_id IN (".implode(',',$expired_ids).") ";
               $this->ExecuteQuery($Query);
            }
            $Query = "SELECT * FROM ".FEATURED_PRODUCTS."";
            $productsId = $this->ExecuteQuery($Query);
            foreach($productsId->result() as $product){
                $Ids = unserialize($product->featuredProducts);
                foreach($Ids as $id){
                    $featuredIds[] = $id;
                }
            }
           if(count($featuredIds) > 0){
           $Query = "select p.*,u.full_name,u.user_name,sh.short_url,u.email as selleremail,u.id as sellerid,u.thumbnail,u.feature_product from ".PRODUCT." p
		LEFT JOIN ".USERS." u on u.id=p.user_id
		LEFT JOIN ".SHORTURL." sh on sh.id=p.short_url_id WHERE p.seller_product_id IN (".implode(',',$featuredIds).") ";
           return $this->ExecuteQuery($Query);
           }else{
               return array('num_rows'=>0);
           }
        }




        public function get_success_seller_with_users(){
            $Query = "SELECT s.*,u.full_name FROM ".FEATURED_SELLER." s LEFT JOIN ".USERS." u ON u.id = s.userId ORDER BY s.createdOn DESC ";
            return $this->ExecuteQuery($Query);
//            $valu = $this->ExecuteQuery($Query);
//            print_r($valu->result());die;
        }
	public function get_fail_seller_with_users(){
            $Query = "SELECT s.*,u.full_name FROM fc_featured_seller_temp s LEFT JOIN ".USERS." u ON u.id = s.userId ORDER BY s.createdOn DESC ";
            return $this->ExecuteQuery($Query);
        }
}
