<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * This model contains all db functions related to mobile Json management
 * @author Teamtweaks
 *
 */
class Mobile_model extends My_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	/*******Delete product from favorite list***/
	public function product_fav_delete($userid='',$pid=''){
		$this->db->where('p_id', $pid);
		$this->db->where('user_id', $userid);
		$this->db->delete(FAVORITE);
		return;
	}
	/*******Delete Shop from favorite list***/
	public function fav_delete($userid='',$shopid=''){
		$this->db->where('shop_id', $shopid);
		$this->db->where('user_id', $userid);
		$this->db->delete(FAVORITE);
		return;
	}
	/*******select particular colum from DB ***/
	public function get_column_details($table='',$condition='',$columnlist=''){
		$this->db->select($columnlist);
		$this->db->from($table);
		$this->db->where($condition);
		return $this->db->get();
	}
 public function get_conversation_unread_count($userId='',$tid){
	$this->db->select('count(c.id) as unreadcount');
		$this->db->from(CONTACTPEOPLE.' as c');
		$condition='c.receiver_id ='.$userId.' AND  c.receiver_status ="Unread" and c.tid='.$tid.'';
		$this->db->where($condition);

		$conversationlist= $this->db->get();
 # echo $this->db->last_query();die;
  #exit;
		return $conversationlist;
	}
	/*******To get results ***/
	public function get_in_results($table='',$in_ids='',$columnlist=''){
		$in_idsArr=@explode(',',$in_ids); $id='';
		for($i=0;$i<count($in_idsArr);$i++){
			if($in_idsArr[$i] != ''){
				if($i ==0){
					$id.="'".$in_idsArr[$i]."'";
				}else{
					$id.=",'".$in_idsArr[$i]."'";
				}
			}
		}
		$this->db->select($columnlist);
		$this->db->from($table);
		if($id!=''){
			$this->db->where('id IN ('.$id.')',NULL,FALSE);
		}
		$this->db->limit(4);
		return $this->db->get();
	}
	/*******to get product from favorite list***/
	public function getFavoriteProduct($userId='',$condition='',$limit=2000,$offset=0){
		$this->db->select('p.id as product_id,p.image as product_image,p.price,p.seourl as product_url,s.seller_businessname as shop_name,s.seourl as shop_url,a.pricing');
		$this->db->from(PRODUCT.' as p');
		$this->db->join(FAVORITE.' as f','f.p_id=p.id');
		$this->db->join(SELLER.' as s','s.seller_id=p.user_id','left');
		$this->db->join(SUBPRODUCT.' as a','p.id=a.product_id','left');
		$this->db->where('f.user_id',$userId);
		if($condition != ''){
			$this->db->where($condition);
		}
		$this->db->group_by('p.id');
		return $this->db->get();
	}
	/*******to get product list***/
	function get_product_detail($condition='',$slectColums=''){
		$this->db->select($slectColums);
		$this->db->from(PRODUCT.' as p');
		$this->db->join(SELLER.' as s','s.seller_id=p.user_id','left');
		$this->db->join(SUBPRODUCT.' as a','p.id=a.product_id','left');
		$this->db->where($condition,NULL,FALSE);
		return $this->db->get();
	}

	function get_search_product_detail($condition='',$slectColums=''){
		$this->db->select($slectColums);
		$this->db->from(PRODUCT.' as p');
		$this->db->join(USERS.' as u','p.user_id=u.id','left');
		$this->db->join(SELLER.' as s','s.seller_id=p.user_id','left');
		$this->db->join(SUBPRODUCT.' as a','p.id=a.product_id','left');
		$this->db->join(SUB_SHIPPING.' as ss','p.id=ss.product_id','left');
		$this->db->join('shopsy_subproducts as sub','p.id= sub.product_id','left');
		$this->db->join(FEATURE_PRODUCT.' as fp','fp.product_seo = p.seourl','left');
		$this->db->where($condition,NULL,FALSE);
		return $this->db->get();
	}
	/*******to get shop list***/
	function get_shops_details($condition='',$colums='',$postnumbers=20,$offset=0){
		$this->db->select($colums);
		$this->db->from(SELLER.' as s');
		$this->db->join(USERS.' as u','u.id=s.seller_id');
		if($condition !=''){
			$this->db->where('s.seller_id',$condition);
		}
		$this->db->where('s.status','active');
		$this->db->where('u.group','seller');
		$this->db->limit($postnumbers,$offset);
		return $this->db->get();
	}
	/*******to get all shop list***/
	public function get_allshop_details($condition='',$colums='',$type="DESC",$postnumbers=20,$offset=0){
		$this->db->select($colums);
		$this->db->from(SELLER.' as s');
		$this->db->join(USERS.' as u','u.id=s.seller_id');
		if($condition !=''){
			$this->db->where('s.seller_id',$condition);
		}
		$this->db->where('s.status','active');
		$this->db->where('u.group','seller');
		$this->db->order_by('s.id',$type);
		$this->db->limit($postnumbers,$offset);
		return $this->db->get();
	}
	/*******to get all shop list***/
	public function get_total_shop($condition=''){
		$this->db->select('s.id');
		$this->db->from(SELLER.' as s');
		$this->db->join(USERS.' as u','u.id=s.seller_id');
		if($condition !=''){
			$this->db->where('s.seller_id',$condition);
		}
		$this->db->where('s.status','active');
		$this->db->where('u.group','seller');
		return $this->db->get();
	}
	/*******to get shop product count ***/
	public function get_shops_productCount($sid=''){
		$this->db->select('COUNT(id) as pCount');
		$this->db->from(PRODUCT.' as p');
		$this->db->where('user_id',$sid);
		$this->db->where('status','Publish');
		$this->db->where('pay_status','Paid');
		$this->db->group_by('user_id');
		return $this->db->get();
	}
	/*******to get activity count ***/
	public function get_activity($condition='',$postnumbers=20,$offset=0){
		$this->db->select('ua.*');
		$this->db->from(USER_ACTIVITY.' as ua');
		$this->db->where($condition);
		#$this->db->group_by('ua.activity_id');
		$this->db->order_by('ua.activity_time','DESC');
		$this->db->limit($postnumbers,$offset);
		$activityDetails= $this->db->get();
		return $activityDetails;
	}



	/*******to get all Activity ***/
	public function get_all_activity($condition=''){
		$this->db->select('ua.id,ua.activity_id');
		$this->db->from(USER_ACTIVITY.' as ua');
		$this->db->where($condition);
		$this->db->order_by('ua.activity_time','DESC');
		$activityDetails= $this->db->get();
		return $activityDetails;
	}
	/*******to get Feed Product***/
	public function get_feed_product($product_id=""){
		$this->db->select('p.id as productId,p.product_name as productName,p.base_price as productPrice,p.seourl as productUrl,p.image as productImage,s.seller_businessname as storeName');
		$this->db->from(PRODUCT.' as p');
		$this->db->join(SELLER.' as s' , 's.seller_id=p.user_id','left');
		$this->db->where('p.id',$product_id);
		$feedproductDetails= $this->db->get();
		return $feedproductDetails;
	}
	/*******to get Feed shop***/
	public function get_feed_shop($shop_id=""){
		$this->db->select('s.seller_id as sellerId,s.seller_businessname as shopName,s.seourl as shopUrl,u.user_name as sellerLink,u.full_name as sellerFirstName,u.last_name as sellerLastName,u.thumbnail as sellerImage,u.city as Location');
		$this->db->from(SELLER.' as s');
		$this->db->join(USERS.' as u' , 'u.id=s.seller_id');
		$this->db->where('s.seller_id',$shop_id);
		$feedShopDetails= $this->db->get();
		return $feedShopDetails;
	}
	/*******to get Feed user***/
	public function get_feed_user($user_id=""){
		$this->db->select('u.id as userId,u.user_name as userName,u.full_name as userFirstName,u.last_name as userLastName,u.thumbnail as userImage');
		$this->db->from(USER.' as u');
		$this->db->where('u.id',$user_id);
		$feeduserDetails= $this->db->get();
		return $feeduserDetails;
	}
	/*******to get favourite product list***/
	public function getFavoriteListProduct($userId='',$postnumbers=2000,$offset=0){
		$this->db->select('p.id as product_id,p.product_name,p.image as product_image,p.price,p.base_price,p.seourl as product_url,s.seller_businessname as shop_name,s.seourl as shop_url,s.seller_id as sellerId');
		$this->db->from(FAVORITE.' as f');
		$this->db->join(PRODUCT.' as p','p.id=f.p_id');
		$this->db->join(SELLER.' as s','s.seller_id=p.user_id','left');
		$this->db->where('f.user_id',$userId);
		$this->db->where('p.status','Publish');
		$this->db->where('p.pay_status','Paid');
		$this->db->where('s.status','Active');
		$this->db->where('f.p_id IS NOT NULL');
		$this->db->group_by('p.id');
		$this->db->order_by('f.time','DESC');

		$this->db->limit($postnumbers,$offset);
		$favListProduct= $this->db->get();
		return $favListProduct;
	}
	/*Get Recent Favorites */
	public function get_resent_favorite_list(){
		$this->db->select('f.*,u.city,u.id as sellerid,u.email as selleremail,u.full_name,u.last_name,u.country,u.user_name,u.thumbnail,u.feature_product,s.seller_businessname as shop_name,s.seller_banner as seller_banner,s.seourl as shop_seourl,s.shop_ratting,s.review_count');
		$this->db->from(FAVORITE.' as f');
		$this->db->join(USERS.' as u' , 'u.id = f.shop_id');
		$this->db->join(SELLER.' as s' , 'f.shop_id = s.seller_id');
		#$this->db->where('f.shop_id IS NOT NULL '.$this->session->userdata("geo_country_filter"));
		#$this->db->group_by('f.shop_id');
		$this->db->where('s.status','active');
		$this->db->order_by('f.time','DESC');
		#$this->db->limit(4,0);
		$resultVal= $this->db->get();

		#echo '<pre>'; print_r($resultVal->result_array()); die;
		#echo $this->db->last_query(); die;
		return $resultVal;
   }
	public function getFavoriteListProduct_count($userId=""){
		$Query = "select p.id from ".FAVORITE." f LEFT JOIN ".PRODUCT." p ON p.id=f.p_id LEFT JOIN ".SELLER." s ON s.seller_id=p.user_id  where f.user_id='".$userId."' and p.status='Publish' and p.pay_status ='Paid' and s.status ='Active' and f.p_id IS NOT NULL group by p.id";
		return $this->ExecuteQuery($Query);
	}
	/*******to get favourite shop list***/
	public function getFavoriteListShop($userId='',$postnumbers=2000,$offset=0){
		$this->db->select('s.seller_username,s.seller_firstname,s.seller_lastname,s.seller_id as shopId,s.seller_banner as seller_banner,s.seller_businessname as shop_name,s.seourl as shop_url,u.user_name as sellerLink,u.full_name as sellerFirstName,u.last_name as sellerLastName,u.thumbnail as sellerImage');
		$this->db->from(FAVORITE.' as f');
		$this->db->join(SELLER.' as s','s.seller_id=f.shop_id','left');
		$this->db->join(USERS.' as u','u.id=s.seller_id','left');
		$this->db->where('f.user_id',$userId);
		$this->db->where('s.status','Active');
		$this->db->where('f.shop_id IS NOT NULL');
		$this->db->order_by('f.time','DESC');
		$this->db->limit($postnumbers,$offset);
		$favListShop= $this->db->get();
		return $favListShop;
	}


		public function get_featured_product_details_mobile(){
		$this->db->select('p.*,p.seourl as product_seourl, u.thumbnail,u.user_name,u.full_name,s.seourl as shop_seourls,s.review_count,s.shop_ratting,s.seller_businessname');
		$this->db->from(PRODUCT.' as p');
		$this->db->join(USERS.' as u','p.user_id=u.id');
		$this->db->join(SELLER.' as s','p.user_id=s.seller_id');
		$this->db->where('p.status','Publish');
		$this->db->where('p.product_featured','Yes');
		$this->db->where('s.status','active');
		$this->db->order_by('p.id','desc');
		$this->db->limit(4);
		return $this->db->get();


	}

		public function get_featured_shop_details_mobile(){

		$this->db->select('s.*,u.id as user_newid,u.thumbnail,u.user_name,u.full_name');
		$this->db->from(SELLER.' as s');
		$this->db->join(USERS.' as u','s.seller_id=u.id');

		$this->db->where('s.status','active');
		$this->db->where('s.featured_shop','Yes');
		$this->db->order_by('s.id','desc');
		$this->db->limit(7);
		return $this->db->get();


	}

		public function getmaxfav_mobile()
	{

		$this->db->select('f.*,s.*,u.thumbnail,count(f.shop_id) as new_id');

	$this->db->from(FAVORITE.' as f');
		$this->db->join(USERS.' as u','f.shop_id=u.id');
		$this->db->join(SELLER.' as s','s.seller_id= f.shop_id');
		$this->db->where('f.favorite','Yes');
		$this->db->where('s.status','Active');
		$this->db->group_by('f.shop_id,');
		$this->db->order_by('new_id','desc');
		$this->db->limit(4);
		return $this->db->get();

	}

	public function  get_fav_product_details_mobile($user_id = '')
	{
	$this->db->select('*');
	$this->db->from(PRODUCT.' as p');
	$this->db->where('p.user_id',$user_id);
	$this->db->order_by('p.id','desc');
		$this->db->limit(4);
		 return $this->db->get();
		// print_r($this->db->last_query());die;
	}

	public function get_recent_product_details_mobile(){

		$this->db->select('p.*,p.seourl as product_seourl,u.thumbnail,u.user_name,u.full_name,s.seourl as shop_seourls,s.review_count,s.shop_ratting,s.seller_businessname');
		$this->db->from(PRODUCT.' as p');
		$this->db->join(USERS.' as u','p.user_id=u.id');
		$this->db->join(SELLER.' as s','p.user_id=s.seller_id');
		$this->db->where('p.status','Publish');
		$this->db->where('s.status','Active');
		$this->db->order_by('p.id','desc');
		$this->db->limit(4);
		return $this->db->get();


	}
	/*******to get Shops product list***/
	public function get_shopProducts($shop_id="",$postnumbers=2000,$offset=0){
		$this->db->select('p.id as productId,p.product_name as productName,p.seourl as productUrl,p.image as productImage');
		$this->db->from(PRODUCT.' as p');
		$this->db->where('p.user_id',$shop_id);
		$this->db->where('p.status','Publish');
		$this->db->where('p.pay_status','Paid');
		$this->db->order_by('p.created','DESC');
		$this->db->limit($postnumbers,$offset);
		$shopProducts= $this->db->get();
		return $shopProducts;
	}

	public function get_shopProducts_count($shop_id=""){
		$Query = "select count(p.id) as total from ".PRODUCT." as p where p.user_id='".$shop_id."' and p.status='Publish' and p.pay_status ='Paid'";
		return $this->ExecuteQuery($Query);
	}


	/*******to get seller id***/

	/*******to get product list to pay***/
	public function get_paymentProduct($userId='',$sellerId=''){
		$this->db->select('p.image,p.product_name,us.quantity');
		$this->db->from(PRODUCT.' as p');
		$this->db->join(USER_SHOPPING_CART.' as us','us.product_id=p.id');
		$this->db->where('us.user_id',$userId);
		$this->db->where('us.sell_id',$sellerId);
		return $this->db->get();
	}
	/******************************* SELLER APP MODELS STARTS FROM HERE***********************************/
	public function sellerAuthentication($email="",$password=""){
		$condition = '(u.email = \''.addslashes($email).'\' OR u.user_name = \''.addslashes($email).'\') AND u.password=\''.$password.'\' AND u.status=\'Active\'';
		$this->db->select('s.seller_id as sellerId,s.seourl as shopUrl,u.thumbnail as sellerImage,u.user_name as sellerName,s.shop_ratting starRating');
		$this->db->from(USERS.' as u');
		$this->db->join(SELLER.' as s','s.seller_id=u.id','left');
		$this->db->where($condition);
		#$this->db->where('s.status','Active');
		$sellerAuth= $this->db->get();
		return $sellerAuth;
	}
	/******************************* SELLER APP MODELS ENDS HERE***********************************/
	public function get_myshopactivity($condition='',$postnumbers=20,$offset=0){
		#$this->db->select('ua.*,u.*');
		$this->db->select('ua.*,u.id,u.user_name,u.followers_count,u.following_count,u.thumbnail');
		$this->db->from(USER_ACTIVITY.' as ua');
		$this->db->join(USERS.' as u' , 'ua.user_id = u.id');
		$this->db->where($condition);
		#$this->db->where('ua.activity_name','favorite item');
		#$this->db->group_by('ua.activity_id');
		$this->db->order_by('ua.activity_time','desc');
		$this->db->limit($postnumbers,$offset);
		$shopactivityDetails= $this->db->get();
		#echo $this->db->last_query(); die;
		return $shopactivityDetails;
	}
	/*******to get shop order details***/
	public function view_shop_order_details($sell_id="",$shipstatus=""){
		$this->db->select('p.created,p.quantity,p.dealCodeNumber,p.shipping_status,p.total,u.id as userId,u.email,u.full_name,u.user_name,u.thumbnail as userImage,u.address,u.phone_no,u.postal_code,u.state,u.country,u.city,pd.image, pd.product_name,pd.id as PrdID,c.attr_name,count(p.product_id) as totalItems');
		$this->db->from(USER_PAYMENT.' as p');
		$this->db->join(USERS.' as u' , 'p.user_id = u.id');
		$this->db->join(PRODUCT.' as pd' , 'pd.id = p.product_id');
		$this->db->join(PRODUCT_ATTRIBUTE.' as c' , 'c.id = p.attribute_values','left');
		$this->db->where('p.status','Paid');
		$this->db->where('p.payment_type != "COD"');
		if($shipstatus=="Delivered"){
			$this->db->where('p.shipping_status ="'.$shipstatus.'"');
		}else{
			$this->db->where('p.shipping_status !="Delivered"');
		}
		$this->db->where('p.sell_id = "'.$sell_id.'"');
		$this->db->group_by("p.dealCodeNumber");
		$this->db->order_by("p.created", "desc");
		$PrdList = $this->db->get();

		return $PrdList;
	}

	public function view_shop_order_details_pagination($sell_id="",$shipstatus="",$postnumbers="",$offset=""){
		$this->db->select('p.*,u.id as userId,u.email,u.full_name,u.user_name,u.thumbnail as userImage,u.address,u.phone_no,u.postal_code,u.state,u.country,u.city,pd.image, pd.product_name,pd.id as PrdID,c.attr_name,count(p.product_id) as totalItems');
		$this->db->from(USER_PAYMENT.' as p');
		$this->db->join(USERS.' as u' , 'p.user_id = u.id');
		$this->db->join(PRODUCT.' as pd' , 'pd.id = p.product_id');
		$this->db->join(PRODUCT_ATTRIBUTE.' as c' , 'c.id = p.attribute_values','left');
		$this->db->where('p.status','Paid');
		$this->db->where('p.payment_type != "COD"');
		if($shipstatus=="Delivered"){
			$this->db->where('p.shipping_status ="'.$shipstatus.'"');
		}else{
			$this->db->where('p.shipping_status !="Delivered"');
		}
		$this->db->where('p.sell_id = "'.$sell_id.'"');
		$this->db->group_by("p.dealCodeNumber");
		$this->db->order_by("p.created", "desc");
		$this->db->limit($postnumbers,$offset);
		$PrdList = $this->db->get();
		#echo $this->db->last_query(); die();
		return $PrdList;
	}
	/*******to get total Order amount***/
	public function get_total_order_amount($sid='0'){
		$Query = "select sum(pr.sumTotal) as TotalAmt,sum(pr.Tax) as TotalTax, count(pr.sumTotal) as orders from (
			select p.dealCodeNumber, sum(p.sumtotal) as sumTotal,p.tax as Tax ,u.full_name from ".USERS." u
			JOIN ".USER_PAYMENT." p on p.sell_id=u.id
			where u.id='".$sid."' and p.status='Paid' and p.payment_type!='COD'  and p.shipping_status='Delivered'	 group by p.dealCodeNumber
			) pr";
		return $this->ExecuteQuery($Query);
	}
	/*******to get total paid details***/
	public function get_total_paid_details($sid='0'){
		$Query = "select sum(amount) as totalPaid from ".VENDOR_PAYMENT." where `status`='success' and `vendor_id`='".$sid."' group by `vendor_id`";
		return $this->ExecuteQuery($Query);
	}
	/*******to get Order Information***/
	public function get_order_info($sell_id="",$user_id="",$dealCodeNumber=""){
		$this->db->select('p.*,u.id as userId,u.email,u.user_name,u.thumbnail as userImage,s.id as sellerId,s.email as sellerEmail,s.user_name as sellerUserName,s.thumbnail as sellerImage,p.sell_id as shopId');
		$this->db->from(USER_PAYMENT.' as p');
		$this->db->join(USERS.' as u' , 'p.user_id = u.id');
		$this->db->join(USERS.' as s' , 'p.sell_id = s.id');
		$this->db->where('p.sell_id = "'.$sell_id.'"');
		$this->db->where('p.user_id = "'.$user_id.'"');
		$this->db->where('p.dealCodeNumber = "'.$dealCodeNumber.'"');
		$orderList = $this->db->get();
		return $orderList;
	}
	/*******to get product order***/
	public function view_order_product($id=""){
		$this->db->select('p.*,pd.product_name as productName,pd.image as productImage');
		$this->db->from(USER_PAYMENT.' as p');
		$this->db->join(PRODUCT.' as pd' , 'pd.id = p.product_id');
		$this->db->where('p.id = "'.$id.'"');
		$prdInfo = $this->db->get();
		return $prdInfo;
	}
	/*******to get product information ***/
	public function list_product_info($id=''){
		$this->db->select("p.id,p.image,p.base_price,p.product_name,s.seller_businessname as shop_name,s.seourl as shop_url,s.seller_id");
		$this->db->from(PRODUCT.' as p');
		$this->db->join(SELLER.' as s','s.seller_id=p.user_id','left');
		$this->db->where('p.id = "'.$id.'"');
		return $this->db->get();
	}
	/*******to get user order list***/
	public function view_user_order_details($user_id="",$status="Paid",$postnumbers=20,$offset=0){
		$this->db->select('p.*,u.thumbnail as sellerImage,s.seller_businessname as shopName,s.seller_id as sellerId,count(p.product_id) as totalItems,pd.image, pd.product_name,pd.id as PrdID');
		$this->db->from(USER_PAYMENT.' as p');
		$this->db->join(PRODUCT.' as pd' , 'pd.id = p.product_id');
		$this->db->join(SELLER.' as s' , 's.seller_id = p.sell_id');
		$this->db->join(USERS.' as u' , 'u.id = s.seller_id');
		$this->db->where('p.payment_type != "COD"');
		$this->db->where('p.status ="'.$status.'"');
		$this->db->where('p.user_id = "'.$user_id.'"');
		$this->db->group_by("p.dealCodeNumber");
		$this->db->order_by("p.created", "DESC");
		$this->db->limit($postnumbers,$offset);
		$PrdList = $this->db->get();
		return $PrdList;
	}
	/*******to get order status***/
	public function get_order_stats($condition=''){
		$this->db->select('up.id');
		$this->db->from(USER_PAYMENT.' as up');
		$this->db->where($condition);
		$this->db->group_by("up.dealCodeNumber");
		#$this->db->order_by('up.created','DESC');
		$orderStats= $this->db->get();
		return $orderStats;
	}
	/*******to get revenue Tax ***/
	public function get_revenue_tax_stats($sid="",$condition=""){
		$Query = "select sum(pr.sumTotal) as TotalAmt,sum(pr.Tax) as TotalTax, count(pr.sumTotal) as orders from (
			select p.dealCodeNumber, sum(p.sumtotal) as sumTotal,p.tax as Tax ,u.full_name from ".USERS." u
			JOIN ".USER_PAYMENT." p on p.sell_id=u.id
			where u.id='".$sid."' and p.status='Paid' and p.shipping_status='Delivered' AND ".$condition." group by p.dealCodeNumber
			) pr";
		return $this->ExecuteQuery($Query);
	}
	/*******to get discussion history***/
	public function get_discussion_history($orderId=""){
		$this->db->select('id,posted_by,posted_id,claim_id,post_message,image,post_time,posted_by');
		$this->db->from(ORDER_COMMENTS);
		$this->db->where('orderid = "'.$orderId.'"');
		$this->db->order_by("post_time", "desc");
		$disHistory = $this->db->get();
		return $disHistory;
	}
	public function get_discussion_history_count($orderId=""){
		$Query = "select count(id) as total from ".ORDER_COMMENTS." where orderid='".$orderId."' group by orderid ";
		return $this->ExecuteQuery($Query);
	}
	public function get_discussion_history_pagination($orderId="",$postnumbers="",$offset=""){
		$this->db->select('id,posted_by,posted_id,claim_id,post_message,image,post_time,posted_by');
		$this->db->from(ORDER_COMMENTS);
		$this->db->where('orderid = "'.$orderId.'"');
		$this->db->order_by("post_time", "asc");
		$this->db->limit($postnumbers,$offset);
		$disHistory = $this->db->get();
		return $disHistory;
	}
	/*******to get claim list***/
	public function get_claimList($sellerId="",$claimStatus){
		$this->db->select('c.seller_id,c.dealcodenumber,c.total_amount,u.id as buyerId,u.user_name buyerName,u.thumbnail');
		$this->db->from(ORDER_CLAIM.' as c');
		$this->db->join(USERS.' as u' , 'u.id = c.buyer_id','left');
		$this->db->where('c.seller_id = "'.$sellerId.'"');
		$this->db->where('c.status = "'.$claimStatus.'"');
		$claimList = $this->db->get();
		return $claimList;
	}
	public function get_claimList_pagination($sellerId="",$claimStatus,$postnumbers="",$offset=""){
		$this->db->select('c.seller_id,c.dealcodenumber,c.total_amount,u.id as buyerId,u.user_name buyerName,u.thumbnail');
		$this->db->from(ORDER_CLAIM.' as c');
		$this->db->join(USERS.' as u' , 'u.id = c.buyer_id','left');
		$this->db->where('c.seller_id = "'.$sellerId.'"');
		$this->db->where('c.status = "'.$claimStatus.'"');
		$this->db->limit($postnumbers,$offset);
		$claimList = $this->db->get();
		return $claimList;
	}
	/*******to get claim details***/
	public function getClaim($sellerId="",$claimStatus=''){
		$this->db->select('c.seller_id,c.dealcodenumber,c.total_amount,c.claimed_time,c.status,u.id as buyerId,u.user_name buyerName,u.thumbnail');
		$this->db->from(ORDER_CLAIM.' as c');
		$this->db->join(USERS.' as u' , 'u.id = c.buyer_id','left');
		$this->db->where('c.seller_id = "'.$sellerId.'"');
		$this->db->where('c.status = "Opened"');
		$this->db->order_by("c.claimed_time", "desc");
		$claimList = $this->db->get();
		return $claimList;
	}
	/*******to get order details***/
	public function get_order_details($dealCodeNumber){
		$this->db->select('p.dealCodeNumber,p.user_id as buyer_id,p.sell_id as seller_id,buyer.user_name as buyer_name,buyer.thumbnail as buyer_img,buyer.email as buyer_mail,seller.user_name as seller_name,seller.thumbnail as seller_img,seller.email as seller_mail');
		$this->db->from(USER_PAYMENT.' as p');
		$this->db->join(USERS.' as buyer' , 'buyer.id = p.user_id','left');
		$this->db->join(USERS.' as seller' , 'seller.id = p.sell_id','left');
		$this->db->where('p.dealCodeNumber = "'.$dealCodeNumber.'"');
		$this->db->group_by("p.dealCodeNumber");
		$orderInfo = $this->db->get();
		return $orderInfo;
	}
	/*******to get order***/
	public function getOrders($sell_id=""){
		$this->db->select('p.created,p.dealCodeNumber,p.total,p.sell_id as shopId,u.id as userId,u.user_name,u.thumbnail as userImage,count(p.product_id) as totalItems');
		$this->db->from(USER_PAYMENT.' as p');
		$this->db->join(USERS.' as u' , 'p.user_id = u.id');
		$this->db->where('p.status','Paid');
		$this->db->where('p.sell_id = "'.$sell_id.'"');
		$this->db->group_by("p.dealCodeNumber");
		$this->db->order_by("p.created", "desc");
		$orderList = $this->db->get();
		return $orderList;
	}
	/*******to get image size***/
	public function get_image_size($image_path=''){
		list($w, $h) = getimagesize($image_path);
		return $w.'--'.$h;
	}
	/*******to get Conversation list***/
	public function get_total_conversation_List($userId='',$type = '',$perpage='',$paginationVal=''){
		$this->db->select('id');
		$this->db->from(CONTACTPEOPLE);
		if($type == 'U'){
		$condition='(sender_id ='.$userId.' AND (sender_status ="Read" OR sender_status ="Unread"))';
		}else if($type == 'S'){
		$condition='(receiver_id ='.$userId.' AND (receiver_status ="Read" OR receiver_status ="Unread"))';
		}else{
		$condition='(sender_id ='.$userId.' AND (sender_status ="Read" OR sender_status ="Unread")) OR (receiver_id ='.$userId.' AND (receiver_status ="Read" OR receiver_status ="Unread"))';
		}

		$this->db->where($condition);
		$this->db->group_by('tid');
		$conversation= $this->db->get();
		return $conversation;
	}

		public function get_conversation_List($userId='',$ownerId = ''){
		$this->db->select('*');
		$this->db->from(CONTACTPEOPLE);
		$condition='(sender_id ='.$userId.' AND receiver_id ='.$ownerId.' AND (sender_status ="Read" OR sender_status ="Unread")) OR (receiver_id ='.$userId.' AND sender_id ='.$ownerId.' AND (receiver_status ="Read" OR receiver_status ="Unread"))';
		$this->db->where($condition);
		$this->db->group_by('tid');
		$this->db->limit(1);
		$conversation= $this->db->get();
		return $conversation;
	}
	/*******to get Conversation details list***/
	public function get_conversation_details_List($userId='',$perpage='',$paginationVal='',$type=''){
		$this->db->select('max(id) as recent_id');
		$this->db->from(CONTACTPEOPLE);

        if($type =='U'){
			$condition='(sender_id ='.$userId.' AND (sender_status ="Read" OR sender_status ="Unread")) ';
		}else if($type =='S'){
			$condition='(receiver_id ='.$userId.' AND (receiver_status ="Read" OR receiver_status ="Unread"))';
		}else{
			$condition='(sender_id ='.$userId.' AND (sender_status ="Read" OR sender_status ="Unread")) OR (receiver_id ='.$userId.' AND (receiver_status ="Read" OR receiver_status ="Unread"))';
		}

		$this->db->where($condition);
		$this->db->group_by('tid');
		if($perpage>=0 && $paginationVal>=0){
			$this->db->limit($perpage,$paginationVal);
		}
		$conversation= $this->db->get();
		$idList=''; $i=0;
		foreach($conversation->result() as $threads){
			if($i==0){
				$idList.="'".$threads->recent_id."'";
			} else {
				$idList.=",'".$threads->recent_id."'";
			}
			$i++;
		}
		$this->db->select('c.*,u1.user_name as sender_name,u1.email as sender_email,u1.thumbnail as senderthumbnail,s1.seller_businessname as sendershopname,s1.seourl as sendershopurl,u.user_name,u.email,u.full_name,u.thumbnail,s.seller_businessname as shopname,s.seourl as shopurl');
		$this->db->from(CONTACTPEOPLE.' as c');
		$this->db->join(USERS.' as u' , 'c.receiver_id = u.id','left');
		$this->db->join(USERS.' as u1' , 'c.sender_id = u1.id','left');
		$this->db->join(SELLER.' as s' , 's.seller_id = u.id','left','left');
		$this->db->join(SELLER.' as s1' , 's1.seller_id = u1.id','left','left');
		if($idList!=""){
		$condition='c.id IN('.$idList.')';
		}else{
		$condition='c.id IN(-1)';
		}
		$this->db->where($condition);
		$this->db->order_by('c.dataAdded','desc');
		$conversationlist= $this->db->get();
		return $conversationlist;
	}
	/*******to get Message details***/
	public function get_message_details($msgId='',$userId=''){
		$this->db->select('c.*,u1.user_name as sender_name,u1.email as sender_email,u1.thumbnail as senderthumbnail,u.user_name as receiver_name,u.email,u.full_name,u.thumbnail,s.seller_businessname as shopname,s.seourl as shopurl');
		$this->db->from(CONTACTPEOPLE.' as c');
		$this->db->join(USERS.' as u' , 'c.receiver_id = u.id','left');
		$this->db->join(USERS.' as u1' , 'c.sender_id = u1.id','left');
		$this->db->join(SELLER.' as s' , 's.seller_id = u.id','left');
		$this->db->where('c.tid ='.$msgId);
		$this->db->where('(c.sender_id ='.$userId.' OR c.receiver_id ='.$userId.')');
		$this->db->order_by('dataAdded','asc');
		$messageDetails= $this->db->get();
		return $messageDetails;
	}

	/*******to get shop feedback details***/
	public function get_shop_feed_details($shop_id='',$postnumbers='',$offset=''){
		$this->db->select('buyer.privacy,buyer.id as userId,buyer.user_name as userName,buyer.email as userEmail,buyer.thumbnail as thumbnail,buyer.full_name as fullname,buyer.last_name as last_name,p.id as productId,p.product_name,image as image,p.seourl as seo_url,feed.*');
		$this->db->from(PRODUCT.' as p');
		$this->db->join(PRODUCT_FEEDBACK.' as feed' , 'feed.seller_product_id=p.id','inner');
		$this->db->join(USERS.' as buyer' , 'buyer.id=feed.voter_id','inner');
		$this->db->where('feed.shop_id',$shop_id);
		$this->db->order_by('feed.id','desc');
		if($postnumbers !=''){
		$this->db->limit($postnumbers,$offset);
		}
		$feedback_query = $this->db->get();
		return $feedback_query;
	}

	public function get_shop_review_details($shop_id='',$perpage=-1,$paginationVal=-1){
		$this->db->select('buyer.id as userId,buyer.user_name as userName,buyer.thumbnail as thumbnail,buyer.full_name as fullname,buyer.last_name as last_name,p.id as productId,p.product_name,image as image,p.seourl as seo_url,feed.*');
		$this->db->from(PRODUCT.' as p');
		$this->db->join(PRODUCT_FEEDBACK.' as feed' , 'feed.seller_product_id=p.id','inner');
		$this->db->join(USERS.' as buyer' , 'buyer.id=feed.voter_id','buyer.status=Active','inner');
		$this->db->where('feed.shop_id',$shop_id);
		$this->db->where('feed.status','Active');
		$this->db->order_by('feed.id','desc');
		if($perpage>=0 && $paginationVal>=0){
			$this->db->limit($perpage,$paginationVal);
		}
		$feedback_query = $this->db->get();
		return $feedback_query;
	}
	/*******to get report details***/
	public function get_feed_report_details($id=''){
		$this->db->select('report.description,report.report_time');
		$this->db->from(REPORT_REVIEW.' as report' , 'report.review_id=feed.id','inner');
		$this->db->where('report.review_id',$id);
		$report_query = $this->db->get();
		return $report_query;
	}

	/*******to Insert & update* push key**/
	public function insertupdatePushKey($user_id='',$UDID='',$user_type='',$device_type=''){
		$this->db->select('gcm_buyer_id,gcm_seller_id,ios_device_id');
		$this->db->from(USERS);
		$this->db->where('id',$user_id);
		$userVals= $this->db->get();
		if($userVals->num_rows()>0){
			if($UDID==""){
				$UDID=NULL;
			}
			if($device_type=='android'){
				if($user_type=='user'){
					$sqlupdate = "UPDATE ".USERS." SET gcm_buyer_id=NULL WHERE gcm_buyer_id='".$UDID."'";
					$sqlinsert = "UPDATE ".USERS." SET gcm_buyer_id='".$UDID."' WHERE id=".$user_id."";
				}
				if($user_type=='seller'){
					$sqlupdate = "UPDATE ".USERS." SET gcm_seller_id=NULL WHERE gcm_seller_id='".$UDID."'";
					$sqlinsert = "UPDATE ".USERS." SET gcm_seller_id='".$UDID."' WHERE id=".$user_id."";
				}

				// changes on 11-08-2015
				$sqlupdate = "UPDATE ".USERS." SET gcm_buyer_id=NULL WHERE gcm_buyer_id='".$UDID."'";
				$sqlinsert = "UPDATE ".USERS." SET gcm_buyer_id='".$UDID."' WHERE id=".$user_id."";

			}else if($device_type=='ios'){
				$sqlupdate = "UPDATE ".USERS." SET ios_device_id=NULL WHERE ios_device_id='".$UDID."'";
				$sqlinsert = "UPDATE ".USERS." SET ios_device_id='".$UDID."' WHERE id=".$user_id."";
			}
			$this->ExecuteQuery($sqlupdate);
			$this->ExecuteQuery($sqlinsert);
		}
	}
		public function view_controller_details(){
		$this->db->select('*');
		$this->db->from(CONTROLMGMT);
		$ControlList = $this->db->get();
		return $ControlList;
	}

	public function view_product_details1($condition = ''){
		$select_qry = "select p.*,u.full_name,u.user_name,u.store_payment,u.thumbnail,sh.short_url,u.email as selleremail,u.id as sellerid,u.thumbnail,u.feature_product from ".PRODUCT." p
		LEFT JOIN ".USERS." u on (u.id=p.user_id)
		LEFT JOIN ".SHORTURL." sh on (sh.id=p.short_url_id)
		".$condition;
		$productList = $this->ExecuteQuery($select_qry);
		return $productList;
	}


	public function view_product_details($condition = ''){
		$select_qry = "select p.*,u.full_name,u.user_name,u.store_payment,u.thumbnail,sh.short_url,u.email as selleremail,u.id as sellerid,u.thumbnail,u.feature_product from ".PRODUCT." p
		LEFT JOIN ".USERS." u on (u.id=p.user_id)
		LEFT JOIN ".SHORTURL." sh on (sh.id=p.short_url_id)
		".$condition;
		$productList = $this->ExecuteQuery($select_qry);
		return $productList;
	}

	public function get_store_details($user_id){
		$this->db->select('*');
		$this->db->from(STORE_FRONT);
		$this->db->where('user_id',$user_id);
		$store_details = $this->db->get();
		return $store_details;
	}

		public function GetThisUserAddedProduct($user_id){
			$selectQuery = "Select * from ".PRODUCT." where user_id=".$user_id." and status='Publish' limit 10";
			$ThisUserAddedProd = $this->ExecuteQuery($selectQuery);
			return $ThisUserAddedProd;
		}

		public function view_product_comments_details($condition = ''){
		$select_qry = "select p.product_name,c.product_id,u.full_name,u.user_name,u.thumbnail,c.comments ,u.email,c.id,c.status,c.user_id as CUID
		from ".PRODUCT_COMMENTS." c
		LEFT JOIN ".USERS." u on u.id=c.user_id
		LEFT JOIN ".PRODUCT." p on p.seller_product_id=c.product_id ".$condition;
		$productComment = $this->ExecuteQuery($select_qry);
		return $productComment;

	}

	public function get_sellerId($seourl=''){
		$this->db->select('seller_id');
		$this->db->from(SELLER);
		$this->db->where('seourl',$seourl);
		$feeduserDetails= $this->db->get();
		return $feeduserDetails->row()->seller_id;
	}

		public function get_recent_user_likes($uid='',$pid='',$limit='3',$sort='desc'){
		$condition = '';
		if ($pid!=''){
			$condition = ' and pl.product_id != "'.$pid.'" ';
		}
		$Query = 'select pl.*,u.user_name,u.full_name,u.thumbnail,p.product_name,p.id as PID,p.created,p.sale_price,p.image from '.PRODUCT_LIKES.' pl
					JOIN '.USERS.' u on u.id=pl.user_id
					JOIN '.PRODUCT.' p on p.seller_product_id=pl.product_id
					JOIN '.USERS.' u1 on u1.id=p.user_id and u1.group="Seller" and u1.status="Active"
					where pl.user_id = "'.$uid.'" '.$condition.' order by pl.id '.$sort.' limit '.$limit;
		return $this->ExecuteQuery($Query);
	}
		public function get_recent_like_users($pid='',$limit='10',$sort='desc'){
		$Query = 'select pl.*, p.product_name, p.likes, u.full_name, u.user_name,u.thumbnail from '.PRODUCT_LIKES.' pl
					JOIN '.PRODUCT.' p on p.seller_product_id=pl.product_id
					JOIN '.USERS.' u on u.id=pl.user_id and u.status="Active"
					where pl.product_id="'.$pid.'" order by pl.id '.$sort.' limit '.$limit;
		return $this->ExecuteQuery($Query);
	}

		public function product_feedback_view($seller_id){
		if ($seller_id == '')$seller_id=0;
		/*$select_qry = "select * from ".PRODUCT." where user_id='".$userid."'";
		 $attList = $this->ExecuteQuery($select_qry);
		 return  $attList->result_array();*/

		$this->db->select(array(PRODUCT_FEEDBACK.'.*',USERS.'.full_name',USERS.'.user_name',USERS.'.thumbnail',PRODUCT.'.product_name',PRODUCT.'.image'));
		$this->db->from(PRODUCT_FEEDBACK);
		$this->db->join(USERS, USERS.'.id = '.PRODUCT_FEEDBACK.'.voter_id');
		$this->db->join(PRODUCT, PRODUCT.'.id = '.PRODUCT_FEEDBACK.'.product_id');
		//$this->db->from(PRODUCT_FEEDBACK);
		$this->db->where(array(PRODUCT_FEEDBACK.'.seller_id'=>$seller_id,PRODUCT_FEEDBACK.'.status'=>'Active'));
		//$this->db->limit(7);
		$query = $this->db->get();
		$result = $query->result_array();
		//echo $this->db->last_query(); die;
		return $result;

	}

		public function view_subproduct_details_join($prdId=''){
		$select_qry = "select a.*,b.attr_name as attr_type from ".SUBPRODUCT." a join ".PRODUCT_ATTRIBUTE." b on a.attr_id = b.id where a.product_id = '".$prdId."'";
		return $attList = $this->ExecuteQuery($select_qry);

	}

		public function searchShopyByCategory($whereCond) {
			$sel = 'select p.* , u.user_name from '.PRODUCT.' p
			LEFT JOIN '.USERS.' u on u.id=p.user_id
			'.$whereCond.' ';
			/*echo $sel;
			die;*/
		return $this->ExecuteQuery($sel);
	}

		public function getCategoryValues($selVal,$whereCond) {
		$sel = 'select '.$selVal.' from '.CATEGORY.' c LEFT JOIN '.CATEGORY.' sbc ON c.id = sbc.rootID '.$whereCond.' ';
		return $this->ExecuteQuery($sel);
	}
		public function view_notsell_product_details($condition = ''){
		$select_qry = "select p.*,sh.short_url,u.full_name,u.id as userid,u.user_name,u.thumbnail,u.feature_product from ".USER_PRODUCTS." p
		LEFT JOIN ".USERS." u on u.id=p.user_id
		LEFT JOIN ".SHORTURL." sh on sh.id=p.short_url_id
		".$condition;
		$productList = $this->ExecuteQuery($select_qry);
		return $productList;

	}
		public function get_products_by_category($categoryid='',$sort='desc'){
		$Query = "select p.*,u.user_name,u.full_name,u.thumbnail from ".PRODUCT." p
			LEFT JOIN ".USERS." u on u.id=p.user_id
			where p.status='Publish' and FIND_IN_SET('".$categoryid."',p.category_id) order by p.`created` ".$sort." limit 10";
		return $this->ExecuteQuery($Query);
	}
	public function get_activity_details($uid='0',$limit='5',$sort='desc'){#echo 'in';die;
    		$Query = 'select a.*,p.product_name,p.image,p.id as productID,up.product_name as user_product_name,up.image as user_product_image,up.web_link, u.full_name,u.thumbnail,u.user_name from '.USER_ACTIVITY.' a
    					LEFT JOIN '.PRODUCT.' p on a.activity_id=p.seller_product_id
    					LEFT JOIN '.USER_PRODUCTS.' up on a.activity_id=up.seller_product_id
    					LEFT JOIN '.USERS.' u on a.activity_id=u.id
    					where a.user_id='.$uid.' order by a.activity_time '.$sort.' limit '.$limit;
    		return $this->ExecuteQuery($Query);
    }


   public function get_wants_product($wantList){
		$productList = '';
		if ($wantList->num_rows() == 1){
			$productIds = array_filter(explode(',', $wantList->row()->product_id));
			$this->db->where_in('p.seller_product_id',$productIds);
			$this->db->where('p.status','Publish');
			$this->db->select('p.*,u.full_name,u.user_name,u.thumbnail,u.feature_product');
			$this->db->from(PRODUCT.' as p');
			$this->db->join(USERS.' as u','u.id=p.user_id');
			$productList = $this->db->get();
		}
		return $productList;
	}

		public function get_notsell_wants_product($wantList){
		$productList = '';
		if ($wantList->num_rows() == 1){
			$productIds = array_filter(explode(',', $wantList->row()->product_id));
			$this->db->where_in('p.seller_product_id',$productIds);
			$this->db->where('p.status','Publish');
			$this->db->select('p.*,u.full_name,u.user_name,u.thumbnail,u.feature_product');
			$this->db->from(USER_PRODUCTS.' as p');
			$this->db->join(USERS.' as u','u.id=p.user_id');
			$productList = $this->db->get();
		}
		return $productList;
	}

	public function get_userlike_products($uid='0',$limit='5'){
		$Query = "select pl.*,p.id as pid,p.product_name,p.image from ".PRODUCT_LIKES.' pl
					JOIN '.PRODUCT.' p on pl.product_id=p.seller_product_id
					where pl.user_id='.$uid.' limit '.$limit;
		return $this->ExecuteQuery($Query);
	}

	public function view_follow_list($id=''){
		$getfollow = $this->db->query("SELECT ".LISTS_DETAILS.".*, ".USERS.".`user_name` as pname FROM ".LISTS_DETAILS." JOIN ".USERS." ON ".USERS.".`id` = ".LISTS_DETAILS.".`user_id` WHERE  FIND_IN_SET($id,".LISTS_DETAILS.".followers)");
		return $getfollow;

	}

	public function get_orderpurchase_details($uid){
		// echo $uid;die;
		$Query = "select * from ".PAYMENT.' where (sell_id='.$uid.') or (user_id='.$uid.') order by created desc';
		return $this->ExecuteQuery($Query);
	}
	  public function get_active_sell_products($condition='',$fields=''){
		$Query = "select ".$fields." from ".PRODUCT." p
				LEFT JOIN ".USERS." u on (u.id=p.user_id) ".$condition;
		return $this->ExecuteQuery($Query);
	}

	public function get_latest_notifications($searchArr=array(),$activityArr=array(),$uid='0'){
		if (count($searchArr)>0){
			$activitySearch = 'and `activity` in (';
			$activitySearchStr = '';
			foreach ($searchArr as $searchRow){
				$activitySearchStr .= '\''.$searchRow.'\',';
			}
			$activitySearchStr = substr($activitySearchStr, 0, -1);
			$activitySearch .= $activitySearchStr;
			$activitySearch .= ')';
		}else {
			$activitySearch = '';
		}
		$activityIdStr = '';
		foreach ($activityArr as $activityRow){
			$activityIdStr .= "'".$activityRow."',";
		}
		$activityIdStr = substr($activityIdStr, 0,-1);
// 		$Query = 'select * from '.NOTIFICATIONS.' where `user_id` != "1'.$uid.'" '.$activitySearch.' and created > (now() - interval 5 day) order by created desc';
 		$Query = 'select * from '.NOTIFICATIONS.' where `user_id` != "'.$uid.'" '.$activitySearch.' and `activity_id` in ('.$activityIdStr.') order by id desc limit 1000';
		return $this->ExecuteQuery($Query);
	}


		public function get_groupgift_list($uid='0'){
		$Query = "select * from ".GROUP_GIFTS.' where user_id='.$uid.' order by created desc';
		return $this->ExecuteQuery($Query);
	}

		public function get_user_orders_list($uid='0'){
		//$Query = "select *, sum(sumtotal) as TotalPrice from ".PAYMENT.' where sell_id='.$uid.' and status="Paid" group by dealCodeNumber order by created desc';
		$Query = "select *, sum(sumtotal) as TotalPrice from ".PAYMENT.' where (sell_id='.$uid.' and status="Paid") or (sell_id='.$uid.' and status="Pending" and (payment_type ="Cash on Delivery" or payment_type ="Bank Tranfer")) group by dealCodeNumber order by created desc';
		return $this->ExecuteQuery($Query);
	}

		public function get_total_noncod_order_amount($sid='0'){
		$Query = "select sum(pr.sumTotal) as TotalAmt, count(pr.sumTotal) as orders from (
			select p.dealCodeNumber, sum(p.sumtotal) as sumTotal ,u.full_name from fc_users u
			JOIN fc_payment p on p.sell_id=u.id
			where u.id='".$sid."' and p.status='Paid'and p.payment_type != 'Cash on Delivery' group by p.dealCodeNumber
			) pr";
		return $this->ExecuteQuery($Query);
	}
		public function get_purchase_details($uid='0'){
   	 	//$Query = "select p.*,u.full_name from ".PAYMENT." p JOIN ".USERS." u on u.id=p.user_id where p.user_id='".$uid."' and p.status='Paid' group by p.dealCodeNumber order by created desc";
   		$Query = "select p.*,u.full_name from ".PAYMENT." p JOIN ".USERS." u on u.id=p.user_id where (p.user_id='".$uid."' and p.status='Paid') or(p.user_id='".$uid."' and p.status='Pending' and (p.payment_type ='Cash on Delivery')) group by p.dealCodeNumber order by created desc";
   	 	return $this->ExecuteQuery($Query);
   }


	public function get_purchase_details2($uid='0'){
		$Query = "select p.*,u.full_name,pro.product_name,pro.image from ".PAYMENT." p JOIN ".USERS." u on u.id=p.user_id JOIN ".PRODUCT." pro on p.product_id=pro.id where (p.user_id='".$uid."' and p.status='Paid') or(p.user_id='".$uid."' and p.status='Pending' and (p.payment_type ='Cash on Delivery')) group by p.dealCodeNumber order by created desc";
		return $this->ExecuteQuery($Query);
	}

	public function get_total_codorder_amount($sid='0'){
// 		$Query = "select sum(pr.sumTotal) as TotalAmt, count(pr.sumTotal) as orders from (
// 			select p.dealCodeNumber, sum(p.sumtotal) as sumTotal ,u.full_name from fc_users u
// 			JOIN fc_payment p on p.sell_id=u.id
// 			where u.id='".$sid."' and p.status='Paid' and p.payment_type = 'Cash on Delivery' group by p.dealCodeNumber
// 			) pr";

		$Query = "select sum(pr.sumTotal) as TotalAmt,sum(pr.price) as price, count(pr.sumTotal) as orders from (
			select p.dealCodeNumber, sum(p.sumtotal) as sumTotal , sum(p.price) as price,u.full_name from fc_users u
			JOIN fc_payment p on p.sell_id=u.id
			where u.id='".$sid."' and p.status='Paid' and p.payment_type = 'Cash on Delivery' group by p.dealCodeNumber
			) pr";

		return $this->ExecuteQuery($Query);
	}

		public function get_total_cod_amount($sid=''){
		$Query = "select sum(amount) as CODAmt, count(amount) as codrow from ".COD_PAYMENT." where seller_id ='".$sid."' and status = 'success'";
		return $this->ExecuteQuery($Query);
	}

		  public function get_featured_product($userId,$seller_plan_id){
            $Query = "SELECT * FROM ".FEATURED_PRODUCTS." WHERE userId =".$userId." AND sellerPlan_id = ".$seller_plan_id." ";
            $data = $this->ExecuteQuery($Query);
            if($data->num_rows > 0){
            $data = $data->result();
            //print_r(implode(',',unserialize($data[0]->featuredProducts)));
            $Query = "SELECT * FROM ".PRODUCT." WHERE seller_product_id IN (".implode(',',unserialize($data[0]->featuredProducts)).") ";
            $products = $this->ExecuteQuery($Query);
            }
            if($products->num_rows > 0){
            return $products->result();
            }else{
                return array();
            }
            //foreach(unserialize($data[0]->featuredProducts) as $product )
        }
        public function get_featured_sellers(){
            $datestring = "%Y-%m-%d %h:%i:%s";
            $time = time();
            $now = strtotime(mdate($datestring,$time));
            $Query = "SELECT id FROM ".FEATURED_SELLER." WHERE UNIX_TIMESTAMP(expiresOn) < '".$now."' AND isExpired = 0 ";
            $result = $this->ExecuteQuery($Query);
            if($result->num_rows > 0){
                foreach($result->result() as $id){
                    $expired_ids[] = $id->id;
                }
               $Query = "UPDATE ".FEATURED_SELLER." SET isExpired = 1 WHERE id IN (".implode(',',$expired_ids).") ";
               $this->ExecuteQuery($Query);
               $Query = "DELETE FROM ".FEATURED_PRODUCTS." WHERE sellerPlan_id IN (".implode(',',$expired_ids).") ";
               $this->ExecuteQuery($Query);
            }
            $Query = "SELECT u.* FROM ".USERS." u LEFT JOIN ".FEATURED_SELLER." s ON s.userId = u.id WHERE s.isExpired = 0  ";
            return $this->ExecuteQuery($Query);
        }

	public function getAttrubteValues($condition){
		$sel = 'select * from '.LIST_VALUES.' '.$condition.' ';
		return $this->ExecuteQuery($sel);
	}
		public function get_subscriptions_list($uid='0'){
		$Query = "select * from ".FANCYYBOX_USES.' where user_id='.$uid.' group by invoice_no order by created desc';
		return $this->ExecuteQuery($Query);
	}

	public function get_gift_cards_list($email=''){
		$Query = "select * from ".GIFTCARDS.' where recipient_mail=\''.$email.'\' order by created desc';
		return $this->ExecuteQuery($Query);
	}

	public function get_featured_products(){
            $datestring = "%Y-%m-%d %h:%i:%s";
            $time = time();
            $now = strtotime(mdate($datestring,$time));
            $Query = "SELECT id FROM ".FEATURED_SELLER." WHERE UNIX_TIMESTAMP(expiresOn) < '".$now."' AND isExpired = 0 ";
            $result = $this->ExecuteQuery($Query);
            if($result->num_rows > 0){
                foreach($result->result() as $id){
                    $expired_ids[] = $id->id;
                }
               $Query = "UPDATE ".FEATURED_SELLER." SET isExpired = 1 WHERE id IN (".implode(',',$expired_ids).") ";
               $this->ExecuteQuery($Query);
               $Query = "DELETE FROM ".FEATURED_PRODUCTS." WHERE sellerPlan_id IN (".implode(',',$expired_ids).") ";
               $this->ExecuteQuery($Query);
            }
            $Query = "SELECT * FROM ".FEATURED_PRODUCTS."";
            $productsId = $this->ExecuteQuery($Query);
            foreach($productsId->result() as $product){
                $Ids = unserialize($product->featuredProducts);
                foreach($Ids as $id){
                    $featuredIds[] = $id;
                }
            }
           if(count($featuredIds) > 0){
           $Query = "select p.*,u.full_name,u.user_name,sh.short_url,u.email as selleremail,u.id as sellerid,u.thumbnail,u.feature_product from ".PRODUCT." p
		LEFT JOIN ".USERS." u on u.id=p.user_id
		LEFT JOIN ".SHORTURL." sh on sh.id=p.short_url_id WHERE p.seller_product_id IN (".implode(',',$featuredIds).") ";
           return $this->ExecuteQuery($Query);
           }else{
               return array('num_rows'=>0);
           }
    }

		public function get_like_details_fully($uid='0'){
			 $Query = 'select p.*,u.full_name,u.user_name from '.PRODUCT_LIKES.' pl
						 JOIN '.PRODUCT.' p on pl.product_id=p.seller_product_id
						 LEFT JOIN '.USERS.' u on p.user_id=u.id
						 where pl.user_id='.$uid.' and p.status="Publish" order by pl.time desc';
			 return $this->ExecuteQuery($Query);
		}

		public function get_like_details_fully_user_products($uid='0'){
			 $Query = 'select p.*,u.full_name,u.user_name from '.PRODUCT_LIKES.' pl
						 JOIN '.USER_PRODUCTS.' p on pl.product_id=p.seller_product_id
						 LEFT JOIN '.USERS.' u on p.user_id=u.id
						 where pl.user_id='.$uid.' and p.status="Publish" order by pl.time desc';
			 return $this->ExecuteQuery($Query);
		}

		 public function get_list_details($tid='0',$uid='0'){
	   		$Query = 'select l.*,c.cat_name from '.LISTS_DETAILS.' l
	   					LEFT JOIN '.CATEGORY.' c on l.category_id=c.id
	   					where l.user_id='.$uid.' and l.product_id='.$tid.' or l.user_id='.$uid.' and l.product_id like "'.$tid.',%" or l.user_id='.$uid.' and l.product_id like "%,'.$tid.'" or l.user_id='.$uid.' and l.product_id like "%,'.$tid.',%"';
	   		return $this->ExecuteQuery($Query);
  		 }

        public function get_sorted_array_mobile($ar1 = array(), $ar2 = array(), $field = 'id', $type = 'asc') {
        	$products_list_arr = array();
        	if (count($ar1) > 0 && $ar1->num_rows() > 0) {
        		// foreach ($ar1->result() as $ar1_row) {
        		// 	$ar1_row->url = base_url().'json/product?pid='.$ar1_row->id;
        		// 	$products_list_arr['product'][] = $ar1_row;
        		// 	$products_list_arr[$field][] = $ar1_row->$field;
        		// }
				foreach ($ar1->result_array() as $ar1_row) {
					$ar1_row['type'] = 'SellerProduct';
        			$ar1_row['url'] = base_url().'json/product?pid='.$ar1_row['id'];
					$ar1_row['shareUrl'] = base_url().'things/'.$ar1_row['id'].'/'.$ar1_row['product_name'];
        			$products_list_arr['product'][] = $ar1_row;
        			$products_list_arr[$field][] = $ar1_row[$field];
        		}
        	}
        	if (count($ar2) > 0 && $ar2->num_rows() > 0) {
        		foreach ($ar2->result_array() as $ar2_row) {
					$ar2_row['type'] = 'UserProduct';
        			$ar2_row['url'] = $ar1_row['url'] = base_url().'json/userproduct?pid='.$ar2_row['seller_product_id'].'&uname='.$ar2_row['user_name'];
					$ar2_row['shareUrl'] =  base_url().'user/'.$ar2_row['user_name'].'/things/'.$ar2_row['seller_product_id'].'/'.$ar2_row['product_name'];
        			$products_list_arr['product'][] = $ar2_row;
        			$products_list_arr[$field][] = $ar2_row[$field];
        		}
        	}
        	if ($type == 'asc') {
        		$sort = SORT_ASC;
        	} else {
        		$sort = SORT_DESC;
        	}
        	if (is_array($products_list_arr[$field])) {
	        	array_multisort($products_list_arr[$field], $sort, $products_list_arr['product']
        	);
        	}
        	return $products_list_arr['product'];
        }

	public function check_product_id($pid=''){
		$checkId = $this->get_all_details(USER_PRODUCTS,array('seller_product_id'=>$pid));
		if ($checkId->num_rows()==0){
			$checkId = $this->get_all_details(PRODUCT,array('seller_product_id'=>$pid));
		}
		return $checkId;
	}

	public function add_user_product($uid='',$short_url=''){
		$returnStr = array();
		$seller_product_id = mktime();
		$checkId = $this->check_product_id($seller_product_id);
		while ($checkId->num_rows()>0){
			$seller_product_id = mktime();
			$checkId = $this->check_product_id($seller_product_id);
		}
		$url = base_url().'user/'.$this->data['userProfileDetails']->row()->user_name.'/things/'.$seller_product_id.'/'.url_title($this->input->post('name'),'-');
		$this->simple_insert(SHORTURL,array('short_url'=>$short_url,'long_url'=>$url));
		$urlid = $this->get_last_insert_id();
		$returnStr['pid'] = $seller_product_id;
		$returnStr['image'] = $image_name = $this->input->post('image');

	// 		if ($this->input->post('tag_url') && $this->input->post('photo_url')!=''){
	// 			/****----------Move image to server-------------****/
	// 			$image_url = trim(addslashes($this->input->post('photo_url')));
	// 			$image_url = str_replace(" ", '%20', $image_url);
	// //			$img_data = file_get_contents($image_url);
	// 			$ch = curl_init();
	// 			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	// 			curl_setopt($ch, CURLOPT_HEADER, false);
	// 			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	// 			curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.52 Safari/537.17');
	// 			curl_setopt($ch, CURLOPT_URL, $image_url);
	// 			curl_setopt($ch, CURLOPT_REFERER, $image_url);
	// 			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); //Set curl to return the data instead of printing it to the browser.
	// 			$img_data = curl_exec($ch);
	// 			curl_close($ch);
	// 			$img_full_name = substr($image_url, strrpos($image_url, '/')+1);
	// 			$img_name_arr = explode('.', $img_full_name);
	// 			$img_name = $img_name_arr[0];
	// 			$ext = $img_name_arr[1];
	// 			if ($ext == ''){
	// 				$ext = 'jpg';
	// 			}
	// 			$new_name = str_replace(array(',','&','<','>','$','(',')','?'), '', $img_name.mktime().'.'.$ext);
	// 			$new_img = 'images/product/'.$new_name;
	// 			file_put_contents($new_img, $img_data);
	// 			$returnStr['image'] = $new_name;
	// 			/****----------Move image to server-------------****/
	//
	// 			$image_name = $new_name;
	// 		}

		$dataArr = array(
									'product_name'	=>	$this->input->post('name'),
									'seourl'		=>	url_title($this->input->post('name'),'-'),
									'web_link'		=>	$this->input->post('link'),
									'category_id'	=>	$this->input->post('category'),
									'excerpt'		=>	$this->input->post('note'),
									'image'			=>	$image_name,
									'user_id'		=>	$uid,
									'seller_product_id' => $seller_product_id,
									'short_url_id'	=>	$urlid
								);

		$this->simple_insert(USER_PRODUCTS,$dataArr);
		return $returnStr;
	}
	public function get_products_search_results($search_key='',$limit='',$offset=''){
		$Query = 'select p.*, u.user_name from '.PRODUCT.' p	LEFT JOIN '.USERS.' u on u.id=p.user_id	where p.product_name like "%'.$search_key.'%" and p.status="Publish" and p.quantity>0 and u.status="Active" and u.group="Seller" or p.product_name like "%'.$search_key.'%" and p.status="Publish" and p.quantity>0 and p.user_id=0 limit '. $limit. ' offset '.$offset;
		return $this->ExecuteQuery($Query);
	}
	public function get_user_products_search_results($search_key='',$limit='',$offset=''){
		$Query = 'select p.*, u.user_name from '.USER_PRODUCTS.' p LEFT JOIN '.USERS.' u on u.id=p.user_id where p.product_name like "%'.$search_key.'%" and p.status="Publish" and u.status="Active"limit '. $limit. ' offset '.$offset;
		return $this->ExecuteQuery($Query);
	}
	public function get_user_search_results($search_key='',$limit='20',$offset=''){
		$Query = 'select * from '.USERS.' where full_name like "%'.$search_key.'%" and status="Active" OR user_name like "%'.$search_key.'%" and status="Active" limit '. $limit. ' offset '.$offset;
		return $this->ExecuteQuery($Query);
	}
	public function getCountryValues($condition){
		$sel = 'select * from '.COUNTRY_LIST.' '.$condition.' ';
		return $this->ExecuteQuery($sel);
	}
	public function getProductsInCart($userid){
		//$this->db->select('a.*,b.product_name,b.seller_product_id,b.shipping_cost,b.quantity as mqty,b.seourl,b.image,b.id as prdid,b.price as orgprice,b.ship_immediate,c.attr_name as attr_type,d.attr_name');
		$this->db->select('a.*,b.product_name,b.quantity as mqty,b.seourl,b.image,b.id as prdid,b.price as orgprice,b.ship_immediate, b.ship_duration, c.attr_name as attr_type,d.attr_name,d.attr_qty,b.country_code,u.user_name as sellerName');
		$this->db->from(SHOPPING_CART . ' as a');
		$this->db->join(PRODUCT . ' as b', 'b.id = a.product_id');
		$this->db->join(SUBPRODUCT . ' as d', 'd.pid = a.attribute_values', 'left');
		$this->db->join(PRODUCT_ATTRIBUTE . ' as c', 'c.id = d.attr_id', 'left');
		$this->db->join(USERS . ' as u', 'u.id = a.sell_id', 'left');
		$this->db->where('a.user_id = ' . $userid);
		$cartVal = $this->db->get();
		return $cartVal;
	}

	public function mani_cart_total($userid = '',$ShipId) {

        $giftRes = $this->cart_model->get_all_details(GIFTCARDS_TEMP, array('user_id' => $userid));
        $cartVal = $this->cart_model->get_all_details(SHOPPING_CART, array('user_id' => $userid));
        $SubcribRes = $this->cart_model->get_all_details(FANCYYBOX_TEMP, array('user_id' => $userid));
        $cartAmt = 0;
        $cartShippingAmt = 0;
        $cartTaxAmt = 0;
        $cartMiniMainCount = 0;
        if($cartVal->num_rows() > 0) {//echo 'in';die;
            foreach ($cartVal->result() as $CartRow){
                $cartAmt = $cartAmt + (($CartRow->product_shipping_cost + ($CartRow->product_tax_cost * 0.01 * $CartRow->price ) + $CartRow->price) * $CartRow->quantity);
                $cartMiniMainCount = $cartMiniMainCount + $CartRow->quantity;
            }
            //$cartSAmt = $cartVal->row()->shipping_cost;
            $cartTAmt = $cartAmt * 0.01 * $cartVal->row()->tax;
            //$grantAmt = $cartAmt + $cartSAmt + $cartTAmt;
			$grantAmt = $cartAmt + $cartTAmt;

        }
        $countVal = $giftRes->num_rows() + $SubcribRes->num_rows() + $cartMiniMainCount;
        $this->db->select('discountAmount');
        $this->db->where('user_id', $userid);
        $query = $this->db->get(SHOPPING_CART);

        if ($query->row()->discountAmount != '') {
            $grantAmt = $grantAmt - $query->row()->discountAmount;
        }

    //    return number_format($cartAmt, 2, '.', '') . '|' . number_format($cartSAmt, 2, '.', '') . '|' . number_format($cartTAmt, 2, '.', '') . '|' . number_format($grantAmt, 2, '.', '') . '|' . $countVal . '|' . number_format($query->row()->discountAmount, 2, '.', '');

		$details = array('cartAmt' => $cartAmt, 'cartSAmt'=>$cartSAmt, 'cartTAmt'=>$cartTAmt,'grantAmt'=>$grantAmt,'countVal'=>$countVal,'discountAmount'=>$query->row()->discountAmount);
		return $details;
    }

	// public function mani_cart_total($userid = '',$ShipId) {
    //     $giftRes = $this->cart_model->get_all_details(GIFTCARDS_TEMP, array('user_id' => $userid));
    //     $cartVal = $this->cart_model->get_all_details(SHOPPING_CART, array('user_id' => $userid));
    //     $SubcribRes = $this->cart_model->get_all_details(FANCYYBOX_TEMP, array('user_id' => $userid));
	// //	$getshipcountry = $this->cart_model->get_all_details(SHIPPING_ADDRESS, array('user_id' => $userid,'primary'=>'Yes'));
	//     $getshipcountry = $this->cart_model->get_all_details(SHIPPING_ADDRESS, array('id' => $ShipId));
	//
    //     $cartAmt = 0;
    //     $cartShippingAmt = 0;
    //     $cartTaxAmt = 0;
    //     $cartMiniMainCount = 0;
	//
    //     if ($cartVal->num_rows() > 0) {//echo 'in';die;
	//
    //         foreach ($cartVal->result() as $CartRow) {
    //             $cartAmt = $cartAmt + ((($CartRow->product_tax_cost * 0.01 * $CartRow->price ) + $CartRow->price) * $CartRow->quantity);
    //             $cartMiniMainCount = $cartMiniMainCount + $CartRow->quantity;
	//
	// 			$productDetails =  $this->cart_model->get_all_details(PRODUCT, array('id' => $CartRow->product_id));
    //             $getshipcost = $this->cart_model->get_all_details(SHIPPING_COST, array('product_id' => $productDetails->row()->seller_product_id,'country_code'=>$getshipcountry->row()->country));
	//
	//
    //             if($getshipcost->num_rows()>0){
 // 				   $cartShippingAmt = $cartShippingAmt + ($getshipcost->row()->separate_ship_cost * $CartRow->quantity);
 // 			   }else{
 // 				   $cartShippingAmt = $cartShippingAmt + ($productDetails->row()->shipping_cost * $CartRow->quantity);
 // 			   }
	//
    //         }
	//         $ShipCostVal = $this->cart_model->get_all_details(COUNTRY_LIST, array('country_code' => $getshipcountry->row()->country));
	//         $MainTaxCost = $ShipCostVal->row()->shipping_tax;
	// 		$cartSAmt = $cartShippingAmt;
	//         $cartTAmt = $cartAmt * 0.01 * $MainTaxCost;
	//         $grantAmt = $cartAmt + $cartSAmt + $cartTAmt;
    //     }
	//
    //     $countVal = $giftRes->num_rows() + $SubcribRes->num_rows() + $cartMiniMainCount;
    //     $this->db->select('discountAmount');
    //     $this->db->where('user_id', $userid);
    //     $query = $this->db->get(SHOPPING_CART);
    //     if ($query->row()->discountAmount != '') {
    //         $grantAmt = $grantAmt - $query->row()->discountAmount;
    //     }
    //      $details = array('cartAmt' => $cartAmt, 'cartSAmt'=>$cartSAmt, 'cartTAmt'=>$cartTAmt,'grantAmt'=>$grantAmt,'countVal'=>$countVal,'discountAmount'=>$query->row()->discountAmount);
	// 	 return $details;
    // }

	public function mani_gift_total($userid = '') {

        $giftRes = $this->mobile_model->get_all_details(GIFTCARDS_TEMP, array('user_id' => $userid));
        $giftAmt = 0;
        if ($giftRes->num_rows() > 0) {

            foreach ($giftRes->result() as $giftRow) {
                $giftAmt = $giftAmt + $giftRow->price_value;
            }
        }
        $SubcribRes = $this->mobile_model->get_all_details(FANCYYBOX_TEMP, array('user_id' => $userid));
        $cartVal = $this->mobile_model->get_all_details(SHOPPING_CART, array('user_id' => $userid));
        $countVal = $giftRes->num_rows() + $SubcribRes->num_rows() + $cartVal->num_rows();

        return number_format($giftAmt, 2, '.', '') . '|' . $countVal;
    }
	public function mani_subcribe_total($userid = '') {

        $SubcribRes = $this->mobile_model->get_all_details(FANCYYBOX_TEMP, array('user_id' => $userid));
        $SubcribAmt = 0;
        $SubcribSAmt = 0;
        $SubcribTAmt = 0;
        $SubcribTotalAmt = 0;
        if ($SubcribRes->num_rows() > 0) {

            foreach ($SubcribRes->result() as $SubscribRow) {
                $SubcribAmt = $SubcribAmt + $SubscribRow->price;
            }
            $SubcribSAmt = $SubcribRes->row()->shipping_cost;
            $SubcribTAmt = $SubcribRes->row()->tax;
            $SubcribTotalAmt = $SubcribAmt + $SubcribSAmt + $SubcribTAmt;
        }
        $giftRes = $this->mobile_model->get_all_details(GIFTCARDS_TEMP, array('user_id' => $userid));
        $cartVal = $this->mobile_model->get_all_details(SHOPPING_CART, array('user_id' => $userid));
        $countVal = $SubcribRes->num_rows() + $giftRes->num_rows() + $cartVal->num_rows();

        return number_format($SubcribAmt, 2, '.', '') . '|' . number_format($SubcribSAmt, 2, '.', '') . '|' . number_format($SubcribTAmt, 2, '.', '') . '|' . number_format($SubcribTotalAmt, 2, '.', '') . '|' . $countVal;
    }



	public function addPaymentCart($userid = '', $paymentType = '', $ship_products) {
		$productArr = array_filter(explode(',', $ship_products));
        $condition = "a.user_id = " . $userid . "";
        ///$this->db->select('a.*,p.product_name,p.seller_product_id,p.image,c.attr_name as attr_type,d.attr_name');
		$this->db->select('a.*,p.product_name,p.seller_product_id,p.shipping_cost as prodDefalutShipCost,p.image,c.attr_name as attr_type,d.attr_name');
        $this->db->from(SHOPPING_CART . ' as a');
        $this->db->join(PRODUCT . ' as p', 'p.id=a.product_id');
        $this->db->join(SUBPRODUCT . ' as d', 'd.pid = a.attribute_values', 'left');
        $this->db->join(PRODUCT_ATTRIBUTE . ' as c', 'c.id = d.attr_id', 'left');
        $this->db->where($condition);
		$this->db->where_in('a.product_id', $productArr);
        $AddPayt = $this->db->get();
        if ($this->session->userdata('randomNo') != '') {
            $delete = 'delete from ' . PAYMENT . ' where dealCodeNumber = "' . $this->session->userdata('randomNo') . '" and user_id = "' . $userid . '" ';
            $this->ExecuteQuery($delete, 'delete');
            $dealCodeNumber = $this->session->userdata('randomNo');
        } else {
            $dealCodeNumber = mt_rand();
        }

        $condition1 = 'user_id = "' . $userid . '" and id="' . $_GET['Ship_address_val'] . '"';
        $shippingAddr = $this->cart_model->get_all_details(SHIPPING_ADDRESS, $condition1);
        $insertIds = array();
        foreach ($AddPayt->result() as $result) {
			if(in_array($result->product_id, $productArr)){
				$this->cart_model->update_details(SHOPPING_CART, array('ship_product'=>'Yes'), array('product_id'=>$result->product_id));
			}
            if ($_GET['is_gift'] == '') {
                $ordergift = 0;
            } else {
                $ordergift = 1;
            }
            $attr_name = '';
            if ($result->attr_type != '' && $result->attr_name != '') {
                $attr_name = $result->attr_type . '/' . $result->attr_name;
            }
        //    $sumTotal = number_format((($result->price + $result->product_shipping_cost + ($result->product_tax_cost * 0.01 * $result->price)) * $result->quantity), 2, '.', '');
			$sumTotal = number_format((($result->product_tax_cost * 0.01 * $result->price) * $result->quantity), 2, '.', '');

			$cond = "select separate_ship_cost from ".SHIPPING_COST." where product_id=".$result->seller_product_id." && country_code='".$shippingAddr->row()->country."'";
			$separateShippingCost = $this->cart_model->ExecuteQuery($cond);
			$MainShipCost = 0;
			if(intval($separateShippingCost->row()->separate_ship_cost) != ""){
				$productShippingCost =  $result->quantity * (intval($separateShippingCost->row()->separate_ship_cost));
				$MainShipCost = $MainShipCost + $productShippingCost;
			}else{
				$productShippingCost =  $result->quantity * (intval($result->prodDefalutShipCost));
				$MainShipCost = $MainShipCost + $productShippingCost;
			}


            if ($result->product_type == 'physical') {
                $shipdata = 'shippingcountry = "' . $shippingAddr->country . '",
				shippingid = "' . $_GET['Ship_address_val'] . '",
				shippingstate = "' . $shippingAddr->state . '",
				shippingcity = "' . $shippingAddr->city . '",
				shippingcost = "' . $_GET['cart_ship_amount'] . '",
				tax = "' . $_GET['cart_tax_amount'] . '",
				product_shipping_cost = "'.$MainShipCost.'",
				product_tax_cost = "' . $result->product_tax_cost . '",';
            } else {
                $shipdata = '';
            }

            if ($result->product_type == 'digital') {
                $digital = 'product_download_code = "' . $dealCodeNumber . '@' . $result->seller_product_id . '",
				product_code_status = "Pending",
				product_type = "' . $result->product_type . '",';
            } else {
                $digital = '';
            }
            $insert = ' insert into ' . PAYMENT . ' set
								product_id = "' . $result->product_id . '",
								sell_id = "' . $result->sell_id . '",
								price = "' . $result->price . '",
								quantity = "' . $result->quantity . '",
								indtotal = "' . $result->indtotal . '",
								coupon_id  = "' . $result->couponID . '",
								discountAmount = "' .  $_GET['discount_Amt'] . '",
								couponCode  = "' . $result->couponCode . '",
								coupontype = "' . $result->coupontype . '",
								sumtotal = "' . $sumTotal . '",
								user_id = "' . $result->user_id . '",
								created = now(),
								dealCodeNumber = "' . $dealCodeNumber . '",
								status = "Pending",
								' . $shipdata . '
								payment_type = "' . $paymentType . '",
								attribute_values = "' . $result->attribute_values . '",
								shipping_status = "Pending",
								total  = "' . $_GET['cart_total_amount'] . '",
								note = "' . $_GET['note'] . '",
								order_gift = "' . $ordergift . '",
								old_product_name = "' . $result->product_name . '",
								old_attr_name = "' . $attr_name . '",
								old_image = "' . $result->image . '",
								' . $digital . '
								inserttime = "' . time() . '"';
            $insertIds[] = $this->cart_model->ExecuteQuery($insert, 'insert');
        }

        $paymtdata = array(
            'randomNo' => $dealCodeNumber,
            'randomIds' => $insertIds,
        );

        $this->session->set_userdata($paymtdata);

    //    return $insertIds;

	return $dealCodeNumber;
    }

	public function getPurchaseList($uid='0',$dealCode='0'){
		$this->db->select('p.*,u.email,u.full_name,u.address,u.phone_no,u.postal_code,u.state,u.country,u.city,pd.product_name,pd.id as PrdID,pd.image,pAr.attr_name as attr_type,sp.attr_name');
		$this->db->from(PAYMENT.' as p');
		$this->db->join(USERS.' as u' , 'p.user_id = u.id');
		$this->db->join(PRODUCT.' as pd' , 'pd.id = p.product_id','left');
		$this->db->join(SUBPRODUCT.' as sp' , 'sp.pid = p.attribute_values','left');
		$this->db->join(PRODUCT_ATTRIBUTE.' as pAr' , 'pAr.id = sp.attr_id','left');
		$this->db->where('p.user_id = "'.$uid.'" and p.dealCodeNumber="'.$dealCode.'"');
		return $this->db->get();
	}

	public function Check_Code_Val($Code = '', $amount = '', $shipamount = '', $userid = '') {

        $code = $Code;
        $amount = $amount;
        $amountOrg = $amount;
        $ship_amount = $shipamount;
        $CoupRes = $this->cart_model->get_all_details(COUPONCARDS, array('code' => $code, 'card_status' => 'not used'));
        $GiftRes = $this->cart_model->get_all_details(GIFTCARDS, array('code' => $code));
        $ShopArr = $this->cart_model->get_all_details(SHOPPING_CART, array('user_id' => $userid));
        $excludeArr = array('code', 'cartAmount', 'shipAmount','totalAmount','UserId','taxAmount');
        if ($CoupRes->num_rows() > 0) {
            $PayVal = $this->cart_model->get_all_details(PAYMENT, array('user_id' => $userid, 'coupon_id' => $CoupRes->row()->id, 'status' => 'Paid'));
            if ($PayVal->num_rows() == 0) {
                if ($ShopArr->row()->couponID == 0) {
                    if ($CoupRes->row()->quantity > $CoupRes->row()->purchase_count) {
                        $today = getdate();
                        $tomorrow = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
                        $currDate = date("Y-d-m", $tomorrow);
                        $couponExpDate = $CoupRes->row()->dateto;
                        $curVal = (strtotime($couponExpDate) < time());
                        if ($curVal != '') {
                            return '5';
                        }
                        if ($CoupRes->row()->coupon_type == "shipping") {
                            $totalamt = number_format($amount - $ship_amount, 2, '.', '');
                            $discount = '0';

                            $dataArr = array('discountAmount' => $discount,
                                'couponID' => $CoupRes->row()->id,
                                'couponCode' => $code,
                                'coupontype' => 'Free Shipping',
                                'is_coupon_used' => 'Yes',
                                'shipping_cost' => 0,
                                'total' => $totalamt);
                            $condition = array('user_id' => $userid);
                            $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
                            return 'Success|' . $CoupRes->row()->id . '|Shipping';
                        } elseif ($CoupRes->row()->coupon_type == "category") {
                            $newAmt = $amount;
                            $catAry = @explode(',', $CoupRes->row()->category_id);
                            foreach ($ShopArr->result() as $shopRow) {
                                $shopCatArr = '';
                                $shopCatArr = @explode(',', $shopRow->cate_id);
                                $combArr = array_merge($catAry, $shopCatArr);
                                $combArr1 = array_unique($combArr);
                                if (count($combArr) != count($combArr1)) {
                                    if ($CoupRes->row()->price_type == 2) {
                                        $percentage = $CoupRes->row()->price_value;
                                        $amountOrg = $shopRow->indtotal;
                                        $discount = ($percentage * 0.01) * $amountOrg;
                                        $IndAmt = number_format($amountOrg - $discount, 2, '.', '');
                                        $TotalAmt = $newAmt = number_format($newAmt - $discount, 2, '.', '');
                                        $dataArr = array('discountAmount' => $discount,
                                            'couponID' => $CoupRes->row()->id,
                                            'couponCode' => $code,
                                            'coupontype' => 'Category',
                                            'is_coupon_used' => 'Yes',
                                            'indtotal' => $IndAmt);
                                        $condition = array('id' => $shopRow->id);
                                        $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
                                        $dataArr1 = array('total' => $TotalAmt);
                                        $condition1 = array('user_id' => $userid);
                                        $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr1, $condition1);
                                    } elseif ($CoupRes->row()->price_type == 1) {
                                        $discount = $CoupRes->row()->price_value;
                                        $amountOrg = $shopRow->indtotal;
                                        if ($amountOrg > $discount) {
                                            $amountOrg = number_format($amountOrg - $discount, 2, '.', '');
                                            $TotalAmt = $newAmt = number_format($newAmt - $discount, 2, '.', '');
                                            $dataArr = array('discountAmount' => $discount,
                                                'couponID' => $CoupRes->row()->id,
                                                'couponCode' => $code,
                                                'coupontype' => 'Category',
                                                'is_coupon_used' => 'Yes',
                                                'indtotal' => $amountOrg);
                                            $condition = array('id' => $shopRow->id);
                                            $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
                                            $dataArr1 = array('total' => $TotalAmt);
                                            $condition1 = array('user_id' => $userid);
                                            $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr1, $condition1);
                                        } else {
                                            return '7';
                                        }
                                    }
                                }
                            }
                            return 'Success|' . $CoupRes->row()->id . '|Category';
                        } elseif ($CoupRes->row()->coupon_type == "product") {
                            $PrdArr = @explode(',', $CoupRes->row()->product_id);
                            $newAmt = $amount;
                            foreach ($ShopArr->result() as $shopRow) {
                                $shopPrd = $shopRow->product_id;
                                if (in_array($shopPrd, $PrdArr) == 1) {
                                    if ($CoupRes->row()->price_type == 2) {
                                        $percentage = $CoupRes->row()->price_value;
                                        $amountOrg = $shopRow->indtotal;
                                        $discount = ($percentage * 0.01) * $amountOrg;
                                        $IndAmt = number_format($amountOrg - $discount, 2, '.', '');
                                        $TotalAmt = $newAmt = number_format($newAmt - $discount, 2, '.', '');
                                        $dataArr = array('discountAmount' => $discount,
                                            'couponID' => $CoupRes->row()->id,
                                            'couponCode' => $code,
                                            'coupontype' => 'Product',
                                            'is_coupon_used' => 'Yes',
                                            'indtotal' => $IndAmt);
                                        $condition = array('id' => $shopRow->id);
                                        $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
                                        $dataArr1 = array('total' => $TotalAmt);
                                        $condition1 = array('user_id' => $userid);
                                        $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr1, $condition1);
                                    } elseif ($CoupRes->row()->price_type == 1) {
                                        $discount = $CoupRes->row()->price_value;
                                        $amountOrg = $shopRow->indtotal;
                                        if ($amountOrg > $discount) {
                                            $newDisAmt = number_format($amountOrg - $discount, 2, '.', '');
                                            $TotalAmt = $newAmt = number_format($newAmt - $discount, 2, '.', '');
                                            $dataArr = array('discountAmount' => $discount,
                                                'couponID' => $CoupRes->row()->id,
                                                'couponCode' => $code,
                                                'coupontype' => 'Product',
                                                'is_coupon_used' => 'Yes',
                                                'indtotal' => $newDisAmt);
                                            $condition = array('id' => $shopRow->id);
                                            $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
                                            $dataArr1 = array('total' => $TotalAmt);
                                            $condition1 = array('user_id' => $userid);
                                            $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr1, $condition1);
                                        } else {
                                            return '7';
                                        }
                                    }
                                }
                            }
                            return 'Success|' . $CoupRes->row()->id . '|Product';
                        } else {
                            if ($CoupRes->row()->price_type == 2) {
                                $percentage = $CoupRes->row()->price_value;
                                $discount = ($percentage * 0.01) * $amount;
                                $totalAmt = number_format($amount - $discount, 2, '.', '');
                                $dataArr = array('discountAmount' => $discount,
                                    'couponID' => $CoupRes->row()->id,
                                    'couponCode' => $code,
                                    'coupontype' => 'Cart',
                                    'is_coupon_used' => 'Yes',
                                    'total' => $totalAmt);
                                $condition = array('user_id' => $userid);
                                $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
                                return 'Success|' . $CoupRes->row()->id;
                            } elseif ($CoupRes->row()->price_type == 1) {
                                $discount = $CoupRes->row()->price_value;
                                if ($amount > $discount) {
                                    $amountOrg = number_format($amount - $discount, 2, '.', '');
                                    $dataArr = array('discountAmount' => $discount,
                                        'couponID' => $CoupRes->row()->id,
                                        'couponCode' => $code,
                                        'coupontype' => 'Cart',
                                        'is_coupon_used' => 'Yes',
                                        'total' => $amountOrg);
                                    $condition = array('user_id' => $userid);
                                    $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
                                    return 'Success|' . $CoupRes->row()->id;
                                } else {
                                    return '7';
                                }
                            }
                        }
                    } else {
                        return '6';
                    }
                } else {
                    return '2';
                }
            } else {
                return '2';
            }
        } elseif ($GiftRes->num_rows() > 0) {

            $curGiftVal = (strtotime($GiftRes->row()->expiry_date) < time());
            if ($curGiftVal != '') {
                return '8';
            }

            if ($GiftRes->row()->price_value > $GiftRes->row()->used_amount) {

                $NewGiftAmt = $GiftRes->row()->price_value - $GiftRes->row()->used_amount;
                if ($amount > $NewGiftAmt) {
                    $amountOrg = $amountOrg - $NewGiftAmt;
                    $dataArr = array('discountAmount' => $NewGiftAmt,
                        'couponID' => $GiftRes->row()->id,
                        'couponCode' => $code,
                        'coupontype' => 'Gift',
                        'is_coupon_used' => 'Yes',
                        'total' => $amountOrg);
                    $condition = array('user_id' => $userid);
                    $this->cart_model->update_details(SHOPPING_CART, $dataArr, $condition);
                } else {
                    $dataArr = array('discountAmount' => $amountOrg,
                        'couponID' => $GiftRes->row()->id,
                        'couponCode' => $code,
                        'coupontype' => 'Gift',
                        'is_coupon_used' => 'Yes',
                        'total' => '0');
                    $condition = array('user_id' => $userid);
                    $this->cart_model->update_details(SHOPPING_CART, $dataArr, $condition);
                }
                return 'Success|' . $GiftRes->row()->id . '|Gift';
            } else {
                return '2';
            }
        } else {
            return '1';
        }
    }

	// public function mani_cart_coupon_sucess($userid = '') {
    //     $cartVal = $this->cart_model->get_all_details(SHOPPING_CART, array('user_id' => $userid));
    //     $cartAmt = 0;
    //     $cartShippingAmt = 0;
    //     $cartTaxAmt = 0;
    //     if ($cartVal->num_rows() > 0) {
    //         $k = 0;
    //         foreach ($cartVal->result() as $CartRow) {
    //             $cartAmt = $cartAmt + (($CartRow->product_shipping_cost + ($CartRow->product_tax_cost * 0.01 * $CartRow->price ) + $CartRow->price) * $CartRow->quantity);
    //             $newCartInd[] = $CartRow->indtotal;
    //             $k = $k + 1;
    //         }
    //         $cartSAmt = $cartVal->row()->shipping_cost;
    //         $cartTAmt = $cartAmt * 0.01 * $cartVal->row()->tax;
    //         $grantAmt = $cartAmt + $cartSAmt + $cartTAmt;
    //     }
    //     $this->db->select('discountAmount');
    //     $this->db->from(SHOPPING_CART);
    //     $this->db->where('user_id = ' . $userid);
    //     $query = $this->db->get();
    //     $newAmtsValues = @implode('|', $newCartInd);
    //     if ($query->row()->discountAmount != '') {
    //         $grantAmt = $grantAmt - $query->row()->discountAmount;
    //     }
    //     return number_format($cartAmt, 2, '.', '') . '|' . number_format($cartSAmt, 2, '.', '') . '|' . number_format($cartTAmt, 2, '.', '') . '|' . number_format($grantAmt, 2, '.', '') . '|' . number_format($query->row()->discountAmount, 2, '.', '') . '|' . $k . '|' . $newAmtsValues;
    // }

	public function mani_cart_coupon_sucess($userid = '',$taxamount,$shipamount,$cartamount) {
        $cartVal = $this->cart_model->get_all_details(SHOPPING_CART, array('user_id' => $userid));
        $cartAmt = $cartamount;
        $cartSAmt = $shipamount;
        $cartTAmt = $taxamount;
        // if ($cartVal->num_rows() > 0) {
        //     $k = 0;
        //     foreach ($cartVal->result() as $CartRow) {
        //         //$cartAmt = $cartAmt + (($CartRow->product_shipping_cost + ($CartRow->product_tax_cost * 0.01 * $CartRow->price ) + $CartRow->price) * $CartRow->quantity);
        //         $cartAmt = $cartAmt + ((($CartRow->product_tax_cost * 0.01 * $CartRow->price ) + $CartRow->price) * $CartRow->quantity);
        //         $newCartInd[] = $CartRow->indtotal;
        //         $k = $k + 1;
        //     }
        //
        //     $cartSAmt = $cartVal->row()->shipping_cost;
        //     $cartTAmt = $cartAmt * 0.01 * $cartVal->row()->tax;
           $grantAmt = $cartAmt + $cartSAmt + $cartTAmt;
        // }
        $this->db->select('discountAmount');
        $this->db->from(SHOPPING_CART);
        $this->db->where('user_id = ' . $userid);
        $query = $this->db->get();
        //$newAmtsValues = @implode('|', $newCartInd);
        if ($query->row()->discountAmount != '') {
            $grantAmt = $grantAmt - $query->row()->discountAmount;
        }
        // return number_format($cartAmt, 2, '.', '') . '|' . number_format($cartSAmt, 2, '.', '') . '|' . number_format($cartTAmt, 2, '.', '') . '|' . number_format($grantAmt, 2, '.', '') . '|' . number_format($query->row()->discountAmount, 2, '.', '') . '|' . $k . '|' . $newAmtsValues;
        return number_format($cartAmt, 2, '.', '') . '|' . number_format($cartSAmt, 2, '.', '') . '|' . number_format($cartTAmt, 2, '.', '') . '|' .number_format($grantAmt, 2, '.', '') . '|' . number_format($query->row()->discountAmount, 2, '.', '') ;
    }


	// public function Check_Code_Val_Remove($userid = '') {
    //     $excludeArr = array('code','UserId');
    //     $dataArr = array('discountAmount' => 0,
    //         'couponID' => 0,
    //         'couponCode' => '',
    //         'coupontype' => '',
    //         'is_coupon_used' => 'No');
    //     $condition = array('user_id' => $userid);
    //     $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
    //     return;
    // }

	public function Check_Code_Val_Remove($userid = '') {
        $excludeArr = array('code','taxAmount','shipAmount','cartAmount','UserId','totalAmount');
        $dataArr = array('discountAmount' => 0,
            'couponID' => 0,
            'couponCode' => '',
            'coupontype' => '',
            'is_coupon_used' => 'No');
        $condition = array('user_id' => $userid);
        $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
        return;
    }
    public function sellerProductListValue($listValuesString=''){
		$Query = "SELECT * FROM ".LIST_VALUES." where id in (".$listValuesString.")";
		return	$list_Value = $this->product_model->ExecuteQuery($Query);
	}

	public function sellerProductShippingDetails($prdId=''){
		$select_qry = "select * from ".SUB_SHIPPING." where status = 'Active' and product_id = '".$prdId."' order by id asc";
		return $this->ExecuteQuery($select_qry);
	}

	public function productFeedback($seller_id){
		if ($seller_id == '')$seller_id=0;
		$this->db->select(array(PRODUCT_FEEDBACK.'.*',USERS.'.full_name',USERS.'.user_name',USERS.'.thumbnail',PRODUCT.'.product_name',PRODUCT.'.image'));
		$this->db->from(PRODUCT_FEEDBACK);
		$this->db->join(USERS, USERS.'.id = '.PRODUCT_FEEDBACK.'.voter_id');
		$this->db->join(PRODUCT, PRODUCT.'.id = '.PRODUCT_FEEDBACK.'.product_id');
		$this->db->where(array(PRODUCT_FEEDBACK.'.seller_id'=>$seller_id,PRODUCT_FEEDBACK.'.status'=>'Active'));
		$result = $this->db->get();
		return $result;
	}

}
