<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * This model contains all db functions related to user management
 * @author Teamtweaks
 *
 */
class User_model extends My_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	/**
    *
    * Getting Users details
    * @param String $condition
    */
   public function get_users_details($condition=''){
   		$Query = " select * from ".USERS." ".$condition;
   		return $this->ExecuteQuery($Query);
   }

   public function getTodayBrithdayList($condition=''){
		$Query = " select * from ".USERS." ".$condition;
   		return $this->ExecuteQuery($Query);
	}

   public function register_via_linkedin($username='',$fullname='',$email='',$api_id='',$thumbnail='',$brand='no'){

   $dataArr = array(
			'user_name'	=>	$username,
			'full_name'	=>	$fullname,
			'group'		=>	'User',
			'email'		=>	$email,
			'status'	=>	'Active',
			'is_brand'	=> $brand,
			'api_id'	=> $api_id,
			'thumbnail'	=> $thumbnail,
			'created'	=>	mdate($this->data['datestring'],time())
		);

		$this->simple_insert(USERS,$dataArr);

   }

   public function insertUserQuick($fullname='',$username='',$email='',$pwd='',$brand='no'){

    $api_id = $this->input->post('api_id');
    $thumbnail = $this->input->post('thumbnail');
	if($thumbnail != '')
		$thumbnail = $thumbnail;
	else
		$thumbnail = '';

	/* get Referal user id start */

	$getReferalUserId =$this->getReferalUserId();


	/* get Referal user id end */
   		$dataArr = array(
			'full_name'	=>	$fullname,
			'user_name'	=>	$username,
			'group'		=>	'User',
			'email'		=>	$email,
			'password'	=>	md5($pwd),
			'status'	=>	'Active',
			'is_verified'=>	'No',
   			'is_brand'	=> $brand,
			'api_id'	=> $api_id,
			'twitter_id'	=> $api_id,
			'thumbnail'	=> $thumbnail,
			'referId' => $getReferalUserId,
			'created'	=>	mdate($this->data['datestring'],time()),
   			'email_notifications'	=>	implode(',', $this->data['emailArr']),
	    	'notifications'			=>	implode(',', $this->data['notyArr']),
			'commision'=>$this->config->item('seller_commission')
		);

		$this->simple_insert(USERS,$dataArr);
		if($this->session->userdata('referenceName') != '')
		{
			$this->session->unset_userdata('referenceName');
		}

		include('fc_mail_chimp_settings.php');

		if($config['mailchimp_status'] = 'Yes'){
			$mailchimp = 1;
			include('./mailchimp/mailchimpapi.php');
		}


   }

   public function updateUserQuick($fullname='',$username='',$email='',$pwd=''){
   		$dataArr = array(
			'full_name'	=>	$fullname,
			'user_name'	=>	$username,
			'password'	=>	md5($pwd)
		);
		$conditionArr = array('email'=>$email);
		$this->update_details(USERS,$dataArr,$conditionArr);
   }


   public function updategiftcard($table='',$temp_id='',$user_id=''){
   		$dataArr = array('user_id'	=>	$user_id,);
		$conditionArr = array('user_id'=>$temp_id);
		$this->update_details($table,$dataArr,$conditionArr);
   }

   public function get_purchase_details($uid='0'){
   	 	//$Query = "select p.*,u.full_name from ".PAYMENT." p JOIN ".USERS." u on u.id=p.user_id where p.user_id='".$uid."' and p.status='Paid' group by p.dealCodeNumber order by created desc";
   		$Query = "select p.*,u.full_name from ".PAYMENT." p JOIN ".USERS." u on u.id=p.user_id where (p.user_id='".$uid."' and p.status='Paid') or (p.user_id='".$uid."' and p.status='Pending' and (p.payment_type ='Cash on Delivery')) or (p.user_id='".$uid."' and p.status='Pending' and (p.payment_type ='Paypal Authorize Payment')) or ( p.user_id='".$uid."' and p.status='Refund' and ( p.payment_type ='Paypal Authorize Payment') and p.do_capture ='Success') group by p.dealCodeNumber order by created desc";
   	 	return $this->ExecuteQuery($Query);
   }

   public function get_like_details_fully($uid='0'){
   		$Query = 'select p.*,u.full_name,u.user_name from '.PRODUCT_LIKES.' pl
   					JOIN '.PRODUCT.' p on pl.product_id=p.seller_product_id
   					LEFT JOIN '.USERS.' u on p.user_id=u.id
   					where pl.user_id='.$uid.' and p.status="Publish" order by pl.time desc';
   		return $this->ExecuteQuery($Query);
   }

   public function get_like_details_fully_user_products($uid='0'){
   		$Query = 'select p.*,u.full_name,u.user_name from '.PRODUCT_LIKES.' pl
   					JOIN '.USER_PRODUCTS.' p on pl.product_id=p.seller_product_id
   					LEFT JOIN '.USERS.' u on p.user_id=u.id
   					where pl.user_id='.$uid.' and p.status="Publish" order by pl.time desc';
   		return $this->ExecuteQuery($Query);
   }

   public function get_activity_details($uid='0',$limit='5',$sort='desc',$not_in=''){

   		if($not_in=='all'){
	   		$not_in = '';
   		}else{
	   		$not_in = ' and a.activity_name not in ("add_product")';
	   	}

   		$Query = 'select a.*,p.product_name,p.image,p.id as productID,up.product_name as user_product_name,up.image as user_product_image,up.web_link, u.full_name,u.thumbnail,u.user_name from '.USER_ACTIVITY.' a
   					LEFT JOIN '.PRODUCT.' p on a.activity_id=p.seller_product_id
   					LEFT JOIN '.USER_PRODUCTS.' up on a.activity_id=up.seller_product_id
   					LEFT JOIN '.USERS.' u on a.activity_id=u.id
   					where a.user_id='.$uid.' '.$not_in.' order by a.activity_time '.$sort.' limit '.$limit;
   		return $this->ExecuteQuery($Query);
   }

   public function get_list_details($tid='0',$uid='0'){
   		$Query = 'select l.*,c.cat_name from '.LISTS_DETAILS.' l
   					LEFT JOIN '.CATEGORY.' c on l.category_id=c.id
   					where l.user_id='.$uid.' and l.product_id='.$tid.' or l.user_id='.$uid.' and l.product_id like "'.$tid.',%" or l.user_id='.$uid.' and l.product_id like "%,'.$tid.'" or l.user_id='.$uid.' and l.product_id like "%,'.$tid.',%"';
   		return $this->ExecuteQuery($Query);
   }

   public function get_search_user_list($search_key='',$uid='0'){
   		$Query = 'select * from '.USERS.' where
   					`full_name` like "%'.$search_key.'%" and `id` != "'.$uid.'" and `status` = "Active"
   					or
   					`user_name` like "%'.$search_key.'%" and `id` != "'.$uid.'" and `status` = "Active"';
   		return $this->ExecuteQuery($Query);
   }
public function social_network_login_check($apiId=''){
	$twitterQuery = "select * from ".USERS." where twitter_id=".$apiId." AND status='Active'";
	$twitterQueryDetails  = mysql_query($twitterQuery);
	$twitterFetchDetails = mysql_fetch_row($twitterQueryDetails);
	return $social_check = mysql_num_rows($twitterQueryDetails);
}
public function get_social_login_details($apiId=''){
	$twitterQuery = "select * from ".USERS." where twitter_id=".$apiId." AND status='Active'";
	$twitterQueryDetails  = mysql_query($twitterQuery);
	return $twitterFetchDetails = mysql_fetch_assoc($twitterQueryDetails);
	//return $twitterCountById = mysql_num_rows($twitterQueryDetails);
}
public function googleLoginCheck($email=''){
  // echo $email;die;
   		$this->db->select('id');
		$this->db->from(USERS);
		$this->db->where('email',$email);
		$this->db->where('status','Active');
		$googleQuery = $this->db->get();
		return $googleResult = $googleQuery->num_rows();
}
public function google_user_login_details($email=''){
   		$this->db->select('*');
		$this->db->from(USERS);
		$this->db->where('email',$email);
		$this->db->where('status','Active');
		$googleQuery1 = $this->db->get();
		return $googleResult1 = $googleQuery1->row_array();
}

	public function getReferalUserId()
	{
		$referenceName = $this->session->userdata('referenceName');
		$referenceId = '';
		if($referenceName != '')
		{
			$this->db->select('id');
			$this->db->from(USERS);
			$this->db->where('user_name',$referenceName);
			$referQuery = $this->db->get();
			$referResult = $referQuery->row_array();

			if(!empty($referResult))
			{
				return $referenceId = $referResult['id'];
			}
			else
			{
				return $referenceId = '';
			}
		}
		else
		{
			return $referenceId = '';
		}
	}

	public function getReferalList($perpage='',$start='')
	{
		//echo $this->session->userdata('fc_session_user_id');die;
		$this->db->select('full_name,user_name,email,thumbnail');
		$this->db->from(USERS);
		$this->db->where('referId',$this->session->userdata('fc_session_user_id'));

		if($perpage !='')
		{
			$this->db->limit($perpage,$start);
		}


		$this->db->order_by('id','desc');
		$referQuery = $this->db->get();
		return $referResult = $referQuery->result_array();
	}

	public function get_userlike_products($uid='0',$limit='5'){
		$Query = "select pl.*,p.id as pid,p.product_name,p.image from ".PRODUCT_LIKES.' pl
					JOIN '.PRODUCT.' p on pl.product_id=p.seller_product_id
					where pl.user_id='.$uid.' limit '.$limit;
		return $this->ExecuteQuery($Query);
	}

	public function get_user_orders_list($uid='0'){
		//$Query = "select py.*,p.product_name,p.image, sum(py.sumtotal) as TotalPrice  from ".PAYMENT.' py JOIN '.PRODUCT.' p on py.user_id=p.user_id where (py.sell_id='.$uid.' and py.status="Paid") or (py.sell_id='.$uid.' and py.status="Pending" and (py.payment_type ="Cash on Delivery" or py.payment_type ="Bank Tranfer")) group by py.dealCodeNumber order by py.created desc';
		$Query = "select *, sum(sumtotal) as TotalPrice from ".PAYMENT.' where (sell_id='.$uid.' and status="Paid") or (sell_id='.$uid.' and status="Pending" and (payment_type ="Cash on Delivery" or payment_type ="Bank Tranfer")) or ( sell_id='.$uid.' and status="Pending" and ( payment_type ="Paypal Authorize Payment") ) or ( sell_id='.$uid.' and status="Refund" and ( payment_type ="Paypal Authorize Payment") and do_capture ="Success" ) group by dealCodeNumber order by created desc';
		
		return $this->ExecuteQuery($Query);
	}

	public function get_subscriptions_list($uid='0'){
		$Query = "select * from ".FANCYYBOX_USES.' where user_id='.$uid.' group by invoice_no order by created desc';
		return $this->ExecuteQuery($Query);
	}

	public function get_gift_cards_list($email=''){
		$Query = "select * from ".GIFTCARDS.' where recipient_mail=\''.$email.'\' order by created desc';
		return $this->ExecuteQuery($Query);
	}

	public function get_purchase_list($uid='0',$dealCode='0'){
		$this->db->select('p.*,u.email,u.full_name,u.address,u.phone_no,u.postal_code,u.state,u.country,u.city,pd.product_name,pd.id as PrdID,pd.image,pAr.attr_name as attr_type,sp.attr_name');
		$this->db->from(PAYMENT.' as p');
		$this->db->join(USERS.' as u' , 'p.user_id = u.id');
		$this->db->join(PRODUCT.' as pd' , 'pd.id = p.product_id','left');
		$this->db->join(SUBPRODUCT.' as sp' , 'sp.pid = p.attribute_values','left');
		$this->db->join(PRODUCT_ATTRIBUTE.' as pAr' , 'pAr.id = sp.attr_id','left');
		$this->db->where('p.user_id = "'.$uid.'" and p.dealCodeNumber="'.$dealCode.'"');
		return $this->db->get();
	}



	public function get_order_list($uid='0',$dealCode='0'){
		$this->db->select('p.*,u.email,u.full_name,u.address,u.phone_no,u.postal_code,u.state,u.country,u.city,pd.product_name,pd.id as PrdID,pd.image,pAr.attr_name as attr_type,sp.attr_name');
		$this->db->from(PAYMENT.' as p');
		$this->db->join(USERS.' as u' , 'p.user_id = u.id');
		$this->db->join(PRODUCT.' as pd' , 'pd.id = p.product_id','left');
		$this->db->join(SUBPRODUCT.' as sp' , 'sp.pid = p.attribute_values','left');
		$this->db->join(PRODUCT_ATTRIBUTE.' as pAr' , 'pAr.id = sp.attr_id','left');
		$this->db->where('p.sell_id = "'.$uid.'" and p.dealCodeNumber="'.$dealCode.'"');
		return $this->db->get();
	}
	public function get_user_details_by_id($id=''){
		$Query = 'select * from '.USERS.' where id = '.$id.'';
		return $this->ExecuteQuery($Query);
	}
	public function get_new_notifications($uid=0){
		if($uid>0){
			$notifications = array_filter(explode(',', $this->data['userDetails']->row()->notifications));
			$searchArr = array();
			if (in_array('wmn-follow', $notifications)){
				array_push($searchArr, 'follow');
			}
			if (in_array('wmn-comments_on_fancyd', $notifications)){
				array_push($searchArr, 'comment');
			}
			if (in_array('wmn-fancyd', $notifications)){
				array_push($searchArr, 'like');
			}
			if (in_array('wmn-featured', $notifications)){
				array_push($searchArr, 'featured');
			}
			if (in_array('wmn-comments', $notifications)){
				array_push($searchArr, 'own-product-comment');
			}
 			if (in_array('wmn-reply-comments', $notifications)){
 				array_push($searchArr, 'replied-comment');
 			}
 			if (in_array('wmn-tagged-comments', $notifications)){
 				array_push($searchArr, 'tagged-comment');
 			}


			//Notification for tag name
			array_push($searchArr, 'tagged');
			//////////////////////////

			$likedProducts = $this->get_all_details(PRODUCT_LIKES,array('user_id'=>$uid));
			$likedProductsIdArr = array();
			if($likedProducts->num_rows()>0){
				foreach ($likedProducts->result() as $likeProdRow){
					array_push($likedProductsIdArr, $likeProdRow->product_id);
				}
				array_filter($likedProductsIdArr);
			}

			$addedSellProducts = $this->get_all_details(PRODUCT,array('user_id'=>$uid,'status'=>'Publish'));
			$addedUserProducts = $this->get_all_details(USER_PRODUCTS,array('user_id'=>$uid,'status'=>'Publish'));
			$addedSellProductsArr = array();
			$addedUserProductsArr = array();
			$addedProductsArr = array();
			if($addedSellProducts->num_rows()>0){
				foreach ($addedSellProducts->result() as $addedSellProductsRow){
					array_push($addedSellProductsArr, $addedSellProductsRow->seller_product_id);
					array_push($addedProductsArr, $addedSellProductsRow->seller_product_id);
				}
			}
			if ($addedUserProducts->num_rows()>0){
				foreach ($addedUserProducts->result() as $addedUserProductsRow){
					array_push($addedUserProductsArr, $addedUserProductsRow->seller_product_id);
					array_push($addedProductsArr, $addedUserProductsRow->seller_product_id);
				}
			}
			$activityArr = array_merge($likedProductsIdArr,$addedProductsArr);
			array_push($activityArr, $uid);
			///////////

			if(count($searchArr)>0){
				$activitySearch = 'and `activity` in (';
				$activitySearchStr = '';
				foreach ($searchArr as $searchRow){
					$activitySearchStr .= '\''.$searchRow.'\',';
				}
				$activitySearchStr = substr($activitySearchStr, 0, -1);
				$activitySearch .= $activitySearchStr;
				$activitySearch .= ')';
			}else {
				$activitySearch = '';
			}

			$activityIdStr = '';
			foreach ($activityArr as $activityRow){
				$activityIdStr .= "'".$activityRow."',";
			}
			$activityIdStr = substr($activityIdStr, 0,-1);
			$Query = 'select * from '.NOTIFICATIONS.' where `user_id` != "'.$uid.'" '.$activitySearch.' and `activity_id` in ('.$activityIdStr.') and NOT FIND_IN_SET('.$uid.',`read`)';
			$data = $this->ExecuteQuery($Query);
			return $data;
		}else {
			return 0;
		}
	}
	/**   get_unread_messages_count end **/
	public function get_unread_messages_count($user_id){
		$this->db->select('*');
	    $this->db->from(BID_MESSAGE);
	    $condtion = "(msg_read = 'No' or rspn_read = 'NO')";
		$this->db->where('receiverId', $user_id);
	    $this->db->where('type !=','offer');
	   /*  $this->db->where('msg_read','No');
	    $this->db->where('rspn_read','NO'); */
	    $this->db->where($condtion);
	    $this->db->where('auction_FR_status !=','Expired');
        $this->db->group_by('BidId');
        $this->db->order_by('dateAdded','desc');
		$q = $this->db->get();
		// echo '<pre>';print_r($q->result());
	    $result = $q->num_rows();
		
		return $result;	
    }

	
}
