<?php
$this->load->view('admin/templates/header.php');
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6>Edit Attribute</h6>
					</div>
					<div class="widget_content">
						<?php 
							$attributes = array('class' => 'form_container left_label', 'id' => 'edituser_form', 'enctype' => 'multipart/form-data');
							echo form_open_multipart('admin/brand/EditBrand',$attributes) 
						?>
							<ul>
								<li>
									<div class="form_grid_12">
										<label class="field_title" for="brand_name">Brand Name <span class="req">*</span></label>
										<div class="form_input">
											<input name="brand_name" id="brand_name" type="text" tabindex="1" class="required large tipTop" title="Please enter the brand name" value="<?php echo $brand_details->row()->brand_name;?>"/>
										</div>
									</div>
								</li>
								
								<li>
									<div class="form_grid_12">
									<label class="field_title" for="attribute_name">Description<span class="req">*</span></label>
									<div class="form_input">
										<textarea style="margin-top: 0px;margin-bottom: 0px;height: 102px;" name="brand_description" class="required large tipTop" required title="Please enter the brand description"><?php echo $brand_details->row()->brand_description;?></textarea>
									</div>
									</div>
								</li>
								<?php if( $brand_details->row()->brand_logo != ''){ ?>
								<li>
									<div class="form_grid_12">
										<label class="field_title" for="attribute_name"></label>
											<img style="width: 17%; margin: 15%;margin-top: 2%;margin-bottom: 1%;" src="images/brand/<?php echo $brand_details->row()->brand_logo;?>">
									</div>
								</li>
								<?php } ?>
								<li>
									<div class="form_grid_12">
										<label class="field_title" for="attribute_name">LOGO<span class="req">*</span></label>
										<div class="form_input">
											<input name="brand_logo" id="brand_logo" type="file" tabindex="1" class=" large tipTop" title="Please enter the brand logo"/>
										</div>
									</div>
								</li>
								
								<li>
									<div class="form_grid_12">
										<div class="form_input">
											<input type="hidden" name="brand_id" value="<?php echo $brand_details->row()->id;?>"/>                                    
											<button type="submit" class="btn_small btn_blue" tabindex="4"><span>Update</span></button>
										</div>
									</div>
								</li>
							</ul>
						</form>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('admin/templates/footer.php');
?>