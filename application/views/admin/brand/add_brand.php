<?php
$this->load->view('admin/templates/header.php');
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6>Add New Brand</h6>
                        
					</div>
					<div class="widget_content">
					<?php 
						$brand = array('class' => 'form_container left_label', 'id' => 'addattribute_form', 'enctype' => 'multipart/form-data');
						echo form_open_multipart('admin/brand/insertBrand',$brand) 
					?>
                    
							<ul>
								<li>
									<div class="form_grid_12">
									<label class="field_title" for="attribute_name">Brand Name<span class="req">*</span></label>
									<div class="form_input">
										<input name="brand_name" id="brand_name" type="text" tabindex="1" class="required large tipTop" required title="Please enter the brand name"/>
									</div>
									</div>
								</li>
								
								<li>
									<div class="form_grid_12">
									<label class="field_title" for="attribute_name">Description<span class="req">*</span></label>
									<div class="form_input">
										<textarea style="margin-top: 0px;margin-bottom: 0px;height: 102px;" name="brand_description" class="required large tipTop" required title="Please enter the brand description"></textarea>
									</div>
									</div>
								</li>
								
								<li>
									<div class="form_grid_12">
									<label class="field_title" for="attribute_name">LOGO<span class="req">*</span></label>
									<div class="form_input">
										<input name="brand_logo" id="brand_logo" type="file" tabindex="1" class="required large tipTop" required title="Please enter the brand logo"/>
									</div>
									</div>
								</li>
								
								<li>
									<div class="form_grid_12">
										<label class="field_title" for="admin_name">Status <span class="req">*</span></label>
										<div class="form_input">
											<div class="active_inactive">
												<input type="checkbox" tabindex="11" name="status" checked="checked" id="active_inactive_active" class="active_inactive"/>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="form_grid_12">
										<div class="form_input">
											<button type="submit" class="btn_small btn_blue" tabindex="9"><span>Submit</span></button>
										</div>
									</div>
								</li>
							</ul>
                    
						</form>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('admin/templates/footer.php');
?>