<?php
$this->load->view('admin/templates/header.php');

?>
<script type="text/javascript">
$(document).ready(function(){
	$('#tab2 input[type="checkbox"]').click(function(){
		var cat = $(this).parent().attr('class');
		var curCat = cat;
		var catPos = '';
		var added = '';
		var curPos = curCat.substring(3);
		var newspan = $(this).parent().prev();
		if($(this).is(':checked')){
			while(cat != 'cat1'){
				cat = newspan.attr('class');
				catPos = cat.substring(3);
				if(cat != curCat && catPos<curPos){
					if (jQuery.inArray(catPos, added.replace(/,\s+/g, ',').split(',')) >= 0) {
					    //Found it!
					}else{
						newspan.find('input[type="checkbox"]').attr('checked','checked');
						added += catPos+',';
					}
				}
				newspan = newspan.prev(); 
			}
		}else{
			var newspan = $(this).parent().next();
			if(newspan.get(0)){
				var cat = newspan.attr('class');
				var catPos = cat.substring(3);
			}
			while(newspan.get(0) && cat != curCat && catPos>curPos){
				newspan.find('input[type="checkbox"]').attr('checked',this.checked);	
				newspan = newspan.next(); 	
				cat = newspan.attr('class');
				catPos = cat.substring(3);
			}
		}
	});
});
</script>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6>Add Product</h6>
                        <div id="widget_tab">
              				<ul>
               					 <!--<li><a href="#tab1" class="active_tab">Content</a></li>
               					 <li><a href="#tab2">Category</a></li>
               					 <li><a href="#tab3">Images</a></li>
               					 <li><a href="#tab4">Lists</a></li>
               					 <li><a href="#tab5">SEO</a></li>-->
             				 </ul>
            			</div>
					</div>
					<div class="widget_content">
					<?php 
						$attributes = array('class' => 'form_container left_label', 'id' => 'addproduct_form', 'enctype' => 'multipart/form-data');
						echo form_open_multipart('admin/product/insertEditaffliatedProduct',$attributes) ;
?>					
                     <div id="tab1" class="vali">
						<ul>
	 							
							<li>
							<div class="form_grid_12">
							<label class="field_title" for="product_name">Product Name <span id="name" class="req">*</span></label>
							<div class="form_input">
								<input name="product_name" id="product_name"  type="text" tabindex="1" class="large tipTop required"  title="Please enter the product name"/>
							</div>
							</div>
							</li>
                                <li>
								<div class="form_grid_12">
								<label class="field_title" for="description">Web Site<span  class="req"></span></label>
								<div class="form_input">
								<input type="text" name="web_link" id="web_link" tabindex="9"  class="large tipTop" title="Please enter the product price" />
								</div>
								</div>
							</li>
                               <li>
								<div class="form_grid_12">
								<label class="field_title" for="description">Category<span class="req">*</span></label>
								<div class="form_input">
						<?php //	echo "<pre>";print_r($mainCategories->result());die; ?>
							     <select name="category_id" id="fancy-category">
									<option selected="" value="">select category</option>
									<?php foreach ($mainCategories->result() as $catRow){?>
									<option value="<?php echo $catRow->id;?>"><?php echo $catRow->cat_name;?></option>
									<?php }?>
								</select>
								</div>
								</div>
                                </li>
                                <li>
								<div class="form_grid_12">
									<label class="field_title required" for="product_image">Product Image</label>
									<div class="form_input">
										<input name="product_image" id="product_image" type="file" tabindex="7" class="larg" title="Please select product image"/><span id="img" class="input_instruction green"></span>
									</div>
								</div>
								</li>
                                 <li>
								<div class="form_grid_12">
									<label class="field_title" for="admin_name">Status <span class="req">*</span></label>
									<div class="form_input">
										<div class="publish_unpublish">
											<input type="checkbox" tabindex="11" name="status" id="publish_unpublish_publish" class="publish_unpublish"/>
										</div>
									</div>
								</div>
								</li>
                                <li>
						</div>
						</li>
								
								<div class="form_grid_12">
									<div class="form_input">
										<button type="submit" id="sub" class="btn_small btn_blue"  tabindex="9"><span>Submit</span></button>
									</div>
                    
								</div>
							</ul>
                     </div>
                     
						</form>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>

<script>
$(document).ready(function(){


	var i = 1;
	
	
	$('#add').click(function() { 
		$('<div style="float: left; margin: 12px 10px 10px; width:85%;" class="field">'+
				'<div class="image_text" style="float: left;margin: 5px;margin-right:50px;">'+
					'<span>List Name:</span>&nbsp;'+
					'<select name="attribute_name[]" onchange="javascript:loadListValues(this)" style="width:200px;color:gray;width:206px;" class="chzn-select">'+
						'<option value="">--Select--</option>'+
						<?php foreach ($atrributeValue->result() as $attrRow){ 
							if (strtolower($attrRow->attribute_name) != 'price'){
						?>
						'<option value="<?php echo $attrRow->id; ?>"><?php echo $attrRow->attribute_name; ?></option>'+
						<?php }} ?>
					 '</select>'+
				'</div>'+
				'<div class="attribute_box attrInput" style="float: left;margin: 5px;" >'+
					 '<span>List Value :</span>&nbsp;'+
					 '<select name="attribute_val[]" style="width:200px;color:gray;width:206px;" class="chzn-select">'+
					 '<option value="">--Select--</option>'+
					 '</select>'+
				'</div>'+
		'</div>').fadeIn('slow').appendTo('.inputs');
		i++;
	});
	
	$('#remove').click(function() {
		$('.field:last').remove();
	});
	
	$('#reset').click(function() {
		$('.field').remove();
		$('#add').show();
		i=0;
	
	
	});
	
	
});

</script>
<?php 
$this->load->view('admin/templates/footer.php');
?>