<?php
$this->load->view('admin/templates/header.php');
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6>Upload file here</h6>
					</div>
					<div class="widget_content">
						<form action="<?php echo base_url().'admin/product/bulk_affiliate_upload_action' ?>" method="post" enctype="multipart/form-data" class="form_container left_label">
						<ul>		
							<li>
							<div class="form_grid_12">
							<label class="field_title" for="product_name">Upload file<span class="req">*</span></label>
							<div class="form_input">
								<input name="product_name" id="product_name" type="file" tabindex="1" class="required large tipTop" title="Please enter the product name"/>
							</div>
							</div>
							</li>
						</ul>
						<ul>
							<li>
								<div class="form_grid_12">
										<div class="form_input">
											<button type="submit" class="btn_small btn_blue" tabindex="15"><span>Upload</span></button>
										</div>
								</div>
							</li>
						</ul>
<!-- 						<ul>
							<li>
								<div class="form_grid_12">

								</div>
							</li>
						</ul> -->
						</form>
					</div>
				</div>
			</div>
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6>Important Note:</h6>
					</div>
					<div class="widget_content">
            <div class="table_content"><table class="display display_tbl dataTable">
						<thead>
						<tr role="row"><th rowspan="1" colspan="1">
                            	Id
                            </th><th rowspan="1" colspan="1">
								Category name
							</th><th rowspan="1" colspan="1">
								Parent Name
							</th></tr>
						<tbody role="alert" aria-live="polite" aria-relevant="all">
                                                    <?php if($categories->num_rows > 0){ foreach($categories->result() as $category){ ?>
                                                    <tr class="odd">
							<td class="center tr_select  ">
							<?php echo $category->id; ?>
							</td>
                                                        <td class="center ">
							<?php echo $category->cat_name; ?>
                                                        </td>
							<td class="center ">
							<?php echo $category->parent_name; ?>
                                                        </td>
						</tr>
                                                    <?php } } ?>
                                                </tbody></table></div>
						<h2>Download Sample File Below:</h2>
						<a href="<?php echo base_url().'csv_sample/bulk.csv' ?>" target="_blank"><button style="margin-left:2%">Download</button></a>
					</div>
				</div>			
			</div>			
		</div>
</div>						
<?php 
$this->load->view('admin/templates/footer.php');
?>