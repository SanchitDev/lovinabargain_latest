<?php
$this->load->view('admin/templates/header.php');
extract($privileges);
?>
<div id="content">
		<div class="grid_container">
			<?php 
				$attributes = array('id' => 'display_form');
				echo form_open('admin/seller/change_feature_status_global',$attributes) 
			?>
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading?></h6>
						
						<div style="float: right;line-height:40px;padding:0px 10px;height:39px;">
						<?php 
						if ($allPrev == '1' || in_array('3', $seller)){
						?>
							<div class="btn_30_light" style="height: 29px;">
								<a href="admin/seller/add_feature_plan"  class="tipTop" original-title="Add new seller Plan"><span class="icon add_co"></span><span class="btn_link">Add New</span></a>
								<a href="javascript:void(0)" onclick="return checkBoxValidationAdmin('Delete','<?php echo $subAdminMail; ?>');" class="tipTop" title="Select any checkbox and click here to delete records"><span class="icon cross_co"></span><span class="btn_link">Delete</span></a>
							</div>
						<?php }?>
						</div>
					</div>
					<div class="widget_content">
						<table class="display display_tbl" id="sellerPlan_tbl">
						<thead>
						<tr>
							<th class="center">
								<input name="checkbox_id[]" type="checkbox" value="on" class="checkall">
							</th>
							<th class="tip_top" title="Click to sort">
								 Plan Name
							</th>
							<th class="tip_top" title="Click to sort">
								 Plan Price
							</th>
							<th class="tip_top" title="Click to sort">
								Days
							</th>
							<th class="tip_top" title="Click to sort">
								Products Promoted
							</th>
							<th class="tip_top" title="Click to sort">
								Status
							</th>
							<th>
								 Action
							</th>
						</tr>
						</thead>
						<tbody>
						<?php 
						if ($sellerPlan->num_rows() > 0){
							foreach ($sellerPlan->result() as $row){
						?>
						<tr>
							<td class="center tr_select ">
								<input name="checkbox_id[]" type="checkbox" value="<?php echo $row->id;?>">
							</td>
							<td class="center">
								<?php echo $row->plan_name;?>
							</td>
							<td class="center">
								<?php echo $row->plan_price;?>
							</td>
							<td class="center">
								<?php echo $row->plan_period;?>
							</td>
							<td class="center">
								<?php echo $row->plan_product_count;?>
							</td>
							<td class="center">
							<?php 
							if ($allPrev == '1' || in_array('2', $user)){
								$mode = ($row->status == 'Active')?'0':'1';
								if ($mode == '0'){
							?>
								<a title="Click to inactive" class="tip_top" href="javascript:confirm_status('admin/seller/change_sellerP_status/<?php echo $mode;?>/<?php echo $row->id;?>');"><span class="badge_style b_done"><?php echo $row->status;?></span></a>
							<?php
								}else {	
							?>
								<a title="Click to active" class="tip_top" href="javascript:confirm_status('admin/seller/change_sellerP_status/<?php echo $mode;?>/<?php echo $row->id;?>')"><span class="badge_style"><?php echo $row->status;?></span></a>
							<?php 
								}
							}else {
							?>
							<span class="badge_style b_done"><?php echo $row->status;?></span>
							<?php }?>
							
							
							
							</td>
							<td class="center">
							<?php if ($allPrev == '1' || in_array('2', $seller)){?>
								<span><a class="action-icons c-edit" href="admin/seller/edit_sellerPlan_form/<?php echo $row->id;?>" title="Edit">Edit</a></span>
							<?php }?>
								
							<?php if ($allPrev == '1' || in_array('3', $seller)){?>	
								<span><a class="action-icons c-delete" href="javascript:confirm_delete('admin/seller/delete_sellerPlan/<?php echo $row->id;?>')" title="Delete">Delete</a></span>
							<?php }?>
							</td>
						</tr>
						<?php 
							}
						}
						?>
						</tbody>
						<tfoot>
						<tr>
							<th class="center">
								<input name="checkbox_id[]" type="checkbox" value="on" class="checkall">
							</th>
							<th>
								 Plan Name
							</th>
							<th>
								 Plan Price
							</th>
							<th>
								Days
							</th>
							<th>
								Product Promoted
							</th>
							<th>
								Status
							</th>
							<th>
								 Action
							</th>
						</tr>
						</tfoot>
						</table>
					</div>
				</div>
			</div>
			<input type="hidden" name="statusMode" id="statusMode"/>
		</form>	
			
		</div>
		<span class="clear"></span>
	</div>
</div>
<style>
.btn_link {
margin-left: 5px;
height: 30px;
display: inline-block;
margin-right: 3px;
border-left: #bbb 1px solid;
padding-left: 6px;
box-shadow: 1px 0 1px rgba(255, 255, 255, 0.8) inset;
-moz-box-shadow: 1px 0 1px rgba(255, 255, 255, 0.8) inset;
-webkit-box-shadow: 1px 0 1px rgba(255, 255, 255, 0.8) inset;
}
 .icon {
height: 20px;
width: 20px;
margin-top: 5px;
background: url(../images/sprite-icons/icons-color.png) no-repeat;
}
</style>
<?php 
$this->load->view('admin/templates/footer.php');
?>