<?php
$this->load->view('admin/templates/header.php');
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6><?php echo $heading;?></h6>
					</div>
					<div class="widget_content">
					<?php 
						$attributes = array('class' => 'form_container left_label', 'id' => 'edituser_form', 'enctype' => 'multipart/form-data');
						echo form_open('admin/seller/update_store_details',$attributes) 
					?>
	 						<ul>
								<li>
								<div class="form_grid_12">
									<label class="field_title" for="store_name">Store Name</label>
									<div class="form_input">
										<input type="text" name="store_name" value="<?php echo $store_details->row()->store_name;?>" class="tipTop large" tabindex="1" title="Enter the store name" />
									</div>
								</div>
								</li>
								<li>
								<div class="form_grid_12">
									<label class="field_title" for="tagline">Tag Line</label>
									<div class="form_input">
										<input name="tagline" style=" width:295px" id="tagline" value="<?php echo $store_details->row()->tagline;?>" type="text" tabindex="2" class="required tipTop large" title="Please enter the store tagline"/>
									</div>
								</div>
								</li>
								<li>
								<div class="form_grid_12">
									<label class="field_title" for="logo_image">Logo</label>
									<div class="form_input">
										<input name="logo_image" style=" width:295px" id="logo_image" type="file" tabindex="3" class="required tipTop large" title="Please select store logo"/>
										<span class="label_intro green">Logo size 150x150 pixels</span>
									</div>
									<div class="form_input">
									<?php if ($store_details->row()->logo_image != ''){?>
									<img src="images/store/<?php echo $store_details->row()->logo_image;?>" width="150"/>
									<?php }?>
									</div>
								</div>
								</li>
								<li>
								<div class="form_grid_12">
									<label class="field_title" for="cover_image">Banner Image</label>
									<div class="form_input">
										<input type="file" name="cover_image" class="tipTop large" tabindex="4" title="Please select the cover image for store" />
										<span class="label_intro green">Banner size 930x240 pixels</span>
									</div>
									<div class="form_input">
									<?php if ($store_details->row()->cover_image != ''){?>
									<img src="images/store/<?php echo $store_details->row()->cover_image;?>" width="300"/>
									<?php }?>
									</div>
								</div>
								</li>
								<li>
								<div class="form_grid_12">
									<label class="field_title" for="description">About Store</label>
									<div class="form_input">
										<textarea name="description" class="tipTop" tabindex="5" title="Enter about store" ><?php echo $store_details->row()->description;?></textarea>
									</div>
								</div>
								</li>
								<li>
								<div class="form_grid_12">
									<label class="field_title" for="privacy">Terms and Conditions</label>
									<div class="form_input">
										<textarea name="privacy" class="tipTop large" tabindex="6" title="Enter the terms and conditions"><?php echo $store_details->row()->privacy;?></textarea>
									</div>
								</div>
								</li>
								<li>
								<div class="form_grid_12">
									<div class="form_input">
										<button type="submit" class="btn_small btn_blue" tabindex="7"><span>Update</span></button>
									</div>
								</div>
								</li>
							</ul>
						<input type="hidden" name="id" value="<?php echo $store_details->row()->id;?>"/>
						<input type="hidden" name="user_id" value="<?php echo $user_id;?>"/>
						</form>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('admin/templates/footer.php');
?>