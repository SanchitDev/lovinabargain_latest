<?php
$this->load->view('admin/templates/header.php');
extract($privileges);
?>
<div id="content">
		<div class="grid_container">
			<?php 
				$attributes = array('id' => 'display_form');
				echo form_open('admin/groupgift/change_groupgift_status_global',$attributes) 
			?>
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading?></h6>
					</div>
					<div class="widget_content">
						<table class="display display_tbl" id="groupgift_tbl">
						<thead>
						<tr>
							<th>
								ID
							</th>
							<th class="tip_top" title="Click to sort">
								 Groupgift Name
							</th>
							<th class="tip_top" title="Click to sort">
								 Total
							</th>
							<th class="tip_top" title="Click to sort">
								No of Contributors
							</th>
							<th class="tip_top" title="Click to sort">
								 Remaining Days
							</th>
							<th class="tip_top" title="Click to sort">
								Status
							</th>
							<th>
								 Action
							</th>
						</tr>
						</thead>
						<tbody>
						<?php if($groupgiftDetails !='' && $groupgiftDetails->num_rows()>0){
							foreach($groupgiftDetails->result() as $_groupgiftDetails){
						?>
						<tr>
							<td class="center">
								<?php echo $_groupgiftDetails->gift_seller_id;?>
							</td>
							<td class="center">
								<?php echo $_groupgiftDetails->gift_name;?>
							</td>
                            <td class="center">
								<?php echo $_groupgiftDetails->gift_total;?>
							</td>
							<td class="center">
								<?php echo $contributors_count[$_groupgiftDetails->id];?>
							</td>
							<td class="center">
							<?php 
								$db_date = $_groupgiftDetails->created;
								$db_date = strtotime($db_date);
								$current_date = strtotime(date('Y-m-d'));
								$expired_date = strtotime("+".$this->config->item('expiry_days')." day", $db_date);						
								$val = $expired_date - $current_date;
								$temp=$val/86400;
								$days = floor($temp);
								if($days <=0){
									echo "0";
								}else{
									echo $days;
								}
								?>	
							</td>
							<td class="center">
								<a class="tip_top"><span class="badge_style b_done"><?php echo $_groupgiftDetails->status;?></span></a>
							</td>
							<td class="center">
							<span><a class="action-icons c-suspend" href="admin/groupgift/view_contributors/<?php echo $_groupgiftDetails->gift_seller_id;?>" title="View Contributors">View Contributors</a></span>
							<?php if($days ==0){ ?>
							<span><a class="action-icons c-suspend" href="admin/groupgift/refund/<?php echo $_groupgiftDetails->gift_seller_id;?>" title="Refund">Refund</a></span>
							<?php }?>
							<?php if($_groupgiftDetails->gift_total == $contributors_amt[$_groupgiftDetails->id]){ ?>
							<span><a class="action-icons c-suspend" href="admin/groupgift/pay_to_seller/<?php echo $_groupgiftDetails->gift_seller_id;?>" title="Pay to Seller">Pay to Seller</a></span>
							<?php }?>
							</td>
						</tr>
						<?php }}?>
						</tbody>
						<tfoot>
						<tr>
							<th>
								 ID
							</th>
							<th>
								 Groupgift Name
							</th>
							<th>
								 Total
							</th>
							<th>
								No of Contributors
							</th>
							<th>
								Remaining Days
							</th>
							<th>
								Status
							</th>
							<th>
								 Action
							</th>
						</tr>
						</tfoot>
						</table>
					</div>
				</div>
			</div>
			<input type="hidden" name="statusMode" id="statusMode"/>
			<input type="hidden" name="SubAdminEmail" id="SubAdminEmail"/>
		</form>	
			
		</div>
		<span class="clear"></span>
	</div>
</div>
<script>
$('#groupgift_tbl').dataTable({   
		 "aoColumnDefs": [
							{ "bSortable": false, "aTargets": [ 4,5 ] }
						],
						"aaSorting": [[1, 'asc']],
		"sPaginationType": "full_numbers",
		"iDisplayLength": 100,
		"oLanguage": {
	        "sLengthMenu": "<span class='lenghtMenu'> _MENU_</span><span class='lengthLabel'>Entries per page:</span>",	
	    },
		 "sDom": '<"table_top"fl<"clear">>,<"table_content"t>,<"table_bottom"p<"clear">>'
		 
		});
		</script>
<style>
#groupgift_tbl tr th, #groupgift_tbl tr td {border-right: 1px solid #CCCCCC;}
</style>
<?php 
$this->load->view('admin/templates/footer.php');
?>