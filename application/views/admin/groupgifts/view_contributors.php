<?php
$this->load->view('admin/templates/header.php');
extract($privileges);
?>
<div id="content">
		<div class="grid_container">
			<?php 
				$attributes = array('id' => 'display_form');
				echo form_open('admin/groupgift/change_groupgift_status_global',$attributes) 
			?>
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading?></h6>
					</div>
					<div class="widget_content">
						<table class="display display_tbl" id="groupgift_tbl">
						<thead>
						<tr>
							<th class="tip_top" title="Click to sort">
								 Contributors Name
							</th>
							<th class="tip_top" title="Click to sort">
								 Amount
							</th>
							<th class="tip_top" title="Click to sort">
								Payment Type
							</th>
							<th class="tip_top" title="Click to sort">
								 Transaction Id
							</th>
							<th class="tip_top" title="Click to sort">
								Status
							</th>
							<th class="tip_top" title="Click to sort">
								Created Date
							</th>
						</tr>
						</thead>
						<tbody>
						<?php if($view_contributors->num_rows()>0){
							foreach($view_contributors->result() as $_view_contributors){
						?>
						<tr>
							<td class="center">
								<?php echo $_view_contributors->contributor_name;?>
							</td>
                            <td class="center">
								<?php echo $_view_contributors->amount;?>
							</td>
							<td class="center">
								<?php echo $_view_contributors->payment_type;?>	
							</td>
							<td class="center">
								<?php echo $_view_contributors->transaction_id;?>
							</td>
							<td class="center">
								<a class="tip_top"><span class="badge_style b_done"><?php echo $_view_contributors->status;?></span></a>
							</td>
							<td class="center">
								<?php echo $_view_contributors->created;?>
							</td>
						</tr>
						<?php }}?>
						</tbody>
						<tfoot>
						<tr>
							<th>
								 Contributors Name
							</th>
							<th>
								 Amount
							</th>
							<th>
								Payment Type
							</th>
							<th>
								Transaction Id
							</th>
							<th>
								Status
							</th>
							<th>
								 Created Date
							</th>
						</tr>
						</tfoot>
						</table>
					</div>
				</div>
			</div>
			<input type="hidden" name="statusMode" id="statusMode"/>
			<input type="hidden" name="SubAdminEmail" id="SubAdminEmail"/>
		</form>	
			
		</div>
		<span class="clear"></span>
	</div>
</div>
<script>
$('#groupgift_tbl').dataTable({   
		 "aoColumnDefs": [
							{ "bSortable": false, "aTargets": [ 4,5 ] }
						],
						"aaSorting": [[1, 'asc']],
		"sPaginationType": "full_numbers",
		"iDisplayLength": 100,
		"oLanguage": {
	        "sLengthMenu": "<span class='lenghtMenu'> _MENU_</span><span class='lengthLabel'>Entries per page:</span>",	
	    },
		 "sDom": '<"table_top"fl<"clear">>,<"table_content"t>,<"table_bottom"p<"clear">>'
		 
		});
		</script>
<style>
#groupgift_tbl tr th, #groupgift_tbl tr td {border-right: 1px solid #CCCCCC;}
</style>
<?php 
$this->load->view('admin/templates/footer.php');
?>