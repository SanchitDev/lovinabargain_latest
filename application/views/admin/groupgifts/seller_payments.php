<?php
$this->load->view('admin/templates/header.php');
extract($privileges);
?>
<div id="content">
		<div class="grid_container">
			<?php 
				$attributes = array('id' => 'display_form');
				echo form_open('admin/groupgift/change_groupgift_status_global',$attributes) 
			?>
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading?></h6>
					</div>
					<div class="widget_content">
						<table class="display display_tbl" id="groupgift_tbl">
						<thead>
						<tr>
							<th class="tip_top" title="Click to sort">
								 Groupgift Id
							</th>
							<th>
								 seller Id
							</th>
							<th>
								Total Amount
							</th>
							<th>
								 Payment Type
							</th>
							<th class="tip_top" title="Click to sort">
								 Transaction Id
							</th>
							<th>
								 Paid Amount
							</th>
							<th>
								 Admin Commission
							</th>
							<th class="tip_top" title="Click to sort">
								Status
							</th>
							<th class="tip_top" title="Click to sort">
								Created Date
							</th>
						</tr>
						</thead>
						<tbody>
						<?php if($seller_payment->num_rows()>0){
							foreach($seller_payment->result() as $row){
						?>
						<tr>
							<td class="center">
								<?php echo $row->grupgiftId;?>
							</td>
                            <td class="center">
								<?php echo $row->sell_id;?>
							</td>
							<td class="center">
								<?php echo $row->total_amt;?>	
							</td>
							<td class="center">
								<?php echo $row->payment_type;?>
							</td>
							<td class="center">
								<?php echo $row->transactionId;?>
							</td>
							<td class="center">
								<?php echo $row->paid_amt;?>
							</td>
							<td class="center">
								<?php echo $row->admin_comm;?>
							</td>
							<td class="center">
							<?php if($row->status =='Paid'){?>
								<a class="tip_top"><span class="badge_style b_done"><?php echo $row->status;?></span></a>
							<?php } else{ ?>
								<a class="tip_top"><span class="badge_style b_pending"><?php echo $row->status;?></span></a>
							<?php }?>
							</td>
							<td class="center">
								<?php echo $row->created;?>
							</td>
						</tr>
						<?php }}?>
						</tbody>
						<tfoot>
						<tr>
							<th>
								 Groupgift Id
							</th>
							<th>
								 seller Id
							</th>
							<th>
								Total Amount
							</th>
							<th>
								 Payment Type
							</th>
							<th>
								 Transaction Id
							</th>
							<th>
								 Paid Amount
							</th>
							<th>
								 Admin Commission
							</th>
							<th>
								Status
							</th>
							<th>
								Created Date
							</th>
						</tr>
						</tfoot>
						</table>
					</div>
				</div>
			</div>
			<input type="hidden" name="statusMode" id="statusMode"/>
			<input type="hidden" name="SubAdminEmail" id="SubAdminEmail"/>
		</form>	
			
		</div>
		<span class="clear"></span>
	</div>
</div>
<script>
$('#groupgift_tbl').dataTable({   
		 "aoColumnDefs": [
							{ "bSortable": false, "aTargets": [ 4,5 ] }
						],
						"aaSorting": [[1, 'asc']],
		"sPaginationType": "full_numbers",
		"iDisplayLength": 100,
		"oLanguage": {
	        "sLengthMenu": "<span class='lenghtMenu'> _MENU_</span><span class='lengthLabel'>Entries per page:</span>",	
	    },
		 "sDom": '<"table_top"fl<"clear">>,<"table_content"t>,<"table_bottom"p<"clear">>'
		 
		});
		</script>
<style>
#groupgift_tbl tr th, #groupgift_tbl tr td {border-right: 1px solid #CCCCCC;}
</style>
<?php 
$this->load->view('admin/templates/footer.php');
?>