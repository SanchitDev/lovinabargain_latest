<?php
$this->load->view('admin/templates/header.php');
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6>Add New Condition</h6>
                        
					</div>
					<div class="widget_content">
					<?php 
						$condition = array('class' => 'form_container left_label', 'id' => 'addattribute_form', 'enctype' => 'multipart/form-data');
						echo form_open_multipart('admin/condition/insertCondition',$condition) 
					?>
                    
							<ul>
								<li>
									<div class="form_grid_12">
									<label class="field_title" for="attribute_name">Condition Name<span class="req">*</span></label>
									<div class="form_input">
										<input name="condition_name" id="condition_name" type="text" tabindex="1" class="required large tipTop" required title="Please enter the condition name"/>
									</div>
									</div>
								</li>
								
								<li>
									<div class="form_grid_12">
									<label class="field_title" for="attribute_name">Description<span class="req">*</span></label>
									<div class="form_input">
										<textarea style="margin-top: 0px;margin-bottom: 0px;height: 102px;" name="condition_description" class="required large tipTop" required title="Please enter the condition description"></textarea>
									</div>
									</div>
								</li>
								
								<!--<li>
									<div class="form_grid_12">
									<label class="field_title" for="attribute_name">LOGO<span class="req">*</span></label>
									<div class="form_input">
										<input name="brand_logo" id="condition_logo" type="file" tabindex="1" class="required large tipTop" required title="Please enter the condition logo"/>
									</div>
									</div>
								</li>-->
								
								<li>
									<div class="form_grid_12">
										<label class="field_title" for="admin_name">Status <span class="req">*</span></label>
										<div class="form_input">
											<div class="active_inactive">
												<input type="checkbox" tabindex="11" name="status" checked="checked" id="active_inactive_active" class="active_inactive"/>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="form_grid_12">
										<div class="form_input">
											<button type="submit" class="btn_small btn_blue" tabindex="9"><span>Submit</span></button>
										</div>
									</div>
								</li>
							</ul>
                    
						</form>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('admin/templates/footer.php');
?>