<?php
$this->load->view('admin/templates/header.php');
extract($privileges);
?>
<div id="content">
		<div class="grid_container">
			<?php 
				$attributes = array('id' => 'display_form');
				echo form_open('admin/commission/change_commission_status_global',$attributes) 
			?>
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading?></h6>
						<div style="float: right;line-height:40px;padding:0px 10px;height:39px;">
						
						
							
						</div>
					</div>
					<div class="widget_content">
						<table class="display" id="userListTbl">
						<thead>
						<tr>
							
							<th class="tip_top" title="Click to sort">
								 Price Range
							</th>
							<th class="tip_top" title="Click to sort">
								 Product Listing
							</th>
							
 							<th class="tip_top" title="Click to sort">
								Commission
							</th>
 							
							<th width="12%">
								 Action
							</th>
						</tr>
						</thead>
						<tbody>
						<?php 
						if ($commissionList->num_rows() > 0){
							foreach ($commissionList->result() as $row){
						?>
						<tr>
								
							<td class="center" style="text-transform:capitalize;">
								<?php if ($row->price_to != '500000.00' ) { echo $row->price_from . ' - ' . $row->price_to; }else{ echo 'Above 150.00';}?>
							</td>
							<td class="center" style="text-transform:capitalize;">
								<?php echo $row->product_fee.' $';?>
							</td>
							
							<td class="center">
								 <?php echo $row->booking_fee.' %';?>
							</td>
                            
							<td class="center">
							<?php if ($allPrev == '1' || in_array('2', $Commission)){?>
								<span><a class="action-icons c-edit" href="admin/commission/edit_form_commission/<?php echo $row->id;?>" title="Edit">Edit</a></span>
							<?php }?>
								
							</td>
						</tr>
						<?php 
							
							}
						}
						?>
						</tbody>
						<tfoot>
						<tr>
							<th>
								 Price Range
							</th>
							<th>
								 Product Fee
							</th>
							
 							<th>
							    Booking Fee
							</th>
 							
							<th>
								 Action
							</th>
						</tr>
						</tfoot>
						</table>
					</div>
				</div>
			</div>
			<input type="hidden" name="statusMode" id="statusMode"/>
			<input type="hidden" name="SubAdminEmail" id="SubAdminEmail"/>
		</form>	
			
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('admin/templates/footer.php');
?>