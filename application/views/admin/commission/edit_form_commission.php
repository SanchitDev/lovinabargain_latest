<?php
$this->load->view('admin/templates/header.php');
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6>Update Commission</h6>
						<div id="widget_tab">
			              <ul>
			                <li><a href="#tab1" class="active_tab">Commission Detail</a></li>
			             </ul>
			            </div>
					</div>
					<div class="widget_content">
					<?php 
						$attributes = array('class' => 'form_container left_label', 'id' => 'edit_commissions_form', 'enctype' => 'multipart/form-data');
						echo form_open_multipart('admin/commission/update_commision',$attributes) 
					?>
						<div id="tab1">
	 						<ul>
	 							<li>
								<div class="form_grid_12">
									<label class="field_title" for="Price Range">Type</label>
									<div class="form_input">
										<?php echo strtoupper($commissionDetail->row()->type); ?>
									</div>
								</div>
								</li>
								
								<li>
								<div class="form_grid_12">
									<label class="field_title" for="Price Range">Price Range</label>
									<div class="form_input">
										<?php if ($commissionDetail->row()->price_to != '500000.00' ) { echo $commissionDetail->row()->price_from . ' - ' . $commissionDetail->row()->price_to.'  $'; }else{ echo 'Above 150.00 $';}?>
									</div>
								</div>
								</li>
								
	 							<li>
								<div class="form_grid_12">
									<label class="field_title" for="product_fee">Product Listing ( $ )<span class="req">*</span></label>
									<div class="form_input">
										<input name="product_fee" style=" width:295px" id="product_fee" value="<?php echo $commissionDetail->row()->product_fee;?>" type="text" tabindex="1" class="required tipTop" title="Please enter the product fee" required/>
									</div>
								</div>
								</li>
                                
                                <li>
								<div class="form_grid_12">
									<label class="field_title" for="booking_fee">Commission ( % )<span class="req">*</span></label>
									<div class="form_input">
										<input name="booking_fee" style=" width:295px" id="booking_fee" value="<?php echo $commissionDetail->row()->booking_fee;?>" type="text" tabindex="1" class="required tipTop" title="Please enter the booking fee" required/>
									</div>
								</div>
								</li>
                                
								<input type="hidden" name="id" value="<?php echo $commissionDetail->row()->id;?>"/>
								</li>
								<li>
								<div class="form_grid_12">
									<div class="form_input">
										<button type="submit"  class="btn_small btn_blue" tabindex="4"><span>Update</span></button>
									</div>
								</div>
								</li>
							</ul>
						</div>
						
						</form>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>

<?php 
$this->load->view('admin/templates/footer.php');
?>