<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php if($this->config->item('google_verification')){ echo stripslashes($this->config->item('google_verification')); }
    if ($heading == ''){?>
    <title><?php echo $title;?></title>
    <?php }else {?>
    <title><?php echo $heading;?></title>
    <?php }?>
        <script src="<?php echo base_url(); ?>js/mobile/jquery.js"></script>
    <link href="<?php echo base_url(); ?>css/mobile/bootstrap.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>css/mobile/style.css" rel="stylesheet" />
  </head>

<body>
	<div class="wrapper">
		<div class="container">
			<div class="fancy-logo">
				<a href="<?php echo base_url(); ?>" target="_blank" id="logo"><img src="<?php echo base_url(); ?>images/logo/<?php echo $logo; ?>" alt="<?php echo $meta_title ; ?>" title="<?php echo $meta_title ; ?>" /></a>
			</div>
		</div>
        <?php
            if ($order_details->num_rows() > 0) {
                $subTotal = $order_details->row()->total - ($order_details->row()->tax + $order_details->row()->shippingcost) + $order_details->row()->discountAmount;
        ?>
    		<div class="order-section">
        		<div class="order-details-sec clearfix">
        			<div class="pull-left clearfix">
        				<div>
        					<p><?php if ($this->lang->line('Order_id') != '') {echo stripslashes($this->lang->line('Order_id'));} else echo "Order Id"; ?> :</p>
        					<p>#<?php echo $order_details->row()->dealCodeNumber; ?></p>
        				</div>
        				<div>
        					<p><?php
                                    if ($this->lang->line('order_date') != '') {
                                        echo stripslashes($this->lang->line('order_date'));
                                    } else
                                        echo "Order Date";
                                    ?>  :</p>
        					<p><?php echo $order_details->row()->created; ?></p>
        				</div>
        			</div>
        			<div class="pull-right clearfix" style="text-align:right">
        				<div>
        					<p><?php
                                    if ($this->lang->line('sub_total') != '') {
                                        echo stripslashes($this->lang->line('sub_total'));
                                    } else
                                        echo "Sub Total";
                                    ?> : </p>
        					<p><?php echo $currencySymbol . number_format($subTotal, 2); ?></p>
        				</div>
        				<div>
        					<p><?php
                                    if ($this->lang->line('checkout_discount') != '') {
                                        echo stripslashes($this->lang->line('checkout_discount'));
                                    } else
                                        echo "Discount";
                                    ?> : </p>
        					<p><?php echo $currencySymbol . $order_details->row()->discountAmount; ?></p>
        				</div>
        				<div>
        					<p><?php
                                    if ($this->lang->line('referrals_shipping') != '') {
                                        echo stripslashes($this->lang->line('referrals_shipping'));
                                    } else
                                        echo "Shipping";
                                    ?> : </p>
        					<p><?php echo $currencySymbol . $order_details->row()->shippingcost; ?></p>
        				</div>
        				<div>
        					<p><?php
                                    if ($this->lang->line('checkout_tax') != '') {
                                        echo stripslashes($this->lang->line('checkout_tax'));
                                    } else
                                        echo "Tax";
                                    ?> : </p>
        					<p><?php echo $currencySymbol . $order_details->row()->tax; ?></p>
        				</div>
        				<div>
        					<p><?php
                                    if ($this->lang->line('grand_total') != '') {
                                        echo stripslashes($this->lang->line('grand_total'));
                                    } else
                                        echo "Grand Total";
                                    ?> : </p>
        					<p><?php echo $currencySymbol . $order_details->row()->total; ?></p>
        				</div>
        			</div>

        		</div>


            <?php
                foreach ($order_details->result() as $orderRow) {
                    $prodImg = 'dummyProductImage.jpg';
                    if ($prod_details[$orderRow->product_id]->row()->image == '') {
                        $imgArr = array_filter(explode(',', $orderRow->old_image));
                    } else {
                        $imgArr = array_filter(explode(',', $prod_details[$orderRow->product_id]->row()->image));
                    }
                    if (count($imgArr) > 0) {
                        $prodImg = $imgArr[0];
                    }
                    if ($prod_details[$orderRow->product_id]->row()->product_name == '') {
                        $product_name = $orderRow->old_product_name;
                    } else {
                        $product_name = $prod_details[$orderRow->product_id]->row()->product_name;
                    }
            ?>



        		<div class="product-list">
        			<div class="container">
        				<div class="col-md-2 no-padding">
        					 <p><?php
                                     if ($this->lang->line('product_prod_image') != '') {
                                         echo stripslashes($this->lang->line('product_prod_image'));
                                     } else
                                         echo "Product Image";
                                     ?></p>
        					 <p class="clearfix">
                                 <img src="<?php echo base_url(); ?>images/product/<?php echo $prodImg; ?>" alt="<?php echo $prod_details[$orderRow->product_id]->row()->product_name; ?>">
                             </p>
        				</div>
        				<div class="col-md-2 no-padding">
        					 <p><?php
                                     if ($this->lang->line('product_name') != '') {
                                         echo stripslashes($this->lang->line('product_name'));
                                     } else
                                         echo "Product Name";
                                     ?></p>
        					 <p><?php echo $product_name; ?>
                                     <?php
                                     if ($orderRow->attr_name != '' || $orderRow->attr_name != '') {
                                         echo '<br>' . $orderRow->attr_type . ' / ' . $orderRow->attr_name;
                                     } else if ($orderRow->old_attr_name != '') {
                                         echo '<br>' . $orderRow->old_attr_name;
                                     }
                                     ?></p>
        				</div>
        				<div class="col-md-1 no-padding">
        					 <p><?php
                                     if ($this->lang->line('qty') != '') {
                                         echo stripslashes($this->lang->line('qty'));
                                     } else
                                         echo "Qty";
                                     ?></p>
        					 <p><?php echo $orderRow->quantity; ?></p>
        				</div>
        				<div class="col-md-2 no-padding">
        					 <p><?php
                                     if ($this->lang->line('unit_price') != '') {
                                         echo stripslashes($this->lang->line('unit_price'));
                                     } else
                                         echo "Unit Price";
                                     ?></p>
        					 <p><?php echo $currencySymbol . $orderRow->price; ?></p>
        				</div>
        				<div class="col-md-2 no-padding">
        					 <p><?php
                                     if ($this->lang->line('sub_total') != '') {
                                         echo stripslashes($this->lang->line('sub_total'));
                                     } else
                                         echo "Sub Total";
                                     ?></p>
        					 <p><?php echo $currencySymbol . $orderRow->sumtotal; ?></p>
        				</div>
        				<div class="col-md-2 no-padding">
        					 <p><?php
                                     if ($this->lang->line('received_status') != '') {
                                         echo stripslashes($this->lang->line('received_status'));
                                     } else
                                         echo "Received Status";
                                     ?></p>
        					 <p>


                                <?php if ($view_mode == 'user') { ?>
                                    <select onChange="changeReceivedStatus(this , '<?php echo $orderRow->id; ?>');" >
                                        <option <?php
                                        if ($orderRow->received_status == 'Not received yet') {
                                            echo 'selected="selected"';
                                        }
                                        ?> value="Not received yet"><?php
                                                if ($this->lang->line('order_not_received_yet') != '') {
                                                    echo stripslashes($this->lang->line('order_not_received_yet'));
                                                } else
                                                    echo "Not received yet";
                                                ?></option>
                                        <option <?php
                                        if ($orderRow->received_status == 'Product received') {
                                            echo 'selected="selected"';
                                        }
                                        ?> value="Product received"><?php
                                                if ($this->lang->line('order_product_received') != '') {
                                                    echo stripslashes($this->lang->line('order_product_received'));
                                                } else
                                                    echo "Product received";
                                                ?></option>
                                    </select>
                                <?php }else { ?>
                                    <?php echo $orderRow->received_status; ?>
                                <?php } ?>
        					</p>
        				</div>
                    <?php
                        if ($view_mode != 'seller') {
                    ?>
        				<div class="col-md-1 no-padding">
        					 <p><?php
                                     if ($this->lang->line('reviews') != '') {
                                         echo stripslashes($this->lang->line('reviews'));
                                     } else
                                         echo "Reviews";
                                     ?></p>
        					 <p><a href="<?php echo base_url(); ?>mobilefeedback/<?php echo $orderRow->product_id; ?>/<?php echo $UserId; ?>?AppWebView=1"><?php
                                     if ($this->lang->line('reviews') != '') {
                                         echo stripslashes($this->lang->line('reviews'));
                                     } else
                                         echo "Reviews";
                                     ?></a></p>
        				</div>
                    <?php } ?>
        			</div>
        		</div>
        		<div class="Comments">

        			<h2>
                        <?php
                            if ($this->lang->line('comments') != '') {
                                echo stripslashes($this->lang->line('comments'));
                            } else
                                echo "Comments";
                            ?>
        			</h2>
                    <?php
                    if ($order_comments->num_rows() > 0) {
                        ?>
                        <section class="comments comments-list comments-list-new">
                            <ul class="commentContainer">
                                <?php
                                $cmt_count = 0;
                                foreach ($order_comments->result() as $cmt_row) {
                                    if ($cmt_row->product_id == $orderRow->product_id) {
                                        $cmt_count++;
                                        $comment_from = $cmt_row->comment_from;
                                        $userImg = 'user-thumb.png';
                                        if ($comment_from == 'admin') {
                                            $userImg = 'user_thumb.png';
                                        } else if ($comment_from == 'seller') {
                                            $userImg = 'user-thumb1.png';
                                        }
                                        if ($view_mode == 'user') {
                                            if ($comment_from == 'user') {
                                                $comment_from = 'You';
                                            }
                                        } else if ($view_mode == 'seller') {
                                            if ($comment_from == 'seller') {
                                                $comment_from = 'You';
                                            }
                                            if ($comment_from == 'user') {
                                                $comment_from = 'Buyer';
                                            }
                                        }

                                        $cmtTime = strtotime($cmt_row->date);
                                        $cmt_time = timespan($cmtTime) . ' ago';
                                        ?>
                                        <li class="comment" style="position: relative;padding: 17px 0 12px 43px;z-index: 1;min-height: 20px;">
                                            <a class="milestone" id="comment-1866615"></a>
                                            <span class="vcard">
                                                <a class="url">
                                                    <img src="<?php echo base_url(); ?>images/users/<?php echo $userImg; ?>" alt="" class="photo">
                                                </a>
                                            </span>
                                            <div class="comment-details">
                                                <span class="fn nickname"><?php echo ucfirst($comment_from); ?></span>
                                                <p class="c-text" style="font-size:13px;"><?php echo $cmt_row->comment; ?></p>
                                                <p style="font-size: 10px;font-style:italic;color:green;"><?php echo $cmt_time; ?></p>
                                            </div>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </section>
                        <?php
                        if ($cmt_count == 0) {
                            ?>
                            <p style="margin: 10px 0 0;color: #0F6697;"><i><?php
                                    if ($this->lang->line('no_cmt_avail') != '') {
                                        echo stripslashes($this->lang->line('no_cmt_avail'));
                                    } else
                                        echo "No comments available";
                                    ?></i></p>
                            <?php
                        }
                    }else {
                        ?>
                        <p style="margin: 10px 0 0;color: #0F6697;"><i><?php
                                if ($this->lang->line('no_cmt_avail') != '') {
                                    echo stripslashes($this->lang->line('no_cmt_avail'));
                                } else
                                    echo "No comments available";
                                ?></i></p>
                    <?php } ?>
        			<!-- <p>No comments available</p> -->
                <form action="javascript:void(0)" onsubmit="post_order_comment('<?php echo $orderRow->product_id; ?>', '<?php echo $view_mode; ?>', '<?php echo $UserId; ?>', '<?php echo $order_details->row()->dealCodeNumber; ?>')" method="post">
            			<div class="form-control-sec">
                                <textarea class="text order_comment_<?php echo $orderRow->product_id; ?>" name="comments" placeholder="<?php
                                if ($this->lang->line('header_write_comment') != '') {
                                    echo stripslashes($this->lang->line('header_write_comment'));
                                } else
                                    echo "Write a comment";
                                ?>..." id="comments"></textarea>
            			</div>
            			<div class="form-control-sec">
            					<!-- <button>post</button> -->
                                <button type="submit"><?php
                                if ($this->lang->line('header_post_comment') != '') {
                                    echo stripslashes($this->lang->line('header_post_comment'));
                                } else
                                    echo "Post Comment";
                                ?></button>
                                <img alt="loading" src="<?php echo base_url(); ?>images/site/loading.gif" style="display: none;"/>
            			</div>
                    </form>
        		</div>
        <?php } ?>
    		</div>
    <?php
        }
    ?>
	</div>
</body>

<script type="text/javascript">
    baseURL = "<?php echo base_url(); ?>";
    function changeReceivedStatus(evt, rid) {
        $.ajax({
            type: 'post',
            url: baseURL + 'site/mobile/change_received_status',
            data: {'rid': rid, 'status': $(evt).val()},
            complete: function () {
            }
        });
    }
</script>
<script type="text/javascript">
    baseURL = "<?php echo base_url(); ?>";
    function post_order_comment(pid, utype, uid, dealcode) {
        if ($('.order_comment_' + pid).hasClass('posting'))
            return;
        $('.order_comment_' + pid).addClass('posting');
        var $form = $('.order_comment_' + pid).parent();
        if (uid == '') {
            alert('Login required');
            $('.order_comment_' + pid).removeClass('posting');
        } else {
            if ($('.order_comment_' + pid).val() == '') {
                alert('Your comment is empty');
                $('.order_comment_' + pid).removeClass('posting');
            } else {
                $form.find('img').show();
                $form.find('button').hide();
                $.ajax({
                    type: 'post',
                    url: baseURL + 'json/productComment?AppWebView=1',
                    data: {'product_id': pid, 'comment_from': utype, 'commentor_id': uid, 'deal_code': dealcode, 'comment': $('.order_comment_' + pid).val()},
                    complete: function (data) {
                        $form.find('img').hide();
                        $form.find('button').show();
                        window.location.reload();
                    }
               });
            }
        }
    }
</script>
</html>
