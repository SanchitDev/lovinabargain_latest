<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?php echo base_url(); ?>css/mobile/bootstrap.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>css/mobile/style.css" rel="stylesheet" />
    <title><?php echo $title; ?></title>
  </head>
<body>
	<div class="wrapper">
		<div class="container">
			<div class="fancy-logo">
                <a href="<?php echo base_url(); ?>" target="_blank" id="logo"><img src="<?php echo base_url(); ?>images/logo/<?php echo $logo; ?>" alt="<?php echo $meta_title ; ?>" title="<?php echo $meta_title ; ?>" /></a>
			</div>
		</div>

		<div class="product-details">

			<div class="container">
				<div class="row clearfix">
					<div class="pull-right clearfix">
						<div class="order-head">
							<p>Order Id</p>
							<p>Order Date</p>
						</div>
						<div class="order-details">
							<p>#<?php echo $invoice_number; ?></p>
							<p><?php echo $invoice_date; ?></p>
						</div>
					</div>
				</div>
			</div>
		<div class="shipping-details">
			<div class="container">
				<div class="col-md-6 no-padding">
					<div class="shipping-head">
						Shipping Address
					</div>
					<div class="shipping-address">
						<div class="clearfix">
							<div class="col-xs-8">
								<p>Full Name</p>
							</div>
							<div class="col-xs-4">
								<p> <?php echo $ship_fullname; ?></p>
							</div>
						</div>
						<div class="clearfix">
							<div class="col-xs-8">
								<p>Address</p>
							</div>
							<div class="col-xs-4">
								<p><?php echo $ship_address1; ?>
                                </p>
							</div>
						</div>
						<div class="clearfix">
							<div class="col-xs-8">
								<p>Address 2</p>
							</div>
							<div class="col-xs-4">
								<p><?php echo $ship_address2; ?>
                                </p>
							</div>
						</div>
						<div class="clearfix">
							<div class="col-xs-8">
								<p>city</p>
							</div>
							<div class="col-xs-4">
								<p><?php echo $ship_city; ?>
                                </p>
							</div>
						</div>
						<div class="clearfix">
							<div class="col-xs-8">
								<p>Country</p>
							</div>
							<div class="col-xs-4">
								<p><?php echo $ship_country; ?>
                                </p>
							</div>
						</div>
						<div class="clearfix">
							<div class="col-xs-8">
								<p>State</p>
							</div>
							<div class="col-xs-4">
								<p><?php echo $ship_state; ?>
                                </p>
							</div>
						</div>
						<div class="clearfix">
							<div class="col-xs-8">
								<p>Zipcode</p>
							</div>
							<div class="col-xs-4">
								<p><?php echo $ship_postalcode; ?>
                                </p>
							</div>
						</div>
						<div class="clearfix">
							<div class="col-xs-8">
								<p>Phone Number</p>
							</div>
							<div class="col-xs-4">
								<p><?php echo $ship_phone; ?></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 no-padding">
					<div class="shipping-head">
						Billing Address
					</div>
					<div class="shipping-address">
						<div class="clearfix">
							<div class="col-xs-8">
								<p>Full Name</p>
							</div>
							<div class="col-xs-4">
								<p><?php echo $bill_fullname; ?>
                                </p>
							</div>
						</div>
						<div class="clearfix">
							<div class="col-xs-8">
								<p>Address</p>
							</div>
							<div class="col-xs-4">
								<p><?php echo $bill_address1; ?>
                                </p>
							</div>
						</div>
						<div class="clearfix">
							<div class="col-xs-8">
								<p>Address 2</p>
							</div>
							<div class="col-xs-4">
								<p><?php echo $bill_address2; ?>
                                </p>
							</div>
						</div>
						<div class="clearfix">
							<div class="col-xs-8">
								<p>city</p>
							</div>
							<div class="col-xs-4">
								<p><?php echo $bill_city; ?>
                                </p>
							</div>
						</div>
						<div class="clearfix">
							<div class="col-xs-8">
								<p>Country</p>
							</div>
							<div class="col-xs-4">
								<p><?php echo $bill_country; ?>
                                </p>
							</div>
						</div>
						<div class="clearfix">
							<div class="col-xs-8">
								<p>State</p>
							</div>
							<div class="col-xs-4">
								<p><?php echo $bill_state; ?>
                                </p>
							</div>
						</div>
						<div class="clearfix">
							<div class="col-xs-8">
								<p>Zipcode</p>
							</div>
							<div class="col-xs-4">
								<p><?php echo $bill_postalcode; ?>
                                </p>
							</div>
						</div>
						<div class="clearfix">
							<div class="col-xs-8">
								<p>Phone Number</p>
							</div>
							<div class="col-xs-4">
								<p><?php echo $bill_phone; ?>
                                </p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

        <?php echo $items; ?>


		<!-- <div class="product-price">
			<div class="container">
				<div class="pull-right">
					<div class="total-row clearfix ">
						<p>Sub Total</p>
						<p>$60.00</p>
					</div>
					<div class="total-row clearfix">
						<p>Discount Amount</p>
						<p>$60.00</p>
					</div>
					<div class="total-row clearfix">
						<p>Shipping Cost</p>
						<p>$60.00</p>
					</div>
					<div class="total-row clearfix">
						<p>Shipping Tax</p>
						<p>$60.00</p>
					</div>
					<div class="total-row clearfix">
						<p>Grand Total</p>
						<p>$60.00</p>
					</div>
				</div>
			</div>
		</div> -->

	</div>
	</div>
<script src="js/script.js"></script>
</body>
</html>
