<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?php echo base_url(); ?>css/mobile/bootstrap.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>css/mobile/style.css" rel="stylesheet" />
    <title><?php echo $title; ?></title>
  </head>
<body>
	<div class="wrapper">
		<div class="container">
			<div class="fancy-logo">
                <a href="<?php echo base_url(); ?>" target="_blank" id="logo"><img src="<?php echo base_url(); ?>images/logo/<?php echo $logo; ?>" alt="<?php echo $meta_title ; ?>" title="<?php echo $meta_title ; ?>" /></a>
			</div>
		</div>

		<div class="product-details cms_page_style">

			<div class="container">
                <h4><?php echo $heading; ?></h4>
                <p>
                    <?php
                        echo $pageDetails->row()->description;
                     ?>
                </p>
			</div>


	</div>
	</div>
<script src="js/script.js"></script>
</body>
</html>
