<?php
    $ProductVal = $this->data['userVal']->result_array();
 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php if($this->config->item('google_verification')){ echo stripslashes($this->config->item('google_verification')); }
    if ($heading == ''){?>
    <title><?php echo $title;?></title>
    <?php }else {?>
    <title><?php echo $heading;?></title>
    <?php }?>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/mobile/jRating.jquery.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/site/my-account.css" />
    <link href="<?php echo base_url(); ?>css/mobile/bootstrap.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>css/mobile/style.css" rel="stylesheet" />
    <script src="<?php echo base_url(); ?>js/mobile/jquery.js"></script>
    <script type="text/javascript">
         baseUrl = "<?php echo base_url(); ?>";
    </script>
    <script src="<?php echo base_url();?>js/mobile/jquery/jRating.jquery.js"></script>
  </head>

<body>
	<div class="wrapper">
		<div class="container">
			<div class="fancy-logo">
				<a href="<?php echo base_url(); ?>" target="_blank" id="logo"><img src="<?php echo base_url(); ?>images/logo/<?php echo $logo; ?>" alt="<?php echo $meta_title ; ?>" title="<?php echo $meta_title ; ?>" /></a>
			</div>
		</div>
		<div class="title-sec">
			<h2><?php if($this->lang->line('product_feedback') != '') { echo stripslashes($this->lang->line('product_feedback')); } else echo "Product Feedback"; ?></h2>
		</div>
		<div class="fancy_shop_text">
			 <form  action = "<?php echo base_url(); ?>site/mobile/add_user_product_feedback" method = "post" <?php if ($feedback_details->num_rows()>0){?>onSubmit="return false;"<?php }else{?>onSubmit="return AddFeedback();"<?php }?>>
				<div class="form-control-sec">
					<p><?php if($this->lang->line('header_title') != '') { echo stripslashes($this->lang->line('header_title')); } else echo "Title"; ?><sup>*</sup> <span style="color:#F00;" class="redFont" id="title_Err"></span></p>
					<!-- <input type="text" class="form-control" name="" value=""/> -->
                         <input type="text" name="title" class="input-text" <?php if ($feedback_details->num_rows()>0){?>value="<?php echo $feedback_details->row()->title;?>" disabled="disabled" <?php }?> id="title" onkeyup="removeError(this.id);" />
				</div>
				<div class="form-control-sec">
					<p><?php if($this->lang->line('header_description') != '') { echo stripslashes($this->lang->line('header_description')); } else echo "Description"; ?><sup>*</sup><span style="color:#F00;" class="redFont" id="description_Err"></span></p>
                         <textarea  id= "description" name= "description" <?php if ($feedback_details->num_rows()>0){?> disabled="disabled" <?php }?> onkeyup="removeError(this.id);"  maxlength="200" ><?php if ($feedback_details->num_rows()>0){ echo $feedback_details->row()->description; }?></textarea>
				</div>


				<!-- <div class="form-control-sec">
					<p>Your rating for this product <sup>*</sup></p>
					<div class="rating"></div>
				</div> -->


                   <div class="feedback_rating">
                       <div class="rating-text">
                           <input type="hidden" name="product_id" id="product_id" value="<?php echo $ProductVal[0]['id'];?>" />
                           <input type="hidden" name="seller_id" id="seller_id" value="<?php echo $ProductVal[0]['user_id'];?>" />
                           <input type="hidden" name="rate" id="rate" value="<?php echo $UserId; ?>" />
                           <input type="hidden" name="path" id="path" value="<?php echo base_url(); ?>" />
                           <input type="hidden" name="AppWebView" value="1" />
                           <label>
                                   <?php
                                       if($this->lang->line('ur_rat_this_prod') != '') {
                                           echo stripslashes($this->lang->line('ur_rat_this_prod'));
                                       } else {
                                           echo "Your rating for this product";
                                       }
                                   ?>
                           </label>
                           <span style="color:#F00;">*</span>
                           <span style="color:#F00;" class="redFont" id="rating_value_Err"></span>
                           <div class="clear"></div>
                           <?php
                               if ($feedback_details->num_rows()>0){
                           ?>
                           <div class="rating_star">
                               <div class="rat_star1" style="width:<?php echo $feedback_details->row()->rating*20; ?>%"></div>
                           </div>
                           <?php
                               }else {
                           ?>
                           <div class="exemple">
                               <?php
                                   if($UserId !=''){
                               ?>
                               <div class="star_rating">
                                   <div class="exemple5" data="10_5"  style="width:60%;"></div>
                               </div>
                               <?php
                                   }else{
                               ?>
                               <div class="star_rating" style="height:35px;">
                                   <div style="cursor:pointer;">
                                       <img src="<?php echo base_url(); ?>images/10stars.png" alt="stars" onclick="javascript:sivarating();" />
                                   </div>
                                   <div id="PetVoteRate"></div>
                               </div>
                               <?php } ?>
                           </div>
                           <?php } ?>


                           <div class="clear"></div><br />
                           <input type="hidden" name="rating_value" id="rating_value"   />
                       </div>
                   </div>
          		<div class="form-control-sec">
                          <?php if ($feedback_details->num_rows()==0){?>
                                      <button type="submit" class="btn-blue-embo-add"><span><?php if($this->lang->line('add_feedback') != '') { echo stripslashes($this->lang->line('add_feedback')); } else echo "Add feedback"; ?></span></button>
                          <?php }else {?>
                                      <button type="submit" onclick="javascript:window.history.go(-1)" class="btn-blue-embo-add"><span><?php if($this->lang->line('signup_goback') != '') { echo stripslashes($this->lang->line('signup_goback')); } else echo "Go Back"; ?></span></button>
                          <?php }?>
                    </div>
		     </form>
		</div>
	</div>
</body>
<script type="text/javascript">
		$(document).ready(function(){
			$('.exemple5').jRating({
                    step: true,
				length:4.6,
				decimalLength:1,
				onSuccess : function(){
					alert('Success : your rate has been saved :)');
				},
				onError : function(){
					alert('Error : please retry');
				}
			});
		});
	</script>


    <script>
	function removeError(idval){
          $("#"+idval+"_Err").html('');
	}

    function AddFeedback(){
          var title = $('#title').val();
          var description = $('#description').val();
          var rating_value = $('#rating_value').val();
          if(title==''){
               $('#title_Err').html('Title is required');
               return false;
          }else if(description==''){
               $('#description_Err').html('Description is required');
               return false;
          } else if(rating_value==''){
          $('#rating_value_Err').html('Plese choose stars for your feedback');
               return false;
          }
	}
    </script>

</html>
