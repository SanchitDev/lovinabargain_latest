<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo $this->config->item('email_title'); ?> - Payment Credit Card</title>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mobile/app-style.css" type="text/css" media="all" />
		<script src="<?php echo base_url(); ?>js/mobile/jquery.js"></script>
		<script src="<?php echo base_url(); ?>js/mobile/jquery.validate.min.js"></script>
		<style>
		#PaymentCreditForm li label{
		color:#f00 !important;
		/*padding-left:5px;*/
		display:inline-block;
		}
		</style>
	</head>
	<body>
		<?php
		if($this->uri->segment(3)=='cart'){
			$checkAmt = @explode('|',$checkoutViewResults);
				if($checkAmt[3] > 0){
		?>
		<section>
			<div class="app-shipping">
				<div class="main">
					<ul class="app-shipping-level level-2">
						<li>Shipping</li>
						<li class="active"><h1>Payment</h1></li>
						<li>Success</li>
					</ul>
				</div>
			</div>

			<form name="PaymentCreditForm" id="PaymentCreditForm" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>site/mobileCheckout/PaymentCredit" autocomplete="off">
			<div class="shipping_address">
				<div class="main">
					<div class="app-content-box">
						<h1><?php if($this->lang->line('checkout_billing_addr') != '') { echo stripslashes($this->lang->line('checkout_billing_addr')); } else echo "Billing Address"; ?></h1>

						<ul>
							<li><input type="text" class="input-scroll" placeholder="Full Name" name="full_name" id="full_name" value="<?php echo $userDetails->row()->full_name; ?>"></input></li>
							<li><input type="text" class="input-scroll" placeholder="Address" name="address" id="address" value="<?php echo $userDetails->row()->address; ?>"></input></li>
							<li><input type="text" class="input-scroll" placeholder="Address2" name="address2" id="address2" value="<?php echo $userDetails->row()->address2; ?>"></input></li>
							<li><input type="text" class="input-scroll" placeholder="city" name="city" id="city" value="<?php echo $userDetails->row()->city; ?>"></input></li>
							<li><input type="text" class="input-scroll" placeholder="state" name="state" id="state" value="<?php echo $userDetails->row()->state; ?>"></input></li>
							<li>
								<select id="country" name="country" class="input-scroll-2">
									<option value=""><?php if($this->lang->line('checkout_select') != '') { echo stripslashes($this->lang->line('checkout_select')); } else echo "Select Country"; ?></option>
									<?php foreach($countryList->result() as $cntyRow){ ?>
									<option value="<?php echo $cntyRow->country_code; ?>" <?php if($cntyRow->country_code == $userDetails->row()->country){ echo 'selected="selected"';} ?> ><?php echo $cntyRow->name; ?></option>
									<?php } ?>
								</select>
							</li>
							<li><input type="text" class="input-scroll" placeholder="Zip Code" name="postal_code" id="postal_code"  value="<?php echo $userDetails->row()->postal_code; ?>"></input></li>
							<li><input type="text" class="input-scroll" placeholder="Phone No" name="phone_no" id="phone_no" value="<?php echo $userDetails->row()->phone_no; ?>"></input></li>
						</ul>
					</div>
				</div>
			</div>


			<div class="shipping_address">
				<div class="main">
					<div class="app-content-box">
						<h1>Enter Card Details</h1>
						<ul>
							<li><input type="text" class="input-scroll-3" placeholder="Card number" id="cardNumber" name="cardNumber"></input></li>
							<li><!--<label>Expiration</label> -->
								<?php $Sel ='selected="selected"';  ?>
								<select id="CCExpDay" name="CCExpDay" class="input-scroll-2 webView">
								<option value="01" <?php if(date('m')=='01'){ echo $Sel;} ?>>01</option>
								<option value="02" <?php if(date('m')=='02'){ echo $Sel;} ?>>02</option>
								<option value="03" <?php if(date('m')=='03'){ echo $Sel;} ?>>03</option>
								<option value="04" <?php if(date('m')=='04'){ echo $Sel;} ?>>04</option>
								<option value="05" <?php if(date('m')=='05'){ echo $Sel;} ?>>05</option>
								<option value="06" <?php if(date('m')=='06'){ echo $Sel;} ?>>06</option>
								<option value="07" <?php if(date('m')=='07'){ echo $Sel;} ?>>07</option>
								<option value="08" <?php if(date('m')=='08'){ echo $Sel;} ?>>08</option>
								<option value="09" <?php if(date('m')=='09'){ echo $Sel;} ?>>09</option>
								<option value="10" <?php if(date('m')=='10'){ echo $Sel;} ?>>10</option>
								<option value="11" <?php if(date('m')=='11'){ echo $Sel;} ?>>11</option>
								<option value="12" <?php if(date('m')=='12'){ echo $Sel;} ?>>12</option>
								</select>
								<select id="CCExpMnth" name="CCExpMnth" class="input-scroll-2 webView">
									<?php for($i=date('Y');$i< (date('Y') + 30);$i++){ ?>
									<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
									<?php } ?>
								</select>
							</li>
							<li><input type="password" class="input-scroll" placeholder="Security Code" id="creditCardIdentifier" name="creditCardIdentifier"></input></li>
							<input id="total_price" name="total_price" value="<?php echo number_format($checkAmt[3],2,'.',''); ?>" type="hidden">
							<input type="hidden" name="shipping_id" id="shipping_id" value="<?php echo $checkAmt[6]; ?>" />
							<input id="email" name="email" value="<?php echo $userDetails->row()->email; ?>" type="hidden">
							<input type="hidden" name="UserId" id="UserId" value="<?php echo $UserId; ?>"  />
							<input type="hidden" name="creditvalue" id="creditvalue" value="paypaldodirect" />
							<input type="hidden" name="AppWebView" id="AppWebView" value=1  />
							<input type="hidden" name="randomNo" id="randomNo" value="<?php echo $invoiceNumber; ?>"  />
							<li class="last"><input type="submit" class="input-submit-btn" value="Make Payment"></input></li>
						</ul>

					</div>
				</div>
			</div>
			</form>
		</section>
		<?php
				}
			}
		?>
	</body>

	<script>
	$(function () {
		   $("#PaymentCreditForm").validate({
			   rules: {
				   full_name: {
					   required: true,
				   },
				   address: {
					   required: true,
				   },
				   city: {
					   required: true,
				   },
				   state: {
					  required: true,
				  },
				   country: {
					   required: true,
				   },
				   postal_code: {
					   required: true,
				   },
				   phone_no: {
					   required: true,
					   digits:true,
				   },
				  cardNumber: {
					  required: true,
					  maxlength: 16,
					  digits: true
				  },
				  CCExpDay: {
					  required: true,
				  },
				  CCExpMnth: {
					  required: true,
				  },
				  creditCardIdentifier: {
					  required: true,
					  digits:true,
				  },
			   },
			   messages: {
				   full_name: {
					   required: "Enter Full Name !!"
				   },
				   address: {
					   required: "Enter Address !!"
				   },
				   city: {
					   required: "Enter City !!"
				   },
				   state: {
					   required: "Enter State !!"
				   },
				   country: {
					   required: "Select Country !!"
				   },
				   postal_code: {
					   required: "Enter Zip Code !!"
				   },
				   phone_no:  {
					   required: "Enter Phone No !!"
				   },
				   cardNumber :  {
					   required: "Invalid Card Number!!"
				   },
				   CCExpDay: {
						required: "Select Expiry Month !!",
				 	},
				 	CCExpMnth: {
						required: "Select Expiry Year !!",
				 	},
					creditCardIdentifier: {
					 required: "Enter CVV Number !!",
				 },
			   },
			   submitHandler: function (form) {
				   form.submit();
			   }
		   });
	   });
	</script>
</html>
