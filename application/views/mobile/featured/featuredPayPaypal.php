<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo $this->config->item('email_title'); ?> - Payment</title>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/mobile/app-style.css" type="text/css" media="all" />
		<script src="<?php echo base_url(); ?>js/mobile/jquery.js"></script>
		<script src="<?php echo base_url(); ?>js/mobile/jquery.validate.min.js"></script>

		<!-- <script src="<?php echo base_url(); ?>js/mobile/pace.min.js"></script> -->
		<style>
		#PaymentPaypalForm li label{
			color:#f00 !important;
			padding-left:5px;
			display:inline-block;
		}
		body{
			background:url(../images/01-progress.gif) center center;
		}
		</style>
	</head>
	<body>
		<?php
            $paypalProcess = unserialize($paypal_ipn_settings['settings']);
		    if($plan->row()->plan_price > 0){
		?>
		<section>
			<div class="app-shipping">
				<div class="main">
					<ul class="app-shipping-level level-2">
						<li>Plan Type</li>
						<li class="active">Payment</li>
						<li>Success</li>
					</ul>
				</div>
			</div>
            <div class="shipping_address">
				<div class="main">
					<div class="app-content-box">
						<h1>Plan Cost</h1>
                        <div class="cart_featured">
                                <p>
                                    <label>Total Plan Cost</label>
                                    <span><b><?php echo $currencySymbol;?><?php echo number_format($plan->row()->plan_price,2,'.',''); ?></b> <?php echo $currencyType;?></span>
                                </p>
                                <p>
                                    <label>Tax</label>
                                    <span><b><?php echo $currencySymbol;?><?php echo number_format(0,2,'.',''); ?></b> <?php echo $currencyType;?></span>
                                </p>
                                <p class="tota_featured_total">
                                    <label>Total</label>
                                    <span><b><?php echo $currencySymbol;?><?php echo number_format($plan->row()->plan_price,2,'.',''); ?></b> <?php echo $currencyType;?></span>
                                </p>

                        </div>
                    </div>
                </div>
            </div>
			<div class="shipping_address">
				<div class="main">
					<div class="app-content-box">
						<h1>
				<?php if($this->lang->line('checkout_billing_addr') != ''){ echo stripslashes($this->lang->line('checkout_billing_addr')); } else echo "Billing Address"; ?></h1>
						<form name="PaymentPaypalForm" id="PaymentPaypalForm" method="post" enctype="multipart/form-data" action="<?php echo base_url() ?>site/ios/paypal_payment?AppWebView=1"  autocomplete="off">
						<ul>
							<li><input type="text" class="input-scroll" placeholder="Full Name" name="full_name" id="full_name" value="<?php echo $userDetails->row()->full_name; ?>"></input></li>
							<li><input type="text" class="input-scroll" placeholder="Address" name="address" id="address" value="<?php echo $userDetails->row()->address; ?>"></input></li>
							<li><input type="text" class="input-scroll" placeholder="Address2" name="address2" id="address2" value="<?php echo $userDetails->row()->address2; ?>"></input></li>
							<li><input type="text" class="input-scroll" placeholder="city" name="city" id="city" value="<?php echo $userDetails->row()->city; ?>"></input></li>
							<li><input type="text" class="input-scroll" placeholder="state" name="state" id="state" value="<?php echo $userDetails->row()->state; ?>"></input></li>
							 <li><!--<label>Country</label> -->
								<select id="country" name="country" class="input-scroll-2">
									<option value=""><?php if($this->lang->line('checkout_select') != '') { echo stripslashes($this->lang->line('checkout_select')); } else echo "Select Country"; ?></option>
									<?php foreach($countryList->result() as $cntyRow){ ?>
									<option value="<?php echo $cntyRow->country_code; ?>" <?php if($cntyRow->country_code == $userDetails->row()->country){ echo 'selected="selected"';} ?> ><?php echo $cntyRow->name; ?></option>
									<?php } ?>
								</select>
							</li>
							<li><input type="text" class="input-scroll" placeholder="Zip Code" name="postal_code" id="postal_code"  value="<?php echo $userDetails->row()->postal_code; ?>"></input></li>
							<li><input type="text" class="input-scroll" placeholder="Phone No" name="phone_no" id="phone_no" value="<?php echo $userDetails->row()->phone_no; ?>"></input></li>
								<li class="last"><input type="submit" class="input-submit-btn" value="Make Payment"></input></li>
                                <input type="hidden" name="plan_id" id="plan_id" value="<?php echo $plan->row()->id; ?>">
                                <input type="hidden" name="paypalmode" id="paypalmode" value="<?php echo $paypalProcess['mode']; ?>"  />
                                <input type="hidden" name="paypalEmail" id="paypalEmail" value="<?php echo $paypalProcess['merchant_email']; ?>"  />
                                <input id="total_price" name="total_price" value="<?php echo number_format($plan->row()->plan_price,2,'.',''); ?>" type="hidden">
                                <input id="user_id" name="user_id" value="<?php echo $userDetails->row()->id; ?>" type="hidden">
                                <input id="email" name="email" value="<?php echo $userDetails->row()->email; ?>" type="hidden">
								<input type="hidden" name="UserId" id="UserId" value="<?php echo $UserId; ?>"  />
								<input type="hidden" name="AppWebView" id="AppWebView" value=1  />
						</ul>
					</form>
					</div>
				</div>
			</div>
		</section>
		<?php
			}
		?>
	</body>
	<script>
	$(function () {
		   $("#PaymentPaypalForm").validate({
			   rules: {
				   full_name: {
					   required: true,
				   },
				   address: {
					   required: true,
				   },
				   city: {
					   required: true,
				   },
				   state: {
					  required: true,
				  },
				   country: {
					   required: true,
				   },
				   postal_code: {
					   required: true,
				   },
				   phone_no: {
					   required: true,
					   digits:true,
				   },
			   },
			   messages: {
				   full_name: {
					   required: "Enter Full Name !!"
				   },
				   address: {
					   required: "Enter Address !!"
				   },
				   city: {
					   required: "Enter City !!"
				   },
				   state: {
					   required: "Enter State !!"
				   },
				   country: {
					   required: "Select Country !!"
				   },
				   postal_code: {
					   required: "Enter Zip Code !!"
				   },
				   phone_no:  {
					   required: "Enter Phone No !!"
				   },
			   },
			   submitHandler: function (form) {
				   form.submit();
			   }
		   });
	   });
	</script>
</html>
