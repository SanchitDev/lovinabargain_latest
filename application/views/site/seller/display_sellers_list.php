<?php
$this->load->view('site/templates/header');
?>
<style type="text/css">
    .noproducts {
        padding: 1%;
        text-align: center;
    }
    .overlay {
        position: absolute;
        bottom: 0;
        left: 0;
        right: 0;
        /*background-color: rgb(93, 210, 186);*/
        background-color: rgba(0, 0, 0, 0.66);
        overflow: hidden;
        width: 95%;
        height: 20%;
        transition: .5s ease;
        margin: 0 auto;
    }
    .product-desc span.username{
        background-color: #ffffff;
    }

    .ibox-content.product-box:hover .overlay {
        height: 20%;
        background-color: rgba(0, 0, 0, 0.76);
    }
    .product-imitation:hover img{
        opacity: 0.4;
    }
    .product-imitation img{
        margin: 0 auto;
    }
    .product-imitation{
        background: #949293;
    }
</style>
<script type="text/javascript">
    var can_show_signin_overlay = false;
    if (navigator.platform.indexOf('Win') != -1) {
        document.write("<style>::-webkit-scrollbar, ::-webkit-scrollbar-thumb {width:7px;height:7px;border-radius:4px;}::-webkit-scrollbar, ::-webkit-scrollbar-track-piece {background:transparent;}::-webkit-scrollbar-thumb {background:rgba(255,255,255,0.3);}:not(body)::-webkit-scrollbar-thumb {background:rgba(0,0,0,0.3);}::-webkit-scrollbar-button {display: none;}</style>");
    }
</script>
<!--[if lt IE 9]>
<script src="js/html5shiv/dist/html5shiv.js"></script>
<![endif]-->


<div class="col-lg-10 col-lg-offset-1 redesign">
    <div class="ibox panel panel-primary">
        <div class="ibox-title panel-heading">
            <h5 style="margin:0px;padding:0px;color: #fff !important;"><?php
                if ($this->lang->line('Lovinabargain_Merchants') != '') {
                    echo stripslashes($this->lang->line('Lovinabargain_Merchants'));
                } else
                    echo "Lovinabargain Merchants";
                ?>
            </h5>
        </div>
        <div class="ibox-content">
            <?php if ($featured_seller->num_rows() > 0) { ?>
                <div class="row">
                    <?php
                    foreach ($featured_seller->result() as $productListVal) {
                        $img = 'users/default_user.jpg';
                        if ($productListVal->logo_image != '') {
                            $img = 'store/' . $productListVal->logo_image;
                        } elseif ($productListVal->thumbnail != '') {
                            $img = 'users/' . $productListVal->thumbnail;
                        }
                        $store_name = $productListVal->brand_name;

                        if ($store_name == '') {
                            $store_name = $productListVal->full_name;
                        }
                        if ($store_name == '') {
                            $store_name = $productListVal->user_name;
                        }
                        if ($productListVal->store_payment != 'Paid') {
                            $href = "user/seller-timeline/" . $productListVal->user_name;
                        } else {
                            $href = "store/" . $productListVal->user_name;
                        }
                        if (file_exists("images/" . $img)) {
                            $imgPath = $img;
                        } else {
                            $imgPath = "noimage.jpg";
                        }
                        ?>
                        <div class="col-md-3">
                            <div class="ibox">
                                <div class="ibox-content product-box">
                                    <div class="figure-product-new mini">
                                        <div class="product-imitation">
                                            <a href="<?php echo $href; ?>">
                                                <img class="img-responsive" src="images/<?php echo $imgPath; ?>" />
                                            </a>
                                        </div>
                                        <div class="overlay">
                                            <div class="product-desc">
                                                <div class="text-center" style="color: white;">
                                                    <b><?php echo $store_name; ?></b>
                                                </div>
                                                <div class="small m-t-xs col-md-6 pull-left">
                                                    <a href="<?php echo $href; ?>">
                                                        <span class="username label">
                                                            <b class="price1">
                                                                <?php echo $productListVal->productCount; ?> <?php
                                                                if ($this->lang->line('products') != '') {
                                                                    echo stripslashes($this->lang->line('products'));
                                                                } else
                                                                    echo "products";
                                                                ?>
                                                            </b>
                                                        </span>
                                                    </a> 
                                                </div>
                                                <div class="text-righ col-md-6">
                                                    <a href="<?php echo $href; ?>" class="btn btn-xs btn-default pull-right">
                                                        Info <i class="fa fa-long-arrow-right"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>


        <?php if ($sellers_list->num_rows() > 0) { ?>
            <div class="row">
                <?php
                foreach ($sellers_list->result() as $productListVal) {
                    $img = 'users/default_user.jpg';
                    if ($productListVal->logo_image != '') {
                        $img = 'store/' . $productListVal->logo_image;
                    } elseif ($productListVal->thumbnail != '') {
                        $img = 'users/' . $productListVal->thumbnail;
                    }

                    if (file_exists("images/" . $img)) {
                        $imgPath = $img;
                    } else {
                        $imgPath = "noimage.jpg";
                    }

                    $store_name = $productListVal->brand_name;
                    if ($store_name == '') {
                        $store_name = $productListVal->full_name;
                    }
                    if ($store_name == '') {
                        $store_name = $productListVal->user_name;
                    }
                    ?>
                    <div class="col-md-3 m-b-lg">
                        <div class="ibox">
                            <div class="ibox-content product-box">
                                <div class="figure-product-new mini"> 
                                    <div class="product-imitation">
                                        <a href="store/<?php echo $productListVal->user_name; ?>">
                                            <img class="img-responsive" src="images/<?php echo $imgPath; ?>">
                                        </a>
                                    </div>
                                    <div class="overlay">
                                        <div class="product-desc">
                                            <div class="text-center" style="color: white;">
                                                <b><?php echo $store_name; ?></b>
                                            </div>    

                                            <div class="small m-t-xs col-md-6 pull-left">
                                                <a href="store/<?php echo $productListVal->user_name; ?>">
                                                    <span class="username label">
                                                        <b class="price"><?php echo $productListVal->ProductCount; ?> 
                                                            <?php
                                                            if ($this->lang->line('products') != '') {
                                                                echo stripslashes($this->lang->line('products'));
                                                            } else
                                                                echo "products";
                                                            ?>
                                                        </b>
                                                    </span>
                                                </a>
                                            </div>
                                            <div class="text-righ col-md-6">
                                                <a href="store/<?php echo $productListVal->user_name; ?>" class="btn btn-xs btn-default pull-right">
                                                    Info <i class="fa fa-long-arrow-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>


        <?php } else { ?>
            <div class="alert alert-info text-center noproducts">
                <?php
                if ($this->lang->line('no_shops_avail') != '') {
                    echo stripslashes($this->lang->line('no_shops_avail'));
                } else
                    echo "No shops available";
                ?>
            </div>
        <?php } ?>
    </div>
</div>
</div>
<a style="display: none;" href="#header" id="scroll-to-top">
    <span>
        <?php
        if ($this->lang->line('signup_jump_top') != '') {
            echo stripslashes($this->lang->line('signup_jump_top'));
        } else
            echo "Jump to top";
        ?>
    </span>
</a>
<script src="js/site/<?php echo SITE_COMMON_DEFINE; ?>shoplist.js" type="text/javascript"></script>

<?php $this->load->view('site/templates/footer'); ?>
