<?php $this->load->view('site/templates/header');?>

<style type="text/css">
ol.stream {
	position: relative;
}
ol.stream.use-css3 li.anim {
transition:all .25s;
-webkit-transition:all .25s;
-moz-transition:all .25s;
-ms-transition:all .25s;
	visibility:visible;
	opacity:1;
}
ol.stream.use-css3 li {
	visibility:hidden;
}
ol.stream.use-css3 li.anim.fadeout {
	opacity:0;
}
ol.stream.use-css3.fadein li {
	opacity:0;
}
ol.stream.use-css3.fadein li.anim.fadein {
	opacity:1;
}
.noproducts {
	float: left;
	width: 90%;
	padding: 5%;
	text-align: center;
	font-size: 25px;
	font-family: cursive;
}

.breadcrumbs{
        margin-bottom: 0px
    }

    .breadcrumbs li{
        float: left;
        margin-right: 5px;
    }

    .breadcrumbs li a{
        margin: 0px;
        color: #fff;
    }
</style>

<script type="text/javascript">
		var can_show_signin_overlay = false;
		if (navigator.platform.indexOf('Win') != -1) {document.write("<style>::-webkit-scrollbar, ::-webkit-scrollbar-thumb {width:7px;height:7px;border-radius:4px;}::-webkit-scrollbar, ::-webkit-scrollbar-track-piece {background:transparent;}::-webkit-scrollbar-thumb {background:rgba(255,255,255,0.3);}:not(body)::-webkit-scrollbar-thumb {background:rgba(0,0,0,0.3);}::-webkit-scrollbar-button {display: none;}</style>");}
	</script>
<!--[if lt IE 9]>
<script src="js/html5shiv/dist/html5shiv.js"></script>
<![endif]-->

<!-- header_start -->
<!-- header_end -->
<!-- Section_start -->
<div id="" class="redesign">
    <div class="col-lg-2 ui-sortable "></div>
    <div class="col-lg-8 ui-sortable" style="background:#fff none repeat scroll 0 0;">
  
		<div class="outside"> </div>

    <div class="">
    
     <div class="row wrapper border-bottom m-b" style="background: #18a689 none repeat scroll 0 0;padding: 10px;">
            <div class="col-lg-10">
                <h2 style="margin:0px;padding:0px;color: #fff !important;"> <?php
                    echo trim_slashes($breadCumps);
                    ?></h2>
            </div>

        </div>
	
		<div class="row">
            <div class="col-lg-12">
                <div class="contact-box">
                    <?php
                    if (isset($currBrand)) {
                        if ($currBrand->row()->brand_description != '' && $currBrand->row()->brand_logo != '') {
                            ?>
                            <a href="">
                                <div class="col-sm-4">
                                    <div class="text-center">
                                        <img class="img-responsive" alt="image" src="images/brand/<?php echo $currBrand->row()->brand_logo; ?>">

                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="">
        <?php echo $currBrand->row()->brand_description; ?>
                                    </div>


                                </div>
                                <div class="clearfix"></div>
                            </a>
                        <?php
                        }
                    }
                    ?>
                </div>
            </div>
      <div class="sns-right"> </div>
      
      <div id="content">
          <div class="row">
        <div class="search-frm">
          <div class="search">
          
           <div class="col-lg-12">
            <?php  if($brandslist->num_rows() > 0){ ?>
               <div class="col-sm-3">
            <select style="display: none;" class="form-control shop-select brands-filter selectBox">
                <option value="">All Brands</option>
              <?php foreach($brandslist->result() as $brands){ ?>
                <option value="<?php echo $brands->brand_seourl; ?>" <?php if(  $brands->brand_seourl == $this->input->get('b') ){ echo 'selected'; } ?> > <?php echo $brands->brand_name; ?></option>
              <?php } ?>
            </select>
               </div>
            <?php 
               }
            ?>
           
               <div class="col-sm-3">
             <?php echo $listSubCatSelBox;  ?>
               </div>
           
             
              <div class="col-sm-3">
            <select style="display: none;" class="form-control shop-select price-range selectBox">
             <option value=""><?php if($this->lang->line('product_any_price') != '') { echo stripslashes($this->lang->line('product_any_price')); } else echo "Any Price"; ?></option>	
              <?php foreach ($pricefulllist->result() as $priceRangeRow){ ?>
                 
               <option <?php if($_GET['p']==url_title($priceRangeRow->price_range)){ echo 'selected="selected"'; } ?> value="<?php echo url_title($priceRangeRow->price_range); ?>"><?php echo $currencySymbol;?> <?php echo $priceRangeRow->price_range; ?></option>
           <?php } ?>
            </select></div>
          
           <div class="col-sm-3">
			
			<select style="display: none;" class="form-control shop-select countries-filter selectBox">
              <option value="" selected="selected">Ships to</option>
              <?php 
                      foreach ($mainCountryLists->result() as $countryRow){
                      	//if ($colorRow->list_value != ''){
                      ?>
              <option <?php if($_GET['c']==url_title($countryRow->name)){ echo 'selected="selected"'; } ?> value="<?php echo $countryRow->name;?>"><?php echo $countryRow->name;?></option>
              <?php 
                      //	}
                      }
              ?>
            </select>
           </div></div>
              
              <div class="col-lg-12" style="margin-top:15px">
                  
                  
            <div class="col-sm-3">
            <select style="display: none;" class="shop-select sort-by-price selectBox">
              <option selected="selected" value=""><?php if($this->lang->line('product_newest') != '') { echo stripslashes($this->lang->line('product_newest')); } else echo "Newest"; ?></option>
              <option value="asc"><?php if($this->lang->line('product_low_high') != '') { echo stripslashes($this->lang->line('product_low_high')); } else echo "Price: Low to High"; ?></option>
              <option value="desc"><?php if($this->lang->line('product_high_low') != '') { echo stripslashes($this->lang->line('product_high_low')); } else echo "Price: High to Low"; ?></option>
            </select>
            </div>
                  <div class="col-sm-3">
			<select style="display: none; width:120px;" class="shop-select product-type selectBox">
              <option selected="selected" value="all"><?php if($this->lang->line('product_type_both') != '') { echo stripslashes($this->lang->line('product_type_both')); } else echo "All Products"; ?></option>
              <option value="affliate"><?php if($this->lang->line('product_type_aff') != '') { echo stripslashes($this->lang->line('product_type_aff')); } else echo "Affliate Products"; ?></option>
              <option value="seller"><?php if($this->lang->line('product_type_selp') != '') { echo stripslashes($this->lang->line('product_type_selp')); } else echo "Seller Products"; ?></option>
            </select>
                  </div>
                  
                  <div class="col-sm-3">
            <label for="immediateShipping" class="shipping-filter button-wrapper "> <span class="quick-shipping"> <span class="fa fa-truck"></span> </span>
                                
                                <input type="checkbox" value="true" name="is" id="immediateShipping"  />
                                <i class=""></i><b><?php if($this->lang->line('product_ship_immed') != '') { echo stripslashes($this->lang->line('product_ship_immed')); } else echo "Items that ship immediately"; ?></b> </label>
            
            
                  </div>
                  <div class="col-sm-3">
                      <input style="width:220px;" class="form-control search-string" type="text"  placeholder="<?php if($this->lang->line('product_filter_key') != '') { echo stripslashes($this->lang->line('product_filter_key')); } else echo "Filter by keyword"; ?>" />
                      <span class="label"><i class="fa fa-search"></i><em class="hidden"><?php if($this->lang->line('header_search') != '') { echo stripslashes($this->lang->line('header_search')); } else echo "Search"; ?></em></span> 
            <a href="#" class="del-val"><i class="fa fa-remove"></i><em class="hidden"><?php if($this->lang->line('shipping_delete') != '') { echo stripslashes($this->lang->line('shipping_delete')); } else echo "Delete"; ?></em></a>
                  </div>
              </div>
          </div>
          <div class="sorting"> </div>
        </div>
              
          </div>	
		
        <?php if(count($productList)>0){?>
        <ol class="stream">
		<?php if(isset($featuredList)){if($featuredList->num_rows()>0){ ?>
         <?php foreach ($featuredList->result() as $_featuredList) {
        	$img = 'dummyProductImage.jpg';
			$imgArr = explode(',', $_featuredList->image);
			if (count($imgArr)>0){
				foreach ($imgArr as $imgRow){
					if ($imgRow != ''){
						$img = $pimg = $imgRow;
						break;
					}
				}
			}
	        $fancyClass = 'fancy';
			$fancyText = LIKE_BUTTON;
			if (count($likedProducts)>0 && $likedProducts->num_rows()>0){
				foreach ($likedProducts->result() as $likeProRow){
					if ($likeProRow->product_id == $_featuredList->seller_product_id){
						$fancyClass = 'fancyd';$fancyText = LIKED_BUTTON;break;
					}
				}
			}
		?>
          <li>
            <div class="figure-product-new mini"> <a href="things/<?php echo $_featuredList->id;?>/<?php echo url_title($_featuredList->product_name,'-');?>">
              <figure><img src="images/product/<?php echo $img; ?>"> </figure>
              <figcaption><?php echo $_featuredList->product_name;?></figcaption>
              </a>
             
			 <div class="offer-tag">
									<p class="off-price">featured</p>
								</div>
			 
			  <span class="username"><b class="price"><?php echo $currencySymbol;?><?php echo $_featuredList->sale_price;?></b></span>
               <a href="#" class="button <?php echo $fancyClass;?>" tid="<?php echo $_featuredList->seller_product_id;?>" <?php if ($loginCheck==''){?>require_login="true"<?php }?> item_img_url="images/product/<?php echo $img;?>"><span><i></i></span><?php echo $fancyText;?></a> </div>
          </li>
         <?php } }?>
		
		<?php } ?>
         <?php foreach($productList as $productListVal){
        	$img = 'dummyProductImage.jpg';
			$imgArr = explode(',', $productListVal->image);
			if (count($imgArr)>0){
				foreach ($imgArr as $imgRow){
					if ($imgRow != ''){
						$img = $pimg = $imgRow;
						break;
					}
				}
			}
	        $fancyClass = 'fancy';
			$fancyText = LIKE_BUTTON;
			if (count($likedProducts)>0 && $likedProducts->num_rows()>0){
				foreach ($likedProducts->result() as $likeProRow){
					if ($likeProRow->product_id == $productListVal->seller_product_id){
						$fancyClass = 'fancyd';$fancyText = LIKED_BUTTON;break;
					}
				}
			}
		?>
          <li>
            <div class="figure-product-new mini new_featured"> 
			<?php
				if (isset($productListVal->web_link)) {
					$prodLink = "user/" . $productListVal->user_name . "/things/" . $productListVal->seller_product_id . "/" . url_title($productListVal->product_name, '-');
				} else {
					$prodLink = "things/" . $productListVal->id . "/" . url_title($productListVal->product_name, '-');
				}
			?>
			<a href="<?php echo $prodLink; ?>">
              <figure><img src="images/product/<?php echo $img; ?>"> </figure>
              <figcaption><?php echo $productListVal->product_name;?></figcaption>
      <?php if($productListVal->product_type=='digital'){?>
      <div class="offer-tag">
            <p class="off-price">Digital</p>
          </div>
       <?php }?>   
			</a> 
			  <?php if($productListVal->sale_price!=''){?>
					<span class="username">
						<b class="price">
							<?php echo $currencySymbol;?><?php echo $productListVal->sale_price;?>
						</b>
					</span>
			  <?php } ?>
               <a href="#" class="button <?php echo $fancyClass;?>" tid="<?php echo $productListVal->seller_product_id;?>" <?php if ($loginCheck==''){?>require_login="true"<?php }?> item_img_url="images/product/<?php echo $img;?>"><span><i></i></span><?php echo $fancyText;?></a> </div>
          </li>
         <?php } ?>
        </ol>
        
        <div class="pagination" style="display:none">
                    <?php echo $paginationDisplay; ?>
        </div>
        <?php }else {?>
        <ol class="stream">
        <li style="width: 100%;"><p class="noproducts"><?php if($this->lang->line('product_no_more') != '') { echo stripslashes($this->lang->line('product_no_more')); } else echo "No more products available"; ?></p></li>
        </ol>
        <?php }?>
      </div>
      <a style="display: none;" href="#header" id="scroll-to-top"><span><?php if($this->lang->line('signup_jump_top') != '') { echo stripslashes($this->lang->line('signup_jump_top')); } else echo "Jump to top"; ?></span></a>
    <!-- / container -->
  </div>
  <!-- / container -->
<div class="col-lg-2 ui-sortable "></div>
<script src="js/site/<?php echo SITE_COMMON_DEFINE ?>shoplist.js" type="text/javascript"></script>
<script>
    
    $(document).ready(function () {
        $(".shop-select").select2();
        
    });
	jQuery(function($) {
		var $select = $('.gift-recommend select.select-round');
		$select.selectBox();
		$select.each(function(){
			var $this = $(this);
			if($this.css('display') != 'none') $this.css('visibility', 'visible');
		});
	});
</script>
<script>
    //emulate behavior of html5 textarea maxlength attribute.
    jQuery(function($) {
        $(document).ready(function() {
            var check_maxlength = function(e) {
                var max = parseInt($(this).attr('maxlength'));
                var len = $(this).val().length;
                if (len > max) {
                    $(this).val($(this).val().substr(0, max));
                }
                if (len >= max) {
                    return false;
                }
            }
            $('textarea[maxlength]').keypress(check_maxlength).change(check_maxlength);
            
            
        });
    });
</script>
<style type="text/css">
.shop .list .stream .figure-product-new.mini {
    min-height: 260px;
}
</style>
<?php
$this->load->view('site/templates/footer');
?>
