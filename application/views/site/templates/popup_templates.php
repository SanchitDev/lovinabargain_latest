<!-- popups -->
<div id="popup_container"> <img class="loader" src="images/site/loading.gif">
  <div class="popup ly-title reply-popup"> </div>
  
<?php 
$this->load->view('site/popup/register');
$this->load->view('site/popup/auction');
$this->load->view('site/popup/conditions');
$this->load->view('site/popup/shipping');
$this->load->view('site/popup/product');
$this->load->view('site/popup/fancyybox');
$this->load->view('site/popup/list');
$this->load->view('site/popup/gift_recommend');
$this->load->view('site/popup/comment');
$this->load->view('site/popup/upload');
$this->load->view('site/popup/share');
$this->load->view('site/popup/contact_seller');
$this->load->view('site/popup/feature_product');
/****************seller dashboard**********************/
$this->load->view('site/store/store_about');
$this->load->view('site/store/store_privacy');
$this->load->view('site/store/store_contact');

/*************seller dashboard*******************/

/********************GROUP GIFT*********************/
$this->load->view('site/groupgifts/recipient');
$this->load->view('site/groupgifts/personalize');
$this->load->view('site/groupgifts/contributors');
$this->load->view('site/groupgifts/edit_groupgift_product');
/***************************************************/
?>

</div>
<!-- /popups -->