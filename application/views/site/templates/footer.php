
<footer class="footer-section">
	<div class="footer-main">
		<p>&copy; Copyright Lovin A Bargain All rights reserved.</p>
	</div>
</footer><!--/ footer-section -->

<!-- #Scrips 	=================================-->
<script src="js/video/video-bg.js"></script>
<script src="js/video/video-main.js"></script>

<script src="js/menu/cbpHorizontalSlideOutMenu.min.js"></script>
<script>
	var menu = new cbpHorizontalSlideOutMenu( document.getElementById( 'cbp-hsmenu-wrapper' ) );
</script>

<script type="text/javascript">
	$( document ).ready(function() {
		$(window).scroll(function() {    
			var scroll = $(window).scrollTop();

			if (scroll >= 200) {
				$(".header-section").addClass("fixed");
			} else {
				$(".header-section").removeClass("fixed");
			}
		});
	});
</script>

</body>
</html>