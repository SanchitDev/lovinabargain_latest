<?php
// ini_set("display_errors", "1");
// error_reporting(E_ALL)
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
    <head>
        <meta charset="utf-8">
        <!-- Mobile Specific Metas
        ================================================== -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <?php
        if ($this->config->item('google_verification')) {
            echo stripslashes($this->config->item('google_verification'));
        }
        if ($heading == '') {
            ?>
            <title><?php echo $title; ?></title>
        <?php } else { ?>
            <title><?php echo $heading; ?></title>
        <?php } ?>
        <meta name="Title" content="<?php echo $meta_title; ?>" />
        <meta name="keywords" content="<?php echo $meta_keyword; ?>" />
        <meta name="description" content="<?php echo $meta_description; ?>" />
        <base href="<?php echo base_url(); ?>" />
        <!--CSS
        ================================================== -->                  
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>images/logo/<?php echo $fevicon; ?>"/>

        <!-- Loading Css Files -->
        <?php $this->load->view('site/templates/css_files'); ?>


        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- Loading Script Files -->
        <?php $this->load->view('site/templates/script_files'); ?>
        <!-- Favicons
            ================================================== -->
        <link rel="shortcut icon" href="images/favicon.ico">
        <!--[if gte IE 9]>
              <style type="text/css">
                .gradient {
                   filter: none;
                }
              </style>
              <![endif]-->
        <!-- JS
            ================================================== -->

    </head>
    <body>
        <header class="header-section">
            <div class="header-main">
                <div class="mobile-brand-box">
                    <h1 class="logo">Logo Here</h1>
                </div>
                <div class="left-box">
                    <ul class="languagepicker roundborders large">
                        <a href="lang/en"><li><img src="http://i64.tinypic.com/fd60km.png"/>English</li></a>
                        <a href="lang/de"><li><img src="http://i63.tinypic.com/10zmzyb.png"/>German</li></a>
                        <a href="lang/fr"><li><img src="http://i65.tinypic.com/300b30k.png"/>Français</li></a>
                        <a href="lang/es"><li><img src="http://i68.tinypic.com/avo5ky.png"/>Español</li></a>
                        <a href="lang/nl"><li><img src="http://i65.tinypic.com/2d0kyno.png"/>Nederlands</li></a>
                        <a href="lang/it"><li><img src="http://i65.tinypic.com/23jl6bn.png"/>Italiano</li></a>
                    </ul>
                </div>
                <div class="right-box">
                    <div id="navigation-test">
                        <div class="search">
                            <form action="shopby/all" class="search">
                                <fieldset class="hastext header-top">
                                    <div class="searchbar">
                                        <input type="text" name="q" class="text searchTerm" id="search-query" placeholder="Search" value="" autocomplete="off">
                                        <button type="submit" class="searchButton">
                                            <img src="images/icon/search.png">
                                        </button>
                                    </div>                                
                                    <div class="feed-search"><h4>Suggestions</h4>
                                        <ul class="user" style="display: block;">
                                            <li>
                                                <a href="user/sdfasfsa" class="hover"><img src="images/users/user-thumb1.png" alt="sdfasfsa" class="photo"> <b>sdfasfsa</b> (sdfasfsa)</a>
                                            </li>
                                        </ul>

                                        <a href="http://localhost/lovinabargain_working/shopby/all?q=fsa" class="more">See full search results</a>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                        <ul class="list-items">
                            <?php if ($loginCheck == '') { ?>
                                <li><a href="login"><img src="images/icon/man-user.png" /></a></li>
                            <?php } ?>
                            <?php if ($loginCheck != '') { ?>
                                <li>                            
                                    <a href="cart"><img src="images/icon/cart.png" /></a>
                                </li>
                                <li>
                                    <ul class="gnb-wrap new-notify">
                                        <li class="gnb none gnb-notification">
                                            <a href="notifications" class="mn-notification">
                                                <img src="images/icon/notifications.png" />
                                            </a>
                                            <div class="feed-notification">
                                                <i class="arrow"></i>
                                                <h4><?php
                                                    if ($this->lang->line('referrals_notification') != '') {
                                                        echo stripslashes($this->lang->line('referrals_notification'));
                                                    } else
                                                        echo "Notifications";
                                                    ?></h4>
                                                <div class="loading"><i></i></div>
                                                <ul>
                                                    <li>
                                                        <a href="">
                                                            <img src="" class="photo"/>
                                                        </a>

                                                        <a href="">
                                                            <img src="" class="thing"/>
                                                        </a>
                                                    </li>
                                                </ul>
                                                <a href="notifications" class="moreFeed"><?php
                                                    if ($this->lang->line('see_all_noty') != '') {
                                                        echo stripslashes($this->lang->line('see_all_noty'));
                                                    } else
                                                        echo "See all notifications";
                                                    ?></a>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="auctioncart"><img src="images/icon/auction.png" /></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </header><!--/ header-section -->


        <section class="videobg-section">
            <div class="videobg-main">
                <video id="background_video" loop muted></video>
                <div id="video_cover"></div>
                <div id="overlay"></div>

                <div id="video_controls">
                    <span id="play">
                        <img src="images/play.png">
                    </span>
                    <span id="pause">
                        <img src="images/pause.png">
                    </span>
                </div>
            </div>
        </section>
        <section class="sidebar-section">
            <div class="sidebar-inner">

                <div class="brand-box">
                    <a href="<?php echo base_url(); ?>"><h1 class="logo">Logo Here</h1></a>
                </div>

                <nav class="cbp-hsmenu-wrapper" id="cbp-hsmenu-wrapper">
                    <div class="cbp-hsinner">
                        <ul class="cbp-hsmenu">
                            <li>
                                <a href="#">Shope by department</a>
                                <ul class="cbp-hssubmenu shop-listing">                            
                                    <?php
                                    foreach ($mainCategories->result() as $row) {
                                        if ($row->cat_name != '' && $row->status == 'Active') {
                                            $imgSrc = 'images/img-1.jpg';
                                            if ($row->image != '') {
                                                $imgSrc = 'images/category/' . $row->image;
                                            }
                                            ?>
                                            <li>
                                                <a href="shopby/<?php echo $row->seourl; ?>">
                                                    <div class="image-box">
                                                        <img src="<?php echo base_url() . $imgSrc; ?>" />
                                                    </div>
                                                    <div class="box-overlay"></div>
                                                    <div class="box-text">
                                                        <p><?php echo $row->cat_name; ?></p>
                                                    </div>
                                                </a>
                                            </li>
                                            <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Discover</a>
                                <ul class="cbp-hssubmenu discover-listing">
                                    <li>
                                        <a <?php if ($loginCheck != '') { ?>class="mn-add" <?php } ?> href="<?php
                                        if ($loginCheck != '') {
                                            echo 'add';
                                        } else {
                                            echo 'login';
                                        }
                                        ?>">
                                            <span class="text">Add</span>
                                            <span class="icon"><img src="images/icon/add.png"</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="invite-friends">
                                            <span class="text"><?php echo mirage_lg('onboarding_invite_frd', "Invite Friends") ?></span>
                                            <span class="icon"><img src="images/icon/add-user.png"</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php
                                        if ($loginCheck != '') {
                                            if ($userDetails->row()->group == "User") {
                                                echo 'user/' . $userDetails->row()->user_name;
                                            } else {
                                                echo 'user/seller-timeline/' . $userDetails->row()->user_name;
                                            }
                                        } else {
                                            echo 'login';
                                        }
                                        ?>">
                                            <span class="text">Profiles</span>
                                            <span class="icon"><img src="images/icon/man-user.png"</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="bookmarklets">
                                            <span class="text"><?php
                                                if ($this->lang->line('bookmarklets') != '') {
                                                    echo stripslashes($this->lang->line('bookmarklets'));
                                                } else
                                                    echo "Bookmarklets";
                                                ?></span>
                                            <span class="icon"><img src="images/icon/book.png"</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="#">Stores</a>
                                <ul class="cbp-hssubmenu stores-listing">
                                    <?php
                                    if ($sellers_list->num_rows() > 0) {
                                        foreach ($sellers_list->result() as $productListVal) {
                                            $img = 'img-1.jpg';
                                            if ($productListVal->logo_image != '') {
                                                $img = 'store/' . $productListVal->logo_image;
                                            } elseif ($productListVal->thumbnail != '') {
                                                $img = 'users/' . $productListVal->thumbnail;
                                            }
                                            $store_name = $productListVal->brand_name;
                                            if ($store_name == '') {
                                                $store_name = $productListVal->full_name;
                                            }
                                            if ($store_name == '') {
                                                $store_name = $productListVal->user_name;
                                            }
                                            ?>
                                            <li class="item-box">
                                                <a href="#">
                                                    <div class="item-media">
                                                        <span class="background-overlay-img"></span>
                                                        <img src="images/<?php echo $img; ?>" />
                                                        <div class="box-text"> <span><?php echo $productListVal->ProductCount; ?></span> </div>
                                                    </div>
                                                    <div class="product-title"><h4><?php echo $store_name; ?></h4></div>
                                                </a>
                                            </li>

                                            <?php
                                        }
                                    }
                                    ?>

                                </ul>
                            </li>  

                            <li>
                                <a href="#">Brands</a>
                                <ul class="cbp-hssubmenu brands-listing">
                                    <?php
                                    if ($brandslist->num_rows() > 0) {
                                        foreach ($brandslist->result() as $brands) {
                                            ?>
                                            <li>
                                                <a href="shopby/all?b=<?php echo $brands->brand_seourl; ?>">
                                                    <img src="images/brand/<?php echo $brands->brand_logo; ?>" alt="img10"/>
                                                </a>
                                            </li>
                                            <?php
                                        }
                                    }
                                    ?>                                                                
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>

            </div>
        </section>