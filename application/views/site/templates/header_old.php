<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <?php
        if ($this->config->item('google_verification')) {
            echo stripslashes($this->config->item('google_verification'));
        }
        if ($heading == '') {
            ?>
            <title><?php echo $title; ?></title>
        <?php } else { ?>
            <title><?php echo $heading; ?></title>
        <?php } ?>
        <meta name="Title" content="<?php echo $meta_title; ?>" />
        <meta name="keywords" content="<?php echo $meta_keyword; ?>" />
        <meta name="description" content="<?php echo $meta_description; ?>" />
        <base href="<?php echo base_url(); ?>" />
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>images/logo/<?php echo $fevicon; ?>"/>

        <!-- Loading Css Files -->
        <?php $this->load->view('site/templates/css_files'); ?>

        <!-- Loading Script Files -->
        <?php $this->load->view('site/templates/script_files'); ?>

        <!-- Loading Theme Settings-->
        <?php $this->load->view('site/templates/theme_settings'); ?>

    </head>
    <body>

        <!-- header_start -->
        <header>
            <div class="header_top">
                <?php
                $pricesval = $pricefulllist->result_array();
                $ColorsListVal = $mainColorLists->result_array();
                $CountryListVal = $mainCountryLists->result_array();
                ?>

                <?php
                $by_creating_accnt = str_replace("{SITENAME}", $siteTitle, $this->lang->line('header_create_acc'));
                ?>
                <div class="main">
                    <div id="navigation-test">
                        <div class="header-top">
                            <h1 class="logo"><a href="<?php echo base_url(); ?>" alt="<?php echo $siteTitle; ?>" title="<?php echo $siteTitle; ?>"><img src="images/logo/<?php echo $logo; ?>"/></a></h1>
                            <form action="<?php base_url(); ?>shopby/all" class="search">
                                <fieldset>
                                    <input type="text" name="q" class="text" id="search-query" placeholder="<?php
                                    if ($this->lang->line('header_search') != '') {
                                        echo stripslashes($this->lang->line('header_search'));
                                    } else
                                        echo "Search";
                                    ?>" value="" autocomplete="off"/>
                                    <button type="submit" class="btn-submit"><?php
                                        if ($this->lang->line('header_search') != '') {
                                            echo stripslashes($this->lang->line('header_search'));
                                        } else
                                            echo "Search";
                                        ?></button>
                                    <div class="feed-search" style="display: none;">
                                        <h4><?php
                                            if ($this->lang->line('header_suggestions') != '') {
                                                echo stripslashes($this->lang->line('header_suggestions'));
                                            } else
                                                echo "Suggestions";
                                            ?></h4>
                                        <div class="loading" style="display: block;"><i></i>
                                            <img class="loader" src="images/site/loading.gif"/>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                            <div class="new-main-menu header-language">
                                <ul>
                                    <li>
                                        <span>Language<i></i>

                                            <ul class="drop-down drop-dowm-small">
                                                <?php
                                                if ($selectedLanguage == '') {
                                                    $selectedLanguage = $defaultLg[0]['lang_code'];
                                                }
                                                if (count($activeLgs) > 0) {
                                                    foreach ($activeLgs as $activeLgsRow) {
                                                        ?>
                                                        <li><a href="lang/<?php echo $activeLgsRow['lang_code']; ?>" <?php
                                                            if ($selectedLanguage == $activeLgsRow['lang_code']) {
                                                                echo 'class="selected"';
                                                            }
                                                            ?>><?php echo $activeLgsRow['name']; ?></a></li>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                            </ul>
                                        </span>
                                    </li>
                                </ul>
                            </div> 
                        </div>

                    </div>
                </div>
                <div class="header-bottom">
                    <div class="main">
                        <div id="navigation-test">
                            <div class="new-main-menu">
                                <ul>
                                    <li>
                                        <span>Shop By Department<i></i>
                                            <ul class="drop-down bigger-menu">
                                                <!--<li>
                                                  <span>Edo Videos</span>
                                                  <div class="big-menu">
                                                    <div class="big-menu-left">
                                                      <h2>Sports</h2>
                                                      <ul>
                                                        <li><a href="#">
                                                          <span class="head-title">Athletic Clothing</span>
                                                        </a></li>
                                                        <li><a href="#">
                                                          <span class="head-title">Exercise & Fitness</span>
                                                        </a></li>
                                                        <li><a href="#">
                                                          <span class="head-title">Hunting & Fishing</span>
                                                        </a></li>
                                                        <li><a href="#">
                                                          <span class="head-title">Team Sports</span>
                                                        </a></li>
                                                        <li><a href="#">
                                                          <span class="head-title">Fan Shop</span>
                                                        </a></li>
                                                        <li><a href="#">
                                                          <span class="head-title">Leisure Sports & Game Room</span>
                                                        </a></li>
                                                      </ul>
                                                    </div>
                                                    <div class="big-menu-right">
                                                      <h2>Outdoors</h2>
                                                      <ul>
                                                        <li><a href="#">
                                                          <span class="head-title">Camping & Hiking</span>
                                                        </a></li>
                                                        <li><a href="#">
                                                          <span class="head-title">Cycling</span>
                                                        </a></li>
                                                        <li><a href="#">
                                                          <span class="head-title">Outdoor Clothing</span>
                                                        </a></li>
                                                        <li><a href="#">
                                                          <span class="head-title">Scooters, Skateboards & Skates</span>
                                                        </a></li>
                                                        <li><a href="#">
                                                          <span class="head-title">Water Sports</span>
                                                        </a></li>
                                                        <li><a href="#">
                                                          <span class="head-title">Climbing</span>
                                                        </a></li>
                                                      </ul>               
                                                    </div>
                                                    <div class="header-bg">
                                                      <img src="images/site/menu-bg.png">
                                                    </div>
                                                  </div>
                                                </li>-->
                                                <?php
                                                foreach ($mainCategories->result() as $row) {
                                                    if ($row->cat_name != '' && $row->status == 'Active') {
                                                        $imgSrc = 'images/category/white-box.jpg';
                                                        if ($row->image != '') {
                                                            $imgSrc = 'images/category/' . $row->image;
                                                        }
                                                        ?>
                                                        <li>
                                                            <span><?php echo $row->cat_name; ?></span>
                                                            <div class="big-menu">
                                                                <div class="big-menu-left">
                                                                    <?php
                                                                    $sub_category = $this->db->query('select * from ' . CATEGORY . ' where `rootID` = "' . $row->id . '"  and `status`="Active" order by cat_position asc limit 1');
                                                                    foreach ($sub_category->result() as $sub_cat) {
                                                                        if ($sub_cat->cat_name != '') {
                                                                            ?>
                                                                            <h2><?php echo $sub_cat->cat_name; ?></h2>

                                                                            <ul>
                                                                                <?php
                                                                                $subcat = $this->db->query('select * from ' . CATEGORY . ' where `rootID` = "' . $sub_cat->id . '"  and `status`="Active" order by cat_position asc limit 6');

                                                                                foreach ($subcat->result() as $sub_cat2) {
                                                                                    if ($sub_cat2->cat_name != '') {
                                                                                        ?>
                                                                                        <li>
                                                                                            <a href="shopby/<?php echo $sub_cat2->seourl; ?>">
                                                                                                <span class="head-title"><?php echo $sub_cat2->cat_name; ?></span>
                                                                                            </a>
                                                                                        </li>
                                                                                        <?php
                                                                                    }
                                                                                }
                                                                                ?>

                                                                            </ul>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </div>

                                                                <div class="big-menu-right">
                                                                    <?php
                                                                    $sub_category = $this->db->query('select * from ' . CATEGORY . ' where `rootID` = "' . $row->id . '"  and `status`="Active" order by cat_position asc limit 1,1');
                                                                    foreach ($sub_category->result() as $sub_cat) {
                                                                        if ($sub_cat->cat_name != '') {
                                                                            ?>
                                                                            <h2><?php echo $sub_cat->cat_name; ?></h2>
                                                                            <ul>
                                                                                <?php
                                                                                $subcat = $this->db->query('select * from ' . CATEGORY . ' where `rootID` = "' . $sub_cat->id . '"  and `status`="Active" order by cat_position asc limit 6');

                                                                                foreach ($subcat->result() as $sub_cat2) {
                                                                                    if ($sub_cat2->cat_name != '') {
                                                                                        ?>
                                                                                        <li>
                                                                                            <a href="shopby/<?php echo $sub_cat2->seourl; ?>">
                                                                                                <span class="head-title"><?php echo $sub_cat2->cat_name; ?></span>
                                                                                            </a>
                                                                                        </li>
                                                                                        <?php
                                                                                    }
                                                                                }
                                                                                ?>
                                                                            </ul>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>                
                                                                </div>

                                                                <div class="header-bg">
                                                                    <a href="shopby/<?php echo $row->seourl; ?>">
                                                                      <!-- <img src="images/site/menu-bg.png"> -->
                                                                        <img src="<?php echo base_url() . $imgSrc; ?>"> 
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </ul>
                                        </span>
                                    </li>


                                    <li>
                                        <span><a href="shop">Discover<i></i></a>
                                            <ul class="drop-down drop-dowm-small">
                                                <li>
                                                    <a class="<?php echo ($loginCheck != '') ? "mn-add" : ""; ?>" href="<?php echo ($loginCheck != '') ? 'add' : 'login'; ?>">Add</a>
                                                </li>
                                                <li><a href="invite-friends"><?php echo mirage_lg('onboarding_invite_frd', "Invite Friends") ?></a></li>
                                                <li><a href="<?php
                                                    if ($loginCheck != '') {
                                                        if ($userDetails->row()->group == "User") {
                                                            echo 'user/' . $userDetails->row()->user_name;
                                                        } else {
                                                            echo 'user/seller-timeline/' . $userDetails->row()->user_name;
                                                        }
                                                    } else {
                                                        echo 'login';
                                                    }
                                                    ?>">Profile</a></li>
                                                <li><a href="bookmarklets"><?php
                                                        if ($this->lang->line('bookmarklets') != '') {
                                                            echo stripslashes($this->lang->line('bookmarklets'));
                                                        } else
                                                            echo "Bookmarklets";
                                                        ?></a></li>

                                                <?php /*
                                                  if ($fancyBoxCount>0){
                                                  ?>
                                                  <li><a href="fancybox"><?php if($this->lang->line('header_fancybox') != '') { echo stripslashes($this->lang->line('header_fancybox')); } else echo "Fancy Box"; ?></a></li>
                                                  <?php
                                                  }
                                                  if ($this->config->item('giftcard_status') == 'Enable'){
                                                  ?>
                                                  <li><a href="gift-cards"><?php if($this->lang->line('giftcard_cards') != '') { echo stripslashes($this->lang->line('giftcard_cards')); } else echo "Gift Cards"; ?></a></li>
                                                  <?php
                                                  }
                                                  if($loginCheck != ''){
                                                  ?>
                                                  <li><a href="bookmarklets"><?php if($this->lang->line('bookmarklets') != '') { echo stripslashes($this->lang->line('bookmarklets')); } else echo "Bookmarklets"; ?></a></li>
                                                  <?php }?>

                                                  <li>
                                                  <span><a href="shopby/all?p=<?php echo url_title($pricesval[0]['price_range']); ?>"><i class="arrow"></i><?php if($this->lang->line('header_by_price') != '') { echo stripslashes($this->lang->line('header_by_price')); } else echo "By Price"; ?></a></span>
                                                  <div class="big-menu big-menu-small">
                                                  <ul>
                                                  <?php foreach ($pricefulllist->result() as $priceRangeRow){ ?>
                                                  <li><a href="shopby/all?p=<?php echo url_title($priceRangeRow->price_range); ?>"><?php echo $currencySymbol;?> <?php echo ucfirst($priceRangeRow->price_range);  ?></a></li>
                                                  <?php } ?>
                                                  </ul>
                                                  </div>
                                                  </li>
                                                  <?php if ($mainCountryLists->num_rows()>0){ ?>
                                                  <li>
                                                  <span><a class="color-red" href="shopby/all?c=<?php echo $CountryListVal[0]['name'];?>"><i class="arrow"></i>By Country</a></span>
                                                  <div class="big-menu big-menu-small country-menu-small">
                                                  <ul>
                                                  <?php
                                                  foreach ($mainCountryLists->result() as $countryRow){
                                                  if ($countryRow->name != ''){
                                                  ?>
                                                  <li><a href="shopby/all?c=<?php echo strtolower($countryRow->name);?>"><i class="color <?php echo strtolower($countryRow->name);?>"></i> <?php echo ucfirst($countryRow->name);?></a></li>
                                                  <?php
                                                  }
                                                  }
                                                  ?>
                                                  </ul>
                                                  </div>
                                                  </li>
                                                  <?php }
                                                  ?>

                                                  <?php if ($mainCategories->num_rows()>0){ ?>
                                                  <li>
                                                  <span><a href="shopby/all"><i class="arrow"></i><?php if($this->lang->line('header_by_category') != '') { echo stripslashes($this->lang->line('header_by_category')); } else echo "By Category"; ?></a></span>
                                                  <div class="big-menu big-menu-small">
                                                  <ul>
                                                  <?php
                                                  foreach ($mainCategories->result() as $row){
                                                  if ($row->cat_name != ''){
                                                  ?>
                                                  <li><span><a href="shopby/<?php echo $row->seourl;?>"><?php echo $row->cat_name;?></a></span>
                                                  <?php if (in_array($row->id, $root_id_arr)){?>
                                                  <div class="big-menu big-menu-small">
                                                  <ul>
                                                  <?php
                                                  foreach ($all_categories->result() as $row1){
                                                  if ($row1->cat_name != '' && $row->id==$row1->rootID){
                                                  ?>
                                                  <li><span><a href="shopby/<?php echo $row->seourl;?>/<?php echo $row1->seourl;?>"><?php echo $row1->cat_name;?></a></span>
                                                  <?php if (in_array($row1->id, $root_id_arr)){?>
                                                  <div class="big-menu big-menu-small">
                                                  <ul>
                                                  <?php
                                                  foreach ($all_categories->result() as $row2){
                                                  if ($row2->cat_name != '' && $row1->id==$row2->rootID){
                                                  ?>
                                                  <li><span><a href="shopby/<?php echo $row->seourl;?>/<?php echo $row1->seourl;?>/<?php echo $row2->seourl;?>"><?php echo $row2->cat_name;?></a></span>
                                                  <?php if (in_array($row2->id, $root_id_arr)){?>
                                                  <div class="big-menu big-menu-small last_cat_small">
                                                  <ul>
                                                  <?php
                                                  foreach ($all_categories->result() as $row3){
                                                  if ($row3->cat_name != '' && $row2->id==$row3->rootID){
                                                  ?>
                                                  <li><span><a href="shopby/<?php echo $row->seourl;?>/<?php echo $row1->seourl;?>/<?php echo $row2->seourl;?>/<?php echo $row3->seourl;?>"><?php echo $row3->cat_name;?></a></span>
                                                  <?php if (in_array($row3->id, $root_id_arr)){?>
                                                  <div class="big-menu big-menu-small ">
                                                  <ul>
                                                  <?php
                                                  foreach ($all_categories->result() as $row4){
                                                  if ($row4->cat_name != '' && $row3->id==$row4->rootID){
                                                  ?>
                                                  <li><a href="shopby/<?php echo $row->seourl;?>/<?php echo $row1->seourl;?>/<?php echo $row2->seourl;?>/<?php echo $row3->seourl;?>/<?php echo $row4->seourl;?>"><?php echo $row4->cat_name;?></a>
                                                  </li>
                                                  <?php
                                                  }
                                                  }
                                                  ?>
                                                  </ul>
                                                  </div>
                                                  <?php }?>
                                                  </li>
                                                  <?php
                                                  }
                                                  }
                                                  ?>
                                                  </ul>
                                                  </div>
                                                  <?php }?>
                                                  </li>
                                                  <?php
                                                  }
                                                  }
                                                  ?>
                                                  </ul>
                                                  </div>
                                                  <?php }?>
                                                  </li>
                                                  <?php
                                                  }
                                                  }
                                                  ?>
                                                  </ul>
                                                  </div>
                                                  <?php } ?>
                                                  </li>
                                                  <?php
                                                  }
                                                  }
                                                  ?>
                                                  </ul>
                                                  </div>
                                                  </li>
                                                  <?php } */
                                                ?>

                                            </ul>
                                        </span>
                                    </li>

                                    <li><a href="stores">Stores</a></li>
                                    <?php if ($loginCheck != '' && $userDetails->row()->group == "Seller") { ?>
                                        <li><a href="seller-product">Sell Something</a></li>   
                                    <?php } ?>

                                    <?php if ($brandslist->num_rows() > 0) { ?>
                                        <li>
                                            <span>Brands<i></i>
                                                <ul class="drop-down brands-down brands-length">
                                                    <li><span><input type="text" placeholder="Enter search text here.." style="font-color:black;"class="brand-search" id="brandsearch"></span></li>
                                                    <div id="brandslist">
                                                        <?php
                                                        $k = '0';
                                                        $sm = '0';
                                                        foreach ($brandslist->result() as $brand) {

                                                            foreach (range('a', 'z') as $char) {
                                                                if ($char == lcfirst($brand->brand_name[0])) {
                                                                    if ($k != $char) {
                                                                        ?>
                                                                        <span style="color: black;" class="letter" ><?php
                                                                            echo ucfirst($char);
                                                                            $k = $char;
                                                                            $temp = 0;
                                                                            ?></span>
                                                                    <?php } ?>
                                                                    <li class="<?php
                                                                    if ($temp < 3) {
                                                                        echo '';
                                                                    } else {
                                                                        echo 'hidebrand brandhide_' . $char;
                                                                    }
                                                                    ?>">
                                                                        <span><a href="shopby/all?b=<?php echo $brand->brand_seourl; ?>"><?php echo $brand->brand_name; ?></a>
                                                                            <div class="brand-hover-img">
                                                                                <a href="shopby/all?b=<?php echo $brand->brand_seourl; ?>"><img src="images/brand/<?php echo $brand->brand_logo; ?>"></a>
                                                                            </div>
                                                                        </span>
                                                                    </li>
                                                                    <?php if ($temp >= 3 && $sm != $char) { ?>
                                                                                                                                <span style="color:black;" class="seemore_link seemore_<?php echo $char; ?>" ><a href="a-to-z-of-brands" >See more</a></span> <!-- onclick="show_brands('<?php echo $char; ?>');"-->
                                                                        <?php
                                                                        $sm = $char;
                                                                    }
                                                                    ?>

                                                                    <?php
                                                                    $temp++;
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </div>
                                                    <img id="brandgif" style="float: left; margin-left: 29%;display:none;"src="images/ajax-loader/brand.gif">
                                                </ul>
                                            </span>
                                        </li>
                                    <?php } ?>
                                    <?php if ($loginCheck == '') { ?>
                                        <li><a href="login"><?php echo mirage_lg('login_signup_in', "Sign Up/In") ?></a></li>
                                    <?php } ?>         
                                    <?php if ($loginCheck != '') { ?>

                                        <li class="notifylist-right">
                                            <span>You<i></i>
                                                <ul class="drop-down brands-down">
                                                    <li><span><a href="settings"><?php echo mirage_lg('header_profile_settings', "Profile Settings") ?></a></span></li>
                                                    <li><span><a href="bids"><?php
                                                                echo mirage_lg('Your_Auctions', "Your Auction(s)");
                                                                if ($unread_messages_count != '' || $unread_messages_count != 0) {
                                                                    ?>( <?php echo $unread_messages_count; ?> )<?php } ?></a></span></li>
                                                    <li><span><a href="auctions"><?php
                                                                echo mirage_lg('header_auctions', "Auctions");
                                                                if ($unread_auctions_count != '') {
                                                                    echo '(' . $unread_auctions_count . ')';
                                                                }
                                                                ?></a></span></li>
                                                    <?php if ($userDetails->row()->store_payment == 'Paid') { ?>
                                                        <li> <a href="seller/dashboard"><?php
                                                                if ($this->lang->line('your_store') != '') {
                                                                    echo stripslashes($this->lang->line('your_store'));
                                                                } else
                                                                    echo "Your Store";
                                                                ?></a> </li>
                                                    <?php } ?>
                                                    <li><span><a  href="logout"><?php echo mirage_lg('header_signout', "Sign out") ?></a></span></li>
                                                </ul>
                                            </span>
                                        </li>
                                        <li>
                                            <ul class="gnb-wrap new-notify">
                                                <li class="gnb none gnb-notification">
                                                    <a href="notifications" class="mn-notification">
                                                        <em class="ic-notification">
                                                            <span class="hide"><?php
                                                                if ($this->lang->line('referrals_notification') != '') {
                                                                    echo stripslashes($this->lang->line('referrals_notification'));
                                                                } else
                                                                    echo "Notifications";
                                                                ?></span> 
                                                            <i class="count"></i>
                                                        </em>
                                                    </a>
                                                    <div class="feed-notification">
                                                        <i class="arrow"></i>
                                                        <h4><?php
                                                            if ($this->lang->line('referrals_notification') != '') {
                                                                echo stripslashes($this->lang->line('referrals_notification'));
                                                            } else
                                                                echo "Notifications";
                                                            ?></h4>
                                                        <div class="loading"><i></i></div>
                                                        <ul>
                                                            <li>
                                                                <a href="">
                                                                    <img src="" class="photo"/>
                                                                </a>

                                                                <a href="">
                                                                    <img src="" class="thing"/>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                        <a href="notifications" class="moreFeed"><?php
                                                            if ($this->lang->line('see_all_noty') != '') {
                                                                echo stripslashes($this->lang->line('see_all_noty'));
                                                            } else
                                                                echo "See all notifications";
                                                            ?></a>
                                                    </div>
                                                </li>
                                            </ul>     
                                        </li>          

                                    <?php } ?>
                                </ul>
                                <?php if ($loginCheck != '') { ?>
                                    <div class="nav-auction-cart">
                                        <ul class="nav navbar-nav navbar-right new-cart auction-cart">
                                            <li><div class="block-wrap-cart">
                                                    <?php echo $AuctionMiniCartViewSet; ?>
                                                </div>
                                            </li>
                                        </ul>
                                        <ul class="nav navbar-nav navbar-right new-cart cart-new">
                                            <li>

                                                <div class="block-wrap-cart">
                                                    <?php echo $MiniCartViewSet; ?>
                                                </div>

                                            </li>
                                        </ul>     
                                    </div>     
                                <?php } ?>
                            </div>   
                        </div>    
                    </div>
                </div>    
            </div>
        </header>

        <!-- COUNTDOWN -->
        <script type="text/javascript" src="js/assets/lib/countdown/jquery.plugin.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/site/jquery.countdown.js"></script>

        <script type="text/javascript">
            $(document).ready(function (e) {
                $('[data-countdown]').each(function () {
                    var day = "Day";
                    var days = "Days";
                    var $this = $(this), finalDate = $(this).data('countdown');
                    $this.countdown(finalDate, function (event) {
                        if (event.strftime('%D') != 00 && event.strftime('%D') == 01) {
                            $this.html(event.strftime('%D ' + day + ' %H:%M:%S'));
                        } else if (event.strftime('%D') > 01) {
                            $this.html(event.strftime('%D ' + days + ' %H:%M:%S'));
                        } else {
                            $this.html(event.strftime('%H:%M:%S'));
                        }
                    });
                });

            });
        </script>

        <!-- header_end -->
        <!-- Loading Popup Templates -->
        <?php $this->load->view('site/templates/popup_templates'); ?>


        <?php if ($flash_data != '') { ?>
            <div class="errorContainer" id="<?php echo $flash_data_type; ?>">
                <script>setTimeout("hideErrDiv('<?php echo $flash_data_type; ?>')", 3000);</script>
                <p><span><?php echo $flash_data; ?></span></p>
            </div>
        <?php } ?>

        <?php
        if ($this->config->item('google_verification_code')) {
            echo stripslashes($this->config->item('google_verification_code'));
        }
        ?>