<?php if( $this->uri->segment(1) == 'things'){ ?>
    <div style="display:none;" class="popup ly-title update pconditions animated new-auction-pop condition-pop" style="width:80px; opacity: 1; display: block;" tid="">
        <div class="default" style="display: block;">
            <p class="ltit">Help</p>
           
            <div class="condition-pop-inner">
				<ul>
					<?php  foreach($conditionsList->result() as $condision){ ?>
					<li>
						<label><?php echo $condision->condition_name; ?></label>
						<div class="conditions-desc">
							<?php echo $condision->condition_description; ?>
						</div>
					</li>
					<?php } ?>
				</ul>
            </div>
			
        </div>
		<button title="Close" class="ly-close"><i class="ic-del-black"></i></button>
    </div>
<?php } ?>