<?php if ($loginCheck != '' && $this->uri->segment(3) == 'confirmation') { ?>
    <!-- add_to_list overlay -->
    <div style="display:none;" class="popup ly-title update auction-popup animated new-auction-pop" style="margin-top: 5px; margin-left: 750.5px; opacity: 1; display: block;" tid="">
        <div class="default" style="display: block;">
            <p class="ltit"><?php
                if ($this->lang->line('Create_your_auction_now') != '') {
                    echo stripslashes($this->lang->line('Create_your_auction_now'));
                } else
                    echo "Create your auction now";
                ?></p>
           
            <div class="fancyd-item">
				
				<div class="auct-product-name">
					<?php echo $product_detail->row()->product_name; ?>
				</div>
				
                <div class="image-wrapper">
                    <div class="item-image"><?php $img = explode(',',$product_detail->row()->image)[0]; if( $img == ''){ $img  = 'dummy.gif'; } ?>
						<img src="images/product/<?php echo $img; ?>">
					</div>
                </div>
				<form action="site/product/new_auctioon" method="post">
					<div class="auction-duration">
						<label>Auction duration</label>
						<select name="duration" required>
							<option value="">Select</option>
							<option value="1">1 day</option>
							<option value="2">2 days</option>
							<option value="3">3 days</option>
							<option value="4">4 days</option>
							<option value="5">5 days</option>
							<option value="6">6 days</option>
							<option value="7">7 days</option>
						</select>
                    </div>
					
					<input name="tnst" type="hidden" value="<?php echo $payment_detail->row()->paypal_transaction_id;?>" >
					<input name="pd" type="hidden" value="<?php echo $product_detail->row()->id;?>" >
					<input name="sku_cat" type="hidden" value="<?php echo $product_detail->row()->sku_category;?>" >
					<input name="action_price" type="hidden" value="<?php echo $payment_detail->row()->total;?>" >
					<div class="sub_mit_buttn">
						<input type="submit" class="btn-save" value="Create">
					</div>
				</form>
            </div>
        </div>
    </div>
    <!-- /add_to_list overlay -->
<script>
$(document).ready(function(){
    auction();
	/* window.onbeforeunload = function() {
		return "Data will be lost if you leave the page, are you sure?";
		return false;
	}; */
	
	$(window).on('beforeunload', function(){
        return "Any changes will be lost";
    });

    // Form Submit
    $(document).on("submit", "form", function(event){
        // disable warning
        $(window).off('beforeunload');
    });

});	
</script>
    
    <?php
}?>