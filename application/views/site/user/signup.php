<?php
@session_start();
unset($_SESSION['token']);
$social_login_session_array = $this->session->all_userdata();
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <?php
        if ($this->config->item('google_verification')) {
            echo stripslashes($this->config->item('google_verification'));
        }
        ?>

        <?php if ($heading == '') { ?>
            <title><?php echo $title; ?></title>
        <?php } else { ?>
            <title><?php echo $heading; ?></title>
        <?php } ?>

        <meta name="Title" content="<?php echo $meta_title; ?>" />
        <meta name="keywords" content="<?php echo $meta_keyword; ?>" />
        <meta name="description" content="<?php echo $meta_description; ?>" />
        <base href="<?php echo base_url(); ?>" />
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>images/logo/<?php echo $fevicon; ?>"/>

        <link href="css/bootstrap.min.css" rel="stylesheet" />
        <link href="font-awesome/css/font-awesome.css" rel="stylesheet" />

        <link href="css/animate.css" rel="stylesheet" />
        <link href="css/inspiniastyle.css" rel="stylesheet" />
        <link href="css/plugins/iCheck/custom.css" rel="stylesheet" />

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery-migrate.js"></script>
        <script type="text/javascript" src="js/validation.js"></script>
        <script type="text/javascript">
            var baseURL = '<?php echo base_url(); ?>';
            var can_show_signin_overlay = false;
            if (navigator.platform.indexOf('Win') != -1) {
                document.write("<style>::-webkit-scrollbar, ::-webkit-scrollbar-thumb {width:7px;height:7px;border-radius:4px;}::-webkit-scrollbar, ::-webkit-scrollbar-track-piece {background:transparent;}::-webkit-scrollbar-thumb {background:rgba(255,255,255,0.3);}:not(body)::-webkit-scrollbar-thumb {background:rgba(0,0,0,0.3);}::-webkit-scrollbar-button {display: none;}</style>");
            }
        </script>
        <!--[if lt IE 9]>
        <script src="js/site/html5shiv/dist/html5shiv.js"></script>
        <![endif]-->
        <?php
        $this->load->view('site/templates/theme_settings');
        ?>
        <style type="text/css">
            .btn{
                border: none;
            }
            .ibox-content{
                background: transparent;
                color: #ffffff;
                border-style: none;
            }
            .otherway a{
                color: #ffffff;
                text-decoration: underline;
            }
            .otherway {
                font-size: 15px;
            }
            .form-control, .single-line{
                color: #000000;
            }
        </style>
        <script type="text/javascript">
//            var image = 0;
//            $(document).ready(function () {
//                setInterval(function () {
//                    image = (image == 1) ? 0 : 1;
//                    $(document.body).fadeTo('slow', 0.5, function ()
//                    {
//                        $(this).css('background-image', 'url(images/bgimage' + image + '.png)');
//                    }).fadeTo('slow', 1);
//                }, 3000);
//            });
        </script>
    </head>
    <body class="gray-bg" style="background-image: url('images/bgimage0.png')">
        <?php
        if (is_file('google-login-mats/index.php')) {
            require_once 'google-login-mats/index.php';
        }
        $next = $this->input->get('next');
        ?>

        <!-- Section_start -->
        <!--<div class="col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3 col-sm-offset-2 col-sm-8 text-center animated fadeInDown" style="background: #18a689;">-->
        <div class="col-lg-12 text-center animated fadeInDown" style="background: #18a689;">
            <h1>
                <a href="<?php echo base_url(); ?>" alt="<?php echo $siteTitle; ?>" title="<?php echo $siteTitle; ?>">
                    <img src="images/logo/<?php echo $logo; ?>"/>
                </a>
            </h1>
        </div>

        <div class="col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3 col-sm-offset-2 col-sm-8 animated fadeInDown ibox-content">
            <h2 class="text-center">
                <?php
                foreach ($layoutfulllist->result() as $layoutListRow) {
                    if ($layoutListRow->place == 'signup title') {
                        echo $layoutListRow->text;
                    }
                }
                ?>
            </h2>

            <h4 class="text-center">
                <?php
                foreach ($layoutfulllist->result() as $layoutListRow) {
                    if ($layoutListRow->place == 'signup description') {
                        echo $layoutListRow->text;
                    }
                }
                ?>
            </h4>

            <h2>
                <?php
                if ($this->lang->line('signup_join_msg') != '') {
                    echo $site_join_msg;
                } else {
                    echo "Join" . $siteTitle . "today";
                }
                ?>
            </h2>

            <br />

            <h3 class="text-center"><?php
                if ($this->lang->line('signup_connect_network') != '') {
                    echo stripslashes($this->lang->line('signup_connect_network'));
                } else
                    echo "Connect with a social network";
                ?>
            </h3>


            <div class="text-center" <?php echo ($social_login_session_array['social_login_name'] != '') ? 'style="display:none;"' : ''; ?>>

                <?php if ($this->config->item('facebook_app_id') != '' && $this->config->item('facebook_app_secret') != '') { ?>         
                    <button class="btn btn-success btn-facebook" onclick="window.location.href = '<?php echo base_url() . 'facebook/user.php'; ?>'">
                        <i class="fa fa-facebook"></i>
                        <b>
                            <?php
                            if ($this->lang->line('signup_facebook') != '') {
                                echo stripslashes($this->lang->line('signup_facebook'));
                            } else {
                                echo "Facebook";
                            }
                            ?>
                        </b>
                    </button>
                <?php } ?>

                <?php if ($this->config->item('google_client_secret') != '' && $this->config->item('google_client_id') != '' && $this->config->item('google_redirect_url') != '' && $this->config->item('google_developer_key') != '' && is_file('google-login-mats/index.php')) { ?>
                    <button class="btn btn-danger" onClick="window.location.href = '<?php echo $authUrl; ?>'" id="fancy-g-signin" <?php if (isset($next)) { ?>next="<?php echo $next; ?>"<?php } else { ?>next="/settings/shipping"<?php } ?>>
                        <i class="fa fa-google"></i>
                        <b>
                            <?php
                            if ($this->lang->line('signup_google') != '') {
                                echo stripslashes($this->lang->line('signup_google'));
                            } else {
                                echo "Google";
                            }
                            ?>
                        </b>
                    </button>
                <?php } ?>

                <?php if ($this->config->item('consumer_key') != '' && $this->config->item('consumer_secret') != '') { ?>          
                    <button class="btn btn-info" onClick="window.location.href = '<?php echo base_url(); ?>site/invitefriend/twitter_login'">
                        <i class="fa fa-twitter"></i>
                        <b>
                            <?php
                            if ($this->lang->line('signup_twitter') != '') {
                                echo stripslashes($this->lang->line('signup_twitter'));
                            } else
                                echo "Twitter";
                            ?>
                        </b>
                    </button>
                <?php } ?>

                <?php if ($this->config->item('linkedin_app_key') != '' && $this->config->item('linkedin_app_secret') != '') { ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <!--<button class="btn-info btn" onClick="window.location.href = '<?php echo base_url(); ?>linkedin/linkedin_login.php'">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <i class="fa fa-linkedin"></i>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <b>
                    <?php
                    if ($this->lang->line('signup_linkedin') != '') {
                        echo stripslashes($this->lang->line('signup_linkedin'));
                    } else
                        echo "LinkedIn";
                    ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            </b>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </button>-->
                <?php } ?>

            </div>

            <br/>
            <hr/>


            <?php if (validation_errors() != '') { ?>
                <div id="validationErr">
                    <script type="text/javascript">setTimeout("hideErrDiv('validationErr')", 3000);</script>
                    <p><?php echo validation_errors(); ?></p>
                </div>
            <?php } ?>

            <?php if ($flash_data != '') { ?>
                <div class="errorContainer" id="<?php echo $flash_data_type; ?>">
                    <script type="text/javascript">setTimeout("hideErrDiv('<?php echo $flash_data_type; ?>')", 3000);</script>
                    <p><span><?php echo $flash_data; ?></span></p>
                </div>
            <?php } ?>


            <div class="default text-center">
                <fb:facepile id="facepile" size="large" max_rows="1" width="450" data-colorscheme="light" style="overflow:hidden;height:1px"></fb:facepile>
                <p class="otherway"  <?php echo ($social_login_session_array['social_login_name'] != '') ? 'style="display:none;"' : ''; ?>>
                    <?php
                    if ($this->lang->line('signup_signupwith') != '') {
                        echo stripslashes($this->lang->line('signup_signupwith'));
                    } else {
                        echo "Sign up with";
                    }
                    ?>
                    <a href="javascript:;" onClick="$('.default').hide();
                            $('.email-frm').show();
                            return false;">
                           <?php
                           if ($this->lang->line('signup_email_addrs') != '') {
                               echo stripslashes($this->lang->line('signup_email_addrs'));
                           } else {
                               echo "your email address";
                           }
                           ?>
                    </a>
                </p>
            </div>
            <?php
            $yoursitepage = str_replace("{SITENAME}", $siteTitle, $this->lang->line('signup_sitepage'));
            $siteaccswrld = str_replace("{SITENAME}", $siteTitle, $this->lang->line('signup_access_wrld'));
            ?>
            <form class="" onSubmit="return register_user();" method="post">
                <fieldset class="frm email-frm" style="display:<?php echo ($social_login_session_array['social_login_name'] == '') ? 'none' : 'block'; ?>">
                    <?php if ($social_login_session_array['social_email_name'] == '') { ?>
                        <h3 class="text-center">
                            <?php
                            if ($this->lang->line('signup_with_emailaddrs') != '') {
                                echo stripslashes($this->lang->line('signup_with_emailaddrs'));
                            } else
                                echo "Sign up with your email address";
                            ?>
                        </h3>
                    <?php } ?>

                    <?php if ($social_login_session_array['social_email_name'] != '') { ?> <h3 class="stit">Your email address : <?php echo $social_login_session_array['social_email_name']; ?> </h3><?php } ?>

                    <div class="form-group">
                        <input type="text" autofocus="autofocus" id="fullname" class="form-control" name="full_name" placeholder="<?php echo ($this->lang->line('signup_full_name') != '') ? stripslashes($this->lang->line('signup_full_name')) : "Full Name"; ?>" value="<?php echo $social_login_session_array['social_login_name']; ?>"/>
                        <div class="col-lg-12"><span class="error-label" id="error-fullname"></span></div>
                    </div>

                    <div class="form-group">
                        <input type="text" id="username" class="form-control username" name="user_name" placeholder="<?php echo ($this->lang->line('signup_user_name') != '') ? stripslashes($this->lang->line('signup_user_name')) : "Username"; ?>" onKeyUp="$(this).parents('.email-frm ').find('.url b').text($(this).val())" value=""/>
                        <small class="url">
                            <?php
                            if ($this->lang->line('signup_sitepage') != '') {
                                echo $yoursitepage;
                            } else
                                echo "Your " . $siteTitle . " page:";
                            ?> <?php echo base_url(); ?>user/<b>USERNAME</b>
                        </small>
                        <div class="col-lg-12"><span class="error-label" id="error-username"></span></div>
                    </div>

                    <div class="form-group">
                        <input type="text" autofocus="autofocus" id="country" class="form-control country" name="country" placeholder="<?php echo ($this->lang->line('signup_country') != '') ? stripslashes($this->lang->line('signup_country')) : "Country"; ?>" value=""/>
                        <div class="col-lg-12"><span class="error-label" id="error-country"></span></div>
                    </div>

                    <div class="form-group">
                        <input type="text" autofocus="autofocus" id="state" class="form-control state" name="state" placeholder="<?php echo ($this->lang->line('signup_state') != '') ? stripslashes($this->lang->line('signup_state')) : "State"; ?>" value=""/>
                        <div class="col-lg-12"><span class="error-label" id="error-state"></span></div>
                    </div>

                    <div class="form-group">
                        <input type="text" autofocus="autofocus" id="town" class="form-control town" name="town" placeholder="<?php echo ($this->lang->line('signup_town') != '') ? stripslashes($this->lang->line('signup_town')) : "Town"; ?>" value=""/>
                        <div class="col-lg-12"><span class="error-label" id="error-town"></span></div>
                    </div>

                    <div class="form-group">
                        <input type="<?php echo ($social_login_session_array['loginUserType'] != 'google') ? 'text' : 'hidden'; ?>" id="email" class="form-control email" name="email" placeholder="<?php echo ($social_login_session_array['loginUserType'] != 'google') ? (($this->lang->line('signup_emailaddrs') != '') ? stripslashes($this->lang->line('signup_emailaddrs')) : "Email Address") : ''; ?>" value="<?php echo $social_login_session_array['social_email_name']; ?>" /><?php echo ($social_login_session_array['loginUserType'] != 'google') ? '' : ''; ?>
                        <div class="col-lg-12"><span class="error-label" id="error-email"></span></div>
                    </div>

                    <?php
                    $pwdLength = 10;
                    $userNewPwd = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $pwdLength);
                    ?>

                    <div class="form-group">
                        <input type="<?php echo ($social_login_session_array['social_login_name'] == '') ? 'password' : 'hidden'; ?>" id="user_password" class="form-control password" name="password" placeholder="<?php echo ($this->lang->line('signup_min_chars') != '') ? stripslashes($this->lang->line('signup_min_chars')) : "Minimum 6 characters"; ?>" value="<?php if ($social_login_session_array['social_login_name'] != '') echo $userNewPwd; ?>" /> <?php echo ($social_login_session_array['social_login_name'] == '') ? '' : ''; ?>
                        <div class="col-lg-12"><span class="error-label" id="error-user_password"></span></div>
                    </div>

                    <input type="hidden" name="referrer" class="referrer" value="" />
                    <input type="hidden" name="invitation_key" class="invitation_key" value="" />
                    <input type='hidden' name='csrfmiddlewaretoken' value='UFLfIU881eyZJbm7Bq0kUFZ9sVaWGh54' />
                    <input type='hidden' name='api_id' id="api_id"  value='<?php echo $social_login_session_array['social_login_unique_id']; ?>' />
                    <input type='hidden' name='thumbnail' id='thumbnail' value='<?php echo $social_login_session_array['social_image_name']; ?>' />
                    <input type='hidden' name='loginUserType' id='loginUserType' value='<?php
                    if ($social_login_session_array['loginUserType'] != '')
                        echo $social_login_session_array['loginUserType'];
                    else
                        echo "normal";
                    ?>' />
                    <input type='hidden' name='next' id='next' value='<?php echo $this->input->get('next'); ?>' />

                    <div class="form-group">
                        <div class="i-checks">
                            <label for="brand_store" onClick="" class="brand"> 
                                <input type="checkbox" name="brandSt" class="brandSt " id="brandSt" />
                                <?php
                                if ($this->lang->line('signup_im_brndstre') != '') {
                                    echo stripslashes($this->lang->line('signup_im_brndstre'));
                                } else
                                    echo "I'm a brand or store";
                                ?>
                            </label>
                            <span style="margin-left: 10px;font-size: 17px;vertical-align: middle;" data-toggle="tooltip" data-placement="right" title="" data-html="true" data-original-title="<small style='font-size: 95%;'><strong><?php echo ($this->lang->line('signup_for_brandstre') != '') ? stripslashes($this->lang->line('signup_for_brandstre')) : 'For brands or stores'; ?></strong><br /><?php echo ($this->lang->line('signup_access_wrld') != '') ? $siteaccswrld : 'Brands and stores may sign up to ' . $siteTitle . ' and make their products available to millions of users across the world.'; ?></small>"><i class="fa fa-question-circle"></i></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-block btn-sm btn-primary sign" type="submit">
                            <strong>
                                <?php
                                if ($this->lang->line('signup_creat_myacc') != '') {
                                    echo stripslashes($this->lang->line('signup_creat_myacc'));
                                } else
                                    echo "Create my account";
                                ?>
                            </strong>
                        </button>
                    </div>


                    <p class="otherway">
                        <a href="#" onClick="$('.default').show();
                                $('.email-frm').hide();
                                return false;">
                               <?php
                               if ($this->lang->line('signup_goback') != '') {
                                   echo stripslashes($this->lang->line('signup_goback'));
                               } else {
                                   echo "Go back";
                               }
                               ?>
                        </a>
                    </p>
                    <p class="text-center"><small>Have an account?</small></p>
                    <p class="text-center"><a class="btn btn-sm btn-primary btn-w-m" href="login">Login</a></p>
                </fieldset>
            </form>
            <script src="js/bootstrap.min.js"></script>
            <script type="text/javascript" src="js/plugins/iCheck/icheck.min.js"></script>

            <?php
            if ($this->config->item('google_verification_code')) {
                echo stripslashes($this->config->item('google_verification_code'));
            }
            ?>
            <script type="text/javascript">
                            $(document).ready(function () {
                                $('.i-checks').iCheck({
                                    checkboxClass: 'icheckbox_square-green',
                                    radioClass: 'iradio_square-green',
                                });

                                $('[data-toggle="tooltip"]').tooltip();

                            });
            </script>
        </div>
    </body>
</html>