<?php
//error_reporting(E_ALL);
//ini_set("display_errors", "ON");
$this->load->view('site/templates/header');
?>
<!--<link rel="stylesheet" href="<?php echo base_url(); ?>css/site/<?php echo SITE_COMMON_DEFINE ?>timeline.css" type="text/css" media="all"/>-->
<link rel="stylesheet" media="all" type="text/css" href="<?php echo base_url(); ?>css/site/<?php echo SITE_COMMON_DEFINE ?>setting.css">
<link rel="stylesheet" media="all" type="text/css" href="<?php echo base_url(); ?>css/plugins/iCheck/custom.css">
<link rel="stylesheet" media="all" type="text/css" href="<?php echo base_url(); ?>css/plugins/datapicker/datepicker3.css">
<link rel="stylesheet" media="all" type="text/css" href="<?php echo base_url(); ?>css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css">
<link rel="stylesheet" media="all" type="text/css" href="<?php echo base_url(); ?>css/plugins/dataTables/datatables.min.css">
<script src="<?php echo base_url(); ?>js/plugins/dataTables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>js/plugins/datapicker/bootstrap-datepicker.js" type="text/javascript"></script>
<style type="text/css">
    ol.stream {position: relative;}
    ol.stream.use-css3 li.anim {transition:all .25s;-webkit-transition:all .25s;-moz-transition:all .25s;-ms-transition:all .25s;visibility:visible;opacity:1;}
    ol.stream.use-css3 li {visibility:hidden;}
    ol.stream.use-css3 li.anim.fadeout {opacity:0;}
    ol.stream.use-css3.fadein li {opacity:0;}
    ol.stream.use-css3.fadein li.anim.fadein {opacity:1;}

    .panel .fa{ width:20px; }
    .panel-body table tr td { padding-left: 15px }
    .panel-body .table {margin-bottom: 0px; }

    .overlay {
        position: absolute;
        bottom: 0;
        left: 0;
        right: 0;
        background-color: rgba(0, 0, 0, 0.7);
        overflow: hidden;
        width: 88%;
        height: 0%;
        transition: .5s ease;
        margin: 0 auto;
    }
    .photo-preview{
        background: #000000;
    }
    .profileImg:hover .overlay {
        height: 100%;
    }
    .vcenter{
        display:flex;
        flex-direction:column;
        justify-content:center;
    }
    .upload-file > span {
        color: white;
    }
    .comment {
        color: white;
    }
    .modal .modal-body{
        padding: 30px !important;
        font-size: 14px !important;
        color: #191717;
        background-color: white;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $('#purchasesList, #subscriptionList, #shippingAddress').DataTable();
        $('#settings_tab a').click(function (e) {
            e.preventDefault();
            $('#settings_tab td').removeClass('active');
            $(this).parent().addClass('active');
        });

        $('#dob .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: 'dd-mm-yyyy'
        });

        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        $('.select-select2').select2();

        var url = document.location.toString();
        if (url.match('#')) {
            $('#settings_tab a[href="#' + url.split('#')[1] + '"]').tab('show');
        }

        $('#settings_tab a').on('shown.bs.tab', function (e) {
            window.location.hash = e.target.hash;
        });

        var $select = $('.gift-recommend select.select-round');
        $select.selectBox();
        $select.each(function () {
            var $this = $(this);
            if ($this.css('display') != 'none')
                $this.css('visibility', 'visible');
        });

        var check_maxlength = function (e) {
            var max = parseInt($(this).attr('maxlength'));
            var len = $(this).val().length;
            if (len > max) {
                $(this).val($(this).val().substr(0, max));
            }
            if (len >= max) {
                return false;
            }
        }
        $('textarea[maxlength]').keypress(check_maxlength).change(check_maxlength);

    });
</script>

<?php if ($flash_data != '') { ?>
    <div class="col-lg-10 col-lg-offset-2 redesign">
        <div class="alert <?php echo ($flash_data_type == 'message-green') ? 'alert-success' : 'alert-danger '; ?> alert-dismissable" id="<?php echo $flash_data_type; ?>">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <script>setTimeout("hideErrDiv('<?php echo $flash_data_type; ?>')", 5000);</script>
            <?php echo $flash_data; ?>
        </div>
    </div>
<?php } ?>

<div class="col-lg-10 col-lg-offset-2 redesign">
    <div class="row">
        <div class="col-sm-3 col-md-3">
            <div class="panel-group" id="accordion">
                <div role="tabpanel" id="settings_tab">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                    <span class="fa fa-folder"></span>Account
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <table class="table">
                                    <tr>
                                        <td>
                                            <span class="fa fa-user text-navy"></span><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="fa fa-flash text-navy"></span><a href="#preferences" aria-controls="preferences" role="tab" data-toggle="tab">Preferences</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="fa fa-eye-slash text-navy"></span><a href="#password" aria-controls="password" role="tab" data-toggle="tab">Password</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="fa fa-bell text-navy"></span><a href="#notification" aria-controls="notification" role="tab" data-toggle="tab">Notifications</a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                    <span class="fa fa-shopping-basket"></span>
                                    Shop
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <table class="table">
                                    <tr>
                                        <td>
                                            <span class="fa fa-history text-navy"></span><a href="#purchases" aria-controls="purchases" role="tab" data-toggle="tab">Purchases</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="fa fa-bookmark text-navy"></span><a href="#subscriptions" aria-controls="subscriptions" role="tab" data-toggle="tab">Subscriptions</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="fa fa-ship  text-navy"></span><a href="#shipping" aria-controls="shipping" role="tab" data-toggle="tab">Shipping</a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                    <span class="fa fa-group"></span>Sharing
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                            <div class="panel-body">
                                <table class="table">
                                    <tr>
                                        <td>
                                            <span class="fa fa-gift text-navy"></span><a href="#giftcard" aria-controls="giftcard" role="tab" data-toggle="tab">Gift Card</a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-9 col-md-9">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="profile">
                    <form class="myform" id="profile_settings_form" method="post" action="site/user_settings/changePhoto" enctype="multipart/form-data" onSubmit="return profileUpdate();">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <span class="fa fa-user"></span>
                                    <?php
                                    if ($this->lang->line('settings_edit_prof') != '') {
                                        echo stripslashes($this->lang->line('settings_edit_prof'));
                                    } else
                                        echo "Edit Profile Settings";
                                    ?>
                                </h4>
                            </div>
                            <div class="panel-body p-sm">
                                <?php if ($userDetails->row()->is_verified == 'No') { ?>
                                    <div class="alert alert-info confirm-email">
                                        <?php
                                        if ($this->lang->line('settings_check_mail') != '') {
                                            echo stripslashes($this->lang->line('settings_check_mail'));
                                        } else
                                            echo "Check your email";
                                        ?>
                                        <b>(<?php echo $userDetails->row()->email; ?>)</b>
                                        <?php
                                        if ($this->lang->line('settings_toconfirm') != '') {
                                            echo stripslashes($this->lang->line('settings_toconfirm'));
                                        } else
                                            echo "to confirm.";
                                        ?>
                                        <a class="alert-link" id="resend_confirmation" href="javascript:void(0)" onclick="javascript:resendConfirmation('<?php echo $userDetails->row()->email; ?>')">
                                            <?php
                                            if ($this->lang->line('settings_resendconfm') != '') {
                                                echo stripslashes($this->lang->line('settings_resendconfm'));
                                            } else
                                                echo "Resend confirmation";
                                            ?>
                                        </a>
                                    </div>
                                <?php } ?>

                                <div class="row">
                                    <div class="col-md-4 profileImg">
                                        <?php
                                        $userImg = 'user-thumb1.png';
                                        if ($userDetails->row()->thumbnail != '') {
                                            $userImg = $userDetails->row()->thumbnail;
                                        }
                                        ?>
                                        <div class="photo-preview"><img src="images/site/blank.gif" style="width:100%;height:100%;background-image:url(<?php echo base_url(); ?>images/users/<?php echo $userImg; ?>);background-size:cover" alt="<?php echo $userDetails->row()->full_name; ?>"></div>
                                        <div class="overlay">
                                            <div class="photo-func">		
                                                <?php if ($userDetails->row()->thumbnail == '') { ?>		
                                                    <input type="button" style="cursor: pointer;" class="btn-change" onClick="$('.photo-func').hide();
                                                                $('.upload-file').show();
                                                                return false;" value="<?php echo ($this->lang->line('header_up_photo') != '') ? stripslashes($this->lang->line('header_up_photo')) : "Upload Photo"; ?>"/>
                                                       <?php } else { ?>
                                                    <input type="button" style="cursor: pointer;" class="btn-change" onClick="$('.photo-func').hide();$('.upload-file').show();return false;" value="<?php echo ($this->lang->line('change_photo') != '') ? stripslashes($this->lang->line('change_photo')) : "Change Photo"; ?>"/>
                                                    <input type="button" style="cursor: pointer;" class="btn-delete" id="delete_profile_image" onClick="return deleteUserPhoto();" value="<?php echo ($this->lang->line('header_delete_photo') != '') ? stripslashes($this->lang->line('header_delete_photo')) : "Delete Photo"; ?>"/>
                                                <?php } ?>
                                            </div>                                            
                                            <div class="upload-file">
                                                <input id="uploadavatar" class="uploadavatar" name="upload-file" type="file">
                                                <span class="uploading" style="display:none">
                                                    <?php
                                                    if ($this->lang->line('settings_uploading') != '') {
                                                        echo stripslashes($this->lang->line('settings_uploading'));
                                                    } else
                                                        echo "Uploading...";
                                                    ?>
                                                </span>
                                                <br>
                                                <span class="description">
                                                    <?php
                                                    if ($this->lang->line('settings_allowedimag') != '') {
                                                        echo stripslashes($this->lang->line('settings_allowedimag'));
                                                    } else
                                                        echo "Allowed file types JPG, GIF or PNG.<br>Maximum width and height is 600px";
                                                    ?>
                                                </span>
                                                <br>
                                                <input type="button" style="cursor: pointer;" class="btn-upload" id="save_profile_image" onclick="return updateUserPhoto();" value="<?php echo ($this->lang->line('header_up_photo') != '') ? stripslashes($this->lang->line('header_up_photo')) : "Upload Photo"; ?>"/>
                                                <input type="button" style="cursor: pointer;" class="btn-cancel" onClick="$('.photo-func').show();$('.upload-file').hide();return false;" value="<?php echo ($this->lang->line('header_cancel') != '') ? stripslashes($this->lang->line('header_cancel')) : "Cancel"; ?>"/>
                                            </div>
                                            <br>
                                            <small class="comment">
                                                <?php
                                                if ($this->lang->line('settings_profile_identy') != '') {
                                                    echo stripslashes($this->lang->line('settings_profile_identy'));
                                                } else {
                                                    echo "Your profile photo is your identity on";
                                                }
                                                ?>
                                                <?php echo $siteTitle; ?>, 
                                                <?php
                                                if ($this->lang->line('settings_pickone') != '') {
                                                    echo stripslashes($this->lang->line('settings_pickone'));
                                                } else {
                                                    echo "so pick a good one that expresses who you are.";
                                                }
                                                ?>
                                            </small>
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <input class="form-control" disabled="disabled" type="text" value="<?php echo $userDetails->row()->user_name; ?>" placeholder="<?php echo ($this->lang->line('signup_user_name') != '') ? stripslashes($this->lang->line('signup_user_name')) : "Username"; ?>"/>
                                            <small class="comment"><?php echo base_url(); ?>user/<?php echo $userDetails->row()->user_name; ?></small>
                                        </div>

                                        <div class="form-group">
                                            <input id="name" class="setting_fullname form-control" name="setting-fullname" value="<?php echo $userDetails->row()->full_name; ?>" type="text" placeholder="<?php echo ($this->lang->line('signup_full_name') != '') ? stripslashes($this->lang->line('signup_full_name')) : "Full Name"; ?>"/>
                                            <small class="comment"><?php echo ($this->lang->line('settings_realname') != '') ? stripslashes($this->lang->line('settings_realname')) : "Your real name, so your friends can find you."; ?></small>
                                        </div>

                                        <div class="form-group" id="dob">
                                            <div class="input-group date">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                <input type="text" class="form-control date_of_birth" value="<?php echo date('d-m-Y', strtotime($userDetails->row()->birthday)); ?>" />
                                            </div>
                                            <small class="comment">
                                                <?php
                                                if ($this->lang->line('settings_surprise_bday') != '') {
                                                    echo stripslashes($this->lang->line('settings_surprise_bday'));
                                                } else {
                                                    echo "We'll send you a surprise on your birthday!";
                                                }
                                                ?>
                                            </small>
                                        </div>
                                    </div>

                                    <div class="col-md-10 col-md-offset-1" style="padding: 10px 0px;">
                                        <div class="form-group">
                                            <input type="text" id="email" class="setting_email form-control" name="setting-email" data-email="<?php echo $userDetails->row()->email; ?>" value="<?php echo $userDetails->row()->email; ?>" placeholder="<?php echo ($this->lang->line('referrals_email') != '') ? stripslashes($this->lang->line('referrals_email')) : "Email"; ?>">
                                            <input id="user_email" value="<?php echo $userDetails->row()->email; ?>" type="hidden">				
                                            <small class="comment">
                                                <?php
                                                if ($this->lang->line('settings_email_not') != '') {
                                                    echo stripslashes($this->lang->line('settings_email_not'));
                                                } else {
                                                    echo "Email will not be publicly displayed.";
                                                }
                                                ?>
                                            </small>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">
                                                <?php
                                                if ($this->lang->line('settings_gender') != '') {
                                                    echo stripslashes($this->lang->line('settings_gender'));
                                                } else
                                                    echo "Gender";
                                                ?>
                                            </label>
                                            <div class="col-sm-10">
                                                <label class="checkbox-inline i-checks">
                                                    <input type="radio" name="gender" class="setting_gender" id="gender1" <?php echo ($userDetails->row()->gender == 'Male') ? 'checked="checked"' : ''; ?> value="Male">
                                                    <?php
                                                    if ($this->lang->line('settings_male') != '') {
                                                        echo stripslashes($this->lang->line('settings_male'));
                                                    } else
                                                        echo "Male";
                                                    ?>
                                                </label>

                                                <label class="checkbox-inline i-checks">
                                                    <input type="radio" name="gender" class="setting_gender" id="gender2" <?php echo ($userDetails->row()->gender == 'Female') ? 'checked="checked"' : ''; ?> value="Female">
                                                    <?php
                                                    if ($this->lang->line('settings_female') != '') {
                                                        echo stripslashes($this->lang->line('settings_female'));
                                                    } else
                                                        echo "Female";
                                                    ?>
                                                </label>

                                                <label class="checkbox-inline i-checks">
                                                    <input type="radio" name="gender" class="setting_gender" id="gender3" <?php echo ($userDetails->row()->gender == 'Unspecified') ? 'checked="checked"' : ''; ?> value="Unspecified">
                                                    <?php
                                                    if ($this->lang->line('settings_unspecified') != '') {
                                                        echo stripslashes($this->lang->line('settings_unspecified'));
                                                    } else
                                                        echo "Unspecified";
                                                    ?>
                                                </label>
                                            </div>
                                        </div>

                                        <br />
                                        <div class="form-group m-t-md">
                                            <input type="text" id="site" class="setting_website form-control" name="setting-website" value="<?php echo $userDetails->row()->web_url; ?>" placeholder="<?php echo ($this->lang->line('settings_website') != '') ? stripslashes($this->lang->line('settings_website')) : "Website"; ?>">
                                        </div>

                                        <div class="form-group">
                                            <input id="loc" class="setting_location form-control" name="setting-location" placeholder="<?php
                                            echo ($this->lang->line('settings_location') != '') ? stripslashes($this->lang->line('settings_location')) : "Location";
                                            echo " ";
                                            echo ($this->lang->line('settings_eg_new') != '') ? stripslashes($this->lang->line('settings_eg_new')) : "e.g. New York, NY";
                                            ?>" value="<?php echo $userDetails->row()->location; ?>" class="text" />
                                        </div>

                                        <div class="form-group">
                                            <input id="twitter" class="setting_twitter form-control" name="setting-twitter" value="<?php echo $userDetails->row()->twitter; ?>" type="text" placeholder="<?php echo ($this->lang->line('signup_twitter') != '') ? stripslashes($this->lang->line('signup_twitter')) : "Twitter"; ?>">
                                        </div>

                                        <div class="form-group">
                                            <input id="facebook" class="setting_facebook form-control" name="setting-facebook" value="<?php echo $userDetails->row()->facebook; ?>" type="text" placeholder="<?php echo ($this->lang->line('signup_facebook') != '') ? stripslashes($this->lang->line('signup_facebook')) : "Facebook"; ?>">
                                        </div>

                                        <div class="form-group">
                                            <input id="google" class="setting_google form-control" name="setting-google" value="<?php echo $userDetails->row()->google; ?>" type="text" placeholder="<?php echo ($this->lang->line('signup_google') != '') ? stripslashes($this->lang->line('signup_google')) : "Google"; ?>+">
                                        </div>

                                        <div class="form-group">
                                            <textarea id="bio" rows="5" class="setting_bio form-control" name="setting-bio" max-length="180" placeholder="<?php echo ($this->lang->line('settings_about') != '') ? stripslashes($this->lang->line('settings_about')) : "About"; ?>"><?php echo $userDetails->row()->about; ?></textarea>
                                            <small class="comment">
                                                <b class="byte"></b>
                                                <?php
                                                if ($this->lang->line('settings_write_yours') != '') {
                                                    echo stripslashes($this->lang->line('settings_write_yours'));
                                                } else
                                                    echo "Write something about yourself.";
                                                ?>
                                            </small>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <input type="submit" name="profile" id="save_account" class="btn btn-primary btn-sm pull-right btn-save"  value="<?php echo ($this->lang->line('settings_save_profile') != '') ? stripslashes($this->lang->line('settings_save_profile')) : "Save Profile"; ?>" />
                                <input type="button" onClick="return deactivateUser();" class="btn btn-sm btn-outline btn-primary btn-deactivate" id="close_account" value="<?php
                                if ($this->lang->line('settings_deact_acc') != '') {
                                    echo stripslashes($this->lang->line('settings_deact_acc'));
                                } else
                                    echo "Deactivate my account";
                                ?>"/>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </form>
                </div>
                <div role="tabpanel" class="tab-pane" id="preferences">
                    <form method="post" action="site/user_settings/update_preferences" class="myform">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <span class="fa fa-flash"></span>
                                    <?php
                                    if ($this->lang->line('prference_edit') != '') {
                                        echo stripslashes($this->lang->line('prference_edit'));
                                    } else
                                        echo "Edit Preferences";
                                    ?>
                                </h4>
                            </div>
                            <div class="panel-body p-sm">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group text-center">
                                            <div class="row row-eq-height">
                                                <div class="col-md-3">
                                                    <h2>
                                                        <?php
                                                        if ($this->lang->line('prference_localization') != '') {
                                                            echo stripslashes($this->lang->line('prference_localization'));
                                                        } else
                                                            echo "Localization";
                                                        ?>
                                                        <br/>
                                                        <small>
                                                            <?php
                                                            if ($this->lang->line('prference_language') != '') {
                                                                echo stripslashes($this->lang->line('prference_language'));
                                                            } else
                                                                echo "Language";
                                                            ?>
                                                        </small>
                                                    </h2>
                                                </div>
                                                <div class="col-md-9 vcenter">
                                                    <select style="width: 100%" data-langcode="en" id="lang" name="language" class="form-control select-select2">
                                                        <?php
                                                        $selectedLangCode = $userDetails->row()->language;
                                                        if ($selectedLangCode == '') {
                                                            $selectedLangCode = 'en';
                                                        }
                                                        if (count($activeLgs) > 0) {
                                                            foreach ($activeLgs as $activeLgsRow) {
                                                                ?>	
                                                                <option value="<?php echo $activeLgsRow['lang_code']; ?>" <?php
                                                                if ($selectedLangCode == $activeLgsRow['lang_code']) {
                                                                    echo 'selected="selected"';
                                                                }
                                                                ?>><?php echo $activeLgsRow['name']; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                    </select>
                                                    <br/>
                                                    <small class="comment">
                                                        <?php
                                                        if ($this->lang->line('prference_lang_not') != '') {
                                                            echo stripslashes($this->lang->line('prference_lang_not'));
                                                        } else
                                                            echo "Can't find your language on";
                                                        ?> <?php echo $siteTitle; ?>? 
                                                        <a href="mailto:<?php echo $siteContactMail; ?>">
                                                            <?php
                                                            if ($this->lang->line('prference_letus') != '') {
                                                                echo stripslashes($this->lang->line('prference_letus'));
                                                            } else
                                                                echo "Let us know";
                                                            ?>
                                                        </a>.
                                                    </small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group text-center">
                                            <div class="row row-eq-height">
                                                <div class="col-md-3">
                                                    <h2>
                                                        <?php
                                                        if ($this->lang->line('prference_privacy') != '') {
                                                            echo stripslashes($this->lang->line('prference_privacy'));
                                                        } else
                                                            echo "Privacy";
                                                        ?>
                                                        <br/>
                                                        <small>
                                                            <?php
                                                            if ($this->lang->line('prference_prof_visible') != '') {
                                                                echo stripslashes($this->lang->line('prference_prof_visible'));
                                                            } else
                                                                echo "Profile visibility";
                                                            ?>
                                                        </small>
                                                    </h2>
                                                </div>
                                                <div class="col-md-9">
                                                    <span class="description">
                                                        <?php
                                                        if ($this->lang->line('prference_mange_acti') != '') {
                                                            echo stripslashes($this->lang->line('prference_mange_acti'));
                                                        } else
                                                            echo "Manage who can see your activity, things you";
                                                        ?> <?php echo LIKE_BUTTON; ?>, <?php
                                                        if ($this->lang->line('prference_search_res') != '') {
                                                            echo stripslashes($this->lang->line('prference_search_res'));
                                                        } else
                                                            echo "your followers, people you follow or in anyone's search results.";
                                                        ?>
                                                    </span>
                                                    <br/>
                                                    <br/>
                                                    <label class="checkbox-inline i-checks">
                                                        <input type="radio" name="visibility" id="visibility1" <?php echo ($userDetails->row()->visibility == 'Everyone') ? 'checked="checked"' : ''; ?> value="Everyone">
                                                        <?php
                                                        if ($this->lang->line('prference_everyone') != '') {
                                                            echo stripslashes($this->lang->line('prference_everyone'));
                                                        } else
                                                            echo "Everyone";
                                                        ?>
                                                    </label>

                                                    <label class="checkbox-inline i-checks">
                                                        <input type="radio" name="visibility" id="visibility2" <?php echo ($userDetails->row()->visibility == 'Only you') ? 'checked="checked"' : ''; ?> value="Only you">
                                                        <?php
                                                        if ($this->lang->line('prference_onlyu') != '') {
                                                            echo '<i class="fa fa-lock"></i>' . stripslashes($this->lang->line('prference_onlyu'));
                                                        } else
                                                            echo '<i class="fa fa-lock"></i>' . "Only you";
                                                        ?>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group text-center">
                                            <div class="row row-eq-height">
                                                <div class="col-md-3">
                                                    <h2>
                                                        <?php
                                                        if ($this->lang->line('prference_content') != '') {
                                                            echo stripslashes($this->lang->line('prference_content'));
                                                        } else
                                                            echo "Content";
                                                        ?>
                                                        <br/>
                                                        <small>
                                                            <?php
                                                            if ($this->lang->line('prference_disp_list') != '') {
                                                                echo stripslashes($this->lang->line('prference_disp_list'));
                                                            } else
                                                                echo "Display lists";
                                                            ?>
                                                        </small>
                                                    </h2>
                                                </div>
                                                <div class="col-md-9">
                                                    <span class="description">
                                                        <?php
                                                        if ($this->lang->line('prference_show_list') != '') {
                                                            echo stripslashes($this->lang->line('prference_show_list'));
                                                        } else
                                                            echo "Show list options for organizing your things when you";
                                                        ?> <?php echo LIKE_BUTTON; ?> <?php
                                                        if ($this->lang->line('prference_something') != '') {
                                                            echo stripslashes($this->lang->line('prference_something'));
                                                        } else
                                                            echo "something.";
                                                        ?>
                                                    </span>
                                                    <br/>
                                                    <br/>
                                                    <label class="checkbox-inline i-checks">
                                                        <input type="radio" name="display_lists" id="display1" <?php echo ($userDetails->row()->display_lists == 'Yes') ? 'checked="checked"' : ''; ?> value="Yes">
                                                        <?php
                                                        if ($this->lang->line('prference_yes') != '') {
                                                            echo stripslashes($this->lang->line('prference_yes'));
                                                        } else
                                                            echo "Yes";
                                                        ?>
                                                    </label>

                                                    <label class="checkbox-inline i-checks">
                                                        <input type="radio" name="display_lists" id="display2" <?php echo ($userDetails->row()->display_lists == 'No') ? 'checked="checked"' : ''; ?> value="No">
                                                        <?php
                                                        if ($this->lang->line('prference_no') != '') {
                                                            echo stripslashes($this->lang->line('prference_no'));
                                                        } else
                                                            echo "No";
                                                        ?>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <input type="submit" id="save_preferences" class="btn btn-primary btn-sm pull-right btn-save"  value="<?php echo ($this->lang->line('prference_save_prefer') != '') ? stripslashes($this->lang->line('prference_save_prefer')) : "Save Preferences"; ?>" />
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </form>
                </div>
                <div role="tabpanel" class="tab-pane" id="password">
                    <form onsubmit="return change_user_password();" method="post" action="<?php echo base_url() . 'site/user_settings/change_user_password' ?>">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <span class="fa fa-eye-slash"></span>
                                    <?php
                                    if ($this->lang->line('change_password') != '') {
                                        echo stripslashes($this->lang->line('change_password'));
                                    } else
                                        echo "Change Password";
                                    ?>
                                </h4>
                            </div>
                            <div class="panel-body p-sm">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row row-eq-height">
                                                <div class="col-md-8 col-md-offset-2">
                                                    <label>
                                                        <?php
                                                        if ($this->lang->line('change_new_pwd') != '') {
                                                            echo stripslashes($this->lang->line('change_new_pwd'));
                                                        } else
                                                            echo "New Password";
                                                        ?>
                                                    </label>
                                                    <input type="password" name="pass" id="pass" class="form-control">
                                                    <small class="comment">
                                                        <?php
                                                        if ($this->lang->line('change_pwd_limt') != '') {
                                                            echo stripslashes($this->lang->line('change_pwd_limt'));
                                                        } else
                                                            echo "New password, at least 6 characters.";
                                                        ?>
                                                    </small>

                                                    <br/>
                                                    <br/>

                                                    <br/>
                                                    <label>
                                                        <?php
                                                        if ($this->lang->line('change_conf_pwd') != '') {
                                                            echo stripslashes($this->lang->line('change_conf_pwd'));
                                                        } else
                                                            echo "Confirm Password";
                                                        ?>
                                                    </label>
                                                    <input type="password" name="confirmpass" id="confirmpass" class="form-control">
                                                    <small class="comment">
                                                        <?php
                                                        if ($this->lang->line('change_ur_pwd') != '') {
                                                            echo stripslashes($this->lang->line('change_ur_pwd'));
                                                        } else
                                                            echo "Confirm your new password.";
                                                        ?>
                                                    </small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <input type="submit" id="save_password" class="btn btn-primary btn-sm pull-right btn-save"  value="<?php echo ($this->lang->line('change_password') != '') ? stripslashes($this->lang->line('change_password')) : "Change Password"; ?>" />
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </form>
                </div>
                <div role="tabpanel" class="tab-pane" id="notification">
                    <form enctype="multipart/form-data"  method="post" action="site/user_settings/update_notifications">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <span class="fa fa-flash"></span>
                                    <?php
                                    if ($this->lang->line('referrals_notification') != '') {
                                        echo stripslashes($this->lang->line('referrals_notification'));
                                    } else
                                        echo "Notifications";
                                    ?>
                                </h4>
                            </div>
                            <div class="panel-body p-sm">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row row-eq-height">
                                                <div class="col-md-3 text-center">
                                                    <h2>
                                                        <?php
                                                        if ($this->lang->line('referrals_email') != '') {
                                                            echo stripslashes($this->lang->line('referrals_email'));
                                                        } else
                                                            echo "Email";
                                                        ?>
                                                        <br/>
                                                        <small>
                                                            <?php
                                                            if ($this->lang->line('notify_email_sett') != '') {
                                                                echo stripslashes($this->lang->line('notify_email_sett'));
                                                            } else
                                                                echo "Email settings";
                                                            ?>
                                                        </small>
                                                    </h2>
                                                </div>
                                                <div class="col-md-9">
                                                    <?php
                                                    $emailNoty = explode(',', $userDetails->row()->email_notifications);
                                                    if (is_array($emailNoty)) {
                                                        $emailNotifications = $emailNoty;
                                                    }
                                                    ?>

                                                    <div class="checkbox checkbox-primary col-md-6">
                                                        <input type="checkbox" name="following" id="following" <?php echo (in_array('following', $emailNotifications)) ? 'checked="checked"' : ''; ?>>
                                                        <label for="following">
                                                            <?php
                                                            if ($this->lang->line('notify_some_follu') != '') {
                                                                echo stripslashes($this->lang->line('notify_some_follu'));
                                                            } else {
                                                                echo "When someone follows you";
                                                            }
                                                            ?>
                                                        </label>
                                                    </div>

                                                    <div class="checkbox checkbox-primary col-md-6">
                                                        <input type="checkbox" name="comments_on_fancyd" id="comments_on_fancyd"<?php echo (in_array('comments_on_fancyd', $emailNotifications)) ? 'checked="checked"' : ''; ?>>
                                                        <label for="comments_on_fancyd">
                                                            <?php
                                                            if ($this->lang->line('notify_comm_things') != '') {
                                                                echo stripslashes($this->lang->line('notify_comm_things'));
                                                            } else {
                                                                echo "When someone comments on a thing you";
                                                            }
                                                            ?>
                                                            <?php echo LIKED_BUTTON; ?>
                                                        </label>
                                                    </div>

                                                    <div class="checkbox checkbox-primary col-md-6">
                                                        <input type="checkbox" name="featured" id="featured" <?php echo (in_array('featured', $emailNotifications)) ? 'checked="checked"' : ''; ?>>
                                                        <label for="featured">
                                                            <?php
                                                            if ($this->lang->line('notify_thing_feature') != '') {
                                                                echo stripslashes($this->lang->line('notify_thing_feature'));
                                                            } else {
                                                                echo "When one of your things is featured";
                                                            }
                                                            ?>
                                                        </label>
                                                    </div>

                                                    <div class="checkbox checkbox-primary col-md-6">
                                                        <input type="checkbox" name="comments" id="comments" <?php echo (in_array('comments', $emailNotifications)) ? 'checked="checked"' : ''; ?>>
                                                        <label for="comments">
                                                            <?php
                                                            if ($this->lang->line('cmt_on_ur_thing') != '') {
                                                                echo stripslashes($this->lang->line('cmt_on_ur_thing'));
                                                            } else
                                                                echo "When someone comments on your thing";
                                                            ?>
                                                        </label>
                                                    </div>

                                                    <div class="checkbox checkbox-primary col-md-6">
                                                        <input type="checkbox" name="review-comments" id="review-comments" <?php echo (in_array('review-comments', $emailNotifications)) ? 'checked="checked"' : ''; ?>>
                                                        <label for="review-comments">
                                                            <?php
                                                            if ($this->lang->line('cmt_on_ur_thing') != '') {
                                                                echo stripslashes($this->lang->line('cmt_on_ur_thing_seller_dashboard'));
                                                            } else
                                                                echo "When someone comments on your seller dashboard";
                                                            ?>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr/>
                                            <div class="row row-eq-height">
                                                <div class="col-md-3 text-center">
                                                    <h2>
                                                        <?php
                                                        if ($this->lang->line('referrals_notification') != '') {
                                                            echo stripslashes($this->lang->line('referrals_notification'));
                                                        } else
                                                            echo "Notifications";
                                                        ?>
                                                        <br/>
                                                        <small>
                                                            <?php
                                                            if ($this->lang->line('notify_web_sett') != '') {
                                                                echo stripslashes($this->lang->line('notify_web_sett'));
                                                            } else
                                                                echo "Web settings";
                                                            ?>
                                                        </small>
                                                    </h2>
                                                </div>
                                                <div class="col-md-9">
                                                    <span class="description">
                                                        <?php
                                                        if ($this->lang->line('notify_notify_showup') != '') {
                                                            echo stripslashes($this->lang->line('notify_notify_showup'));
                                                        } else
                                                            echo "The web notifications show up in the topbar of the";
                                                        ?> <?php echo $siteTitle; ?> <?php
                                                        if ($this->lang->line('settings_website') != '') {
                                                            echo stripslashes($this->lang->line('settings_website'));
                                                        } else
                                                            echo "website";
                                                        ?>.
                                                    </span>
                                                    <br/>
                                                    <br/>
                                                    <label class="clear">
                                                        <?php
                                                        if ($this->lang->line('notify_active_involves') != '') {
                                                            echo stripslashes($this->lang->line('notify_active_involves'));
                                                        } else
                                                            echo "Activity that involves you";
                                                        ?>
                                                    </label>
                                                    <?php
                                                    $noty = explode(',', $userDetails->row()->notifications);
                                                    if (is_array($noty)) {
                                                        $notifications = $noty;
                                                    }
                                                    ?>

                                                    <div class="row">
                                                        <div class="checkbox checkbox-primary col-md-6">
                                                            <input type="checkbox" name="wmn-follow" id="wmn-follow" <?php echo (in_array('wmn-follow', $notifications)) ? 'checked="checked"' : ''; ?>>
                                                            <label for="wmn-follow">
                                                                <?php
                                                                if ($this->lang->line('notify_some_follu') != '') {
                                                                    echo stripslashes($this->lang->line('notify_some_follu'));
                                                                } else
                                                                    echo "When someone follows you";
                                                                ?>
                                                            </label>
                                                        </div>

                                                        <div class="checkbox checkbox-primary col-md-6">
                                                            <input type="checkbox" name="wmn-comments_on_fancyd" id="wmn-comments_on_fancyd" <?php echo (in_array('wmn-comments_on_fancyd', $notifications)) ? 'checked="checked"' : ''; ?>>
                                                            <label for="wmn-comments_on_fancyd">
                                                                <?php
                                                                if ($this->lang->line('notify_comm_things') != '') {
                                                                    echo stripslashes($this->lang->line('notify_comm_things'));
                                                                } else
                                                                    echo "When someone comments on a thing you";
                                                                ?> <?php echo LIKED_BUTTON; ?>
                                                            </label>
                                                        </div>

                                                        <div class="checkbox checkbox-primary col-md-6">
                                                            <input type="checkbox" name="wmn-fancyd" id="wmn-fancyd" <?php echo (in_array('wmn-fancyd', $notifications)) ? 'checked="checked"' : ''; ?>>
                                                            <label for="wmn-fancyd">
                                                                <?php
                                                                if ($this->lang->line('notify_when_some') != '') {
                                                                    echo stripslashes($this->lang->line('notify_when_some'));
                                                                } else
                                                                    echo "When someone";
                                                                ?> <?php echo LIKE_BUTTON; ?> <?php
                                                                if ($this->lang->line('notify_ur_thing') != '') {
                                                                    echo stripslashes($this->lang->line('notify_ur_thing'));
                                                                } else
                                                                    echo "one of your things";
                                                                ?>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="checkbox checkbox-primary col-md-6">
                                                            <input type="checkbox" name="wmn-featured" id="wmn-featured" <?php echo (in_array('wmn-featured', $notifications)) ? 'checked="checked"' : ''; ?>>
                                                            <label for="wmn-featured">
                                                                <?php
                                                                if ($this->lang->line('notify_thing_feature') != '') {
                                                                    echo stripslashes($this->lang->line('notify_thing_feature'));
                                                                } else
                                                                    echo "When one of your things is featured";
                                                                ?>
                                                            </label>
                                                        </div>

                                                        <div class="checkbox checkbox-primary col-md-6">
                                                            <input type="checkbox" name="wmn-comments" id="wmn-comments" <?php echo (in_array('wmn-comments', $notifications)) ? 'checked="checked"' : ''; ?>>
                                                            <label for="wmn-comments">
                                                                <?php
                                                                if ($this->lang->line('cmt_on_ur_thing') != '') {
                                                                    echo stripslashes($this->lang->line('cmt_on_ur_thing'));
                                                                } else
                                                                    echo "When someone comments on your thing";
                                                                ?>
                                                            </label>
                                                        </div>

                                                        <div class="checkbox checkbox-primary col-md-6">
                                                            <input type="checkbox" name="wmn-review-comments" id="wmn-review-comments" <?php echo (in_array('wmn-review-comments', $notifications)) ? 'checked="checked"' : ''; ?>>
                                                            <label for="wmn-review-comments">
                                                                <?php
                                                                if ($this->lang->line('cmt_on_seller_discussion') != '') {
                                                                    echo stripslashes($this->lang->line('cmt_on_seller_discussion'));
                                                                } else
                                                                    echo "When someone comments on a product's seller discussion";
                                                                ?>
                                                            </label>
                                                        </div>

                                                        <div class="checkbox checkbox-primary col-md-6">
                                                            <input type="checkbox" name="wmn-purchased" id="wmn-purchased" <?php echo (in_array('wmn-purchased', $notifications)) ? 'checked="checked"' : ''; ?>>
                                                            <label for="wmn-purchased">
                                                                <?php
                                                                if ($this->lang->line('cmt_on_seller_purchase') != '') {
                                                                    echo stripslashes($this->lang->line('cmt_on_seller_purchase'));
                                                                } else
                                                                    echo "When someone Purchase your product";
                                                                ?>
                                                            </label>
                                                        </div>

                                                        <div class="checkbox checkbox-primary col-md-6">
                                                            <input type="checkbox" name="wmn-auction-process" id="wmn-auction-process" <?php echo (in_array('wmn-auction-process', $notifications)) ? 'checked="checked"' : ''; ?>>
                                                            <label for="wmn-auction-process">
                                                                <?php
                                                                if ($this->lang->line('auction_process') != '') {
                                                                    echo stripslashes($this->lang->line('auction_process'));
                                                                } else
                                                                    echo "Auction Process";
                                                                ?>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr/>
                                            <div class="row row-eq-height">
                                                <div class="col-md-3 text-center">
                                                    <h2>
                                                        <?php
                                                        if ($this->lang->line('notify_update') != '') {
                                                            echo stripslashes($this->lang->line('notify_update'));
                                                        } else
                                                            echo "Updates";
                                                        ?>
                                                    </h2>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="row">
                                                        <label class="clear">
                                                            <?php
                                                            if ($this->lang->line('notify_update_from') != '') {
                                                                echo stripslashes($this->lang->line('notify_update_from'));
                                                            } else
                                                                echo "Updates from";
                                                            ?> <?php echo $siteTitle; ?>
                                                        </label>
                                                        <div class="checkbox checkbox-primary col-md-6">
                                                            <input type="checkbox" id="updates" name="updates" id="updates" <?php echo ($userDetails->row()->updates == '1') ? 'checked="checked"' : ''; ?>>
                                                            <label for="wmn-follow">
                                                                <?php
                                                                if ($this->lang->line('notify_send_news') != '') {
                                                                    echo stripslashes($this->lang->line('notify_send_news'));
                                                                } else {
                                                                    echo "Send news about";
                                                                }
                                                                ?> <?php echo $siteTitle; ?>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <input type="submit" id="save_notifications" class="btn btn-primary btn-sm pull-right btn-save"  value="<?php echo ($this->lang->line('notify_save_change') != '') ? stripslashes($this->lang->line('notify_save_change')) : "Save Changes"; ?>" />
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </form>
                </div>
                <div role="tabpanel" class="tab-pane" id="purchases">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <span class="fa fa-ship"></span>
                                <?php
                                if ($this->lang->line('purchases_common') != '') {
                                    echo stripslashes($this->lang->line('purchases_common'));
                                } else
                                    echo "Purchase History";
                                ?>
                            </h4>
                        </div>
                        <div class="panel-body p-sm">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php if ($purchasesList->num_rows() > 0) { ?>
                                        <div class="table-responsive">
                                            <table class="table table-striped" id="purchasesList">
                                                <thead>
                                                    <tr>
                                                        <th><?php
                                                            if ($this->lang->line('purchases__invoice') != '') {
                                                                echo stripslashes($this->lang->line('purchases__invoice'));
                                                            } else
                                                                echo "Invoice";
                                                            ?></th>
                                                        <th><?php
                                                            if ($this->lang->line('purchases__paystatus') != '') {
                                                                echo stripslashes($this->lang->line('purchases__paystatus'));
                                                            } else
                                                                echo "Payment Status";
                                                            ?></th>
                                                        <th><?php
                                                            if ($this->lang->line('purchases_shipstatus') != '') {
                                                                echo stripslashes($this->lang->line('purchases_shipstatus'));
                                                            } else
                                                                echo "Shipping Status";
                                                            ?></th>
                                                        <th><?php
                                                            if ($this->lang->line('purchases_total') != '') {
                                                                echo stripslashes($this->lang->line('purchases_total'));
                                                            } else
                                                                echo "Total";
                                                            ?></th>
                                                        <th><?php
                                                            if ($this->lang->line('purchases_orddate') != '') {
                                                                echo stripslashes($this->lang->line('purchases_orddate'));
                                                            } else
                                                                echo "Order-Date";
                                                            ?></th>
                                                        <th><?php
                                                            if ($this->lang->line('purchases_option') != '') {
                                                                echo stripslashes($this->lang->line('purchases_option'));
                                                            } else
                                                                echo "Option";
                                                            ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($purchasesList->result() as $row) { ?>
                                                        <tr>
                                                            <td>#<?php echo $row->dealCodeNumber; ?></td>
                                                            <td><?php
                                                                if ($row->type == 'auction' && $row->product_type == 'physical' && $row->do_capture == 'Pending' && $row->payment_type == 'Paypal Authorize Payment') {
                                                                    echo 'Authorized';
                                                                } else {
                                                                    echo $row->status;
                                                                }
                                                                ?>
                                                            </td>

                                                            <td><?php echo $row->shipping_status; ?></td>
                                                            <td><?php echo $this->data['currencySymbol'] . ' ' . $row->total; ?></td>

                                                            <td><?php echo $row->created; ?></td>
                                    <!--                         <td><a target="_blank" href="view-purchase/<?php echo $row->user_id; ?>/<?php echo $row->dealCodeNumber; ?>"><?php
                                                            if ($this->lang->line('purchases_view') != '') {
                                                                echo stripslashes($this->lang->line('purchases_view'));
                                                            } else
                                                                echo "View";
                                                            ?></a></td>
                                                            -->                        <td>
                                                                <a style="color:green;" target="_blank" href="view-purchase/<?php echo $row->user_id; ?>/<?php echo $row->dealCodeNumber; ?>"><?php
                                                                    if ($this->lang->line('view_invoice') != '') {
                                                                        echo stripslashes($this->lang->line('view_invoice'));
                                                                    } else
                                                                        echo "View Invoice";
                                                                    ?></a><br/>
                                                                <?php if ($row->status == 'Paid') { ?>
                                                                    <a style="color:red;" href="order-review/<?php echo $row->user_id; ?>/<?php echo $row->sell_id; ?>/<?php echo $row->dealCodeNumber; ?>"><?php
                                                                        if ($this->lang->line('seller_discuss') != '') {
                                                                            echo stripslashes($this->lang->line('seller_discuss'));
                                                                        } else
                                                                            echo "Seller Discussion";
                                                                        ?></a>
                                                                <?php } ?>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>

                                                </tbody>
                                            </table>
                                        </div>
                                    <?php } else { ?>
                                        <div class="alert alert-info alert-dismissable">
                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                            <?php
                                            if ($this->lang->line('purchases_not_purchase') != '') {
                                                echo stripslashes($this->lang->line('purchases_not_purchase'));
                                            } else
                                                echo "You haven't made any purchases yet.";
                                            ?>
                                        </div>
                                    <?php } ?>

                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="subscriptions">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <span class="fa fa-bookmark"></span>
                                <?php
                                if ($this->lang->line('referrals_subscribe') != '') {
                                    echo stripslashes($this->lang->line('referrals_subscribe'));
                                } else
                                    echo "Subscriptions";
                                ?>
                            </h4>
                        </div>
                        <div class="panel-body p-sm">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php if ($subscribeList->num_rows() > 0) { ?>
                                        <div class="table-responsive">
                                            <table class="table table-striped" id="subscriptionList">
                                                <thead>
                                                    <tr>
                                                        <th><?php
                                                            if ($this->lang->line('purchases__invoice') != '') {
                                                                echo stripslashes($this->lang->line('purchases__invoice'));
                                                            } else
                                                                echo "Invoice";
                                                            ?></th>
                                                        <th><?php
                                                            if ($this->lang->line('manage_subsname') != '') {
                                                                echo stripslashes($this->lang->line('manage_subsname'));
                                                            } else
                                                                echo "Subscription Name";
                                                            ?></th>
                                                        <th><?php
                                                            if ($this->lang->line('purchases_total') != '') {
                                                                echo stripslashes($this->lang->line('purchases_total'));
                                                            } else
                                                                echo "Total";
                                                            ?></th>
                                                        <th><?php
                                                            if ($this->lang->line('order_date') != '') {
                                                                echo stripslashes($this->lang->line('order_date'));
                                                            } else
                                                                echo "Date";
                                                            ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($subscribeList->result() as $row) { ?>
                                                        <tr>
                                                            <td>#<?php echo $row->invoice_no; ?></td>
                                                            <td><?php echo $row->name; ?></td>
                                                            <td><?php echo $row->total; ?></td>

                                                            <td><?php echo $row->created; ?></td>
                                                        </tr>
                                                    <?php } ?>

                                                </tbody>
                                            </table>
                                        </div>
                                    <?php } else { ?>
                                        <div class="alert alert-info alert-dismissable">
                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                            <?php
                                            if ($this->lang->line('manage_not_subs') != '') {
                                                echo stripslashes($this->lang->line('manage_not_subs'));
                                            } else
                                                echo "You haven't subscribed to anything yet.";
                                            ?>
                                        </div>
                                    <?php } ?>

                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="shipping">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <span class="fa fa-history"></span>
                                <?php
                                if ($this->lang->line('shipping_address') != '') {
                                    echo stripslashes($this->lang->line('shipping_address'));
                                } else
                                    echo "Shipping Address";
                                ?>
                            </h4>
                        </div>
                        <div class="panel-body p-sm">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php if ($shippingList->num_rows() > 0) { ?>
                                        <?php foreach ($shippingList->result() as $row) { ?>
                                            <div class="col-md-3">
                                                <div class="ibox" aid="<?php echo $row->id; ?>" isdefault="<?php echo ($row->primary == 'Yes') ? TRUE : FALSE; ?>">
                                                    <div class="ibox-content product-box">
                                                        <div class="product-desc">
                                                            <span class="product-price" style="display: <?php echo ($row->primary == 'Yes') ? 'block' : 'none'; ?>">
                                                                Default
                                                            </span>
                                                            <small class="text-muted"><?php echo $row->phone; ?></small>
                                                            <a class="product-name text-center"><?php echo $row->nick_name; ?></a>

                                                            <div class="small m-t-xs">
                                                                <?php echo $row->address1 . ', ' . $row->address2 . '<br/>' . $row->city . '<br/>' . $row->state . '<br/>' . $row->country . '-' . $row->postal_code; ?>
                                                            </div>
                                                            <div class="m-t text-righ">
                                                                <div class="text-center m-b-xs">
                                                                    <button class="btn btn-xs btn-success" style="visibility: <?php echo ($row->primary == 'Yes') ? 'hidden' : 'visible'; ?>"><i class="fa fa-star"></i>Make Default</button>
                                                                </div>
                                                                <div class="text-center">
                                                                    <button class="btn btn-xs btn-info edit_" aid="<?php echo $row->id; ?>"><i class="fa fa-edit"></i><?php echo ($this->lang->line('shipping_edit') != '') ? stripslashes($this->lang->line('shipping_edit')) : "Edit"; ?></button>
                                                                    <button class="btn btn-xs btn-danger remove_"><i class="fa fa-times"></i><?php echo ($this->lang->line('shipping_delete') != '') ? stripslashes($this->lang->line('shipping_delete')) : "Delete"; ?></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    <?php } else { ?>
                                        <div class="alert alert-info alert-dismissable">
                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                            <?php
                                            if ($this->lang->line('shipping_no_shipaddr') != '') {
                                                echo stripslashes($this->lang->line('shipping_no_shipaddr'));
                                            } else
                                                echo "You haven't added any shipping address yet.";
                                            ?>
                                        </div>
                                        <div class="text-center">
                                            <button class="btn-shipping add_ btn btn-primary">
                                                <i class="fa fa-plus"></i>
                                                <?php
                                                if ($this->lang->line('shipping_add_ship') != '') {
                                                    echo stripslashes($this->lang->line('shipping_add_ship'));
                                                } else
                                                    echo "Add Shipping Address";
                                                ?>
                                            </button>
                                        </div>
                                    <?php } ?>

                                </div>
                            </div>


                            <div class="modal inmodal fade" id="addEditShippingAddress" data-backdrop="static" data-keyboard="false" role="dialog" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <h4 class="modal-title shippingTitle add">
                                                <?php
                                                if ($this->lang->line('shipping_add_ship') != '') {
                                                    echo stripslashes($this->lang->line('shipping_add_ship'));
                                                } else
                                                    echo "Add Shipping Address";
                                                ?>
                                            </h4>                                            
                                        </div>

                                        <form role="form" class="ltxt" method="post" id="shippingAddForm" action="site/user_settings/insertEdit_shipping_address" novalidate="novalidate">
                                            <div class="modal-body">
                                                <div>
                                                    <h4><b>New Shipping Address</b></h4>
                                                    <small>We ships worldwide with global delivery services.</small>  
                                                </div>
                                                <div class="row m-t-md">
                                                    <div class="col-md-6">
                                                        <label>Full Name</label>
                                                        <input name="full_name" class="full required form-control" type="text">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Nick Name</label>
                                                        <input name="nick_name" class="full required form-control" placeholder="E.g. Home, Work, Aunt Jane" type="text">
                                                    </div>
                                                </div>
                                                <div class="row m-t-md">
                                                    <div class="col-md-6">
                                                        <label>Address Line 1</label>
                                                        <input name="address1" class="full required form-control" type="text">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Address Line 2</label>
                                                        <input name="address2" class="full required form-control" type="text">
                                                    </div>
                                                </div>
                                                <div class="row m-t-md">
                                                    <div class="col-md-4">
                                                        <label>Country</label>
                                                        <input name="full_name" class="full required form-control" type="text">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>State / Province</label>
                                                        <input name="full_name" class="full required form-control" type="text">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>City</label>
                                                        <input name="full_name" class="full required form-control" type="text">
                                                    </div>
                                                </div>
                                                <div class="row m-t-md">
                                                    <div class="col-md-6">
                                                        <label>Telephone</label>
                                                        <input name="phone" class="full required digits form-control" placeholder="10 digits only, no dashes" type="text">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Zip / Postal Code</label>
                                                        <input name="postal_code" class="zip required form-control" type="text">
                                                    </div>
                                                </div>
                                                <div class="row m-t-md">
                                                    <div class="col-md-12 .checkbox-circle">                                                        

                                                        <label class="checkbox-inline i-checks">
                                                            <input name="set_default" id="make_this_primary_addr" value="true" type="checkbox"> Make this my primary shipping address
                                                        </label>                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary">Save changes</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="panel-footer">
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="giftcard">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <span class="fa fa-gift"></span>
                                <?php
                                if ($this->lang->line('giftcard_cards') != '') {
                                    echo stripslashes($this->lang->line('giftcard_cards'));
                                } else
                                    echo "Gift Cards";
                                ?>
                            </h4>
                        </div>
                        <div class="panel-body p-sm">
                            <div class="row">

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div><div style="padding:30px;">
        <h4><b>New Shipping Address</b></h4>
        <small>We ships worldwide with global delivery services.</small>  
    </div>
</div>
<script src="<?php echo base_url(); ?>js/site/<?php echo SITE_COMMON_DEFINE ?>shoplist.js" type="text/javascript"></script>
<?php $this->load->view('site/templates/footer'); ?>