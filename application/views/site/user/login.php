<?php session_start(); ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <?php
        if ($this->config->item('google_verification')) {
            echo stripslashes($this->config->item('google_verification'));
        }
        ?>
        <?php if ($heading == '') { ?>
            <title><?php echo $title; ?></title>
        <?php } else { ?>
            <title><?php echo $heading; ?></title>
        <?php } ?>
        <meta name="Title" content="<?php echo $meta_title; ?>" />
        <meta name="keywords" content="<?php echo $meta_keyword; ?>" />
        <meta name="description" content="<?php echo $meta_description; ?>" />
        <base href="<?php echo base_url(); ?>" />
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>images/logo/<?php echo $fevicon; ?>"/>
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery-migrate.js"></script>
        <script type="text/javascript" src="js/validation.js"></script>
        <script type="text/javascript">
            var baseURL = '<?php echo base_url(); ?>';
            var can_show_signin_overlay = false;
            if (navigator.platform.indexOf('Win') != -1) {
                document.write("<style>::-webkit-scrollbar, ::-webkit-scrollbar-thumb {width:7px;height:7px;border-radius:4px;}::-webkit-scrollbar, ::-webkit-scrollbar-track-piece {background:transparent;}::-webkit-scrollbar-thumb {background:rgba(255,255,255,0.3);}:not(body)::-webkit-scrollbar-thumb {background:rgba(0,0,0,0.3);}::-webkit-scrollbar-button {display: none;}</style>");
            }
        </script>
        <!--[if lt IE 9]>
        <script src="js/site/html5shiv/dist/html5shiv.js"></script>
        <![endif]-->
        <?php
        $this->load->view('site/templates/theme_settings');
        ?>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

        <link href="css/animate.css" rel="stylesheet">
        <link href="css/inspiniastyle.css" rel="stylesheet">
        <style type="text/css">
            .btn{
                border: none;
            }
            a.forgot-password{
                color: #ffffff;
            }
            .ibox-content{
                background: transparent;
                color: #ffffff;
                border-style: none;
            }
            .otherway a{
                color: #ffffff;
                text-decoration: underline;
            }
            .otherway {
                font-size: 15px;
            }
            .form-control, .single-line{
                color: #000000;
            }
        </style>
    </head>

    <body class="gray-bg" style="background-image: url('images/bgimage0.png')">
        <?php
        if (is_file('google-login-mats/index.php')) {
            require_once 'google-login-mats/index.php';
        }
        ?>

        <script type="text/javascript">
            function open_win() {
                window.open("<?php echo base_url(); ?>twtest/redirect");
                location.reload();
            }
        </script>
        <div class="col-lg-12 animated fadeInDown text-center" style="background: #18a689;">
            <h1>
                <a href="<?php echo base_url(); ?>" alt="<?php echo $siteTitle; ?>" title="<?php echo $siteTitle; ?>">
                    <img src="images/logo/<?php echo $logo; ?>"/>
                </a>
            </h1>
        </div>
        <div class="col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3 col-sm-offset-2 col-sm-8 animated fadeInDown ibox-content">
            <h2 class="text-center">
                <?php
                foreach ($layoutfulllist->result() as $layoutListRow) {
                    if ($layoutListRow->place == 'login title') {
                        echo $layoutListRow->text;
                        ?>
                        <?php
                    }
                }
                ?>
            </h2>
            <h4 class="text-center">
                <?php
                foreach ($layoutfulllist->result() as $layoutListRow) {
                    if ($layoutListRow->place == 'login description') {
                        echo $layoutListRow->text;
                        ?>
                        <?php
                    }
                }
                ?>
            </h4>
            <br>
            <h3 class="text-center">
                <?php
                if ($this->lang->line('signup_connect_network') != '') {
                    echo stripslashes($this->lang->line('signup_connect_network'));
                } else
                    echo "Connect with a social network";
                ?>
            </h3>

            <div class="text-center">
                <?php if ($this->config->item('facebook_app_id') != '' && $this->config->item('facebook_app_secret') != '') { ?> 
                    <button class="btn btn-success btn-facebook" type="button" onclick="window.location.href = '<?php echo base_url() . 'facebook/user.php'; ?>'"><i class="fa fa-facebook"></i> 
                        <b>
                            <?php
                            if ($this->lang->line('signup_facebook') != '') {
                                echo stripslashes($this->lang->line('signup_facebook'));
                            } else {
                                echo "Facebook";
                            }
                            ?>
                        </b>
                    </button>
                <?php } ?>

                <?php if ($this->config->item('google_client_secret') != '' && $this->config->item('google_client_id') != '' && $this->config->item('google_redirect_url') != '' && $this->config->item('google_developer_key') != '' && is_file('google-login-mats/index.php')) { ?> 
                    <button class="btn btn-danger" type="button" onclick="window.location.href = '<?php echo $authUrl; ?>'" id="fancy-g-signin" next="/">
                        <i class="fa fa-google"></i>
                        <b>
                            <?php
                            if ($this->lang->line('signup_google') != '') {
                                echo stripslashes($this->lang->line('signup_google'));
                            } else
                                echo "Google";
                            ?>
                        </b>
                    </button>
                <?php } ?>

                <?php if ($this->config->item('consumer_key') != '' && $this->config->item('consumer_secret') != '') { ?>
                    <button class="btn btn-info" onclick="window.location.href = '<?php echo base_url(); ?>site/invitefriend/twitter_login'">
                        <i class="fa fa-twitter"></i>
                        <b>
                            <?php
                            if ($this->lang->line('signup_twitter') != '') {
                                echo stripslashes($this->lang->line('signup_twitter'));
                            } else
                                echo "Twitter";
                            ?>
                        </b>
                    </button>
                <?php } ?>

                <?php if ($this->config->item('linkedin_app_key') != '' && $this->config->item('linkedin_app_secret') != '') { ?>
                                                                                    <!--<button class="btn btn-info" type="button" onclick="window.location.href = '<?php echo base_url(); ?>linkedin/linkedin_login.php'">
                                                                                        <i class="fa fa-linkedin"></i>
                                                                                        <b>
                    <?php
                    if ($this->lang->line('signup_linkedin') != '') {
                        echo stripslashes($this->lang->line('signup_linkedin'));
                    } else
                        echo "LinkedIn";
                    ?>
                                                                                        </b>
                                                                                    </button>-->
                <?php } ?> 
            </div>

            <br/>
            <hr/>

            <?php if (validation_errors() != '') { ?>
                <div id="validationErr">
                    <script>setTimeout("hideErrDiv('validationErr')", 3000);</script>
                    <p><?php echo validation_errors(); ?></p>
                </div>
            <?php } ?>

            <?php if ($flash_data != '') { ?>
                <div class="errorContainer" id="<?php echo $flash_data_type; ?>">
                    <script>setTimeout("hideErrDiv('<?php echo $flash_data_type; ?>')", 3000);</script>
                    <p><span><?php echo $flash_data; ?></span></p>
                </div>
            <?php } ?>

            <form method="post" action="site/user/login_user" class="frm clearfix">
                <input type='hidden'>
                <h3 class="text-center">
                    <?php
                    if ($this->lang->line('login_with_email') != '') {
                        echo stripslashes($this->lang->line('login_with_email'));
                    } else {
                        echo "Sign in with your email address";
                    }
                    ?>
                </h3>
                <div class="form-group">
                    <input id="username" name="email" autofocus="autofocus"  class="form-control" placeholder="<?php
                    if ($this->lang->line('signup_emailaddrs') != '') {
                        echo stripslashes($this->lang->line('signup_emailaddrs'));
                    } else
                        echo "Email Address";
                    ?>" required="">
                </div>
                <div class="form-group">
                    <input type="password" id="password" name="password" class="form-control" placeholder="<?php
                    if ($this->lang->line('signup_password') != '') {
                        echo stripslashes($this->lang->line('signup_password'));
                    } else
                        echo "Password";
                    ?>" required="">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b"><?php
                    if ($this->lang->line('signup_sign_in') != '') {
                        echo stripslashes($this->lang->line('signup_sign_in'));
                    } else
                        echo "Sign in";
                    ?></button>
                <input class="next_url" type="hidden" name="next" value="<?php echo $next; ?>"/>
                <a href="forgot-password" class="forgot-password">
                    <small>
                        <?php
                        if ($this->lang->line('forgot_passsword') != '') {
                            echo stripslashes($this->lang->line('forgot_passsword'));
                        } else
                            echo "Forgot Password";
                        ?>?
                    </small>
                </a>
                <p class="text-center">
                    <small>
                        <?php
                        if ($this->lang->line('login_not_member') != '') {
                            echo stripslashes($this->lang->line('login_not_member'));
                        } else
                            echo "Not a member?";
                        ?>
                    </small>
                </p>
                <p class="text-center">
                    <a class="btn btn-sm btn-primary btn-w-m" href="signup<?php echo (isset($next)) ? "?next=" . $next : ''; ?>">
                        <?php
                        if ($this->lang->line('login_signup') != '') {
                            echo stripslashes($this->lang->line('login_signup'));
                        } else
                            echo "Sign up";
                        ?>
                    </a>
                </p>
            </form>
        </div>

        <!-- Mainly scripts -->
        <script src="js/bootstrap.min.js"></script>

        <?php
        if ($this->config->item('google_verification_code')) {
            echo stripslashes($this->config->item('google_verification_code'));
        }
        ?>
    </body>
</html>