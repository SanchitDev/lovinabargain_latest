<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <?php
        if ($this->config->item('google_verification')) {
            echo stripslashes($this->config->item('google_verification'));
        }
        ?>
        <?php if ($heading == '') { ?>
            <title><?php echo $title; ?></title>
        <?php } else { ?>
            <title><?php echo $heading; ?></title>
        <?php } ?>
        <meta name="Title" content="<?php echo $meta_title; ?>" />
        <meta name="keywords" content="<?php echo $meta_keyword; ?>" />
        <meta name="description" content="<?php echo $meta_description; ?>" />
        <base href="<?php echo base_url(); ?>" />

        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>images/logo/<?php echo $fevicon; ?>"/>
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery-migrate.js"></script>
        <script type="text/javascript" src="js/validation.js"></script>
        <script type="text/javascript">
            var can_show_signin_overlay = false;
            if (navigator.platform.indexOf('Win') != -1) {
                document.write("<style>::-webkit-scrollbar, ::-webkit-scrollbar-thumb {width:7px;height:7px;border-radius:4px;}::-webkit-scrollbar, ::-webkit-scrollbar-track-piece {background:transparent;}::-webkit-scrollbar-thumb {background:rgba(255,255,255,0.3);}:not(body)::-webkit-scrollbar-thumb {background:rgba(0,0,0,0.3);}::-webkit-scrollbar-button {display: none;}</style>");
            }
        </script>
        <!--[if lt IE 9]>
        <script src="js/site/html5shiv/dist/html5shiv.js"></script>
        <![endif]-->
        <?php
        $this->load->view('site/templates/theme_settings');
        ?>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

        <link href="css/animate.css" rel="stylesheet">
        <link href="css/inspiniastyle.css" rel="stylesheet">

        <style type="text/css">
            a.sign-in{
                color: #ffffff;
                text-decoration: underline;
            }
            .ibox-content{
                background: transparent;
                color: #ffffff;
                border-style: none;
            }
            .otherway a{
                color: #ffffff;
                text-decoration: underline;
            }
            .otherway {
                font-size: 15px;
            }
            .form-control, .single-line{
                color: #000000;
            }
        </style>
    </head>

    <body class="gray-bg" style="background-image: url('images/bgimage0.png')">
        <div class="col-lg-12 text-center animated fadeInDown" style="background: #18a689;">
            <h1>
                <a href="<?php echo base_url(); ?>" alt="<?php echo $siteTitle; ?>" title="<?php echo $siteTitle; ?>">
                    <img src="images/logo/<?php echo $logo; ?>"/>
                </a>
            </h1>
        </div>
        <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 animated fadeInDown ibox-content">
            <h2 class="text-center">
                <?php
                if ($this->lang->line('forgot_passsword') != '') {
                    echo stripslashes($this->lang->line('forgot_passsword'));
                } else
                    echo "Forgot Password";
                ?>
            </h2>

            <h4 class="text-center">
                <?php
                if ($this->lang->line('forgot_enter_email') != '') {
                    echo stripslashes($this->lang->line('forgot_enter_email'));
                } else
                    echo "Forgot your password? Enter your email address to reset it.";
                ?>
            </h4>


            <?php if (validation_errors() != '') { ?>
                <div id="validationErr">
                    <script>setTimeout("hideErrDiv('validationErr')", 3000);</script>
                    <p><?php echo validation_errors(); ?></p>
                </div>
            <?php } ?>
            <?php if ($flash_data != '') { ?>
                <div class="errorContainer" id="<?php echo $flash_data_type; ?>">
                    <script>setTimeout("hideErrDiv('<?php echo $flash_data_type; ?>')", 3000);</script>
                    <p><span><?php echo $flash_data; ?></span></p>
                </div>
            <?php } ?>
            <form method="post" action="site/user/forgot_password_user" class="frm clearfix"><input type='hidden' />
                <div class="form-group">
                    <input type="text" id="username" name="email" autofocus="autofocus" class="form-control" placeholder="<?php
                    if ($this->lang->line('signup_emailaddrs') != '') {
                        echo stripslashes($this->lang->line('signup_emailaddrs'));
                    } else
                        echo "Email Address";
                    ?>" required="">
                    <input class="next_url" type="hidden" name="next" value="/"/>
                </div>

                <button type="submit" class="btn btn-primary block full-width m-b"><?php
                    if ($this->lang->line('forgot_reset_pwd') != '') {
                        echo stripslashes($this->lang->line('forgot_reset_pwd'));
                    } else
                        echo "Reset Password";
                    ?></button>

            </form>

            <hr/>

            <div class="text-center">
                <?php
                if ($this->lang->line('forgot_never_mind') != '') {
                    echo stripslashes($this->lang->line('forgot_never_mind'));
                } else
                    echo "Never mind?";
                ?> 
                <a href="login" class="sign-in"><?php echo ($this->lang->line('signup_sign_in') != '') ? stripslashes($this->lang->line('signup_sign_in')) : "Sign In"; ?></a>
            </div>
        </div>
    </body>
    <?php
    if ($this->config->item('google_verification_code')) {
        echo stripslashes($this->config->item('google_verification_code'));
    }
    ?>
</body>
</html>
