<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $siteTitle; ?></title>
        <?php
        $is_secure = substr(base_url(), 0, 5);
        if ($is_secure == 'http:') {
            $is_secure = 'http';
        }
        if ($is_secure == '') {
            $is_secure = 'http';
        }
        ?>
        <script type="text/javascript" src="<?php echo $is_secure; ?>://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <style type="text/css">
            body{
                float: left;
                outline:none;
                height: auto;
                width: 277px;
                margin:0;
                min-height: 380px;
                min-width: 281px;
            }
            #bookmarklet-tagger-iframe{
                width:298px !important;
            }
            .top_head {
                background-color: #18a689;
                margin-bottom: 5px;
                padding: 10px;
                width: 281px;
            }
            .add_thing .img-pick {
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
                border: 1px solid #CFCFCF;
                box-shadow: 0 1px 0 #F2F2F2;
                float: left;
                height: 23px;
                text-align: center;
                vertical-align: middle;
                width: 40px;
                border-radius: 0 3px 3px 0;
                background: #fff;
                text-decoration: none;
                font-weight: bold;
                color: #000;
            }
            img#add_picked-image {
                max-width: 200px;
                max-height: 100px;
            }
            
           
            .noimg {
                float: left;
                width: 100%;
                text-align: center;
                margin-top: 50px;
                font-weight: bold;
                color: black;
                font-size: 20px;
            }
            .clear{clear:both;}
            #bookmarklet-tagger-iframe{
                min-height: 358px !important;
                border: 0px none !important;
            }
        </style>

        <script type="text/javascript" src="<?php echo base_url(); ?>js/bookmarklet/bookmarklet.js"></script>
    </head>

    <body>
        <div id="main col-md-12">
            <form class="no_image" style="display:none">
                <div class="top_head pull-left" >
                    <a style="float:left;" target="parent" href="<?php echo base_url(); ?>"><img height="19px" src="<?php echo base_url(); ?>images/logo/<?php echo $logo; ?>"/></a>
                    <a style="float:right;font-weight: bold;color: #fff;font-size: 20px;text-decoration:none;" title="Close this" href="javascript:void(0)" class="close_box"><i class="fa fa-times"></i></a>
                </div>
                <div class="main frm col-md-12" style="float: left;margin: 5px;">
                    <img src="<?php echo base_url(); ?>images/error_icon.png" width="73" height="92">
                    <p><strong>Couldn't find any good images on this page.</strong></p>
                    <p>The images might not be large enough, or they might be protected or inside a web plugin.</p>
                    <p>Try again on a different page.</p>
                </div>
                <div class="footer" style="text-align:center">
                    <a class="btn btn-primary close_box" style="text-align:center;" href="javascrip:;">Cancel</a>
                </div>
            </form>
            <form class="add_thing" style="display:none">
                <div class="top_head" >
                    <a style="float:left;" target="parent" href="<?php echo base_url(); ?>"><img height="19px" src="<?php echo base_url(); ?>images/logo/<?php echo $logo; ?>"/></a>
                    <a style="float:right;font-weight: bold;color: #fff;font-size: 20px;text-decoration:none;" title="Close this" href="javascript:void(0)" class="close_box">X</a>
                </div>
                <div class="main frm" style="float: left;margin: 0px;">
                    <fieldset>
                        <img class="f-preview" id="add_picked-image">
                        <div id="add-imgpick">
                            <a class="img-pick" did="0" href="#">
                                Prev
                            </a>
                            <a class="img-pick" did="1" href="#">
                                Next
                            </a>
                            <span class="cur_" style="height: 23px;vertical-align: middle;margin-left: 5px;"><span>1</span> of <?php echo $total; ?></span>
                        </div>
                        <div class="clear"></div><br/>
                        <input type="hidden" value="" id="add_photo_url">
                        <input type="hidden" value="<?php echo $uid; ?>" id="add_uid">
                        <input type="hidden" value="<?php echo base_url(); ?>" id="baseurl">
                        <label>Title</label>
                        <div class="clear"></div>
                        <input type="text" class="input-text" id="add_name" style="width: 228px;height: 25px;">
                        <div class="clear"></div>
                        <label>Web Link</label>
                        <div class="clear"></div>
                        <input type="text" class="input-text" placeholder="http://" id="add_link" style="width: 228px;height: 25px;">
                        <div class="clear"></div>
                        <!--                     <label>Price</label>
                                            <div class="clear"></div>
                                            <input type="text" class="input-text" id="add_price" style="width: 228px;height: 25px;">
                                            <div class="clear"></div>
                        -->                     <?php if ($mainCategories->num_rows() > 0) { ?>
                            <label>Category</label>
                            <div class="clear"></div>
                            <select class="select-round selectBox categories_" id="add_category" style="width: 235px; padding:5px 10px 5px 5px; height: 30px;border: 1px solid #B4B9C7;">
                                <option value="">Choose a category</option>
                                <?php
                                foreach ($mainCategories->result() as $row) {
                                    if ($row->cat_name != '') {
                                        ?>
                                        <option value="<?php echo $row->id; ?>"><?php echo $row->cat_name; ?></option>
                                        <?php
                                    }
                                }
                                ?>                      
                            </select>
                        <?php } ?>                           
                        <div class="clear"></div>
                    </fieldset>
                    <div id="if-success" style="display:none">
                        <div style="float: left;"><img src="<?php echo base_url(); ?>images/success.png"></div>
                        <p> Product has been added to your list!</p>
                        <a href="<?php echo base_url(); ?>" target="_blank" style="float: left;text-align:center;" class="btn btn-primary">Go see it</a>
                    </div>
                </div>
                <div class="btns-area footer" >
                    <button class="btn btn-primary add_new_thing">Add</button>
                    <a href="javascript:;" class="m-b btn btn-primary cancel_add_thing">Cancel</a>
                </div>
            </form>
        </div>
    </body>
</html>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="font-awesome/css/font-awesome.css" rel="stylesheet">

        <link href="css/animate.css" rel="stylesheet">