<?php
$this->load->view('site/templates/header');
?>
<section class="content-section" style="height:450px; background: #f7f5f6;">
    <script type="text/javascript" src="js/site/jquery.mSimpleSlidebox.js"></script>
    <!-- slidebox function call -->
    <script type="text/javascript">
        $(document).ready(function () {
            $(".mSlidebox").mSlidebox({
                autoPlayTime: 4000,
                controlsPosition: {
                    buttonsPosition: "outside"
                }
            });
            $("#mSlidebox_3").mSlidebox({
                easeType: "easeInOutCirc",
                numberedThumbnails: true,
                pauseOnHover: false
            });
        });
    </script>
    <style>
        .wrapper-content {
            padding: 20px 10px 40px;
        }

        .ibox {
            clear: both;
            margin-bottom: 25px;
            margin-top: 0;
            padding: 0;
        }
        .ibox.collapsed .ibox-content {
            display: none;
        }
        .ibox.collapsed .fa.fa-chevron-up:before {
            content: "\f078";
        }
        .ibox.collapsed .fa.fa-chevron-down:before {
            content: "\f077";
        }
        .ibox:after,
        .ibox:before {
            display: table;
        }
        .ibox-title {
            -moz-border-bottom-colors: none;
            -moz-border-left-colors: none;
            -moz-border-right-colors: none;
            -moz-border-top-colors: none;
            background-color: #ffffff;
            border-color: #e7eaec;
            border-image: none;
            border-style: solid solid none;
            border-width: 3px 0 0;
            color: inherit;
            margin-bottom: 0;
            padding: 14px 15px 7px;
            min-height: 48px;
        }
        .ibox-content {
            background-color: #ffffff;
            color: inherit;
            padding: 15px 20px 20px 20px;
            border-color: #e7eaec;
            border-image: none;
            border-style: solid solid none;
            border-width: 1px 0;
        }
        .ibox-footer {
            color: inherit;
            border-top: 1px solid #e7eaec;
            font-size: 90%;
            background: #ffffff;
            padding: 10px 15px;
        }

        /* E-commerce */
        .product-box {
            padding: 0;
            border: 1px solid #e7eaec;
        }
        .product-box:hover,
        .product-box.active {
            border: 1px solid transparent;
            -webkit-box-shadow: 0 3px 7px 0 #a8a8a8;
            -moz-box-shadow: 0 3px 7px 0 #a8a8a8;
            box-shadow: 0 3px 7px 0 #a8a8a8;
        }
        .product-imi {
            text-align: center;
            padding: 140px 0;
            background-color: #f8f8f9;
            color: #bebec3;
            font-weight: 600;
        }
        .cart-product-imitation {
            text-align: center;
            padding-top: 30px;
            height: 80px;
            width: 80px;
            background-color: #f8f8f9;
        }
        .product-imi.xl {
            padding: 120px 0;
        }
        .product-desc {
            padding: 20px;
            position: relative;
        }
        .ecommerce .tag-list {
            padding: 0;
        }
        .ecommerce .fa-star {
            color: #d1dade;
        }
        .ecommerce .fa-star.active {
            color: #f8ac59;
        }
        .ecommerce .note-editor {
            border: 1px solid #e7eaec;
        }

    </style>
    <!-- Section_start -->
    <div class="lang-en wider no-subnav thing signed-out winOS">
        <div id="container-wrapper">
            <div class="container shoppage">
                <div class="animated fadeInRight">
                    <div class="row">
                        <?php
                        //if ($productDetails) {
                        $count = 0;
                        foreach ($productDetails as $productListVal) {
                            if ($count % 3 == 0) {
                                ?>
                                <div class="col-md-12">                                    
                                <?php } else { ?>
                                    <div class="col-md-6">
                                    <?php } ?>

                                    <div class="ibox">
                                        <div class="ibox-content product-box">

                                            <div class="product-imi" style="background:url('images/product/<?php echo trim($productListVal->image, ','); ?>') center;background-size: cover;"></div>
                                            <div class="product-desc">
                                                <span class="product-price">
                                                    <?php echo $currencySymbol; ?> <?php echo ($productListVal->price == null ? 0 : $productListVal->price); ?>
                                                </span>                                                
                                                <a href="#" class="product-name"><?php echo $productListVal->product_name; ?></a>
                                                <small>by</small>
                                                <a href="<?php echo base_url(); ?>user/seller-timeline/<?php echo $productListVal->user_name; ?>" target="_blank">
                                                    <?php echo $productListVal->full_name; ?>
                                                </a>

                                                <?php
                                                $userListHtml = "";
                                                foreach ($productListVal->users_list as $usersLiked) {
                                                    if (!empty($usersLiked->thumbnail)) {
                                                        $userListHtml = "<a href='" . base_url() . "user/" . $productListVal->user_name . "' target='_blank'><img alt='image' class='img-circle' src='" . base_url("images/users/" . $usersLiked->thumbnail) . "'></a>";
                                                    }
                                                }
                                                ?>

                                                <div class="small m-t-xs">
                                                    <div class="btn btn-xs btn-white <?php echo ($userListHtml != "") ? "pull-left" : ""; ?>">
                                                        <i class="fa fa-thumbs-up"></i>
                                                        <?php echo $productListVal->likes; ?>
                                                    </div>
                                                    <?php if (count($productListVal->users_list) > 0) { ?>
                                                        <dl class="m-l-xl" >
                                                            <dd class="project-people">
                                                                <?php echo $userListHtml; ?>
                                                            </dd>
                                                        </dl>
                                                    <?php } ?>
                                                </div>                                               
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                                <?php
                                $count++;
                            }
                            //}
                            ?>                                                             

                        </div>
                    </div>
                </div>

                <!-- / container -->
            </div>
        </div>
    </div>
    <?php
    $this->load->view('site/templates/footer');
    ?>