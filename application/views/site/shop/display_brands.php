<?php  






$this->load->view('site/templates/header');
?>
<!-- Section_start -->
<div class="lang-en wider no-subnav thing signed-out winOS">
	<div id="container-wrapper">
		<div class="container shoppage a2z-brandz">
		
		<div class="title-wrap">
			<h1 class="hero-title">A - Z of Brands</h1>
			<h3 class="sub-title">Big brands - low prices</h3>
		</div>
		
		<div class="brandz-logo">
			<ol >
				<?php if($brandslist->num_rows() > 0){ 
					foreach($brandslist->result() as $brands){
					?>
					<li class="">
						<a href="shopby/all?b=<?php echo $brands->brand_seourl; ?>"><img src="images/brand/<?php echo $brands->brand_logo; ?>" class=""/></a>
					</li>
				<?php } } ?>
			</ol>
		</div>
		
		<div class="brandz-details">
		
			<!-- <div class="sort-letter">
			 	<p>#</p>
			</div>
			
			<div class="brandz-content">
				<ul >
					<li><a href="#">#LAD</a></li>
				</ul>
			</div> -->
		
		</div>
		
		<?php 
			foreach($brandslist->result() as $brnd){
			 	$bname[] = substr(lcfirst($brnd->brand_name),0, 1);
			}
 			$array=  array_unique($bname);
 			//echo '<pre>';print_r($array);die;
			$k = '0'; $sm = '0'; 
				foreach($array as $brand){ 
                	foreach (range('a', 'z') as $char) {
                 		if($char == lcfirst($brand) ){ ?>
							<div class="brandz-details border-top-0" >
								<div class="sort-letter">
								 	<?php if($k != $char){ ?>
								 	<p><?php echo ucfirst($char);     $k = $char;$temp = 0; ?></p>
								 	<?php } ?>
								</div>
								
								<div class="brandz-content">
									<ul>

										<?php foreach($brandslist->result() as $brandss){ 
											if($char == lcfirst($brandss->brand_name[0]) ){ ?>
										<li><a href="shopby/all?b=<?php echo $brandss->brand_seourl; ?>"><?php echo $brandss->brand_name; ?></a>
										<div class="brand-hover">
											<img src="images/brand/<?php echo $brandss->brand_logo;?>" class=""/>
										</div>
										</li>
										<?php } } ?>
									</ul>
								</div>
							</div>
	<?php 				} 
					} 
				} ?>
		
		</div>
	</div>

	
	</div>
		<?php 
     $this->load->view('site/templates/footer_menu');
     ?>
		<a href="#header" id="scroll-to-top"><span><?php if($this->lang->line('signup_jump_top') != '') { echo stripslashes($this->lang->line('signup_jump_top')); } else echo "Jump to top"; ?></span></a>

	</div>
	
	<!-- / container -->
</div>

</div>
<?php
$this->load->view('site/templates/footer');
?>