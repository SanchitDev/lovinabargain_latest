<?php
$this->load->view('site/templates/header',$this->data);
?>
<link rel="stylesheet" media="all" type="text/css" href="css/site/<?php echo SITE_COMMON_DEFINE ?>groupgift.css">
<link rel="stylesheet" media="all" type="text/css" href="css/site/<?php echo SITE_COMMON_DEFINE ?>setting.css">
<!-- Section_start -->
<div class="lang-en no-subnav wider winOS">
<!-- Section_start -->
<div id="container-wrapper">
	<div class="container set_area">
		<?php if($flash_data != '') { ?>
		<div class="errorContainer" id="<?php echo $flash_data_type;?>">
			<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
			<p><span><?php echo $flash_data;?></span></p>
		</div>
		<?php } ?>
        <div id="content">
	<?php if ($groupgiftList->num_rows() == 0){?>
		<div class=" section shipping no-data">
			
			<span class="icon"><i class="ic-ship"></i></span>
			<p><?php if($this->lang->line('Your_havent_group_gift_history') != '') { echo stripslashes($this->lang->line('Your_havent_group_gift_history')); } else echo "You haven't group gift history yet."; ?></p>
			
			<!--<button class="btn-shipping add_"><i class="ic-plus"></i> <?php if($this->lang->line('Your_group_gift_history') != '') { echo stripslashes($this->lang->line('Your_group_gift_history')); } else echo "Your Fancy group gift history."; ?></button>-->
		</div>
	<?php 
	}else {
	?>
	<div class="section shipping">
            <h3><?php if($this->lang->line('Your_group_gift_history') != '') { echo stripslashes($this->lang->line('Your_group_gift_history')); } else echo "Your group gift History List."; ?></h3>
                	<div class="chart-wrap">
            <table class="chart">
                <thead>
                    <tr>
                        <th><?php if($this->lang->line('groupgift_name') != '') { echo stripslashes($this->lang->line('groupgift_name')); } else echo "Gift Name"; ?></th>
                        <th><?php if($this->lang->line('groupgift_status') != '') { echo stripslashes($this->lang->line('groupgift_status')); } else echo "Status"; ?></th>
                        <th><?php if($this->lang->line('groupgift_end_date') != '') { echo stripslashes($this->lang->line('groupgift_end_date')); } else echo "End Date"; ?></th>
                        <th><?php if($this->lang->line('groupgift_contribution') != '') { echo stripslashes($this->lang->line('groupgift_contribution')); } else echo "Contribution"; ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if($groupgiftList->num_rows() > 0){?>
					      <?php foreach ($groupgiftList->result() as $row){?>
                               <tr>
                                  <td><a style="text-decoration:underline;" href="gifts/<?php echo $row->gift_seller_id;?>"><?php echo $row->gift_name;?></a><br/>
								  <span><a href="gifts/<?php echo $row->gift_seller_id;?>">
								  <?php echo "#".$row->gift_seller_id;?>
								  </a></span>
								  </td>
                                  <?php if($row->status == "Cancelled") { ?>
                                  <td><?php echo "<b>Canceled(by creator)</b>";?></td>
								  <?php } elseif($row->status == "Cancelled Admin") {?>
								     <td><?php echo "<b>Canceled(by admin)</b>";?></td>
								  <?php } elseif($row->status == "Unsuccessful"){?>
								     <td><?php echo "<b>".$row->status."</b>";?></td>
								 <?php } else{?>
								      <td><?php echo "<b>".$row->status."</b>";?></td>
								  <?php }?>
                                  <td>
								  <?php $date = $row->created;
								        $date = strtotime($date);
                                        $date = strtotime("+".$this->config->item('expiry_days')." day", $date);
                                        echo date('M d, Y, h:i', $date);
								  ?>
								  </td>
								 <?php if($row->status == "Cancelled" || $row->status == "Cancelled Admin") {
								 ?>
                                 <td>
								  <?php echo "Cancelled";?><br/>
								 
								 </td>
								 <?php } elseif($row->status == "Successful") {?>
								 <td><?php echo $row->status ;?></td>
								 <?php } elseif($row->status == "Unsuccessful"){?>
								 <td><?php echo "<b>Expired</b>" ;?></td>
								 <?php }else{?>
								 <td><?php echo "<b>Pending</b>";?></td>
								 <?php }?>
                              </tr>
                        <?php }?>
					<?php } ?>
                    
                </tbody>
				
            </table>
			</div>
			</div>
	<?php 
	}
	?>
	<?php if ($groupgiftPayment->num_rows() == 0){?>
		<div class=" section purchases no-data">
			
			<span class="icon"><i class="ic-pur"></i></span>
			<p><?php if($this->lang->line('purchases_not_purchase') != '') { echo stripslashes($this->lang->line('purchases_not_purchase')); } else echo "You haven\'t made any purchases yet."; ?></p>
			
			<!--<button class="btn-shipping add_"><i class="ic-plus"></i> <?php if($this->lang->line('Your_group_gift_history') != '') { echo stripslashes($this->lang->line('Your_group_gift_history')); } else echo "Your Fancy group gift history."; ?></button>-->
		</div>
	<?php 
	}else {
	?>
	<div class="section shipping">
            <h3><?php if($this->lang->line('Your_group_gift_purchase') != '') { echo stripslashes($this->lang->line('Your_group_gift_purchase')); } else echo "Your contributed group gift List"; ?></h3>
                	<div class="chart-wrap">
            <table class="chart">
                <thead>
                    <tr>
                        <th><?php if($this->lang->line('groupgift_id') != '') { echo stripslashes($this->lang->line('groupgift_id')); } else echo "Gift Id"; ?></th>
                        <th><?php if($this->lang->line('contributor_name') != '') { echo stripslashes($this->lang->line('contributor_name')); } else echo "Contributor Name"; ?></th>
                        <th><?php if($this->lang->line('groupgift_payment_type') != '') { echo stripslashes($this->lang->line('groupgift_payment_type')); } else echo "Payment Type"; ?></th>
						<th><?php if($this->lang->line('groupgift_payment_amt') != '') { echo stripslashes($this->lang->line('groupgift_payment_amt')); } else echo "Amount"; ?></th>
                        <th><?php if($this->lang->line('groupgift_payment_txid') != '') { echo stripslashes($this->lang->line('groupgift_payment_txid')); } else echo "Transaction Id"; ?></th>
						<th><?php if($this->lang->line('groupgift_status') != '') { echo stripslashes($this->lang->line('groupgift_status')); } else echo "Status"; ?></th>
						<th><?php if($this->lang->line('groupgift_payment_created') != '') { echo stripslashes($this->lang->line('groupgift_payment_created')); } else echo "Created Date"; ?></th>
					</tr>
                </thead>
                <tbody>
                    <?php if($groupgiftPayment->num_rows() > 0){?>
					      <?php foreach ($groupgiftPayment->result() as $row){?>
                               <tr>
                                  <td><a style="text-decoration:underline;" href="gifts/<?php echo $row->groupgift_id;?>"><?php echo "#".$row->groupgift_id;?></a></td>
                                  <td><span><?php echo $row->contributor_name;?></span></td>
								  <td><span><?php echo "Paypal";?></span></td>
								  <td><span><?php echo $row->amount;?></span></td>
								  <td><span><?php echo $row->transaction_id;?></span></td>
								  <td><span><?php echo $row->status;?></span></td>
								  <td><span><?php echo $row->created;?></span></td>
                              </tr>
                        <?php }?>
					<?php } ?>
                    
                </tbody>
				
            </table>
			</div>
			</div>
	<?php 
	}
	?>
	</div>
		<?php 
		$this->load->view('site/user/settings_sidebar');
     $this->load->view('site/templates/side_footer_menu');
     ?>
	</div>
	<!-- / container -->
</div>
</div>
<!-- Section_start -->
<script type="text/javascript" src="js/site/<?php echo SITE_COMMON_DEFINE ?>address_helper.js"></script>
<script type="text/javascript" src="js/site/jquery.validate.js"></script>
<?php 
$this->load->view('site/templates/footer',$this->data);
?>
