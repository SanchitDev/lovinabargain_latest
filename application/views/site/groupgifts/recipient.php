<?php if($this->uri->segment(1) == 'things' && $this->uri->segment(3) != 'edit'){ ?>
<script	src="//connect.facebook.net/en_US/all.js"></script>
<script type="text/javascript">
$(document).ready(function(){
var pid = '<?php echo $productDetails->row()->seller_product_id;?>';
formURL = '<?php echo base_url();?>site/groupgift/InsertProduct';
vurl = '<?php echo base_url();?>site/groupgift/validate_country';
var popup1 = $.dialog('group_gifts');
var popup2 = $.dialog('group_giftsa');
var popup3 = $.dialog('group_giftsb');
/************ONLOAD SHOW & HIDE FUNCTION******************/	
	$('#pickafriend').show();
	$('.enter_manually_box1').hide();
	$('#entermanually').hide();
/*************************END*****************************/
/************RECIPIENT FORM SUBMIT FUNCTION***************/
$('.group-gift').click(function(event){

	var AttrCountVal ='<?php echo $PrdAttrVal->num_rows() ?>';
	event.preventDefault();
	<?php if($loginCheck == ''){ ?>
		window.location.href = baseURL+'login?next=things/<?php echo $productDetails->row()->id?>/<?php echo $productDetails->row()->seourl;?>'
	<?php }else{?>
		var url = '<?php echo base_url();?>site/groupgift/getCountry';
		sug_url = '<?php echo base_url();?>site/groupgift/suggest_friends';
		$.get(url,function(data){
			$("#gift_country").html(data);
		});
		$.get(sug_url,function(data){
			$("#suggested_friends ul").html(data);
		});
		if($('#attribute_must').val() == 'yes'){
			if(AttrCountVal > 0){
			var attribute_selected = 0;
			$('#AttrErr').html(' ');
			$(".attr_name_id").each(function(){
				var AttrVal = $(this).val();
				if(AttrVal == 0){
				}else{
					attribute_selected++;
				}
			});
			if(attribute_selected == 0){
				$('#AttrErr').html('Please Choose the Option');
				return false;
			}
		}
	}
		$('#gift_qty').val($('#quantity').val());
		$('#gift_qty1').val($('#quantity').val());
		$('#gift_price').val($('#SalePrice').text());
		$('#gift_price1').val($('#SalePrice').text());
		popup1.open();
	<?php } ?>
});
/*************************END*****************************/
/************PERSONALIZE BACK BUTTON FUNCTION*************/
$('#btn-back').click(function(event){
	value = $('#seller-id').val();
	$('#sellers-id').val(value);
	$('#seller_id').val(value); 
	var url = '<?php echo base_url();?>site/groupgift/delete_gift';
	$.post(url,{'seller_id':value},function(data){
		if(data ==1){
			$('#seller-id').val('');
			popup1.open();
			event.preventDefault();
		}else{
			popup2.open();
			event.preventDefault();
		}
	});
});
$("#go_connect_links").click(function(){
	$(".friends-lists dd ul").html('');
	 handleClientLoad();
});
$(".close-gift").click(function(){
	value = $('#seller-id').val();
	var url = '<?php echo base_url();?>site/groupgift/delete_gift';
	$.post(url,{'seller_id':value},function(data){	
		window.location.href = '<?php echo current_url();?>';
	});
});
/*************************END*****************************/
/************RECIPIENT CANCEL FUNCTION********************/
$('#btn-cancel').click(function(event){
	popup1.close();
});
/*************************END*****************************/
/************CONTRIBUTOR DONE FUNCTION****************/
$('#btn-done').click(function(event){
	window.location.href = baseURL+'gifts/'+$('#sellers-ids').val();
});
$('.change_friend').click(function(event){
	$('.friends-lists').show();
	$('.connent_share_links').show();
	$('.search-social-friends').show();
	$('.enter_manually_box1').hide();
	popup1.open();
	return false;
});
/*************************END*****************************/
/************RECIPIENT SEARCH FUNCTION********************/
$('.search-social-friends').keyup(function(){
	var value = $(this).val();
	url= "<?php echo base_url();?>site/groupgift/search_user";
	if(value != ''){
		$.post(url,{'search':value},function(html){
			$("#result").html(html).show();
		});
	}else{
		$("#result").hide();
	}return false;
});
jQuery("#result").live("click",function(e){ 
	var $clicked = $(e.target);
	var $name = $clicked.find('.name').html();
	if(!$name){
		$name = $clicked.html();
	}
	var decoded = $("<div/>").html($name).text();
	$('#searchid').val(decoded);
	url = "<?php echo base_url();?>site/groupgift/get_user_result";
	$.post(url,{'user_name':decoded},function(data){
		$('.connent_share_links').hide();
		$('.search-social-friends').hide();
		$('#friends-lists').hide();
		$('.enter_manually_box1').show();
		var split_val = data.split('+');
		$('#get_gift_fullname').val(split_val[0]);
		$('#get_gift_recipient').val(split_val[1]);
		$('#get_gift_address1').val(split_val[3]);
		$('#get_gift_address2').val(split_val[4]);
		$('#get_gift_city').val(split_val[5]);
		$('#get_gift_phone').val(split_val[9]);
		if(split_val[8] == 0){
			$('#get_gift_zipcode').val('');
		}else{
			$('#get_gift_zipcode').val(split_val[8]);
		}
		$('#get_gift_zipcode').val();
		if(split_val[7] == ''){
			var url = '<?php echo base_url();?>site/groupgift/getCountry';
			$.get(url,function(data){
				country = '<label for=""><?php if($this->lang->line('grougift_country') != '') { echo stripslashes($this->lang->line('grougift_country')); } else echo "Country"; ?></label><select id="get_gift_country" class="manually_input manually_select" name="country">'+data+'</select>';
				$("#append-country").html(country);
			});
		}else{
			var url = '<?php echo base_url();?>site/groupgift/getcurrentCountry';
			$.post(url,{'code':split_val[7]},function(data){
				country = '<label for=""><?php if($this->lang->line('grougift_country') != '') { echo stripslashes($this->lang->line('grougift_country')); } else echo "Country"; ?></label><select id="get_gift_country" class="manually_input manually_select" name="country">'+data+'</select>';
				$("#append-country").html(country);
			});
		}
		if(split_val[6] == ''){
			state = '<label for=""><?php if($this->lang->line('grougift_country') != '') { echo stripslashes($this->lang->line('grougift_state')); } else echo "State"; ?></label><input type="text" id="get_gift_text_state" class="manually_input" name="state" value=""/>';
		}else{
			state = '<label for=""><?php if($this->lang->line('grougift_country') != '') { echo stripslashes($this->lang->line('grougift_state')); } else echo "State"; ?></label><input type="text" id="get_gift_text_state" class="manually_input" name="state" value="'+split_val[6]+'"/>';
		}
		if(split_val[2] == ''){
			image = '<img width="190" height="190" src=\'<?php echo base_url()?>images/users/user-thumb1.png\'/>';
			user_image = '';
		}else{
			image = '<img width="190" height="190" src=\'<?php echo base_url()?>images/users/'+split_val[2]+'\'/>';
			user_image = split_val[2];
		}
		$("#user_image").val(user_image);
		$(".enter_manually_photos1").html(image);
		$("#append-state").html(state);
	});	   
});
jQuery(".show").live("click", function(e) { 
	var $clicked = $(e.target);
	if(!$clicked.hasClass("search-social-friends")){
		jQuery("#result").fadeOut(); 
	}
}); 
$("#btn-existuser").click(function(){
	if($('#get_gift_recipient').val() == ''){
	   alert("Please Enter Recipient Name or Email Address");
	   return false;
	}else if($('#get_gift_fullname').val() == ''){
	   alert("Please Enter Fullname");
	   return false;
	}else if($('#get_gift_address1').val() == ''){
	   alert("Please Enter Address");
	   return false;
	}else if($('#get_gift_text_state').val() == ''){
	   alert("Please Enter State");
	   return false;
	}else if($('#get_gift_city').val() == ''){
	   alert("Please Enter City");
	   return false;
	}else if($('#get_gift_zipcode').val() == ''){
	   alert("Please Enter Zipcode");
	   return false;
	}
	if($('#get_gift_country option:selected').val()!=''){
		code = $('#get_gift_country option:selected').val();
		$.ajax({url: vurl, type: 'POST', data:{'country':code,'pid':pid}, dataType: "JSON",
			success: function(data, textStatus, jqXHR){
				if(data.status==1 || data.status==2){
					$.ajax({url: formURL, type: 'POST', data:{data: $("#form2").serialize()}, dataType: "JSON",
						success: function(data, textStatus, jqXHR){
							var shipping_detail = data.shippingval;
							var val_split = shipping_detail.split('+');
							var product_shipping_cost = parseInt(val_split[0]) * parseInt(val_split[3]);
							var price = parseInt(val_split[5]);
							tax = parseInt(val_split[1])/100;
							shipping_tax = price * tax;
							item_price = price * parseInt(val_split[3]);
							goal_total = item_price + product_shipping_cost + shipping_tax;
							$("#item-cost").html("<?php echo $currencySymbol;?>"+price.toFixed(2)+"<small><?php echo $currencyType;?></small>");
							$("#item-qty").html("<small>"+val_split[3]+"</small>");
							$("#shipping-cost").html("<?php echo $currencySymbol;?>"+product_shipping_cost.toFixed(2)+"<small><?php echo $currencyType;?></small>");
							$("#shipping-tax").html("<?php echo $currencySymbol;?>"+shipping_tax.toFixed(2)+"<small><?php echo $currencyType;?></small>");
							$("#goal-total").html("<?php echo $currencySymbol;?>"+goal_total.toFixed(2)+"<small><?php echo $currencyType;?></small>");
							$("#goal-total-input").val(goal_total);
							$("#total-val").val(val_split[2]);
							$('#seller-id').val(val_split[4]);
							$('#gift_qty2').val(val_split[3]);
							popup2.open();
						} 	        
					});
				}else{
					alert("Not shipped to your country");
					return false;
				}
			} 	        
		});
	}else{
		alert("Please Select Country");
		return false;
	}
});
$('#btn-continue').click(function(){
	if($('#gift_recipient').val() == ''){
	   alert("Please Enter Recipient Name or Email Address");
	   return false;
	}else if($('#gift_fullname').val() == ''){
	   alert("Please Enter Fullname");
	   return false;
	}else if($('#gift_address1').val() == ''){
	   alert("Please Enter Address");
	   return false;
	}else if($('#gift_text_state').val() == ''){
	   alert("Please Enter State");
	   return false;
	}else if($('#gift_city').val() == ''){
	   alert("Please Enter City");
	   return false;
	}else if($('#gift_zipcode').val() == ''){
	   alert("Please Enter Zipcode");
	   return false;
	}
	if($('#gift_country option:selected').val()!=''){
		code = $('#gift_country option:selected').val();
		$.ajax({url: vurl, type: 'POST', data:{'country':code,'pid':pid}, dataType: "JSON",
			success: function(data, textStatus, jqXHR){
				if(data.status==1 || data.status==2){
					$.ajax({url: formURL, type: 'POST', data:{data: $("#form1").serialize()}, dataType: "JSON",
						success: function(data, textStatus, jqXHR){
							var shipping_detail = data.shippingval;
							var val_split = shipping_detail.split('+');
							var product_shipping_cost = parseInt(val_split[0]) * parseInt(val_split[3]);
							var price = parseInt(val_split[5]);
							tax = parseInt(val_split[1])/100;
							shipping_tax = price * tax;
							item_price = price * parseInt(val_split[3]);
							goal_total = item_price + product_shipping_cost + shipping_tax;
							$("#item-qty").html("<small>"+val_split[3]+"</small>");
							$("#item-cost").html("<?php echo $currencySymbol;?>"+price.toFixed(2)+"<small><?php echo $currencyType;?></small>");
							$("#shipping-cost").html("<?php echo $currencySymbol;?>"+product_shipping_cost.toFixed(2)+"<small><?php echo $currencyType;?></small>");
							$("#shipping-tax").html("<?php echo $currencySymbol;?>"+shipping_tax.toFixed(2)+"<small><?php echo $currencyType;?></small>");
							$("#goal-total").html("<?php echo $currencySymbol;?>"+goal_total.toFixed(2)+"<small><?php echo $currencyType;?></small>");
							$("#goal-total-input").val(goal_total);
							$("#total-val").val(val_split[2]);
							$('#seller-id').val(val_split[4]);
							$('#gift_qty2').val(val_split[3]);
							popup2.open();
						} 	        
					});
				}else{
					alert("Not shipped to your country");
					return false;
				}
			} 	        
		});
	}else{
		alert("Please Select Country");
		return false;
	}
});
$('#btn-create').click(function(){
	value = $('#seller-id').val();
	ship_value = $('#total-val').val();
	gifttotal_value = $('#goal-total-input').val();
	name = $('#gift_name').val();
	description = $('#gift_description').val();
	notes = $('#gift_notes').val();
	if(name == ''){
		alert("Please Enter Gift Name");
		return false;
	}else if(description == ''){
		alert("Please Enter Gift Description");
		return false;
	}else{
		$.ajax({url: formURL, type: 'POST', data:{data: $("#form4").serialize()}, dataType: "JSON",
			success: function(data, textStatus, jqXHR){
				var shipping_detail = data.shippingval;
				var val_split = shipping_detail.split('+');
				var product_shipping_cost = parseInt(val_split[7]) * parseInt(val_split[10]);
				var price = parseInt(val_split[11]);
				tax = parseInt(val_split[8])/100;
				shipping_tax = price * tax;
				item_price = price * parseInt(val_split[10]);
				goal_total = item_price + product_shipping_cost + shipping_tax;
				if(val_split[9]==''){
					$('#gift_receipt_img').attr("src", "images/users/user-thumb1.png");
				}else{
					$('#gift_receipt_img').attr("src", "images/users/"+val_split[9]);
				}
				$('#gift_names').text(val_split[4]);
				$('#gift_descriptions').text(val_split[5]);
				$('#recipient_name').text(val_split[0]);
				$('#recipient_address').text(val_split[1]+','+val_split[2]+','+val_split[3]);
				$('#seller_id').val(val_split[6]);
				$("#contb-item-cost").html("<?php echo $currencySymbol;?>"+price.toFixed(2)+"<small><?php echo $currencyType;?></small>");
				$("#contb-shipping-cost").html("<?php echo $currencySymbol;?>"+product_shipping_cost.toFixed(2)+"<small><?php echo $currencyType;?></small>");
				$("#contb-shipping-tax").html("<?php echo $currencySymbol;?>"+shipping_tax.toFixed(2)+"<small><?php echo $currencyType;?></small>");
				$("#contb-goal-total").html("<?php echo $currencySymbol;?>"+goal_total.toFixed(2)+"<small><?php echo $currencyType;?></small>");
				$('#sellers-ids').val(val_split[6]);
				popup3.open();
			} 	        
		});
	}
});
/************************END**********************************/
FB.init({
	appId:'<?php echo $this->config->item('facebook_app_id');?>',
	cookie:true,
	status:true,
	xfbml:true,
	oauth : true
});
$('.face_connect_links').click(function(){
	FB.login(function(response){
		if(response.authResponse){
			FB.api('/me/taggable_friends', {
				fields:'name,picture.width(50).height(50)',
				}, function(response){
				if(response.data) {
					$(".friends-lists dd ul").html('');
				    $.each(response.data,function(index,friend) {
					html = '<li id="'+friend.picture.data.url+'" onclick="friend_name(this.title,this.id);" title="'+friend.name+'"><a><div class="img-wrap"><img src="'+friend.picture.data.url+'"/><div>'+friend.name+'</div></div></a></li>';
					$(".friends-lists dd ul").append(html); 
					});
				}else{
					alert("Cannot able to fetch the friends list");
				}
			});
		}else{
			alert('User cancelled login or did not fully authorize.');
		}
	}, {scope:'user_friends'});
});
});
function friend_name(title,img){
	$('.connent_share_links').hide();
	$('.search-social-friends').hide();
	$('.friends-lists').hide();
	$('.enter_manually_box1').show();
	$('#get_gift_fullname').val(title);
	$('#get_gift_recipient').val(title);
	$('#fb_image').val(img);
	country = '<label for=""><?php if($this->lang->line('grougift_country') != '') { echo stripslashes($this->lang->line('grougift_country')); } else echo "Country"; ?></label><select id="get_gift_country" class="manually_input manually_select" name="country"></select>';
	state = '<label for=""><?php if($this->lang->line('grougift_country') != '') { echo stripslashes($this->lang->line('grougift_state')); } else echo "State"; ?></label><input type="text" id="get_gift_text_state" class="manually_input" name="state" value=""/>';
	if(img ==''){
	   images = '<img src="<?php echo base_url();?>images/users/default_user.jpg" style="width:150px; height:150px;"/>';
	}else{
	   images = '<img src="'+img+'"/>';
	}
	$('.enter_manually_photos1').html(images);
	$("#append-country").html(country);
	$("#append-state").html(state);
	var url = '<?php echo base_url();?>site/groupgift/getCountry';
	$.get(url,function(data){
		$("#get_gift_country").html(data);
	});
}
function pickfriend(){
	$('#pickafriend').show();
	$('.connent_share_links').show();
	$('.search-social-friends').show();
	$('#entermanually').hide();
	$('.enter_manually_box1').hide();
	$("#dep4").attr("class","depth4 current");
	$("#dep5").attr("class","depth5");
	$("#dep4 a").attr("class","current");
	$("#dep5 a").attr("class","");
}
function entermanually(){
	$('#pickafriend').hide();
	$('#entermanually').show();
	$('.enter_manually_photos img').hide();
	$("#dep5").attr("class","depth5 current");
	$("#dep4").attr("class","depth4");
	$("#dep5 a").attr("class","current");
	$("#dep4 a").attr("class","");	
}
function sug_fri_li(id){
	var img = $("#img-"+id).attr('src');
	name = $("#user-"+id).text();
	$("#gift_recipient").val(name);
	$("#gift_fullname").val(name);
	$("#img_prev").attr("src", img).width(119).height(115);
	$('#img_prev').css('display','block');
	$(".btn-upload").text('');
	$(".btn-upload span").hide();
	$("#recipient_image").attr('value',img);
	
}
function readURL(input){
	if(input.files && input.files[0]){
	var reader = new FileReader();
	reader.onload = function(e){
		$('#img_prev').attr('src',e.target.result).width(119).height(115);
		$('#img_prev').css('display','block');
		$(".btn-upload").text('');
	    $(".btn-upload span").hide();
	};
		reader.readAsDataURL(input.files[0]);
	}
}
</script>
<div class="popup group_gifts ly-title" style="display:none;">
	<p class="ltit"><?php if($this->lang->line('create_a_group_gift') != '') { echo stripslashes($this->lang->line('create_a_group_gift')); } else echo "Create a Group Gift"; ?></p>
	<div class="ltxt">
		<ol class="gift-group-create" style="position:relative;">
		   <li class="depth1 current" id="dep1"><a onclick="javascript:recipient();" class="current"><?php if($this->lang->line('group_gift_recipient') != '') { echo stripslashes($this->lang->line('group_gift_recipient')); } else echo "Choose Recipient"; ?></a><span></span></li>
		   <li class="depth2" id="dep2"><a onclick="javascript:personalize();"><?php if($this->lang->line('group_gift_personalize') != '') { echo stripslashes($this->lang->line('group_gift_personalize')); } else echo "Personalize"; ?></a><span></span></li>
		   <li class="depth3" id="dep3"><a onclick="javascript:contributions();"><?php if($this->lang->line('ask_for_contributions') != '') { echo stripslashes($this->lang->line('ask_for_contributions')); } else echo "Ask for Contributions"; ?></a></li>
		</ol>
		<div style="float:left; margin-top:25px; border-bottom:1px solid #ECEEF4; width:100%;">
		   <ol class="gift-group-tab" style="position:relative; border-bottom-style:none;">
		      <li style="background:none;" class="depth4 current" id="dep4"><a onclick="javascript:pickfriend();" class="current"><?php if($this->lang->line('pick_a_friend') != '') { echo stripslashes($this->lang->line('pick_a_friend')); } else echo "Pick a Friend"; ?></a></li>
		      <li style="background:none;" class="depth5" id="dep5"><a onclick="javascript:entermanually();"><?php if($this->lang->line('enter_manually') != '') { echo stripslashes($this->lang->line('enter_manually')); } else echo "Enter Manually"; ?></a></li>
		   </ol>
	  </div>
	<div class="pickafriend" id="pickafriend">
	      <input class="search-social-friends" id="searchid" type="text" placeholder="Search friends" style="width:100%;margin-top:5px;float:left;">
		  <div id="result"></div>
		  <ul class="connent_share_links">
      	     <li id="go_connect_links" class="">
        	     <a class="go_connect_links current">
            	    <span class="icon"></span>
                    <strong><?php if($this->lang->line('connect_to_google') != '') { echo stripslashes($this->lang->line('connect_to_google')); } else echo "Connect to Google+"; ?></strong>
                </a>
             </li>
             <li>
        	    <a class="face_connect_links" id="facebook">
            	   <span class="icon"></span>
                   <strong><?php if($this->lang->line('facebbok_friends') != '') { echo stripslashes($this->lang->line('facebbok_friends')); } else echo "Facebook friends"; ?></strong>
                </a>
            </li>
         </ul>
		<div class="friends-lists" id="friends-lists"><dd><ul></ul></dd></div>
		<form id="form2" style="padding:0px!important;">
			<div class="enter_manually_box1">
				<input type="hidden" class="hidden" name="user_image" id="user_image"/>
				<input type="hidden" name="product_id" value="<?php echo $productDetails->row()->seller_product_id;?>">
				<input type="hidden" name="long_url" value="<?php echo base_url();?>gifts/">
				<input type="hidden" name="seller_id" id ="seller_id" value=""/>
				<input type="hidden" name="gift_qty" id ="gift_qty" value=""/>
				<input type="hidden" name="gift_price" id ="gift_price" value=""/>
				<input type="hidden" name="fb_image" id="fb_image" value="">
				<ul>
					<li><label for=""><?php if($this->lang->line('grougift_recipient') != '') { echo stripslashes($this->lang->line('grougift_recipient')); } else echo "Recipient"; ?></label>
						<input type="text" value="" class="manually_input" name="recipient_name" id="get_gift_recipient" placeholder="Enter a username or an email address">
					</li>
					<li><label for=""><?php if($this->lang->line('grougift_fullname') != '') { echo stripslashes($this->lang->line('grougift_fullname')); } else echo "Full name"; ?></label>
						 <input type="text" value="" class="manually_input" name="recipient_fullname" id="get_gift_fullname" placeholder="">
					</li>
					<li><label for=""><?php if($this->lang->line('grougift_address1') != '') { echo stripslashes($this->lang->line('grougift_address1')); } else echo "Address1"; ?></label>
						<input type="text" value="" class="manually_input" name="address1" id ="get_gift_address1" placeholder="Address line 1">
					</li>
					<li><label for="">&nbsp;</label>
						<input type="text" value="" id="get_gift_address2" class="manually_input" name="address2" placeholder="Address line 2">
					</li>
					<li id="append-country"></li>
					<li id="append-state"></li>
					<li><label for=""><?php if($this->lang->line('grougift_city') != '') { echo stripslashes($this->lang->line('grougift_city')); } else echo "City"; ?></label>
						 <input type="text" value="" id="get_gift_city" name="city" class="manually_input manually_city" placeholder="e.g Newyork"><input type="text" value="" class="manually_input manually_zip" id="get_gift_zipcode" name="zip_code" placeholder="ZIP Code">
					</li>
					<li><label for=""><?php if($this->lang->line('grougift_telephone') != '') { echo stripslashes($this->lang->line('grougift_telephone')); } else echo "Telephone"; ?></label>
						 <input type="text" id="get_gift_phone" value="" class="manually_input" name="telephone" placeholder="10 digits, no dashes">
					</li>
				</ul>
				<div class="enter_manually_photos1"></div><br>
				<div style="float:left;"><button class="change_friend"><?php if($this->lang->line('change_friend') != '') { echo stripslashes($this->lang->line('change_friend')); } else echo "Change Friend"; ?></button></div>
			</div>
			<div class="btn-area">
				<div class="howit_works"><i></i><a href="<?php echo base_url();?>pages/group-gift"><?php if($this->lang->line('how_it_works') != '') { echo stripslashes($this->lang->line('how_it_works')); } else echo "How it works"; ?></a></div>
				<button class="btn-back" id="btn-cancel" type="button"><?php if($this->lang->line('header_cancel') != '') { echo stripslashes($this->lang->line('header_cancel')); } else echo "Cancel"; ?></button>
				<button class="btn-done" id= "btn-existuser" type="button"><?php if($this->lang->line('continue') != '') { echo stripslashes($this->lang->line('continue')); } else echo "Continue"; ?></button>
			</div>
		</form>
	</div>
	<form id="form1" style="padding:0px!important;">
		<div class="entermanually" id ="entermanually" style="float:left; width:100%">
	       <div class="enter_manually">
		       <div class="enter_manually_box">
			        <input type="hidden" name="product_id" value="<?php echo $productDetails->row()->seller_product_id;?>">
					<input type="hidden" name="long_url" value="<?php echo base_url();?>gifts/">
					<input type="hidden" name="seller_id" id ="sellers-id" value=""/>
					<input type="hidden" name="gift_qty" id ="gift_qty1" value=""/>
					<input type="hidden" name="gift_price" id ="gift_price1" value=""/>
               		<ul>
                        <li><label for=""><?php if($this->lang->line('grougift_recipient') != '') { echo stripslashes($this->lang->line('grougift_recipient')); } else echo "Recipient"; ?></label>
                        	<input type="text" value="" class="manually_input" name="recipient_name" id="gift_recipient" placeholder="Enter a username or an email address">
                        </li>
                        <li><label for=""><?php if($this->lang->line('grougift_fullname') != '') { echo stripslashes($this->lang->line('grougift_fullname')); } else echo "Full name"; ?></label>
                        	 <input type="text" value="" class="manually_input" name="recipient_fullname" id="gift_fullname" placeholder="">
                        </li>
						<li><label for=""><?php if($this->lang->line('grougift_address1') != '') { echo stripslashes($this->lang->line('grougift_address1')); } else echo "Address1"; ?></label>
                        	<input type="text" value="" class="manually_input" name="address1" id ="gift_address1" placeholder="Address line 1">
                        </li>
                        <li><label for="">&nbsp;</label>
                        	<input type="text" value="" class="manually_input" name="address2" placeholder="Address line 2">
                        </li>
                        <li><label for=""><?php if($this->lang->line('grougift_country') != '') { echo stripslashes($this->lang->line('grougift_country')); } else echo "Country"; ?></label>
							<select id="gift_country" class="manually_input manually_select" name="country"></select>
                        </li>
                        <li><label for=""><?php if($this->lang->line('grougift_state') != '') { echo stripslashes($this->lang->line('grougift_state')); } else echo "State"; ?></label>
                             <input type="text" id="gift_text_state" class="manually_input" name="state" value=""/>
                        </li>
						<li><label for=""><?php if($this->lang->line('grougift_city') != '') { echo stripslashes($this->lang->line('grougift_city')); } else echo "City"; ?></label>
                        	 <input type="text" value="" id="gift_city" name="city" class="manually_input manually_city" placeholder="e.g Newyork"><input type="text" value="" class="manually_input manually_zip" id="gift_zipcode" name="zip_code" placeholder="ZIP Code">
                        </li>
						<li><label for=""><?php if($this->lang->line('grougift_telephone') != '') { echo stripslashes($this->lang->line('grougift_telephone')); } else echo "Telephone"; ?></label>
							<input type="text" value="" class="manually_input" name="telephone" placeholder="10 digits, no dashes">
                        </li>
                    </ul>
                    <div class="enter_manually_photos">
                    	<input type="file" class="hidden" name="recipient_image" id="recipient_image" onchange="readURL(this);"/>
						<img id="img_prev" src="#" alt="your image" style="margin-top:-120px;"/>
						<button class="btn-upload" type="button"><span class="icon"></span><?php if($this->lang->line('grougift_upload') != '') { echo stripslashes($this->lang->line('grougift_upload')); } else echo "Upload Photo"; ?></button>
                    </div>
			   </div> 
		   </div>
		   <div class="suggested_box">
				 <h4><?php if($this->lang->line('sugessted_friends') != '') { echo stripslashes($this->lang->line('sugessted_friends')); } else echo "Suggested friends"; ?></h4>
				 <div id="suggested_friends"><ul></ul></div>
		   </div>
	    <div class="btn-area">
			<div class="howit_works"><i></i><a href="<?php echo base_url();?>pages/group-gift"><?php if($this->lang->line('how_it_works') != '') { echo stripslashes($this->lang->line('how_it_works')); } else echo "How it works"; ?></a></div>
				<button class="btn-back" id="btn-cancel" type="button"><?php if($this->lang->line('header_cancel') != '') { echo stripslashes($this->lang->line('header_cancel')); } else echo "Cancel"; ?></button>
			<button class="btn-done" id= "btn-continue" type="button"><?php if($this->lang->line('continue') != '') { echo stripslashes($this->lang->line('continue')); } else echo "Continue"; ?></button>
	</div></div>
	  </form>
	</div>
	
	<button title="Close" class="ly-close"><i class="ic-del-black"></i></button>
</div>
<script type="text/javascript">
	 var clientId = '<?php echo $this->config->item('google_client_id')?>';
      var apiKey = '<?php echo $this->config->item('google_developer_key')?>';
      var scopes = 'https://www.googleapis.com/auth/plus.login';
      function handleClientLoad() {
        gapi.client.setApiKey(apiKey);
        window.setTimeout(checkAuth,1);
      }
      function checkAuth() {
        gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: true}, handleAuthResult);
      }
      function handleAuthResult(authResult) {
        var authorizeButton = document.getElementById('go_connect_links');
        if (authResult && !authResult.error) {
          authorizeButton.className = "mystyle";
          makeApiCall();
		  authorizeButton.className = "";
        } else {
          authorizeButton.className = "";
          authorizeButton.onclick = handleAuthClick;
        }
      }

      function handleAuthClick(event) {
        gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: false}, handleAuthResult);
        return false;
      }
      function makeApiCall() {
        gapi.client.load('plus', 'v1', function() {
			var request = gapi.client.plus.people.list({
				'userId': 'me','collection':'visible'
			});
			request.execute(function(resp) {
			  var numItems = resp.items.length;
			  for (var i = 0; i < numItems; i++) {
					html = '<li id="'+resp.items[i].image.url+'" onclick="friend_name(this.title,this.id);" title="'+resp.items[i].displayName+'"><a><div class="img-wrap"><img src="'+resp.items[i].image.url+'"/><div>'+resp.items[i].displayName+'</div></div></a></li>';
					$(".friends-lists dd ul").append(html); 
				}
			});
        });
      }  
</script>

<script src="https://apis.google.com/js/client.js?onclick=handleClientLoad"></script> 
<?php } ?>