<?php 
   if(($this->uri->segment(1) == 'gifts') && ($this->uri->segment(2) != 'contribute')){
?>
<?php if($gift_ProductDetails->num_rows() ==1){
         $img = 'dummyProductImage.jpg';
		$imgArr = explode(',', $gift_ProductDetails->row()->image);
		if (count($imgArr)>0){
			foreach ($imgArr as $imgRow){
				if ($imgRow != ''){
					$img = $pimg = $imgRow;
					break;
				}
			}
		}
 ?>
<div class="popup edit_group_gifts ly-title" style="display:none;">
    <p class="ltit"><?php if($this->lang->line('edit_group_gift') != '') { echo stripslashes($this->lang->line('edit_group_gift')); } else echo "Edit Group Gift"; ?></p>
	 <form  action="" method="post" id="form3" enctype="multipart/form-data" style="padding:0px!important;">
	<div class="ltxt">
	    <div class="edit-product-detail">
		    <div class="iedit-product-detail">
			    <div class="left">
				    <img src="<?php echo base_url();?>images/product/<?php echo $img;?>" alt="<?php echo $gift_ProductDetails->row()->product_name;?>" height="135" width="130">
				</div>
				<div class="right">
				    <figcaption><?php echo $gift_ProductDetails->row()->product_name;?></figcaption>
					<span class="price"><?php echo $currencySymbol;?><b><?php echo $gift_ProductDetails->row()->gift_total;?></b></span>
					<div class="description" style="height:50px;overflow:hidden">
					    <?php echo $gift_ProductDetails->row()->description;?>
					</div>
				</div>
			</div>
		</div>
		<fieldset class="gift-summary">
		     <p><label class="label"><?php if($this->lang->line('header_title') != '') { echo stripslashes($this->lang->line('header_title')); } else echo "Title"; ?></label><input class="gift_input" type="text" value="<?php echo $gift_ProductDetails->row()->gift_name;?>" name="gift_name"></p>
			 
			 <p><label class="label"><?php if($this->lang->line('groupgift_description') != '') { echo stripslashes($this->lang->line('groupgift_description')); } else echo "Description"; ?></label><textarea class="gift_input description" name="gift_description"><?php echo $gift_ProductDetails->row()->gift_description;?></textarea></p>
			 
			 <p><label class="label"><?php if($this->lang->line('groupgift_note') != '') { echo stripslashes($this->lang->line('groupgift_note')); } else echo "Note"; ?></label><textarea class="gift_input" placeholder="Optional note to merchant" name="gift_notes"><?php echo $gift_ProductDetails->row()->gift_notes;?></textarea></p>
			 
		</fieldset>
		<fieldset class="receipent">
		      <p><label class="label"><?php if($this->lang->line('grougift_recipient') != '') { echo stripslashes($this->lang->line('grougift_recipient')); } else echo "Recipient"; ?></label><input class="gift_input" type="text" placeholder="Enter a username or an email address" value="<?php echo $gift_ProductDetails->row()->recipient_name;?>" name="recipient_name"></p>
			  
			  <p><label class="label"><?php if($this->lang->line('signup_full_name') != '') { echo stripslashes($this->lang->line('signup_full_name')); } else echo "Full Name"; ?></label><input class="gift_input" type="text" value="<?php echo $gift_ProductDetails->row()->recipient_fullname;?>" name="recipient_fullname"></p>
			  
			  <p><label class="label"><?php if($this->lang->line('shipping_address_comm') != '') { echo stripslashes($this->lang->line('shipping_address_comm')); } else echo "Address"; ?></label><input class="gift_input" type="text" placeholder="Address Line 1" value="<?php echo $gift_ProductDetails->row()->address1;?>" name="address1">
			  
              	<label class="label">&nbsp;</label><input class="gift_input" type="text" placeholder="Address Line 2" value="<?php echo $gift_ProductDetails->row()->address2;?>" name="address2"></p>
				
			  <p class="hastext"><label class="label"><?php if($this->lang->line('header_country') != '') { echo stripslashes($this->lang->line('header_country')); } else echo "Country"; ?></label><input class="gift_input" type="text" placeholder="Country" value="<?php echo $gift_ProductDetails->row()->country;?>" name="country">
			       <label class="label">&nbsp;</label><input class="gift_input width90" type="text" placeholder="City" value="<?php echo $gift_ProductDetails->row()->city;?>" name="city">
				   <input class="gift_input width90" type="text" placeholder="State" value="<?php echo $gift_ProductDetails->row()->state;?>" name="state">
				   <input class="gift_input width90" type="text" placeholder="Zip Code" value="<?php echo $gift_ProductDetails->row()->zip_code;?>" name="zip_code">
			  </p>
			  
			  <p><label class="label"><?php if($this->lang->line('header_telephone') != '') { echo stripslashes($this->lang->line('header_telephone')); } else echo "Telephone"; ?></label><input class="gift_input" type="text" value="<?php echo $gift_ProductDetails->row()->telephone;?>" name="telephone"></p>

			  <input id="sellers-ids" name ="seller_id" type="hidden" value="<?php echo $gift_ProductDetails->row()->gift_seller_id;?>">
		</fieldset>
		 <div class="btn-area">
		      <!--button class="popupcancel_btn" id="btn-cancel-campaign" type="button"><?php if($this->lang->line('cancel_campaign') != '') { echo stripslashes($this->lang->line('cancel_campaign')); } else echo "Cancel Campaign"; ?></button>-->
		      <button class="btn-done" id= "btn-save" type="button"><?php if($this->lang->line('continue') != '') { echo stripslashes($this->lang->line('continue')); } else echo "Continue"; ?></button>
	      </div>
	</div>
	</form>
	<button title="Close" class="ly-close"><i class="ic-del-black"></i></button>
</div>
<?php } } ?>
<script>
$(document).ready(function(){
    var popup4 = $.dialog('edit_group_gifts');
	$('a[href="#edit_group_gift"]').click(function(event){
	    popup4.open();
		event.preventDefault();
	});
	$('#btn-save').click(function(event){
	    url = '<?php echo base_url();?>/site/groupgift/updateGrouptProduct';
		$.post(url,$("#form3").serialize(),function(data){
		   window.location.href = baseURL+'gifts/'+$('#sellers-ids').val();
		});
	});
	$('#btn-cancel-campaign').click(function(event){
	    cancel = $(this).attr('id');
	    url = '<?php echo base_url();?>/site/groupgift/updateGrouptProduct';
		id = $("#sellers-ids").val();
		$.post(url,{'cancelled':cancel,'group_id':id},function(data){
		   window.location.href = baseURL+'gifts/'+$('#sellers-ids').val();
		});
	});
});
</script>