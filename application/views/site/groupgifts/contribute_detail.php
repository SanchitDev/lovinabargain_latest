<?php $this->load->view('site/templates/header'); 
$paypal_settings = unserialize($this->config->item('payment_3'));
$paypalProcess = unserialize($paypal_settings['settings']);
?>
<link rel="stylesheet" media="all" type="text/css" href="css/site/<?php echo SITE_COMMON_DEFINE ?>groupgift.css">
<div class="lang-en wider no-subnav thing signed-out winOS">

<?php if($gift_ProductDetails->num_rows() ==1){
/* echo "<pre>";
print_r($gift_ProductDetails->result());
die; */
?>
<?php
	$img = 'dummyProductImage.jpg';
	$imgArr = explode(',', $gift_ProductDetails->row()->image);
	if (count($imgArr)>0){
		foreach ($imgArr as $imgRow){
			if ($imgRow != ''){
				$img = $pimg = $imgRow;
				break;
			}
		}
	}
	$user_img = 'user-thumb1.png';
	$user_imgArr = explode(',', $gift_ProductDetails->row()->thumbnail);
	if(count($user_imgArr)>0){
		foreach ($user_imgArr as $user_imgRow){
			if ($user_imgRow != ''){
				$user_img = $user_pimg = $user_imgRow;
				break;
			}
		}
	}
	$recipient_img = 'user-thumb1.png';
	$recp_imgArr = explode(',', $gift_ProductDetails->row()->recipient_image);
	if (count($recp_imgArr)>0){
		foreach ($recp_imgArr as $recp_imgRow){
			if ($recp_imgRow != ''){
				$recipient_img = $recp_pimg = $recp_imgRow;
				break;
			}
		}
	}
?>
  <form name="PaymentPaypalForm" id="PaymentPaypalForm" method="post" enctype="multipart/form-data" action="site/checkout/contributor_payment"  autocomplete="off">
	<div id="container-wrapper">
		<div class="container">
			<div class="wrapper-content right-sidebar" style="background:none; box-shadow:none;">
			<?php if($flash_data != '') { ?>
					<div class="errorContainer" id="<?php echo $flash_data_type;?>">
						<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
						<p><span><?php echo $flash_data;?></span></p>
					</div>
			<?php } ?>
				<div id="content" style="padding:0px; background:none;width: 680px;">
					<div class="detail_leftbar4" style="width: 670px;">
						<h3 class="detail_link_list"><?php if($this->lang->line('gift_contribution') != '') { echo stripslashes($this->lang->line('gift_contribution')); } else echo "Gift Contribution"; ?></h3>
						<div class="contribution_box">
							<h4><?php if($this->lang->line('my_contribution') != '') { echo stripslashes($this->lang->line('my_contribution')); } else echo "My contribution"; ?></h4>
							<p><?php if($this->lang->line('contributor_amount_text') != '') { echo stripslashes($this->lang->line('contributor_amount_text')); } else echo "You are free to enter any amount from"; ?><b>$<?php echo $this->config->item('default_amount');?></b><?php if($this->lang->line('contributor_amount_text1') != '') { echo stripslashes($this->lang->line('contributor_amount_text1')); } else echo "up to the maximum of the group gift. Please note that you wont be charged if the campaign is unsuccessful."; ?></p> 
							<ul class="contribution_amount">
								<li class= "list1" id="5.00">$5</li>
								<li class= "list2" id="15.00">$15</li>
								<li class= "list3" id="25.00">$25</li>
								<li class= "list4" id="35.00">$35</li>
								<li class= "list5" id="50.00">$50</li>
							</ul>
							<a class="contribution_otheramount"><?php if($this->lang->line('other_amount') != '') { echo stripslashes($this->lang->line('other_amount')); } else echo "other amount"; ?></a>
							<?php if($contribute_details >0){
									$contribute_amount = 0;
									foreach($contribute_details->result() as $_contributedetails){
										$contribute_amount += $_contributedetails->amount;
											if($contribute_amount!=''){
												$amount = $contribute_amount; 
											}else{
												$amount = "0";
											}
									}							  
								}else{}					
							    $total_value= $gift_ProductDetails->row()->gift_total;
								$contribute_value = round($amount, 2);
								$remain_value = $total_value - $contribute_value;
							?>
							<div class="total_contribution"><input type="text" name="total_price" value="" id="amount" class="total_scroll" onkeyup="getamount(this.value,'<?php echo $remain_value;?>');"> <small>USD</small></div>
							<div class="contribution_maximum"><?php if($this->lang->line('maximum_amount_contribute') != '') { echo stripslashes($this->lang->line('maximum_amount_contribute')); } else echo "Maximum amount to contribute:"; ?> <strong>$<?php echo $gift_ProductDetails->row()->gift_total;?> USD</strong></div>
						</div>
						<div class="contribution_msg">
							<div class="contribution_msg_box">
								<h5><?php if($this->lang->line('giftcard_person_msg') != '') { echo stripslashes($this->lang->line('giftcard_person_msg')); } else echo "Personal message"; ?></h5>
								<textarea class="contribution_textbox"><?php if($this->lang->line('write_something_nice_to') != '') { echo stripslashes($this->lang->line('write_something_nice_to')); } else echo "Write something nice to"; ?> <?php echo $gift_ProductDetails->row()->user_name;?>.</textarea>	
							</div>
						</div>
					</div>
					<div class="detail_leftbar4" style="width: 670px;">
						<h3 class="detail_link_list"><?php if($this->lang->line('cart_pay_mthd') != '') { echo stripslashes($this->lang->line('cart_pay_mthd')); } else echo "Payment Method"; ?></h3>
						<div class="contribution_payment">
							<dl class="left-contribute">
								<dt><b>Billing Address</b><small>Enter your billing address</small></dt>
								<dd>
									<input type="hidden" name="groupgift_id" value="<?php echo $gift_ProductDetails->row()->gift_seller_id;?>"/>
  									<input type="hidden" name="paypalEmail" id="paypalEmail" value="<?php echo $paypalProcess['merchant_email']; ?>" />
									<input type="hidden" name="sellerEmail" id="sellerEmail" value="<?php echo $sellerDetails->row()->paypal_email; ?>"  />
									<label for="payment-personal-name-fst"><?php if($this->lang->line('header_name') != '') { echo stripslashes($this->lang->line('header_name')); } else echo "Name"; ?><b>*</b></label>
									<input id="full_name" class="required" type="text" value="<?php echo $userDetails->row()->full_name; ?>" name="full_name">
								</dd>
								<dd class="hastext">
									<label for="payment-adds-1"><?php if($this->lang->line('shipping_address_comm') != '') { echo stripslashes($this->lang->line('shipping_address_comm')); } else echo "Address"; ?><b>*</b></label>
									<input id="address" class="required valid" type="text" value="<?php echo $userDetails->row()->address; ?>" name="address">
								</dd>
								<dd class="hastext">
									<label for="payment-adds-1"><?php if($this->lang->line('shipping_address_comm') != '') { echo stripslashes($this->lang->line('shipping_address_comm')); } else echo "Address"; ?> 2</label>
									<input id="address2" class="sffocus" type="text" value="<?php echo $userDetails->row()->address2; ?>" name="address2">
								</dd>
								<dd class="hastext">
									<label for="payment-city"><?php if($this->lang->line('header_city') != '') { echo stripslashes($this->lang->line('header_city')); } else echo "City"; ?><b>*</b></label>
									<input id="city" class="required valid" type="text" value="<?php echo $userDetails->row()->city; ?>" name="city">
								</dd>
							</dl>
							<dl class="right-contribute">
								<dt><b></b><small></small></dt>
								<dd>
									<label for="payment-state"><?php if($this->lang->line('checkout_state') != '') { echo stripslashes($this->lang->line('checkout_state')); } else echo "State"; ?><b>*</b></label>
									<input id="state" class="required" type="text" value="<?php echo $userDetails->row()->state; ?>" name="state">
								</dd>
								<dd>
									<label for="payment-state"><?php if($this->lang->line('header_country') != '') { echo stripslashes($this->lang->line('header_country')); } else echo "Country"; ?><b>*</b></label>
									<select id="country" name="country" class="select-round select-white select-country selectBox required">
										<option value="">-------------------- <?php if($this->lang->line('checkout_select') != '') { echo stripslashes($this->lang->line('checkout_select')); } else echo "SELECT"; ?> --------------------</option>											
										<?php foreach($country_list->result() as $cntyRow){ ?>	
										<option value="<?php echo $cntyRow->country_code; ?>" <?php if($cntyRow->country_code == $userDetails->row()->country){ echo 'selected="selected"';} ?> ><?php echo $cntyRow->name; ?></option>
										<?php } ?>    
									</select>
								</dd>
								<dd>
									<label for="payment-zipcode"><?php if($this->lang->line('checkout_zip_code') != '') { echo stripslashes($this->lang->line('checkout_zip_code')); } else echo "Zip Code"; ?><b>*</b></label>
									<input id="postal_code" class="required ValidZipCode" type="text" value="<?php echo $userDetails->row()->postal_code; ?>" name="postal_code">
								</dd>
								<dd>
									<label for="payment-phone"><?php if($this->lang->line('checkout_phone_no') != '') { echo stripslashes($this->lang->line('checkout_phone_no')); } else echo "Phone No"; ?><b>*</b></label>
									<input id="phone_no" class="required number" type="text" value="<?php echo $userDetails->row()->phone_no; ?>" name="phone_no">
								</dd>
							</dl>
						</div>
						<div class="contribution_btn_area">
							<input type="submit" value="Submit Contribution" style="padding:9px 12px 9px 15px; float:left; cursor:pointer;" class="contribute_btn" />
							<div class="security">
								<i class="ic-security"></i>
								<?php if($this->lang->line('secure_transaction') != '') { echo stripslashes($this->lang->line('secure_transaction')); } else echo "Secure Transaction"; ?>
							</div>
						</div>
					</div>
				</div>
			<aside style="padding:0px; width: 255px;" id="sidebar">
				<section class="thing-section gift-section">
					<div class="detail_sidebar_list">
						<h3 class="detail_link_list"><?php if($this->lang->line('contribute_to') != '') { echo stripslashes($this->lang->line('contribute_to')); } else echo "Contribute to"; ?></h3>
						<div class="contribute_img"><img alt="" src="images/product/<?php echo $img;?>" /></div>
						<div class="contribute_summary">
							<h5><a href="gifts/<?php echo $gift_ProductDetails->row()->gift_seller_id;?>"><?php echo $gift_ProductDetails->row()->gift_name;?></a>
							<small><?php if($this->lang->line('header_small_for') != '') { echo stripslashes($this->lang->line('header_small_for')); } else echo "for"; ?> <?php echo $gift_ProductDetails->row()->user_name;?></small></h5>
							<p class="contribute_message"><?php echo $gift_ProductDetails->row()->gift_description;?></p>
						</div>
						<div class="statistics">
							<ul>
							<li class="time"><span><b>�<?php echo round($amount, 2);?></b> <?php if($this->lang->line('contributed') != '') { echo stripslashes($this->lang->line('contributed')); } else echo "Contributed"; ?></span></li>
							<li class="people"><span>
								<?php if($expiry_date->num_rows() ==1) {
										$start_date = $gift_ProductDetails->row()->created;
										$start_date = strtotime($start_date);
										$current_date = strtotime(date('Y-m-d'));
										$end_date = strtotime("+".$this->config->item('expiry_days')." day", $start_date);
										$timeleft = $end_date-$current_date;
										//$daysleft = round((($timeleft/24)/60)/60); 
										$temp=$timeleft/86400;
										$daysleft = floor($temp);
								?>		
								<b><?php echo $daysleft;}?></b> <?php if($this->lang->line('days_left') != '') { echo stripslashes($this->lang->line('days_left')); } else echo "Days left"; ?></span>
							</li>
							</ul>
						</div>
						<div class="contribute_prize_zone">
							<div class="progress-bar">
					  <span class="fst"><?php if($this->lang->line('gift_goal') != '') { echo stripslashes($this->lang->line('gift_goal')); } else echo "Gift Goal"; ?></span>
					  <span class="lst">$<?php echo $gift_ProductDetails->row()->gift_total;?></span>
					<?php 
					function progressBar($percentage){
						print "<div id=\"progress-bar\" class=\"all-rounded\">\n";
						print "<div id=\"progress-bar-percentage\" class=\"all-rounded\" style=\"width: $percentage%\">";
								if ($percentage > 0) {/* print "$percentage%"; */} else {print "<div class=\"spacer\">&nbsp;</div>";}
						print "</div></div>";
			       }
					$val1 = $gift_ProductDetails->row()->gift_total;
					$val2 = round($amount, 2);
					$res = (($val1 - $val2)/($val1))*100;
					$res1 = 100 - $res;
					$res1 = round($res1, 2); 
					progressBar($res1);
					?>
					</div>
						</div>
					</div>
				</section>
				<!-- / thing-section -->
				<hr>
			 </aside>
		  </div>
	  </div>
   </div>
   </form>
<script type="text/javascript">
$(document).ready(function(){
	$(".total_contribution").hide();
	$(".contribution_amount li:first-child").addClass("on_select");
	var amount = $(".contribution_amount li:first-child").attr('id');
	$("#amount").val(amount);
	$(".contribution_otheramount").click(function(){
		$(".contribution_otheramount").hide();
		$(".total_contribution").show();
	});
	$(".contribution_amount li").click(function(){
            $(".contribution_amount li").removeClass('on_select');
            $(this).addClass('on_select');
			var amounts = $(this).attr('id');
				$("#amount").val(amounts);
	});
	$("#PaymentPaypalForm").validate();
	$.validator.addMethod("ValidZipCode", function( value, element ){
		var result = this.optional(element) || value.length >= 3;
		if(!result){
			return false;
		}else{
			return true;
		}
	}, "Please Enter the Correct ZipCode");
    var total_value = '<?php echo $gift_ProductDetails->row()->gift_total;?>';
	contributor_value = '<?php echo round($amount, 2);?>';
	remain_value = parseFloat(total_value) - parseFloat(contributor_value);
	if($('.list1').attr('id') > remain_value){
		$('.contribution_amount li').hide();
	}else if($('.list2').attr('id') > remain_value){
		$('.list1').show(); $('.list2').hide();$('.list3').hide(); $('.list4').hide();$('.list5').hide();
	}else if($('.list3').attr('id') > remain_value){
		$('.list1').show(); $('.list2').show();$('.list3').hide(); $('.list4').hide();$('.list5').hide();
	}else if($('.list4').attr('id') > remain_value){
		$('.list1').show(); $('.list2').show();$('.list3').show(); $('.list4').hide();$('.list5').hide();
		
	}else if($('.list5').attr('id') > remain_value){
	   $('.list1').show(); $('.list2').show();$('.list3').show(); $('.list4').show();$('.list5').hide();
	}else{
	  $('.contribution_amount li').show();
	}
});
function getamount(value,total){
		if(parseFloat(value) > parseFloat(total)){
			$("#amount").val(total);
		}
	}
</script>
<style>
#progress-bar {width:225px; margin:10px 0px 0px 0px; background:#cccccc; border:3px solid #f2f2f2; height:5px;}
#progress-bar-percentage {background:#3063A5; padding:5px 0px 0px 0px; color:#FFF; font-weight:bold;
   text-align: center; }
</style>
<?php }?>
</div>
<script type="text/javascript" src="js/site/jquery.validate.js"></script>
<script type="text/javascript" src="js/site/<?php echo SITE_COMMON_DEFINE ?>selectbox.js"></script>
<script type="text/javascript" src="js/site/<?php echo SITE_COMMON_DEFINE ?>shoplist.js"></script>
<script type="text/javascript" src="js/site/<?php echo SITE_COMMON_DEFINE ?>address_helper.js"></script>
<?php
$this->load->view('site/templates/footer');
?>