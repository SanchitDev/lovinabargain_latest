<?php 
$this->load->view('site/templates/header.php');
?>
<link rel="stylesheet" media="all" type="text/css" href="css/site/<?php echo SITE_COMMON_DEFINE ?>groupgift.css">
<link rel="stylesheet" media="all" type="text/css" href="css/site/<?php echo SITE_COMMON_DEFINE ?>setting.css">
<script type="text/javascript">
	var can_show_signin_overlay = false;
	if (navigator.platform.indexOf('Win') != -1) {document.write("<style>::-webkit-scrollbar, ::-webkit-scrollbar-thumb {width:7px;height:7px;border-radius:4px;}::-webkit-scrollbar, ::-webkit-scrollbar-track-piece {background:transparent;}::-webkit-scrollbar-thumb {background:rgba(255,255,255,0.3);}:not(body)::-webkit-scrollbar-thumb {background:rgba(0,0,0,0.3);}::-webkit-scrollbar-button {display: none;}</style>");}
</script>
<div class="lang-en no-subnav wider winOS">     
	<div id="container-wrapper">
	<div class="container">
	<?php if($flash_data != '') { ?>
	<div class="errorContainer" id="<?php echo $flash_data_type;?>">
		<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
		<p><span><?php echo $flash_data;?></span></p>
	</div>
	<?php } ?>
	<div class="wrapper-content order" >
		<div id="content" style="padding:0px 20px 20px 20px;">
			<div class="cart-list chept2">
				<h2><?php if($this->lang->line('contributor_confirmation') != '') { echo stripslashes($this->lang->line('contributor_confirmation')); } else echo "Contributor Confirmation"; ?></h2>       
				<div class="cart-payment-wrap card-payment new-card-payment">
					<strong><?php if($this->lang->line('order_tran_failure') != '') { echo stripslashes($this->lang->line('order_tran_failure')); } else echo "Your Transaction Failure"; ?></strong>
					<div class="payment_success"><b><?php echo urldecode($errors); ?></b></div>
					<div class="payment_success"><img src="images/site/failure_payment.png" /></div>
				</div>
				<?php $this->output->set_header('refresh:5;url='.base_url().'gifts/contribute/1396007108'); ?>
			</div>
		</div>
	</div>
<?php 
     $this->load->view('site/templates/footer_menu');
     ?>
<script type="text/javascript" src="js/site/jquery.validate.js"></script>
<script type="text/javascript" src="js/site/<?php echo SITE_COMMON_DEFINE ?>selectbox.js"></script>
<script type="text/javascript" src="js/site/<?php echo SITE_COMMON_DEFINE ?>shoplist.js"></script>
<script type="text/javascript" src="js/site/<?php echo SITE_COMMON_DEFINE ?>address_helper.js"></script>
</body>
</html>