<?php 
   if($this->uri->segment(1) == 'things'){
?>
<div class="popup group_giftsb ly-title" style="display:none;">
	<p class="ltit"><?php if($this->lang->line('create_a_group_gift') != '') { echo stripslashes($this->lang->line('create_a_group_gift')); } else echo "Create a Group Gift"; ?></p>
	<div class="ltxt">
		<ol class="gift-group-contributions" style="position:relative;">
		   <li class="depth1" id="dep1"><a onclick="javascript:recipient();" class="current"><?php if($this->lang->line('group_gift_recipient') != '') { echo stripslashes($this->lang->line('group_gift_recipient')); } else echo "Choose Recipient"; ?></a><span></span></li>
		   <li class="depth2" id="dep2"><a onclick="javascript:personalize();"><?php if($this->lang->line('group_gift_personalize') != '') { echo stripslashes($this->lang->line('group_gift_personalize')); } else echo "Personalize"; ?></a><span></span></li>
		   <li class="depth3 current" id="dep3"><a onclick="javascript:contributions();"><?php if($this->lang->line('ask_for_contributions') != '') { echo stripslashes($this->lang->line('ask_for_contributions')); } else echo "Ask for Contributions"; ?></a></li>
		</ol>
        <?php if ($productDetails->num_rows()==1){
		      $img = 'dummyProductImage.jpg';
		         $imgArr = explode(',', $productDetails->row()->image);
		             if (count($imgArr)>0){
			             foreach ($imgArr as $imgRow){
				            if ($imgRow != ''){
					           $img = $pimg = $imgRow;
					           break;
				            }
			            }
		            }
			?>
        <div class="contributions_title">
        
        	<h2><?php if($this->lang->line('ask_your_friends_contributors') != '') { echo stripslashes($this->lang->line('ask_your_friends_contributors')); } else echo "Ask your friends to contribute"; ?></h2>
        
        	<ul class="contributions_share">
            
            	<li><a class="contributions_go" target="_blank" href="http://plus.google.com/share?url=<?php echo base_url().'things/'.$productDetails->row()->id.'/'.$productDetails->row()->seourl.'?ref='.$productDetails->row()->user_name;?>"`><span class="icon"></span><?php if($this->lang->line('ask_for_contributions') != '') { echo stripslashes($this->lang->line('ask_for_contributions')); } else echo "Ask for contributions"; ?></a></li>
                
                <li><a class="contributions_fa" target="_blank" href="http://www.facebook.com/sharer.php?u=<?php echo base_url().'things/'.$productDetails->row()->id.'/'.$productDetails->row()->seourl.'?ref='.$productDetails->row()->user_name;?>"><span class="icon"></span></a></li>
                
                <li><a class="contributions_tw" target="_blank" href="http://twitter.com/share?text=&<?php echo $productDetails->row()->product_name;?>url=<?php echo base_url().'things/'.$productDetails->row()->id.'/'.$productDetails->row()->seourl.'?ref='.$productDetails->row()->user_name;?>"><span class="icon"></span></a></li>
                
                <li><a class="contributions_bk" target="_blank" href="http://vkontakte.ru/share.php?url=<?php echo base_url().'things/'.$productDetails->row()->id.'/'.$productDetails->row()->seourl.'?ref='.$productDetails->row()->user_name;?>"><span class="icon"></span></a></li>

                <li><a class="contributions_re" target="_blank" href="https://mixi.jp/share.pl?mode=login&u=<?php echo base_url().'things/'.$productDetails->row()->id.'/'.$productDetails->row()->seourl.'?ref='.$productDetails->row()->user_name;?>"><span class="icon"></span></a></li>
                
                <li><a class="contributions_qz" target="_blank" href="http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?url=<?php echo base_url().'things/'.$productDetails->row()->id.'/'.$productDetails->row()->seourl.'?ref='.$productDetails->row()->user_name;?>"><span class="icon"></span></a></li>
                
                <li><a class="contributions_we" target="_blank" href="http://service.weibo.com/share/share.php?url=<?php echo base_url().'things/'.$productDetails->row()->id.'/'.$productDetails->row()->seourl.'?ref='.$productDetails->row()->user_name.'&appkey=&title='.$productDetails->row()->product_name.'&pic=images/product/'.$img;?>"><span class="icon"></span></a></li>
            
            </ul>
        
        </div>
		<div class="popup_details_gift_title">
		   <h3><?php if($this->lang->line('group_gift_summary') != '') { echo stripslashes($this->lang->line('group_gift_summary')); } else echo "Group gift summary"; ?></h3>
	  </div>
	  
      <div class="group_summary">
      <input type="hidden" name="seller_id" id ="sellers-ids" value=""/>
      	<div class="group_product_img"><img src="<?php echo base_url();?>images/product/<?php echo $img;?>"
                                                    alt="<?php echo $productDetails->row()->product_name;?>"/></div>
        
        <div class="group_content">
        
        	<h2 id="gift_names"></h2>
            
            <p id="gift_descriptions"></p>
            
            
            <div class="group_gift_issue">
            
            	<div class="group_gift_issue_icon"><img id="gift_receipt_img" style="width:50px; height:50px;" src="" /></div>
                
                <ul class="group_gift_issue_detail">
                
                	<li><span><?php if($this->lang->line('group_gift_to') != '') { echo stripslashes($this->lang->line('group_gift_to')); } else echo "Gift to"; ?></span></li>
                    
                    <li><strong id="recipient_name"></strong></li>
                    
                    <li><p id="recipient_address"></p></li>
                
                </ul>
            
            </div>
        </div>
        
        <div class="group_gift_payment">
            
        	<h2><?php if($this->lang->line('group_gift_totalhead') != '') { echo stripslashes($this->lang->line('group_gift_totalhead')); } else echo "Group Gift Total"; ?></h2>
            
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="group_gift_payment_tab">
            
            	<tr>
                
                	<td><?php if($this->lang->line('groupgift_itemtotal') != '') { echo stripslashes($this->lang->line('groupgift_itemtotal')); } else echo "Item total"; ?></td>
                    
                    <td><strong id="contb-item-cost"></strong></td>
                
                
                </tr>
                
                <tr>
                
                	<td><?php if($this->lang->line('groupgift_shipping') != '') { echo stripslashes($this->lang->line('groupgift_shipping')); } else echo "Shipping"; ?></td>
                    
                    <td><strong id="contb-shipping-cost"></strong></td>
                
                
                </tr>
                
                <tr>
                
                	<td><?php if($this->lang->line('groupgift_tax') != '') { echo stripslashes($this->lang->line('groupgift_tax')); } else echo "Tax"; ?></td>
                    
                    <td><strong id ="contb-shipping-tax"></strong></td>
                
                
                </tr>
                
                <tr style="border-top: 1px solid #F4F4F4;">
                
                	<td><?php if($this->lang->line('goal_total') != '') { echo stripslashes($this->lang->line('goal_total')); } else echo "Goal Total"; ?></td>
                    
                    <td><strong id="contb-goal-total"></strong></td>
                
                
                </tr>
            
            </table>
        
        
        </div>
      
      
      </div>
      <?php } ?>
	</div>
	<div class="btn-area">
    <div class="howit_works"><i></i><a href="#"><?php echo $this->config->item('description'); ?></a></div>
		<button class="btn-done" id= "btn-done"><?php if($this->lang->line('gift_done') != '') { echo stripslashes($this->lang->line('gift_done')); } else echo "Done"; ?></button>
	</div>
	<button title="Close" class="ly-close"><i class="ic-del-black"></i></button>
</div>
<?php } ?>