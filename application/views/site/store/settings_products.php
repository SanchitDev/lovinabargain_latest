<?php
$this->load->view('site/templates/header',$this->data);
?>
<link rel="stylesheet" media="all" type="text/css" href="css/site/<?php echo SITE_COMMON_DEFINE ?>setting.css">
<!-- Section_start -->
<div class="lang-en no-subnav wider winOS">
<!-- Section_start -->
<div id="container-wrapper">
	<div class="container set_area">
		<?php if($flash_data != '') { ?>
		<div class="errorContainer" id="<?php echo $flash_data_type;?>">
			<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
			<p><span><?php echo $flash_data;?></span></p>
		</div>
		<?php } ?>
        <div id="content">
		<h2 class="ptit"><?php if($this->lang->line('store_display_products') != '') { echo stripslashes($this->lang->line('store_display_products')); } else echo "Products"; ?></h2>
	<?php if ($productDetails->num_rows() == 0){?>
		<div class=" section shipping no-data">
			<span class="icon"><i class="ic-ship"></i></span>
			<p><?php if($this->lang->line('products_no_store') != '') { echo stripslashes($this->lang->line('products_no_store')); } else echo "You haven't added any Products yet."; ?></p>
			
			<a href="seller-product"><button class="btn-shipping add_store_product"><i class="ic-plus"></i> <?php if($this->lang->line('store_add_product') != '') { echo stripslashes($this->lang->line('store_add_product')); } else echo "Add Product"; ?></button></a>
		</div>
	<?php 
	}else {
	?>
	<div class="section shipping">
            <h3><?php if($this->lang->line('store_saved_products') != '') { echo stripslashes($this->lang->line('store_saved_products')); } else echo "Your Added Products"; ?></h3>
                	<div class="chart-wrap">
            <table class="chart">
                <thead>
                    <tr>
                        <th><?php if($this->lang->line('store_prod_name') != '') { echo stripslashes($this->lang->line('store_prod_name')); } else echo "Product Name"; ?></th>
                        <th><?php if($this->lang->line('store_prod_image') != '') { echo stripslashes($this->lang->line('store_prod_image')); } else echo "Image"; ?></th>
                        <th><?php if($this->lang->line('store_prod_purchase_count') != '') { echo stripslashes($this->lang->line('store_prod_purchase_count')); } else echo "Purchased"; ?></th>
                        <th><?php if($this->lang->line('store_prod_quantity') != '') { echo stripslashes($this->lang->line('store_prod_quantity')); } else echo "Remain"; ?></th>
                        <th><?php if($this->lang->line('store_prod_price') != '') { echo stripslashes($this->lang->line('store_prod_price')); } else echo "Price"; ?></th>
						<th><?php if($this->lang->line('purchases_option') != '') { echo stripslashes($this->lang->line('purchases_option')); } else echo "Option"; ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($productDetails->result() as $row){
							$img = 'dummyProductImage.jpg';
							$imgArr = explode(',', $row->image);
							if (count($imgArr)>0){
								foreach ($imgArr as $imgRow){
									if ($imgRow != ''){
										$img = $pimg = $imgRow;
										break;
									}
								}
							}
					?>
                    <tr>
                        <td><?php echo $row->product_name;?></td>
                        <td><a href="things/<?php echo $row->id.'/'.$row->seourl;?>"><img src="<?php echo base_url();?>images/product/<?php echo $img;?>" alt="<?php echo $row->product_name;?>" height="50" width="50"></a></td>
                        <td><?php echo $row->purchasedCount;?></td>
                        <td><?php echo $row->quantity;?></td>
                        <td><?php echo $row->sale_price;?></td>
                        <td><a href="things/<?php echo $row->seller_product_id.'/edit';?>" class=""><?php if($this->lang->line('store_prod_edit') != '') { echo stripslashes($this->lang->line('store_prod_edit')); } else echo "Edit"; ?></a> / <a href="things/<?php echo $row->id.'/'.$row->seourl;?>" class=""><?php if($this->lang->line('store_prod_view') != '') { echo stripslashes($this->lang->line('store_prod_view')); } else echo "View"; ?></a>/ 
                        <?php 
						if ($row->status=='Publish'){
						?>
                        <a onclick="return confirm('Are you sure..?');" href="things/<?php echo $row->seller_product_id;?>/change_status/UnPublish?next=<?php echo urlencode('seller/dashboard/products');?>" thing_id="<?php echo $row->seller_product_id;?>" uid="<?php echo $row->user_id;?>" class=""><?php if($this->lang->line('unpublish') != '') { echo stripslashes($this->lang->line('unpublish')); } else echo "Unpublish"; ?></a>
                        <?php }else {?>
                        <a onclick="return confirm('Are you sure..?');" href="things/<?php echo $row->seller_product_id;?>/change_status/Publish?next=<?php echo urlencode('seller/dashboard/products');?>" uid="<?php echo $row->user_id;?>" class=""><?php if($this->lang->line('publish') != '') { echo stripslashes($this->lang->line('publish')); } else echo "Publish"; ?></a>
                        <?php }?>
                        </td>
                    </tr>
                    <?php }?>
                    
                </tbody>
            </table>
			</div>
            	<a href="seller-product"><button class="btn-shipping add_store_product"><i class="ic-plus"></i> <?php if($this->lang->line('store_add_product') != '') { echo stripslashes($this->lang->line('store_add_product')); } else echo "Add Product"; ?></button></a>
			</div>
	<?php 
	
	
	
	}
	?>
	</div>
	<?php 
		$this->load->view('site/store/settings_sidebar');
		$this->load->view('site/templates/side_footer_menu');
     ?>
	</div>
	<!-- / container -->
</div>
</div>
<script>
$('.add_store_product').click(function(){
	$('.mn-add').trigger('click');
});
</script>
<!-- Section_start -->