<?php $this->load->view('site/templates/header');?>
<link rel="stylesheet" href="<?php echo base_url();?>css/site/<?php echo SITE_COMMON_DEFINE ?>timeline.css" type="text/css" media="all"/>
<link rel="stylesheet" media="all" type="text/css" href="<?php echo base_url();?>css/site/<?php echo SITE_COMMON_DEFINE ?>setting.css">
<style type="text/css">
ol.stream {position: relative;}
ol.stream.use-css3 li.anim {transition:all .25s;-webkit-transition:all .25s;-moz-transition:all .25s;-ms-transition:all .25s;visibility:visible;opacity:1;}
ol.stream.use-css3 li {visibility:hidden;}
ol.stream.use-css3 li.anim.fadeout {opacity:0;}
ol.stream.use-css3.fadein li {opacity:0;}
ol.stream.use-css3.fadein li.anim.fadein {opacity:1;}
</style>
<!-- Section_start -->
<div class="lang-en no-subnav wider winOS">
<div id="container-wrapper">
	<div class="container set_area">
		<?php if($flash_data != '') { ?>
		<div class="errorContainer" id="<?php echo $flash_data_type;?>">
			<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 4000);</script>
			<p><span><?php echo $flash_data;?></span></p>
		</div>
		<?php } ?>
        <div id="content">
		<h2 class="ptit"><?php if($this->lang->line('settings_edit_store') != '') { echo stripslashes($this->lang->line('settings_edit_store')); } else echo "Edit Store Settings"; ?></h2>
		<div class="notification-bar" style="display:none"></div>
		<form class="myform" id="profile_settings_form" method="post" action="site/store/InsertEditstore" enctype="multipart/form-data">
			<div class="section profile">
				<h3 class="stit"><?php if($this->lang->line('referrals_profile') != '') { echo stripslashes($this->lang->line('referrals_profile')); } else echo "Profile"; ?></h3>
				<fieldset class="frm">
					<label><?php if($this->lang->line('profile_store_name') != '') { echo stripslashes($this->lang->line('profile_store_name')); } else echo "Store Name"; ?></label>
					<p><?php echo $storeDetails->row()->store_name;?></p>
					<label><?php if($this->lang->line('tag_line') != '') { echo stripslashes($this->lang->line('tag_line')); } else echo "Tag Line"; ?></label>
					<textarea id="tagline" class="tagline" name="tagline"><?php echo $storeDetails->row()->tagline;?></textarea>
				</fieldset>
			</div>
			<div class="section photo">
				<h3 class="stit"><?php if($this->lang->line('store_logo') != '') { echo stripslashes($this->lang->line('store_logo')); } else echo "Logo Image"; ?></h3>
				<?php $userImg = 'dummy-logo.png';
				if ($storeDetails->row()->logo_image != ''){
					$userImg = $storeDetails->row()->logo_image;
				}
				?>
				<fieldset class="frm">
				<div class="photo-preview"><img src="images/site/blank.gif" style="width:100%;height:100%;background-image:url(<?php echo base_url();?>images/store/<?php echo $userImg;?>);background-size:cover" alt="<?php echo $storeDetails->row()->store_name;?>"></div>
				<div class="logo-func">		
					<?php if ($storeDetails->row()->logo_image == ''){?>		
					<input type="button" style="cursor: pointer;" class="btn-change" onClick="$('.logo-func').hide();$('.upload-logo').show();return false;" value="<?php if($this->lang->line('header_store_logo') != '') { echo stripslashes($this->lang->line('header_store_logo')); } else echo "Upload Logo"; ?>"/>
					<?php }else {?>
					<input type="button" style="cursor: pointer;" class="btn-change" onClick="$('.logo-func').hide();$('.upload-logo').show();return false;" value="<?php if($this->lang->line('header_store_logoc') != '') { echo stripslashes($this->lang->line('header_store_logoc')); } else echo "Change Logo"; ?>"/>
					<input type="button" style="cursor: pointer;" class="btn-delete" id="delete_logo_image" onClick="return deleteUserPhoto(this.id);" value="<?php if($this->lang->line('header_store_logod') != '') { echo stripslashes($this->lang->line('header_store_logod')); } else echo "Delete Logo"; ?>"/>
					<?php }?>
				</div>
				<div class="upload-logo">
					<input id="uploadavatar" class="uploadavatar" name="logo-file" type="file">
					<span class="uploading" style="display:none"><?php if($this->lang->line('settings_uploading') != '') { echo stripslashes($this->lang->line('settings_uploading')); } else echo "Uploading..."; ?></span>
					<span class="description"><?php if($this->lang->line('store_allowedlogoimag') != '') { echo stripslashes($this->lang->line('store_allowedlogoimag')); } else echo "Allowed file types JPG, GIF or PNG.<br>Maximum width and height is 600px"; ?></span>
				</div>
				<small class="comment"><?php if($this->lang->line('lg_logo_size') != '') { echo stripslashes($this->lang->line('lg_logo_size')); } else echo "Logo size"; ?> 150 x 150 <?php if($this->lang->line('lg_pixels') != '') { echo stripslashes($this->lang->line('lg_pixels')); } else echo "pixels"; ?></small>
			</fieldset>
			</div>
			<div class="section photo">
				<h3 class="stit"><?php if($this->lang->line('store_banner') != '') { echo stripslashes($this->lang->line('store_banner')); } else echo "Banner Image"; ?></h3>
				<?php $userImg = 'banner-dummy.jpg';
				if ($storeDetails->row()->cover_image != ''){
					$userImg = $storeDetails->row()->cover_image;
				}
				?>
				<fieldset class="frm">
				<div class="photo-preview" style="width:450px;"><img src="images/site/blank.gif" style="width:100%; max-width:450px! important; height:100%;background-image:url(<?php echo base_url();?>images/store/<?php echo $userImg;?>);background-size:cover" alt="<?php echo $storeDetails->row()->store_name;?>"></div>
				<div class="banner-func">		
					<?php if ($storeDetails->row()->cover_image == ''){?>		
					<input type="button" style="cursor: pointer;" class="btn-change" onClick="$('.banner-func').hide();$('.upload-banner').show();return false;" value="<?php if($this->lang->line('header_store_banner') != '') { echo stripslashes($this->lang->line('header_store_banner')); } else echo "Upload Banner"; ?>"/>
					<?php }else {?>
					<input type="button" style="cursor: pointer;" class="btn-change" onClick="$('.banner-func').hide();$('.upload-banner').show();return false;" value="<?php if($this->lang->line('header_store_bannerc') != '') { echo stripslashes($this->lang->line('header_store_bannerc')); } else echo "Change Banner"; ?>"/>
					<input type="button" style="cursor: pointer;" class="btn-delete" id="delete_banner_image" onClick="return deleteUserPhoto(this.id);" value="<?php if($this->lang->line('header_store_bannerd') != '') { echo stripslashes($this->lang->line('header_store_bannerd')); } else echo "Delete Banner"; ?>"/>
					<?php }?>
				</div>
				<div class="upload-banner">
					<input id="uploadavatar" class="uploadavatar" name="banner-file" type="file">
					<span class="uploading" style="display:none"><?php if($this->lang->line('settings_uploading') != '') { echo stripslashes($this->lang->line('settings_uploading')); } else echo "Uploading..."; ?></span>
					<span class="description"><?php if($this->lang->line('store_allowedbannerimag') != '') { echo stripslashes($this->lang->line('store_allowedbannerimag')); } else echo "Allowed file types JPG, GIF or PNG.<br>Maximum width is 940px and height is 600px"; ?></span>
				</div>
				<small class="comment"><?php if($this->lang->line('lg_ban_size') != '') { echo stripslashes($this->lang->line('lg_ban_size')); } else echo "Banner size"; ?> 930 x 240 <?php if($this->lang->line('lg_pixels') != '') { echo stripslashes($this->lang->line('lg_pixels')); } else echo "pixels"; ?></small>
			</fieldset>
			</div>
			<div class="btn-area">
			<input type="hidden" name="user_id" value="<?php echo $loginCheck; ?>"/>
			<input type="hidden" name="store_id" value="<?php echo $storeDetails->row()->id; ?>"/>
			<input type="submit" style="cursor:pointer;" class="btn-save" id="save_account" value="<?php if($this->lang->line('settings_save_profile') != '') { echo stripslashes($this->lang->line('settings_save_profile')); } else echo "Save Profile"; ?>"/>
			<span class="checking" style="display:none"><i class="ic-loading"></i></span>
			<!--<input type="button" style="cursor:pointer;" onClick="return deactivateUser();" class="btn-deactivate" id="close_account" value="<?php if($this->lang->line('settings_deact_acc') != '') { echo stripslashes($this->lang->line('settings_deact_acc')); } else echo "Deactivate my account"; ?>"/>-->          
		</div>
		</form>
	</div>
	<?php 
     $this->load->view('site/store/settings_sidebar');
     $this->load->view('site/templates/side_footer_menu');
     ?>
	</div>
	<!-- / container -->
</div>
<script>
$('.upload-banner').hide();
$('.upload-logo').hide();
function deleteUserPhoto(id){
	if(id =='delete_banner_image'){
		$('#delete_banner_image').disable();
	}else{
		$('#delete_logo_image').disable();
	}
	var res = window.confirm('Are you sure?');
	if(res){
		$.ajax({
			type:'POST',
			url:baseURL+'site/store/delete_store_image',
			data:{'id':id},
			dataType:'json',
			success:function(response){
				if(response.success == '0'){
					alert(response.msg);
					if(id =='delete_banner_image'){
						$('#delete_banner_image').removeAttr('disabled');
					}else{
						$('#delete_logo_image').removeAttr('disabled');
					}
					return false;
				}else{
					window.location.href = baseURL+'seller/dashboard';
				}
			}
		});
	}else{
		if(id =='delete_banner_image'){
			$('#delete_banner_image').removeAttr('disabled');
		}else{
			$('#delete_logo_image').removeAttr('disabled');
		}
		return false;
	}
}
</script>
</div>
<!-- Section_start -->
<?php $this->load->view('site/templates/footer');?>