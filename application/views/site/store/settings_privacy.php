<?php $this->load->view('site/templates/header');?>
<link rel="stylesheet" href="<?php echo base_url();?>css/site/<?php echo SITE_COMMON_DEFINE ?>timeline.css" type="text/css" media="all"/>
<link rel="stylesheet" media="all" type="text/css" href="<?php echo base_url();?>css/site/<?php echo SITE_COMMON_DEFINE ?>setting.css">
<!-- Section_start -->
<div class="lang-en no-subnav wider winOS">
<div id="container-wrapper">
	<div class="container set_area">
		<?php if($flash_data != '') { ?>
		<div class="errorContainer" id="<?php echo $flash_data_type;?>">
			<script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 4000);</script>
			<p><span><?php echo $flash_data;?></span></p>
		</div>
		<?php } ?>
        <div id="content">
		<h2 class="ptit"><?php if($this->lang->line('terms_condition') != '') { echo stripslashes($this->lang->line('terms_condition')); } else echo "Terms and Conditions"; ?></h2>
		<div class="notification-bar" style="display:none"></div>
		<form class="myform" id="profile_settings_form" method="post" action="site/store/privacyUpdate" enctype="multipart/form-data">
			<div class="section profile">
				<fieldset class="frm">
					<label><?php if($this->lang->line('terms_condition') != '') { echo stripslashes($this->lang->line('terms_condition')); } else echo "Terms and Conditions"; ?></label>
					<textarea id="bio" class="setting_bio" max-length="500" name="privacy"><?php echo $storeDetails->row()->privacy; ?></textarea>
				</fieldset>
			</div>
			<div class="btn-area">
			<input type="hidden" name="user_id" value="<?php echo $loginCheck; ?>"/>
			<input type="hidden" name="store_id" value="<?php echo $storeDetails->row()->id; ?>"/>
			<input type="submit" style="cursor:pointer;" class="btn-save" id="save_account" value="<?php if($this->lang->line('settings_save_profile') != '') { echo stripslashes($this->lang->line('settings_save_profile')); } else echo "Save"; ?>"/>
			<span class="checking" style="display:none"><i class="ic-loading"></i></span>
			<!--<input type="button" style="cursor:pointer;" onClick="return deactivateUser();" class="btn-deactivate" id="close_account" value="<?php if($this->lang->line('settings_deact_acc') != '') { echo stripslashes($this->lang->line('settings_deact_acc')); } else echo "Deactivate my account"; ?>"/>-->          
		</div>
		</form>
	</div>
	<?php 
     $this->load->view('site/store/settings_sidebar');
     $this->load->view('site/templates/side_footer_menu');
     ?>
	</div>
	<!-- / container -->
</div>
</div>
<!-- Section_start -->
<?php $this->load->view('site/templates/footer');?>