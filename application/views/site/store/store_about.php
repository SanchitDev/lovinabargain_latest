<?php if($this->uri->segment(1,0)=='store'){ 
 if ($user_Details!=''){
	if ($user_Details->num_rows()>0){ 
?>
<div class="popup display-about ly-title" style="display:none;">
	<p class="ltit"><?php if($this->lang->line('store_display_about') != '') { echo stripslashes($this->lang->line('store_display_about')); } else echo "About"; ?></p>
	<div class="ltxt">
		<p class="figcaption">
		<?php if($user_Details->row()->store_name!=''){
			echo $user_Details->row()->store_name;
		}else{
			echo $user_Details->row()->user_name;
		}?></p>
		<div class="inner-ltxt">
			<?php if($user_Details->row()->description!=''){?>
				<?php echo $user_Details->row()->description;?>
			<?php } else{ ?>
				<?php if($this->lang->line('store_details_no_more') != '') { echo stripslashes($this->lang->line('store_details_no_more')); } else echo "No Details available";?>
			<?php } ?>
		</div>
	</div>
	<button title="Close" class="ly-close"><i class="ic-del-black"></i></button>
</div>
<?php }}} ?>
<script type="text/javascript"> 
$(document).ready(function(){
    var popup = $.dialog('display-about');
	$('.display_about').click(function(event){
	    popup.open();
		event.preventDefault();
	});
});
</script>
<style type="text/css">
.popup.display-about{ width: 450px; margin-top: 170px !important;}
.popup .ltxt .figcaption{margin:10px 0px 0px 20px; font-weight:bold; font-size:16px;}
.popup.display-about .ltxt { position: relative; border-radius: 0; background: #fff; color: #393d4d; float:left; width:100%; padding:0px; min-height:230px;}
.popup .ltxt .inner-ltxt{margin:10px 20px ; color: #4D515B; font-size: 13px; line-height: 20px;
    padding-bottom: 15px; text-align:justify;}
</style>